
<div class="w3-light-grey w3-padding-32 w3-margin-top" id="contact">
<div class="w3-row-padding" id="myGrid" style="margin-bottom:100px">
<div class="w3-container w3-padding-16 w3-indigo">
  <p>Perlu Training ACCURATE? Hubungi Kami di <strong>0812 9008 3983</strong></p>
  <p><span class="w3-tag">Temukan Kami!</span> Jl. Jatiwaringin Raya Blok H No.8, RT.2/RW.13, Cipinang Melayu, Makasar, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta 13620.</p>
  <p><strong>Hari buka</strong> : 09.00–17.00</p>
</div>
<div class="w3-container w3-center w3-padding-16 ">
  <h2 class="w3-center" style="margin-bottom:24px;">Follow us on facebook/instagram</h2>
  <a href="https://www.facebook.com/FAC.Institute/" target="_blank" class="btn fa fa-facebook"></a>
  <a href="https://www.instagram.com/facinstitute/" target="_blank" class="btn fa fa-instagram"></a>
</div>
</div>
</div>
