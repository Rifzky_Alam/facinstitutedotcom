<?php include_once '../baseurl.php'; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset='utf-8'>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href=<?php echo "'".getBaseUrl()."css/bootstrap.css'"; ?>>
	<title>Download Page</title>

	<script type="text/javascript" src=<?php echo "'".getBaseUrl()."js/jquery.js'";?>></script>
	<script type="text/javascript" src=<?php echo "'".getBaseUrl()."js/bootstrap.min.js'"; ?>></script>
</head>
<body>
	

	<div class='container' style="padding-top:40px">
		<div class='row'>
			<div class='page-header'>
				<h2>Here are files you can download:</h2>
			</div>
		</div>

		<div class="row">
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingOne">
						<h4>
							<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
								Accurate 5 32Bit
							</a>
						</h4>
					</div>
					<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
						<div class="panel-body">
							<ul class="nav nav-pills nav-stacked">
								<li><a href="accurate?file=acc5std32">Accurate Standard</a></li>
								<li><a href="accurate?file=acc5dlx32">Accurate Deluxe</a></li>
								<li><a href="accurate?file=acc5etp32">Accurate Enterprise</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingTwo">
						<h4>
							<a role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
								Accurate 5 64Bit
							</a>
						</h4>
					</div>
					<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
						<div class="panel-body">
							<ul class="nav nav-pills nav-stacked">
								<li><a href="accurate?file=acc5std64">Accurate Standard</a></li>
								<li><a href="accurate?file=acc5dlx64">Accurate Deluxe</a></li>
								<li><a href="accurate?file=acc5etp64">Accurate Enterprise</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="panel-group" id="accordion3" role="tablist" aria-multiselectable="true">
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="heading3">
						<h4>
							<a role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapse3" aria-expanded="true" aria-controls="collapse3">
								Supporting tools
							</a>
						</h4>
					</div>
					<div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
						<div class="panel-body">
							<ul class="nav nav-pills nav-stacked">
								<li><a href="accurate?file=acc5alm">Accurate License Manager</a></li>
								<li><a href="accurate?file=firebird">Firebird Linux</a></li>
								<li><a href="accurate?file=xmlproject">FAC - Excel to Xml 32</a></li>
								<li><a href="accurate?file=xmlproject64">FAC - Excel to XML 64</a></li>
								<li><a href="accurate?file=ipscanner">IP Scanner</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

		</div>


		<br>
		<div class='row'>
			<div class='col-md-12'>
				<a href=<?php echo "'".getBaseUrl()."'"; ?> class='btn btn-md btn-default'>Back to Home</a>
			</div>
		</div>
	</div>

	<div class='container-fluid' style='margin-top: 160px; background-color: aqua; padding: 0px;'>
		<div style='padding-left: 40px; padding-top: 8px;'>
			<p>Copyright: FAC-Institute 2016</p>
		</div>
	</div>

</body>
</html>