<?php 
if (isset($_GET['file'])) {
	
	if ($_GET['file']=='acc5std32'){
		$file="files/accurate/ACCURATE5 Standard 1790 Setup-x86.exe.zip";
	}elseif ($_GET['file']=='acc5dlx32') {
		$file="files/accurate/ACCURATE5 Deluxe-5.0.15.1790-Setup-x86.exe.zip";
	}elseif ($_GET['file']=='acc5etp32') {
		$file="files/accurate/ACCURATE5 Enterprise-5.0.15.1790-Setup-x86.exe.zip";
	}elseif ($_GET['file']=='acc5alm') {
		$file="files/accurate/ACCURATE 5 License Manager-1.0.0.306-Setup.exe.zip";
	}elseif ($_GET['file']=='acc5std64') {
		$file="files/accurate/ACCURATE5 Standard 1790 Setup-x86.exe.zip";
	}elseif ($_GET['file']=='acc5dlx64') {
		$file="files/accurate/ACCURATE5 Deluxe-5.0.15.1790-Setup-x64.exe.zip";
	}elseif ($_GET['file']=='acc5etp64') {
		$file="files/accurate/ACCURATE5 Enterprise-5.0.15.1790-Setup-x64.exe.zip";
	}elseif ($_GET['file']=='firebird') {
		$file="accurate5/FirebirdACCURATE-2.5.4.amd64.tar.gz";
	}elseif ($_GET['file']=='xmlproject') {
		$file="xl2xm/excel to xml 1-1-3.zip";
	}elseif ($_GET['file']=='xmlproject64') {
		$file="xl2xm/accuratexmlproject64x.zip";
	}elseif ($_GET['file']=='ipscanner') {
		$file="network/ipscan24.exe";
	}elseif ($_GET['file']=='rifzky_alam_cool') {
		$file="other/Billing Rifzky.zip";
	}elseif ($_GET['file']=='troubleshoot') {
		$file="other/Movedata.rar";
	}

	if (file_exists($file)){
		header("Content-Description: File Transfer");
	    header('Content-Type: application/octet-stream');
	    header('Content-Disposition: attachment; filename="'.basename($file).'"');
	    header('Expires: 0');
	    header('Cache-Control: must-revalidate');
	    header('Pragma: public');
	    header('Content-Length: ' . filesize($file));
	    readfile($file);
	    exit;

	}

}

?>