<?php 
include_once '../baseurl.php';
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0" />

	<title>Member Area || FAC Institute</title>
	<link rel="stylesheet" type="text/css" href=<?php echo "'".getBaseUrl()."css/bootstrap.min.css"."'"; ?>>
	<style type="text/css">
		body{
		    background: url(<?= getBaseUrl().'images/accounting.jpeg' ?>);
			background-color: #444;
		    background: url(<?= getBaseUrl().'images/accounting.jpeg' ?>),url(<?= getBaseUrl().'images/accounting.jpeg' ?>),url(<?= getBaseUrl().'images/accounting.jpeg' ?>);    
		}

		.vertical-offset-100{
		    padding-top:100px;
		}
	</style>
</head>
<body>

<!-- This is a very simple parallax effect achieved by simple CSS 3 multiple backgrounds, made by http://twitter.com/msurguy -->

	<div class="container">
	    <div class="row vertical-offset-100">
	    	<div class="col-md-4 col-md-offset-4">
	    		<div class="panel panel-default">
				  	<div class="panel-heading">
				    	<h3 class="panel-title">Please sign in</h3>
				 	</div>
				  	<div class="panel-body">
				    	<form accept-charset="UTF-8" role="form">
	                    <fieldset>
				    	  	<div class="form-group">
				    		    <input class="form-control" placeholder="E-mail" name="email" type="text">
				    		</div>
				    		<div class="form-group">
				    			<input class="form-control" placeholder="Password" name="password" type="password" value="">
				    		</div>
				    		<input class="btn btn-lg btn-success btn-block" type="submit" value="Login">
				    	</fieldset>
				      	</form>
				    </div>
				</div>
			</div>
		</div>
	</div>
	<script src=<?php echo "'".getBaseUrl()."js/jquery-1.11.1.js'"; ?>></script>
	<script src=<?php echo "'".getBaseUrl()."js/bootstrap.min.js'"; ?>></script>
	<!--<script src="http://mymaplist.com/js/vendor/TweenLite.min.js"></script>-->
	<script type="text/javascript">
		$(document).ready(function(){
			  $(document).mousemove(function(e){
			     TweenLite.to($('body'), 
			        .5, 
			        { css: 
			            {
			                backgroundPosition: ""+ parseInt(event.pageX/8) + "px "+parseInt(event.pageY/'12')+"px, "+parseInt(event.pageX/'15')+"px "+parseInt(event.pageY/'15')+"px, "+parseInt(event.pageX/'30')+"px "+parseInt(event.pageY/'30')+"px"
			            }
			        });
			  });
		});
	</script>
</body>
</html>