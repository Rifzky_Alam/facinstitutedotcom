<?php 
include_once '../baseurl.php';
include_once '../model/Pendaftar.php';
$pendaftar= new Pendaftar();
?>

<html>
<head>
	
	<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>Daftar Training</title>
	<link rel="stylesheet" type="text/css" href=<?php echo "'".getBaseUrl()."css/bootstrap.min.css"."'"; ?>>
	<link rel="stylesheet" type="text/css" href=<?php echo "'".getBaseUrl()."assets/registration/style.css"."'"; ?>>
	<link rel="shortcut icon" href=<?php echo "'".getBaseUrl()."images/favicon-fac.ico"."'"; ?>>
	<link rel="stylesheet" type="text/css" href=<?php echo "'".getBaseUrl()."css/datepicker/css/datepicker.css'"; ?>> 
	<link rel="stylesheet" type="text/css" href=<?php echo "'".getBaseUrl()."css/multiselect/css/bootstrap-multiselect.css'"; ?>> 
	<link rel="stylesheet" href=<?php echo "'".getBaseUrl()."css/styles.css"."'"; ?> />
	<link rel="stylesheet" type="text/css" href=<?php echo "'".getBaseUrl()."css/custom.css"."'"; ?>>
	<link rel="stylesheet" type="text/css" href=<?php echo "'".getBaseUrl()."css/set1.css"."'"; ?> />
	<script src=<?php echo "'".getBaseUrl()."js/jquery-1.11.1.js'"; ?>></script>
	<style type="text/css">
	#top-gap{
		padding-top: 80px;
	}
	</style>
</head>
<body>
	<?php $page='orderan' ?>
	<div id='top-gap'></div>
	<div class='container'>
		<?php include_once '../header.php'; ?>
	<div class="page-header">
		<h2>Daftar Training ACCURATE</h2>
		<p>By FAC-Institute</p>
	</div>

	<div class="stepwizard">
    <div class="stepwizard-row setup-panel">
        <div class="stepwizard-step">
            <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
            <p>Data Perusahaan</p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
            <p>Data Training</p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
            <p>Data Personal</p>
        </div>
    </div>
</div>
<form role="form" action="" method="post">
    <div class="row setup-content" id="step-1">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3> Step 1</h3>


                <div class="row">
                <?php 
                if (isset($_POST['data'])) {

                    // print_r($_POST['data']);
                    $objek = array(
                                    'np' => $_POST['data']['namaUsaha'],
                                    'ap' => $_POST['data']['alamatUsaha'],
                                    'ep' => $_POST['data']['email'],
                                    'tp' => $_POST['data']['telepon'],
                                    'kp' => $_POST['data']['kota'],
                                    'pp' => $_POST['data']['provinsi'],
                                    'deskripsi' => $_POST['data']['deskripsi'],
                                    'jnsPengguna' => $_POST['data']['jnsPengguna'], 
                                    'jnsAccurate'=> $_POST['data']['jnsAccurate'],
                                    'paket'=> $_POST['data']['paket'],
                                    'qty'=> $_POST['data']['qty'],
                                    'transport'=> $_POST['data']['transport'],
                                    'qtyTrans'=> $_POST['data']['qtyTrans'],
                                    'tempatBeli'=> $_POST['data']['tempatBeli'],
                                    'sales'=> $_POST['data']['sales'],
                                    'namaPersonal'=>$_POST['data']['namaPersonal'],
                                    'teleponPersonal'=>$_POST['data']['teleponPersonal'],
                                    'jabatan' => $_POST['data']['jabatan'],
                                    'emailPersonal'=> $_POST['data']['emailPersonal']

                    );

                    $objek = (object) $objek;

                    echo "Nama Usaha: ".$objek->np."<br>";
                    echo "Alamat Usaha: ".$objek->ap."<br>";
                    echo "email Usaha: ".$objek->ep."<br>";
                    echo "Telepon Usaha: ".$objek->tp."<br>";
                    echo "Kota Usaha: ".$objek->kp."<br>";
                    echo "Provinsi Usaha: ".$objek->pp."<br>";
                    echo "deskripsi Usaha: ".$objek->deskripsi."<br>";
                    echo "jenis pengguna: ".$objek->jnsPengguna."<br>";
                    echo "jenis accurate: ".implode(" # ", $objek->jnsAccurate)."<br>";
                    echo "paket: ".$objek->paket."<br>";
                    echo "qty: ".$objek->qty."<br>";
                    echo "transport: ".$objek->transport."<br>";
                    echo "Qty Transport: ".$objek->qtyTrans."<br>";
                    echo "tempat-beli: ".$objek->tempatBeli."<br>";
                    echo "sales: ".$objek->sales."<br>";
                    echo "nama-personal: ".$objek->namaPersonal."<br>";
                    echo "telepon-personal: ".$objek->teleponPersonal."<br>";
                    echo "jabatan: ".$objek->jabatan."<br>";
                    echo "email-personal: ".$objek->emailPersonal."<br>";

                }
                ?>
                </div>


                <div class="form-group">
                    <label class="control-label">Nama Perusahaan</label>
                    <input  maxlength="100" type="text" required="required" name="data[namaUsaha]" class="form-control" placeholder="Nama Perusahaan" />
                </div>
                <div class="form-group">
                    <label class="control-label">Alamat Perusahaan</label>
                    <textarea class="form-control" name="data[alamatUsaha]" required="required" id="alamatPerusahaan" placeholder="(Berdasarkan lokasi diadakan training) Boleh di isi dengan alamat google map"></textarea>
                </div>
                <div class="form-group">
                	<label>E-Mail Perusahaan</label>
                	<input type="text" name="data[email]" class="form-control" placeholder="tidak wajib">
                </div>
                <div class="form-group">
                	<label>Telepon Perusahaan</label>
                	<input type="text" name="data[telepon]" class="form-control" placeholder="tidak wajib">
                </div>
                <div class="form-group">
                	<label>Kota/Kabupaten</label>
                	<input type="text" name="data[kota]" required="required" class="form-control" placeholder="Berdasarkan lokasi diadakan training">
                </div>
                <div class="form-group">
                	<label>Provinsi</label>
                	<input type="text" name="data[provinsi]" required="required" class="form-control" placeholder="Berdasarkan lokasi diadakan training">
                </div>
                <div class="form-group">
                    <label class="control-label">Deskripsi Usaha</label>
                    <textarea class="form-control" name="data[deskripsi]" required="required" id="alamatPerusahaan" placeholder="Deskripsikan usaha anda"></textarea>
                </div>

                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-2">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3> Step 2</h3>


                <div class="form-group">
                <h4>Jenis Pengguna</h4>
                    <div class="radio"><label><input type="radio" name="data[jnsPengguna]" value="baru">Baru</label></div>
                    <div class="radio"><label><input type="radio" name="data[jnsPengguna]" value="lama">Lama</label></div>
                </div>

                <div class="form-group">
                    <h4>Versi Accurate</h4>
                    <div class="checkbox"><label><input type="checkbox" name="data[jnsAccurate][]" value="accurate 4 standard edition">ACCURATE 4 Standar Edition</label></div>
                    <div class="checkbox"><label><input type="checkbox" name="data[jnsAccurate][]" value="accurate 4 deluxe edition">ACCURATE 4 Deluxe Edition</label></div>
                    <div class="checkbox"><label><input type="checkbox" name="data[jnsAccurate][]" value="accurate 4 enterprise edition">ACCURATE 4 Enterprise Edition</label></div>
                    <div class="checkbox"><label><input type="checkbox" name="data[jnsAccurate][]" value="accurate 5 standard edition">ACCURATE 5 Standar Edition</label></div>
                    <div class="checkbox"><label><input type="checkbox" name="data[jnsAccurate][]" value="accurate 5 deluxe edition">ACCURATE 5 Deluxe Edition</label></div>
                    <div class="checkbox"><label><input type="checkbox" name="data[jnsAccurate][]" value="accurate 5 enterprise edition">ACCURATE 5 Enterprise Edition</label></div>
                    <div class="checkbox"><label><input type="checkbox" name="data[jnsAccurate][]" value="accurate cloud">ACCURATE Online/Cloud</label></div>
                    <div class="checkbox"><label><input type="checkbox" name="data[jnsAccurate][]" value="rene point of sales">RENE Point Of Sales</label></div>
                    <div class="checkbox"><label><input type="checkbox" name="data[jnsAccurate][]" value="rene dan accurate">Gabungan RENE dan ACCURATE</label></div>
                    <div class="form-inline"><input type="text" class="form-control" name="data[jnsAccurate][]" id="jns-accurateLainnya" placeholder='lainnya'></div>
                </div>

                
                <div class="form-group">
                    <label><a href="input-paket">Nama Paket</a></label>
                    <select class="form-control" id="nitem" name="data[paket]" required>
                        <?php $dataSelect = json_decode($pendaftar->getPaketForWeb()); ?>
                        <?php for ($i=0; $i < count($dataSelect) ; $i++) { 
                            echo "<option value='".$dataSelect[$i]->id."'>".$dataSelect[$i]->nama_item." - Harga: Rp ".number_format($dataSelect[$i]->harga)."</option>";
                        } ?>                    
                    </select>
                </div>
                
                <br>

               <div class="form-group">
                    <label for="qty">Agenda Training/Kebutuhan Anda</label>
                    <textarea class="form-control" name="data[qty]" id="agendaTraining" placeholder="Tuliskan apa kebutuhan anda di dalam software Accurate atau jasa Accounting Service Contoh: setup database, penjelasan fitur accurate, entry data" ></textarea>
                </div>
                            

                <div class="form-group">
                    <label for="biaya-transport">Biaya Transport</label>
                    <input class="form-control" type="text" name="data[transport]" id="biaya-transport" placeholder="Hanya Angka" value="150000" required>
                </div>
                            
                <div class="form-group">
                    <label for="harian-transport">Jumlah Hari Transport</label>
                    <input class="form-control" type="number" name="data[qtyTrans]" id="harian-transport" placeholder="Hanya Angka" value="1" maxlength="1" required>
                </div>
            
                <div class="form-group">
                    <label for="tempat-beli">Tempat Membeli Accurate</label>
                    <input class="form-control" type="text" name="data[tempatBeli]" id="tempat-beli" placeholder="Jika Anda Tahu Tempat Membeli Software Kami">
                </div>
                    
                        
                <div class="form-group">
                    <label for="sales">Salesman Accurate</label>
                    <input class="form-control" type="text" name="data[sales]" id="sales" placeholder="Jika Anda Tahu Nama Salesman yang Menjual Software Accurate Kepada Anda">
                </div>
                            
                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-3">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3> Step 3</h3>

                <div class="form-group">
                    <label for="form-namaPersonal">Nama Personal Pendaftar</label>
                    <input class="form-control" name="data[namaPersonal]" id="form-namaPersonal" placeholder='' required>
                </div>
                
                <div class="form-group">
                    <label for="form-teleponCP">Telepon Personal</label>
                    <input class="form-control" name="data[teleponPersonal]" id="form-teleponCP" placeholder='' required>
                </div>
                
                <div class="form-group">
                    <label for="form-emailCP">E-mail Personal Kontak</label>
                    <input class="form-control" type="email" name="data[emailPersonal]" id="form-emailCP" placeholder='' required>
                </div>
                
                <div class="form-group">
                    <label for="form-jabatanCP">Jabatan Personal Kontak</label>
                    <input class="form-control" type="text" name="data[jabatan]" id="form-jabatanCP" placeholder='optional'>
                </div>

                <button class="btn btn-success btn-lg" style="width:100%" type="submit">Finish!</button>
            </div>
        </div>
    </div>
</form>
		

	</div>
 
	
	<script src=<?php echo "'".getBaseUrl()."js/bootstrap.min.js'"; ?>></script>
	<script src=<?php echo "'".getBaseUrl()."js/custom.js"."'"; ?>></script>
	<script src=<?php echo "'".getBaseUrl()."js/slippry.min.js"."'"; ?>></script>
	<script type="text/javascript" src=<?php echo "'".getBaseUrl()."css/multiselect/js/bootstrap-multiselect.js'"; ?>></script>
	<script type="text/javascript" src=<?php echo "'".getBaseUrl()."css/datepicker/js/bootstrap-datepicker.js'"; ?>></script>
	<script type="text/javascript">

	$(document).ready(function(){
		var apakah = 0;
		$('#cobaa').datepicker({
		   format: "yyyy-mm-dd"
		});
		$('#fr-accurateLainnya').hide();


		$('#tambahTombol').click(function(){
    
		    var cobayah = $('.tglPelaksanaan').length;
		    var iseng = "tambahan" + cobayah;
		    if ($('#tambahan').length){

        if (cobayah==2) {
            $('#tambahan').after("<div class='row' id='"+iseng+"'><div class='col-md-10'><div class='form-inline'><input name='rq[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"'+iseng+'"'+")'></span></div></div></div>");
        }else{
            var okeh = cobayah-1;
            var iseng = "tambahan" + okeh;
            $("#"+iseng).after("<div class='row' id='tambahan"+cobayah+"'><div class='col-md-10'><div class='form-inline'><input name='rq[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"tambahan'+cobayah+'"'+")'></span></div></div></div>");
        };
            //nilai +=1;
         }else{
        	$('#jajal').after("<div class='row' id='tambahan'><div class='col-md-10'><div class='form-inline'><input name='rq[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"'+"tambahan"+'"'+")'></span></div></div></div>");
    	};

    	$('.tglPelaksanaan').datepicker({
	        format: 'yyyy-mm-dd'
	    });
	    
	});

		$('.tglPelaksanaan').datepicker({
	        format: 'yyyy-mm-dd'
	    });

	$('#try').click(function(){
		var cobaz = $("#example-getting-started option:selected").map(function () {return this.value;}).get().join(" # ");
		alert(cobaz);
	});

	});

	function removeElement(id) {
	    $('#'+id).remove();
	}




	function getValue(){
	  var x=document.getElementById("example-getting-started");
	  for (var i = 0; i < x.options.length; i++) {
	     if(x.options[i].selected){
	          if (x.options[i].value=='lainnya') {
	          	$('#fr-accurateLainnya').show();
	          }else{
	          	$('#fr-accurateLainnya').hide();
	          };
	      }
	  }
	}

	</script>

<script type="text/javascript">
    $(document).ready(function () {

    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url'],#alamatPerusahaan"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
    
    });
</script>

</body>
</html>