<?php
$data = array(
        'base_url' => base_url(),
				'judul' => 'home'
    );
    $data = (object) $data;
?>



<?php
include_once '../baseurl.php';
include_once '../model/Mail.php';
include_once '../model/Querybuilder.php';
$db = new QueryBuilder();

$daftarmarketing = $db->FetchWhere(['username'],'users',array($db->GetCond('team','=','3'),$db->GetCond('status','=','aktif')));
?>
<?php
function base_url(){
  $currentPath = $_SERVER['PHP_SELF'];$pathInfo = pathinfo($currentPath);$hostName = $_SERVER['HTTP_HOST'];$protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https://'?'https://':'http://';
  return 'https://'.$hostName."/";
	// return 'http://localhost/facftp/';
}
?>

<html>
<head>

	<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>Daftar Training</title>

	<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
	<link rel="shortcut icon" href='https://fac-institute.com/_caramel/assets/img/g25992_9_kUd_icon.ico'>

	<link rel="shortcut icon" href=<?php echo "'".getBaseUrl()."images/favicon-fac.ico"."'"; ?>> -->
	<link rel="stylesheet" type="text/css" href=<?php echo "'".getBaseUrl()."css/datepicker/css/datepicker.css'"; ?>>
	<link rel="stylesheet" type="text/css" href=<?php echo "'".getBaseUrl()."css/multiselect/css/bootstrap-multiselect.css'"; ?>>
	<!-- <link rel="stylesheet" href=<?php echo "'".getBaseUrl()."css/styles.css"."'"; ?> />
	<link rel="stylesheet" type="text/css" href=<?php echo "'".getBaseUrl()."css/custom.css"."'"; ?>>
	<link rel="stylesheet" type="text/css" href=<?php echo "'".getBaseUrl()."css/set1.css"."'"; ?> /> -->


	<link rel="stylesheet" href="https://fac-institute.com/assets/homepage2019/css/linearicons.css">
	<link rel="stylesheet" href="https://fac-institute.com/assets/homepage2019/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://fac-institute.com/assets/homepage2019/css/bootstrap.css">
	<link rel="stylesheet" href="https://fac-institute.com/assets/homepage2019/css/magnific-popup.css">
	<link rel="stylesheet" href="https://fac-institute.com/assets/homepage2019/css/nice-select.css">
	<link rel="stylesheet" href="https://fac-institute.com/assets/homepage2019/css/animate.min.css">
	<link rel="stylesheet" href="https://fac-institute.com/assets/homepage2019/css/owl.carousel.css">
	<link rel="stylesheet" href="https://fac-institute.com/assets/homepage2019/css/main.css">

	<style media="screen">
		.banner-content h1 {
		font-size: 40px;
		}
		.nav-menu {
		padding-top: 15px;
		}
		.nav-menu a {
		padding: 1px 8px 1px 8px;
		font-size: 16px;
		font-weight: 500;
		}
		.nav-menu ul li a {
		font-size: 14px;
		}
		.menu-active {
		border-bottom: 5px solid #df003a;
		}
		.navbar {
		padding: 0.2rem 1rem;
		}
	</style>
	<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">


	<style type="text/css">
	#top-gap{
		padding-top: 80px;
	}
	</style>
</head>

<body>
	<?php $page='orderan' ?>
	<div id='top-gap'></div>
	<div class='container'>

		<?php include_once '../view/homepage/header.homepage.php'; ?>

	<div class="page-header">
		<h2>Daftar Training ACCURATE</h2>
		<p>By FAC-Institute</p>
	</div>


		<?php if (isset($_POST['rq'])): ?>

			<div class='row'>
				<?php
			if (!empty($_POST['rq']['np'])&&!empty($_POST['rq']['namaAnda'])&&!empty($_POST['rq']['teleponAnda'])&&!empty($_POST['rq']['email'])){


				include_once '../model/Pendaftar.php';
				$surat = new FACMail();

				$dataEmail = array(
					'to' => 'training@fac-institute.com',
					'cc'=>array('rifzky.mail@gmail.com','fajarfifa@gmail.com'),
					'nama'=> '',
					'subject'=> 'Online Registration of '.$_POST['rq']['np'],
					'content'=> 'System detected that a client has sucessfully registered on FAC Institute Online Registration System for Accurate Software Training.'
				);
				$dataEmail = (object) $dataEmail;




				$control = new Pendaftar();
					$arrInput = array(
						'perusahaan' => $_POST['rq']['np'],
						'usaha'=> $_POST['rq']['jenisUsaha'],
						'namacp' => $_POST['rq']['namaAnda'],
						'telepon' => $_POST['rq']['teleponAnda'],
						'email' => $_POST['rq']['email'],
						'paket'=> $_POST['rq']['pakettraining'],
						'jabatan' => $_POST['rq']['jabatan'],
						'pengguna' => $_POST['rq']['jenisPengguna'],
						'versi' => implode(" # ", $_POST['rq']['jnsAccurate']),
						'agenda' => $_POST['rq']['agenda'],
						'tanggal'=> implode(' # ',$_POST['rq']['tanggalPelaksanaan']),
						'jam'=>$_POST['rq']['waktu'],
						'alamatTraining'=>$_POST['rq']['tempatTraining'],
						'tempatbeli' => $_POST['rq']['tempatBeli'],
						'st'=>'requested',
						'sales' => $_POST['rq']['salesman']
					);
				$myObj = (object) $arrInput;

				if ($control->masukDataRequest($myObj)){
					$surat->sendMailWithCC($dataEmail);
					foreach ($daftarmarketing as $key) {
						$db->InsertData('notifikasi',
						    [
							    'ntf_judul' => 'A new Client Registration on Website.',
							    'ntf_desc' => $_POST['rq']['namaAnda'].' ('.$_POST['rq']['np'].')'. ' has registered on website for training accurate',
							    'ntf_username' => $key['username'],
							    'ntf_url_target' => $data->base_url.'administrasi/marketing/data-pendaftar'
						    ]
		    			);
					}

					echo "<script>alert('Data berhasil disimpan, harap tunggu konfirmasi dari kami.');</script>";
					echo "<script>location.replace('http://www.fac-institute.com');</script>";
				}else{
					echo "<script>alert('Data gagal disimpan, anda dapat mengubungi kami via telepon atau chat di bawah bila menemukan kesulitan dalam mendaftar.');</script>";
				}
			}else{
				echo "<script>alert('Harap lengkapi data anda.');</script>";
			}
				?>
			</div><br>
		<?php endif ?>

		<form class="mx-2 my-auto d-inline w-100" action='' method='POST'>

			<div class='form-group'>
				<div class='col-md-10'>
					<?php if (isset($_POST['rq']['np'])): ?>
						<?php if (empty($_POST['rq']['np'])): ?>
							<label style="color:red">Nama Perusahaan *</label>
						<?php else: ?>
							<label>Nama Perusahaan *</label>
						<?php endif ?>
						<input class='form-control' name='rq[np]' type='text' value="<?php echo $_POST['rq']['np'] ?>">
						<?php else: ?>
						<label>Nama Perusahaan *</label>
						<input class='form-control' name='rq[np]' type='text' required >
					<?php endif ?>
				</div>
			</div>

		<br>

			<div class='form-group'>
				<div class='col-md-10'>
					<label>Deskripsi Usaha</label>
					<?php if (isset($_POST['rq']['jenisUsaha'])): ?>
					<textarea class='form-control' name='rq[jenisUsaha]' placeholder='Jelaskan kepada kami seperti apa usaha anda, contoh: perusahaan saya bergerak di pertambangan'><?php echo $_POST['rq']['jenisUsaha']; ?></textarea>
						<?php else: ?>
					<textarea class='form-control' name='rq[jenisUsaha]' placeholder='Jelaskan kepada kami seperti apa usaha anda, contoh: perusahaan saya bergerak di pertambangan' required></textarea>
					<?php endif ?>
				</div>
			</div>
		<br>

			<div class='form-group'>
				<div class='col-md-10'>
					<?php if (isset($_POST['rq']['namaAnda'])): ?>
					<?php if (empty($_POST['rq']['namaAnda'])): ?>
						<label style="color:red">Nama Anda *</label>
					<?php else: ?>
						<label>Nama Anda *</label>
					<?php endif ?>
					<input class='form-control' name='rq[namaAnda]' type='text' value="<?php echo $_POST['rq']['namaAnda']; ?>" >
						<?php else: ?>
					<label>Nama Anda *</label>
					<input class='form-control' name='rq[namaAnda]' type='text' required>
					<?php endif ?>
				</div>
			</div>
		<br>

			<div class='form-group'>
				<div class='col-md-10'>
					<?php if (isset($_POST['rq']['teleponAnda'])): ?>
						<?php if (empty($_POST['rq']['teleponAnda'])): ?>
						<label style="color:red">Telepon Anda *</label>
							<?php else: ?>
						<label>Telepon Anda *</label>
						<?php endif ?>
						<input class='form-control' name='rq[teleponAnda]' type='text' value="<?php echo $_POST['rq']['teleponAnda']; ?>">
						<?php else: ?>
						<label>Telepon Anda *</label>
						<input class='form-control' name='rq[teleponAnda]' type='text' required>
					<?php endif ?>
				</div>
			</div>
		<br>

			<div class='form-group'>
				<div class='col-md-10'>
					<?php if (isset($_POST['rq']['email'])): ?>
					<?php if (empty($_POST['rq']['email'])): ?>
					<label style="color:red">Email *</label>
							<?php else: ?>
					<label>Email *</label>
					<?php endif ?>
					<input class='form-control' name='rq[email]' type='email' value="<?php echo $_POST['rq']['email'] ?>" >
						<?php else: ?>
					<label>Email *</label>
					<input class='form-control' name='rq[email]' type='email' required>
					<?php endif ?>
				</div>
			</div>
		<br>

			<div class='form-group'>
				<div class='col-md-10'>
					<label>Jabatan anda di perusahaan</label>
					<?php if (isset($_POST['rq']['jabatan'])): ?>
					<input class='form-control' name='rq[jabatan]' type='text' value="<?php echo $_POST['rq']['jabatan'] ?>" />
						<?php else: ?>
					<input class='form-control' name='rq[jabatan]' type='text' />
					<?php endif ?>
				</div>
			</div>
		<br>

			<div class='form-group'>
				<div class='col-md-10'>
					<label>Apakah anda pernah memakai Accurate Sofware?</label>
					<?php if (isset($_POST['rq']['jabatan'])): ?>
					<select name='rq[jenisPengguna]' class='form-control'>
					<?php
					if ($_POST['rq']['jabatan']=='baru') {
						echo "<option value='baru' selected>Tidak Pernah</option>";
						echo "<option value='lama'>Ya, Saya pernah pakai software Accurate</option>";
					}else if($_POST['rq']['jabatan']=='lama'){
						echo "<option value='baru'>Tidak Pernah</option>";
						echo "<option value='lama' selected>Ya, Saya pernah pakai software Accurate</option>";
					}else{
						echo "<option value='baru'>Tidak Pernah</option>";
						echo "<option value='lama'>Ya, Saya pernah pakai software Accurate</option>";
					}
					?>
					</select>
						<?php else: ?>
					<select name='rq[jenisPengguna]' class='form-control'>
						<option value='baru'>Tidak Pernah</option>
						<option value='lama'>Ya, Saya pernah pakai software Accurate</option>
					</select>
					<?php endif ?>

				</div>
			</div>
		<br>


			<div class="form-group">
				<div class="col-md-10">
					<label>Jenis Paket</label> <a href=<?php echo "'".getBaseUrl()."order/"."'"; ?>><span class="glyphicon glyphicon-question-sign"></span></a>
					<select name="rq[pakettraining]" class="form-control">
						<option value="os">Onsite Standard</option>
						<option value="ps">Paket Standard</option>
						<option value="oe">Onsite Expert</option>
						<option value="pe1">Paket Expert 1</option>
						<option value="pe2">Paket Expert 2</option>
						<option value="aol1">Accurate Online 1</option>
						<option value="aol2">Accurate Online 2</option>
					</select>

				</div>
			</div>
			<br>

		<div id='jajal'>
			<div class="col-md-10">
				<label for="tgl-pelaksanaan">Kapan agenda dimulai?</label>
					<div class="form-inline mb-10" >
						<input type="text" id="tgl-pelaksanaan" name='rq[tanggalPelaksanaan][]' class="form-control tglPelaksanaan" placeholder="klik disini untuk memilih tanggal"/>
							<span class=' mx-2 my-auto btn btn-sm btn-primary' id='tambahTombol'>Tambah Tanggal</span>
					</div>
			</div>
		</div>
		<br>

		<div id='jajal'>
			<div class="col-md-10 mb-10">
				<label for="">Waktu</label>
					<div class="form-inline" >
					<?php if (isset($_POST['rq']['waktu'])): ?>
					<input type="text"  name='rq[waktu]' class="form-control" placeholder="format: 09:00-16:00" value="<?php echo $_POST['rq']['waktu']; ?>" />
						<?php else: ?>
					<input type="text"  name='rq[waktu]' class="form-control" placeholder="format: 09:00-16:00" value="09:00-16:00" />
					<?php endif ?>
					</div>
			</div>
		</div>
		<br>

		<div id='jajal'>
			<div class="col-md-10 mb-10">
				<label for="">Lokasi Training/Jasa Accounting Service Diadakan</label>
				 <textarea type="text" name='rq[tempatTraining]' class="form-control" placeholder="Alamat tempat training di adakan, bisa di isi dengan pin Google Map (URL)"></textarea>
			</div>
		</div>
		<br>

            <div class="col-md-10 mb-10">
                <div class="form-group">
                    <h4 class="mb-10">Versi Accurate</h4>
          <div class="checkbox"><label><input type="checkbox" name="rq[jnsAccurate][]" value="accurate 4 standard edition"> ACCURATE 4 Standar Edition</label><a target="_blank" href="http://cpssoft.com/allproducts/acc" class="btn glyphicon glyphicon-question-sign"></a></div>
					<div class="checkbox"><label><input type="checkbox" name="rq[jnsAccurate][]" value="accurate 4 deluxe edition"> ACCURATE 4 Deluxe Edition</label><a target="_blank" href="http://cpssoft.com/allproducts/acc" class="btn glyphicon glyphicon-question-sign"></a></div>
					<div class="checkbox"><label><input type="checkbox" name="rq[jnsAccurate][]" value="accurate 4 enterprise edition"> ACCURATE 4 Enterprise Edition</label><a target="_blank" href="http://cpssoft.com/allproducts/acc" class="btn glyphicon glyphicon-question-sign"></a></div>
					<div class="checkbox"><label><input type="checkbox" name="rq[jnsAccurate][]" value="accurate 5 standard edition"> ACCURATE 5 Standar Edition</label><a target="_blank" href="http://cpssoft.com/allproducts/acc" class="btn glyphicon glyphicon-question-sign"></a></div>
					<div class="checkbox"><label><input type="checkbox" name="rq[jnsAccurate][]" value="accurate 5 deluxe edition"> ACCURATE 5 Deluxe Edition</label><a target="_blank" href="http://cpssoft.com/allproducts/acc" class="btn glyphicon glyphicon-question-sign"></a></div>
					<div class="checkbox"><label><input type="checkbox" name="rq[jnsAccurate][]" value="accurate 5 enterprise edition"> ACCURATE 5 Enterprise Edition</label><a target="_blank" href="http://cpssoft.com/allproducts/acc" class="btn glyphicon glyphicon-question-sign"></a></div>
					<div class="checkbox"><label><input type="checkbox" name="rq[jnsAccurate][]" value="accurate cloud"> ACCURATE Online/Cloud</label><a target="_blank" href="http://cpssoft.com/allproducts/acc" class="btn glyphicon glyphicon-question-sign"></a></div>
					<div class="checkbox"><label><input type="checkbox" name="rq[jnsAccurate][]" value="rene point of sales"> RENE Point Of Sales</label><a target="_blank" href="http://cpssoft.com/allproducts/rn2" class="btn glyphicon glyphicon-question-sign"></a></div>
					<div class="checkbox"><label><input type="checkbox" name="rq[jnsAccurate][]" value="rene dan accurate"> Gabungan RENE dan ACCURATE</label></div>
					<div class="form-inline">
					<input type="text" style="width:75%" class="form-control" name="rq[jnsAccurate][]" id="jns-accurateLainnya" placeholder='lainnya'>
					</div>
				</div>
			</div>
		<br>

			<div class='form-group'>
				<div class='col-md-10'>
					<label>Agenda Training/Kebutuhan anda</label>
					<?php if (isset($_POST['rq']['agenda'])): ?>
					<textarea name='rq[agenda]' placeholder='Tuliskan apa kebutuhan anda di dalam software Accurate atau jasa Accounting Service Contoh: setup database, penjelasan fitur accurate, entry data' class='form-control'><?php echo $_POST['rq']['agenda'] ?></textarea>
						<?php else: ?>
					<textarea name='rq[agenda]' placeholder='Tuliskan apa kebutuhan anda di dalam software Accurate atau jasa Accounting Service Contoh: setup database, penjelasan fitur accurate, entry data' class='form-control'></textarea>
					<?php endif ?>

				</div>
			</div>
		<br>

			<div class='form-group'>
				<div class='col-md-10'>
					<label>Dimanakah Anda Membeli Software Accurate?</label>
					<?php if (isset($_POST['rq']['tempatBeli'])): ?>
					<input class='form-control' name='rq[tempatBeli]' type='text' value="<?php echo $_POST['rq']['tempatBeli'] ?>" />
						<?php else: ?>
					<input class='form-control' name='rq[tempatBeli]' type='text' />
					<?php endif ?>
				</div>
			</div>
		<br>

			<div class='form-group'>
				<div class='col-md-10'>
					<label>Salesman Accurate</label>
					<?php if (isset($_POST['rq']['salesman'])): ?>
					<input class='form-control' name='rq[salesman]' type='text' placeholder='Beritahu kami dengan siapa anda membeli accurate' value="<?php echo $_POST['rq']['salesman'] ?>" />
						<?php else: ?>
					<input class='form-control' name='rq[salesman]' type='text' placeholder='Beritahu kami dengan siapa anda membeli accurate' />
					<?php endif ?>
				</div>
			</div>
		<br>

			<div class="col-md-10 mb-40">
				<button class='btn btn-lg btn-primary' style="width:200px">Submit</button>
			</div>

		</form>

		</div>
	</div>

	<?php include_once '../view/homepage/footer.homepage.php'; ?>
			<script src="https://fac-institute.com/assets/homepage2019/js/vendor/jquery-2.2.4.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
			<script src="https://fac-institute.com/assets/homepage2019/js/vendor/bootstrap.min.js"></script>
			<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
			<script src="https://fac-institute.com/assets/homepage2019/js/easing.min.js"></script>
			<script src="https://fac-institute.com/assets/homepage2019/js/hoverIntent.js"></script>
			<script src="https://fac-institute.com/assets/homepage2019/js/superfish.min.js"></script>
			<script src="https://fac-institute.com/assets/homepage2019/js/jquery.ajaxchimp.min.js"></script>
			<script src="https://fac-institute.com/assets/homepage2019/js/jquery.magnific-popup.min.js"></script>
			<script src="https://fac-institute.com/assets/homepage2019/js/owl.carousel.min.js"></script>
			<script src="https://fac-institute.com/assets/homepage2019/js/jquery.sticky.js"></script>
			<script src="https://fac-institute.com/assets/homepage2019/js/jquery.nice-select.min.js"></script>
			<script src="https://fac-institute.com/assets/homepage2019/js/parallax.min.js"></script>
			<script src="https://fac-institute.com/assets/homepage2019/js/mail-script.js"></script>
			<script src="https://fac-institute.com/assets/homepage2019/js/main.js"></script>
			<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>


	<script src=<?php echo "'".getBaseUrl()."js/bootstrap.min.js'"; ?>></script>
	<script src=<?php echo "'".getBaseUrl()."js/custom.js"."'"; ?>></script>
	<script src=<?php echo "'".getBaseUrl()."js/slippry.min.js"."'"; ?>></script>
	<script type="text/javascript" src=<?php echo "'".getBaseUrl()."css/multiselect/js/bootstrap-multiselect.js'"; ?>></script>
	<script type="text/javascript" src=<?php echo "'".getBaseUrl()."css/datepicker/js/bootstrap-datepicker.js'"; ?>></script>
	<script type="text/javascript">

	$(document).ready(function(){
		var apakah = 0;
		$('#cobaa').datepicker({
		   format: "yyyy-mm-dd"
		});
		$('#fr-accurateLainnya').hide();


		$('#tambahTombol').click(function(){

		    var cobayah = $('.tglPelaksanaan').length;
		    var iseng = "tambahan" + cobayah;
		    if ($('#tambahan').length){

        if (cobayah==2) {
            $('#tambahan').after("<div id='"+iseng+"'><div class='col-md-10 mb-10'><div class='form-inline'><input name='rq[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger fa fa-minus onclick='removeElement("+'"'+iseng+'"'+")'></span></div></div></div>");
        }else{
            var okeh = cobayah-1;
            var iseng = "tambahan" + okeh;
            $("#"+iseng).after("<div id='tambahan"+cobayah+"'><div class='col-md-10 mb-10'><div class='form-inline'><input name='rq[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger fa fa-minus' onclick='removeElement("+'"tambahan'+cobayah+'"'+")'></span></div></div></div>");
        };
            //nilai +=1;
         }else{
        	$('#jajal').after("<div id='tambahan'><div class='col-md-10 mb-10'><div class='form-inline'><input name='rq[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger fa fa-minus' onclick='removeElement("+'"'+"tambahan"+'"'+")'></span></div></div></div>");
    	};

    	$('.tglPelaksanaan').datepicker({
	        format: 'yyyy-mm-dd'
	    });

	});

		$('.tglPelaksanaan').datepicker({
	        format: 'yyyy-mm-dd'
	    });

	$('#try').click(function(){
		var cobaz = $("#example-getting-started option:selected").map(function () {return this.value;}).get().join(" # ");
		alert(cobaz);
	});

	});

	function removeElement(id) {
	    $('#'+id).remove();
	}




	function getValue(){
	  var x=document.getElementById("example-getting-started");
	  for (var i = 0; i < x.options.length; i++) {
	     if(x.options[i].selected){
	          if (x.options[i].value=='lainnya') {
	          	$('#fr-accurateLainnya').show();
	          }else{
	          	$('#fr-accurateLainnya').hide();
	          };
	      }
	  }
	}

	</script>
</body>
</html>
