
<?php session_start() ?>
<?php include_once '../baseurl.php'; ?>
<?php
if (!isset($_SESSION['fac_reg_token'])||empty($_SESSION['fac_reg_token'])) {
  $waktu = date('Y-m-d.H:i');
  $_SESSION['fac_reg_token'] = md5($_SERVER['REMOTE_ADDR'].$waktu);
}
// echo $_SESSION['fac_reg_token'];
?>
<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <meta name="google-site-verification" content="EfNVtXecxElbcbugY59SeKXJuMzO67bjVvTvlHCc_CM" />
    <meta name="msvalidate.01" content="D925497D8F382164FF0CE0267D381FDA" />
    <meta name="thumbnailUrl" content="https://fac-institute.com/_caramel/assets/img/g44508.png" />
    <meta name="description" content="FAC - Kami Melayani Training Software Akuntansi ACCURATE dan Accounting Service Untuk Perusahaan Anda" />
    <meta content="First Asian Consulting. Konsultan Akuntansi & Lembaga Pendidikan Komputerisasi Akuntansi" />
    <meta name="keywords" content="Training,Akuntansi,ACCURATE,Jasa">
    <meta name="author" content="Rifzky Alam, Dino Damara">
    <title>FAC Institute</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet">

    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.8/css/mdb.min.css" rel="stylesheet"> -->



<link href="https://fac-institute.com/daftar-training/dist/css/smart_wizard.min.css" rel="stylesheet" type="text/css" />
<link href="https://fac-institute.com/daftar-training/dist/css/smart_wizard_theme_circles.css" rel="stylesheet" type="text/css" />
<link href="https://fac-institute.com/daftar-training/dist/css/smart_wizard_theme_arrows.css" rel="stylesheet" type="text/css" />
<link href="https://fac-institute.com/daftar-training/dist/css/smart_wizard_theme_dots.css" rel="stylesheet" type="text/css" />


<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/maps/easy-autocomplete.min.css.map">
<link href="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/maps/easy-autocomplete.themes.min.css.map">
<link href="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/easy-autocomplete.min.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/easy-autocomplete.themes.min.css" rel="stylesheet"> -->

<link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,100,300,500,600,700c" type="text/css" />
<style>
.navbar {
  font-family: "Raleway", sans-serif;
}
body, html, h3 {
    font-family: 'PT Sans', sans-serif;
}
.navbar-expand-lg .navbar-nav .nav-link {
   padding-right: 1em;
}
</style>

</head>

<body>

  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCUn6Q6sg0JVJBlgiz4tL2kF1zmVoC0ScE&libraries=places&sensor=true"></script>
  <script type="text/javascript">
      var latitude;
      var longitude;

      function getLocation() {
          if (navigator.geolocation) {
              navigator.geolocation.getCurrentPosition(showPosition);
          } else {
              alert("Geolocation is not supported by this browser.");
          }
      }

      function showPosition(position) {
          var myCurrentLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
          return myCurrentLocation;
      }


      function showError(error) {
          switch (error.code) {
              case error.PERMISSION_DENIED:
                  var myCurrentLocation = new google.maps.LatLng(-6.248123, 106.907952);
                  var mapProp = {
                      center: myCurrentLocation,
                      zoom: 11,
                      mapTypeId: google.maps.MapTypeId.ROADMAP
                  };

                  var input = document.getElementById('my_maps');
                  input.value = "";
                  var sBox = new google.maps.places.SearchBox(input);
                  var myMarker = new google.maps.Marker({
                      position: myCurrentLocation,
                  });

                  var markers = [];
                  markers.push(myMarker);

                  var map = new google.maps.Map(document.getElementById("map-canvas"), mapProp);
                  var infowindow = new google.maps.InfoWindow({
                      content: "FAC Institute"
                  });
                  infowindow.open(map, myMarker);
                  myMarker.setMap(map);

                  sBox.addListener('places_changed', function() {
                      var places = sBox.getPlaces();
                      if (places.length == 0) {
                          return;
                      };
                      deleteMarkers();
                      var bounds = new google.maps.LatLngBounds();
                      places.forEach(function(place) {
                          var icon = {
                              url: place.icon,
                              size: new google.maps.Size(71, 71),
                              origin: new google.maps.Point(0, 0),
                              anchor: new google.maps.Point(17, 34),
                              scaledSize: new google.maps.Size(25, 25)
                          };
                          markers.push(new google.maps.Marker({
                              map: map,
                              //icon: icon,
                              title: place.name,
                              position: place.geometry.location
                          }));

                          if (place.geometry.viewport) {
                              bounds.union(place.geometry.viewport);
                          } else {
                              bounds.extend(place.geometry.location);
                          }
                      });
                      map.fitBounds(bounds);
                  });

                  google.maps.event.addListener(map, 'click', function(event) {
                      placeMarker(event.latLng);
                  });

                  function setMapOnAll(map) {
                      for (var i = 0; i < markers.length; i++) {
                          markers[i].setMap(map);
                      }
                  }

                  function placeMarker(location) {
                      deleteMarkers();
                      var my_maps = document.getElementById('my_maps');
                      var myMarker = new google.maps.Marker({
                          position: location,
                          map: map
                      });
                      my_maps.value = location;
                      var myString = my_maps.value;
                      myString = myString.replace('(', '');
                      myString = myString.replace(')', '');
                      myString = myString.replace(', ', ',');
                      my_maps.value = myString;
                      markers.push(myMarker);
                  }

                  function deleteMarkers() {
                      clearMarkers();
                      markers = [];
                  }

                  function clearMarkers() {
                      setMapOnAll(null);
                  }

                  break;
              case error.POSITION_UNAVAILABLE:
                  alert('Location information is unavailable.');
                  break;
              case error.TIMEOUT:
                  alert('The request to get user location timed out.');
                  break;
              case error.UNKNOWN_ERROR:
                  alert('An unknown error occurred.');
                  break;
          }
      }

      function initialize() {
          if (navigator.geolocation) {
              navigator.geolocation.getCurrentPosition(function(position) {
                  var myCurrentLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                  var mapProp = {
                      center: myCurrentLocation,
                      zoom: 16,
                      mapTypeId: google.maps.MapTypeId.ROADMAP
                  };

                  var input = document.getElementById('my_maps');
                  input.value = position.coords.latitude + ',' + position.coords.longitude;
                  var sBox = new google.maps.places.SearchBox(input);
                  var myMarker = new google.maps.Marker({
                      position: myCurrentLocation,
                  });

                  var markers = [];
                  markers.push(myMarker);
                  var map = new google.maps.Map(document.getElementById("map-canvas"), mapProp);
                  var infowindow = new google.maps.InfoWindow({
                      content: "Anda berada disini"
                  });
                  infowindow.open(map, myMarker);
                  myMarker.setMap(map);

                  sBox.addListener('places_changed', function() {
                      var places = sBox.getPlaces();
                      if (places.length == 0) {
                          return;
                      }
                      deleteMarkers()

                      var bounds = new google.maps.LatLngBounds();
                      places.forEach(function(place) {
                          var icon = {
                              url: "",
                              size: new google.maps.Size(71, 71),
                              origin: new google.maps.Point(0, 0),
                              anchor: new google.maps.Point(17, 34),
                              scaledSize: new google.maps.Size(25, 25)
                          };

                          markers.push(new google.maps.Marker({
                              map: map,
                              // icon: icon,
                              title: place.name,
                              position: place.geometry.location
                          }));

                          if (place.geometry.viewport) {
                              bounds.union(place.geometry.viewport);
                          } else {
                              bounds.extend(place.geometry.location);
                          }
                      });
                      map.fitBounds(bounds);

                  });

                  google.maps.event.addListener(map, 'click', function(event) {
                      placeMarker(event.latLng);
                  });

                  function setMapOnAll(map) {
                      for (var i = 0; i < markers.length; i++) {
                          markers[i].setMap(map);
                      }
                  }

                  function placeMarker(location) {
                      deleteMarkers();
                      var my_maps = document.getElementById('my_maps');
                      var myMarker = new google.maps.Marker({
                          position: location,
                          map: map
                      });
                      my_maps.value = location;
                      var myString = my_maps.value;
                      myString = myString.replace('(', '');
                      myString = myString.replace(')', '');
                      myString = myString.replace(', ', ',');

                      my_maps.value = myString;
                      markers.push(myMarker);
                  }

                  function deleteMarkers() {
                      clearMarkers();
                      markers = [];
                  }

                  function clearMarkers() {
                      setMapOnAll(null);
                  }

              }, showError);

          } else {
              alert("Geolocation is not supported by this browser.");
          }
      }
      google.maps.event.addDomListener(window, 'load', initialize);
  </script>


    <!-- <nav class="mb-1 navbar navbar-expand-lg navbar-dark bg-primary">
        <div class="container">
            <a class="navbar-brand" href="#"><strong>MDB</strong></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7" aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Link</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Profile</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-7" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown </a>
                        <div class="dropdown-menu dropdown-pink" aria-labelledby="navbarDropdownMenuLink-7">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li>
                </ul>
                <form class="form-inline">
                    <div class="md-form my-0">
                        <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
                    </div>
                </form>
            </div>
        </div>
    </nav> -->

    <!-- Just an image -->

<nav class="navbar navbar-expand-lg navbar-dark" style="background-color: #3f51b5;">
  <a class="navbar-brand" href="#">
    <img src="https://fac-institute.com/_caramel/assets/img/logo.png" width="30" height="30" alt="">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo getBaseUrl() ?>"onclick="w3_close()">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo getBaseUrl() ?>training.php">Training</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo getBaseUrl() ?>kursus-akuntansi/">Kursus</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo getBaseUrl() ?>artikel/">Artikel</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo getBaseUrl() ?>tutorial/">Tutorial</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href=<?php echo "'".getBaseUrl()."login"."'"; ?>>Login</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo getBaseUrl() ?>download_new.php" >Download</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo getBaseUrl() ?>facteam.php" >FAC Team</a>
      </li>
    </ul>
  </div>
</nav>


    <div class="container">
            <br />
            <form action="#" id="myForm" role="form" data-toggle="validator" method="post" accept-charset="utf-8">

            <!-- SmartWizard html -->
            <div id="smartwizard">
                <ul>
                    <li><a href="#step-1">Step 1<br /><small>Data Customer</small></a></li>
                    <li><a href="#step-2">Step 2<br /><small>Data Training</small></a></li>
                    <li><a href="#step-3">Step 3<br /><small>Data Kebutuhan Training</small></a></li>
                </ul>

                <div>
                    <div class="p-4" id="step-1">
                        <div id="form-step-0" role="form" data-toggle="validator">
                            <div class="form-group">
                            	<label class="control-label font-weight-bold">Nama Customer</label>
                            	<input  maxlength="100" type="text" required="required" name="data[namaUsaha]" class="form-control" placeholder="Nama Customer" required>
                              <small class="form-text text-muted help-block with-errors"></small>
                            </div>
                            <div class="form-group">
                              <label class="font-weight-bold" for="email">Email Customer</label>
                              <input type="email" name="data[email]" id="email" class="form-control" placeholder="Tidak Wajib">
                              <small class="form-text text-muted help-block with-errors"></small>
                            </div>
                            <div class="form-group">
                            	<label class="control-label font-weight-bold">Alamat Customer</label>
                            	<textarea class="form-control" name="data[alamatUsaha]" required="required" id="alamatCustomer" placeholder="(Berdasarkan lokasi diadakan training) Boleh di isi dengan alamat google map"></textarea>
                              <small class="form-text text-muted help-block with-errors"></small>
                            </div>
                            <div class="form-group">
                            	<label class="font-weight-bold">Telepon Customer</label>
                            	<input type="tel" name="data[telepon]" class="form-control" placeholder="Tidak Wajib">
                              <small class="form-text text-muted help-block with-errors"></small>
                            </div>
                            <div class="form-group">
                              <label class="font-weight-bold" for="provinsi">Provinsi</label>
                              <select class="form-control" id="provinsi">
                              </select>
                            </div>
                            <div class="form-group">
                              <label class="font-weight-bold" for="kota">Kota/Kabupaten</label>
                              <select class="form-control" id="kota">
                              </select>
                            </div>
                            <div class="form-group">
                              <label class="font-weight-bold" for="kec">Kecamatan</label>
                              <select class="form-control" id="kec">
                              </select>
                            </div>
                            <fieldset class="form-group">
                                <div class="row">
                                  <legend class="col-form-label col-sm-2 pt-0 font-weight-bold">Referensi</legend>
                                  <div class="col-sm-10">
                                    <div class="form-check">
                                      <input class="form-check-input" id="marketingW" type="radio" name="data[ref]" value="website" checked>
                                      <label class="form-check-label">
                                        Website (www.fac-institute.com) / Sosial Media
                                      </label>
                                    </div>
                                    <div class="form-check">
                                      <input class="form-check-input" id="marketingCR" type="radio" name="data[ref]" value="marketing">
                                      <label class="form-check-label">
                                        Marketing
                                      </label>
                                    </div>
                                  </div>
                                </div>
                              </fieldset>
                              <div class="form-group col-sm-8">
                                <label class="font-weight-bold" for="markCB">Cabang</label>
                                <select class="form-control" id="markCB"></select>
                              </div>
                              <div class="form-group col-sm-8">
                              	<label class="control-label font-weight-bold">Nama Marketing</label>
                              	<input type="text" name="data[namaMark]" id="markN" class="form-control" placeholder="Nama Marketing">
                                <small class="form-text text-muted help-block with-errors"></small>
                              </div>
                            <fieldset class="form-group">
                                <div class="row">
                                  <legend class="col-form-label col-sm-2 pt-0 font-weight-bold">Jenis Usaha</legend>
                                  <div class="col-sm-10">
                                    <div class="form-check">
                                      <input class="form-check-input" type="radio" name="data[jnsUsaha]" value="perdagangan" checked>
                                      <label class="form-check-label">
                                        Perdagangan
                                      </label>
                                    </div>
                                    <div class="form-check">
                                      <input class="form-check-input" type="radio" name="data[jnsUsaha]" value="jasa">
                                      <label class="form-check-label">
                                        Jasa
                                      </label>
                                    </div>
                                  <div class="form-check">
                                      <input class="form-check-input" type="radio" name="data[jnsUsaha]" value="konstruksi">
                                      <label class="form-check-label">
                                        Konstruksi
                                      </label>
                                    </div>
                                    <div class="form-check">
                                      <input class="form-check-input" type="radio" name="data[jnsUsaha]" value="pabrikasi">
                                      <label class="form-check-label">
                                        Pabrikasi
                                      </label>
                                    </div>
                                  </div>
                                </div>
                              </fieldset>
                            <div class="form-group">
                            	<label class="control-label font-weight-bold">Deskripsi Usaha</label>
                            	<textarea class="form-control" name="data[deskripsi]" required="required" id="alamatCustomer" placeholder="Deskripsikan usaha anda"></textarea>
                              <small class="form-text text-muted help-block with-errors"></small>
                            </div>

                            <div class="form-group">
                              <label for="alamat" class="font-weight-bold">Lokasi Map</label>
                              <input type="text" name="data[map]" id="my_maps" class="form-control" placeholder="Cari Nama Tempat atau Tandai Peta di Bawah ini">
                            </div>
                            <div class="row" style="margin-bottom:20px">
                                <div class="col-md-12">
                                    <div id="map-canvas" style="width:100%;height:400px;border:0px solid"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="p-4" id="step-2">
                        <div id="form-step-1" role="form" data-toggle="validator">

                          <div class="form-group">
                            <label class="font-weight-bold" for="custName">Nama Anda</label>
                            <input type="text" name="data[custName]" id="custName" class="form-control" placeholder="Nama Anda">
                            <small class="form-text text-muted help-block with-errors"></small>
                          </div>

                          <div class="form-group">
                            <label class="font-weight-bold" for="custTelp">No. Telp Anda</label>
                            <input type="tel" name="data[custTelp]" id="custTelp" class="form-control" placeholder="No. Telp Anda">
                            <small class="form-text text-muted help-block with-errors"></small>
                          </div>

                          <div class="form-group">
                            <label class="font-weight-bold" for="custEmail">Email Anda</label>
                            <input type="email" name="data[custEmail]" id="custEmail" class="form-control" placeholder="Email Anda">
                            <small class="form-text text-muted help-block with-errors"></small>
                          </div>

                          <div class="form-group">
                            <label class="font-weight-bold" for="custJbt">Jabatan Anda</label>
                            <input type="text" name="data[custJbt]" id="custJbt" class="form-control" placeholder="Jabatan Anda">
                            <small class="form-text text-muted help-block with-errors"></small>
                          </div>


                            <!-- <div class="form-group row">
                                <div class="col-sm-2 font-weight-bold">Versi Accurate</div>
                                <div class="col-sm-10">
                                  <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="data[jnsAccurate][]" value="accurate 4 standard edition"><label class="form-check-label">ACCURATE 4 Standar Edition</label>
                                  </div>
                                  <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="data[jnsAccurate][]" value="accurate 4 deluxe edition"><label class="form-check-label">ACCURATE 4 Deluxe Edition</label>
                                  </div>
                                  <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="data[jnsAccurate][]" value="accurate 4 enterprise edition"><label class="form-check-label">ACCURATE 4 Enterprise Edition</label>
                                  </div>
                                  <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="data[jnsAccurate][]" value="accurate 5 standard edition"><label class="form-check-label">ACCURATE 5 Standar Edition</label>
                                  </div>
                                  <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="data[jnsAccurate][]" value="accurate 5 deluxe edition"><label class="form-check-label">ACCURATE 5 Deluxe Edition</label>
                                  </div>
                                  <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="data[jnsAccurate][]" value="accurate 5 enterprise edition"><label class="form-check-label">ACCURATE 5 Enterprise Edition</label>
                                  </div>
                                  <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="data[jnsAccurate][]" value="accurate cloud"><label class="form-check-label">ACCURATE Online/Cloud</label>
                                  </div>
                                  <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="data[jnsAccurate][]" value="rene point of sales"><label class="form-check-label">RENE Point Of Sales</label>
                                  </div>
                                  <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="data[jnsAccurate][]" value="rene dan accurate"><label class="form-check-label">Gabungan RENE dan ACCURATE</label>
                                  </div>
                                  <div class="form-inline"><input type="text" class="form-control" name="data[jnsAccurate][]" id="jns-accurateLainnya" placeholder='Lainnya...'></div>
                                </div>
                              </div> -->

                        </div>
                    </div>

                    <div id="step-3">
                        <h2>Your Address</h2>

                        <div class="form-group">
                            <label for="nitem" class="font-weight-bold">Nama Paket</label>
                            <select class="form-control" id="nitem" name="data[paket]" required>
                              <option value='web-os'>Onsite Standard (OS) - Harga: Rp 750,000</option>
                              <option value='web-oe'>Onsite Expert (OE) - Harga: Rp 1,000,000</option>
                              <option value='web-aol1'>Accurate Online 1 (AOL 1) - Harga: Rp 1,250,000</option>
                              <option value='web-ps'>Paket Standard (PS) - Harga: Rp 3,500,000</option>
                              <option value='web-aol2'>Accurate Online 2 (AOL 2) - Harga: Rp 6,000,000</option>
                              <option value='web-pe1'>Paket Expert 1 (PE 1) - Harga: Rp 9,500,000</option>
                              <option value='web-pe2'>Paket Expert 2 (PE 2) - Harga: Rp 18,000,000</option>
                              <div class="help-block with-errors"></div>
                            </select>
                            <small class="form-text text-muted help-block with-errors"></small>
                          </div>

                          <div class="form-group">
                            <label for="qty" class="font-weight-bold">Agenda Training/Kebutuhan Anda</label>
                            <textarea class="form-control" name="data[qty]" id="agendaTraining" placeholder="Tuliskan apa kebutuhan anda di dalam software Accurate atau jasa Accounting Service Contoh: setup database, penjelasan fitur accurate, entry data" ></textarea>
                          </div>

                        <!-- <fieldset class="form-group">
                            <div class="row">
                              <legend class="col-form-label col-sm-2 pt-0 font-weight-bold">Jenis Pengguna</legend>
                              <div class="col-sm-10">
                                <div class="form-check">
                                  <input class="form-check-input" type="radio" name="data[jnsPengguna]" value="baru" checked>
                                  <label class="form-check-label">
                                    Baru
                                  </label>
                                </div>
                                <div class="form-check">
                                  <input class="form-check-input" type="radio" name="data[jnsPengguna]" value="lama">
                                  <label class="form-check-label">
                                    Lama
                                  </label>
                                </div>
                              </div>
                            </div>
                          </fieldset> -->

                    </div>
                    <div id="step-4" class="">


                    </div>
                </div>
            </div>

            </form>

        </div>



    <!-- JQuery -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/umd/popper.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.8/js/mdb.min.js"></script> -->
    <script type="text/javascript" src="https://fac-institute.com/daftar-training/dist/js/jquery.smartWizard.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
    <!-- <script src="https://fac-institute.com/administrasi/view/quotation/dist/jquery.easy-autocomplete.min.js"></script> -->
    <script type="text/javascript">
    $(document).ready(function() {

        // Toolbar extra buttons
        var btnFinish = $('<button></button>').text('Finish')
            .addClass('btn btn-info')
            .on('click', function() {
                if (!$(this).hasClass('disabled')) {
                    var elmForm = $("#myForm");
                    if (elmForm) {
                        elmForm.validator('validate');
                        var elmErr = elmForm.find('.has-error');
                        if (elmErr && elmErr.length > 0) {
                            alert('Oops we still have error in the form');
                            return false;
                        } else {
                            alert('Great! we are ready to submit form');
                            elmForm.submit();
                            return false;
                        }
                    }
                }
            });

        var btnCancel = $('<button></button>').text('Cancel')
            .addClass('btn btn-danger')
            .on('click', function() {
                $('#smartwizard').smartWizard("reset");
                $('#myForm').find("input, textarea").val("");
            });



        // Smart Wizard
        $('#smartwizard').smartWizard({
            selected: 0,
            theme: 'default',
            transitionEffect: 'fade',
            toolbarSettings: {
                toolbarPosition: 'bottom',
                toolbarExtraButtons: [btnFinish, btnCancel]
            },
            anchorSettings: {
                markDoneStep: true, // add done css
                markAllPreviousStepsAsDone: true, // When a step selected by url hash, all previous steps are marked done
                removeDoneStepOnNavigateBack: true, // While navigate back done step after active step will be cleared
                enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
            }
        });

        $("#smartwizard").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) {
            var elmForm = $("#form-step-" + stepNumber);
            // stepDirection === 'forward' :- this condition allows to do the form validation
            // only on forward navigation, that makes easy navigation on backwards still do the validation when going next
            if (stepDirection === 'forward' && elmForm) {
                elmForm.validator('validate');
                var elmErr = elmForm.children('.has-error');
                if (elmErr && elmErr.length > 0) {
                    // Form validation failed
                    return false;
                }
            }
            return true;
        });

        $("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection) {
            // Enable finish button only on last step
            if (stepNumber == 3) {
                $('.btn-finish').removeClass('disabled');
            } else {
                $('.btn-finish').addClass('disabled');
            }
        });


        // if ($('#marketingW').is(':checked')) { $('#markCB').prop('disabled', true)
        // $("#markN").prop('disabled', true); }

        if ($('#marketingW').is(':checked')) {
            $('#markCB').prop('disabled', true);
            $("#markN").prop('disabled', true);
        }

        $('#marketingCR').change(
            function() {
                if (this.checked) {
                    $('#markCB').prop('disabled', false);
                    $("#markN").prop('disabled', false);
                }
            });

        $('#marketingW').change(
            function() {
                if (this.checked) {
                    $('#markCB').prop('disabled', true);
                    $("#markN").prop('disabled', true);
                }
            });

    });


    $.ajax({
        type: "GET",
        url: "https://fac-institute.com/api/orsvc?req=cabanglist",
        dataType: 'json',
        cache: false,
        success: function(data) {
            console.log(data);
            for (i = 0; i < data.length; i++) {
                document.getElementById("markCB").innerHTML += "<option value='" + data[i].id_cabang + "'>" + data[i].nama + "</option>";
            }
            $('#markCB').val("1");
        }
    });


    $.ajax({
        type: "GET",
        url: "https://fac-institute.com/administrasi/wilayah/provinsi/",
        dataType: 'json',
        cache: false,
        success: function(data) {
            for (i = 0; i < data.rajaongkir.results.length; i++) {
                document.getElementById("provinsi").innerHTML += "<option value='" + data.rajaongkir.results[i].province_id + "'>" + data.rajaongkir.results[i].province + "</option>";
            }
            $('#provinsi').val("6");
            chp();
        }
    });

    $("#provinsi").change(function() {
        chp();
    });

    $("#kota").change(function() {
        chk();
    });



    function chp() {
        $.ajax({
            type: "GET",
            url: "https://fac-institute.com/administrasi/wilayah/kota/" + $("#provinsi").val(),
            dataType: 'json',
            cache: false,
            success: function(data) {
                document.getElementById("kota").innerHTML = "";
                for (i = 0; i < data.rajaongkir.results.length; i++) {
                    document.getElementById("kota").innerHTML += "<option value='" + data.rajaongkir.results[i].city_id + "'>" + data.rajaongkir.results[i].city_name + "</option>";
                }
                chk();
            }
        });
    }

    function chk() {
        $.ajax({
            type: "GET",
            url: "https://fac-institute.com/administrasi/wilayah/subdist/" + $("#kota").val(),
            dataType: 'json',
            cache: false,
            success: function(data) {
                document.getElementById("kec").innerHTML = "";
                for (i = 0; i < data.rajaongkir.results.length; i++) {
                    document.getElementById("kec").innerHTML += "<option value='" + data.rajaongkir.results[i].subdistrict_id + "'>" + data.rajaongkir.results[i].subdistrict_name + "</option>";
                }
            }
        });
    }



    $.ajax({
        type: "GET",
        url: "https://fac-institute.com/api/orsvc?req=hasilinputregusaha/",
        // dataType: 'json',
        cache: false,
        success: function(data) {
            console.log(data);
            for (i = 0; i < data.length; i++) {}
        }
    });

    $.ajax({
        type: "GET",
        url: "https://fac-institute.com/api/orsvc?req=hasilinputregcust/",
        dataType: 'json',
        cache: false,
        success: function(data) {
            console.log(data);
            for (i = 0; i < data.length; i++) {}
        }
    });

    console.log("fuck");

        // var option1 = {
        //     url: "https://fac-institute.com/administrasi/wilayah/provinsi/",
        //     list: {
        //         match: {enabled: true},
        //         showAnimation: {type: "fade",time: 200,callback: function() {}},
        //         hideAnimation: {type: "slide",time: 400,callback: function() {}},
        //         maxNumberOfElements: 99,
        //         sort: {
        //             enabled: true
        //         }
        //     }
        //   };
        //
        //   var option2 = {
        //       url: "https://fac-institute.com/administrasi/wilayah/kota/",
        //       list: {
        //           match: {enabled: true},
        //           showAnimation: {type: "fade",time: 200,callback: function() {}},
        //           hideAnimation: {type: "slide",time: 400,callback: function() {}},
        //           maxNumberOfElements: 99,
        //           sort: {
        //               enabled: true
        //           }
        //       }
        //     };


          // $("#kota").easyAutocomplete(option1);
          // $("#provinsi").easyAutocomplete(option1);

    </script>
</body>

</html>
