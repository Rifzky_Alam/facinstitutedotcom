<!DOCTYPE html>
<html>
<head>
	<title>Registrasi Training Accurate</title>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container" style="padding-bottom:40px;">
	<div class="row" style="padding-top:20px;">
		<div class="col-md-12">
			<div class="page-header">
				<h1>Form Registrasi Training Software Accurate Step-2</h1>
			</div>
		</div>
	</div>
	<form action="" method="post" accept-charset="utf-8">
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<label for="nama">Nama Anda</label>
				<input type="text" name="in[nama]" class="form-control" id="nama" placeholder="Nama Anda, Wajib disi" required>
                <input type="hidden" name="in[usaha]" value="<?= $data->idusaha ?>">
			</div>
			<div class="form-group">
				<label for="email">Email Anda</label>
				<input type="email" name="in[email]" class="form-control" id="email" placeholder="Email aktif, Wajib diisi." required>
			</div>
			<div class="form-group">
				<label for="telpusaha">Telepon Anda</label>
				<input type="text" name="in[telepon]" class="form-control" id="telpusaha" placeholder="Telepon Aktif, Wajib Diisi." required>
			</div>
			<div class="form-group">
				<label for="jabatan">Jabatan</label>
				<input id="jabatan" type="text" name="in[jabatan]" class="form-control" placeholder="Jabatan di perusahaan anda"/>
			</div>
			<div class="form-group">
				<label for="marketing">Marketing</label>
				<input type="text" id="marketing" name="in[marketing]" class="form-control" placeholder="Dari siapa anda tahu fac-institute.com">
			</div>
            <div class="form-group">
                <label for="agenda">Agenda Training/Kebutuhan Anda</label>
                <textarea name="in[agenda]" class="form-control" placeholder="Tuliskan apa kebutuhan anda di dalam software Accurate atau jasa Accounting Service Contoh: setup database, penjelasan fitur accurate, entry data" required></textarea>
            </div>
            <div class="form-group">
                <label for="kota">Pengguna</label>
                <div class="radio">
                    <label><input type="radio" name="in[pengguna]" value="0" required>Lama</label>
                </div>
                <div class="radio">
                    <label><input type="radio" name="in[pengguna]" value="1" checked>Baru</label>
                </div>
            </div>

			<button class="btn btn-lg btn-primary" style="width:100%;">Submit</button>
		</div>
	</div>
	</form>

</div><!-- end container -->
</body>
</html>