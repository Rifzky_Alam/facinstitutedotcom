<!DOCTYPE html>
<html>
<head>
	<title>Registrasi Training Accurate</title>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCUn6Q6sg0JVJBlgiz4tL2kF1zmVoC0ScE&libraries=places&sensor=true"></script>
<?php $this->JustInclude('/administrasi/map-script'); ?>
<div class="container h-100">
	<div class="row" style="padding-top:20px;">
		<div class="col-md-12">
			<div class="page-header">
				<h1>Form Registrasi Training Software Accurate Step-1</h1>
			</div>
		</div>
	</div>
	<form action="" method="post" accept-charset="utf-8">
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<label for="namausaha">Nama Perusahaan</label>
				<input type="text" name="in[namausaha]" class="form-control" id="namausaha" placeholder="Nama Usaha Anda" required>
			</div>
			<div class="form-group">
				<label for="emailusaha">Email Perusahaan</label>
				<input type="email" name="in[email]" class="form-control" id="emailusaha" placeholder="Email aktif, biarkan kosong bila tidak ada.">
			</div>
			<div class="form-group">
				<label for="telpusaha">Telepon Perusahaan</label>
				<input type="text" name="in[telepon]" class="form-control" id="telpusaha" placeholder="Telepon Aktif, kosongkan bila tidak ada.">
			</div>
			<div class="form-group">
				<label for="alamatusaha">Alamat Perusahaan</label>
				<textarea id="alamatusaha" name="in[alamat]" class="form-control" placeholder="Wajib diisi." required></textarea>
			</div>
			<div class="form-group">
				<label for="kota">Kota</label>
				<input type="text" id="kota" name="in[kota]" class="form-control" placeholder="Wajib diisi." required>
			</div>
			<div class="form-group">
				<label for="kota">Provinsi</label>
				<select name="in[provinsi]" class="form-control">
					<option value="" disabled>-- Pilih Provinsi --</option>
					<option value='aceh'>Nanggroe Aceh Darussalam</option>
                    <option value='sumatera utara'>Sumatera Utara</option>
                    <option value='riau'>Riau</option>
                    <option value='kepulauan riau'>Kepulauan Riau</option>
                    <option value='sumatera barat'>Sumatera Barat</option>
                    <option value='jambi'>Jambi</option>
                    <option value='bengkulu'>Bengkulu</option>
                    <option value='bangka belitung'>Bangka Belitung</option>
                    <option value='sumatera selatan'>Sumatera Selatan</option>
                    <option value='lampung'>Lampung</option>

                    <option value='banten'>Banten</option>
                    <option value='jawa barat'>Jawa Barat</option>
                    <option value='jakarta' selected>Jakarta</option>
                    <option value='jawa tengah'>Jawa tengah</option>
                    <option value='jogjakarta'>Jogjakarta</option>
                    <option value='jawa timur'>Jawa Timur</option>

                    <option value='bali'>Bali</option>
                    <option value='ntb'>Nusa tenggara Barat</option>
                                                                
                    <option value='kalbar'>Kalimantan Barat</option>
                    <option value='kaltim'>Kalimantan Timur</option>
                    <option value='kalteng'>Kalimantan Tengah</option>
                    <option value='kalsel'>Kalimantan Selatan</option>
                    <option value='kalut'>Kalimantan Utara</option>

                    <option value='sulbar'>Sulawesi Barat</option>
                    <option value='sulsel'>Sulawesi Selatan</option>
                    <option value='sulteng'>Sulawesi Tengah</option>
                    <option value='sultra'>Sulawesi Tenggara</option>
                    <option value='gorontalo'>Gorontalo</option>
                    <option value='sulut'>Sulawesi Utara</option>

                    <option value='maluku utara'>Maluku Utara</option>
                    <option value='maluku'>Maluku</option>

                    <option value='papua barat'>Papua Barat</option>
                    <option value='papua'>Papua</option>
				</select>
			</div>
			<div class="form-group">
                <h4>Jenis Usaha</h4>
                <div class="checkbox">
            		<label><input name="in[jnsUsaha][]" value="1" type="checkbox">Perdagangan</label>
        		</div>
            	<div class="checkbox">
            		<label><input name="in[jnsUsaha][]" value="2" type="checkbox">Jasa</label>
        		</div>
            	<div class="checkbox">
            		<label><input name="in[jnsUsaha][]" value="3" type="checkbox">Konstruksi</label>
        		</div>
            	<div class="checkbox">
            		<label><input name="in[jnsUsaha][]" value="4" type="checkbox">Pabrikasi</label>
        		</div>	                           
            </div>

            <div class='form-group'>
                <label>Keterangan Jenis Usaha</label>
            	<textarea id="ketJenisUsaha" name="in[ketJenisUsaha]" class="form-control" placeholder="Misal: Perusahaan saya bergerak di bidang konstruksi bangunan"></textarea>
            </div>

            <div class='form-group'>
                <h4>Versi Accurate</h4>
                <div class="checkbox">
        			<label>
            			<input type="checkbox" name="in[jnsAccurate][]" value="1">
            			Accurate 4 Standard Edition
            		</label>
    			</div>
        		<div class="checkbox">
        			<label>
            			<input type="checkbox" name="in[jnsAccurate][]" value="10">
            			Accurate 4 Deluxe Dengan RAB
            		</label>
    			</div>
        		<div class="checkbox">
        			<label>
            			<input type="checkbox" name="in[jnsAccurate][]" value="11">
            			Accurate 5 Deluxe Dengan RAB
            		</label>
    			</div>
        		<div class="checkbox">
        			<label>
            			<input type="checkbox" name="in[jnsAccurate][]" value="2">
            			ACCURATE 4 Deluxe Edition
            		</label>
    			</div>
        		<div class="checkbox">
        			<label>
            			<input type="checkbox" name="in[jnsAccurate][]" value="3">
            			ACCURATE 4 Enterprise Edition
            		</label>
    			</div>
        		<div class="checkbox">
        			<label>
            			<input type="checkbox" name="in[jnsAccurate][]" value="4">
            			ACCURATE 5 Standar Edition
            		</label>
    			</div>
        		<div class="checkbox">
        			<label>
            			<input type="checkbox" name="in[jnsAccurate][]" value="5">
            			ACCURATE 5 Deluxe Edition
            		</label>
    			</div>
        		<div class="checkbox">
        			<label>
            			<input type="checkbox" name="in[jnsAccurate][]" value="6">
            			ACCURATE 5 Enterprise Edition
            		</label>
    			</div>
        		<div class="checkbox">
        			<label>
            			<input type="checkbox" name="in[jnsAccurate][]" value="7">
            			ACCURATE Online/Cloud
            		</label>
    			</div>
        		<div class="checkbox">
        			<label>
            			<input type="checkbox" name="in[jnsAccurate][]" value="8">
            			RENE Point Of Sales
            		</label>
    			</div>
        		<div class="checkbox">
        			<label>
            			<input type="checkbox" name="in[jnsAccurate][]" value="9">
            			Gabungan RENE dan ACCURATE
            		</label>
    			</div>
            </div>

            <div class="form-group">
                <label for="alamat">Lokasi Map</label>
            	<input type="text" name="in[map]" id="alamatz" class="form-control" placeholder="Cari Nama Tempat atau Tandai Peta di Bawah ini">                    
            </div>

            <div class="form-group">
            	<div id="map-canvas" style="width:100%;height:400px;border:1px solid"></div>
            </div>

			<button class="btn btn-lg btn-primary" style="width:100%;">Submit</button>
		</div>
	</div>
	</form>

</div><!-- end container -->
<script>
	$(document).ready(function(){
        $("form").bind("keypress", function (e) {
        if (e.keyCode == 13) {
            $("#btnSearch").attr('value');
            //add more buttons here
            return false;
        }
    	});
    });
</script>

</body>
</html>