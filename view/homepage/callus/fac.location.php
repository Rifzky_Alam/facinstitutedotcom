<!-- <?php include_once 'baseurl.php'; ?> -->
<html class="no-js" lang="id">
<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="<?= $data->base_url ?>images/homepage2019/elements/fav.png">
	<!-- Author Meta -->
	<meta name="author" content="Rifzky Alam, Dino Damara">
	<!-- Meta Description -->
	<meta name="description" content="<?= $data->pagedesc ?>">
	<!-- Meta Keyword -->
	<meta name="keywords" content="<?= $data->pagekeywords ?>">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title><?= $data->title ?></title>
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
		<!--
			CSS
			============================================= -->
    <link rel="shortcut icon" href='https://fac-institute.com/_caramel/assets/img/g25992_9_kUd_icon.ico'>
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/linearicons.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/bootstrap.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/magnific-popup.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/nice-select.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/animate.min.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/owl.carousel.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/main.css">
		<style media="screen">
			.banner-content h1 {
			font-size: 40px;
			}
			.nav-menu {
			padding-top: 15px;
			}
			.nav-menu a {
			padding: 1px 8px 1px 8px;
			font-size: 16px;
			font-weight: 500;
			}
			.nav-menu ul li a {
			font-size: 14px;
			}
			.menu-active {
			border-bottom: 5px solid #df003a;
			}
			.navbar {
			padding: 0.2rem 1rem;
			}

		</style>
		<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">


	</head>
	<body>



    <?php include_once $data->homedir.'view/homepage/header.homepage.php'; ?>

		<!-- <section class="mt-80" style='background: url("") ;background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:20px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
			<div class="container-fluid">
				<div class="row justify-content-center align-items-center">
					<div class="col-lg-12" style="max-width:1000px">

				</div>
			</div>
		</section> -->


<section class="testomial-area section-gap mt-40">
  <div class="container-fluid">
    <h2 class="text-center">Temukan Kami!</h2>
    <hr >
      <div class="row d-flex justify-content-center">
        <div class="col-sm-8">
          <iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDbCTacWoWsk9QoAD3DpYGS8j0n7wzdPfc&q=Bank+Muamalat+Capem+Duren+Sawit" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>

        <div class="col-sm-4" id="contact2">
            <h4 class="pt-2 mb-10">No. Telp</h4>
            <i class="fa fa-phone mb-10" style="color:#000"></i><a style="font-weight:bold;font-size:16px;text-decoration:none;" href="tel:+6281290083983"> 081290083983 (Siti Nurhasanah)</a><br>
            <i class="fa fa-phone" style="color:#000"></i><a style="font-weight:bold;font-size:16px;text-decoration:none" href="tel:+6281219115005">  081219115005 (Farida Zahra)</a><br>
            <h4 class="pt-2 mb-10">Alamat</h4>
            <i class="fa fa-map" style="color:#000"></i> Jl. Pahlawan Revolusi 10GG No.3 (Lantai 2), RT.9/RW.7, Pd. Bambu, Kec. Duren Sawit, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta Indonesia 13430<br>
						<h4 class="pt-2 mb-10">Jam Kerja</h4>
						<i class="fa fa-clock-o mb-10" style="color:#000"></i> Senin s/d Jumat (09.00–17.00) Sabtu (09.00–12.00) </br>
						<h4 class="pt-2 mb-10">Email</h4>
						<i class="fa fa-envelope mb-10" style="color:#000"></i> training@fac-institute.com</br>

					</div>
      </div>
  </div>
</section>




	  <?php include_once $data->homedir.'view/homepage/footer.homepage.php'; ?>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/jquery-2.2.4.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/bootstrap.min.js"></script>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/easing.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/hoverIntent.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/superfish.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.ajaxchimp.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.magnific-popup.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/owl.carousel.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.sticky.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.nice-select.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/parallax.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/mail-script.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/main.js"></script>
		<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

		<script>
		AOS.init();

		$(function () {
		  $('[data-toggle="popover"]').popover()
		})

			var owl = $('.owl-carousel');
			owl.owlCarousel({
			    items:5,
			    // loop:true,
			    margin:10,
			    autoplay:true,
			    // autoplayTimeout:4000,
			    autoplayHoverPause:true,
					responsiveClass:true,
					loop:true,
			responsive:{
			0:{
				items:2,
				nav:true
			},
			600:{
				items:3,
				nav:false
			},
			1000:{
				items:5,
				nav:false
			}
			}

			});

		</script>


	</body>
</html>
