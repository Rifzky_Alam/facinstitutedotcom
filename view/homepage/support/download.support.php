<!-- <?php include_once 'baseurl.php'; ?> -->
<html class="no-js" lang="id">
<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="<?= $data->base_url ?>images/homepage2019/elements/fav.png">
	<!-- Author Meta -->
	<meta name="author" content="Rifzky Alam, Dino Damara">
	<!-- Meta Description -->
	<meta name="description" content="<?= $data->pagedesc ?>">
	<!-- Meta Keyword -->
	<meta name="keywords" content="<?= $data->pagekeywords ?>">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title><?= $data->title ?></title>
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
		<!--
			CSS
			============================================= -->
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/linearicons.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/bootstrap.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/magnific-popup.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/nice-select.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/animate.min.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/owl.carousel.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/main.css">
		<style media="screen">
			.banner-content h1 {
			font-size: 40px;
			}
			.nav-menu {
			padding-top: 15px;
			}
			.nav-menu a {
			padding: 1px 8px 1px 8px;
			font-size: 16px;
			font-weight: 500;
			}
			.nav-menu ul li a {
			font-size: 14px;
			}
			.menu-active {
			border-bottom: 5px solid #df003a;
			}
			.navbar {
			padding: 0.2rem 1rem;
			}

		</style>
		<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />

	</head>
	<body>



    <?php include_once $data->homedir.'view/homepage/header.homepage.php'; ?>
		<section class="we-offer-area section-gap mb-40" id="offer">
			<div class="container" >
				<div class="row">
					<div class="col-lg-12">
					<h4 class="pb-30 mt-40">Download</h4>

						<div id="jstree">
							<ul>
								<li>Accurate
									<ul>
										<li>aol-private</li>
										<li><a href="#">v5</a>
											<ul>
												<li>deluxe
													<ul>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1fG9QmzSxBB3XoEXBaUEupEoUR6I7qL37'" href="#">5.0.19.1863-Setup-x86.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1-6EBOiYS5Xgb6BZQHS7YIO-LVg7fQ73k'" href="#">5.0.19.1863-Setup-x64.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1Lq_AVx5hpyBgOaMN0tixuEtLM2Ac5_0P'" href="#">5.0.18.1853-Setup-x86.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1Pvk3vORAGe-v-jqzm5avCiK85kV3zpnL'" href="#">5.0.18.1853-Setup-x64.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1yz5Qgp_BOmmalWRAIJcuM-tGQKMXRi9i'" href="#">5.0.17.1833-Setup-x86.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1VGt9gTya7NQMUrwtKfjxTYyLLwL89Daf'" href="#">5.0.17.1833-Setup-x64.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=15l1GrjhOTYoroSkouk5r4Tw9NjDYiHQD'" href="#">5.0.16.1807-Setup-x86.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1dzmk-SL97-7p4m1VhQyyhmolymkgDkoN'" href="#">5.0.16.1807-Setup-x64.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1WC2TQftNwSumycbs51la_l2BtP9l0iZp'" href="#">5.0.16.1806-Setup-x86.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1tE4FHpOgueWusaZyQ_0gLu3zGcicB1b8'" href="#">5.0.16.1806-Setup-x64.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1s96HC0_FWWiPoiKaidmmS-rcIA8Lt2y4'" href="#">5.0.15.1790-Setup-x86.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1QG5SkFy2tOlRGRvHRIKq-T83w0N2ILbi'" href="#">5.0.15.1790-Setup-x64.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1LJM4HlwBJsOAOJi-i9f10u1vOExbBNnt'" href="#">5.0.14.1772-Setup-x86.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1fL6BoW_PyypmQd5ECA8VLDOVv-HFNXaC'" href="#">5.0.14.1772-Setup-x64.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1UnIHkX4X8Wh_NXM0mC6Aj9HTRvSfpVOy'" href="#">5.0.13.1765-Setup-x86.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1jgK1muAEYjgOeiAxHj6T0_EGPG1NIjFB'" href="#">5.0.13.1765-Setup-x64.exe</a></li>
													</ul>
												</li>
												<li>enterprise
													<ul>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1d0NBHk7GVkdhC__-coO_7aDdZo6Gkt22'" href="#">5.0.19.1863-Setup-x86.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1L6h7eO7uT-OCYbk4xjlifXBNrVMsDN-8'" href="#">5.0.19.1863-Setup-x64.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1m47sNHa3_wOGP3QkJMc1Eo63jaKs4ZTy'" href="#">5.0.18.1853-Setup-x86.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1bpRrw0NGmue_SfBIwS6KR7uQoYTVUJUb'" href="#">5.0.18.1853-Setup-x64.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=17u8kjQcy-1cdtqrnpv7fVQ8D7BnUiQHw'" href="#">5.0.17.1833-Setup-x86.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1mpJWIFLB80p_6spHz1iBfW-XJIHu8muy'" href="#">5.0.17.1833-Setup-x64.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1nhmR531Krfgtf41kQ4208oGgwHdjYvjd'" href="#">5.0.16.1807-Setup-x86.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=17-Pb_FTyUhgBlSJbBNT6WjAHducHYluS'" href="#">5.0.16.1807-Setup-x64.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1mPrheMK3iuLmj8rxhztc84Ac6UtiwwwK'" href="#">5.0.16.1806-Setup-x86.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1o_8IIMvomg7fZX3H8v8v-h7CWf347tCg'" href="#">5.0.16.1806-Setup-x64.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1tINhLx_tS1Wjv_xuLNu1di2r03PSoqK-'" href="#">5.0.15.1790-Setup-x86.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1eTbluG5AIx-rCGDPFH2h1vZouw_pMEUI'" href="#">5.0.15.1790-Setup-x64.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1eF0JjoRJ6VOj9N9w1vZ8NRRDyrsCfr3t'" href="#">5.0.14.1772-Setup-x86.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=12azj0MgR7giJx1D7B-fHC49_sqqKs5UF'" href="#">5.0.14.1772-Setup-x64.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1wZEkCl4SHhjcybjR9pCboIZ9-0FqOeid'" href="#">5.0.13.1765-Setup-x86.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=18Hp7EDa2jvlSpnuuc-xig7vzU0bteMlZ'" href="#">5.0.13.1765-Setup-x64.exe</a></li>
													</ul>
												</li>
												<li>licensemanager
													<ul>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1rHQlIMLd_PJVmE5hPIzpWEZH-rrY7u-Z'" href="#">License Manager-1.0.0.197-Setup.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1Fs52SeJhz1Uk2Iy3ccLGenx8A2_hxNXx'" href="#">License Manager-1.0.0.219-Setup.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1vhqZ84rJrI8HmyNp8oYb-f4RYKJ0mCD0'" href="#">License Manager-1.0.0.288-Setup.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1VpfylN4-H4osiz7w9VmHAgTkh0iw1YZi'" href="#">License Manager-1.0.0.290-Setup.exe</a></li>
													</ul>
												</li>
												<li>standard
													<ul>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1yoJEKc4_4CdJcyw2uDadrUMBILEPurEA'" href="#">5.0.19.1863-Setup-x64.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1uiczE79-YGFHU6TS2ENBJDUCZNURJq2w'" href="#">5.0.19.1863-Setup-x86.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1-ha5R2FJ4v5iMYD_V0O0NhZMOSMgWfVP'" href="#">5.0.18.1853-Setup-x86.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1POw2ydQic21FgoXAkpyVUfbRtjhEWPO1'" href="#">5.0.18.1853-Setup-x64.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=15RkoGWP-41wb2cthKLUhsrptdr2WFy01'" href="#">5.0.17.1833-Setup-x86.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1Cs6mg-kcwzPKwMx3lsmPrMdUih9GTjvD'" href="#">5.0.17.1833-Setup-x64.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=13TQgpN180wcDv-6XEKJJuuNcYreIYFiC'" href="#">5.0.16.1807-Setup-x86.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1Mnw4PalxLyo0UUSYnqm7j6PS5HZxbRLs'" href="#">5.0.16.1807-Setup-x64.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1eLbOujTzBtAbX8WD0Pml7m7kfmKyzixc'" href="#">5.0.16.1806-Setup-x86.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1tZFdtNHoqxokXyLREId1vMJaQJrvpmWP'" href="#">5.0.16.1806-Setup-x64.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1mrBZVwmVDf2yGzwk7LnybkyJYkayUn_s'" href="#">5.0.15.1790-Setup-x86.exe</a></li>
														<li data-jstree='{"icon":"fa fa-file"}'><a onclick="window.location.href='https://drive.google.com/open?id=1V7XSL4e8EBmM4SSiriDcRlyKqA6ZUjvl'" href="#">5.0.15.1790-Setup-x64.exe</a></li>
													</ul>
												</li>
											</ul>
										</li>
									</ul>
								</li>
								<li>Rene
									<ul>
										<li>Child node 1</li>
										<li><a href="#">Child node 2</a></li>
									</ul>
								</li>
							</ul>
						</div>

					</div>
				</div>
			</div>
		</section>
	  <?php include_once $data->homedir.'view/homepage/footer.homepage.php'; ?>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/jquery-2.2.4.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/bootstrap.min.js"></script>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/easing.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/hoverIntent.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/superfish.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.ajaxchimp.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.magnific-popup.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/owl.carousel.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.sticky.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.nice-select.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/parallax.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/mail-script.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/main.js"></script>
		<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script>
		<script>
$(function () {

	$('#jstree').jstree({
  "core" : {
    "animation" : 0,
    "check_callback" : true,
    "themes" : { "stripes" : true }
  },
  "types" : {
    "#" : {
      "max_children" : 1,
      "max_depth" : 4,
      "valid_children" : ["root"]
    },
    "root" : {
      "icon" : "/static/3.3.7/assets/images/tree_icon.png",
      "valid_children" : ["default"]
    },
    "default" : {
      "valid_children" : ["default","file"]
    },
    "file" : {
      "icon" : "glyphicon glyphicon-file",
      "valid_children" : []
    }
  },
  "plugins" : [
     "state", "types"
  ]
});
	// 6 create an instance when the DOM is ready
	$('#jstree').jstree();
	// 7 bind to events triggered on the tree
	$('#jstree').on("changed.jstree", function (e, data) {
		// console.log(data.node.a_attr.href);
		//
	});
	// 8 interact with the tree - either way is OK
	// $('button').on('click', function () {
	//   $('#jstree').jstree(true).select_node('child_node_1');
	//   $('#jstree').jstree('select_node', 'child_node_1');
	//   $.jstree.reference('#jstree').select_node('child_node_1');
	// });
});



		AOS.init();

		$(function () {
		  $('[data-toggle="popover"]').popover()
		})

			var owl = $('.owl-carousel');
			owl.owlCarousel({
			    items:5,
			    // loop:true,
			    margin:10,
			    autoplay:true,
			    // autoplayTimeout:4000,
			    autoplayHoverPause:true,
					responsiveClass:true,
					loop:true,
			responsive:{
			0:{
				items:2,
				nav:true
			},
			600:{
				items:3,
				nav:false
			},
			1000:{
				items:5,
				nav:false
			}
			}

			});

		</script>


	</body>
</html>
