<!-- <?php include_once 'baseurl.php'; ?> -->
<html class="no-js" lang="id">
<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="<?= $data->base_url ?>images/homepage2019/elements/fav.png">
	<!-- Author Meta -->
	<meta name="author" content="Rifzky Alam, Dino Damara">
	<!-- Meta Description -->
	<meta name="description" content="<?= $data->pagedesc ?>">
	<!-- Meta Keyword -->
	<meta name="keywords" content="<?= $data->pagekeywords ?>">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title><?= $data->title ?></title>
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
		<!--
			CSS
			============================================= -->
		<link rel="shortcut icon" href='https://fac-institute.com/_caramel/assets/img/g25992_9_kUd_icon.ico'>
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/linearicons.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/bootstrap.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/magnific-popup.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/nice-select.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/animate.min.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/owl.carousel.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/main.css">
		<style media="screen">
			.banner-content h1 {
			font-size: 40px;
			}
			.nav-menu {
			padding-top: 15px;
			}
			.nav-menu a {
			padding: 1px 8px 1px 8px;
			font-size: 16px;
			font-weight: 500;
			}
			.nav-menu ul li a {
			font-size: 14px;
			}
			.menu-active {
			border-bottom: 5px solid #df003a;
			}
			.navbar {
			padding: 0.2rem 1rem;
			}

		</style>
		<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">


	</head>
	<body>



    <?php include_once $data->homedir.'view/homepage/header.homepage.php'; ?>

		<!-- <section class="mt-80" style='background: url("") ;background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:20px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
			<div class="container-fluid">
				<div class="row justify-content-center align-items-center">
					<div class="col-lg-12" style="max-width:1000px">

				</div>
			</div>
		</section> -->


		<section class="we-offer-area section-gap" id="offer">
			<div class="row d-flex justify-content-center">
				<div class="menu-content mt-80 col-lg-10">
					<div class="title text-center">
						<h1>Dukungan yang kami berikan?</h1>
						<p class="mt-40" style="font-size:20px">FREE Support</p>
					</div>
				</div>
			</div>
		</section>

		<section style='background: url("") ;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:20px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
			<div class="container-fluid">
				<div class="row justify-content-center align-items-center">
					<div class="col-lg-8" style="max-width:500px">
						<p style="font-size:18px;font-weight:bold;line-height:18px" class="mb-10">WhatsApp ke Team Support <span style="color:green;font-style:italic">(Layanan Prioritas)</span></p>
						<p class="text-justify" style="font-size:14px;line-height:28px">
							Jika ada masalah atau troubleshooting dan pertanyaan seputar cara penggunaan ACCURATE silahkan konsultasikan melalui No. WhatsApp 0813-8072-7262, Jelaskan permasalahan yang dihadapi dan jika perlu kirimkan Print Screen atau Capture Screen (Difoto) Error yang muncul untuk memudahkan team Support dalam Identifikasi masalah yang sedang Anda hadapi. Jam Operasional Team Support Kami adalah sebagai berikut:
							<ul style="list-style-type: circle;margin-bottom:20px">
								<li>Senin – Jum’at : 09.00 – 17.00 WIB</li>
								<li>Sabtu : 09.00 –  12.00 WIB</li>
							</ul>
							Browse masalah atau tutorial solusi dari setiap masalah di dalam Blog Solution center ACCURATE kami kapanpun dan dimanapun Anda berada dengan mengunjungi
							<a href="https://fac-institute.com/tutorial">https://fac-institute.com/tutorial</a>
						</p>
					</div>
					<div class="col-lg-4 mt-40">
						<img class="img-fluid" data-aos="fade-up" src="https://fac-institute.com/images/wa1.png" alt="Entry Data FAC">
					</div>
				</div>
			</div>
		</section>
		<hr>
		<section class="mb-80" style='background: url("") ;background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:20px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
			<div class="container-fluid">
				<div class="row justify-content-center align-items-center">
					<div class="col-lg-4 mt-40">
						<img class="img-fluid" data-aos="fade-up" src="https://fac-institute.com/images/homepage2019/lc.png" alt="Entry Data FAC">
					</div>
					<div class="col-lg-8" style="max-width:500px">
						<p style="font-size:18px;font-weight:bold;line-height:18px" class="mb-10">LIVECHAT Dengan Customer Support</p>
						<p class="text-justify" style="font-size:14px;line-height:28px">
							Kunjungi beranda website kami di <a href="https://fac-institute.com/tutorial">www.fac-institute.com</a> agar bisa melakukan chatting dengan Team Support Kami
						</p>
					</div>
				</div>
			</div>
		</section>

  <hr>
		<section class="mb-80" style='background: url("") ;background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:20px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
			<div class="container-fluid">
				<div class="row justify-content-center align-items-center">
					<div class="col-lg-8" style="max-width:500px">
						<p style="font-size:18px;font-weight:bold;line-height:18px" class="mb-10">Telepon <span style="color:green;font-style:italic">(Bukan Layanan Prioritas)</span></p>
						<p class="text-justify" style="font-size:14px;line-height:28px">
							Anda bisa menghubungi Team Support melalui nomor telepon <b>0813-8072-7262</b>
						</p>
					</div>
					<div class="col-lg-4 mt-40">
						<img class="img-fluid" data-aos="fade-up" src="https://fac-institute.com/images/homepage2019/call2.png" alt="Entry Data FAC">
					</div>
				</div>
			</div>
		</section>

		<hr>

		<section class="mb-80" style='background: url("") ;background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:20px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
			<div class="container-fluid">
				<div class="row justify-content-center align-items-center">
					<div class="col-lg-4 mt-40">
						<img class="img-fluid" data-aos="fade-up" src="https://fac-institute.com/images/homepage2019/teamviewer-logo-icon1.png" alt="Entry Data FAC">
					</div>
					<div class="col-lg-8" style="max-width:500px">
						<p style="font-size:18px;font-weight:bold;line-height:18px" class="mb-10">Premium Support (TeamViewer)</p>
						<p class="text-justify" style="font-size:14px;line-height:28px">
							Premium Support merupakan Support berbayar dan biasanya dalam bentuk Remote Desktop menggunakan software teamviewer. Support ini bersifat kepada Implementasi Software atau Troubleshooting.
							Harga Implementasi bisa berubah sewaktu-waktu. Silahkan klik <a href="https://fac-institute.com/support/premium">disini</a> untuk mendapatkan informasi lebih lanjut.
					</div>
				</div>
			</div>
		</section>



	  <?php include_once $data->homedir.'view/homepage/footer.homepage.php'; ?>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/jquery-2.2.4.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/bootstrap.min.js"></script>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/easing.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/hoverIntent.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/superfish.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.ajaxchimp.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.magnific-popup.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/owl.carousel.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.sticky.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.nice-select.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/parallax.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/mail-script.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/main.js"></script>
		<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

		<script>
		AOS.init();

		$(function () {
		  $('[data-toggle="popover"]').popover()
		})

			var owl = $('.owl-carousel');
			owl.owlCarousel({
			    items:5,
			    // loop:true,
			    margin:10,
			    autoplay:true,
			    // autoplayTimeout:4000,
			    autoplayHoverPause:true,
					responsiveClass:true,
					loop:true,
			responsive:{
			0:{
				items:2,
				nav:true
			},
			600:{
				items:3,
				nav:false
			},
			1000:{
				items:5,
				nav:false
			}
			}

			});

		</script>


	</body>
</html>
