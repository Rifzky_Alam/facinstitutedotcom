<!-- <?php include_once 'baseurl.php'; ?> -->
<html class="no-js" lang="id">
<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="<?= $data->base_url ?>images/homepage2019/elements/fav.png">
	<!-- Author Meta -->
	<meta name="author" content="Rifzky Alam, Dino Damara">
	<!-- Meta Description -->
	<meta name="description" content="<?= $data->pagedesc ?>">
	<!-- Meta Keyword -->
	<meta name="keywords" content="<?= $data->pagekeywords ?>">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title><?= $data->title ?></title>
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
		<!--
			CSS
			============================================= -->
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/linearicons.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/bootstrap.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/magnific-popup.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/nice-select.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/animate.min.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/owl.carousel.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/main.css">
		<style media="screen">
			.banner-content h1 {
			font-size: 40px;
			}
			.nav-menu {
			padding-top: 15px;
			}
			.nav-menu a {
			padding: 1px 8px 1px 8px;
			font-size: 16px;
			font-weight: 500;
			}
			.nav-menu ul li a {
			font-size: 14px;
			}
			.menu-active {
			border-bottom: 5px solid #df003a;
			}
			.navbar {
			padding: 0.2rem 1rem;
			}

		</style>
		<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">


	</head>
	<body>



    <?php include_once $data->homedir.'view/homepage/header.homepage.php'; ?>

		<!-- <section class="mt-80" style='background: url("") ;background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:20px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
			<div class="container-fluid">
				<div class="row justify-content-center align-items-center">
					<div class="col-lg-12" style="max-width:1000px">

				</div>
			</div>
		</section> -->


		<section class="we-offer-area section-gap" id="offer">
			<div class="row d-flex justify-content-center">
				<div class="menu-content mt-80 col-lg-10">
					<div class="title text-center">
						<h1>Dukungan Premium Support</h1>
						<!-- <p class="mt-40" style="font-size:20px">FREE Support</p> -->
					</div>
				</div>
			</div>
		</section>

		<section class="mb-80" style='background: url("") ;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:20px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
			<div class="container-fluid">
				<div class="col-lg-12 text-center mb-40" style="max-width:1200px">
						<img class="img-fluid" data-aos="fade-up" width="500" src="http://fac-institute.com/images/homepage2019/mass-deployment.png" alt="Entry Data FAC">
				</div>
				<div class="row justify-content-center align-items-center">
					<div class="col-lg-6" style="max-width:800px">
						<p style="font-size:18px;font-weight:bold;line-height:18px" class="mb-10">Premium Support<span style="color:green;"> (TeamViewer)</span></p>
						<p class="text-justify" style="font-size:18px;line-height:28px">
							Premium Support merupakan Support berbayar dalam bentuk Remote Desktop menggunakan software TeamViewer. Support ini bersifat kepada Implementasi Software atau Troubleshooting.
							Ketentuan Layanan Premium Support adalah sebagai berikut:
							<ul style="list-style-type: circle;margin-bottom:20px;font-size:18px">
								<li>Biaya TeamViewer <b>200 Ribu</b>/jam</li>
								<li>Min. 1 jam</li>
								<li>Pembayaran dilakukan sebelum TeamViewer dimulai</li>
								<li>Waktu TeamViewer sesuai dengan kesepakatan bersama</li>
							</ul>
							<p>*Jika melebihi 1 jam, akan dikenakan charge sisa waktu. Maksimal toleransi 10 menit.</p>
						</p>
					</div>


				</div>
			</div>
		</section>






	  <?php include_once $data->homedir.'view/homepage/footer.homepage.php'; ?>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/jquery-2.2.4.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/bootstrap.min.js"></script>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/easing.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/hoverIntent.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/superfish.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.ajaxchimp.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.magnific-popup.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/owl.carousel.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.sticky.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.nice-select.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/parallax.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/mail-script.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/main.js"></script>
		<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

		<script>
		AOS.init();

		$(function () {
		  $('[data-toggle="popover"]').popover()
		})

			var owl = $('.owl-carousel');
			owl.owlCarousel({
			    items:5,
			    // loop:true,
			    margin:10,
			    autoplay:true,
			    // autoplayTimeout:4000,
			    autoplayHoverPause:true,
					responsiveClass:true,
					loop:true,
			responsive:{
			0:{
				items:2,
				nav:true
			},
			600:{
				items:3,
				nav:false
			},
			1000:{
				items:5,
				nav:false
			}
			}

			});

		</script>


	</body>
</html>
