<style>
.how-it-work {
margin-bottom: 60px;
position: relative;
}
.main-how-it {
position: absolute;
top: 60px;
left: -225px;
background: #f6f6f6;
width: 273px;
height: 110px;
transform: rotate(-90deg);
border-radius: 50px;
box-shadow: 0px 2px 11px -2px rgba(0,0,0,0.4);
}
.main-how-it h4 {
font-size: 32px;
padding-top: 50px;
text-align: center;
text-transform: uppercase;
color:#3299ff;
font-weight: 800;
}
.bg-theme {
color: #0066cc;
}
.how-it-work .panel {
border-top-left-radius: 60px;
border-bottom-left-radius: 60px;
color:#fff;
font-size: 16px;
background: #0066cc;
box-shadow: 0px 1px 7px 1px rgba(0,0,0,0.4);
margin-bottom:15px;
}
.how-it-work .panel span,
.how-it-work .panel:hover span  {
-webkit-transition: all ease-out 0.3s;
-moz-transition: all ease-out 0.3s;
-o-transition: all ease-out 0.3s;
transition: all ease-out 0.3s;
}
.how-it-work .panel:hover span {
background: rgba(0,0,0,0.4);
}
.how-it-work .panel:nth-child(even) {
background: #3299ff;
}
.how-it-work .panel-body {
overflow-y: auto;
overflow: visible;
height: 120px;
padding-left: 120px;
position: relative;
}
.how-it-work .panel-body span {
position: absolute;
top: 15px;
left: 0px;
width: 90px;
height: 90px;
line-height: 90px;
color: #008931;
background: #f6f6f6;
text-align: center;
font-size: 36px;
font-weight: 800;
border-radius: 50%;
box-shadow: 0px 2px 10px rgba(0,0,0,0.4);
}
.how-it-work .panel-body span:before {
content: "";
position: absolute;
top: 35px;
left: -49px;
width: 50px;
height: 20px;
background: #f6f6f6;
box-shadow: 0px 8px 3px -5px rgba(0,0,0,0.2);
}
.how-it-work .panel:nth-child(even) span {
color: #78c043;
}
.how-it-work .step-heading {
font-size: 28px;
margin-top: 0px;
/* color: rgba(0,0,0,0.3); */
padding-top:15px;
}
</style>

<div data-aos="fade-right" class="container mt-100">
  <div class="row">
    <div class="how-it-work clearfix">
      <div class="main-how-it">
        <h4> Order<span class="bg-theme"> Now!</span> </h4>
      </div>
      <div class="panel panel-default col-sm-12 col-sm-offset-2 ">
        <div class="panel-body">
          <span> <img src="<?= $data->base_url ?>_hazelnut/img/rika_Fotor.jpg" width="100%" class="rounded-circle" alt="img"> </span>
          <h3 class="step-heading text-white mb-2"> Siti Nurhasanah </h3>
          <p style="font-weight:bold;font-size:30px" class="mb-10"><a style="color:white" href="tel:+6281290083983">081290083983</a></p>
          (Call/WhatsApp)
        </div>
      </div>
      <div class="panel panel-default col-sm-12 col-sm-offset-2">
        <div class="panel-body">
          <span> <img src="http://fac-institute.com/images/homepage/_DSC4131_Fotor_Fotor.jpg" width="100%" class="rounded-circle" alt="img"> </span>
          <h3 class="step-heading text-white mb-2">Siti Sarah </h3>
          <p style="font-weight:bold;font-size:30px" class="mb-10"><a style="color:white" href="tel:+6281219115005">081219115005</a></p>
          (Call/WhatsApp)
        </div>
      </div>
    </div>
  </div>
</div>
