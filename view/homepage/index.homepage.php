<!-- <?php include_once 'baseurl.php'; ?> -->
<html class="no-js" lang="id">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="google-site-verification" content="EfNVtXecxElbcbugY59SeKXJuMzO67bjVvTvlHCc_CM" />
    <meta name="msvalidate.01" content="D925497D8F382164FF0CE0267D381FDA" />
    <meta name="thumbnailUrl" content="<?= $data->base_url ?>_caramel/assets/img/g44508.png" />
    <meta name="description" content="FAC - Kami Melayani Training Software Akuntansi ACCURATE dan Accounting Service Untuk Perusahaan Anda" />
    <meta content="First Asian Consulting. Konsultan Akuntansi & Lembaga Pendidikan Komputerisasi Akuntansi" />
    <meta name="keywords" content="Training,Akuntansi,ACCURATE,Jasa">
    <meta name="author" content="Rifzky Alam, Dino Damara">
    <title>FAC Institute</title>
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
		<!--
			CSS
			============================================= -->
    <link rel="shortcut icon" href='https://fac-institute.com/_caramel/assets/img/g25992_9_kUd_icon.ico'>
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/linearicons.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/bootstrap.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/magnific-popup.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/nice-select.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/animate.min.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/owl.carousel.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/main.css">
		<style media="screen">
			.banner-content h1 {
			font-size: 40px;
			}
			.nav-menu {
			padding-top: 15px;
			}
			.nav-menu a {
			padding: 1px 8px 1px 8px;
			font-size: 16px;
			font-weight: 500;
			}
			.nav-menu ul li a {
			font-size: 14px;
			}
			.menu-active {
			border-bottom: 5px solid #df003a;
			}
			.navbar {
			padding: 0.2rem 1rem;
			}

      #banner1{
        height: 600px;
      }

      @media (max-width: 780px) {

      }

      @media (max-width: 480px) {
        .banner-area {
          height:500px;
        }
      }

      @media (max-width: 360px) {
        .banner-content h1 {
          font-size: 22px;
        }
        .banner-area {
          height:400px;
        }
        #banner1{
          height: 500px;
        }
      }




		</style>
		<link href="https://fac-institute.com/assets/homepage2019/css/aos.css" rel="stylesheet">
    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-M4XWFZM');
    </script>
    <!-- End Google Tag Manager -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-79005928-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-79005928-1');
</script>



    <link rel="stylesheet" href="https://fac-institute.com/assets/homepage2019/css/floating-wpp.min.css">

    <!-- <link href="https://vjs.zencdn.net/7.4.1/video-js.css" rel="stylesheet">
    <script src="https://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script> -->
    <link href="https://vjs.zencdn.net/4.3/video-js.css" rel="stylesheet">
    <script src="https://vjs.zencdn.net/4.3/video.js"></script>
	</head>

	<body>


    <!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="ModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <img src="https://fac-institute.com/images/homepage2019/lebaran2019.png" width="100%" />
      </div>
    </div>
  </div>
</div>

    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M4XWFZM" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->

  <?php include_once $data->homedir.'view/homepage/header.homepage.php'; ?>
		<section class="banner-area" id="home">
			<div class="container">
				<div id="banner1" class="row d-flex align-items-center justify-content-center">
					<div data-aos="flip-left" class="banner-content col-lg-8">
						<h1 class="pt-20" style="background-color: rgba(245, 245, 245, 0.9);">
							Sukses Kelola Keuangan Usaha Anda dengan ACCURATE!
						</h1>
						<p class="pt-10 pb-20 px-2" style="font-size:17px;background-color: rgba(245, 245, 245, 0.9);">
							<b>FAC Institute</b> membantu Anda mahir <b>Software Accurate</b> <br>tanpa harus mahir akuntansi
						</p>
            <button type="button" onclick="window.location.href=<?= "'".$data->base_url."training'" ?>" class="btn btn-primary btn-lg mt-10">Selengkapnya</button>
          </div>
				</div>
			</div>
		</section>
		<!-- End banner Area -->
		<!-- Start we-offer Area -->
		<section class="we-offer-area mb-40 section-gap" id="offer">
			<div class="container">
				<div class="row d-flex justify-content-center">
					<div class="menu-content pb-60 col-lg-10">
						<div class="title text-center">
							<h1 class="mb-10" style="font-size:34px">FAC Institute Partner Terbaik Pembukuan Bisnis Anda</h1>
              <!-- <p style="font-size:18px">FAC Institute memberikan <b>training</b> dan <b>pelatihan SOFTWARE ACCURATE</b> sejak 2013. Kami menciptakan metodologi pembelajaran ACCURATE tanpa perlu mahir akuntansi, karena Kami ingin membuat proses akuntansi lebih mudah & cepat untuk semua pemilik bisnis.
              </p>
              <p style="font-size:18px">Banyak perusahaan pengguna ACCURATE ACCOUNTING SOFTWARE di Indonesia yang merekrut karyawan keuangan dan pembukuan berdasarkan integritas alias kepercayaan dibanding berdasarkan kompetensi skill keuangan dan skill akuntansi. FAC Institute sangat konsen menyikapi fakta ini. Dari metodologi pembelajaran yang telah kami ciptakan dan kami ajarkan, telah banyak perusahaan UMKM yang kini sudah berkembang dan ekspansi bisnis karena memiliki laporan keuangan yang akuntabel sebagai dasar pengambilan keputusan.
              </p> -->

              <p style="font-size:18px">Kami menciptakan metodologi pembelajaran ACCURATE tanpa perlu mahir akuntansi.
              </p>
              <p style="font-size:18px">Kami membantu laporan keuangan yang akuntabel sebagai dasar pengambilan keputusan.
              </p>

							<p style="font-size:18px;font-weight:bold;font-style:italic;margin-top:80px">Mengapa Harus FAC Institute ?</p>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-6">
						<div data-aos="" class="single-offer d-flex flex-row pb-30">
							<div class="icon">
								<img width="95" src="https://fac-institute.com/images/services-icon-1-1.png" alt="">
							</div>
							<div class="desc">
								<a href="#">
									<h4>Trainer Berpengalaman</h4>
								</a>
								<p>
									Kami memiliki trainer berpengalaman yang siap diterjunkan ke seluruh kota di <b>Indonesia</b>
								</p>
							</div>
						</div>
						<div data-aos="" class="single-offer d-flex flex-row pb-30">
							<div class="icon">
								<img width="90" src="<?= $data->base_url ?>_caramel/assets/img/Logo-ACCURATE-5.png" alt="">
							</div>
							<div class="desc">
								<a href="#">
									<h4>ACCURATE Authorized Training Center</h4>
								</a>
								<p>
									Memiliki izin resmi dari <b>CPSSOFT</b> sebagai Trainer Accurate
								</p>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div data-aos="" class="single-offer d-flex flex-row pb-30">
							<div class="icon">
								<img width="90"  src="<?= $data->base_url ?>images/homepage2019/online-support.png" alt="">
							</div>
							<div class="desc">
                <a href="#">
                  <h4 >Free Support <span><i class="fa fa-star text-warning" aria-hidden="true"></i></span></h4>
                </a>
								<p>
									Dukungan penuh setelah training via <b>WhatsApp</b> / Chat / Email dan Telepon seluler <button type="button" class="btn btn-danger btn-sm text-white" onclick="window.location.href=<?= "'".$data->base_url."support'" ?>">Selengkapnya</button>
								</p>
							</div>
						</div>
						<div data-aos="" class="single-offer d-flex flex-row pb-30">
							<div class="icon">
								<img  width="90" src="<?= $data->base_url ?>images/homepage2019/calendar.png" alt="">
							</div>
							<div class="desc">
								<a href="#">
									<h4>Scheduled Training</h4>
								</a>
								<p>
									Tentukan sendiri jadwal training Anda!  <button type="button" class="btn btn-danger btn-sm text-white" onclick="window.location.href=<?= "'".$data->base_url."daftar-training'" ?>">Order Now</button>
								</p>
							</div>
						</div>
					</div>
				</div>

			</div>
		</section>
		<!-- End we-offer Area -->
		<!-- Start home-video Area -->
		<section data-aos="flip-down" class="home-video-area" id="about">
			<div class="container-fluid">
				<div class="row justify-content-end align-items-center">
					<div class="col-lg-4 no-padding video-right">
						<!-- <p class="top-title">Tutorial for beginner</p> -->
						<h1>Ketahui Tentang <br>
							FAC Institute
						</h1>
						<p style="font-size:16px"><span>FAC Institute adalah ACCURATE Athorized Training Center</span></p>
						<p>
              <i>Lembaga Pendidikan Komputerisasi Akuntansi
              yang menyelenggarakan <b>training ACCURATE</b>
              di seluruh INDONESIA.</i>
						</p>
					</div>
					<section class="col-lg-6">
						<div class="container">
							<!-- <div class="video-content">
								<a href="https://www.youtube.com/watch?v=JUxhwFdWhw0" class="play-btn"><img src="img/play-btn.png" alt=""></a>
								</div> -->
							<!-- <iframe width="100%" height="315" src="https://www.youtube.com/embed/JUxhwFdWhw0" frameborder="0" allowfullscreen></iframe> -->

              <!-- <video id='my-video' class='video-js' controls preload='auto' width='640' height='264'
                poster='http://fac-institute.com/video/DSC_4024_1.mp4_snapshot_00.01.546.jpg' data-setup='{}'>
                  <source src='https://fac-institute.com/video/DSC_4024_1.mp4' type='video/mp4'>
                  <source src='MY_VIDEO.webm' type='video/webm'>
                  <p class='vjs-no-js'>
                    To view this video please enable JavaScript, and consider upgrading to a web browser that
                    <a href='https://videojs.com/html5-video-support/' target='_blank'>supports HTML5 video</a>
                  </p>
                </video> -->


 <video id="my_video_1" class="video-js vjs-default-skin" controls preload="auto"
 data-setup='{ "asdf": true }' poster="https://fac-institute.com/video/DSC_4024_1.mp4_snapshot_00.01.546.jpg" >
   <source src="https://fac-institute.com/video/DSC_4024_1.mp4" type='video/mp4'>
 </video>
            </div>
					</section>
				</div>
			</div>
		</section>

    <!-- <div class="container-fluid mt-100 ">
      <div class="row justify-content-center align-items-center" >
        <div class="col-lg-12 text-center">
          <p style="font-size:32px;font-style:italic;font-weight:bold">BEST TRAINER </p>
          <p style="font-size:22px;font-style:italic;font-weight:bold">EDWIN</p>
        </div>
        <div class="col-lg-12 text-center" style="max-width:400px">
          <img class="img-fluid mb-20" width="100%" data-aos="fade-up" src="https://fac-institute.com/images/homepage2019/eded.PNG" alt="">
        </div>
      </div>
    </div> -->





		<!-- Start testomial Area -->
		<section class="testomial-area section-gap">
			<div class="container">
				<div class="row d-flex justify-content-center">
					<div class="menu-content pb-40 col-lg-12">
						<div  data-aos="zoom-out" class="title text-center">
							<h1 class="mb-10">Dipercaya oleh 1000+ Bisnis di Seluruh <b >Indonesia</b></h1>
							<p style="font-size:18px">FAC Institute telah melakukan training ACCURATE ke lebih dari 1000 klien di berbagai wilayah Indonesia.</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="owl-carousel owl-theme">
						<div class="item"><img src="<?= $data->base_url ?>images/homepage/image5594.png"> </div>
						<div class="item"><img src="<?= $data->base_url ?>images/homepage/image4352.png"> </div>
						<div class="item"><img src="<?= $data->base_url ?>images/homepage/image3634.png"> </div>
						<div class="item"><img src="<?= $data->base_url ?>images/homepage/image3640.png"> </div>
						<div class="item"><img src="<?= $data->base_url ?>images/homepage/image3578.png"> </div>
						<div class="item"><img src="<?= $data->base_url ?>images/homepage/image4958.png"> </div>
						<div class="item"><img src="<?= $data->base_url ?>images/homepage/rect32011.png"> </div>
						<div class="item"><img src="<?= $data->base_url ?>images/homepage/rect3201g.png"> </div>
						<div class="item"><img src="<?= $data->base_url ?>images/homepage/rect3201a.png"> </div>
						<div class="item"><img src="<?= $data->base_url ?>images/homepage/rect3201d.png"> </div>
					</div>
				</div>
			</div>
		</section>



		<section class="home-aboutus-area">
			<div class="container-fluid">
				<div class="row justify-content-center align-items-center">
					<div class="col-lg-8 no-padding about-left mt-40 mb-40">
						<img class="img-fluid" data-aos="fade-up" src="<?= $data->base_url ?>images/homepage2019/why-us-hero.svg" alt="">
					</div>
					<div class="col-lg-4 no-padding about-right">
						<p class="top-title text-white" style="font-size:20px">Weekday Office Support</p>
						<h1 class="text-white" style="font-size:28px">Dukungan Penuh<br>
						Setelah Training</h1>
						<!-- <p><span>Kami mengerti segala masalah Anda</span></p> -->
						<p class="text-white">
							Kami mengerti masalah Anda! Kami memberikan dukungan penuh selama (weekday - office hours) untuk solusi masalah Anda.
              Hubungi kami via WhatsApp, Email & Telepon seluler
						</p>
					</div>
				</div>
			</div>
		</section>



        <section class="testomial-area section-gap">
    			<div class="container">
    				<div class="row d-flex justify-content-center">
    					<div class="menu-content pb-40 col-lg-12">
    						<div  data-aos="zoom-out" class="title text-center">
    							<h1 class="mb-10">Follow us on <b>Instagram!</b></h1>
    						</div>
    					</div>
    				</div>
    				<div class="row">
              	<div id="instagram-feed-demo" class="instagram_feed"></div>
              <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
              <script src="https://www.jqueryscript.net/demo/Instagram-Photos-Without-API-instagramFeed/jquery.instagramFeed.js"></script>
              <script>
              (function($){
              		$(window).on('load', function(){
              			$.instagramFeed({
              				'username': 'facinstitute',
              				'container': "#instagram-feed-demo",
              				'display_profile': false,
              				'display_biography': false,
              				'display_gallery': true,
              				'get_raw_json': false,
              				'callback': null,
              				'styling': true,
              				'items': 8,
              				'items_per_row': 4,
              				'margin': 1
              			});
              		});
              	})(jQuery);
              </script>
    				</div>
    			</div>
    		</section>



      <!-- <script src='https://vjs.zencdn.net/7.4.1/video.js'></script> -->

	  <?php include_once $data->homedir.'view/homepage/footer.homepage.php'; ?>

    <div class="floating-wpp"></div>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/jquery-2.2.4.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/bootstrap.min.js"></script>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/easing.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/hoverIntent.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/superfish.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.ajaxchimp.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.magnific-popup.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/owl.carousel.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.sticky.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.nice-select.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/parallax.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/mail-script.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/main.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/aos.js"></script>

		<script>
		AOS.init();

		$(function () {
		  $('[data-toggle="popover"]').popover()
		})

			var owl = $('.owl-carousel');
			owl.owlCarousel({
			    items:5,
			    // loop:true,
			    margin:10,
			    autoplay:true,
			    // autoplayTimeout:4000,
			    autoplayHoverPause:true,
					responsiveClass:true,
					loop:true,
			responsive:{
			0:{
				items:2,
				nav:true
			},
      300:{
        items:2,
        nav:false
      },
			600:{
				items:3,
				nav:false
			},
			1000:{
				items:5,
				nav:false
			}
			}

			});
			$('.play').on('click',function(){
			    owl.trigger('play.owl.autoplay',[1000])
			})
			$('.stop').on('click',function(){
			    owl.trigger('stop.owl.autoplay')
			})
		</script>

    <script type="text/javascript">
        function visitorDetector(window) {
            {
                var unknown = '-';
                // browser
                var nVer = navigator.appVersion;
                var nAgt = navigator.userAgent;
                var browser = navigator.appName;
                var version = '' + parseFloat(navigator.appVersion);
                var majorVersion = parseInt(navigator.appVersion, 10);
                var nameOffset, verOffset, ix;

                // Opera
                if ((verOffset = nAgt.indexOf('Opera')) != -1) {
                    browser = 'Opera';
                    version = nAgt.substring(verOffset + 6);
                    if ((verOffset = nAgt.indexOf('Version')) != -1) {
                        version = nAgt.substring(verOffset + 8);
                    }
                }
                // Opera Next
                if ((verOffset = nAgt.indexOf('OPR')) != -1) {
                    browser = 'Opera';
                    version = nAgt.substring(verOffset + 4);
                }
                // MSIE
                else if ((verOffset = nAgt.indexOf('MSIE')) != -1) {
                    browser = 'Microsoft Internet Explorer';
                    version = nAgt.substring(verOffset + 5);
                }
                // Chrome
                else if ((verOffset = nAgt.indexOf('Chrome')) != -1) {
                    browser = 'Chrome';
                    version = nAgt.substring(verOffset + 7);
                }
                // Safari
                else if ((verOffset = nAgt.indexOf('Safari')) != -1) {
                    browser = 'Safari';
                    version = nAgt.substring(verOffset + 7);
                    if ((verOffset = nAgt.indexOf('Version')) != -1) {
                        version = nAgt.substring(verOffset + 8);
                    }
                }
                // Firefox
                else if ((verOffset = nAgt.indexOf('Firefox')) != -1) {
                    browser = 'Firefox';
                    version = nAgt.substring(verOffset + 8);
                }
                // MSIE 11+
                else if (nAgt.indexOf('Trident/') != -1) {
                    browser = 'Microsoft Internet Explorer';
                    version = nAgt.substring(nAgt.indexOf('rv:') + 3);
                }
                // Other browsers
                else if ((nameOffset = nAgt.lastIndexOf(' ') + 1) < (verOffset = nAgt.lastIndexOf('/'))) {
                    browser = nAgt.substring(nameOffset, verOffset);
                    version = nAgt.substring(verOffset + 1);
                    if (browser.toLowerCase() == browser.toUpperCase()) {
                        browser = navigator.appName;
                    }
                }
                // trim the version string
                if ((ix = version.indexOf(';')) != -1) version = version.substring(0, ix);
                if ((ix = version.indexOf(' ')) != -1) version = version.substring(0, ix);
                if ((ix = version.indexOf(')')) != -1) version = version.substring(0, ix);

                majorVersion = parseInt('' + version, 10);
                if (isNaN(majorVersion)) {
                    version = '' + parseFloat(navigator.appVersion);
                    majorVersion = parseInt(navigator.appVersion, 10);
                }

                // mobile version
                var mobile = /Mobile|mini|Fennec|Android|iP(ad|od|hone)/.test(nVer);


                // system
                var os = unknown;
                var clientStrings = [{
                        s: 'Windows 10',
                        r: /(Windows 10.0|Windows NT 10.0)/
                    },
                    {
                        s: 'Windows 8.1',
                        r: /(Windows 8.1|Windows NT 6.3)/
                    },
                    {
                        s: 'Windows 8',
                        r: /(Windows 8|Windows NT 6.2)/
                    },
                    {
                        s: 'Windows 7',
                        r: /(Windows 7|Windows NT 6.1)/
                    },
                    {
                        s: 'Windows Vista',
                        r: /Windows NT 6.0/
                    },
                    {
                        s: 'Windows Server 2003',
                        r: /Windows NT 5.2/
                    },
                    {
                        s: 'Windows XP',
                        r: /(Windows NT 5.1|Windows XP)/
                    },
                    {
                        s: 'Windows 2000',
                        r: /(Windows NT 5.0|Windows 2000)/
                    },
                    {
                        s: 'Windows ME',
                        r: /(Win 9x 4.90|Windows ME)/
                    },
                    {
                        s: 'Windows 98',
                        r: /(Windows 98|Win98)/
                    },
                    {
                        s: 'Windows 95',
                        r: /(Windows 95|Win95|Windows_95)/
                    },
                    {
                        s: 'Windows NT 4.0',
                        r: /(Windows NT 4.0|WinNT4.0|WinNT|Windows NT)/
                    },
                    {
                        s: 'Windows CE',
                        r: /Windows CE/
                    },
                    {
                        s: 'Windows 3.11',
                        r: /Win16/
                    },
                    {
                        s: 'Android',
                        r: /Android/
                    },
                    {
                        s: 'Open BSD',
                        r: /OpenBSD/
                    },
                    {
                        s: 'Sun OS',
                        r: /SunOS/
                    },
                    {
                        s: 'Linux',
                        r: /(Linux|X11)/
                    },
                    {
                        s: 'iOS',
                        r: /(iPhone|iPad|iPod)/
                    },
                    {
                        s: 'Mac OS X',
                        r: /Mac OS X/
                    },
                    {
                        s: 'Mac OS',
                        r: /(MacPPC|MacIntel|Mac_PowerPC|Macintosh)/
                    },
                    {
                        s: 'QNX',
                        r: /QNX/
                    },
                    {
                        s: 'UNIX',
                        r: /UNIX/
                    },
                    {
                        s: 'BeOS',
                        r: /BeOS/
                    },
                    {
                        s: 'OS/2',
                        r: /OS\/2/
                    },
                    {
                        s: 'Search Bot',
                        r: /(nuhk|Googlebot|Yammybot|Openbot|Slurp|MSNBot|Ask Jeeves\/Teoma|ia_archiver)/
                    }
                ];
                for (var id in clientStrings) {
                    var cs = clientStrings[id];
                    if (cs.r.test(nAgt)) {
                        os = cs.s;
                        break;
                    }
                }

                var osVersion = unknown;

                if (/Windows/.test(os)) {
                    osVersion = /Windows (.*)/.exec(os)[1];
                    os = 'Windows';
                }

                switch (os) {
                    case 'Mac OS X':
                        osVersion = /Mac OS X (10[\.\_\d]+)/.exec(nAgt)[1];
                        break;

                    case 'Android':
                        osVersion = /Android ([\.\_\d]+)/.exec(nAgt)[1];
                        break;

                    case 'iOS':
                        osVersion = /OS (\d+)_(\d+)_?(\d+)?/.exec(nVer);
                        osVersion = osVersion[1] + '.' + osVersion[2] + '.' + (osVersion[3] | 0);
                        break;
                }


            }

            window.jscd = {
                browser: browser,
                browserVersion: version,
                browserMajorVersion: majorVersion,
                mobile: mobile,
                os: os,
                osVersion: osVersion
            };

            var jsonDatas = {
                'ip': <?php echo "'".$_SERVER['REMOTE_ADDR']."'"; ?>,
                'os': jscd.os + ' ' + jscd.osVersion,
                'browser': jscd.browser + ' ' + jscd.browserMajorVersion,
                'mobile': jscd.mobile
            }
            //alert(peserta);

            $.ajax({
                type: "POST",
                url: "controller/visitor",
                data: {
                    'jsonData': jsonDatas
                },
                cache: false,
                success: function(data) {
                    // alert(data);
                    //window.location.replace("index.php");
                }
            }); //end ajax

        }(this);

        $(document).ready(function() {
            visitorDetector(window);

            $("#back-top").hide();

            $(function() {
                $(window).scroll(function() {
                    if ($(this).scrollTop() > 100) {
                        $('#back-top').fadeIn();
                    } else {
                        $('#back-top').fadeOut();
                    }
                });

                $('#back-top .fi-arrow-up').click(function() {
                    $('body,html').animate({
                        scrollTop: 0
                    }, 800);
                    return false;
                });
            });

        });
    </script>


    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "Course",
            "name": "Training Software ACCURATE, Audit data dan Kursus Akuntansi",
            "description": "Kami mengajari, tanpa harus Anda memahami akuntansi.",
            "provider": {
                "@type": "Organization",
                "name": "FAC Institute",
                "sameAs": "http://www.fac-institute.com"
            }
        }
    </script>

    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "ProfessionalService",
            "@id": "<?= $data->base_url ?>",
            "name": "FAC Institute",
            "image": "<?= $data->base_url ?>images/logo-fac.jpg",
            "address": {
                "@type": "PostalAddress",
                "streetAddress": "jalan no., Jl. Pangkalan Jati I A No.8, Jatiwaringin",
                "addressLocality": "Jakarta Timur",
                "addressRegion": "Jakarta",
                "postalCode": "13620",
                "addressCountry": "Indoneia"
            },
            "geo": {
                "@type": "GeoCoordinates",
                "latitude": -6.248042,
                "longitude": 106.907903
            },
            "telephone": "+6281290083983",
            "potentialAction": {
                "@type": "ReserveAction",
                "target": {
                    "@type": "EntryPoint",
                    "urlTemplate": "<?= $data->base_url ?>order",
                    "inLanguage": "id",
                    "actionPlatform": [
                        "http://schema.org/DesktopWebPlatform",
                        "http://schema.org/IOSPlatform",
                        "http://schema.org/AndroidPlatform"
                    ]
                },
                "result": {
                    "@type": "Reservation",
                    "name": "Pesan Training Software ACCURATE/Accounting Service"
                }
            }
        }
    </script>

    <!--Start of Tawk.to Script-->
    <!-- <script type="text/javascript">
        var Tawk_API = Tawk_API || {},
            Tawk_LoadStart = new Date();
        (function() {
            var s1 = document.createElement("script"),
                s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/576deedca4fa94c76a6d4cad/default';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script> -->


        <script id="rendered-js" >
           videojs.autoSetup();

        videojs('my_video_1').ready(function(){
          console.log(this.options()); //log all of the default videojs options

           // Store the video object
          var myPlayer = this, id = myPlayer.id();
          // Make up an aspect ratio
          var aspectRatio = 264/640;

          function resizeVideoJS(){
            var width = document.getElementById(id).parentElement.offsetWidth;
            myPlayer.width(width).height( width * aspectRatio );

          }

          // Initialize resizeVideoJS()
          resizeVideoJS();
          // Then on resize call resizeVideoJS()
          window.onresize = resizeVideoJS;
        });
          //# sourceURL=pen.js
        </script>


    <!--End of Tawk.to Script-->
    <script type="text/javascript" src="<?= $data->base_url ?>assets/homepage2019/js/floating-wpp.min.js"></script>
    <script type="text/javascript">


    // $(window).load(function()
    // {$('#ModalCenter').modal('show');};

    setTimeout(function() {
    $('#ModalCenter').modal('hide');}, 7000);


        $(function () {
            $('.floating-wpp').floatingWhatsApp({
                phone: '6281290083983',
                popupMessage: 'Selamat datang di layanan WhatsApp Kami. Silahkan tulis kebutuhan training Anda.',
                showPopup: true,
                position: 'right',
                //autoOpen: false,
                //autoOpenTimer: 4000,
                message: '',
                //headerColor: 'orange',
                headerTitle: 'FAC WhatsApp',
            });
        });
    </script>
	</body>
</html>
