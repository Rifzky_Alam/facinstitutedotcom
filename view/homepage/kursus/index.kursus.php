<!DOCTYPE html>
	<html lang="zxx" class="no-js">
	<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="thumbnailUrl" content="<?= $data->base_url ?>_caramel/assets/img/g44508.png" />
    <meta name="description" content="" />
    <meta content="" />
    <meta name="keywords" content="Training,Akuntansi,ACCURATE,Jasa">
    <meta name="author" content="Rifzky Alam, Dino Damara">
    <title>FAC Institute</title>
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
    <!--
      CSS
      ============================================= -->
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/linearicons.css">
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/bootstrap.css">
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/magnific-popup.css">
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/nice-select.css">
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/animate.min.css">
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/owl.carousel.css">
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/main.css">
			<style media="screen">
			.banner-content h1 {
			    font-size: 38px;
			}
			.nav-menu a {
					padding: 15px 8px 1px 8px;
					font-size: 16px;
					font-weight: 500;
			}
			.nav-menu ul li a {
					font-size: 14px;
			}

			.menu-active {
			  border-bottom: 5px solid #df003a;
			}

			.navbar {
			    padding: 0.2rem 1rem;
			}


			.circle {
			  padding: 13px 20px;
			  border-radius: 50%;
			  background-color: #f74e4e;
			  color: #fff;
			  max-height: 50px;
			  z-index: 2;
			}

			.how-it-works.row .col-2 {
			  align-self: stretch;
			}
			.how-it-works.row .col-2::after {
			  content: "";
			  position: absolute;
			  border-left: 3px solid #ED8D8D;
			  z-index: 1;
			}
			.how-it-works.row .col-2.bottom::after {
			  height: 50%;
			  left: 50%;
			  top: 50%;
			}
			.how-it-works.row .col-2.full::after {
			  height: 100%;
			  left: calc(50% - 3px);
			}
			.how-it-works.row .col-2.top::after {
			  height: 50%;
			  left: 50%;
			  top: 0;
			}

			.timeline div {
			  padding: 0;
			  height: 40px;
			}
			.timeline hr {
			  border-top: 3px solid #ED8D8D;
			  margin: 0;
			  top: 17px;
			  position: relative;
			}
			.timeline .col-2 {
			  display: flex;
			  overflow: hidden;
			}
			.timeline .corner {
			  border: 3px solid #ED8D8D;
			  width: 100%;
			  position: relative;
			  border-radius: 15px;
			}
			.timeline .top-right {
			  left: 50%;
			  top: -50%;
			}
			.timeline .left-bottom {
			  left: -50%;
			  top: calc(50% - 3px);
			}
			.timeline .top-left {
			  left: -50%;
			  top: -50%;
			}
			.timeline .right-bottom {
			  left: 50%;
			  top: calc(50% - 3px);
			}


/* tooltip */
			.tooltip {
		      position: relative;
		      display: inline-block;
		      border-bottom: 1px dotted black;
		  }

		  .tooltip .tooltiptext {
		      visibility: hidden;
		      width: 200px;
		      background-color: black;
		      color: #fff;
		      text-align: center;
		      border-radius: 6px;
		      padding: 10px;

		      /* Position the tooltip */
		      position: absolute;
		      z-index: 1;
		      bottom: 100%;
		      left: 50%;
		      margin-left: -90px;
		  }

		  .tooltip:hover .tooltiptext {
		      visibility: visible;
		  }

			#myh2 {
				font-size: 50px;
			}

			#myp {
				font-size: 32px;
			}

			#mybanner {
				height:600px;
			}

			#generic-banner{
				background-color: rgba(255,255,255,0);
				text-align: center;
				background: linear-gradient(rgba(255, 255, 255, 0.3),rgba(255, 255, 255, 0.3)),url(http://fac-institute.com/images/homepage2019/DSC100784171.jpg);
				padding-top: 50px;
				padding-bottom: 50px;
				height:600px;
				-webkit-background-size:cover;
				-moz-background-size:cover;
				-o-background-size:cover;
				background-size:cover;
			}




			@media (max-width: 480px) {
				#myh2 {
					font-size: 22px;
				}
				#mybanner {
					height:400px;
				}
				#myimg{
					display: none;
				}
				#generic-banner{
					height: 350px;
				}
				#mobile-nav-toggle {
					top: 40px;
				}
			}

			@media (max-width: 360px) {
				#mobile-nav-toggle {
					top: 60px;
				}
			}

			</style>
      <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
		</head>
		<body>

    <?php include_once $data->homedir.'view/homepage/kursus/header.kursus.php'; ?>


			<section id="generic-banner">
			<div class="container">
				<div id="mybanner" class="row d-flex align-items-center justify-content-center">
					<div data-aos="flip-left" class="col-lg-6">
							<h2 id="myh2" class="mb-10" style="font-size:50px;color:#df003a;">Kursus Accurate</h2>
							<p id="myp" class="" style="font-size:24px;font-weight:bold">Solusi cepat dari kami untuk mahir Accurate</p>
					</div>
					<div data-aos="flip-right" class="col-lg-6">
						<img id="myimg" width="100%" src="https://fac-institute.com/images/homepage2019/ACC-pabrikasi-rev-min.png" alt="">
					</div>
			</div>
			</div>
			</section>

			<section class="we-offer-area mb-10 mt-40 section-gap" id="offer">
				<div class="container">
					<div class="row d-flex justify-content-center">
						<div class="menu-content pb-60 col-lg-10">
							<div class="title text-center">
								<h1 class="mb-10">Kursus ACCURATE</h1>
								<!-- <p style="font-size:18px"> 6 Alasan Training di FAC Institute </p> -->
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div data-aos="" class="single-offer d-flex flex-row pb-30">
								<div class="icon">
									<img width="90" src="https://fac-institute.com/images/homepage2019/training-course-icon-png-2.png" alt="">
								</div>
								<div class="desc">
									<a href="#">
										<h4>Peserta terbatas</h4>
									</a>
									<p>
										Peserta terbatas untuk mewujudkan kelas kondusif dan efektif
									</p>
								</div>
							</div>

							<div data-aos="" class="single-offer d-flex flex-row pb-30">
								<div class="icon">
									<img width="90" src="https://fac-institute.com/images/homepage2019/training-course-icon-png-3.png" alt="">
								</div>
								<div class="desc">
									<a href="#">
										<h4>Mengulang Gratis</h4>
									</a>
									<p>
										Peserta bisa mengulang gratis di kelas selanjutnya
									</p>
								</div>
							</div>

							<div data-aos="" class="single-offer d-flex flex-row pb-30">
								<div class="icon">
									<img width="90" src="https://fac-institute.com/images/homepage2019/certificate-icon.png" alt="">
								</div>
								<div class="desc">
									<a href="#">
										<h4>Sertifikat Resmi</h4>
									</a>
									<p>
										Mendapatkan sertifikat resmi dari ACCURATE
									</p>
								</div>
							</div>




						</div>

						<div class="col-lg-6">
							<div data-aos="" class="single-offer d-flex flex-row pb-30">
								<div class="icon">
									<img width="90"  src="https://fac-institute.com/_caramel/assets/img/Logo-ACCURATE-5.png" alt="">
								</div>
								<div class="desc">
									<a href="#">
										<h4>Certified ACCURATE Pprofesional</h4>
									</a>
									<p>
										Kami menyediakan ujian CAP (certified ACCURATE profesional)
									</p>
								</div>
							</div>
							<div data-aos=""class="single-offer d-flex flex-row pb-30">
								<div class="icon">
									<img  width="80" src="https://fac-institute.com/images/homepage2019/book_icon_png_165494.png" alt="">
								</div>
								<div class="desc">
									<a href="#">
										<h4>Modul ACCURATE</h4>
									</a>
									<p>
										Peserta mendapatkan modul software ACCURATE
									</p>
								</div>
							</div>
							<div data-aos=""class="single-offer d-flex flex-row pb-30">
								<div class="icon">
									<img  width="90" src="https://fac-institute.com/images/homepage2019/icon-private-coaching.png" alt="">
								</div>
								<div class="desc">
									<a href="#">
										<h4>Private Course</h4>
									</a>
									<p>
										 Kami menyediakan kursus private
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

<hr>
			<section style='background: url("") ;background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:40px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
				<div class="container-fluid">
					<!-- <b>Manfaat yang Anda dapatkan</b> -->
					<div class="row justify-content-center align-items-center" >
						<div class="col-lg-12 text-center" style="max-width:1200px">
							<!-- <p class="top-title" style="font-size:20px">Weekday Office Support</p> -->
							<h1 style="font-size:40px" class="mb-20">Kelas Kursus ACCURATE</h1>
						</div>
						<div class="col-lg-12 " style="max-width:800px">
							<div class="w3-col m8">
                  <ul style="font-size:14px" class="list-group">
                      <li class="list-group-item">Kami ACCURATE Authorized Training Center (AATC) yang mendapatkan izin resmi untuk menyelenggarakan kelas kursus ACCURATE</li>
                      <li class="list-group-item">Kami menciptakan metode pembelajaran Kursus ACCURATE tanpa Anda harus mahir akuntansi.</li>
                      <li class="list-group-item">Peserta terbatas agar kelas kondusif dan efektif</li>
                      <li class="list-group-item">Peserta bisa mengulang gratis di kelas selanjutnya</li>
                      <li class="list-group-item">Mendapatkan sertifikat resmi dari ACCURATE</li>
                      <li class="list-group-item">Kami menyediakan kursus private </li>
                      <li class="list-group-item">Kami menyediakan ujian CAP (certified ACCURATE profesional)</li>
                      <li class="list-group-item">Peserta mendapatkan modul software ACCURATE</li>
                  </ul>

                  <div class="mt-30" style="height:auto;">
                      <p style="font-size:16px"><b>Berikut 3 tema kursus yang biasanya kami berikan kepada klien :</b></p>
                      <h2 class="mt-10"><strong>Kursus Dasar ACCURATE</strong></h2>
                      <p>Ditujukan untuk Sekolah dan Kampus atau akuntan pemula. Dalam kursus ini akan dibahas agenda berikut :
                      </p>
                      <ul class="list-group">
                          <li class="list-group-item">Pelatihan ACCURATE dengan setup metode standar.</li>
                          <li class="list-group-item">Pengenalan fitur-fitur standard dan laporan.</li>
                          <li class="list-group-item">Simulasi dan praktek ACCURATE dengan contoh kasus yang standar.</li>
                      </ul>
                      <h2 class="mt-30"><strong>Kursus Mahir ACCURATE</strong></h2>
                      <p>Ditujukan untuk perusahaan dan konsultan akuntansi. Dalam kursus ini akan dibahas agenda berikut :</p>
                      <ul class="list-group">
                          <li class="list-group-item">Pelatihan ACCURATE dengan setup metode mahir</li>
                          <li class="list-group-item">Pengenalan fitur-fitur dan menerapkannya di perusahaan sesuai proses bisnis yang ada.</li>
                          <li class="list-group-item">Simulasi dan praktek ACCURATE dengan data kasus yang ada di perusahaan.</li>
                      </ul>
                      <h2 class="mt-30"><strong>Kursus ACCURATE secara klasikal atau private</strong></h2>
                      <p>Ditujukan untuk individu yang ingin mendalami ACCURATE. Dalam kursus ini akan dibahas agenda berikut :</p>
                      <ul class="list-group">
                          <li class="list-group-item">Pelatihan ACCURATE dengan setup metode standar dan mahir.</li>
                          <li class="list-group-item">Pengenalan fitur-fitur ACCURATE hingga modul laporan.</li>
                          <li class="list-group-item">Simulasi dan praktek ACCURATE dengan bahan kasus perusahaan contoh.</li>
                      </ul>
                      <p class="mt-10"><strong>Jika kantor Anda membutuhkan kursus akuntansi ACCURATE, silahkan hubungi kami,
												<br>mobile phone : 0812 900 83983, email : training@fac-institute.com</strong></p>
                  </div>

              </div>
						</div>



					</div>
				</div>
			</section>



      <?php include_once $data->homedir.'view/homepage/footer.homepage.php'; ?>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/jquery-2.2.4.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/bootstrap.min.js"></script>
      <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/easing.min.js"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/hoverIntent.js"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/superfish.min.js"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.ajaxchimp.min.js"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.magnific-popup.min.js"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/owl.carousel.min.js"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.sticky.js"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.nice-select.min.js"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/parallax.min.js"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/mail-script.js"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/main.js"></script>
      <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
			<script>
			AOS.init();
			$('.test1').click(function() {
			    var sectionTo = $(this).attr('href');
			    $('html, body').animate({
			      scrollTop: $(sectionTo).offset().top
			    }, 1000);
			});
			</script>
			<script type="text/javascript" src=<?php echo "'".getBaseUrl()."css/datepicker/js/bootstrap-datepicker.js'"; ?>></script>
			<script type="text/javascript">

			$(document).ready(function(){
				var apakah = 0;
				$('#cobaa').datepicker({
					 format: "yyyy-mm-dd"
				});
				$('#fr-accurateLainnya').hide();

					$('[data-toggle="popover"]').popover();


				$('#tambahTombol').click(function(){

						var cobayah = $('.tglPelaksanaan').length;
						var iseng = "tambahan" + cobayah;
						if ($('#tambahan').length){

						if (cobayah==2) {
								$('#tambahan').after("<div class='row' id='"+iseng+"'><div class='col-md-10'><div class='form-inline'><input name='rq[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"'+iseng+'"'+")'></span></div></div></div>");
						}else{
								var okeh = cobayah-1;
								var iseng = "tambahan" + okeh;
								$("#"+iseng).after("<div class='row' id='tambahan"+cobayah+"'><div class='col-md-10'><div class='form-inline'><input name='rq[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"tambahan'+cobayah+'"'+")'></span></div></div></div>");
						};

						 }else{
							$('#jajal').after("<div class='row' id='tambahan'><div class='col-md-10'><div class='form-inline'><input name='rq[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"'+"tambahan"+'"'+")'></span></div></div></div>");
					};

					$('.tglPelaksanaan').datepicker({
							format: 'yyyy-mm-dd'
					});

			});

				$('.tglPelaksanaan').datepicker({
							format: 'yyyy-mm-dd'
					});

			$('#try').click(function(){
				var cobaz = $("#example-getting-started option:selected").map(function () {return this.value;}).get().join(" # ");
				alert(cobaz);
			});

			});

			function removeElement(id) {
					$('#'+id).remove();
			}




			function getValue(){
				var x=document.getElementById("example-getting-started");
				for (var i = 0; i < x.options.length; i++) {
					 if(x.options[i].selected){
								if (x.options[i].value=='lainnya') {
									$('#fr-accurateLainnya').show();
								}else{
									$('#fr-accurateLainnya').hide();
								};
						}
				}
			}

			</script>
		</body>
	</html>
