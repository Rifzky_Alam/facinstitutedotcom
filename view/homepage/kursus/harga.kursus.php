<!DOCTYPE html>
	<html lang="zxx" class="no-js">
	<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="thumbnailUrl" content="<?= $data->base_url ?>_caramel/assets/img/g44508.png" />
    <meta name="description" content="" />
    <meta content="" />
    <meta name="keywords" content="Training,Akuntansi,ACCURATE,Jasa">
    <meta name="author" content="Rifzky Alam, Dino Damara">
    <title>FAC Institute</title>
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
    <!--
      CSS
      ============================================= -->
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/linearicons.css">
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/bootstrap.css">
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/magnific-popup.css">
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/nice-select.css">
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/animate.min.css">
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/owl.carousel.css">
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/main.css">
			<style media="screen">
			.banner-content h1 {
			    font-size: 38px;
			}
			.nav-menu a {
					padding: 15px 8px 1px 8px;
					font-size: 16px;
					font-weight: 500;
			}
			.nav-menu ul li a {
					font-size: 14px;
			}

			.menu-active {
			  border-bottom: 5px solid #df003a;
			}

			.navbar {
			    padding: 0.2rem 1rem;
			}


			.circle {
			  padding: 13px 20px;
			  border-radius: 50%;
			  background-color: #f74e4e;
			  color: #fff;
			  max-height: 50px;
			  z-index: 2;
			}

			.how-it-works.row .col-2 {
			  align-self: stretch;
			}
			.how-it-works.row .col-2::after {
			  content: "";
			  position: absolute;
			  border-left: 3px solid #ED8D8D;
			  z-index: 1;
			}
			.how-it-works.row .col-2.bottom::after {
			  height: 50%;
			  left: 50%;
			  top: 50%;
			}
			.how-it-works.row .col-2.full::after {
			  height: 100%;
			  left: calc(50% - 3px);
			}
			.how-it-works.row .col-2.top::after {
			  height: 50%;
			  left: 50%;
			  top: 0;
			}

			.timeline div {
			  padding: 0;
			  height: 40px;
			}
			.timeline hr {
			  border-top: 3px solid #ED8D8D;
			  margin: 0;
			  top: 17px;
			  position: relative;
			}
			.timeline .col-2 {
			  display: flex;
			  overflow: hidden;
			}
			.timeline .corner {
			  border: 3px solid #ED8D8D;
			  width: 100%;
			  position: relative;
			  border-radius: 15px;
			}
			.timeline .top-right {
			  left: 50%;
			  top: -50%;
			}
			.timeline .left-bottom {
			  left: -50%;
			  top: calc(50% - 3px);
			}
			.timeline .top-left {
			  left: -50%;
			  top: -50%;
			}
			.timeline .right-bottom {
			  left: 50%;
			  top: calc(50% - 3px);
			}


/* tooltip */
			.tooltip {
		      position: relative;
		      display: inline-block;
		      border-bottom: 1px dotted black;
		  }

		  .tooltip .tooltiptext {
		      visibility: hidden;
		      width: 200px;
		      background-color: black;
		      color: #fff;
		      text-align: center;
		      border-radius: 6px;
		      padding: 10px;

		      /* Position the tooltip */
		      position: absolute;
		      z-index: 1;
		      bottom: 100%;
		      left: 50%;
		      margin-left: -90px;
		  }

		  .tooltip:hover .tooltiptext {
		      visibility: visible;
		  }

			@media (max-width: 480px) {
				#mobile-nav-toggle {
					top: 35px;
				}
			}

			@media (max-width: 360px) {
				#mobile-nav-toggle {
					top: 60px;
				}
			}

			</style>
      <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
		</head>
		<body>

    <?php include_once $data->homedir.'view/homepage/kursus/header.kursus.php'; ?>

		<section class="price-area section-gap mt-80" id="price">
						<div class="container">
							<div class="row d-flex justify-content-center">
								<div class="menu-content pb-60 col-lg-8">
									<div class="title text-center">
										<h1 class="mb-10">TARIF TRAINING REGULER & PRIVAT</h1>
										<p> (Lokasi di tempat kami) .</p>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-3">
									<div class="single-price">
										<div class="top-sec d-flex justify-content-between">
											<div class="top-left">
												<!-- <h4>Accurate Standar</h4>
												<p>Per Perserta</p> -->
											</div>
											<div class="top-right">
												<h1>Accurate Standar</h1>
											</div>
										</div>
										<div class="bottom-sec">
											<p style="font-weight:bold">
												Waktu
											</p>
										</div>
										<div class="bottom-sec">
											<p style="font-weight:bold">
												Durasi
											</p>
										</div>
										<div class="bottom-sec">
											<p style="font-weight:bold">
												Materi
											</p>
										</div>

									</div>
								</div>
								<div class="col-lg-4">
									<div class="single-price">
										<div class="top-sec d-flex justify-content-between">
											<div class="top-left">
												<h4>Kelas Reguler</h4>
											</div>
											<div class="top-right">
												<h1>Rp 1.5 </h1>
												<p>Juta / hari</p>
											</div>
										</div>
										<div class="bottom-sec">
											<p>
												Senin, Rabu, Jumat
											</p>
										</div>
										<div class="bottom-sec">
											<p>
												3 hari @6-jam(09:00 s/d 16:00)
											</p>
										</div>
										<div class="end-sec">
											<ul style="list-style-type: circle;">
												<li>Pengenalan Modul Accurate</li>
												<li>Set Up  data awal (Basic & Mahir))</li>
												<li>Latihan kasus Operasional Perusahaan (All Modul)</li>
											</ul>
											<a href="https://bakatcendekia.wordpress.com/training-accurate/kelas/" class="primary-btn price-btn mt-20 text-whitr">Daftar Sekarang</a>
										</div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="single-price">
										<div class="top-sec d-flex justify-content-between">
											<div class="top-left">
												<h4>Kelas Privat</h4>
											</div>
											<div class="top-right">
												<h1>Rp 2 </h1>
												<p>Juta / hari</p>
											</div>
										</div>
										<div class="bottom-sec">
											<p>
												Flexible / sesuai kesepakatan
											</p>
										</div>
										<div class="bottom-sec">
											<p>
												3 hari @6-jam(09:00 s/d 16:00)
											</p>
										</div>
										<div class="end-sec">
											<ul style="list-style-type: circle;">
												<li>Sama seperti Kelas Reguler</li>
												<li>atau sesuai kebutuhan peserta</li>
												<li>atau menyelesaikan kasus Perusahaan</li>
											</ul>
											<a href="https://bakatcendekia.wordpress.com/training-accurate/kelas/" class="primary-btn price-btn mt-20 text-whitr">Daftar Sekarang</a>
										</div>
									</div>
								</div>

							</div>


							<div class="row">
								<div class="col-lg-3">
									<div class="single-price">
										<div class="top-sec d-flex justify-content-between">
											<div class="top-left">
											</div>
											<div class="top-right">
												<h1>PAKET PROGRAM KHUSUS</h1>
												<p>Rp.   750.000,-Per Paket / peserta</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-8">
									<div class="single-price">
										<div class="top-sec d-flex justify-content-between">
										<div class="end-sec">
											<p>Jenis Training lainnya :</p>
											<ul style="list-style-type: circle;">
												<li>ACCURATE DELUXE (1-hari @6jam)</li>
												<li>ACCURATE ENTERPRISE (1-hari @6jam)</li>
												<li>PPN E-faktur import dari ACCURATE (1-hari @6jam)</li>
												<li>PPh 23 import dari ACCURATE (1-hari @6jam)</li>
											</ul>
											<p style="font-style:italic">* Syarat point 1-4,  Wajib sudah mengerti menggunakan Accurate Standar.</p>
											<p style="font-weight:bold">Program Training DASAR-DASAR AKUNTANSI –> Durasi  2-hari per-sesi @6jam (09:00 – 16:00)</p>
											<a href="https://bakatcendekia.wordpress.com/training-accurate/kelas/" class="primary-btn price-btn mt-20 text-whitr">Daftar Sekarang</a>
										</div>
									</div>
								</div>


							</div>

						</div>
					</section>
					<!-- End price Area -->


      <?php include_once $data->homedir.'view/homepage/footer.homepage.php'; ?>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/jquery-2.2.4.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/bootstrap.min.js"></script>
      <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/easing.min.js"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/hoverIntent.js"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/superfish.min.js"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.ajaxchimp.min.js"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.magnific-popup.min.js"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/owl.carousel.min.js"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.sticky.js"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.nice-select.min.js"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/parallax.min.js"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/mail-script.js"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/main.js"></script>
      <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
			<script>
			AOS.init();
			$('.test1').click(function() {
			    var sectionTo = $(this).attr('href');
			    $('html, body').animate({
			      scrollTop: $(sectionTo).offset().top
			    }, 1000);
			});
			</script>
			<script type="text/javascript" src=<?php echo "'".getBaseUrl()."css/datepicker/js/bootstrap-datepicker.js'"; ?>></script>
			<script type="text/javascript">

			$(document).ready(function(){
				var apakah = 0;
				$('#cobaa').datepicker({
					 format: "yyyy-mm-dd"
				});
				$('#fr-accurateLainnya').hide();

					$('[data-toggle="popover"]').popover();


				$('#tambahTombol').click(function(){

						var cobayah = $('.tglPelaksanaan').length;
						var iseng = "tambahan" + cobayah;
						if ($('#tambahan').length){

						if (cobayah==2) {
								$('#tambahan').after("<div class='row' id='"+iseng+"'><div class='col-md-10'><div class='form-inline'><input name='rq[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"'+iseng+'"'+")'></span></div></div></div>");
						}else{
								var okeh = cobayah-1;
								var iseng = "tambahan" + okeh;
								$("#"+iseng).after("<div class='row' id='tambahan"+cobayah+"'><div class='col-md-10'><div class='form-inline'><input name='rq[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"tambahan'+cobayah+'"'+")'></span></div></div></div>");
						};

						 }else{
							$('#jajal').after("<div class='row' id='tambahan'><div class='col-md-10'><div class='form-inline'><input name='rq[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"'+"tambahan"+'"'+")'></span></div></div></div>");
					};

					$('.tglPelaksanaan').datepicker({
							format: 'yyyy-mm-dd'
					});

			});

				$('.tglPelaksanaan').datepicker({
							format: 'yyyy-mm-dd'
					});

			$('#try').click(function(){
				var cobaz = $("#example-getting-started option:selected").map(function () {return this.value;}).get().join(" # ");
				alert(cobaz);
			});

			});

			function removeElement(id) {
					$('#'+id).remove();
			}




			function getValue(){
				var x=document.getElementById("example-getting-started");
				for (var i = 0; i < x.options.length; i++) {
					 if(x.options[i].selected){
								if (x.options[i].value=='lainnya') {
									$('#fr-accurateLainnya').show();
								}else{
									$('#fr-accurateLainnya').hide();
								};
						}
				}
			}

			</script>
		</body>
	</html>
