<header class="fixed-top" id="header">
	<?php include_once $data->homedir.'view/homepage/parentheader.homepage.php'; ?>
	<div class="container">
		<div class="row align-items-center justify-content-between d-flex">
			<div id="logo">

					<a href="index.html"><img style="padding-top:10px" width="200px" src="<?= $data->base_url ?>images/homepage2019/facnew.png" alt="FAC-Institute" title="" /></a>

			</div>

			<nav id="nav-menu-container">
				<ul class="nav-menu">
					<?php if ($data->judul=='kursusaccurate'): ?>
						<li class="menu-active">
							<a href="<?= $data->base_url ?>kursus">
							BERANDA
							</a>
						</li>
						<li>
							<a href="<?= $data->base_url ?>kursus/jadwal">
							JADWAL KURSUS
							</a>
						</li>
						<li>
							<a href="<?= $data->base_url ?>kursus/info-biaya">
							BIAYA KURSUS
							</a>
						</li>
            <li>
              <a href="<?= $data->base_url ?>kursus/info-order">
              CARA ORDER
              </a>
            </li>

					<?php elseif($data->judul=='jadwalkursusaccurate'): ?>
            <li>
							<a href="<?= $data->base_url ?>kursus">
							BERANDA
							</a>
						</li>
						<li class="menu-active">
							<a href="<?= $data->base_url ?>kursus/jadwal">
							JADWAL KURSUS
							</a>
						</li>
						<li>
							<a href="<?= $data->base_url ?>kursus/info-biaya">
							BIAYA KURSUS
							</a>
						</li>
            <li>
              <a href="<?= $data->base_url ?>kursus/info-order">
              CARA ORDER
              </a>
            </li>

					<?php elseif($data->judul=='hargakursusaccurate'): ?>
            <li>
							<a href="<?= $data->base_url ?>kursus">
							BERANDA
							</a>
						</li>
						<li>
							<a href="<?= $data->base_url ?>kursus/jadwal">
							JADWAL KURSUS
							</a>
						</li>
						<li class="menu-active">
							<a href="<?= $data->base_url ?>kursus/info-biaya">
							BIAYA KURSUS
							</a>
						</li>
            <li>
              <a href="<?= $data->base_url ?>kursus/info-order">
              CARA ORDER
              </a>
            </li>
<?php elseif($data->judul=='orderkursusaccurate'): ?>
            <li>
							<a href="<?= $data->base_url ?>kursus">
							BERANDA
							</a>
						</li>
						<li>
							<a href="<?= $data->base_url ?>kursus/jadwal">
							JADWAL KURSUS
							</a>
						</li>
						<li>
							<a href="<?= $data->base_url ?>kursus/info-biaya">
							BIAYA KURSUS
							</a>
						</li>
            <li class="menu-active">
              <a href="<?= $data->base_url ?>kursus/info-order">
              CARA ORDER
              </a>
            </li>


        <?php else: ?>
          <li>
            <a href="<?= $data->base_url ?>kursus">
            BERANDA
            </a>
          </li>
          <li>
            <a href="<?= $data->base_url ?>kursus/jadwal">
            JADWAL KURSUS
            </a>
          </li>
          <li>
            <a href="<?= $data->base_url ?>kursus/info-biaya">
            BIAYA KURSUS
            </a>
          </li>
          <li>
            <a href="<?= $data->base_url ?>kursus/info-order">
            CARA ORDER
            </a>
          </li>
					<?php endif ?>

				</ul>
			</nav>
		</div>
	</div>
</header>
