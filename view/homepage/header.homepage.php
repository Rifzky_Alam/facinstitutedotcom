<header id="header" class="fixed-top" id="home">
	<div class="container">
		<div class="row align-items-center justify-content-between d-flex">
			<div id="logo">
				<a href="<?= $data->base_url ?>"><img style="padding-top:10px" width="200px" src="<?= $data->base_url ?>images/homepage2019/facnew.png" alt="" title="" /></a>
			</div>
			<nav id="nav-menu-container">

				<ul class="nav-menu">

					<?php if ($data->judul=='home'): ?>
						<li class="menu-active" ><a  href="<?= $data->base_url ?>">BERANDA</a></li>
						<li class="menu-has-children">
							<a href="#">Layanan</a>
							<ul>
								<li><a href="<?= $data->base_url ?>training">Training Accurate</a></li>
								<li><a href="<?= $data->base_url ?>accountingservice">Accounting Service</a></li>
								<li><a href="<?= $data->base_url ?>kursus">Kursus Accurate</a></li>
							</ul>
						</li>
						<li class="menu-has-children" >
							<a href="#">Bantuan & Support</a>
							<ul>
								<!--<li><a href="<?= $data->base_url ?>tutorial">Tutorial</a></li>-->
								<!--<li><a href="<?= $data->base_url ?>artikel">Artikel</a></li>-->
									<li><a href="https://fac-institute.com/blogs/">Blogs</a></li>
								<li><a href="<?= $data->base_url ?>download">Download</a></li>
								<!--<li><a href="https://fac-institute.com/premium-support">Premium Support</a></li>-->
							</ul>
						</li>
						<!-- <li><a href="#blog">Blog</a></li> -->
						<li class="menu-has-children">
							<a href="#">Hubungi Kami</a>
							<ul>
								<li><a href="<?= $data->base_url.'me/location' ?>">Alamat FAC</a></li>
								<!--<li><a href="#">Order Layanan</a></li>-->
							</ul>
						</li>
						<li class="menu-has-children"><a href="#">Team FAC</a>
							<ul>
								<li><a href="<?= $data->base_url ?>facteam">Trainers</a></li>
								<li><a href="<?= $data->base_url ?>login">Login</a></li>
							</ul>
						</li>
						<li><a class="bg-primary text-white" href="<?= $data->base_url ?>daftar-training/">Daftar Now!</a></li>




					<?php elseif($data->judul=='DownloadAccurate'): ?>

						<li><a  href="<?= $data->base_url ?>">BERANDA</a></li>
						<li class="menu-has-children">
							<a href="#">Layanan</a>
							<ul>
								<li><a href="<?= $data->base_url ?>training">Training Accurate</a></li>
								<li><a href="<?= $data->base_url ?>accountingservice">Accounting Service</a></li>
								<li><a href="<?= $data->base_url ?>kursus">Kursus Accurate</a></li>
							</ul>
						</li>
						<li class="menu-has-children menu-active" >
							<a href="#">Bantuan & Support</a>
							<ul>
								<!--<li><a href="<?= $data->base_url ?>tutorial">Tutorial</a></li>-->
								<!--<li><a href="<?= $data->base_url ?>artikel">Artikel</a></li>-->
									<li><a href="https://fac-institute.com/blogs/">Blogs</a></li>
								<li><a href="<?= $data->base_url ?>download">Download</a></li>
								<!--<li><a href="https://fac-institute.com/premium-support">Premium Support</a></li>-->
							</ul>
						</li>
						<!-- <li><a href="#blog">Blog</a></li> -->
						<li class="menu-has-children">
							<a href="#">Hubungi Kami</a>
							<ul>
								<li><a href="<?= $data->base_url.'me/location' ?>">Alamat FAC</a></li>
								<!--<li><a href="#">Order Layanan</a></li>-->
							</ul>
						</li>
						<li class="menu-has-children"><a href="#">Team FAC</a>
							<ul>
								<li><a href="<?= $data->base_url ?>facteam">Trainers</a></li>
								<li><a href="<?= $data->base_url ?>login">Login</a></li>
							</ul>
						</li>
						<li><a class="bg-primary text-white" href="<?= $data->base_url ?>daftar-training/">Daftar Now!</a></li>


						<?php elseif($data->judul=='mylocation'): ?>

							<li><a  href="<?= $data->base_url ?>">BERANDA</a></li>
							<li class="menu-has-children">
								<a href="#">Layanan</a>
								<ul>
									<li><a href="<?= $data->base_url ?>training">Training Accurate</a></li>
									<li><a href="<?= $data->base_url ?>accountingservice">Accounting Service</a></li>
									<li><a href="<?= $data->base_url ?>kursus">Kursus Accurate</a></li>
								</ul>
							</li>
							<li class="menu-has-children" >
								<a href="#">Bantuan & Support</a>
								<ul>
									<!--<li><a href="<?= $data->base_url ?>tutorial">Tutorial</a></li>-->
									<!--<li><a href="<?= $data->base_url ?>artikel">Artikel</a></li>-->
										<li><a href="https://fac-institute.com/blogs/">Blogs</a></li>
									<li><a href="<?= $data->base_url ?>download">Download</a></li>
									<!--<li><a href="https://fac-institute.com/premium-support">Premium Support</a></li>-->
								</ul>
							</li>
							<!-- <li><a href="#blog">Blog</a></li> -->
							<li class="menu-has-children menu-active">
								<a href="#">Hubungi Kami</a>
								<ul>
									<li><a href="<?= $data->base_url.'me/location' ?>">Alamat FAC</a></li>
									<!--<li><a href="#">Order Layanan</a></li>-->
								</ul>
							</li>
							<li class="menu-has-children"><a href="#">Team FAC</a>
								<ul>
									<li><a href="<?= $data->base_url ?>facteam">Trainers</a></li>
									<li><a href="<?= $data->base_url ?>login">Login</a></li>
								</ul>
							</li>
							<li><a class="bg-primary text-white" href="<?= $data->base_url ?>daftar-training/">Daftar Now!</a></li>

							<?php else: ?>
								<li><a  href="<?= $data->base_url ?>">BERANDA</a></li>
								<li class="menu-has-children">
									<a href="#">Layanan</a>
									<ul>
										<li><a href="<?= $data->base_url ?>training">Training Accurate</a></li>
										<li><a href="<?= $data->base_url ?>accountingservice">Accounting Service</a></li>
										<li><a href="<?= $data->base_url ?>kursus">Kursus Accurate</a></li>
									</ul>
								</li>
								<li class="menu-has-children" >
									<a href="#">Bantuan & Support</a>
									<ul>
										<!--<li><a href="<?= $data->base_url ?>tutorial">Tutorial</a></li>-->
										<!--<li><a href="<?= $data->base_url ?>artikel">Artikel</a></li>-->
											<li><a href="https://fac-institute.com/blogs/">Blogs</a></li>
										<li><a href="<?= $data->base_url ?>download">Download</a></li>
										<!--<li><a href="https://fac-institute.com/premium-support">Premium Support</a></li>-->
									</ul>
								</li>
								<!-- <li><a href="#blog">Blog</a></li> -->
								<li class="menu-has-children">
									<a href="#">Hubungi Kami</a>
									<ul>
										<li><a href="<?= $data->base_url.'me/location' ?>">Alamat FAC</a></li>
										<!--<li><a href="#">Order Layanan</a></li>-->
									</ul>
								</li>
								<li class="menu-has-children"><a href="#">Team FAC</a>
									<ul>
										<li><a href="<?= $data->base_url ?>facteam">Trainers</a></li>
										<li><a href="<?= $data->base_url ?>login">Login</a></li>
									</ul>
								</li>
								<li><a class="bg-primary text-white" href="<?= $data->base_url ?>daftar-training/">Daftar Now!</a></li>
					<?php endif ?>

				</ul>


			</nav>
		</div>
	</div>
</header>
