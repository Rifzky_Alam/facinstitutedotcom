<!DOCTYPE html>
<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="<?= $data->base_url ?>images/homepage2019/elements/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="Rifzky Alam, Dino Damara">
		<!-- Meta Description -->
		<meta name="description" content="<?= $data->pagedesc ?>">
		<!-- Meta Keyword -->
		<meta name="keywords" content="<?= $data->pagekeywords ?>">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title><?= $data->title ?></title>

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
		<!--
			CSS
			============================================= -->
			<link rel="shortcut icon" href='https://fac-institute.com/_caramel/assets/img/g25992_9_kUd_icon.ico'>
		  <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/linearicons.css">
			<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/owl.carousel.css">
			<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/font-awesome.min.css">
			<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/nice-select.css">
			<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/magnific-popup.css">
			<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/bootstrap.css">
			<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/main.css">
		<style media="screen">
			.banner-content h1 {
			font-size: 38px;
			}
			.nav-menu a {
			padding: 15px 8px 1px 8px;
			font-size: 16px;
			font-weight: 500;
			}
			.nav-menu ul li a {
			font-size: 14px;
			}
			.menu-active {
			border-bottom: 5px solid #df003a;
			}
			.navbar {
			padding: 0.2rem 1rem;
			}

			ul.timeline {
			    list-style-type: none;
			    position: relative;
					font-size: 16px;
			}

			ul.timeline a {
				font-weight: bold;
			}
			ul.timeline:before {
			    content: ' ';
			    background: #d4d9df;
			    display: inline-block;
			    position: absolute;
			    left: 29px;
			    width: 2px;
			    height: 100%;
			    z-index: 400;
			}
			ul.timeline > li {
			    margin: 20px 0;
			    padding-left: 60px;
			}
			ul.timeline > li:before {
			    content: ' ';
			    background: white;
			    display: inline-block;
			    position: absolute;
			    border-radius: 50%;
			    border: 3px solid #22c0e8;
			    left: 20px;
			    width: 20px;
			    height: 20px;
			    z-index: 400;
			}

			@media (max-width: 480px) {
				#mobile-nav-toggle {
					top: 35px;
				}
			}

		</style>
	</head>
	<body>
		<?php include_once $data->homedir.'view/homepage/accountingservice/header.accsvc.php'; ?>
		<!-- <section style='background-color: rgba(255,255,255,0);
		text-align: center;
    background-image: url(https://cpssoft.com/accurate-online/wp-content/uploads/sites/3/2018/10/BG-fitur-AOL-min.png);
    background-position: center bottom;
    background-repeat: no-repeat;
    padding-top: 120px;
    padding-bottom: 80px;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;'>
			<div class="container">
				<div class="row d-flex align-items-center justify-content-center">
					<div class="col-lg-6 no-padding about-right">
						<h1 class="text-white" style="font-size:34px">Cara Pemesanan</h1>
					</div>
			</div>
			</div>
		</section> -->
		<!-- End banner Area -->
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="menu-content mt-80 pb-10 col-lg-10">
					<div class="title text-center">
						<h1 class="mt-50 mb-10">HOW TO ORDER?</h1>
						<!-- <p style="font-size:18px">caranya mudah kok!</p> -->
					</div>
				</div>
			</div>
		</div>


		<div class="container mt-2 mb-5">
			<div class="row">
				<div class="col-md-6 offset-md-3">
					<!-- <h4>Latest News</h4> -->
					<ul class="timeline">
						<li>
							<a target="_blank" href="https://www.totoprayogo.com/#">Call Admin</a>
							<!-- <a href="#" class="float-right">21 March, 2014</a> -->
							<p>Hubungi marketing kami di no. telp : <a href="tel://081290083983" title="Sentuh untuk menelpon"><b>081290083983</b></a> atau <a href="tel://081219115005" title="Sentuh untuk menelpon"><b>081219115005</b></a>. Selanjutnya marketing kami akan melakukan penjadwalan pekerjaan Accounting Service Anda sesuai dengan kesepakatan bersama.
						  </p>
						</li>
						<li>
							<a href="#">Assessment</a>
							<!-- <a href="#" class="float-right">4 March, 2014</a> -->
							<p>Penilaian jumlah transaksi</p>
						</li>
						<li>
							<a href="#">Penentuan Tarif</a>
							<!-- <a href="#" class="float-right">1 April, 2014</a> -->
							<p style="margin-bottom:-1px">Penentuan tarif accounting service</p>
							<p style="font-size:12px;font-style:italic;">*Biaya Accounting Service bersifat tentatif, tergantung banyaknya data perusahaan</p>
						</li>
						<li>
							<a href="#">Pembayaran</a>
							<!-- <a href="#" class="float-right">1 April, 2014</a> -->
							<p>Pembayaran melalui transfer antar bank online <br><span style="font-style:italic;font-size:12px">*pembayaran dilakukan sebelum pekerjaan accounting service</span></p>
						</li>
						<li>
							<a href="#">Accounting Service</a>
							<!-- <a href="#" class="float-right">1 April, 2014</a> -->
							<p>Perkejaan accounting service sesuai jadwal yang telah disepakati</p>
						</li>

					</ul>
				</div>
			</div>
		</div>



<?php include_once $data->homedir.'view/homepage/footer.homepage.php'; ?>

		<!-- End footer Area -->
		<script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/jquery-2.2.4.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/bootstrap.min.js"></script>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/easing.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/hoverIntent.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/superfish.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.ajaxchimp.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.magnific-popup.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/owl.carousel.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.sticky.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.nice-select.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/parallax.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/mail-script.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/main.js"></script>
	</body>
</html>
