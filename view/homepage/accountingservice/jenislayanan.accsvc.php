	<!DOCTYPE html>
	<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="<?= $data->base_url ?>images/homepage2019/elements/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="Rifzky Alam, Dino Damara">
		<!-- Meta Description -->
		<meta name="description" content="<?= $data->pagedesc ?>">
		<!-- Meta Keyword -->
		<meta name="keywords" content="<?= $data->pagekeywords ?>">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title><?= $data->title ?></title>

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
			<!--
			CSS
			============================================= -->
			<link rel="shortcut icon" href='https://fac-institute.com/_caramel/assets/img/g25992_9_kUd_icon.ico'>
			<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/linearicons.css">
			<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/owl.carousel.css">
			<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/font-awesome.min.css">
			<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/nice-select.css">
			<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/magnific-popup.css">
			<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/bootstrap.css">
			<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/main.css">
			<style media="screen">
			.banner-content h1 {
			    font-size: 38px;
			}
			.nav-menu a {
					padding: 15px 8px 1px 8px;
					font-size: 16px;
					font-weight: 500;
			}
			.nav-menu ul li a {
					font-size: 14px;
			}

			.menu-active {
			  border-bottom: 5px solid #df003a;
			}

			.navbar {
			    padding: 0.2rem 1rem;
			}

			@media (max-width: 480px) {
				#mobile-nav-toggle {
					top: 35px;
				}
			}


			</style>
      <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
		</head>
		<body>

			<?php include_once $data->homedir.'view/homepage/accountingservice/header.accsvc.php'; ?>


			<section style='background: url("") ;background-position: center center;background-repeat: no-repeat;padding-top:120px;padding-right:30px;padding-bottom:20px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
				<div class="container-fluid">
					<div class="row justify-content-center align-items-center">
						<div class="col-lg-12 text-center" style="max-width:1200px">
							<h1 style="font-size:40px" class="mb-10">Audit Internal</h1>
								<img class="img-fluid mt-20 mb-40" width="500" data-aos="fade-up" src="<?= $data->base_url ?>images/homepage2019/64683.png" alt="Audit FAC">
							<p style="font-size:20px;font-weight:300;line-height:32px">
								Audit internal berfungsi sebagai alat kontrol yang menjamin keberlangsungan perusahaan sesuai perencanaan dan tujuan. Untuk itu maka perusahaan memerlukan keakurasian pencatatan keuangan dan akuntansi sesuai alat bukti dokumen transaksi dan tujuan transaksi. Dengan data yang akurat, perusahaan bisa membuat keputusan strategis apakah tetap melanjutkan perencanaan atau mengevaluasinya.
Kami menyediakan jasa audit internal atas pencatatan accurate yang telah diinput oleh karyawan Anda agar perusahaan bisa menghasilkan keputusan strategis berdasarkan data yang akurat.
							</p>
						</div>
					</div>
				</div>
			</section>


<hr>

			<section style='background: url("") ;background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:20px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
				<div class="container-fluid">
					<div class="row justify-content-center align-items-center">
						<div class="col-lg-6" style="max-width:500px">
							<h1 style="font-size:40px" class="mb-10">Entry Data</h1>
							<p class="text-justify" style="font-size:18px;line-height:28px">
								Kami menyediakan jasa entry data transaksi keuangan Anda di Accurate jika Anda belum memiliki karyawan pembukuan, atau karyawan Anda sibuk sehingga tidak sempat mencatat pembukuan sendiri.Entri data meliputi mempersiapkan dan mengorganisasikan data, memeriksa akurasi dokumen, memperbarui data, menghapus berkas yang tidak perlu, menyalin informasi ke format elektronik, memindai dokumen, dan tugas administrasi umum.
							</p>
						</div>
						<div class="col-lg-6 mt-40">
							<img class="img-fluid" data-aos="fade-up" width="100%" src="<?= $data->base_url ?>images/homepage2019/64682.png" alt="Entry Data FAC">
						</div>
					</div>
				</div>
			</section>


			<hr>

						<section style='background: url("") ;background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:20px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
							<div class="container-fluid">
								<div class="row justify-content-center align-items-center">
									<div class="col-lg-6 mt-40">
										<img class="img-fluid" data-aos="fade-up" width="100%" src="<?= $data->base_url ?>images/homepage2019/64684.png" alt="Review Data FAC Institute">
									</div>
									<div class="col-lg-6" style="max-width:500px">
										<h1 style="font-size:40px" class="mb-10">Review Data</h1>
										<p class="text-justify" style="font-size:18px;line-height:28px">
											Review data adalah kegiatan memverifikasi data. Review data lebih rinci (berfungsi agar data yang Anda input di accurate sudah sesuai kaidah akuntansi dan perpajakan).
										</p>
									</div>

								</div>
							</div>
						</section>
<hr>


	    <?php include_once $data->homedir.'view/homepage/footer.homepage.php'; ?>

			<script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/jquery-2.2.4.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/bootstrap.min.js"></script>
			<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
  			<script src="<?= $data->base_url ?>assets/homepage2019/js/easing.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/hoverIntent.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/superfish.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.ajaxchimp.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.magnific-popup.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/owl.carousel.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.sticky.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.nice-select.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/parallax.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/mail-script.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/main.js"></script>
			<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
			<script>
			AOS.init();
			$('.test1').click(function() {
			    var sectionTo = $(this).attr('href');
			    $('html, body').animate({
			      scrollTop: $(sectionTo).offset().top
			    }, 1000);
			});
			</script>
		</body>
	</html>
