<header class="fixed-top" id="header">
	<?php include_once $data->homedir.'view/homepage/parentheader.homepage.php'; ?>
	<div class="container">
		<div class="row align-items-center justify-content-between d-flex">
			<div id="logo">

					<a href="<?= $data->base_url ?>accountingservice"><img style="padding-top:10px" width="200px" src="<?= $data->base_url ?>images/homepage2019/facnew.png" alt="FAC-Institute" title="" /></a>

			</div>

			<nav id="nav-menu-container">
				<ul class="nav-menu">
					<?php if ($data->judul=='dasbor'): ?>
						<li class="menu-active">
							<a href="<?= $data->base_url ?>accountingservice">
							BERANDA
							</a>
						</li>
						<li>
							<a href="<?= $data->base_url ?>accountingservice/carapesan">
							CARA PEMESANAN
							</a>
						</li>
						<li>
							<a href="<?= $data->base_url ?>accountingservice/jenislayanan">
							JENIS LAYANAN
							</a>
						</li>
					<?php elseif($data->judul=='carapesan'): ?>
						<li>
							<a href="<?= $data->base_url ?>accountingservice">
							BERANDA
							</a>
						</li>
						<li class="menu-active">
							<a href="<?= $data->base_url ?>accountingservice/carapesan">
							CARA PEMESANAN
							</a>
						</li>
						<li>
							<a href="<?= $data->base_url ?>accountingservice/jenislayanan">
							JENIS LAYANAN
							</a>
						</li>
					<?php elseif($data->judul=='jenislayanan'): ?>
						<li>
							<a href="<?= $data->base_url ?>accountingservice">
							BERANDA
							</a>
						</li>
						<li>
							<a href="<?= $data->base_url ?>accountingservice/carapesan">
							CARA PEMESANAN
							</a>
						</li>
						<li class="menu-active">
							<a href="<?= $data->base_url ?>accountingservice/jenislayanan">
							JENIS LAYANAN
							</a>
						</li>
					<?php else: ?>
						<li>
							<a href="<?= $data->base_url ?>accountingservice">
							BERANDA
							</a>
						</li>
						<li>
							<a href="<?= $data->base_url ?>accountingservice/carapesan">
							CARA PEMESANAN
							</a>
						</li>
						<li>
							<a href="<?= $data->base_url ?>accountingservice/jenislayanan">
							JENIS LAYANAN
							</a>
						</li>
					<?php endif ?>

				</ul>
			</nav>
		</div>
	</div>
</header>
