	<!DOCTYPE html>
	<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="<?= $data->base_url ?>images/homepage2019/elements/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="Rifzky Alam, Dino Damara">
		<!-- Meta Description -->
		<meta name="description" content="<?= $data->pagedesc ?>">
		<!-- Meta Keyword -->
		<meta name="keywords" content="<?= $data->pagekeywords ?>">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title><?= $data->title ?></title>

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
			<!--
			CSS
			============================================= -->
			<link rel="shortcut icon" href='https://fac-institute.com/_caramel/assets/img/g25992_9_kUd_icon.ico'>
			<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/linearicons.css">
			<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/owl.carousel.css">
			<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/font-awesome.min.css">
			<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/nice-select.css">
			<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/magnific-popup.css">
			<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/bootstrap.css">
			<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/main.css">
			<style media="screen">
			.banner-content h1 {
			    font-size: 38px;
			}
			.nav-menu a {
					padding: 15px 8px 1px 8px;
					font-size: 16px;
					font-weight: 500;
			}
			.nav-menu ul li a {
					font-size: 14px;
			}

			.menu-active {
			  border-bottom: 5px solid #df003a;
			}

			.navbar {
			    padding: 0.2rem 1rem;
			}


			.circle {
			  padding: 13px 20px;
			  border-radius: 50%;
			  background-color: #f74e4e;
			  color: #fff;
			  max-height: 50px;
			  z-index: 2;
			}

			.how-it-works.row .col-2 {
			  align-self: stretch;
			}
			.how-it-works.row .col-2::after {
			  content: "";
			  position: absolute;
			  border-left: 3px solid #ED8D8D;
			  z-index: 1;
			}
			.how-it-works.row .col-2.bottom::after {
			  height: 50%;
			  left: 50%;
			  top: 50%;
			}
			.how-it-works.row .col-2.full::after {
			  height: 100%;
			  left: calc(50% - 3px);
			}
			.how-it-works.row .col-2.top::after {
			  height: 50%;
			  left: 50%;
			  top: 0;
			}

			.timeline div {
			  padding: 0;
			  height: 40px;
			}
			.timeline hr {
			  border-top: 3px solid #ED8D8D;
			  margin: 0;
			  top: 17px;
			  position: relative;
			}
			.timeline .col-2 {
			  display: flex;
			  overflow: hidden;
			}
			.timeline .corner {
			  border: 3px solid #ED8D8D;
			  width: 100%;
			  position: relative;
			  border-radius: 15px;
			}
			.timeline .top-right {
			  left: 50%;
			  top: -50%;
			}
			.timeline .left-bottom {
			  left: -50%;
			  top: calc(50% - 3px);
			}
			.timeline .top-left {
			  left: -50%;
			  top: -50%;
			}
			.timeline .right-bottom {
			  left: 50%;
			  top: calc(50% - 3px);
			}

			#myh2 {
				font-size: 50px;
			}

			#myp {
				font-size: 32px;
			}

			#mybanner {
				height:600px;
			}

			#generic-banner{
				background-color: rgba(255,255,255,0);
				text-align: center;
				background: linear-gradient(rgba(255, 255, 255, 0.3),rgba(255, 255, 255, 0.3)),url(http://fac-institute.com/images/homepage2019/DSC100784171.jpg);
				padding-top: 50px;
				padding-bottom: 50px;
				height:600px;
				-webkit-background-size:cover;
			  -moz-background-size:cover;
			  -o-background-size:cover;
			  background-size:cover;
			}


			@media (max-width: 480px) {
				#myh2 {
					font-size: 22px;
				}
				#mybanner {
					height:300px;
				}
				#myimg{
				  display: none;
				}
				#generic-banner{
					height: 350px;
				}
				#mobile-nav-toggle {
					top: 35px;
				}
			}


			</style>
      <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
		</head>
		<body>

		<?php include_once $data->homedir.'view/homepage/accountingservice/header.accsvc.php'; ?>

			<section id="generic-banner">

			<div class="container">
				<div id="mybanner" class="row d-flex align-items-center justify-content-center">
					<div data-aos="flip-right" class="col-lg-6">
						<img id="myimg" width="100%" src="https://fac-institute.com/images/homepage2019/ACC-pabrikasi-rev-min.png" alt="Accounting Service FAC-Institute">
					</div>
					<div data-aos="flip-left" class="col-lg-6">
							<h2 id="myh2" class="mb-10" style="color:#df003a;">Accounting Service</h2>
							<p id="myp" class="" style="font-weight:bold;line-height:40px">Solusi cepat pembukuan dari kami untuk bisnis anda</p>
					</div>
			</div>
			</div>
			</section>


			<section class="we-offer-area section-gap" id="offer">
				<div class="row d-flex justify-content-center">
					<div class="menu-content pb-50 col-lg-10">
						<div class="title text-center">
							<h1>Apa yang kami berikan?</h1>
						</div>
					</div>
				</div>

				<div class="container d-flex justify-content-center" style="max-width:800px">
					<div class="row">
						<div class="col-lg-12">
							<div data-aos="fade-right" class="single-offer d-flex flex-row pb-30">
								<div class="icon">
									<img width="90" src="https://fac-institute.com/images/homepage2019/icon1.png" alt="Laporan Keuangan FAC Institute">
								</div>
								<div  class="desc">
									<a class="test1" href="#test1"><h4>Review Transaksi</h4></a>
									<p style="font-size:16px">
									 Kami mereview pencatatan akuntansi Anda
									</p>
								</div>
							</div>
							<div data-aos="fade-right" class="single-offer d-flex flex-row pb-30">
								<div class="icon">
									<img width="95" src="https://fac-institute.com/images/homepage2019/84436.png" alt="Data Entry FAC-Institute">
								</div>
								<div class="desc">
									<a href="#"><h4>Input Data Transaksi</h4></a>
									<p style="font-size:16px">
										Kami membantu menginput data transaksi Anda jika Anda belum memiliki tenaga pencatatan sendiri
									</p>
								</div>
							</div>

							<div data-aos="fade-right" class="single-offer d-flex flex-row pb-30">
								<div class="icon">
									<img width="90"  src="https://fac-institute.com/images/homepage2019/report-icon-1.png" alt="Laporan Keuangan FAC-Institute">
								</div>
								<div class="desc">
									<a href="#"><h4>Penyajian Laporan</h4></a>
									<p style="font-size:16px">
										Kami menyajikan laporan keuangan Anda lebih rapi, akuntabel dan akurat
									</p>
								</div>
							</div>
							<div data-aos="fade-right" class="single-offer d-flex flex-row pb-30">
								<div class="icon">
									<img  width="90" src="https://fac-institute.com/images/homepage2019/consultancy_icon.png" alt="Pembukuan Efektif">
								</div>
								<div class="desc">
									<a href="#"><h4>Konsultasi Bisnis</h4></a>
									<p style="font-size:16px">
										Kami menyediakan konsultasi tentang pembukuan yang efektif di perusahaan Anda
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>


			<!-- <section style='background: url("") ;background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:20px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
				<div class="container-fluid">
					<div class="row justify-content-center align-items-center">
						<div class="col-lg-12 text-center" style="max-width:1200px">
							<h1 style="font-size:40px" class="mb-10">Untuk Siapa Pelatihan Ini?</h1>
								<img class="img-fluid mt-20 mb-40" width="500" data-aos="fade-up" src="img/userall.png" alt="">
							<p style="font-size:20px;font-weight:300">
								Siapapun dan apapun <b>profesi</b> Anda bisa mengikuti pelatihan ini seperti : User <b>ACCURATE</b>, Mahasiswa, Pengusaha, Business Owner, Accountant, Finance Manager, dan lain lain.
							</p>
						</div>
					</div>
				</div>
			</section> -->




			<!-- <section style='background: url("https://png.pngtree.com/thumb_back/fw800/20160721/pngtree-Design-Paper-Graphic-White-background-photo-313370.jpg") ;background-position: center center;background-repeat: no-repeat;padding-top:120px;padding-right:30px;padding-bottom:20px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
				<div class="container-fluid">
					<div class="row justify-content-center align-items-center">
						<div class="col-lg-6" style="max-width:500px">
							<p class="top-title" style="font-size:20px">Weekday Office Support</p>
							<h1 style="font-size:24px" class="mb-10">Untuk Siapa Pelatihan Ini?</h1>
							<p><span>Kami mengerti segala masalah Anda</span></p>
							<p class="text-justify" style="font-size:16px">
								Siapapun dan apapun <b>profesi</b> Anda bisa mengikuti pelatihan ini seperti, User ACCURATE, Mahasiswa, Pengusaha, Business Owner, Accountant, Finance Manager, dan lain lain.
							</p>
						</div>
						<div class="col-lg-6 mt-40">
							<img class="img-fluid" data-aos="fade-up" width="400px" src="" alt="">
						</div>
					</div>
				</div>
			</section> -->

			<!-- <section style='background: url("https://png.pngtree.com/thumb_back/fw800/20160721/pngtree-Design-Paper-Graphic-White-background-photo-313370.jpg") ;background-position: center center;background-repeat: no-repeat;padding-top:120px;padding-right:30px;padding-bottom:20px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
				<div class="container-fluid">
					<div class="row justify-content-center align-items-center">
						<div class="col-lg-6 mt-40">
							<img class="img-fluid" data-aos="fade-up" width="400px" src="" alt="">
						</div>
						<div class="col-lg-6" style="max-width:500px">
							<p class="top-title" style="font-size:20px">Weekday Office Support</p>
							<h1 style="font-size:24px" class="mb-10"></h1>
							<p><span>Kami mengerti segala masalah Anda</span></p>
							<p class="text-justify" style="font-size:16px">
								Kami siap membantu menyelesaikan masalah software ACCURATE Anda dengan Cepat, Tepat dan Akurat.<br>
							</p>
						</div>
					</div>
				</div>
			</section> -->
<hr>
			<section style='background: url("") ;background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:40px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
				<div class="container-fluid">
					<!-- <b>Manfaat yang Anda dapatkan</b> -->
					<div class="row justify-content-center align-items-center" >
						<div class="col-lg-12 text-center" style="max-width:1200px">
							<!-- <p class="top-title" style="font-size:20px">Weekday Office Support</p> -->
							<h1 style="font-size:40px" class="mb-40">Manfaat yang Anda dapatkan</h1>
						</div>
						<div class="col-lg-6 text-center" style="max-width:400px">
							<img class="img-fluid mt-20 mb-60" width="100%" data-aos="fade-up" src="http://fac-institute.com/images/homepage2019/sucs.png" alt="Akuntansi Nyaman FAC-Institute">
							<p class="text-justify" style="font-size:16px">
								Staff pembukuan Anda bisa dengan tenang melakukan input transaksi sendiri karena kami akan me-review sesuai kaidah akuntansi dan perpajakan.
							</p>
						</div>
						<div class="col-lg-6 text-center" style="max-width:400px">
							<img class="img-fluid mt-20 mb-40" width="100%" data-aos="fade-up" src="http://fac-institute.com/images/homepage2019/icon-improve.png" alt="Pengembangan Bisnis FAC Institute">

							<p class="text-justify" style="font-size:16px">
								Jika Anda belum memiliki staff akuntansi, Anda bisa fokus mengembangkan bisnis karena pencatatan keuangan kami yang tangani.
							</p>
						</div>


					</div>
				</div>
			</section>


<?php include_once $data->homedir.'view/homepage/footer.homepage.php'; ?>

			<script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/jquery-2.2.4.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/bootstrap.min.js"></script>
			<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
  			<script src="<?= $data->base_url ?>assets/homepage2019/js/easing.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/hoverIntent.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/superfish.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.ajaxchimp.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.magnific-popup.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/owl.carousel.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.sticky.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.nice-select.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/parallax.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/mail-script.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/main.js"></script>
			<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
			<script>
			AOS.init();
			$('.test1').click(function() {
			    var sectionTo = $(this).attr('href');
			    $('html, body').animate({
			      scrollTop: $(sectionTo).offset().top
			    }, 1000);
			});
			</script>
		</body>
	</html>
