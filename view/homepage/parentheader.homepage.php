<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<ul>
		<li style="display:inline-block;margin-right:20px;text-decoration:none;font-size:12px">
		<a href="<?= $data->base_url ?>" style="color:#fff">Beranda Utama</a>
		</li>
		<div class="dropdown" style="display:inline-block;margin-right:20px;text-decoration:none;font-size:12px">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color:#fff" >Layanan</a>
				<div class="dropdown-menu" style="font-size:12px">
					<a class="dropdown-item" href="<?= $data->base_url ?>training">
					Training Accurate
					</a>
					<a class="dropdown-item" href="<?= $data->base_url ?>accountingservice">
					Accounting Service
					</a>
					<a class="dropdown-item" href="<?= $data->base_url ?>kursus">
					Kursus Accurate
					</a>
				</div>
			</div>

		<li style="display:inline-block;margin-right:20px;text-decoration:none;font-size:12px">
		<a href="<?= $data->base_url ?>support" style="color:#fff">Bantuan & Support</a>
		</li>

		</ul>
	</nav>
