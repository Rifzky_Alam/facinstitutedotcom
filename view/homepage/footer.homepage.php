<!-- <footer class="footer-area" style="border-top: 2px solid #df003a;padding: 40px 0 20px 0;"> -->

<style>
.bubbly-button {
  /* font-family: 'Helvetica', 'Arial', sans-serif; */
  display: inline-block;
  font-size: 1em;
  padding: 0.5em 2em;
  /* margin-top: 100px;
  margin-bottom: 60px; */
  -webkit-appearance: none;
  appearance: none;
  background-color: #ff0081;
  color: #fff;
  border-radius: 4px;
  border: none;
  cursor: pointer;
  position: relative;
  transition: transform ease-in 0.1s, box-shadow ease-in 0.25s;
  box-shadow: 0 2px 25px rgba(255, 0, 130, 0.5);
}
.bubbly-button:focus {
  outline: 0;
}
.bubbly-button:before, .bubbly-button:after {
  position: absolute;
  content: '';
  display: block;
  width: 140%;
  height: 100%;
  left: -20%;
  z-index: -1000;
  transition: all ease-in-out 0.5s;
  background-repeat: no-repeat;
}
.bubbly-button:before {
  display: none;
  top: -75%;
  background-image: radial-gradient(circle, #ff0081 20%, transparent 20%), radial-gradient(circle, transparent 20%, #ff0081 20%, transparent 30%), radial-gradient(circle, #ff0081 20%, transparent 20%), radial-gradient(circle, #ff0081 20%, transparent 20%), radial-gradient(circle, transparent 10%, #ff0081 15%, transparent 20%), radial-gradient(circle, #ff0081 20%, transparent 20%), radial-gradient(circle, #ff0081 20%, transparent 20%), radial-gradient(circle, #ff0081 20%, transparent 20%), radial-gradient(circle, #ff0081 20%, transparent 20%);
  background-size: 10% 10%, 20% 20%, 15% 15%, 20% 20%, 18% 18%, 10% 10%, 15% 15%, 10% 10%, 18% 18%;
}
.bubbly-button:after {
  display: none;
  bottom: -75%;
  background-image: radial-gradient(circle, #ff0081 20%, transparent 20%), radial-gradient(circle, #ff0081 20%, transparent 20%), radial-gradient(circle, transparent 10%, #ff0081 15%, transparent 20%), radial-gradient(circle, #ff0081 20%, transparent 20%), radial-gradient(circle, #ff0081 20%, transparent 20%), radial-gradient(circle, #ff0081 20%, transparent 20%), radial-gradient(circle, #ff0081 20%, transparent 20%);
  background-size: 15% 15%, 20% 20%, 18% 18%, 20% 20%, 15% 15%, 10% 10%, 20% 20%;
}
.bubbly-button:active {
  transform: scale(0.9);
  background-color: #e60074;
  box-shadow: 0 2px 25px rgba(255, 0, 130, 0.2);
}
.bubbly-button.animate:before {
  display: block;
  animation: topBubbles ease-in-out 0.75s forwards;
}
.bubbly-button.animate:after {
  display: block;
  animation: bottomBubbles ease-in-out 0.75s forwards;
}

@keyframes topBubbles {
  0% {
    background-position: 5% 90%, 10% 90%, 10% 90%, 15% 90%, 25% 90%, 25% 90%, 40% 90%, 55% 90%, 70% 90%;
  }
  50% {
    background-position: 0% 80%, 0% 20%, 10% 40%, 20% 0%, 30% 30%, 22% 50%, 50% 50%, 65% 20%, 90% 30%;
  }
  100% {
    background-position: 0% 70%, 0% 10%, 10% 30%, 20% -10%, 30% 20%, 22% 40%, 50% 40%, 65% 10%, 90% 20%;
    background-size: 0% 0%, 0% 0%,  0% 0%,  0% 0%,  0% 0%,  0% 0%;
  }
}
@keyframes bottomBubbles {
  0% {
    background-position: 10% -10%, 30% 10%, 55% -10%, 70% -10%, 85% -10%, 70% -10%, 70% 0%;
  }
  50% {
    background-position: 0% 80%, 20% 80%, 45% 60%, 60% 100%, 75% 70%, 95% 60%, 105% 0%;
  }
  100% {
    background-position: 0% 90%, 20% 90%, 45% 70%, 60% 110%, 75% 80%, 95% 70%, 110% 10%;
    background-size: 0% 0%, 0% 0%,  0% 0%,  0% 0%,  0% 0%,  0% 0%;
  }
}
</style>

<footer class="footer-area" style="padding: 40px 0 20px 0;">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div class="single-footer-widget newsletter">
          <!--<p>Perlu Training ACCURATE ? Hubungi Kami di : <span style="font-size:20px"><strong style="background-color:#000;padding:10px;font-weight:bold">  0812-9008-3983 / 0812-1911-5005 </strong></span></p>-->
          <p>Perlu Training ACCURATE di kantor Anda? Hubungi Kami di :  <a style="font-size:20px;font-weight:bold" href="tel://081290083983">0812-9008-3983</a>  / <a style="font-size:20px;font-weight:bold" href="tel://">0812-1911-5005</a>
          </p>
          <p>untuk Kursus ACCURATE di kantor kami Hubungi :  <a style="font-size:20px;font-weight:bold" href="https://wa.me/6281281737578">0812-8173-7578</a>
          </p>
          <p><span style="background-color:#000;padding:4px;font-weight:bold" class="text-white">Temukan Kami!</span> <?= @$data->companyaddr ?></p>
          <p><strong>Hari buka</strong> : Senin s/d Jumat (09.00–17.00) </p>
          <button onclick="window.location.href='https://fac-institute.com/training/daftarharga'" class="bubbly-button">Info Harga Training</button>

        </div>
      </div>
    </div>

    <hr style="background-color: white; height: 0px; border: 1;">

      <div class="row d-flex justify-content-between mt-20">
      <p class="col-lg-8 col-sm-12 footer-text m-0 text-white">
        Copyright © <script type="text/javascript">document.write(new Date().getFullYear());</script> FAC INSTITUTE | All Rights Reserved | <a href="#">Kebijakan Privasi </a>
      </p>
      <div class="col-lg-4 col-sm-12 footer-social">
        <a href="#"><i class="fa fa-facebook"></i></a>
        <a href="#"><i class="fa fa-twitter"></i></a>
        <a href="#"><i class="fa fa-instagram"></i></a>
      </div>
    </div>


  </div>
</footer>


<script>

var animateButton = function(e) {

  e.preventDefault;
  //reset animation
  e.target.classList.remove('animate');

  e.target.classList.add('animate');
  setTimeout(function(){
    e.target.classList.remove('animate');
  },700);
};

var bubblyButtons = document.getElementsByClassName("bubbly-button");

for (var i = 0; i < bubblyButtons.length; i++) {
  bubblyButtons[i].addEventListener('click', animateButton, false);
}

</script>
