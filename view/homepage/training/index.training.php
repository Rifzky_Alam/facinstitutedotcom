	<!DOCTYPE html>
	<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="<?= $data->base_url ?>images/homepage2019/elements/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="Rifzky Alam, Dino Damara">
		<!-- Meta Description -->
		<meta name="description" content="<?= $data->pagedesc ?>">
		<!-- Meta Keyword -->
		<meta name="keywords" content="<?= $data->pagekeywords ?>">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title><?= $data->title ?></title>

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
			<!--
			CSS
			============================================= -->
			<link rel="shortcut icon" href='https://fac-institute.com/_caramel/assets/img/g25992_9_kUd_icon.ico'>
			<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/linearicons.css">
			<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/owl.carousel.css">
			<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/font-awesome.min.css">
			<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/nice-select.css">
			<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/magnific-popup.css">
			<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/bootstrap.css">
			<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/main.css">
			<style media="screen">
			.banner-content h1 {
			    font-size: 38px;
			}
			.nav-menu a {
					padding: 15px 8px 1px 8px;
					font-size: 16px;
					font-weight: 500;
			}
			.nav-menu ul li a {
					font-size: 14px;
			}

			.menu-active {
			  border-bottom: 5px solid #df003a;
			}

			.navbar {
			    padding: 0.2rem 1rem;
			}


			.circle {
			  padding: 13px 20px;
			  border-radius: 50%;
			  background-color: #f74e4e;
			  color: #fff;
			  max-height: 50px;
			  z-index: 2;
			}

			.how-it-works.row .col-2 {
			  align-self: stretch;
			}
			.how-it-works.row .col-2::after {
			  content: "";
			  position: absolute;
			  border-left: 3px solid #ED8D8D;
			  z-index: 1;
			}
			.how-it-works.row .col-2.bottom::after {
			  height: 50%;
			  left: 50%;
			  top: 50%;
			}
			.how-it-works.row .col-2.full::after {
			  height: 100%;
			  left: calc(50% - 3px);
			}
			.how-it-works.row .col-2.top::after {
			  height: 50%;
			  left: 50%;
			  top: 0;
			}


			.timeline div {
			  padding: 0;
			  height: 40px;
			}
			.timeline hr {
			  border-top: 3px solid #ED8D8D;
			  margin: 0;
			  top: 17px;
			  position: relative;
			}
			.timeline .col-2 {
			  display: flex;
			  overflow: hidden;
			}
			.timeline .corner {
			  border: 3px solid #ED8D8D;
			  width: 100%;
			  position: relative;
			  border-radius: 15px;
			}
			.timeline .top-right {
			  left: 50%;
			  top: -50%;
			}
			.timeline .left-bottom {
			  left: -50%;
			  top: calc(50% - 3px);
			}
			.timeline .top-left {
			  left: -50%;
			  top: -50%;
			}
			.timeline .right-bottom {
			  left: 50%;
			  top: calc(50% - 3px);
			}

			#myh2 {
				font-size: 50px;
			}

			#mybanner {
				height:600px;
			}



			@media (max-width: 480px) {
				#myh2 {
					font-size: 22px;
				}
				#mybanner {
					height:500px;
				}
				#myimg{
				display: none;
				}
				.generic-banner{
					height: 250px;
				}
				#mobile-nav-toggle {
				  top: 35px;
				}
			}

			@media (max-width: 360px) {
				#myh2 {
					font-size: 22px;
				}
				#mybanner {
					height:500px;
				}
				#myimg{
				visibility: hidden;
				}
				.generic-banner{
					height: 250px;
				}
				#mobile-nav-toggle {
					top: 55px;
				}
			}


			</style>
      <link href="https://fac-institute.com/assets/homepage2019/css/aos.css" rel="stylesheet">
		</head>
		<body>

			<?php include_once $data->homedir.'view/homepage/training/header.training.php'; ?>


			<section class="generic-banner relative mt-40">
				<div class="container">
					<div id="mybanner" class="row d-flex align-items-center justify-content-center">
						<div data-aos="flip-left" class="col-lg-6">
								<h2 id="myh2" class="mb-10" style="color:#df003a">Training Accurate</h2>
								<p class="" style="font-size:24px">Solusi cepat dari kami untuk mahir Accurate.</p>
						</div>
						<div data-aos="flip-right" class="col-lg-6">
							<img id="myimg" width="100%" src="<?= $data->base_url ?>images/homepage2019/AOL-banner-atas-min.png" alt="">
						</div>
				</div>
			</div>
			</section>

			<section class="we-offer-area mb-10 mt-40 section-gap" id="offer">
				<div class="container">
					<div class="row d-flex justify-content-center">
						<div class="menu-content pb-60 col-lg-10">
							<div class="title text-center">
								<h1 class="mb-10">8 Alasan Training di FAC Institute</h1>
								<!-- <p style="font-size:18px"> 6 Alasan Training di FAC Institute </p> -->
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div data-aos="" class="single-offer d-flex flex-row pb-30">
								<div class="icon">
									<img width="90" src="https://fac-institute.com/_caramel/assets/img/Logo-ACCURATE-5.png" alt="">
								</div>
								<div class="desc">
									<a href="#">
										<h4>ACCURATE Authorized Training Center</h4>
									</a>
									<p>
										Memiliki izin resmi dari <b>CPSSOFT</b> sebagai ACCURATE Authorized Training Center
									</p>
								</div>
							</div>

							<div data-aos="" class="single-offer d-flex flex-row pb-30">
								<div class="icon">
									<img width="90" src="<?= $data->base_url ?>_maizena/img/logostore.png" alt="">
								</div>
								<div class="desc">
									<a href="#">
										<h4>Partner Resmi</h4>
									</a>
									<p>
										Kami adalah partner training dari cabang resmi penjualan ACCURATE
									</p>
								</div>
							</div>

							<div data-aos="" class="single-offer d-flex flex-row pb-30">
								<div class="icon">
									<img width="90" src="<?= $data->base_url ?>images/homepage2019/services-icon-1-1.png" alt="">
								</div>
								<div class="desc">
									<a href="#">
										<h4>Trainer Berpengalaman</h4>
									</a>
									<p>
										Kami memiliki trainer berpengalaman yang siap diterjunkan ke seluruh kota di <b >Indonesia</b>
									</p>
								</div>

							</div>

							<div data-aos="" class="single-offer d-flex flex-row pb-30">
								<div class="icon">
									<img width="90" src="<?= $data->base_url ?>images/homepage2019/whatsapp_PNG7.png" alt="">
								</div>
								<div class="desc">
									<a href="#">
										<h4>FREE Support</h4>
									</a>
									<p>
								   Dukungan penuh setelah training via <b>WhatsApp</b> / Chat / E-mail / Call (phone cell)
									</p>
								</div>
							</div>



						</div>

						<div class="col-lg-6">
							<div data-aos="" class="single-offer d-flex flex-row pb-30">
								<div class="icon">
									<img width="90"  src="<?= $data->base_url ?>images/homepage2019/TeamViewer-icon.png" alt="">
								</div>
								<div class="desc">
									<a href="#">
										<h4 class="d-inline">Premium Support <span><i class="fa fa-star text-warning" aria-hidden="true"></i></span></h4>
									</a>
									<p>
										Layanan <b>berbayar</b> dari kami untuk solusi masalah Anda dengan cepat! <button type="button" onclick="location.href='https://fac-institute.com/support/premium'" class="btn btn-danger btn-sm text-white">Selengkapnya</button>
									</p>
								</div>
							</div>
							<div data-aos=""class="single-offer d-flex flex-row pb-30">
								<div class="icon">
									<img  width="90" src="<?= $data->base_url ?>images/homepage2019/calendar.png" alt="">
								</div>
								<div class="desc">
									<a href="#">
										<h4>Scheduled Training</h4>
									</a>
									<p>
										Tentukan sendiri jadwal training Anda!  <button type="button" onclick="location.href='https://fac-institute.com/daftar-training/'" class="btn btn-danger btn-sm text-white">Order Now</button>
									</p>
								</div>
							</div>
							<div data-aos=""class="single-offer d-flex flex-row pb-30">
								<div class="icon">
									<img  width="90" src="<?= $data->base_url ?>images/homepage2019/certificate-icon.png" alt="">
								</div>
								<div class="desc">
									<a href="#">
										<h4>Free Sertifikat</h4>
									</a>
									<p>
										Free 1 Sertifikat <i>(Minimal Order 5 Hari)</i>
									</p>
								</div>
							</div>

							<div data-aos=""class="single-offer d-flex flex-row pb-30">
								<div class="icon">
									<img  width="90" src="<?= $data->base_url ?>images/homepage2019/167756.png" alt="">
								</div>
								<div class="desc">
									<a href="#">
										<h4>Training siklus ACCURATE</h4>
									</a>
									<p>
										Training siklus accurate dari A sampai Z
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- End banner Area -->



<hr>
			<section class="mb-80" style='background: url("") ;background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:20px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
				<div class="container-fluid mb-30">
					<div class="row justify-content-center align-items-center">
						<div class="col-lg-12 text-center" style="max-width:800px">
							<h1 style="font-size:40px" class="mb-10">Cakupan Training Accurate</h1>
							<p style="font-size:14px;font-style:italic">**point-point dibawah ini adalah cakupan training, bisa salah satunya, sebagiannya atau seluruhnya, tergantung kesepakatan sebelum training dilaksanakan.</p>
						</div>
					</div>
				</div>

				  <div class="container-fluid" style="max-width:500px">

				    <div class="row align-items-center how-it-works d-flex">
				      <div class="col-2 text-center bottom d-inline-flex justify-content-center align-items-center">
				        <div class="circle font-weight-bold"><span style="font-size:12px"><i class="fa fa-database" aria-hidden="true"></i></span></div>
				      </div>
				      <div class="col-6">
				        <h5>Setup Database ACCURATE</h5>
				      </div>
				    </div>
				    <!--path between 1-2-->
				    <div class="row timeline">
				      <div class="col-2">
				        <div class="corner top-right"></div>
				      </div>
				      <div class="col-8">
				        <hr/>
				      </div>
				      <div class="col-2">
				        <div class="corner left-bottom"></div>
				      </div>
				    </div>
				    <!--second section-->
				    <div class="row align-items-center justify-content-end how-it-works d-flex">
				      <div class="col-6 text-right">
				        <h5>Training Fitur ACCURATE</h5>
				        <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed porttitor gravida aliquam. Morbi orci urna, iaculis in ligula et, posuere interdum lectus.</p> -->
				      </div>
				      <div class="col-2 text-center full d-inline-flex justify-content-center align-items-center">
				        <div class="circle font-weight-bold"><span style="font-size:12px"><i class="fa fa-laptop" aria-hidden="true"></i></span></div>
				      </div>
				    </div>
				    <!--path between 2-3-->
				    <div class="row timeline">
				      <div class="col-2">
				        <div class="corner right-bottom"></div>
				      </div>
				      <div class="col-8">
				        <hr/>
				      </div>
				      <div class="col-2">
				        <div class="corner top-left"></div>
				      </div>
				    </div>
				    <!--third section-->
				    <div class="row align-items-center how-it-works d-flex">
				      <div class="col-2 text-center top d-inline-flex justify-content-center align-items-center">
				        <div class="circle font-weight-bold"><span style="font-size:12px"><i class="fa fa-bug" aria-hidden="true"></i></span></div>
				      </div>
				      <div class="col-6">
				        <h5>Troubleshooting ACCURATE</h5>
				        <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed porttitor gravida aliquam. Morbi orci urna, iaculis in ligula et, posuere interdum lectus.</p> -->
				      </div>
				    </div>

						<div class="row timeline">
							<div class="col-2">
								<div class="corner top-right"></div>
							</div>
							<div class="col-8">
								<hr/>
							</div>
							<div class="col-2">
								<div class="corner left-bottom"></div>
							</div>
						</div>

						<div class="row align-items-center justify-content-end how-it-works d-flex">
							<div class="col-6 text-right">
								<h5>Review Penggunaan ACCURATE</h5>
								<!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed porttitor gravida aliquam. Morbi orci urna, iaculis in ligula et, posuere interdum lectus.</p> -->
							</div>
							<div class="col-2 text-center full d-inline-flex justify-content-center align-items-center">
								<div class="circle font-weight-bold"><span style="font-size:12px"><i class="fa fa-check-square-o" aria-hidden="true"></i></span></div>
							</div>
						</div>

				  </div>

			</section>



	    <?php include_once $data->homedir.'view/homepage/footer.homepage.php'; ?>

			<script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/jquery-2.2.4.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/bootstrap.min.js"></script>
			<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
  			<script src="<?= $data->base_url ?>assets/homepage2019/js/easing.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/hoverIntent.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/superfish.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.ajaxchimp.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.magnific-popup.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/owl.carousel.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.sticky.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.nice-select.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/parallax.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/mail-script.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/main.js"></script>
			<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
			<script>
			AOS.init();
			$('.test1').click(function() {
			    var sectionTo = $(this).attr('href');
			    $('html, body').animate({
			      scrollTop: $(sectionTo).offset().top
			    }, 1000);
			});
			</script>
		</body>
	</html>
