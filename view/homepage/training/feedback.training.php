<!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>
  <!-- Mobile Specific Meta -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Favicon-->
  <link rel="shortcut icon" href="<?= $data->base_url ?>images/homepage2019/elements/fav.png">
  <!-- Author Meta -->
  <meta name="author" content="Rifzky Alam, Dino Damara">
  <!-- Meta Description -->
  <meta name="description" content="<?= $data->pagedesc ?>">
  <!-- Meta Keyword -->
  <meta name="keywords" content="<?= $data->pagekeywords ?>">
  <!-- meta character set -->
  <meta charset="UTF-8">
  <!-- Site Title -->
  <title><?= $data->title ?></title>

  <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
    <!--
    CSS
    ============================================= -->
    <link rel="shortcut icon" href='https://fac-institute.com/_caramel/assets/img/g25992_9_kUd_icon.ico'>
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/linearicons.css">
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/owl.carousel.css">
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/nice-select.css">
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/magnific-popup.css">
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/bootstrap.css">
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/main.css">
    <style media="screen">
    .banner-content h1 {
        font-size: 38px;
    }
    .nav-menu a {
        padding: 15px 8px 1px 8px;
        font-size: 16px;
        font-weight: 500;
    }
    .nav-menu ul li a {
        font-size: 14px;
    }

    .menu-active {
      border-bottom: 5px solid #df003a;
    }

    .navbar {
        padding: 0.2rem 1rem;
    }


    .circle {
      padding: 13px 20px;
      border-radius: 50%;
      background-color: #f74e4e;
      color: #fff;
      max-height: 50px;
      z-index: 2;
    }

    .how-it-works.row .col-2 {
      align-self: stretch;
    }
    .how-it-works.row .col-2::after {
      content: "";
      position: absolute;
      border-left: 3px solid #ED8D8D;
      z-index: 1;
    }
    .how-it-works.row .col-2.bottom::after {
      height: 50%;
      left: 50%;
      top: 50%;
    }
    .how-it-works.row .col-2.full::after {
      height: 100%;
      left: calc(50% - 3px);
    }
    .how-it-works.row .col-2.top::after {
      height: 50%;
      left: 50%;
      top: 0;
    }


    .timeline div {
      padding: 0;
      height: 40px;
    }
    .timeline hr {
      border-top: 3px solid #ED8D8D;
      margin: 0;
      top: 17px;
      position: relative;
    }
    .timeline .col-2 {
      display: flex;
      overflow: hidden;
    }
    .timeline .corner {
      border: 3px solid #ED8D8D;
      width: 100%;
      position: relative;
      border-radius: 15px;
    }
    .timeline .top-right {
      left: 50%;
      top: -50%;
    }
    .timeline .left-bottom {
      left: -50%;
      top: calc(50% - 3px);
    }
    .timeline .top-left {
      left: -50%;
      top: -50%;
    }
    .timeline .right-bottom {
      left: 50%;
      top: calc(50% - 3px);
    }

    #myh2 {
      font-size: 50px;
    }

    #mybanner {
      height:600px;
    }



    @media (max-width: 480px) {
      #myh2 {
        font-size: 22px;
      }
      #mybanner {
        height:500px;
      }
      #myimg{
      display: none;
      }
      .generic-banner{
        height: 250px;
      }
      #mobile-nav-toggle {
        top: 35px;
      }
    }

    @media (max-width: 360px) {
      #myh2 {
        font-size: 22px;
      }
      #mybanner {
        height:500px;
      }
      #myimg{
      visibility: hidden;
      }
      .generic-banner{
        height: 250px;
      }
      #mobile-nav-toggle {
        top: 55px;
      }
    }

    /***
     *  Simple Pure CSS Star Rating Widget Bootstrap 4
     *
     *  www.TheMastercut.co
     *
     ***/

    @import url(//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css);

    /* Styling h1 and links
    ––––––––––––––––––––––––––––––––– */

    .white-a {color: grey; font-size: 0.5em; text-decoration: none}


    .starrating > input {display: none;}  /* Remove radio buttons */

    .starrating > label:before {
      content: "\f005"; /* Star */
      margin: 2px;
      font-size: 4em;
      font-family: FontAwesome;
      display: inline-block;
    }

    .starrating > label
    {
      color: #222222; /* Start color when not clicked */
    }

    .starrating > input:checked ~ label
    { color: #ffca08 ; } /* Set yellow color when star checked */

    .starrating > input:hover ~ label
    { color: #ffca08 ;  } /* Set yellow color when star hover */



    </style>

    <style>


    .imgF {
      border:solid 2px;
      border-bottom-color:#ffe;
      border-left-color:#eed;
      border-right-color:#eed;
      border-top-color:#ccb;
      max-height:100%;
      max-width:100%;
    }

    .frame {
      background-color:#ddc;
      border:solid 5vmin #eee;
      border-bottom-color:#fff;
      border-left-color:#eee;
      border-radius:2px;
      border-right-color:#eee;
      border-top-color:#ddd;
      box-shadow:0 0 5px 0 rgba(0,0,0,.25) inset, 0 5px 10px 5px rgba(0,0,0,.25);
      box-sizing:border-box;
      display:inline-block;
      margin:10vh 10vw;
      height:60vh;
      padding:7vmin;
      position:relative;
      text-align:center;
      &:before {
        border-radius:2px;
        bottom:-2vmin;
        box-shadow:0 2px 5px 0 rgba(0,0,0,.25) inset;
        content:"";
        left:-2vmin;
        position:absolute;
        right:-2vmin;
        top:-2vmin;
      }
      &:after {
        border-radius:2px;
        bottom:-2.5vmin;
        box-shadow: 0 2px 5px 0 rgba(0,0,0,.25);
        content:"";
        left:-2.5vmin;
        position:absolute;
        right:-2.5vmin;
        top:-2.5vmin;
      }
    }

    @media (max-width: 480px) {
    .frame {
    height:40vh;
    }
    }


    </style>

    <link href="https://fac-institute.com/assets/homepage2019/css/aos.css" rel="stylesheet">
  </head>
  <body>

    <?php include_once $data->homedir.'view/homepage/training/header.training.php'; ?>

    <div  class="container-fluid mt-120">
      <div class="row mb-20 justify-content-center align-items-center" >
        <div class="col-lg-12 text-center">
          <p style="font-size:22px;font-style:italic">TERIMAKASIH TELAH MENGGUNAKAN JASA KAMI</p>
          <p style="font-size:16px;font-style:italic">Please review your experience</p>
        </div>
        <div class="col-lg-12 text-center">

          <div class="frame">
          <img class="imgF" src="https://fac-institute.com/images/member/<?= $data->memberpic ?>" alt="">
          <p style="font-size:22px;font-style:italic;font-weight:bold"><?= $data->nama_staff ?></p>
        </div>
</div>


      </div>

<form action="" method="post">
      <h2 class="text-center mb-20" alt="Simple"><a class="white-a">Penguasaan Materi</a></h2>
      <input type="hidden" value="<?= $data->user_staff ?>" name="in[u]" />
      <div class="starrating risingstar d-flex justify-content-center flex-row-reverse">
          <!--<input type="radio" id="star10-a" name="in[mas]" value="10" /><label for="star10-a" title="10 star">10</label>-->
          <!--<input type="radio" id="star9-a" name="in[mas]" value="9" /><label for="star9-a" title="9 star">9</label>-->
          <!--<input type="radio" id="star8-a" name="in[mas]" value="8" /><label for="star8-a" title="8 star">8</label>-->
          <!--<input type="radio" id="star7-a" name="in[mas]" value="7" /><label for="star7-a" title="7 star">7</label>-->
          <!--<input type="radio" id="star6-a" name="in[mas]" value="6" /><label for="star6-a" title="6 star">6</label>-->
          <input type="radio" id="star5-a" name="in[mas]" value="5" /><label for="star5-a" title="5 star">5</label>
          <input type="radio" id="star4-a" name="in[mas]" value="4" /><label for="star4-a" title="4 star">4</label>
          <input type="radio" id="star3-a" name="in[mas]" value="3" /><label for="star3-a" title="3 star">3</label>
          <input type="radio" id="star2-a" name="in[mas]" value="2" /><label for="star2-a" title="2 star">2</label>
          <input type="radio" id="star1-a" name="in[mas]" value="1" /><label for="star1-a" title="1 star">1</label>
      </div>
      <h2 class="text-center mb-20" alt="Simple"><a class="white-a">Penyampaian Materi</a></h2>
      <div class="starrating risingstar d-flex justify-content-center flex-row-reverse">
          <!--<input type="radio" id="star10-b" name="in[del]" value="10" /><label for="star10-b" title="10 star">10</label>-->
          <!--<input type="radio" id="star9-b" name="in[del]" value="9" /><label for="star9-b" title="9 star">9</label>-->
          <!--<input type="radio" id="star8-b" name="in[del]" value="8" /><label for="star8-b" title="8 star">8</label>-->
          <!--<input type="radio" id="star7-b" name="in[del]" value="7" /><label for="star7-b" title="7 star">7</label>-->
          <!--<input type="radio" id="star6-b" name="in[del]" value="6" /><label for="star6-b" title="6 star">6</label>-->
          <input type="radio" id="star5-b" name="in[del]" value="5" /><label for="star5-b" title="5 star">5</label>
          <input type="radio" id="star4-b" name="in[del]" value="4" /><label for="star4-b" title="4 star">4</label>
          <input type="radio" id="star3-b" name="in[del]" value="3" /><label for="star3-b" title="3 star">3</label>
          <input type="radio" id="star2-b" name="in[del]" value="2" /><label for="star2-b" title="2 star">2</label>
          <input type="radio" id="star1-b" name="in[del]" value="1" /><label for="star1-b" title="1 star">1</label>
      </div>
      <h2 class="text-center mb-20" alt="Simple"><a class="white-a">Attitude (Kesopanan)</a></h2>
      <div class="starrating risingstar d-flex justify-content-center flex-row-reverse">
          <!--<input type="radio" id="star10-c" name="in[atd]" value="10" /><label for="star10-c" title="10 star">10</label>-->
          <!--<input type="radio" id="star9-c" name="in[atd]" value="9" /><label for="star9-c" title="9 star">9</label>-->
          <!--<input type="radio" id="star8-c" name="in[atd]" value="8" /><label for="star8-c" title="8 star">8</label>-->
          <!--<input type="radio" id="star7-c" name="in[atd]" value="7" /><label for="star7-c" title="7 star">7</label>-->
          <!--<input type="radio" id="star6-c" name="in[atd]" value="6" /><label for="star6-c" title="6 star">6</label>-->
          <input type="radio" id="star5-c" name="in[atd]" value="5" /><label for="star5-c" title="5 star">5</label>
          <input type="radio" id="star4-c" name="in[atd]" value="4" /><label for="star4-c" title="4 star">4</label>
          <input type="radio" id="star3-c" name="in[atd]" value="3" /><label for="star3-c" title="3 star">3</label>
          <input type="radio" id="star2-c" name="in[atd]" value="2" /><label for="star2-c" title="2 star">2</label>
          <input type="radio" id="star1-c" name="in[atd]" value="1" /><label for="star1-c" title="1 star">1</label>
      </div>
      <h2 class="text-center mb-20" alt="Simple"><a class="white-a">Penampilan</a></h2>
      <div class="starrating risingstar d-flex justify-content-center flex-row-reverse">
          <!--<input type="radio" id="star10-d" name="in[atr]" value="10" /><label for="star10-d" title="10 star">10</label>-->
          <!--<input type="radio" id="star9-d" name="in[atr]" value="9" /><label for="star9-d" title="9 star">9</label>-->
          <!--<input type="radio" id="star8-d" name="in[atr]" value="8" /><label for="star8-d" title="8 star">8</label>-->
          <!--<input type="radio" id="star7-d" name="in[atr]" value="7" /><label for="star7-d" title="7 star">7</label>-->
          <!--<input type="radio" id="star6-d" name="in[atr]" value="6" /><label for="star6-d" title="6 star">6</label>-->
          <input type="radio" id="star5-d" name="in[atr]" value="5" /><label for="star5-d" title="5 star">5</label>
          <input type="radio" id="star4-d" name="in[atr]" value="4" /><label for="star4-d" title="4 star">4</label>
          <input type="radio" id="star3-d" name="in[atr]" value="3" /><label for="star3-d" title="3 star">3</label>
          <input type="radio" id="star2-d" name="in[atr]" value="2" /><label for="star2-d" title="2 star">2</label>
          <input type="radio" id="star1-d" name="in[atr]" value="1" /><label for="star1-d" title="1 star">1</label>
      </div>
      <br>
      <div class="d-flex justify-content-center flex-row-reverse">
          <textarea class="form-control" style="width:50%;" placeholder="Bantu kami untuk lebih baik lagi, sertakan saran anda disini.." name="in[fdbk]"></textarea>
      </div>
        
      <h2 class="text-center mt-20 mb-80" alt="Simple">
        <button class="btn btn-dark">Submit Review</button>
      </h2>

</form>

    </div>





    <?php include_once $data->homedir.'view/homepage/footer.homepage.php'; ?>

    <script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/jquery-2.2.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/easing.min.js"></script>
    <script src="<?= $data->base_url ?>assets/homepage2019/js/hoverIntent.js"></script>
    <script src="<?= $data->base_url ?>assets/homepage2019/js/superfish.min.js"></script>
    <script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.ajaxchimp.min.js"></script>
    <script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.magnific-popup.min.js"></script>
    <script src="<?= $data->base_url ?>assets/homepage2019/js/owl.carousel.min.js"></script>
    <script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.sticky.js"></script>
    <script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.nice-select.min.js"></script>
    <script src="<?= $data->base_url ?>assets/homepage2019/js/parallax.min.js"></script>
    <script src="<?= $data->base_url ?>assets/homepage2019/js/mail-script.js"></script>
    <script src="<?= $data->base_url ?>assets/homepage2019/js/main.js"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
    AOS.init();
    $('.test1').click(function() {
        var sectionTo = $(this).attr('href');
        $('html, body').animate({
          scrollTop: $(sectionTo).offset().top
        }, 1000);
    });
    </script>
  </body>
</html>
