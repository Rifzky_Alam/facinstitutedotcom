	<!DOCTYPE html>
	<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="<?= $data->base_url ?>images/homepage2019/elements/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="Rifzky Alam, Dino Damara">
		<!-- Meta Description -->
		<meta name="description" content="<?= $data->pagedesc ?>">
		<!-- Meta Keyword -->
		<meta name="keywords" content="<?= $data->pagekeywords ?>">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title><?= $data->title ?></title>

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
			<!--
			CSS
			============================================= -->
			<link rel="shortcut icon" href='https://fac-institute.com/_caramel/assets/img/g25992_9_kUd_icon.ico'>
			<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/linearicons.css">
			<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/owl.carousel.css">
			<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/font-awesome.min.css">
			<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/nice-select.css">
			<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/magnific-popup.css">
			<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/bootstrap.css">
			<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/main.css">
			<style media="screen">
			.banner-content h1 {
			    font-size: 38px;
			}
			.nav-menu a {
					padding: 15px 8px 1px 8px;
					font-size: 16px;
					font-weight: 500;
			}
			.nav-menu ul li a {
					font-size: 14px;
			}

			.menu-active {
			  border-bottom: 5px solid #df003a;
			}

			.navbar {
			    padding: 0.2rem 1rem;
			}
			.pricing-header {
			  /* max-width: 1200px; */
			}

			.card-deck .card {
			  min-width: 220px;
			}

			.border-top { border-top: 1px solid #e5e5e5; }
			.border-bottom { border-bottom: 1px solid #e5e5e5; }

			.box-shadow { box-shadow: 0 .25rem .75rem rgba(0, 0, 0, .05); }

			.display-4{
				font-size: 40px;
			}

			@media (max-width: 480px) {
				#mobile-nav-toggle {
					top: 35px;
				}
			}


			</style>
		</head>
		<body>

	  <?php include_once 'header.training.php'; ?>

<!--
	<div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
		<h1 class="display-6">Harga Pabrik</h1>
		<p>Harga yang kami tawarkan terjangkau?</p>
	</div> -->

	<div class="container">
		<div class="pricing-header py-3 pt-md-5 pb-md-4 mx-auto text-center mt-80">
				<h1 class="display-4 mb-10">Harga Training Berdasarkan Varian <b>ACCURATE</b> </h1>
			<h1 class="display-5">STANDAR, DELUXE EDITION & RENE (SUPER)</h1>
		</div>

		<div class="card-deck mb-3 text-center">
			<div class="card mb-4 box-shadow">
				<div class="card-header bg-primary">
					<h4 class="my-0 font-weight-normal text-white">Onsite Super 1 Hari</h4>
				</div>
				<div class="card-body">
					<h1 class="card-title pricing-card-title" style="font-size:40px">Rp 1 Juta <small class="text-muted">/ hari</small></h1>
					<ul class="list-unstyled mt-3 mb-4">
						<li>1 Trainer</li>
						<li>Max. 7 Jam Kerja</li>
						<li>Transport Relatif</li>
					</ul>
					<button type="button" onclick="location.href='https://fac-institute.com/daftar-training/'" class="btn btn-lg btn-block btn-outline-primary">Order Now!</button>
				</div>
			</div>
			<div class="card mb-4 box-shadow">
				<div class="card-header bg-primary">
					<h4 class="my-0 font-weight-normal text-white">Paket Super 3 Hari</h4>
				</div>
				<div class="card-body">
					<h1 class="card-title pricing-card-title">Rp 2,85 Juta <small style="font-size:18px" class="text-muted">/ 3 hari</small></h1>
					<ul class="list-unstyled mt-3 mb-4">
						<li>1 Trainer</li>
						<li>Max. 7 Jam Kerja</li>
						<li>Transport Relatif</li>
					</ul>
					<button type="button" onclick="location.href='https://fac-institute.com/daftar-training/'" class="btn btn-lg btn-block btn-outline-primary ">Order Now!</button>
				</div>
			</div>

			<div class="card mb-4 box-shadow">
				<div class="card-header bg-primary">
					<h4 class="my-0 font-weight-normal text-white">Paket Super 5 Hari</h4>
				</div>
				<div class="card-body">
					<h1 class="card-title pricing-card-title">Rp 4,7 Juta <small style="font-size:18px" class="text-muted">/ 5 hari</small></h1>
					<ul class="list-unstyled mt-3 mb-4">
						<li>1 Trainer</li>
						<li>Max. 7 Jam Kerja</li>
						<li>Transport Relatif</li>
					</ul>
					<button type="button" onclick="location.href='https://fac-institute.com/daftar-training/'" class="btn btn-lg btn-block btn-outline-primary">Order Now!</button>
				</div>
			</div>
		</div>


		<div class="pricing-header py-3 pt-md-5 pb-md-4 mx-auto text-center">
			<h1 class="display-5">ENTERPRISE & DELUXE EDITION – CONTRACTOR (COMPLETE)</h1>
		</div>

		<div class="card-deck mb-3 text-center">
			<div class="card box-shadow" style="margin-bottom:50px">
				<div class="card-header bg-success">
					<h4 class="my-0 font-weight-normal text-white">Onsite Complete 1 Hari</h4>
				</div>
				<div class="card-body">
					<h1 style="font-size:24px" class="card-title pricing-card-title">Rp 1,3 Juta <small style="font-size:14px" class="text-muted">/ hari</small></h1>
					<ul class="list-unstyled mt-3 mb-4">
						<li>1 Trainer</li>
						<li>Max. 7 Jam Kerja</li>
						<li>Transport Relatif</li>
					</ul>
					<button type="button" onclick="location.href='https://fac-institute.com/daftar-training/'" class="btn btn-lg btn-block btn-outline-success">Order Now!</button>
				</div>
			</div>
			<div class="card box-shadow" style="margin-bottom:50px">
				<div class="card-header bg-success">
					<h4 class="my-0 font-weight-normal text-white">Paket Complete 5 Hari</h4>
				</div>
				<div class="card-body">
				<h1 style="font-size:24px" class="card-title pricing-card-title">Rp 6,3 Juta <small style="font-size:14px" class="text-muted">/ 5 hari</small></h1>
					<ul class="list-unstyled mt-3 mb-4">
						<li>1 Trainer</li>
						<li>Max. 7 Jam Kerja</li>
						<li>Transport Relatif</li>
					</ul>
					<button type="button" onclick="location.href='https://fac-institute.com/daftar-training/'" class="btn btn-lg btn-block btn-outline-success">Order Now!</button>
				</div>
			</div>
			<div class="card box-shadow" style="margin-bottom:50px">
				<div class="card-header bg-success">
					<h4 class="my-0 font-weight-normal text-white">Paket Complete 10 Hari</h4>
				</div>
				<div class="card-body">
				<h1 style="font-size:24px" class="card-title pricing-card-title">Rp 12,35 Juta <small style="font-size:14px" class="text-muted">/ 10 hari</small></h1>
					<ul class="list-unstyled mt-3 mb-4">
						<li>1 Trainer</li>
						<li>Max. 7 Jam Kerja</li>
						<li>Transport Relatif</li>
					</ul>
					<button type="button" onclick="location.href='https://fac-institute.com/daftar-training/'" class="btn btn-lg btn-block btn-outline-success">Order Now!</button>
				</div>
			</div>
			<div class="card box-shadow" style="margin-bottom:50px">
				<div class="card-header bg-success">
					<h4 style="font-size:17.5px" class="my-0 font-weight-normal text-white" >Paket Complete 20 Hari</h4>
				</div>
				<div class="card-body">
				<h1 style="font-size:24px" class="card-title pricing-card-title">Rp 23,4 Juta <small style="font-size:14px" class="text-muted">/ 20 hari</small></h1>
					<ul class="list-unstyled mt-3 mb-4">
						<li>1 Trainer</li>
						<li>Max. 7 Jam Kerja</li>
						<li>Transport Relatif</li>
					</ul>
					<button type="button" onclick="location.href='https://fac-institute.com/daftar-training/'" class="btn btn-lg btn-block btn-outline-success">Order Now!</button>
				</div>
			</div>
		</div>

		<div class="pricing-header py-3 pt-md-5 pb-md-4 mx-auto text-center">
			<h1 class="display-5">ACCURATE ONLINE (AOL)</h1>
		</div>

		<div class="card-deck mb-4 text-center">
			<div class="card mb-4 box-shadow">
				<div class="card-header bg-danger">
					<h4 class="my-0 font-weight-normal text-white">Onsite AOL 1 Hari</h4>
				</div>
				<div class="card-body">
					<h1 class="card-title pricing-card-title" style="font-size:38px">Rp 1,25 Juta <small class="text-muted">/ hari</small></h1>
					<ul class="list-unstyled mt-3 mb-4">
						<li>1 Trainer</li>
						<li>Max. 7 Jam Kerja</li>
						<li>Transport Relatif</li>
					</ul>
					<button type="button" onclick="location.href='https://fac-institute.com/daftar-training/'" class="btn btn-lg btn-block btn-outline-danger">Order Now!</button>
				</div>
			</div>
			<div class="card mb-4 box-shadow">
				<div class="card-header bg-danger">
					<h4 class="my-0 font-weight-normal text-white">Paket AOL 3 Hari</h4>
				</div>
				<div class="card-body">
					<h1 class="card-title pricing-card-title">Rp 3,5 Juta <small style="font-size:18px" class="text-muted">/ 3 hari</small></h1>
					<ul class="list-unstyled mt-3 mb-4">
						<li>1 Trainer</li>
						<li>Max. 7 Jam Kerja</li>
						<li>Transport Relatif</li>
					</ul>
					<button type="button" onclick="location.href='https://fac-institute.com/daftar-training/'" class="btn btn-lg btn-block btn-outline-danger">Order Now!</button>
				</div>
			</div>
			<div class="card mb-4 box-shadow">
				<div class="card-header bg-danger">
					<h4 class="my-0 font-weight-normal text-white" >Paket AOL 5 Hari</h4>
				</div>
				<div class="card-body">
					<h1 class="card-title pricing-card-title">Rp 6 Juta <small style="font-size:18px" class="text-muted">/ 5 hari</small></h1>
					<ul class="list-unstyled mt-3 mb-4">
						<li>1 Trainer</li>
						<li>Max. 7 Jam Kerja</li>
						<li>Transport Relatif</li>
					</ul>
					<button type="button" onclick="location.href='https://fac-institute.com/daftar-training/'" class="btn btn-lg btn-block btn-outline-danger">Order Now!</button>
				</div>
			</div>
		</div>


	</div>







			<!-- start footer Area -->
				    <?php include_once $data->homedir.'view/homepage/footer.homepage.php'; ?>
			<!-- End footer Area -->

			<script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/jquery-2.2.4.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/bootstrap.min.js"></script>
			<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
  			<script src="<?= $data->base_url ?>assets/homepage2019/js/easing.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/hoverIntent.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/superfish.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.ajaxchimp.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.magnific-popup.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/owl.carousel.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.sticky.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.nice-select.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/parallax.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/mail-script.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/main.js"></script>
		</body>
	</html>
