<!DOCTYPE html>
<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="<?= $data->base_url ?>images/homepage2019/elements/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="Rifzky Alam, Dino Damara">
		<!-- Meta Description -->
		<meta name="description" content="<?= $data->pagedesc ?>">
		<!-- Meta Keyword -->
		<meta name="keywords" content="<?= $data->pagekeywords ?>">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title><?= $data->title ?></title>
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
		<!--
			CSS
			============================================= -->
		<link rel="shortcut icon" href='https://fac-institute.com/_caramel/assets/img/g25992_9_kUd_icon.ico'>
		<link rel="shortcut icon" href='https://fac-institute.com/_caramel/assets/img/g25992_9_kUd_icon.ico'>
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/linearicons.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/owl.carousel.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/nice-select.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/magnific-popup.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/bootstrap.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/main.css">
		<style media="screen">
			.banner-content h1 {
			font-size: 38px;
			}
			.nav-menu a {
			padding: 15px 8px 1px 8px;
			font-size: 16px;
			font-weight: 500;
			}
			.nav-menu ul li a {
			font-size: 14px;
			}
			.menu-active {
			border-bottom: 5px solid #df003a;
			}
			.navbar {
			padding: 0.2rem 1rem;
			}
			:root {
			--white: #ffffff;
			--black: #000000;
			--blue:#0288d1;
			--gray:#ebebeb;
			--box-shadow1:0px 0px 18px 2px rgba(10, 55, 90, 0.15);
			}
			body{
			color: #637280;
			-moz-user-select: none;
			-webkit-user-select: none;
			user-select: none;
			}
			:focus{
			outline: 0px solid transparent !important;
			}
			.timeline {
			padding: 50px 0;
			position: relative;
			}
			.timeline-nodes {
			padding-bottom: 50px;
			position: relative;
			}
			.timeline-nodes:nth-child(even) {
			flex-direction: row-reverse;
			}
			.timeline h3, .timeline p {
			padding: 5px 15px;
			}
			.timeline h3{
			/* font-weight: lighter; */
			background: var(--blue);
			}
			.timeline p, .timeline time {
			/* color: var(--blue); */
			color: #000;
			font-weight: 300;
			font-size: 16px;
			}
			.timeline::before {
			content: "";
			display: block;
			position: absolute;
			top: 0;
			left: 50%;
			width: 0;
			border-left: 2px dashed var(--blue);
			height: 100%;
			z-index: 1;
			transform: translateX(-50%);
			}
			.timeline-content {
			border: 1px solid var(--blue);
			position: relative;
			border-radius: 0 0 10px 10px;
			box-shadow: 0px 3px 25px 0px rgba(10, 55, 90, 0.2)
			}
			.timeline-nodes:nth-child(odd) h3,
			.timeline-nodes:nth-child(odd) p {
			text-align: justify;
			}
			.timeline-nodes:nth-child(odd) .timeline-date {
			text-align: left;
			}
			.timeline-nodes:nth-child(even) .timeline-date {
			text-align: right;
			}
			.timeline-nodes:nth-child(odd) .timeline-content::after {
			content: "";
			position: absolute;
			top: 5%;
			left: 100%;
			width: 0;
			border-left: 10px solid var(--blue);
			border-top: 10px solid transparent;
			border-bottom: 10px solid transparent;
			}
			.timeline-nodes:nth-child(even) .timeline-content::after {
			content: "";
			position: absolute;
			top: 5%;
			right: 100%;
			width: 0;
			border-right: 10px solid var(--blue);
			border-top: 10px solid transparent;
			border-bottom: 10px solid transparent;
			}
			.timeline-image {
			position: relative;
			z-index: 100;
			}
			.timeline-image::before {
			content: "";
			width: 100px;
			height: 100px;
			border: 2px dashed var(--blue);
			border-radius: 50%;
			display: block;
			position: absolute;
			top: 50%;
			left: 50%;
			transform: translate(-50%,-50%);
			background-color: #fff;
			z-index: 1;
			}
			.timeline-image img {
			position: relative;
			z-index: 100;
			width: 100%;
			}
			/*small device style*/
			@media (max-width: 767px) {
			.timeline-nodes:nth-child(odd) h3,
			.timeline-nodes:nth-child(odd) p {
			text-align: left
			}
			.timeline-nodes:nth-child(even) {
			flex-direction: row;
			}
			.timeline::before {
			content: "";
			display: block;
			position: absolute;
			top: 0;
			left: 4%;
			width: 0;
			border-left: 2px dashed var(--blue);
			height: 100%;
			z-index: 1;
			transform: translateX(-50%);
			}
			.timeline h3 {
			font-size: 1.7rem;
			}
			.timeline p {
			font-size: 14px;
			}
			.timeline-image {
			position: absolute;
			left: 0%;
			top: 60px;
			/*transform: translateX(-50%;);*/
			}
			.timeline-nodes:nth-child(odd) .timeline-content::after {
			content: "";
			position: absolute;
			top: 5%;
			left: auto;
			right: 100%;
			width: 0;
			border-left: 0;
			border-right: 10px solid var(--blue);
			border-top: 10px solid transparent;
			border-bottom: 10px solid transparent;
			}
			.timeline-nodes:nth-child(even) .timeline-content::after {
			content: "";
			position: absolute;
			top: 5%;
			right: 100%;
			width: 0;
			border-right: 10px solid var(--blue);
			border-top: 10px solid transparent;
			border-bottom: 10px solid transparent;
			}
			.timeline-nodes:nth-child(even) .timeline-date {
			text-align: left;
			}
			.timeline-image::before {
			width: 65px;
			height: 65px;
			}
			}
			/*extra small device style */
			@media (max-width: 575px) {
			.timeline::before {
			content: "";
			display: block;
			position: absolute;
			top: 0;
			left: 3%;
			}
			.timeline-image {
			position: absolute;
			left: -5%;
			}
			.timeline-image::before {
			width: 60px;
			height: 60px;
			}
			}

			@media (max-width: 480px) {
				#mobile-nav-toggle {
					top: 35px;
				}
			}


		</style>
    <link href="https://fac-institute.com/assets/homepage2019/css/aos.css" rel="stylesheet">
	</head>
	<body>
  <?php include_once $data->homedir.'view/homepage/training/header.training.php'; ?>
		<!-- <section style='background-color: rgba(255,255,255,0);
		text-align: center;
    background-image: url(https://cpssoft.com/accurate-online/wp-content/uploads/sites/3/2018/10/BG-fitur-AOL-min.png);
    background-position: center bottom;
    background-repeat: no-repeat;
    padding-top: 120px;
    padding-bottom: 80px;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;'>
			<div class="container">
				<div class="row d-flex align-items-center justify-content-center">
					<div class="col-lg-6 no-padding about-right">
						<h1 class="text-white" style="font-size:34px">Cara Pemesanan</h1>
					</div>
			</div>
			</div>
		</section> -->
		<!-- End banner Area -->
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="menu-content mt-80 pb-10 col-lg-10">
					<div class="title text-center">
						<h1 class="mt-50 mb-10">HOW TO ORDER?</h1>
						<p style="font-size:18px">caranya mudah kok!</p>
					</div>
				</div>
			</div>
			<div class="timeline mb-40">
				<div class="row no-gutters justify-content-end justify-content-md-around align-items-start  timeline-nodes" style="padding-bottom: 20px;">
					<div class="col-10 col-md-5 order-3 order-md-1 timeline-content">
						<h3 class=" text-light">Order</h3>
						<p>Pemesanan training dapat dilakukan dengan cara mengunjungi official website <b>FAC Institute</b> atau Hubungi
							Marketing kami di no. telp : <a href="tel://081290083983" title="Klik/Sentuh untuk menelpon"><b>081290083983</b></a> atau <a href="tel://081219115005" title="Klik/Sentuh untuk menelpon"><b>081219115005</b></a>. Selanjutnya marketing kami akan melakukan penjadwalan training Anda sesuai dengan kesepakatan bersama.
						</p>
					</div>
					<div class="col-2 col-sm-1 order-2 timeline-image text-md-center">
						<img src="<?= $data->base_url ?>_hazelnut/img/rika_Fotor.jpg" class="rounded-circle" alt="img">
					</div>
					<div class="col-10 col-md-5 order-1 order-md-3 py-3 timeline-date">
						<time style="font-weight:600">Siti Nurhasanah (marketing 1)</time>
					</div>
				</div>

				<div class="row no-gutters justify-content-end justify-content-md-around align-items-start timeline-nodes" >
					<div class="col-10 col-md-5 order-3 order-md-1 ">
					</div>
					<div class="col-2 col-sm-1 order-2 timeline-image text-md-center">
						<img src="http://fac-institute.com/images/homepage/_DSC4131_Fotor_Fotor.jpg" class="rounded-circle" alt="img">
					</div>
					<div class="col-10 col-md-5 order-1 order-md-3 py-3 timeline-date">
						<time style="font-weight:600">Siti Sarah (marketing 2)</time>
					</div>
				</div>


				<div class="row no-gutters justify-content-end justify-content-md-around align-items-start  timeline-nodes">
					<div class="col-10 col-md-5 order-3 order-md-1 timeline-content">
						<h3 class=" text-light">Pembayaran</h3>
						<p>Pembayaran melalui transfer antar bank online <br><span style="font-style:italic;font-size:12px">*pembayaran dilakukan sebelum training dimulai</span></p>
					</div>
					<div class="col-2 col-sm-1 order-2 timeline-image text-md-center">
						<img src="<?= $data->base_url ?>_caramel/assets/img/trainer/DSC_0139 - Copy.jpg" class="rounded-circle" alt="img">
					</div>
					<div class="col-10 col-md-5 order-1 order-md-3 py-3 timeline-date">
						<time style="font-weight:600">Imelia Silviana (finance)</time>
					</div>
				</div>
				<div class="row no-gutters justify-content-end justify-content-md-around align-items-start  timeline-nodes">
					<div class="col-10 col-md-5 order-3 order-md-1 timeline-content">
						<h3 class=" text-light">Training</h3>
						<p>Kami mengirim trainer berpengalaman untuk implementasi ACCURATE di perusahaan Anda</p>
					</div>
					<div class="col-2 col-sm-1 order-2 timeline-image text-md-center">
						<img src="https://fac-institute.com/images/homepage2019/236842.png" class="img-fluid" alt="img">
					</div>
					<div class="col-10 col-md-5 order-1 order-md-3 py-3 timeline-date">
						<time style="font-weight:600">FAC TEAM (trainers)</time>
					</div>
				</div>
				<div class="row no-gutters justify-content-end justify-content-md-around align-items-start  timeline-nodes">
					<div class="col-10 col-md-5 order-3 order-md-1 timeline-content">
						<h3 class=" text-light">Support</h3>
						<p>Setelah training selesai, tim kami siap memberi dukungan penuh untuk pertanyaan - pertanyaan yang mungkin saja timbul saat Anda sudah mulai menggunakan accurate.</p>
					</div>
					<div class="col-2 col-sm-1 order-2 timeline-image text-md-center">
						<img src="<?= $data->base_url ?>_hazelnut/img/juli_Fotor.jpg" width="100%" class="rounded-circle" alt="img">
					</div>
					<div class="col-10 col-md-5 order-1 order-md-3 py-3 timeline-date">
						<time style="font-weight:600">Juliana Dwi Asri (customer support)</time>
					</div>
				</div>
			</div>
		</div>

		  <?php include_once $data->homedir.'view/homepage/call.me.php'; ?>


		<!-- start footer Area -->
	  <?php include_once $data->homedir.'view/homepage/footer.homepage.php'; ?>
		<!-- End footer Area -->
		<script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/jquery-2.2.4.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/bootstrap.min.js"></script>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/easing.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/hoverIntent.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/superfish.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.ajaxchimp.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.magnific-popup.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/owl.carousel.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.sticky.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.nice-select.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/parallax.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/mail-script.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/main.js"></script>
		<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
		<script>
		AOS.init();
		$('.test1').click(function() {
				var sectionTo = $(this).attr('href');
				$('html, body').animate({
					scrollTop: $(sectionTo).offset().top
				}, 1000);
		});
		</script>
	</body>
</html>
