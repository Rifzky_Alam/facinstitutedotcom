<header class="fixed-top" id="header">
	<?php include_once $data->homedir.'view/homepage/parentheader.homepage.php'; ?>
	<div class="container">
		<div class="row align-items-center justify-content-between d-flex">
			<div id="logo">
				<a href="<?= $data->base_url ?>training"><img style="padding-top:10px" width="200px" src="<?= $data->base_url ?>images/homepage2019/facnew.png" alt="FAC-Institute" title="" /></a>
			</div>

			<nav id="nav-menu-container">
				<ul class="nav-menu">
					<?php if ($data->judul=='traininghome'): ?>
						<li class="menu-active">
							<a href="<?= $data->base_url ?>training">
							BERANDA
							</a>
						</li>
						<li>
							<a href="<?= $data->base_url ?>training/carapesan">CARA PEMESANAN</a>
						</li>
						<li>
							<a href="<?= $data->base_url ?>training/daftarharga">
								HARGA
							</a>
						</li>
					<?php elseif ($data->judul=='trainingcarapesan'): ?>
						<li>
							<a href="<?= $data->base_url ?>training">
							BERANDA
							</a>
						</li>
						<li class="menu-active">
							<a href="<?= $data->base_url ?>training/carapesan">
							CARA PEMESANAN
							</a>
						</li>
						<li>
							<a href="<?= $data->base_url ?>training/daftarharga">
							HARGA
							</a>
						</li>
					<?php elseif ($data->judul=='daftarharga'): ?>
						<li>
							<a href="<?= $data->base_url ?>training">
							BERANDA
							</a>
						</li>
						<li>
							<a href="<?= $data->base_url ?>training/carapesan">
							CARA PEMESANAN
							</a>
						</li>
						<li class="menu-active">
							<a href="<?= $data->base_url ?>training/daftarharga">
							HARGA
							</a>
						</li>
					<?php else: ?>
						<li>
							<a href="<?= $data->base_url ?>training">
							BERANDA
							</a>
						</li>
						<li>
							<a href="<?= $data->base_url ?>training/carapesan">
							CARA PEMESANAN
							</a>
						</li>
						<li>
							<a href="<?= $data->base_url ?>training/daftarharga">
							HARGA
							</a>
						</li>
					<?php endif ?>

					<!-- <li><a href="materi-training.html">MATERI TRAINING</a></li> -->
				</ul>
			</nav>
		</div>
	</div>
</header>
