<header class="fixed-top" id="header">
  <?php include_once $data->homedir.'view/homepage/sub.header.php'; ?>
	<div class="container">
		<div class="row align-items-center justify-content-between d-flex">
			<div id="logo">
				<a href="<?= $data->base_url ?>"><img style="padding-top:10px" width="200px" src="<?= $data->base_url ?>images/homepage2019/facnew.png" alt="<?= 'Logo FAC Institute' ?>" title="Logo FAC Institute" /></a>
			</div>

			<nav id="nav-menu-container">
				<ul class="nav-menu">
					<li class="menu-active"><a href="<?= $data->base_url.'facteam' ?>">FAC TEAM</a></li>
					<!-- <li><a href="<?= $data->base_url.'facteam/gallery' ?>">GALERI</a></li> -->
					<li><a href="<?= $data->base_url.'login' ?>">LOGIN</a></li>
				</ul>
			</nav>
		</div>
	</div>
</header>
