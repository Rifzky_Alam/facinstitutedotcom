<!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>
  <!-- Mobile Specific Meta -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Favicon-->
  <link rel="shortcut icon" href="<?= $data->base_url ?>images/homepage2019/elements/fav.png">
  <!-- Author Meta -->
  <meta name="author" content="Rifzky Alam, Dino Damara">
  <!-- Meta Description -->
  <meta name="description" content="<?= $data->pagedesc ?>">
  <!-- Meta Keyword -->
  <meta name="keywords" content="<?= $data->pagekeywords ?>">
  <!-- meta character set -->
  <meta charset="UTF-8">
  <!-- Site Title -->
  <title><?= $data->title ?></title>

  <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
    <!--
    CSS
    ============================================= -->
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/linearicons.css">
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/owl.carousel.css">
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/nice-select.css">
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/magnific-popup.css">
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/bootstrap.css">
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/main.css">
    <style media="screen">
    .banner-content h1 {
        font-size: 38px;
    }
    .nav-menu a {
        padding: 15px 8px 1px 8px;
        font-size: 16px;
        font-weight: 500;
    }
    .nav-menu ul li a {
        font-size: 14px;
    }

    .menu-active {
      border-bottom: 5px solid #df003a;
    }

    .navbar {
        padding: 0.2rem 1rem;
    }


    .circle {
      padding: 13px 20px;
      border-radius: 50%;
      background-color: #f74e4e;
      color: #fff;
      max-height: 50px;
      z-index: 2;
    }

    .how-it-works.row .col-2 {
      align-self: stretch;
    }
    .how-it-works.row .col-2::after {
      content: "";
      position: absolute;
      border-left: 3px solid #ED8D8D;
      z-index: 1;
    }
    .how-it-works.row .col-2.bottom::after {
      height: 50%;
      left: 50%;
      top: 50%;
    }
    .how-it-works.row .col-2.full::after {
      height: 100%;
      left: calc(50% - 3px);
    }
    .how-it-works.row .col-2.top::after {
      height: 50%;
      left: 50%;
      top: 0;
    }


    .timeline div {
      padding: 0;
      height: 40px;
    }
    .timeline hr {
      border-top: 3px solid #ED8D8D;
      margin: 0;
      top: 17px;
      position: relative;
    }
    .timeline .col-2 {
      display: flex;
      overflow: hidden;
    }
    .timeline .corner {
      border: 3px solid #ED8D8D;
      width: 100%;
      position: relative;
      border-radius: 15px;
    }
    .timeline .top-right {
      left: 50%;
      top: -50%;
    }
    .timeline .left-bottom {
      left: -50%;
      top: calc(50% - 3px);
    }
    .timeline .top-left {
      left: -50%;
      top: -50%;
    }
    .timeline .right-bottom {
      left: 50%;
      top: calc(50% - 3px);
    }

    .rounded-circle {
      border-radius: 50%;
      max-width: 240px;
      display: block;
      margin: 0 auto;
    }


    .imgF {
      border:solid 2px;
      border-bottom-color:#ffe;
      border-left-color:#eed;
      border-right-color:#eed;
      border-top-color:#ccb;
      max-height:100%;
      max-width:100%;
    }

    .frame {
      background-color:#ddc;
      border:solid 5vmin #eee;
      border-bottom-color:#fff;
      border-left-color:#eee;
      border-radius:2px;
      border-right-color:#eee;
      border-top-color:#ddd;
      box-shadow:0 0 5px 0 rgba(0,0,0,.25) inset, 0 5px 10px 5px rgba(0,0,0,.25);
      box-sizing:border-box;
      display:inline-block;
      margin:10vh 10vw;
      height:60vh;
      padding:7vmin;
      position:relative;
      text-align:center;
      &:before {
        border-radius:2px;
        bottom:-2vmin;
        box-shadow:0 2px 5px 0 rgba(0,0,0,.25) inset;
        content:"";
        left:-2vmin;
        position:absolute;
        right:-2vmin;
        top:-2vmin;
      }
      &:after {
        border-radius:2px;
        bottom:-2.5vmin;
        box-shadow: 0 2px 5px 0 rgba(0,0,0,.25);
        content:"";
        left:-2.5vmin;
        position:absolute;
        right:-2.5vmin;
        top:-2.5vmin;
      }
    }

    @media (max-width: 480px) {
  .frame {
    height:40vh;
    }
}


    </style>




    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
  </head>
  <body>

    <?php include_once $data->homedir.'view/homepage/facteam/header.facteam.php'; ?>


      <div  class="container-fluid mt-120 ">
        <div class="row justify-content-center align-items-center" >
          <div class="col-lg-12 text-center">
            <p style="font-size:32px;font-style:italic">BEST TRAINER <?= $data->namabulan.' '.$data->tahun ?></p>
            <p style="font-size:22px;font-style:italic;font-weight:bold"><?= $data->namabest ?></p>
          </div>
          <div class="col-lg-12 text-center" style="max-width:700px">
            <!-- <img class="img-fluid mb-20" width="100%" data-aos="fade-up" src="https://fac-institute.com/images/homepage2019/yamaap.jpg" alt=""> -->

            <div class="frame">
  <img class="imgF" src="https://fac-institute.com/images/member/<?= $data->fotobest ?>" alt="<?= $data->namabest ?>" />
</div>
          </div>
        </div>
      </div>

      <section  class="we-offer-area section-gap" id="offer">
        <div class="container">
          <div class="row d-flex justify-content-center">
            <div class="menu-content pb-60 col-lg-10">
              <div class="title text-center">
                <h1 class="mb-20">OUR TEAM</h1>
                <p style="font-size:32px;font-style:italic">PROFESSIONAL TRAINERS</p>
              </div>
            </div>
          </div>

          <div class="row">
            
            <?php foreach($data->listtrainer as $key): ?>
                <div class="col-xl-3 col-md-12 mb-4">
                  <div class="card border-0 shadow">
                    <img id="<?= md5($key['username']) ?>" src="<?= $data->base_url.'images/member/'.$key['picture'] ?>" class="rounded-circle" alt="<?= $key['nama'] ?>">
                    <div class="card-body text-center">
                      <h5 class="card-title mb-0"><?= $key['nama']; ?></h5>
                    </div>
                  </div>
                </div>
            <?php endforeach ?>
            
            

            
            </div>

        </div>
  </section>


    <section class="mb-80">
        <div class="container-fluid">
          <div class="row justify-content-center align-items-center" >
            <div class="col-lg-12 text-center mb-30">
              <p style="font-size:32px;font-style:italic">MARKETING</p>
            </div>
            <?php foreach($data->listmarketing as $key): ?>
                <div class="col-lg-6 text-center" style="max-width:250px">
                  <img id="<?= md5($key['username']) ?>" class="img-fluid rounded-circle mt-20 mb-20" src="<?= $data->base_url.'images/member/'.$key['picture'] ?>" alt="<?= $key['nama'] ?>">
                  <h5 class="card-title mt-20"><?= $key['nama'] ?></h5>
                </div>
            <?php endforeach ?>
            
          </div>
        </div>
      </section>

    <section>
      <div class="container-fluid mb-40">
        <div class="row justify-content-center align-items-center" >
          <div class="col-lg-12 text-center mb-30">
            <p style="font-size:32px;font-style:italic">CUSTOMER SUPPORT</p>
          </div>
          
          <?php foreach($data->listsupports as $key): ?>
            <div class="col-lg-6 text-center" style="max-width:250px">
                <img id="<?= md5($key['username']) ?>" class="img-fluid rounded-circle mt-20 mb-20" src="<?= $data->base_url.'images/member/'.$key['picture'] ?>" alt="<?= $key['nama'] ?>">
                <h5 class="card-title mt-20"><?= $key['nama'] ?></h5>
            </div>
            <?php endforeach ?>
          
        </div>
      </div>
    </section>




    <?php include_once $data->homedir.'view/homepage/footer.homepage.php'; ?>

    <script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/jquery-2.2.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/easing.min.js"></script>
    <script src="<?= $data->base_url ?>assets/homepage2019/js/hoverIntent.js"></script>
    <script src="<?= $data->base_url ?>assets/homepage2019/js/superfish.min.js"></script>
    <script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.ajaxchimp.min.js"></script>
    <script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.magnific-popup.min.js"></script>
    <script src="<?= $data->base_url ?>assets/homepage2019/js/owl.carousel.min.js"></script>
    <script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.sticky.js"></script>
    <script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.nice-select.min.js"></script>
    <script src="<?= $data->base_url ?>assets/homepage2019/js/parallax.min.js"></script>
    <script src="<?= $data->base_url ?>assets/homepage2019/js/mail-script.js"></script>
    <script src="<?= $data->base_url ?>assets/homepage2019/js/main.js"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
    AOS.init();
    $('.test1').click(function() {
        var sectionTo = $(this).attr('href');
        $('html, body').animate({
          scrollTop: $(sectionTo).offset().top
        }, 1000);
    });
    </script>

    <script>
    $.ajax({
        type: "GET",
        url: "https://fac-institute.com/administrasi/user/api?req=allpic",
        dataType: 'json',
        cache: false,
        success: function(data) {
            console.log(data.listdata);
            var pic_url = "https://fac-institute.com/images/member/";
            $("#img0").attr("src",pic_url + data.listdata[14].picture);
            $("#img1").attr("src",pic_url + data.listdata[20].picture);
            $("#img2").attr("src",pic_url + data.listdata[17].picture);
            $("#img3").attr("src",pic_url + data.listdata[15].picture);
            $("#img4").attr("src",pic_url + data.listdata[10].picture);
            $("#img5").attr("src",pic_url + data.listdata[19].picture);
            $("#img6").attr("src",pic_url + data.listdata[18].picture);
            $("#img7").attr("src",pic_url + data.listdata[2].picture);
            $("#img8").attr("src",pic_url + data.listdata[5].picture);
            $("#img9").attr("src",pic_url + data.listdata[0].picture);
            $("#img10").attr("src",pic_url + data.listdata[3].picture);
            $("#img11").attr("src",pic_url + data.listdata[1].picture);
            $("#img12").attr("src",pic_url + data.listdata[9].picture);
            $("#img13").attr("src",pic_url + data.listdata[8].picture);
            $("#img14").attr("src",pic_url + data.listdata[6].picture);
            $("#img15").attr("src",pic_url + data.listdata[7].picture);
            // $("#img16").attr("src",pic_url + data.listdata[14].picture);


        }
    });
    </script>
  </body>
</html>
