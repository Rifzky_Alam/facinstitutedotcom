<?php include_once '../baseurl.php'; ?>
<?php

if (isset($_POST['in'])) {
  print_r($_POST['in']);
}

?>
<html class="no-js" lang="id">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="google-site-verification" content="EfNVtXecxElbcbugY59SeKXJuMzO67bjVvTvlHCc_CM" />
    <meta name="msvalidate.01" content="D925497D8F382164FF0CE0267D381FDA" />
    <meta name="thumbnailUrl" content="https://fac-institute.com/_caramel/assets/img/g44508.png" />
    <meta name="description" content="FAC - Kami Melayani Training Software Akuntansi ACCURATE dan Accounting Service Untuk Perusahaan Anda" />
    <meta content="First Asian Consulting. Konsultan Akuntansi & Lembaga Pendidikan Komputerisasi Akuntansi" />
    <meta name="keywords" content="Training,Akuntansi,ACCURATE,Jasa">
    <meta name="author" content="Rifzky Alam, Dino Damara">
    <title>FAC Institute</title>
    <link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/w3.css">
    <link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/animate.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,100,300,500,600,700c" type="text/css" />

<style>
 .w3-top{
   font-family: "Raleway", sans-serif;
   z-index: 2;
 }

 body, html, h3 {
     font-family: 'PT Sans', sans-serif;
 }
 p,li{
   /* font-size: 16px; */
 }

.mySlides {display:none;}

img {margin-bottom: -7px}
 .w3-row-padding img {margin-bottom: 12px}

/* li {
  padding-left: 1em;
  text-indent: -.7em;
} */

.steps {
  /* margin: 40px; */
  padding: 0;
  overflow: hidden;
}
.steps a {
  color: white;
  text-decoration: none;
}
.steps em {
  display: block;
  font-size: 1.1em;
  font-weight: bold;
}
.steps li {
  float: left;
  margin-left: 0;
  /* width: 150px; */
  width: 45%;
  height: 70px; /* total height */
  list-style-type: none;
  padding: 8px 5px 5px 30px; /* padding around text, last should include arrow width */
  border-right: 3px solid white; /* width: gap between arrows, color: background of document */
  position: relative;
}
/* remove extra padding on the first object since it doesn't have an arrow to the left */
.steps li:first-child {
  padding-left: 12px;
}
/* white arrow to the left to "erase" background (starting from the 2nd object) */
.steps li:nth-child(n+2)::before {
  position: absolute;
  top:0;
  left:0;
  display: block;
  border-left: 25px solid white; /* width: arrow width, color: background of document */
  border-top: 40px solid transparent; /* width: half height */
  border-bottom: 40px solid transparent; /* width: half height */
  width: 0;
  height: 0;
  content: " ";
}
/* colored arrow to the right */
.steps li::after {
  z-index: 1;
  position: absolute;
  top: 0;
  right: -25px; /* arrow width (negated) */
  display: block;
  border-left: 25px solid #7c8437; /* width: arrow width */
  border-top: 40px solid transparent; /* width: half height */
  border-bottom: 40px solid transparent; /* width: half height */
  width:0;
  height:0;
  content: " ";
}

/* Setup colors (both the background and the arrow) */


#li_act  { background-color: #1a9cb7; }
#li_act::after { border-left-color: #1a9cb7; }

#li_off { background-color: #EBEBEB; }
#li_off::after {	border-left-color: #EBEBEB; }




 </style>





<link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/style.css">
<script src="<?php echo getBaseUrl() ?>assets/homepage/js/jquery-2.1.4.min.js"></script>


<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M4XWFZM');</script>
<!-- End Google Tag Manager -->


</head>
<body>
  <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M4XWFZM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

    <h4>header</h4>
    <?php include_once '../top-nav.php'; ?>


<div class="w3-container" style="padding:28px 16px">
  <div class="w3-row-padding">

    <div class="w3-col">
      <h3 style="font-weight:bold" >Accounting Service</h3>


<ul class="steps steps-5">
  <li id="li_act"><a href="#" title=""><em>Step 5: XXXXXXXX</em><span>Et nequ a quam turpis duisi</span></a></li>
  <li id="li_off"><a href="#" title=""><em>Step 1</em><span>Et nequ a quam turpis duisi</span></a></li>

</ul>

<form class="w3-container w3-card-4" method="post"  action="">
  <h2 class="w3-text-blue">Form input perusahaan Step-1</h2>
  <hr>
  <p>
    <label class="w3-text-blue" for="namausaha"><b>Nama Anda</b></label>
    <input class="w3-input w3-border" type="text" name="in[namausaha]" class="form-control" id="namausaha" placeholder="Nama Usaha Anda" required>
  </p>
  <p>
    <label class="w3-text-blue" for="emailusaha"><b>Email Anda</b></label>
    <input class="w3-input w3-border" type="email" name="in[email]" class="form-control" id="emailusaha" placeholder="Email aktif, biarkan kosong bila tidak ada." required>
  </p>
  <p>
    <label class="w3-text-blue" for="telpusaha"><b>No. Telepon Anda</b></label>
    <input class="w3-input w3-border" type="text" name="in[telepon]" class="form-control" id="telpusaha" placeholder="Telepon Aktif, kosongkan bila tidak ada." required>
  </p>
  <p>
    <label class="w3-text-blue" for="alamatusaha"><b>Alamat </b></label>
    <textarea class="w3-input w3-border" id="alamatusaha" name="in[alamat]" class="form-control" placeholder="Alamat di perusahaan anda"></textarea>
  </p>
  <p>
    <label class="w3-text-blue" for="marketing"><b>Marketing</b></label>
    <input class="w3-input w3-border" type="text" id="marketing" name="in[marketing]" class="form-control" placeholder="Dari siapa anda tahu fac-institute.com" required>
  </p>
  <p>
    <label class="w3-text-blue" for="agenda"><b>Agenda Training/Kebutuhan Anda</b></label>
    <input class="w3-input w3-border" type="text" name="in[agenda]" class="form-control" placeholder="Tuliskan apa kebutuhan anda di dalam software Accurate atau jasa Accounting Service Contoh: setup database, penjelasan fitur accurate, entry data" required>
  </p>
<label class="w3-text-blue" for=""><b>Pengguna</b></label>

<p>
    <input class="w3-radio" type="radio" name="in[pengguna]" value="lama" checked>
    <label>Lama</label>
    <input class="w3-radio" type="radio" name="in[pengguna]" value="baru">
    <label>Baru</label>
</p>



  <img src="https://assets.entrepreneur.com/content/3x2/1300/20141204182214-google-says-rip-captcha-sort-of-1.jpeg?width=750&crop=16:9" class="w3-border" alt="Norway" style="padding:4px;width:150px">

  <input class="w3-input w3-border" type="text" style="width:150px">
  <p>Padding added wil be inside the borders:</p>

  <p><button class="w3-btn w3-blue">Register</button></p>

</form>



    </div>
  </div>
</div>




    <?php //include_once 'modal-order.php'; ?>




</body>
</html>
