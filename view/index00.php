<?php include_once '../baseurl.php'; ?>
<?php

if (isset($_POST['in'])) {
  print_r($_POST['in']);
}

?>
<html class="no-js" lang="id">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="google-site-verification" content="EfNVtXecxElbcbugY59SeKXJuMzO67bjVvTvlHCc_CM" />
    <meta name="msvalidate.01" content="D925497D8F382164FF0CE0267D381FDA" />
    <meta name="thumbnailUrl" content="https://fac-institute.com/_caramel/assets/img/g44508.png" />
    <meta name="description" content="FAC - Kami Melayani Training Software Akuntansi ACCURATE dan Accounting Service Untuk Perusahaan Anda" />
    <meta content="First Asian Consulting. Konsultan Akuntansi & Lembaga Pendidikan Komputerisasi Akuntansi" />
    <meta name="keywords" content="Training,Akuntansi,ACCURATE,Jasa">
    <meta name="author" content="Rifzky Alam, Dino Damara">
    <title>FAC Institute</title>
    <link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/w3.css">
    <link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/animate.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,100,300,500,600,700c" type="text/css" />

<style>
 .w3-top{
   font-family: "Raleway", sans-serif;
   z-index: 2;
 }

 body, html, h3 {
     font-family: 'PT Sans', sans-serif;
 }
 p,li{
   /* font-size: 16px; */
 }

.mySlides {display:none;}

img {margin-bottom: -7px}
 .w3-row-padding img {margin-bottom: 12px}

/* li {
  padding-left: 1em;
  text-indent: -.7em;
} */

.steps {
  /* margin: 40px; */
  padding: 0;
  overflow: hidden;
}
.steps a {
  color: white;
  text-decoration: none;
}
.steps em {
  display: block;
  font-size: 1.1em;
  font-weight: bold;
}
.steps li {
  float: left;
  margin-left: 0;
  /* width: 150px; */
  width: 45%;
  height: 70px; /* total height */
  list-style-type: none;
  padding: 8px 5px 5px 30px; /* padding around text, last should include arrow width */
  border-right: 3px solid white; /* width: gap between arrows, color: background of document */
  position: relative;
}
/* remove extra padding on the first object since it doesn't have an arrow to the left */
.steps li:first-child {
  padding-left: 12px;
}
/* white arrow to the left to "erase" background (starting from the 2nd object) */
.steps li:nth-child(n+2)::before {
  position: absolute;
  top:0;
  left:0;
  display: block;
  border-left: 25px solid white; /* width: arrow width, color: background of document */
  border-top: 40px solid transparent; /* width: half height */
  border-bottom: 40px solid transparent; /* width: half height */
  width: 0;
  height: 0;
  content: " ";
}
/* colored arrow to the right */
.steps li::after {
  z-index: 1;
  position: absolute;
  top: 0;
  right: -25px; /* arrow width (negated) */
  display: block;
  border-left: 25px solid #7c8437; /* width: arrow width */
  border-top: 40px solid transparent; /* width: half height */
  border-bottom: 40px solid transparent; /* width: half height */
  width:0;
  height:0;
  content: " ";
}

/* Setup colors (both the background and the arrow) */


#li_act  { background-color: #1a9cb7; }
#li_act::after { border-left-color: #1a9cb7; }

#li_off { background-color: #EBEBEB; }
#li_off::after {	border-left-color: #EBEBEB; }




 </style>





<link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/style.css">
<script src="<?php echo getBaseUrl() ?>assets/homepage/js/jquery-2.1.4.min.js"></script>


<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M4XWFZM');</script>
<!-- End Google Tag Manager -->

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCUn6Q6sg0JVJBlgiz4tL2kF1zmVoC0ScE&libraries=places&sensor=true"></script>
<script type="text/javascript">
var latitude;
var longitude

function getLocation(){
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        alert("Geolocation is not supported by this browser.");
    }
}

function showPosition(position) {
     var myCurrentLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
     return myCurrentLocation;
}

function cobaaa() {
    getLocation();

    return [position.coords.latitude,position.coords.longitude];
}

function showError(error){
    switch(error.code){
        case error.PERMISSION_DENIED:
            //alert("User denied the request for Geolocation.");

            var myCurrentLocation = new google.maps.LatLng(-6.248123, 106.907952);
            var mapProp = {
                center:myCurrentLocation,
                zoom:11,
                mapTypeId:google.maps.MapTypeId.ROADMAP
            };

            var input = document.getElementById('alamatz');
            input.value = "";
            var sBox = new google.maps.places.SearchBox(input);
                //marker
            var myMarker=new google.maps.Marker({
                position:myCurrentLocation,
            });



            var markers = [];

            markers.push(myMarker);



            var map = new google.maps.Map(document.getElementById("map-canvas"),mapProp);
            var infowindow = new google.maps.InfoWindow({
                content:"FAC Institute Office"
            });
            infowindow.open(map,myMarker);

            myMarker.setMap(map);

            //event listener

            //event listener for search box
            sBox.addListener('places_changed', function() {
              var places = sBox.getPlaces();
              if (places.length == 0) {
                return;
              };
              deleteMarkers();

              // For each place, get the icon, name and location.
              var bounds = new google.maps.LatLngBounds();
              places.forEach(function(place) {
                var icon = {
                  url: place.icon,
                  size: new google.maps.Size(71, 71),
                  origin: new google.maps.Point(0, 0),
                  anchor: new google.maps.Point(17, 34),
                  scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                markers.push(new google.maps.Marker({
                  map: map,
                  //icon: icon,
                  title: place.name,
                  position: place.geometry.location
                }));

                if (place.geometry.viewport) {
                  // Only geocodes have viewport.
                  bounds.union(place.geometry.viewport);
                } else {
                  bounds.extend(place.geometry.location);
                }
              });
              map.fitBounds(bounds);

            });

            //event listener for google map
            google.maps.event.addListener(map, 'click', function(event) {
            placeMarker(event.latLng);
            });


            //marker

            // Sets the map on all markers in the array.
            function setMapOnAll(map) {
              for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
              }
            }

            function placeMarker(location){
                deleteMarkers();
                var alamatku = document.getElementById('alamatz');
                var myMarker = new google.maps.Marker({
                    position: location,
                    map: map
                });
                alamatku.value = location;
                var myString = alamatku.value;
                myString=myString.replace('(','');myString=myString.replace(')','');myString=myString.replace(', ',',');

                alamatku.value = myString;
                markers.push(myMarker);
                //alert(markers.length);
            }
            // Deletes all markers in the array by removing references to them.
            function deleteMarkers() {
              clearMarkers();
              markers = [];
            }

            function clearMarkers() {
              setMapOnAll(null);
            }



            break;
        case error.POSITION_UNAVAILABLE:
            //x.innerHTML = "Location information is unavailable."
            alert('Location information is unavailable.');
            break;
        case error.TIMEOUT:
            alert('The request to get user location timed out.');
            break;
        case error.UNKNOWN_ERROR:
            //x.innerHTML = "An unknown error occurred."
            alert('An unknown error occurred.');
            break;
    }
}


function initialize() {



    if (navigator.geolocation) {


        navigator.geolocation.getCurrentPosition(function(position){
            var myCurrentLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
            var mapProp = {
                center:myCurrentLocation,
                zoom:16,
                mapTypeId:google.maps.MapTypeId.ROADMAP
            };

            var input = document.getElementById('alamatz');
            input.value = position.coords.latitude + ',' + position.coords.longitude;
            var sBox = new google.maps.places.SearchBox(input);
                //marker
            var myMarker=new google.maps.Marker({
                position:myCurrentLocation,
            });



            var markers = [];

            markers.push(myMarker);



            var map = new google.maps.Map(document.getElementById("map-canvas"),mapProp);
            var infowindow = new google.maps.InfoWindow({
                content:"Anda berada disini"
            });
            infowindow.open(map,myMarker);

            myMarker.setMap(map);

            //event listener

            //event listener for search box
            sBox.addListener('places_changed', function() {
              var places = sBox.getPlaces();
              if (places.length == 0) {
                return;
              }
              deleteMarkers()

              // For each place, get the icon, name and location.
              var bounds = new google.maps.LatLngBounds();
              places.forEach(function(place) {
                var icon = {
                  url: place.icon,
                  size: new google.maps.Size(71, 71),
                  origin: new google.maps.Point(0, 0),
                  anchor: new google.maps.Point(17, 34),
                  scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                markers.push(new google.maps.Marker({
                  map: map,
                  //icon: icon,
                  title: place.name,
                  position: place.geometry.location
                }));

                if (place.geometry.viewport) {
                  // Only geocodes have viewport.
                  bounds.union(place.geometry.viewport);
                } else {
                  bounds.extend(place.geometry.location);
                }
              });
              map.fitBounds(bounds);

            });

            //event listener for google map
            google.maps.event.addListener(map, 'click', function(event) {
            placeMarker(event.latLng);
            });


            //marker

            // Sets the map on all markers in the array.
            function setMapOnAll(map) {
              for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
              }
            }

            function placeMarker(location){
                deleteMarkers();
                var alamatku = document.getElementById('alamatz');
                var myMarker = new google.maps.Marker({
                    position: location,
                    map: map
                });
                alamatku.value = location;
                var myString = alamatku.value;
                myString=myString.replace('(','');myString=myString.replace(')','');myString=myString.replace(', ',',');

                alamatku.value = myString;
                markers.push(myMarker);
                //alert(markers.length);
            }
            // Deletes all markers in the array by removing references to them.
            function deleteMarkers() {
              clearMarkers();
              markers = [];
            }

            function clearMarkers() {
              setMapOnAll(null);
            }

            //end marker

      },showError); //end function

    } else {
        alert("Geolocation is not supported by this browser.");
    }
}
    google.maps.event.addDomListener(window, 'load', initialize);

</script>
</head>
<body>
  <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M4XWFZM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

    <h4>header</h4>
    <?php include_once '../top-nav.php'; ?>


<div class="w3-container" style="padding:28px 16px; margin: auto; max-width: 1200px">
  <div class="w3-row-padding">

    <div class="w3-col">
      <h3 style="font-weight:bold" >Accounting Service</h3>


<ul class="steps steps-5">
  <li id="li_off"><a href="#" title=""><em>Step 1</em><span>Et nequ a quam turpis duisi</span></a></li>
  <li id="li_act"><a href="#" title=""><em>Step 5: XXXXXXXX</em><span>Et nequ a quam turpis duisi</span></a></li>
</ul>

<form class="w3-container w3-card-4" method="post"  action="">
  <h2 class="w3-text-blue">Form input perusahaan Step-2</h2>
  <hr>
  <p>
    <label class="w3-text-blue" for="namausaha"><b>Nama Perusahaan</b></label>
    <input class="w3-input w3-border" type="text" name="in[namausaha]" class="form-control" id="namausaha" placeholder="Nama Usaha Anda" required>
  </p>
  <p>
    <label class="w3-text-blue" for="emailusaha"><b>Email Perusahaan</b></label>
    <input class="w3-input w3-border" type="email" name="in[email]" class="form-control" id="emailusaha" placeholder="Email aktif, biarkan kosong bila tidak ada.">
  <p>
    <label class="w3-text-blue" for="telpusaha"><b>No. Telepon Perusahaan</b></label>
    <input class="w3-input w3-border" type="text" name="in[telepon]" class="form-control" id="telpusaha" placeholder="Telepon Aktif, kosongkan bila tidak ada.">
  <p>
    <label class="w3-text-blue" for="alamatusaha"><b>Alamat Perusahaan</b></label>
    <textarea class="w3-input w3-border" id="alamatusaha" name="in[alamat]" class="form-control" placeholder="Alamat di perusahaan anda"></textarea>
  </p>

  <p>
    <label class="w3-text-blue" for="kota"><b>Kota</b></label>
    <input class="w3-input w3-border" type="kota" name="in[kota]" class="form-control" placeholder="Wajib diisi." required>
  </p>




  <p>
<label class="w3-text-blue" for=""><b>Provinsi</b></label>
      <select  class="w3-select w3-border" name="option" name="in[provinsi]" class="form-control">
      					<option value="" disabled>-- Pilih Provinsi --</option>
      					<option value='aceh'>Nanggroe Aceh Darussalam</option>
                          <option value='sumatera utara'>Sumatera Utara</option>
                          <option value='riau'>Riau</option>
                          <option value='kepulauan riau'>Kepulauan Riau</option>
                          <option value='sumatera barat'>Sumatera Barat</option>
                          <option value='jambi'>Jambi</option>
                          <option value='bengkulu'>Bengkulu</option>
                          <option value='bangka belitung'>Bangka Belitung</option>
                          <option value='sumatera selatan'>Sumatera Selatan</option>
                          <option value='lampung'>Lampung</option>

                          <option value='banten'>Banten</option>
                          <option value='jawa barat'>Jawa Barat</option>
                          <option value='jakarta' selected>Jakarta</option>
                          <option value='jawa tengah'>Jawa tengah</option>
                          <option value='jogjakarta'>Jogjakarta</option>
                          <option value='jawa timur'>Jawa Timur</option>

                          <option value='bali'>Bali</option>
                          <option value='ntb'>Nusa tenggara Barat</option>

                          <option value='kalbar'>Kalimantan Barat</option>
                          <option value='kaltim'>Kalimantan Timur</option>
                          <option value='kalteng'>Kalimantan Tengah</option>
                          <option value='kalsel'>Kalimantan Selatan</option>
                          <option value='kalut'>Kalimantan Utara</option>

                          <option value='sulbar'>Sulawesi Barat</option>
                          <option value='sulsel'>Sulawesi Selatan</option>
                          <option value='sulteng'>Sulawesi Tengah</option>
                          <option value='sultra'>Sulawesi Tenggara</option>
                          <option value='gorontalo'>Gorontalo</option>
                          <option value='sulut'>Sulawesi Utara</option>

                          <option value='maluku utara'>Maluku Utara</option>
                          <option value='maluku'>Maluku</option>

                          <option value='papua barat'>Papua Barat</option>
                          <option value='papua'>Papua</option>
      				</select>
   </p>

<label class="w3-text-blue" for=""><b>Jenis Usaha</b></label>
   <p>
<input class="w3-check" name="in[jnsUsaha][]" value="1" type="checkbox">
<label>Perdagangan</label>
 </p>
    <p>
<input class="w3-check" name="in[jnsUsaha][]" value="2" type="checkbox">
<label>Jasa</label>
</p>
<p>
<input class="w3-check" name="in[jnsUsaha][]" value="3" type="checkbox">
<label>Konstruksi</label>
</p>
<p>
<input class="w3-check" name="in[jnsUsaha][]" value="4" type="checkbox">
<label>Pabrikasi</label>
</p>

<p>
  <label class="w3-text-blue" for="jenisusaha"><b>Keterangan Jenis Usaha</b></label>
  <textarea class="w3-input w3-border" id="ketJenisUsaha" name="in[ketJenisUsaha]" class="form-control" placeholder="Misal: Perusahaan saya bergerak di bidang konstruksi bangunan"></textarea>
</p>



<label class="w3-text-blue" for=""><b>Versi Accurate</b></label>
   <p>
<input class="w3-check" name="in[jnsAccurate][]" value="1" type="checkbox">
<label>Accurate 4 Standard Edition</label>
 </p>
 <p>
 <input class="w3-check" name="in[jnsAccurate][]" value="1" type="checkbox">
 <label>Accurate 4 Deluxe Dengan RAB</label>
 </p>
 <p>
 <input class="w3-check" name="in[jnsAccurate][]" value="1" type="checkbox">
 <label>Accurate 4 Deluxe Dengan RAB</label>
 </p>
 <p>
 <input class="w3-check" name="in[jnsAccurate][]" value="1" type="checkbox">
 <label>ACCURATE 4 Deluxe Edition</label>
 </p>
 <p>
 <input class="w3-check" name="in[jnsAccurate][]" value="1" type="checkbox">
 <label>ACCURATE 4 Enterprise Edition</label>
 </p>
 <p>
 <input class="w3-check" name="in[jnsAccurate][]" value="1" type="checkbox">
 <label>ACCURATE 5 Standar Edition</label>
 </p>
 <p>
 <input class="w3-check" name="in[jnsAccurate][]" value="1" type="checkbox">
 <label>ACCURATE 5 Deluxe Edition</label>
 </p>
 <p>
 <input class="w3-check" name="in[jnsAccurate][]" value="1" type="checkbox">
 <label>ACCURATE 5 Enterprise Edition</label>
 </p>
 <p>
 <input class="w3-check" name="in[jnsAccurate][]" value="1" type="checkbox">
 <label>ACCURATE Online/Cloud</label>
 </p>
 <p>
 <input class="w3-check" name="in[jnsAccurate][]" value="1" type="checkbox">
 <label>RENE Point Of Sales</label>
 </p>
 <p>
 <input class="w3-check" name="in[jnsAccurate][]" value="1" type="checkbox">
 <label>Gabungan RENE dan ACCURATE</label>
 </p>


 <p>
             <label class="w3-text-blue" for="alamat"><b>Lokasi Map</b></label>
           <input class="w3-input w3-border" type="text" name="in[map]" id="alamatz" class="form-control" placeholder="Cari Nama Tempat atau Tandai Peta di Bawah ini">
         </p>

         <div class="form-group">
           <div id="map-canvas" style="width:100%;height:400px;border:1px solid"></div>
         </div>

  <p>


  <button class="w3-btn w3-blue">Register</button></p>

</form>



    </div>
  </div>
</div>




    <?php //include_once 'modal-order.php'; ?>




</body>
</html>
