<?php session_start(); ?>
<?php $page="registrasi";?>


<?php include_once '../baseurl.php'; ?>
<?php include_once 'components/top-nav.php'; ?>
<?php include_once 'components/meta.php'; ?>
<?php include_once 'components/links.php'; ?>
<?php include_once 'components/scripts.php'; ?>
<?php include_once '../model/Member.php'; ?>
<?php 
// declare
$member = new Member();

?>


<html>
<head>
    <?php Meta($page) ?>
	<title>FAC-Institute -- Member</title>
	<?php Links($page) ?>
	
	<style type="text/css">
	
	.glyphicon { margin-right:10px; }
	.panel-body { padding:0px; }
	.panel-body table tr td { padding-left: 15px }
	.panel-body .table {margin-bottom: 0px; }
    .navbar-default .navbar-nav > li > a{color:#fff}
    .navbar-default .navbar-nav > li > a:hover{color:#55f267}
    .navbar-default .navbar-nav > li:hover{background-color:#f0ff00}

	</style>
</head>
<body>
<div class="container-fluid" style="padding-right:0px;padding-left:0px;height:50px;">
</div>
<?php TopNavigation($page); ?>

<div class="container-fluid" style="padding-top:50px;padding-bottom:40px;">
    <div class="row">
        <div class="col-sm-3 col-md-3">
            <div class="row">
                <img src="<?= getBaseUrl() ?>images/member/User-3-icon.png" class="img img-responsive">    
            </div>
            
            <div class="row" style="margin-top:15px;">
                <div class="col-md-12">
                    <center>
                        Untuk mendapatkan training Accurate dan support atas training yang nantinya telah dilaksanakan, silahkan untuk registrasi sebagai member client kami.
                    </center>
                </div>
            </div>
        </div>
        <div class="col-sm-9 col-md-9">
            <div class="row">
                <div class="page-header">
                    <h2>Registrasi Member Klien FAC Institute.</h2>
                </div>
            </div>
            
            <?php if (isset($_GET['ac'])&&$_GET['ac']=='ev'&&!empty($_GET['acc'])): ?>
                
                <?php if (isset($_SESSION['client'])&&!empty($_SESSION['client']['email'])): ?>
                    <?php 
                    include_once '../model/Mail.php';
                    $surat = new FACMail();
                    $datas = array(
                        'to' => $_SESSION['client']['email'],
                        'nama' =>'',
                        'subject' =>'Konfirmasi akun FAC Institute',
                        'content'=> "Akun anda terlah terdaftar di sistem kami, silahkan verifikasi pada link berikut "."<a href='www.fac-institute.com/member/verifikasi?a=".$_SESSION['client']['v']."'>www.fac-institute.com/member/verifikasi?a=".$_SESSION['client']['v']."</a>"
                    );
                    $data = (object) $datas;

                    $surat->sendMail($data);
                    ?>
                    
                    <?php else: ?>
                    <script type="text/javascript">
                        alert('data email anda tidak dapat kami lacak, harap untuk mengisi ulang form registrasi');
                    </script>
                    <script type="text/javascript">
                        location.replace('registrasi');
                    </script>
                <?php endif ?>




                <div class="row">
                    <div class="col-md-12">
                        Kami sudah mengirim kode verifikasi ke email anda, silahkan cek dan login. Bila anda belum menerima email anda silahkan untuk mengirim ulang email anda dengan klik tombol di bawah ini. (Cek ulang folder spam sebelum mengirim ulang email anda dan jangan tutup browser yang sedang dibuka.) <br><br>
                        <form action="" method="post">
                            <?php if (isset($_SESSION['client'])&&!empty($_SESSION['client']['email'])): ?>
                            <input type="text" name="email" value="<?= $_SESSION['client']['email'] ?>" style="display:none;">
                                <?php else: ?>
                            <input type="text" name="email" value="" style="display:none;">
                            <?php endif ?>
                            
                            <button class="btn btn-lg btn-primary">Kirim Ulang Email</button>
                        </form>
                    </div>
                </div>

                <?php else: ?>

                    <?php 
                    if (isset($_POST['reg'],$_SESSION['secure'])) {
                        $complete = true;

                        if ($_POST['reg']['password']!=$_POST['reg']['kpassword']) {
                            $complete=false;
                            echo "<script>alert('Konfirmasi password tidak sesuai!, harap isi kembali.');</script>";
                        }

                        if ($_POST['reg']['captcha']!=$_SESSION['secure']) {
                            $complete = false;
                            echo "<script>alert('Data captcha tidak sesuai!');</script>";   
                        }

                        if ($complete) {
                            // echo "Ok"."<br>";
                            $member->setUsername($_POST['reg']['email']);
                            $member->setPassword($_POST['reg']['password']);
                            $member->setNamalengkap($_POST['reg']['namalengkap']);
                            $member->setTelepon($_POST['reg']['telepon']);
                            $member->setJabatan($_POST['reg']['jabatan']);
                            $_SESSION['client'] = array(
                                'email' => $_POST['reg']['email'],
                                'nama' => $_POST['reg']['namalengkap'],
                                'v' => md5($_POST['reg']['password'])
                            );
                            if ($member->InputData()) {
                                echo "<script>alert('Data anda telah tersimpan di sistem kami, silahkan cek email anda untuk konfirmasi.');</script>";       
                            }

                            /*echo $_POST['reg']['email']."<br>";
                            echo $_POST['reg']['password']."<br>";
                            echo $_POST['reg']['kpassword']."<br>";
                            echo $_POST['reg']['namalengkap']."<br>";
                            echo $_POST['reg']['telepon']."<br>";
                            echo $_POST['reg']['jabatan']."<br>";*/
                        }
                        
                    }
                    ?>


            
            <div class="row">
                <div class="col-md-12">
                <form action="" id="myform" method="post">  
                    <div class="form-group">
                        <label>Email</label>
                        <?php if (isset($_POST['reg']['email'])): ?>
                            <input type="email" name="reg[email]" class="form-control" value="<?= $_POST['reg']['email'] ?>" required placeholder="email digunakan sebagai username" />    
                            <?php else: ?>
                            <input placeholder="email digunakan sebagai username" type="email" name="reg[email]" class="form-control" required />
                        <?php endif ?>
                        
                    </div>

                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" id="mypass" name="reg[password]" class="form-control" required />
                    </div>

                    <div class="form-group">
                        <label>Konfirmasi Password</label>
                        <input type="password" id="kpass" name="reg[kpassword]" class="form-control" required />
                    </div>

                    <div class="form-group">
                        <label>Nama Lengkap</label>
                        <?php if (isset($_POST['reg']['namalengkap'])): ?>
                            <input type="text" value="<?= $_POST['reg']['namalengkap'] ?>" placeholder="data di gunakan pada surat-menyurat" name="reg[namalengkap]" class="form-control" required />
                            <?php else: ?>
                            <input placeholder="data di gunakan pada surat-menyurat" type="text" name="reg[namalengkap]" class="form-control" required />
                        <?php endif ?>
                        
                    </div>

                    <div class="form-group">
                        <label>Telepon</label>
                        <?php if (isset($_POST['reg']['telepon'])): ?>
                        <input type="text" placeholder="harap masukan nomor aktif anda" name="reg[telepon]" value="<?= $_POST['reg']['telepon'] ?>" class="form-control" required /> 
                            <?php else: ?>
                        <input type="text" placeholder="harap masukan nomor aktif anda" name="reg[telepon]" class="form-control" required />
                        <?php endif ?>
                        
                    </div>

                    <div class="form-group">
                        <label>Jabatan</label>
                        <?php if (isset($_POST['reg']['jabatan'])): ?>
                        <input type="text" value="<?= $_POST['reg']['jabatan'] ?>" name="reg[jabatan]" class="form-control" placeholder="Optional" />
                            <?php else: ?>
                        <input type="text" name="reg[jabatan]" class="form-control" placeholder="Optional" />
                        <?php endif ?>
                    </div>

                    <div class="row">
                        <center>
                            <div class="row">
                                <div class="col-md-12">
                                    <img src="captcha-fac">
                                </div>
                            </div>
                        </center>
                    </div><br>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="text" name="reg[captcha]" class="form-control" placeholder="tulis angka sesuai dengan gambar diatas">
                            </div>
                        </div>
                    </div>


                    <button class="btn btn-lg btn-primary" id="btnsubmit" style="width:100%">Submit</button>
                </form>
                </div>
            </div>
        <?php endif ?>
        </div>
    </div>
</div>
    <?php Scripts($page) ?>
</body>
</html>