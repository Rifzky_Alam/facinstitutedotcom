<?php
session_start();
header("Content-type: image/jpeg");
$_SESSION['secure'] = rand(1000, 9999);
$text = $_SESSION['secure'];
$font_size = 140;

$image_width = 440;
$image_height = 180;

$image = imagecreate($image_width, $image_height);
// background is white
imagecolorallocate($image, 255, 255, 255);

//text is black
$text_color = imagecolorallocate($image, 0, 0, 0);

// putting lines on random coordinate
for ($i=0; $i <=100 ; $i++) {
	$x1 = rand(1,400);
	$x2 = rand(1,400);
	$y1 = rand(1,400);
	$y2 = rand(1,400);
	imageline($image, $x1, $y1, $x2, $y2, $text_color);
}
// assign image with text
imagettftext($image, $font_size, 0, 15, 140, $text_color, $_SERVER['DOCUMENT_ROOT'].'/fonts/Raleway-Regular.ttf', $text);

// casting object
imagejpeg($image);

?>
