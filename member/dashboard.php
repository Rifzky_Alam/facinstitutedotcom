<?php $page="dashboard";?>


<?php include_once '../baseurl.php'; ?>
<?php include_once 'components/sessions.php'; ?>
<?php include_once 'components/top-nav.php'; ?>
<?php include_once 'components/meta.php'; ?>
<?php include_once 'components/links.php'; ?>
<?php include_once 'components/scripts.php'; ?>

<?php Sessions($page) ?>

<html>
<head>
    <?php Meta($page) ?>
	<title>FAC-Institute -- Member</title>
	<?php Links($page) ?>
	
	<style type="text/css">
	
	.glyphicon { margin-right:10px; }
	.panel-body { padding:0px; }
	.panel-body table tr td { padding-left: 15px }
	.panel-body .table {margin-bottom: 0px; }
    .navbar-default .navbar-nav > li > a{color:#fff}
    .navbar-default .navbar-nav > li > a:hover{color:#55f267}
    .navbar-default .navbar-nav > li:hover{background-color:#f0ff00}

	</style>
</head>
<body>
<div class="container-fluid" style="padding-right:0px;padding-left:0px;height:50px;">
    
</div>
<?php TopNavigation($page); ?>

<div class="container-fluid" style="padding-top:50px">
    <div class="row">
        <div class="col-sm-3 col-md-3">
            <?php include_once 'components/sidebar.php'; Sidebar($page); ?>
        </div>
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1>FAC-Institute ::  Member</h1>
                Kami akan membuka layanan agency pada dalam waktu dekat. Kami akan segera memberitahu anda setelah layanan ini aktif. 
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-success" role="alert"><b>Status data perusahaan:</b> Lengkap.</div>
                    <div class="alert alert-warning" role="alert"><b>Status jadwal training:</b> Menunggu konfirmasi.</div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <h3>Cara Order Training FAC-Institute:</h3>
                    <ol>
                        <li>Lengkapi data perusahaan anda pada edit/input perusahaan.</li>
                        <li>Di menu transaksi silahkan pilih jadwal untuk mengisi kebutuhan training anda.</li>
                        <li>Simpan data jadwal dan tunggu konfirmasi dari kami 1x24 jam hari kerja (Senin-jumat 09:00-17:00, Sabtu 09:00-12:00).</li>
                        <li>Jika anda sudah mendapatkan konfirmasi dari kami, harap cek invoice dan upload bukti pembayaran.</li>
                        <li>Bila anda membutuhkan bantuan silahkan kontak kami di <a href="tel://+6281279222250">081279222250</a>.</li>
                    </ol>
                </div>
            </div>

        </div>
    </div>
</div>
    <?php Scripts($page) ?>
</body>
</html>