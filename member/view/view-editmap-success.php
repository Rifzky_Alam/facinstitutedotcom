<?php $page="dashboard";?>

<?php //include_once HomeDirectory().'member/components/sessions.php'; ?>
<?php include_once HomeDirectory().'member/components/top-nav.php'; ?>
<?php include_once HomeDirectory().'member/components/meta.php'; ?>
<?php include_once HomeDirectory().'member/components/links.php'; ?>
<?php include_once HomeDirectory().'member/components/scripts.php'; ?>

<html>
<head>
    <?php Meta($page) ?>
	<title>FAC-Institute -- Lokasi Training</title>
	<?php Links($page) ?>
	
	<style type="text/css">
	
	.glyphicon { margin-right:10px; }
	.panel-body { padding:0px; }
	.panel-body table tr td { padding-left: 15px }
	.panel-body .table {margin-bottom: 0px; }
    .navbar-default .navbar-nav > li > a{color:#fff}
    .navbar-default .navbar-nav > li > a:hover{color:#55f267}
    .navbar-default .navbar-nav > li:hover{background-color:#f0ff00}

	</style>
</head>
<body>
<div class="container-fluid" style="padding-right:0px;padding-left:0px;height:50px;">
    
</div>
<?php TopNavigation($page); ?>

<div class="container" style="padding-top:50px">
    
        <div class="col-sm-12 col-md-12">
            <div class="well">
                <h1>FAC-Institute ::  Layanan Bantuan</h1>
                Terimakasih telah membantu kami untuk meningkatkan pelayanan kepada anda.<br><br>
                Kami telah menyimpan data lokasi anda agar staff kami dapat menjangkau lokasi anda dengan mudah.<br>
                bila anda mengalami kesulitan harap <a href="tel://+6281279222250" title="our developer">hubungi admin sistem kami.</a>
            </div>
        </div>
    </div>
</div>
    <?php Scripts($page) ?>
</body>
</html>