<?php $page="map";?>

<?php //include_once HomeDirectory().'member/components/sessions.php'; ?>
<?php include_once HomeDirectory().'member/components/top-nav.php'; ?>
<?php include_once HomeDirectory().'member/components/meta.php'; ?>
<?php include_once HomeDirectory().'member/components/links.php'; ?>
<?php include_once HomeDirectory().'member/components/scripts.php'; ?>

<html>
<head>
    <?php Meta($page) ?>
  <title>FAC-Institute -- Lokasi Training</title>
  <?php Links($page) ?>
  
  <style type="text/css">
  
  .glyphicon { margin-right:10px; }
  .panel-body { padding:0px; }
  .panel-body table tr td { padding-left: 15px }
  .panel-body .table {margin-bottom: 0px; }
    .navbar-default .navbar-nav > li > a{color:#fff}
    .navbar-default .navbar-nav > li > a:hover{color:#55f267}
    .navbar-default .navbar-nav > li:hover{background-color:#f0ff00}

  </style>
</head>
<body>
<div class="container-fluid" style="padding-right:0px;padding-left:0px;height:50px;">
    
</div>
<?php TopNavigation($page); ?>

<div class="container" style="padding-top:50px">
    
        <div class="col-sm-12 col-md-12">
            <div class="well">
                <h1>FAC-Institute ::  Layanan Bantuan Lokasi</h1>
                Halo <?= $data->nama_cust ?>, terimakasih telah membantu kami untuk memudahkan staff kami menemukan lokasi anda.<br><br>
                Untuk mengedit map silahkan untuk menandai peta di bawah ini dengan klik area di dalamnya atau anda juga dapat mengetik alamat/perusahaan yang ada pada kolom alamat di bawah map, contoh: FAC Institute <br>
                setelah alamat selesai di input, silahkan klik tombol submit di bawah map.
                <br><br>
                <b>Apabila map tidak muncul, dimohon agar mengizinkan browser anda untuk berbagi lokasi terlebih dahulu.</b>
            </div>

            
      <form action="" method="post" accept-charset="utf-8">
        
        <div class="row" >
            <div class="col-md-10">
                <div id="map-canvas" style="width:100%;height:400px;border:1px solid"></div>
            </div>
        </div>
        <br>
        <div class="row" style="margin-bottom:20px">
            <div class="col-md-10">
                <div class="form-group">
                    <label for="alamat">Lokasi Map</label>
                    <input type="text" name="map" id="alamatz" class="form-control" placeholder="Cari Nama Tempat atau Tandai Peta di Bawah ini">                    
                </div>
                <button class="btn btn-lg btn-primary">Submit</button>
            </div>

        </div>
      </form>            
        </div>
    </div>
</div>
    <?php Scripts($page) ?>
</body>
</html>