<?php $page="order";?>


<?php include_once '../baseurl.php'; ?>
<?php include_once 'components/sessions.php'; ?>
<?php include_once 'components/top-nav.php'; ?>
<?php include_once 'components/meta.php'; ?>
<?php include_once 'components/links.php'; ?>
<?php include_once 'components/scripts.php'; ?>
<?php include_once '../model/Member.php'; ?>

<?php Sessions($page) ?>

<html>
<head>
    <?php Meta($page) ?>
	<title>FAC-Institute -- Member</title>
	<?php Links($page) ?>
	
	<style type="text/css">
	
	.glyphicon { margin-right:10px; }
	.panel-body { padding:0px; }
	.panel-body table tr td { padding-left: 15px }
	.panel-body .table {margin-bottom: 0px; }
    .navbar-default .navbar-nav > li > a{color:#fff}
    .navbar-default .navbar-nav > li > a:hover{color:#55f267}
    .navbar-default .navbar-nav > li:hover{background-color:#f0ff00}

	</style>

    <?php $member=new Member(); ?>



</head>
<body>
<div class="container-fluid" style="padding-right:0px;padding-left:0px;height:50px;">
    
</div>
<?php TopNavigation($page); ?>

<div class="container-fluid" style="padding-top:50px">
    <div class="row">
        <div class="col-sm-3 col-md-3">
            <?php include_once 'components/sidebar.php'; Sidebar($page); ?>
        </div>
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1>FAC-Institute :: Edit data perusahaan</h1>
                Untuk melanjutkan transaksi anda perlu melengkapi data di bawah ini. 
            </div>

            <?php $data=$member->fetchNamaPaket(); ?>

            
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Jenis Item</label>
                        <select class="form-control" name="tr[paket]" id="paket">
                            <option value="">-- Item/Paket yang akan anda order --</option>
                            <?php foreach ($data as $value) {
                                echo "<option value='$value->id'>".$value->nama_item."</option>";
                            } ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label id="jmlhari">...x 0 Hari</label>
                        <input type="text" name="tr[qty]" class="form-control" value="0" id="quantity">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Kebutuhan Training</label>
                        <textarea class="form-control" name="tr[agenda]" id="kebutuhan" placeholder="Misal: Setup database, Troubleshooting, training fitur accurate, audit data. apabila lebih dari satu kebutuhan, pisahkan dengan koma (,)"></textarea>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <h3>Total : Rp <span id="okee"></span></h3>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <button class="btn btn-lg btn-primary" style="width:100%;">Submit</button>
                </div>
            </div>

        </div>
    <?php Scripts($page) ?>
</body>
</html>