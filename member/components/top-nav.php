<?php function TopNavigation($laman){  ?>
<nav class="navbar navbar-default navbar-fixed-top" style="background-color:#18549f;border-color:#1651de">
  <div class="container-fluid">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
      <a class="navbar-brand" href="<?= getBaseUrl() ?>" style="color:#fff;padding-top:10px;">
        <img alt="FAC-Institute" src="<?= getBaseUrl() ?>images/homepage/path9-7-6.png" style="width:30px">
      </a>
    </div>
    <div class="navbar-collapse collapse" id="navbar">
        <ul class="nav navbar-nav">
            <li><a href="<?= getBaseUrl() ?>order/">Order</a></li>
            <li><a href="<?= getBaseUrl() ?>kursus-akuntansi/">Kursus</a></li>
            <li><a href="<?= getBaseUrl() ?>artikel/">Artikel</a></li>
            <li><a href="<?= getBaseUrl() ?>tutorial/">Tutorial</a></li>
            <li><a href="<?= getBaseUrl() ?>download/">Download</a></li>
        </ul>
    </div>
  </div>
</nav>
<?php } ?>