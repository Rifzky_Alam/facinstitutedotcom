<?php 
function Scripts($laman){ ?>
	<script type="text/javascript" src=<?php echo "'".getBaseUrl()."js/jquery.js'"; ?>></script>
	<script type="text/javascript" src=<?php echo "'".getBaseUrl()."js/bootstrap.min.js'"; ?>></script>

	<?php //halaman order ?>
	<?php if ($laman=='order'): ?>
		<script type="text/javascript">
			

			$('#quantity').focusout(function(){
				// alert('hello world!');
				getPrice();
			});

			$('#paket').change(function(){
				// alert('hello world!');
				getPrice();
			});
				// $('#okee').html(addCommas('10000'));
			
			function getPrice() {
				var item = $('#paket').val();
				var qty = $('#quantity').val();
				

				$.ajax({
                  type: "GET",
                  url: "ajax/ajaxorder",
                  data: {'item':item},
                  cache: false,
                  success: function(data){
                  	var hasil = $(data).filter('#harga').html() * qty;
                  	$('#okee').html(addCommas(hasil));
                  	$('#jmlhari').html('...x ' + $(data).filter('#hari').html() + ' Hari')
                     // alert(data);
                     // window.location.replace('edit-absen');
                  }
                }); //end ajax

				

				// alert(item + ' ' + qty);
			}



			function addCommas(nStr){
					nStr += '';
					x = nStr.split('.');
					x1 = x[0];
					x2 = x.length > 1 ? '.' + x[1] : '';
					var rgx = /(\d+)(\d{3})/;
					while (rgx.test(x1)) {
						x1 = x1.replace(rgx, '$1' + ',' + '$2');
					}
					return x1 + x2;
			}
		</script>


		<?php //halaman edit perusahaan ?>
	<?php elseif($laman=='profilusaha'): ?>
		<script type="text/javascript">
			$('#btnalamat').click(function(){
				var cobayah = $('.alamatz').length;
	            var iseng = "tbha" + cobayah;
	            if ($('#tbha').length){

	            if (cobayah==2) {
	                $('#tbha').after("<div id='"+iseng+"' class='col-md-12 alamatz' id='iseng'><div class='form-inline'><textarea name='in[alamat][]' class='form-control' style='width:50%'></textarea><span class='btn btn-danger' title='Remove' onclick='removeElement("+'"'+iseng+'"'+")'><i class='glyphicon glyphicon-minus' style='margin-right:0px'></i></span></div></div>");
	            }else{
	                var okeh = cobayah-1;
	                var iseng = "tbha" + okeh;
	                $("#"+iseng).after("<div id='tbha"+cobayah+"' class='col-md-12 alamatz' id='iseng'><div class='form-inline'><textarea name='in[alamat][]' class='form-control' style='width:50%'></textarea><span class='btn btn-danger' title='Remove' onclick='removeElement("+'"tbha'+cobayah+'"'+")'><i class='glyphicon glyphicon-minus' style='margin-right:0px'></i></span></div></div>");
	            };
	            //nilai +=1;
	            }else{
	                $('#iseng').after("<div id='tbha' class='col-md-12 alamatz' id='iseng'><div class='form-inline'><textarea name='in[alamat][]' class='form-control' style='width:50%'></textarea><span class='btn btn-danger' title='Remove' onclick='removeElement("+'"'+"tbha"+'"'+")'><i class='glyphicon glyphicon-minus' style='margin-right:0px'></i></span></div></div>");
	            };
			});

			$('#btnemail').click(function(){
				var cobayah = $('.email').length;
	            var iseng = "tbhe" + cobayah;
	            if ($('#tbhe').length){

	            if (cobayah==2) {
	                $('#tbhe').after("<div id='"+iseng+"' class='col-md-12 email' style='padding-top:15px;'><div class='form-inline'><input type='text' name='in[email][]' style='width:50%' class='form-control'><span class='btn btn-danger' title='Remove' onclick='removeElement("+'"'+iseng+'"'+")'><i class='glyphicon glyphicon-minus' style='margin-right:0px'></i></span></div></div>");
	            }else{
	                var okeh = cobayah-1;
	                var iseng = "tbhe" + okeh;
	                $("#"+iseng).after("<div id='tbhe"+cobayah+"' class='col-md-12 email' style='padding-top:15px;'><div class='form-inline'><input type='text' name='in[email][]' style='width:50%' class='form-control'><span class='btn btn-danger' title='Remove' onclick='removeElement("+'"tbhe'+cobayah+'"'+")'><i class='glyphicon glyphicon-minus' style='margin-right:0px'></i></span></div></div>");
	            };
	            //nilai +=1;
	            }else{
	                $('#jajal').after("<div id='tbhe' class='col-md-12 email' style='padding-top:15px;'><div class='form-inline'><input type='text' name='in[email][]' style='width:50%' class='form-control'><span class='btn btn-danger' title='Remove' onclick='removeElement("+'"'+"tbhe"+'"'+")'><i class='glyphicon glyphicon-minus' style='margin-right:0px'></i></span></div></div>");
	            };
			});

			$('#btntelepon').click(function(){
				var cobayah = $('.telepon').length;
	            var iseng = "tbh" + cobayah;
	            if ($('#tbh').length){

	            if (cobayah==2) {
	                $('#tbh').after("<div class='col-md-12 telepon' id='"+iseng+"'><div class='form-inline'><input type='text' name='in[telepon]' class='form-control' style='width:50%;'><span class='btn btn-danger' id='btntelepon'><i class='glyphicon glyphicon-minus' style='margin-right:0px;' title='Remove' onclick='removeElement("+'"'+iseng+'"'+")'></i></span></div></div>");
	            }else{
	                var okeh = cobayah-1;
	                var iseng = "tbh" + okeh;
	                $("#"+iseng).after("<div class='col-md-12 telepon' id='tbh"+cobayah+"'><div class='form-inline'><input type='text' name='in[telepon]' class='form-control' style='width:50%;'><span class='btn btn-danger' id='btntelepon'><i class='glyphicon glyphicon-minus' style='margin-right:0px;' title='Remove' onclick='removeElement("+'"tbh'+cobayah+'"'+")'></i></span></div></div>");
	            };
	            //nilai +=1;
	            }else{
	                $('#coba').after("<div class='col-md-12 telepon' id='tbh'><div class='form-inline'><input type='text' name='in[telepon]' class='form-control' style='width:50%;'><span class='btn btn-danger' id='btntelepon'><i class='glyphicon glyphicon-minus' style='margin-right:0px;' title='Remove' onclick='removeElement("+'"'+"tbh"+'"'+")'></i></span></div></div>");
	            };
			});

			function removeElement(id) {
        		$('#'+id).remove();
    		}

		</script>
	<?php elseif($laman=='map'): ?>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCUn6Q6sg0JVJBlgiz4tL2kF1zmVoC0ScE&libraries=places&sensor=true"></script>
		<script type="text/javascript">
			var latitude;
			var longitude

			function getLocation(){
			    if (navigator.geolocation) {
			        navigator.geolocation.getCurrentPosition(showPosition);
			    } else { 
			        alert("Geolocation is not supported by this browser.");
			    }
			}

			function showPosition(position) {
			     var myCurrentLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
			     return myCurrentLocation;
			}

			function cobaaa() {
			    getLocation();

			    return [position.coords.latitude,position.coords.longitude];
			}

			function showError(error){
			    switch(error.code){
			        case error.PERMISSION_DENIED:
			            //alert("User denied the request for Geolocation.");
			            
			            var myCurrentLocation = new google.maps.LatLng(-6.248123, 106.907952);
			            var mapProp = {
			                center:myCurrentLocation,
			                zoom:11,
			                mapTypeId:google.maps.MapTypeId.ROADMAP
			            };

			            var input = document.getElementById('alamatz');
			            input.value = "";
			            var sBox = new google.maps.places.SearchBox(input);
			                //marker
			            var myMarker=new google.maps.Marker({
			                position:myCurrentLocation,
			            });


			    
			            var markers = [];

			            markers.push(myMarker);



			            var map = new google.maps.Map(document.getElementById("map-canvas"),mapProp);
			            var infowindow = new google.maps.InfoWindow({
			                content:"FAC Institute Office"
			            });
			            infowindow.open(map,myMarker);

			            myMarker.setMap(map);

			            //event listener 

			            //event listener for search box
			            sBox.addListener('places_changed', function() {
			              var places = sBox.getPlaces();
			              if (places.length == 0) {
			                return;
			              };
			              deleteMarkers();

			              // For each place, get the icon, name and location.
			              var bounds = new google.maps.LatLngBounds();
			              places.forEach(function(place) {
			                var icon = {
			                  url: place.icon,
			                  size: new google.maps.Size(71, 71),
			                  origin: new google.maps.Point(0, 0),
			                  anchor: new google.maps.Point(17, 34),
			                  scaledSize: new google.maps.Size(25, 25)
			                };

			                // Create a marker for each place.
			                markers.push(new google.maps.Marker({
			                  map: map,
			                  //icon: icon,
			                  title: place.name,
			                  position: place.geometry.location
			                }));

			                if (place.geometry.viewport) {
			                  // Only geocodes have viewport.
			                  bounds.union(place.geometry.viewport);
			                } else {
			                  bounds.extend(place.geometry.location);
			                }
			              });
			              map.fitBounds(bounds);

			            });

			            //event listener for google map
			            google.maps.event.addListener(map, 'click', function(event) {
			            placeMarker(event.latLng);
			            });


			            //marker

			            // Sets the map on all markers in the array.
			            function setMapOnAll(map) {
			              for (var i = 0; i < markers.length; i++) {
			                markers[i].setMap(map);
			              }
			            }

			            function placeMarker(location){
			                deleteMarkers();
			                var alamatku = document.getElementById('alamatz');
			                var myMarker = new google.maps.Marker({
			                    position: location, 
			                    map: map
			                });
			                alamatku.value = location;
			                var myString = alamatku.value;
			                myString=myString.replace('(','');myString=myString.replace(')','');myString=myString.replace(', ',',');

			                alamatku.value = myString;
			                markers.push(myMarker);
			                //alert(markers.length);
			            }
			            // Deletes all markers in the array by removing references to them.
			            function deleteMarkers() {
			              clearMarkers();
			              markers = [];
			            }

			            function clearMarkers() {
			              setMapOnAll(null);
			            }



			            break;
			        case error.POSITION_UNAVAILABLE:
			            //x.innerHTML = "Location information is unavailable."
			            alert('Location information is unavailable.');
			            break;
			        case error.TIMEOUT:
			            alert('The request to get user location timed out.');
			            break;
			        case error.UNKNOWN_ERROR:
			            //x.innerHTML = "An unknown error occurred."
			            alert('An unknown error occurred.');
			            break;
			    }
			}


			function initialize() {



			    if (navigator.geolocation) {


			        navigator.geolocation.getCurrentPosition(function(position){
			            var myCurrentLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
			            var mapProp = {
			                center:myCurrentLocation,
			                zoom:16,
			                mapTypeId:google.maps.MapTypeId.ROADMAP
			            };

			            var input = document.getElementById('alamatz');
			            input.value = position.coords.latitude + ',' + position.coords.longitude;
			            var sBox = new google.maps.places.SearchBox(input);
			                //marker
			            var myMarker=new google.maps.Marker({
			                position:myCurrentLocation,
			            });


			    
			            var markers = [];

			            markers.push(myMarker);



			            var map = new google.maps.Map(document.getElementById("map-canvas"),mapProp);
			            var infowindow = new google.maps.InfoWindow({
			                content:"Anda berada disini"
			            });
			            infowindow.open(map,myMarker);

			            myMarker.setMap(map);

			            //event listener 

			            //event listener for search box
			            sBox.addListener('places_changed', function() {
			              var places = sBox.getPlaces();
			              if (places.length == 0) {
			                return;
			              }
			              deleteMarkers()

			              // For each place, get the icon, name and location.
			              var bounds = new google.maps.LatLngBounds();
			              places.forEach(function(place) {
			                var icon = {
			                  url: place.icon,
			                  size: new google.maps.Size(71, 71),
			                  origin: new google.maps.Point(0, 0),
			                  anchor: new google.maps.Point(17, 34),
			                  scaledSize: new google.maps.Size(25, 25)
			                };

			                // Create a marker for each place.
			                markers.push(new google.maps.Marker({
			                  map: map,
			                  //icon: icon,
			                  title: place.name,
			                  position: place.geometry.location
			                }));

			                if (place.geometry.viewport) {
			                  // Only geocodes have viewport.
			                  bounds.union(place.geometry.viewport);
			                } else {
			                  bounds.extend(place.geometry.location);
			                }
			              });
			              map.fitBounds(bounds);

			            });

			            //event listener for google map
			            google.maps.event.addListener(map, 'click', function(event) {
			            placeMarker(event.latLng);
			            });


			            //marker

			            // Sets the map on all markers in the array.
			            function setMapOnAll(map) {
			              for (var i = 0; i < markers.length; i++) {
			                markers[i].setMap(map);
			              }
			            }

			            function placeMarker(location){
			                deleteMarkers();
			                var alamatku = document.getElementById('alamatz');
			                var myMarker = new google.maps.Marker({
			                    position: location, 
			                    map: map
			                });
			                alamatku.value = location;
			                var myString = alamatku.value;
			                myString=myString.replace('(','');myString=myString.replace(')','');myString=myString.replace(', ',',');

			                alamatku.value = myString;
			                markers.push(myMarker);
			                //alert(markers.length);
			            }
			            // Deletes all markers in the array by removing references to them.
			            function deleteMarkers() {
			              clearMarkers();
			              markers = [];
			            }

			            function clearMarkers() {
			              setMapOnAll(null);
			            }

			            //end marker

			      },showError); //end function

			    } else { 
			        alert("Geolocation is not supported by this browser.");
			    }
			}
			    google.maps.event.addDomListener(window, 'load', initialize);

		</script>
		<script>
		    $(document).ready(function(){
        		$("form").bind("keypress", function (e) {
	        		if (e.keyCode == 13) {
	            		$("#btnSearch").attr('value');
	            		//add more buttons here
	            		return false;
	        		}
    			});
    		});
</script>
		</script>
	<?php elseif($laman=='registrasi'): ?>
		<script type="text/javascript">
			

		</script>
	<?php endif ?>
<?php } ?>


