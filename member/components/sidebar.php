<?php function Sidebar($laman){ ?>

            <div class="panel-group" id="accordion">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-folder-close">
                            </span>Perusahaan</a>
                        </h4>
                    </div>
<?php if ($laman=='dashboard'||$laman=='profilusaha'||$laman=='notifikasi'): ?>
                    <div id="collapseOne" class="panel-collapse collapse in">
    <?php else: ?>
                    <div id="collapseOne" class="panel-collapse collapse">
<?php endif ?>

                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon glyphicon-dashboard text-primary"></span><a href="dashboard">Home</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-globe text-primary"></span><a href="pemberitahuan">Pemberitahuan</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-pencil text-primary"></span>
                                        <a href=<?php echo "'".getBaseUrl()."member/profil-usaha'"; ?>>Edit/input perusahaan</a>
                                    </td>
                                </tr>
                                
                            </table>
                        </div>
                    </div>
                </div>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><span class="glyphicon glyphicon-user">
                            </span><?php echo @$_SESSION['admin']['username'] ?></a>
                        </h4>
                    </div>
<?php if ($laman=='keluhan'): ?>
                    <div id="collapseThree" class="panel-collapse collapse in">
    <?php else: ?>
                    <div id="collapseThree" class="panel-collapse collapse">
<?php endif ?>
                    
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-log-in text-primary"></span><a href="http://www.jquery2dotnet.com">Ubah Password</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-bullhorn text-primary"></span><a href="keluhan">Keluhan</a>
                                    </td>
                                </tr>
                                
                            </table>
                        </div>
                    </div>
                </div>
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><span class="glyphicon glyphicon-file">
                            </span>Transaksi</a>
                        </h4>
                    </div>
<?php if ($laman=='invoice'||$laman=='order'||$laman=='jadwal'): ?>
                    <div id="collapseFour" class="panel-collapse collapse in">
<?php else: ?>
                    <div id="collapseFour" class="panel-collapse collapse">
<?php endif ?>
                    
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-list-alt"></span><a href="order">Order Training</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-usd"></span><a href="jadwal">Jadwal Training</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-user"></span><a href="invoice">Invoice</a><!--dompdf-->
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>





<?php } ?>


