<?php 

class Content{



public function PenawaranTandingan(){
    return '<!DOCTYPE html>
<html>
<head>
    <title>Tes</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
</head>
<body>

<div class="container-fluid" style="background-color:#0066ff;color:#fff">
    <div class="col-md-12">
        <center><h3 style="margin-bottom:20px;">FAC Institute</h3></center>
    </div>
</div>

<div class="container-fluid" style="padding-top:15px;">
    <img class="img img-responsive" src="images/Logo FAC.jpg" style="float:left;width:120px;height:60px;margin-left:40px;" />
    <img class="img img-responsive" src="../../images/logoaccurate5.jpg" style="width:210px;height:50px;margin-left:400px;" />
</div>

<div class="container-fluid" style="padding-top:35px;">
    <div class="row">
        <div class="col-md-12">
            <h4>Kepada Yth,</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h5><b>PT Teknologi Makmur Sejahtera</b></h5>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h5>UP: Lindin Park</h5>
        </div>
    </div>

    <div class="row" style="font-size:12px;">
        <div class="col-md-12">
            <section>
                <p>Kami dari FAC Institute sebagai divisi onsite training Accurate.</p>
                <p>
                    Terimakasih telah memilih Accurate sebagai program akuntansi di <b>PT. Marlanco.</b> Terimakasih pula atas order menggunakan jasa FAC Institute dalam mengimplementasikan Accurate. <br>
                    Berikut ini kami kirimkan jadwal training Accurate :                
                </p>
            </section>
        </div>
    </div>

    <div class="row" style="font-size:12px;">
        <div class="col-md-12">
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr>
                            <td style="width:50%;">
                            <label><strong>WAKTU & TEMPAT PELAKSANAAN</strong></label>
                            <table style="padding-top:10px">
                            <tbody>
                            <tr>
                                <td style="vertical-align:top;">Tanggal</td>
                                <td style="padding-left:10px;vertical-align:top;">:</td>
                                <td style="vertical-align:top;">
                                    <ul type="none" style="margin-top:0px;padding-left:10px;">
                                        <li>9 Januari 2017</li>
                                        <li>2 Mei 2017</li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td>Waktu</td>
                                <td style="padding-left:10px;">:</td>
                                <td style="padding-left:10px;">09:00-17:00</td>
                            </tr>
                            <tr>
                                <td>Tempat</td>
                                <td style="padding-left:10px;">:</td>
                                <td style="padding-left:10px;">Kantor</td>
                            </tr>
                            <tr>
                                <td>Alamat</td>
                                <td style="padding-left:10px;">:</td>
                                <td style="vertical-align:top;padding-left:10px;">
                                    Jl Jatiwaringin no 8 Pangkalan Jati Jakarta Timur
                                </td>
                            </tr>
                            </tbody>
                            </table>
                            </td>
                            <td style="text-align:center;vertical-align:top;">
                            <label><strong>AGENDA KEGIATAN</strong></label>
                            <table style="padding-top:10px">
                            <tbody>
                            <tr>
                                <td style="text-align:left;">
                                    <ol>
                                        <li>Training Accurate.</li>
                                        <li>Setup database Accurate.</li>
                                        <li>Troubleshooting.</li>
                                    </ol>
                                </td>
                            </tr>
                            
                            </tbody>
                            </table>
                            </td>
                        </tr>
                    </tbody>
            </table>    
        </div>
    </div>
</div>

<div class="container-fluid" style="background-color:#660000;margin-top:15px;">
    <center><h4 style="color:#fff">Syarat & Ketentuan</h4></center>
</div>

<div class="container" style="margin-top: 15px;font-size:12px;">
    <div class="row">
        <div class="col-md-12">
            <ol>
                    <li>Training dan implementasi bersifat time oriented, bukan result oriented.</li>
                    <li>Training dan implementasi berfokus pada kegiatan mentransfer <strong>How To UseACCURATE</strong>.</li>
                    <li>Training dan implementasi terdiri dari 1 implementator dengan waktu kerja maksimal 7 jam (termasuk 1 jam istirahat), yaitu jam 09.00 s/d 16.00 atau jam 10.00 s/d 17.00.</li>
                    <li>Jika lembur, mohon di print dan diisi form lembur terlebih dahulu yang telah kami lampirkan.</li>
                    <li>Menyediakan makan siang bagi implementator (Jabodetabek)</li>
                    <li>Implementasi diluar Jabodetabek, akomodasi perjalanan, makan dan penginapan ditanggung customer.</li>
                    <li>Kelebihan jam training akan dikenakan tarif overtime Rp 75.000.- per jam</li>
                    <li>Pembatalan atau perubahan jadwal implementasi oleh Customer harus disampaikan ke FAC paling lambat 2 hari sebelum waktu pelaksanaan implementasi, apabila dilakukan 1 (satu) hari sebelumnya atau di hari pelaksanaan maka dikenakan denda Rp.250.000/hari. Selanjutnya waktu pelaksanaan akan direschedule kembali.</li>
                    <li>Jika anda pengguna awal atau pengguna lama tapi ingin membuat database baru, pada email ini kami lampirkan format excel untuk saldo awal meliputi daftar akun, daftar pelanggan, daftar pemasok, daftar barang dan daftar fix asset. Harap disiapkan sebelum hari training.</li>
                </ol>
        </div>
    </div>
</div>

<div class="container-fluid" style="font-size:12px;">
    <div class="row">
        <div class="col-md-12">
            <p>Demikianlah semoga menjadi maklum adanya. Terimakasih atas perhatian dan kerjasamanya. 
            <br>
            Jika ada hal-hal yang ingin ditanyakan, jangan sungkan untuk senantiasa menghubungi kami. <br>  
            Terimakasih, <br><br>

            Salam
            <br><br>
            <b>Imelia Silviana</b><br>
            <b>Officer of Marketing Division</b>
            </p>
        </div>
    </div>
</div>

</body>
</html>';
}


public function penawaranz($data){
		return '<table style="margin:0;padding:0" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr style="background-color: #0066ff;">
                            <td style="color:white;margin:0 auto;font-size:22px" align="center" valign="middle" height="60"><strong>FAC-Institute</strong></td>
                        </tr>
                    </tbody>
                </table>            
            </td>
        </tr>
        
        <tr>
            <td valign="middle" height="60"><h4>Dear Rifzky Alam,</h4></td>
        </tr>
        <tr>
            <td>
                <p>
                    Kami dari FAC Institute sebagai divisi onsite training Accurate. Terimakasih telah memilih Accurate sebagai program akuntansi di <strong>PT Asik Selalu</strong>.
                     Terimakasih pula atas order menggunakan jasa FAC Institute dalam mengimplementasikan Accurate.<br> 
                    Berikut ini kami kirimkan jadwal dan invoice training Accurate : 
                </p>
                <br>
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr>
                            <td style="width:50%;">
                            <label><strong>WAKTU & TEMPAT PELAKSANAAN</strong></label>
                            <table style="padding-top:10px">
                            <tbody>
                            <tr>
                                <td style="width:150px;vertical-align:top;">Tanggal</td>
                                <td style="vertical-align:top;">:</td>
                                <td>
                                    <ul type="none" style="padding-left:10px;margin-top:0px;">
                                        <li>9 Januari 2017</li>
                                        <li>2 Mei 2017</li>
                                    </ul>
                                </td>
                            </tr>
                            <tr><td>Waktu</td><td>:</td><td>09:00-17:00</td></tr>
                            <tr><td>Tempat</td><td>:</td><td>Kantor</td></tr>
                            <tr><td>Alamat</td><td>:</td><td>Jl Jatiwaringin no 8 Pangkalan Jati Jakarta Timur</td></tr>
                            </tbody>
                            </table>
                            </td>
                            <td style="text-align:center;vertical-align:top;">
                            <label><strong>AGENDA KEGIATAN</strong></label>
                            <table style="padding-top:10px">
                            <tbody>
                            <tr>
                                <td style="text-align: left;">
                                    <ol>
                                        <li>Training Accurate.</li>
                                        <li>Setup database Accurate.</li>
                                        <li>Troubleshooting.</li>
                                    </ol>
                                </td>
                            </tr>
                            
                            </tbody>
                            </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td><br>
            <table valign="top" align="center" border="0" style="padding-bottom:30;" bgcolor="#f9ffee" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                    <tr>
                        <td>
                            <table valign="top" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tbody>
                                    <tr><td><center><h3><strong>BIAYA</strong></h3></center></td></tr>
                                    <tr>
                                    <td style="margin-bottom:10px">
                                    <table border="1" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Keterangan</th>
                                        <th>Biaya</th>
                                        <th>Qty</th>
                                        <th>Jumlah</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Onsite Standard</td>
                                        <td style="text-align:right">1.000.000.-</td>
                                        <td style="text-align:center">2</td>
                                        <td style="text-align:right">2.000.000.-</td>
                                    </tr>
                                    <tr>
                                        <td>Biaya Transport</td>
                                        <td style="text-align:right">150.000.-</td>
                                        <td style="text-align:center">2</td>
                                        <td style="text-align:right"> 300.000.-</td>
                                    </tr>
                                    <tr>
                                    <td colspan="3"><strong>Total</strong></td>
                                    <td style="text-align:right"><strong> 2.300.000.-</strong></td>
                                    </tr>
                                    </tbody>
                                    </table>
                                    </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>

            </table>
                
            </td>
        </tr>
        <tr>
            <td>
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                <tr><td>
                <h3>PEMBAYARAN</h3>
                <p>
                Pembayaran dimuka 50% yaitu sebelum implementasi dilaksanakan dan pembayaran di bayarkan ke No Rekening di bawah ini.
                </p>
                <ul type="square">
                    <li><strong>Bank BCA KCU Bekasi No Acc. 0663162851 a/n : Fajar Shodiq</strong></li>
                    <li><strong>Bank Mandiri KCP Bekasi Ahmad Yani No. Acc. 1670001377901 a/n : Fajar Shodiq</strong></li>
                </ul></td>
                </tr>
                </tbody>
                </table> 
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr style="background-color: #660000;">
                            <td style="color:white;margin:0 auto" align="center" valign="middle" height="40">SYARAT DAN KETENTUAN</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <ol>
                    <li>Training dan implementasi bersifat time oriented, bukan result oriented.</li>
                    <li>Training dan implementasi berfokus pada kegiatan mentransfer <strong>How To UseACCURATE</strong>.</li>
                    <li>Training dan implementasi terdiri dari 1 implementator dengan waktu kerja maksimal 7 jam (termasuk 1 jam istirahat), yaitu jam 09.00 s/d 16.00 atau jam 10.00 s/d 17.00.</li>
                    <li>Jika lembur, mohon di print dan diisi form lembur terlebih dahulu yang telah kami lampirkan.</li>
                    <li>Menyediakan makan siang bagi implementator (Jabodetabek)</li>
                    <li>Implementasi diluar Jabodetabek, akomodasi perjalanan, makan dan penginapan ditanggung customer.</li>
                    <li>Kelebihan jam training akan dikenakan tarif overtime Rp 75.000.- per jam</li>
                    <li>Pembatalan atau perubahan jadwal implementasi oleh Customer harus disampaikan ke FAC paling lambat 2 hari sebelum waktu pelaksanaan implementasi, apabila dilakukan 1 (satu) hari sebelumnya atau di hari pelaksanaan maka dikenakan denda Rp.250.000/hari. Selanjutnya waktu pelaksanaan akan direschedule kembali.</li>
                    <li>Jika anda pengguna awal atau pengguna lama tapi ingin membuat database baru, pada email ini kami lampirkan format excel untuk saldo awal meliputi daftar akun, daftar pelanggan, daftar pemasok, daftar barang dan daftar fix asset. Harap disiapkan sebelum hari training.</li>
                </ol>
            </td>
        </tr>
        <tr>
            <td>
                <p>Demikianlah semoga menjadi maklum adanya. Terimakasih atas perhatian dan kerjasamanya.</p>
                <p>
                Jika ada hal-hal yang ingin ditanyakan, jangan sungkan untuk senantiasa menghubungi kami. Terimakasih, <br><br>Salam <br><br>
                </p>
                <p><strong>Linda Kartinah</strong> <br> <strong>Officer of Marketing Division</strong></p>
            </td>
        </tr>
        <tr>
            <td style="padding:0 0 0 0" align="center" bgcolor="#0066ff">
          <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:#0066ff">
            <tbody>
              <tr>
                <td>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr height="30">
                        <td align="center" width="100%" height="16"></td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr>
                        <td align="center" valign="top">
                          <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                              <tr>
                                <td align="left" width="20"></td>
                                <td align="left" valign="top">
                                  <table align="center" border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                      <tr height="15">
                                        <td align="left" height="15">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td style="line-height:18px" align="center" valign="top">
                                          <span style="font-family:'."'"."Open Sans"."'".',sans-serif;color:white;font-size:12px;text-align:center">Email ini dikirim oleh: <strong><a href="http://www.fac-institute.com" style="color:white">FAC-Institute</a></strong><br>
                                          Jl. Jatiwaringin No.8 Pangkalan Jati, Jakarta Timur<br>
                                          Telp. <a href="tel://+6281290083983" style="color:white">+6281290083983</a></span><br>
                                          <br>
                                            <a href="http://www.fac-institute.com" style="font-family:'."'"."Open Sans"."'".',sans-serif;font-size:22px;color:white">www.fac-institute.com</a><br><br>
                                            <p style="margin:0 0 1em 0;font-family:'."'"."Open Sans"."'".',sans-serif;font-size:16px;line-height:26px;color:white;font-weight:300;text-align:center">Konsultan Akuntansi dan Lembaga Pendidikan Komputerisasi Akuntansi</p>
                                        </td>
                                      </tr>
                                      <tr height="15">
                                        <td align="left" height="15">&nbsp;</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                                <td align="left" width="20"></td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr height="30">
                        <td align="left" width="100%" height="16"></td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
        </tr>
    </tbody>
</table>';
	}





	//buat MM
public function penawaran($data){
	$onsiteStandard=1000000;
	$paketStandard=4700000;
	$onsiteExpert=1300000;
	$paketEnterpriseSatu=12350000;
	$paketEnterpriseDua=23400000;
	$accurateOnline=1250000;
	$paketAccurateOnline=6000000;

		return '
<!DOCTYPE html>
<html>
<head>
	<title>Penawaran FAC</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	  <style>
    	@page { margin: 180px 35px; }
    	#header { position: fixed; left: 0px; top: -150px; right: 0px; height: 90px; }
    	#footer { position: fixed; left: 0px; bottom: -170px; right: 0px; height: 90px; }
    	#footer .page:after { content: counter(page, upper-roman); }
  	</style>
</head>
<body>

 <div id="header" class="row">
 	<div class="col-md-12">
 		<img class="img img-responsive" src="images/Logo FAC.jpg" style="float:left;width:80px;height:50px;margin-left:40px;" />
 		<img class="img img-responsive" src="images/Accurate Online.jpg" style="width:110px;height:30px;margin-left:450px;" />
 		<br><br>
 		<img class="img img-responsive" src="images/Accurate-5-1.jpg" style="float:left;width:150px;height:50px;margin-left:40px" />
 		<img class="img img-responsive" src="images/rene.jpg" style="width:110px;height:30px;margin-left:380px;" />
 	</div>
 </div>

  <div id="footer" class="row" style="font-size:x-small;line-height:5px;text-align:right;">
    <div class="col-md-12">
		<p style="color:red;">FAC Institute (First Asian Consulting)</p>
		<p>“Lembaga Pendidikan Komputer Sistem Akuntansi ACCURATE”</p>
		<p>Telp. 0812 900 83983 / 0823 1194 4359, Email : training@fac-institute.com, finance@fac-institute.com</p>
		<p>http://fac-institute.com http://fac-institute.blogspot.com, http://solutioncenteraccurate.wordpress.com</p>
	</div>
  </div>


	<div class="container" style="margin-top:-50px">

		<div class="row" style="padding-top:30px">
			<div class="col-md-12">
				<section class="col-md-8">
					<p>
					Kepada Yth,<br>
					<strong>'.$data->perusahaan.'</strong><br>
					</p>
					<table>
						<tbody>
							<tr>
								<td style="width:50px;">Nama</td>
								<td>:</td>
								<td style="padding-left:10px;">'.$data->nama.'</td>
							</tr>
							<tr>
								<td>Email</td>
								<td>:</td>
								<td style="padding-left:10px;">'.$data->email.'</td>
							</tr>
						</tbody>
					</table>
				</section>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<center>
					<h2>Form Onsite Implementasi ACCURATE & RENE</h2>
				</center>
			</div>
		</div>
		<br>
		<!--start table-->
		<div class="row">
			<div class="col-md-12">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th style="text-align:center;">Nama Produk</th>
							<th style="text-align:center;">Qty</th>
							<th style="text-align:center;">Harga</th>
							<th style="text-align:center;">Jumlah</th>
						</tr>
					</thead>
						<tbody>
							<!--Start Standard-->
							<tr>
								<td colspan="4" style="text-align:center;">
									<h4>STANDARD, DELUXE EDITION & RENE (STANDARD)</h4>
								</td>
							</tr>
							<tr>
								<td>
								<label>Onsite Standard (OS)</label><br>
								(1 Hari implementasi, Max 7 Jam, Oleh 1 Orang)
								</td>
								<td style="text-align:center;vertical-align:middle;">.....Hari</td>
								<td style="text-align:center;vertical-align:middle;">Rp '.number_format($onsiteStandard).'.-</td>
								<td></td>
							</tr>
							<tr>
								<td>
								<label>Paket Standard (PS)</label><br>
								(5 Hari implementasi, Max 7 Jam, Oleh 1 Orang)
								</td>
								<td style="text-align:center;vertical-align:middle;">....x 5 Hari</td>
								<td style="text-align:center;vertical-align:middle;">Rp '.number_format($paketStandard).'.-</td>
								<td></td>
							</tr>
							<!--End Standard-->
							<!--Start Enterprise-->
							<tr>
								<td colspan="4" style="text-align:center;">
									<h4>ENTERPRISE (EXPERT)</h4>
								</td>
							</tr>
							<tr>
								<td>
								<label>Onsite Expert (OE)</label><br>
								(1 Hari implementasi, Max 7 Jam, Oleh 1 Orang)
								</td>
								<td style="text-align:center;vertical-align:middle;">.....Hari</td>
								<td style="text-align:center;vertical-align:middle;">Rp '.number_format($onsiteExpert).'.-</td>
								<td></td>
							</tr>
							<tr>
								<td>
								<label>Paket Expert 1 (PE 1)</label><br>
								(10 Hari implementasi, Max 7 Jam, Oleh 1 Orang)
								</td>
								<td style="text-align:center;vertical-align:middle;">....x 10 Hari</td>
								<td style="text-align:center;vertical-align:middle;">Rp '.number_format($paketEnterpriseSatu).'.-</td>
								<td></td>
							</tr>
							<tr>
								<td>
								<label>Paket Expert 2 (PE 2)</label><br>
								(20 Hari implementasi, Max 7 Jam, Oleh 1 Orang)
								</td>
								<td style="text-align:center;vertical-align:middle;">....x 20 Hari</td>
								<td style="text-align:center;vertical-align:middle;">Rp '.number_format($paketEnterpriseDua).'.-</td>
								<td></td>
							</tr>
							<!--End Enterprise-->
							<!--Start Accurate Online-->
							<tr>
								<td colspan="4" style="text-align:center;">
									<h4>ACCURATE ONLINE (AOL)</h4>
								</td>
							</tr>
							<tr>
								<td>
								<label>Accurate Online 1 (AOL 1)</label><br>
								(1 Hari implementasi, Max 7 Jam, Oleh 1 Orang)
								</td>
								<td style="text-align:center;vertical-align:middle;">.....Hari</td>
								<td style="text-align:center;vertical-align:middle;">Rp '.number_format($accurateOnline).'.-</td>
								<td></td>
							</tr>
							<tr>
								<td>
								<label>Accurate Online 2 (AOL 2)</label><br>
								(5 Hari implementasi, Max 7 Jam, Oleh 1 Orang)
								</td>
								<td style="text-align:center;vertical-align:middle;">....x 5 Hari</td>
								<td style="text-align:center;vertical-align:middle;">Rp '.number_format($paketAccurateOnline).'.-</td>
								<td></td>
							</tr>
							'.$this->tabelTransport($data->transport).'
							<tr>
								<td colspan="2"></td>
								<td><strong>TOTAL</strong></td>
								<td></td>
							</tr>
							<!--End Accurate Online-->
						</tbody>
				</table>
			</div>
		</div>
		<!--end table-->

		<div class="row">
			<div class="col-md-12">
			Note:
			</div>
		</div>

		<div class="row" style="font-size:x-small;">
			<div class="col-md-12">
				<ul>
					<li style="color:blue">
						Harga diatas adalah tarif nett yang diterima oleh kami (tidak dipotong PPh). Kami bukan PKP, tidak ada faktur pajak juga tidak ada NPWP untuk bukti potong PPh 23.
					</li>
					'.$this->getTransport($data->transport).'
				</ul>
			</div>
		</div>
		<br>
		<br>


		<div class="row">
			<div class="col-md-12">
				<h3>Biodata Perusahaan</h3>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<table border="0" style="width: 100%">
					<thead></thead>
					<tbody>
					<tr>
						<td style="width:150px">Nama Perusahaan</td>
						<td style="width:5px">:</td>
						<td></td>
					</tr>
					<tr style="height:10px!important;"><td colspan="3"><br></td></tr>
					<tr>
						<td style="width:150px;line-height:20px;">Alamat Perusahaan (Invoice)</td>
						<td>:</td>
						<td></td>
					</tr>
					<tr style="height:25px"><td colspan="3"><br></td></tr>
					<tr>
						<td style="width:150px;line-height:20px;">Alamat Pelaksanaan Training</td>
						<td>:</td>
						<td></td>
					</tr>
					<tr style="height:25px"><td colspan="3"><br></td></tr>
					<tr>
						<td style="width:150px">Telepon & Fax</td>
						<td>:</td>
						<td></td>
					</tr>
					<tr style="height:10px"><td colspan="3"><br></td></tr>
					<tr>
						<td style="width:150px">Email</td>
						<td>:</td>
						<td></td>
					</tr>
					<tr style="height:10px"><td colspan="3"><br></td></tr>
					<tr>
						<td style="width:150px">Contact Person/Hp</td>
						<td>:</td>
						<td></td>
					</tr>
					<tr style="height:10px"><td colspan="3"><br></td></tr>
					<tr>
						<td style="width:150px">Jabatan</td>
						<td>:</td>
						<td></td>
					</tr>
					<tr style="height:10px"><td colspan="3"><br></td></tr>
					<tr>
						<td style="width:150px">Jenis Usaha</td>
						<td>:</td>
						<td></td>
					</tr>
					<tr style="height:10px"><td colspan="3"><br></td></tr>
					<tr>
						<td style="width:150px">Tanggal Rencana Implementasi</td>
						<td>:</td>
						<td></td>
					</tr>
					<tr style="height:10px"><td colspan="3"><br></td></tr>
					<tr>
						<td style="width:150px">Jenis Accurate</td>
						<td>:</td>
						<td style="padding-left:10px">
						Accurate 4 / Accurate 5 / Rene
						<span>*coret yang tidak perlu</span>
						</td>
					</tr>
					<tr style="height:10px"><td colspan="3"><br></td></tr>
					<tr>
						<td style="width:150px">Varian</td>
						<td>:</td>
						<td style="padding-left:10px">
						Standard / Deluxe / Enterprise <span>Coret yang tidak perlu</span>
						</td>
					</tr>
					<tr style="height:10px"><td colspan="3"><br></td></tr>
					<tr>
						<td style="width:150px">Agenda Kegiatan</td>
						<td>:</td>
						<td style="padding-left:10px">
							<div class="checkbox"><label><input type="checkbox">1.</label></div>
							<div class="checkbox"><label><input type="checkbox">2.</label></div>
							<div class="checkbox"><label><input type="checkbox">3.</label></div>
						</td>
					</tr>
					</tbody>
				</table>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-12">
				<section>
				<label style="color:red">Note</label>
				<p>
				Semua bagian dan field pada form ini wajib di isi, guna membantu team implementator. Form yang tidak lengkap tidak akan diproses. Form ini wajib dibubuhkan tanda tangan dan stempel perusahaan sebagai tanda persetujuan pemesanan, kemudian dikirimkan by email ke training@fac-institute.com sebagai tanda pemesanan.
				</p>
				</section>
			</div>
		</div>
		'.$this->breakLine($data->transport).'		
		<div class="row">
			<div class="col-md-12">
				<h3>Syarat dan Kententuan Implementasi</h3>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<ol>
					<li>
						Mengisi Form Onsite Implementasi dan mengirimkan kembali form tersebut via email training@fac-institute.com
					</li>
					<li>
						Pemesanan implementasi paling lambat 3 hari sebelum tanggal rencana pelaksanaan.
					</li>
					<li>
						Tanggal rencana pelaksanaan implementasi yang diisi pada form implementasi adalah jadwal sementara. Kepastian jadwal akan dikonfirmasi setelah disepakati oleh Kedua Pihak.
					</li>
					<li>
						Setelah mendapat konfirmasi kepastian jadwal implementasi dari FAC Institute dan jadwal tersebut telah disepakati antara FAC Institute dengan pemesan implementasi, maka FAC Institute akan membuatkan Sales Invoice.
					</li>
					<li>
						Pembayaran onsite dapat ditransfer ke rekening dibawah ini paling lambat dua (2) hari sebelum pelaksanaan Onsite.
						<ul>
							<li><strong>Bank BCA KCU Bekasi No. Acc. 0663162851 a/n : Fajar Shodiq</strong></li>
							<li><strong>Bank Mandiri KCP Bekasi Ahmad Yani No. Acc. 1670001377901 a/n : Fajar Shodiq</strong></li>
						</ul>
					</li>
					<li>
						Kelalaian pembayaran yang mengakibatkan pembatalan onsite, sepenuhnya menjadi tanggungjawab pemesan.
					</li>
					<li>Kelalaian pembayaran yang mengakibatkan pembatalan onsite, sepenuhnya menjadi tanggungjawab pemesan.</li>
					<li>
						Waktu pelaksanaan implementasi pukul 09:00 – 16:00. <br>
						Jika Implementator kami terlambat, maka Implementator wajib mengganti waktu keterlambatan hanya di hari yang sama bukan di hari lain.
					</li>
					<li>
						Kelebihan jam training akan dikenakan tarif over time Rp. 75.000 per jam (tujuh puluh lima ribu rupiah). Jika lembur, mohon di print dan diisi form lembur terlebih dahulu yang telah kami lampirkan.
					</li>
					<li>
						Pihak pemesan onsite (customer) wajib menyediakan makan siang bagi implementator.
					</li>
					<li>
						Hari kerja implementator kami adalah Senin s/d Jumat. Diluar jam kerja tersebut harus persetujuan dengan FAC Institute dan dikenakan charge sebesar Rp 200.000.
					</li>
					<li>
						Perusahaan wajib menyiapkan data saldo awal untuk membantu jalannya proses implementasi dalam format excel,  seperti:
						<ul>
							<li>Data Pelanggan/customer dan saldo piutang.</li>
							<li>Data Pemasok/Vendor dan saldo hutang.</li>
							<li>Data Akun-akun/COA dan saldonya.</li>
							<li>Data barang/jasa dan saldonya.</li>
							<li>Data Fixed Asset dan saldonya.</li>
							<li>Data Bil Of Material (bagi perusahaan manufaktur).</li>
						</ul>
					</li>
					<li>
						Jika data belum siap selama implementasi dan waktu implementasi ini digunakan untuk membantu menyelesaikan data- data yang diperlukan, maka waktu yang digunakan tersebut tidak bisa digantikan.
					</li>
					<li>
						Untuk Paket Standard dan Expert waktu pelaksanaannya harus berurutan sesuai jumlah hari dan tidak boleh dipecah-pecah. Apabila waktunya ingin terpisah maka dikenakan biaya implementasi harian.
					</li>
					<li>
						Pembatalan atau perubahan jadwal implementasi oleh Customer harus disampaikan ke FAC paling lambat 2 hari sebelum waktu pelaksanaan implementasi, apabila dilakukan 1 (satu) hari sebelumnya atau di hari pelaksanaan maka dikenakan denda Rp.250.000/hari. Selanjutnya waktu pelaksanaan akan direschedule kembali.
					</li>
					<li>
						Jadwal implementasi yang sudah disepakati dan sedang dalam pelaksanaan tidak dapat diubah.
					</li>
					<li>
						Implementasi diluar kota, biaya akomodasi, makan dan perjalanan ditanggung oleh Customer. Meliputi :
						<ul>
							<li>Tiket pergi dan pulang.</li>
							<li>Penginapan/Mess/hotel.</li>
							<li>
							Transportasi selama perjalanan dari tempat domisili implementator sampai tempat pelaksanaan implementasi dan sebaliknya.
							</li>
							<li>
								Biaya makan
							</li>
						</ul>
					</li>
					<li>
						Tugas implementator mencakup:
						<ul>
							<li>Membantu menyiapkan Setup Awal Database.</li>
							<li>
								Deliver "How To Use Accurate", yaitu :
								<ul>
									<li>
										Cara Input Transaksi modul penjualan, pembelian, persediaan, general ledger, aktiva tetap, RMA.
									</li>
									<li>
										Cara Input Transaksi modul project (khusus Deluxe Edition - Contractor).
									</li>
									<li>
										Cara Input Transaksi modul manufaktur (khusus Enterprise Edition).
									</li>
									<li>
										Membantu mengajarkan cara pembuatan template (bukan membuatkan template).
									</li>
								</ul>
							</li>
							<li>
								Jumlah hari implementasi untuk melakukan tugas implementasi diatas tergantung kemampuan user yang belajar.
							</li>
						</ul>
					</li>
					<li>
						Pertanyaan/kasus yang diajukan customer diluar lingkup tugas Implementor yang disebutkan dalam point 18 tidak menjadi kewajiban dari implementor untuk menjawab.
					</li>
					<li>
						Mengenai setting hardware, setting internet ataupun setting jaringan ini bukan kewajiban dan tugas dari Implementator.
					</li>
					<li>
						Implementator tidak menyediakan laptop/netbook/infokus ketika implementasi, untuk implementasi semua kebutuhan peralatan disediakan oleh pihak customer/perusahaan pemesan.
					</li>
					<li>
						Untuk setting template bukan merupakan tugas Implementator ataupun dibawa pulang untuk pengerjaannya.
					</li>
					<li>
						Jasa pembuatan design template adalah layanan diluar implementasi ACCURATE sehingga jika Customer meminta dibuatkan mohon terlebih dulu mengirimkan contoh formnya via email training@fac-institute.com agar pihak FAC Institute yang memutuskan bisa atau tidaknya design tersebut dibuat ke dalam ACCURATE. Setting atau desain template diluar jam training akan dikenakan biaya Rp 250.000 per masing-masing desain.
					</li>
					<li>
						Setelah selesai implementasi berdasarkan hitungan hari yang telah dibayar, klien tidak dapat meminta layanan tambahan (extra) implementasi tanpa dengan biaya.
					</li>
					<li>
						Klaim Onsite yang belum dilaksanakan oleh pihak FAC Institute, selambat-lambatnya dilaksanakan dalam waktu 1 bulan setelah tanggal terjadinya reschedule. Apabila melebihi waktu yang telah ditentukan maka klaim tersebut akan dianggap HANGUS.
					</li>
				</ol>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<p>
					Saya menyatakan telah membaca, mengerti dan bersedia mematuhi seluruh syarat dan ketentuan implementasi yang berlaku.
				</p>
			</div>
		</div>
		<br><br>
		<div class="row">
			<div class="col-md-12">
				<table border="0" style="width:100%">
					<tbody>
						<tr>
							<td style="text-align:center;width:50%;">Hormat Kami</td>
							<td style="text-align:center;width:50%;">Pemesan</td>
						</tr>
						<tr style="height:100px"><td colspan="2"><br><br><br></td></tr>
						<tr>
							<td style="text-align:center;"><strong>'.$data->petugas.'</strong></td>
							<td style="text-align:center;">______________________________________</td>
						</tr>
						<tr>
							<td style="text-align:center;">Fac Institute</td>
							<td style="text-align:center;">Tanda tangan, Cap dan Nama Jelas</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		
	</div><!--end container-->
</body>
</html>

		';
	}
	//non MM
	public function penawaran2($data){
		$onsiteStandard=1000000;
		$paketStandard=4700000;
		$onsiteExpert=1300000;
		$paketEnterpriseSatu=12350000;
		$paketEnterpriseDua=23400000;
		$accurateOnline=1250000;
		$paketAccurateOnline=6000000;

		return '
<!DOCTYPE html>
<html>
<head>
	<title>Penawaran FAC</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	  <style>
    	@page { margin: 180px 35px; }
    	#header { position: fixed; left: 0px; top: -150px; right: 0px; height: 90px; }
    	#footer { position: fixed; left: 0px; bottom: -170px; right: 0px; height: 90px; }
    	#footer .page:after { content: counter(page, upper-roman); }
  	</style>
</head>
<body>

 <div id="header" class="row">
 	<div class="col-md-12">
 		<img class="img img-responsive" src="images/Logo FAC.jpg" style="float:left;width:80px;height:50px;margin-left:40px;" />
 		<img class="img img-responsive" src="images/Accurate Online.jpg" style="width:110px;height:30px;margin-left:450px;" />
 		<br><br>
 		<img class="img img-responsive" src="images/Accurate-5-1.jpg" style="float:left;width:150px;height:50px;margin-left:40px" />
 		<img class="img img-responsive" src="images/rene.jpg" style="width:110px;height:30px;margin-left:380px;" />
 	</div>
 </div>

  <div id="footer" class="row" style="font-size:x-small;line-height:5px;text-align:right;">
    <div class="col-md-12">
		<p style="color:red;">FAC Institute (First Asian Consulting)</p>
		<p>“Lembaga Pendidikan Komputer Sistem Akuntansi ACCURATE”</p>
		<p>Telp. 0812 900 83983 / 0823 1194 4359, Email : training@fac-institute.com, finance@fac-institute.com</p>
		<p>http://fac-institute.com http://fac-institute.blogspot.com, http://solutioncenteraccurate.wordpress.com</p>
	</div>
  </div>


	<div class="container" style="margin-top:-50px">

		<div class="row" style="padding-top:30px">
			<div class="col-md-12">
				<section class="col-md-8">
					<p>
					Kepada Yth,<br>
					<strong>'.$data->perusahaan.'</strong><br>
					</p>
					<table>
						<tbody>
							<tr>
								<td style="width:50px;">Nama</td>
								<td>:</td>
								<td style="padding-left:10px;">'.$data->nama.'</td>
							</tr>
							<tr>
								<td>Email</td>
								<td>:</td>
								<td style="padding-left:10px;">'.$data->email.'</td>
							</tr>
						</tbody>
					</table>
				</section>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<center>
					<h2>Form Onsite Implementasi ACCURATE & RENE</h2>
				</center>
			</div>
		</div>
		<br>
		<!--start table-->
		<div class="row">
			<div class="col-md-12">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th style="text-align:center;">Nama Produk</th>
							<th style="text-align:center;">Qty</th>
							<th style="text-align:center;">Harga</th>
							<th style="text-align:center;">Jumlah</th>
						</tr>
					</thead>
						<tbody>
							<!--Start Standard-->
							<tr>
								<td colspan="4" style="text-align:center;">
									<h4>STANDARD, DELUXE EDITION - NON KONTRAKTOR & RENE (STANDARD)</h4>
								</td>
							</tr>
							<tr>
								<td>
								<label>Onsite Standard (OS)</label><br>
								(1 Hari implementasi, Max 7 Jam, Oleh 1 Orang)
								</td>
								<td style="text-align:center;vertical-align:middle;">.....Hari</td>
								<td style="text-align:center;vertical-align:middle;">Rp '.number_format($onsiteStandard).'.-</td>
								<td></td>
							</tr>
							<tr>
								<td>
								<label>Paket Standard (PS)</label><br>
								(5 Hari implementasi, Max 7 Jam, Oleh 1 Orang)
								</td>
								<td style="text-align:center;vertical-align:middle;">....x 5 Hari</td>
								<td style="text-align:center;vertical-align:middle;">Rp '.number_format($paketStandard).'.-</td>
								<td></td>
							</tr>
							<!--End Standard-->
							<!--Start Enterprise-->
							<tr>
								<td colspan="4" style="text-align:center;">
									<h4>ENTERPRISE, DELUXE EDITION - KONTRAKTOR (EXPERT)</h4>
								</td>
							</tr>
							<tr>
								<td>
								<label>Onsite Expert (OE)</label><br>
								(1 Hari implementasi, Max 7 Jam, Oleh 1 Orang)
								</td>
								<td style="text-align:center;vertical-align:middle;">.....Hari</td>
								<td style="text-align:center;vertical-align:middle;">Rp '.number_format($onsiteExpert).'.-</td>
								<td></td>
							</tr>
							<tr>
								<td>
								<label>Paket Expert 1 (PE 1)</label><br>
								(10 Hari implementasi, Max 7 Jam, Oleh 1 Orang)
								</td>
								<td style="text-align:center;vertical-align:middle;">....x 10 Hari</td>
								<td style="text-align:center;vertical-align:middle;">Rp '.number_format($paketEnterpriseSatu).'.-</td>
								<td></td>
							</tr>
							<tr>
								<td>
								<label>Paket Expert 2 (PE 2)</label><br>
								(20 Hari implementasi, Max 7 Jam, Oleh 1 Orang)
								</td>
								<td style="text-align:center;vertical-align:middle;">....x 20 Hari</td>
								<td style="text-align:center;vertical-align:middle;">Rp '.number_format($paketEnterpriseDua).'.-</td>
								<td></td>
							</tr>
							<!--End Enterprise-->
							<!--Start Accurate Online-->
							<tr>
								<td colspan="4" style="text-align:center;">
									<h4>ACCURATE ONLINE (AOL)</h4>
								</td>
							</tr>
							<tr>
								<td>
								<label>Accurate Online 1 (AOL 1)</label><br>
								(1 Hari implementasi, Max 7 Jam, Oleh 1 Orang)
								</td>
								<td style="text-align:center;vertical-align:middle;">.....Hari</td>
								<td style="text-align:center;vertical-align:middle;">Rp '.number_format($accurateOnline).'.-</td>
								<td></td>
							</tr>
							<tr>
								<td>
								<label>Accurate Online 2 (AOL 2)</label><br>
								(5 Hari implementasi, Max 7 Jam, Oleh 1 Orang)
								</td>
								<td style="text-align:center;vertical-align:middle;">....x 5 Hari</td>
								<td style="text-align:center;vertical-align:middle;">Rp '.number_format($paketAccurateOnline).'.-</td>
								<td></td>
							</tr>
							'.$this->tabelTransport($data->transport).'
							<tr>
								<td colspan="2"></td>
								<td><strong>TOTAL</strong></td>
								<td></td>
							</tr>
							<!--End Accurate Online-->
						</tbody>
				</table>
			</div>
		</div>
		<!--end table-->

		<div class="row">
			<div class="col-md-12">
			Note:
			</div>
		</div>

		<div class="row" style="font-size:x-small;">
			<div class="col-md-12">
				<ul>
					<li style="color:blue">
						Harga diatas adalah tarif nett yang diterima oleh kami (tidak dipotong PPh). Kami bukan PKP, tidak ada faktur pajak juga tidak ada NPWP untuk bukti potong PPh 23.
					</li>
					'.$this->getTransport($data->transport).'
				</ul>
			</div>
		</div>
		<br>
		<br>


		<div class="row">
			<div class="col-md-12">
				<h3>Biodata Perusahaan</h3>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<table border="0" style="width: 100%">
					<thead></thead>
					<tbody>
					<tr>
						<td style="width:150px">Nama Perusahaan</td>
						<td style="width:5px">:</td>
						<td></td>
					</tr>
					<tr style="height:10px!important;"><td colspan="3"><br></td></tr>
					<tr>
						<td style="width:150px;line-height:20px;">Alamat Perusahaan (Invoice)</td>
						<td>:</td>
						<td></td>
					</tr>
					<tr style="height:25px"><td colspan="3"><br></td></tr>
					<tr>
						<td style="width:150px;line-height:20px;">Alamat Pelaksanaan Training</td>
						<td>:</td>
						<td></td>
					</tr>
					<tr style="height:25px"><td colspan="3"><br></td></tr>
					<tr>
						<td style="width:150px">Telepon & Fax</td>
						<td>:</td>
						<td></td>
					</tr>
					<tr style="height:10px"><td colspan="3"><br></td></tr>
					<tr>
						<td style="width:150px">Email</td>
						<td>:</td>
						<td></td>
					</tr>
					<tr style="height:10px"><td colspan="3"><br></td></tr>
					<tr>
						<td style="width:150px">Contact Person/Hp</td>
						<td>:</td>
						<td></td>
					</tr>
					<tr style="height:10px"><td colspan="3"><br></td></tr>
					<tr>
						<td style="width:150px">Jabatan</td>
						<td>:</td>
						<td></td>
					</tr>
					<tr style="height:10px"><td colspan="3"><br></td></tr>
					<tr>
						<td style="width:150px">Jenis Usaha</td>
						<td>:</td>
						<td></td>
					</tr>
					<tr style="height:10px"><td colspan="3"><br></td></tr>
					<tr>
						<td style="width:150px">Tanggal Rencana Implementasi</td>
						<td>:</td>
						<td></td>
					</tr>
					<tr style="height:10px"><td colspan="3"><br></td></tr>
					<tr>
						<td style="width:150px">Jenis Accurate</td>
						<td>:</td>
						<td style="padding-left:10px">
						Accurate 4 / Accurate 5 / Rene
						<span>*coret yang tidak perlu</span>
						</td>
					</tr>
					<tr style="height:10px"><td colspan="3"><br></td></tr>
					<tr>
						<td style="width:150px">Varian</td>
						<td>:</td>
						<td style="padding-left:10px">
						Standard / Deluxe / Enterprise <span>Coret yang tidak perlu</span>
						</td>
					</tr>
					<tr style="height:10px"><td colspan="3"><br></td></tr>
					<tr>
						<td style="width:150px">Agenda Kegiatan</td>
						<td>:</td>
						<td style="padding-left:10px">
							<div class="checkbox"><label><input type="checkbox">1.</label></div>
							<div class="checkbox"><label><input type="checkbox">2.</label></div>
							<div class="checkbox"><label><input type="checkbox">3.</label></div>
						</td>
					</tr>
					</tbody>
				</table>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-12">
				<section>
				<label style="color:red">Note</label>
				<p>
				Semua bagian dan field pada form ini wajib di isi, guna membantu team implementator. Form yang tidak lengkap tidak akan diproses. Form ini wajib dibubuhkan tanda tangan dan stempel perusahaan sebagai tanda persetujuan pemesanan, kemudian dikirimkan by email ke training@fac-institute.com sebagai tanda pemesanan.
				</p>
				</section>
			</div>
		</div>
		'.$this->breakLine($data->transport).'
		<div class="row">
			<div class="col-md-12">
				<h3>Syarat dan Ketentuan Implementasi</h3>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<ol>
					<li>
						Mengisi Form Onsite Implementasi dan mengirimkan kembali form tersebut via email training@fac-institute.com
					</li>
					<li>
						Pemesanan implementasi paling lambat 3 hari sebelum tanggal rencana pelaksanaan.
					</li>
					<li>
						Tanggal rencana pelaksanaan implementasi yang diisi pada form implementasi adalah jadwal sementara. Kepastian jadwal akan dikonfirmasi setelah disepakati oleh Kedua Pihak.
					</li>
					<li>
						Setelah mendapat konfirmasi kepastian jadwal implementasi dari FAC Institute dan jadwal tersebut telah disepakati antara FAC Institute dengan pemesan implementasi, maka FAC Institute akan membuatkan Sales Invoice.
					</li>
					<li>
						Pembayaran onsite dapat ditransfer ke rekening dibawah ini paling lambat dua (2) hari sebelum pelaksanaan Onsite.
						<ul>
							<li><strong>Bank BCA KCU Bekasi No. Acc. 0663162851 a/n : Fajar Shodiq</strong></li>
							<li><strong>Bank Mandiri KCP Bekasi Ahmad Yani No. Acc. 1670001377901 a/n : Fajar Shodiq</strong></li>
						</ul>
					</li>
					<li>
						Kelalaian pembayaran yang mengakibatkan pembatalan onsite, sepenuhnya menjadi tanggungjawab pemesan.
					</li>
					<li>Kelalaian pembayaran yang mengakibatkan pembatalan onsite, sepenuhnya menjadi tanggungjawab pemesan.</li>
					<li>
						Waktu pelaksanaan implementasi pukul 09:00 – 16:00. <br>
						Jika Implementator kami terlambat, maka Implementator wajib mengganti waktu keterlambatan hanya di hari yang sama bukan di hari lain.
					</li>
					<li>
						Kelebihan jam training akan dikenakan tarif over time Rp. 75.000 per jam (tujuh puluh lima ribu rupiah). Jika lembur, mohon di print dan diisi form lembur terlebih dahulu yang telah kami lampirkan.
					</li>
					<li>
						Pihak pemesan onsite (customer) wajib menyediakan makan siang bagi implementator.
					</li>
					<li>
						Hari kerja implementator kami adalah Senin s/d Jumat. Diluar jam kerja tersebut harus persetujuan dengan FAC Institute dan dikenakan charge sebesar Rp 200.000.
					</li>
					<li>
						Perusahaan wajib menyiapkan data saldo awal untuk membantu jalannya proses implementasi dalam format excel,  seperti:
						<ul>
							<li>Data Pelanggan/customer dan saldo piutang.</li>
							<li>Data Pemasok/Vendor dan saldo hutang.</li>
							<li>Data Akun-akun/COA dan saldonya.</li>
							<li>Data barang/jasa dan saldonya.</li>
							<li>Data Fixed Asset dan saldonya.</li>
							<li>Data Bil Of Material (bagi perusahaan manufaktur).</li>
						</ul>
					</li>
					<li>
						Jika data belum siap selama implementasi dan waktu implementasi ini digunakan untuk membantu menyelesaikan data- data yang diperlukan, maka waktu yang digunakan tersebut tidak bisa digantikan.
					</li>
					<li>
						Untuk Paket Standard dan Expert waktu pelaksanaannya harus berurutan sesuai jumlah hari dan tidak boleh dipecah-pecah. Apabila waktunya ingin terpisah maka dikenakan biaya implementasi harian.
					</li>
					<li>
						Pembatalan atau perubahan jadwal implementasi oleh Customer harus disampaikan ke FAC paling lambat 2 hari sebelum waktu pelaksanaan implementasi, apabila dilakukan 1 (satu) hari sebelumnya atau di hari pelaksanaan maka dikenakan denda Rp.250.000/hari. Selanjutnya waktu pelaksanaan akan direschedule kembali.
					</li>
					<li>
						Jadwal implementasi yang sudah disepakati dan sedang dalam pelaksanaan tidak dapat diubah.
					</li>
					<li>
						Implementasi diluar kota, biaya akomodasi, makan dan perjalanan ditanggung oleh Customer. Meliputi :
						<ul>
							<li>Tiket pergi dan pulang.</li>
							<li>Penginapan/Mess/hotel.</li>
							<li>
							Transportasi selama perjalanan dari tempat domisili implementator sampai tempat pelaksanaan implementasi dan sebaliknya.
							</li>
							<li>
								Biaya makan
							</li>
						</ul>
					</li>
					<li>
						Tugas implementator mencakup:
						<ul>
							<li>Membantu menyiapkan Setup Awal Database.</li>
							<li>
								Deliver "How To Use Accurate", yaitu :
								<ul>
									<li>
										Cara Input Transaksi modul penjualan, pembelian, persediaan, general ledger, aktiva tetap, RMA.
									</li>
									<li>
										Cara Input Transaksi modul project (khusus Deluxe Edition - Contractor).
									</li>
									<li>
										Cara Input Transaksi modul manufaktur (khusus Enterprise Edition).
									</li>
									<li>
										Membantu mengajarkan cara pembuatan template (bukan membuatkan template).
									</li>
								</ul>
							</li>
							<li>
								Jumlah hari implementasi untuk melakukan tugas implementasi diatas tergantung kemampuan user yang belajar.
							</li>
						</ul>
					</li>
					<li>
						Pertanyaan/kasus yang diajukan customer diluar lingkup tugas Implementor yang disebutkan dalam point 18 tidak menjadi kewajiban dari implementor untuk menjawab.
					</li>
					<li>
						Mengenai setting hardware, setting internet ataupun setting jaringan ini bukan kewajiban dan tugas dari Implementator.
					</li>
					<li>
						Implementator tidak menyediakan laptop/netbook/infokus ketika implementasi, untuk implementasi semua kebutuhan peralatan disediakan oleh pihak customer/perusahaan pemesan.
					</li>
					<li>
						Untuk setting template bukan merupakan tugas Implementator ataupun dibawa pulang untuk pengerjaannya.
					</li>
					<li>
						Jasa pembuatan design template adalah layanan diluar implementasi ACCURATE sehingga jika Customer meminta dibuatkan mohon terlebih dulu mengirimkan contoh formnya via email training@fac-institute.com agar pihak FAC Institute yang memutuskan bisa atau tidaknya design tersebut dibuat ke dalam ACCURATE. Setting atau desain template diluar jam training akan dikenakan biaya Rp 250.000 per masing-masing desain.
					</li>
					<li>
						Setelah selesai implementasi berdasarkan hitungan hari yang telah dibayar, klien tidak dapat meminta layanan tambahan (extra) implementasi tanpa dengan biaya.
					</li>
					<li>
						Klaim Onsite yang belum dilaksanakan oleh pihak FAC Institute, selambat-lambatnya dilaksanakan dalam waktu 1 bulan setelah tanggal terjadinya reschedule. Apabila melebihi waktu yang telah ditentukan maka klaim tersebut akan dianggap HANGUS.
					</li>
				</ol>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<p>
					Saya menyatakan telah membaca, mengerti dan bersedia mematuhi seluruh syarat dan ketentuan implementasi yang berlaku.
				</p>
			</div>
		</div>
		<br><br>
		<div class="row">
			<div class="col-md-12">
				<table border="0" style="width:100%">
					<tbody>
						<tr>
							<td style="text-align:center;width:50%;">Hormat Kami</td>
							<td style="text-align:center;width:50%;">Pemesan</td>
						</tr>
						<tr style="height:100px"><td colspan="2"><br><br><br></td></tr>
						<tr>
							<td style="text-align:center;"><strong>'.$data->petugas.'</strong></td>
							<td style="text-align:center;">______________________________________</td>
						</tr>
						<tr>
							<td style="text-align:center;">Fac Institute</td>
							<td style="text-align:center;">Tanda tangan, Cap dan Nama Jelas</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		
	</div><!--end container-->
</body>
</html>';
	}


	//non MM
	public function penawaranNonMMKosong(){
		$onsiteStandard=1000000;
		$paketStandard=4700000;
		$onsiteExpert=1300000;
		$paketEnterpriseSatu=12350000;
		$paketEnterpriseDua=23400000;
		$accurateOnline=1250000;
		$paketAccurateOnline=6000000;

		return '
<!DOCTYPE html>
<html>
<head>
	<title>Penawaran FAC</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	  <style>
    	@page { margin: 180px 35px; }
    	#header { position: fixed; left: 0px; top: -150px; right: 0px; height: 90px; }
    	#footer { position: fixed; left: 0px; bottom: -170px; right: 0px; height: 90px; }
    	#footer .page:after { content: counter(page, upper-roman); }
  	</style>
</head>
<body>

 <div id="header" class="row">
 	<div class="col-md-12">
 		<img class="img img-responsive" src="images/Logo FAC.jpg" style="float:left;width:80px;height:50px;margin-left:40px;" />
 		<img class="img img-responsive" src="images/Accurate Online.jpg" style="width:110px;height:30px;margin-left:450px;" />
 		<br><br>
 		<img class="img img-responsive" src="images/Accurate-5-1.jpg" style="float:left;width:150px;height:50px;margin-left:40px" />
 		<img class="img img-responsive" src="images/rene.jpg" style="width:110px;height:30px;margin-left:380px;" />
 	</div>
 </div>

  <div id="footer" class="row" style="font-size:x-small;line-height:5px;text-align:right;">
    <div class="col-md-12">
		<p style="color:red;">FAC Institute (First Asian Consulting)</p>
		<p>“Lembaga Pendidikan Komputer Sistem Akuntansi ACCURATE”</p>
		<p>Telp. 0812 900 83983 / 0823 1194 4359, Email : training@fac-institute.com, finance@fac-institute.com</p>
		<p>http://fac-institute.com http://fac-institute.blogspot.com, http://solutioncenteraccurate.wordpress.com</p>
	</div>
  </div>


	<div class="container" style="margin-top:-50px">

		<div class="row" style="padding-top:30px">
			<div class="col-md-12">
				<section class="col-md-8">
					<p>
					Kepada Yth,<br>
					<strong></strong><br>
					</p>
					<table>
						<tbody>
							<tr>
								<td style="width:50px;">Nama</td>
								<td>:</td>
								<td style="padding-left:10px;"></td>
							</tr>
							<tr>
								<td>Email</td>
								<td>:</td>
								<td style="padding-left:10px;"></td>
							</tr>
						</tbody>
					</table>
				</section>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<center>
					<h2>Form Onsite Implementasi ACCURATE & RENE</h2>
				</center>
			</div>
		</div>
		<br>
		<!--start table-->
		<div class="row">
			<div class="col-md-12">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th style="text-align:center;">Nama Produk</th>
							<th style="text-align:center;">Qty</th>
							<th style="text-align:center;">Harga</th>
							<th style="text-align:center;">Jumlah</th>
						</tr>
						<tbody>
							<!--Start Standard-->
							<tr>
								<td colspan="4" style="text-align:center;">
									<h4>STANDARD, DELUXE EDITION - NON KONTRAKTOR & RENE (STANDARD)</h4>
								</td>
							</tr>
							<tr>
								<td>
								<label>Onsite Standard (OS)</label><br>
								(1 Hari implementasi, Max 7 Jam, Oleh 1 Orang)
								</td>
								<td style="text-align:center;vertical-align:middle;">.....Hari</td>
								<td style="text-align:center;vertical-align:middle;">Rp '.number_format($onsiteStandard).'</td>
								<td></td>
							</tr>
							<tr>
								<td>
								<label>Paket Standard (PS)</label><br>
								(5 Hari implementasi, Max 7 Jam, Oleh 1 Orang)
								</td>
								<td style="text-align:center;vertical-align:middle;">....x 5 Hari</td>
								<td style="text-align:center;vertical-align:middle;">Rp '.number_format($paketStandard).'</td>
								<td></td>
							</tr>
							<!--End Standard-->
							<!--Start Enterprise-->
							<tr>
								<td colspan="4" style="text-align:center;">
									<h4>ENTERPRISE, DELUXE EDITION - KONTRAKTOR (EXPERT)</h4>
								</td>
							</tr>
							<tr>
								<td>
								<label>Onsite Expert (OE)</label><br>
								(1 Hari implementasi, Max 7 Jam, Oleh 1 Orang)
								</td>
								<td style="text-align:center;vertical-align:middle;">.....Hari</td>
								<td style="text-align:center;vertical-align:middle;">Rp '.number_format($onsiteExpert).'</td>
								<td></td>
							</tr>
							<tr>
								<td>
								<label>Paket Expert 1 (PE 1)</label><br>
								(10 Hari implementasi, Max 7 Jam, Oleh 1 Orang)
								</td>
								<td style="text-align:center;vertical-align:middle;">....x 10 Hari</td>
								<td style="text-align:center;vertical-align:middle;">Rp '.number_format($paketEnterpriseSatu).'</td>
								<td></td>
							</tr>
							<tr>
								<td>
								<label>Paket Expert 2 (PE 2)</label><br>
								(20 Hari implementasi, Max 7 Jam, Oleh 1 Orang)
								</td>
								<td style="text-align:center;vertical-align:middle;">....x 20 Hari</td>
								<td style="text-align:center;vertical-align:middle;">Rp '.number_format($paketEnterpriseDua).'</td>
								<td></td>
							</tr>
							<!--End Enterprise-->
							<!--Start Accurate Online-->
							<tr>
								<td colspan="4" style="text-align:center;">
									<h4>ACCURATE ONLINE (AOL)</h4>
								</td>
							</tr>
							<tr>
								<td>
								<label>Accurate Online 1 (AOL 1)</label><br>
								(1 Hari implementasi, Max 7 Jam, Oleh 1 Orang)
								</td>
								<td style="text-align:center;vertical-align:middle;">.....Hari</td>
								<td style="text-align:center;vertical-align:middle;">Rp '.number_format($accurateOnline).'</td>
								<td></td>
							</tr>
							<tr>
								<td>
								<label>Accurate Online 2 (AOL 2)</label><br>
								(5 Hari implementasi, Max 7 Jam, Oleh 1 Orang)
								</td>
								<td style="text-align:center;vertical-align:middle;">....x 5 Hari</td>
								<td style="text-align:center;vertical-align:middle;">Rp '.number_format($paketAccurateOnline).'</td>
								<td></td>
							</tr>
							<tr>
								<td>
									<label>Biaya Transport</label>
								</td>
								<td style="text-align:center;vertical-align:middle;">.....Hari</td>
								<td style="text-align:left;vertical-align:middle;">Rp            </td>
								<td></td>
							</tr>
							<tr>
								<td colspan="2"></td>
								<td><strong>TOTAL</strong></td>
								<td></td>
							</tr>
							<!--End Accurate Online-->
						</tbody>
					</thead>
				</table>
			</div>
		</div>
		<!--end table-->

		<div class="row">
			<div class="col-md-12">
			Note:
			</div>
		</div>

		<div class="row" style="font-size:x-small;">
			<div class="col-md-12">
				<ul>
					<li style="color:blue">
						Harga diatas adalah tarif nett yang diterima oleh kami (tidak dipotong PPh). Kami bukan PKP, tidak ada faktur pajak juga tidak ada NPWP untuk bukti potong PPh 23.
					</li>
					<li style="color:blue">
						Harga belum termasuk biaya transportasi.
						<ul>
							<li>DKI Jakarta Tidak ada.</li>
							<li>Depok, Bogor, Cikarang, Tangerang dan Bekasi Rp 150.000/hari.</li>
							<li>Implementasi diluar kota, biaya akomodasi, makan dan perjalanan ditanggung oleh Customer.</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<br>
		<br>


		<div class="row">
			<div class="col-md-12">
				<h3>Biodata Perusahaan</h3>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<table border="0" style="width: 100%">
					<thead></thead>
					<tbody>
					<tr>
						<td style="width:150px">Nama Perusahaan</td>
						<td style="width:5px">:</td>
						<td></td>
					</tr>
					<tr style="height:10px!important;"><td colspan="3"><br></td></tr>
					<tr>
						<td style="width:150px;line-height:20px;">Alamat Perusahaan (Invoice)</td>
						<td>:</td>
						<td></td>
					</tr>
					<tr style="height:25px"><td colspan="3"><br></td></tr>
					<tr>
						<td style="width:150px;line-height:20px;">Alamat Pelaksanaan Training</td>
						<td>:</td>
						<td></td>
					</tr>
					<tr style="height:25px"><td colspan="3"><br></td></tr>
					<tr>
						<td style="width:150px">Telepon & Fax</td>
						<td>:</td>
						<td></td>
					</tr>
					<tr style="height:10px"><td colspan="3"><br></td></tr>
					<tr>
						<td style="width:150px">Email</td>
						<td>:</td>
						<td></td>
					</tr>
					<tr style="height:10px"><td colspan="3"><br></td></tr>
					<tr>
						<td style="width:150px">Contact Person/Hp</td>
						<td>:</td>
						<td></td>
					</tr>
					<tr style="height:10px"><td colspan="3"><br></td></tr>
					<tr>
						<td style="width:150px">Jabatan</td>
						<td>:</td>
						<td></td>
					</tr>
					<tr style="height:10px"><td colspan="3"><br></td></tr>
					<tr>
						<td style="width:150px">Jenis Usaha</td>
						<td>:</td>
						<td></td>
					</tr>
					<tr style="height:10px"><td colspan="3"><br></td></tr>
					<tr>
						<td style="width:150px">Tanggal Rencana Implementasi</td>
						<td>:</td>
						<td></td>
					</tr>
					<tr style="height:10px"><td colspan="3"><br></td></tr>
					<tr>
						<td style="width:150px">Jenis Accurate</td>
						<td>:</td>
						<td style="padding-left:10px">
						Accurate 4 / Accurate 5 / Rene
						<span>*coret yang tidak perlu</span>
						</td>
					</tr>
					<tr style="height:10px"><td colspan="3"><br></td></tr>
					<tr>
						<td style="width:150px">Varian</td>
						<td>:</td>
						<td style="padding-left:10px">
						Standard / Deluxe / Enterprise <span>Coret yang tidak perlu</span>
						</td>
					</tr>
					<tr style="height:10px"><td colspan="3"><br></td></tr>
					<tr>
						<td style="width:150px">Agenda Kegiatan</td>
						<td>:</td>
						<td style="padding-left:10px">
							<div class="checkbox"><label><input type="checkbox">1.</label></div>
							<div class="checkbox"><label><input type="checkbox">2.</label></div>
							<div class="checkbox"><label><input type="checkbox">3.</label></div>
						</td>
					</tr>
					</tbody>
				</table>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-12">
				<section>
				<label style="color:red">Note</label>
				<p>
				Semua bagian dan field pada form ini wajib di isi, guna membantu team implementator. Form yang tidak lengkap tidak akan diproses. Form ini wajib dibubuhkan tanda tangan dan stempel perusahaan sebagai tanda persetujuan pemesanan, kemudian dikirimkan by email ke training@fac-institute.com sebagai tanda pemesanan.
				</p>
				</section>
			</div>
		</div>
		<br><br><br><br><br><br><br><br>
		<div class="row">
			<div class="col-md-12">
				<h3>Syarat dan Kententuan Implementasi</h3>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<ol>
					<li>
						Mengisi Form Onsite Implementasi dan mengirimkan kembali form tersebut via email training@fac-institute.com
					</li>
					<li>
						Pemesanan implementasi paling lambat 3 hari sebelum tanggal rencana pelaksanaan.
					</li>
					<li>
						Tanggal rencana pelaksanaan implementasi yang diisi pada form implementasi adalah jadwal sementara. Kepastian jadwal akan dikonfirmasi setelah disepakati oleh Kedua Pihak.
					</li>
					<li>
						Setelah mendapat konfirmasi kepastian jadwal implementasi dari FAC Institute dan jadwal tersebut telah disepakati antara FAC Institute dengan pemesan implementasi, maka FAC Institute akan membuatkan Sales Invoice.
					</li>
					<li>
						Pembayaran onsite dapat ditransfer ke rekening dibawah ini paling lambat dua (2) hari sebelum pelaksanaan Onsite.
						<ul>
							<li><strong>Bank BCA KCU Bekasi No. Acc. 0663162851 a/n : Fajar Shodiq</strong></li>
							<li><strong>Bank Mandiri KCP Bekasi Ahmad Yani No. Acc. 1670001377901 a/n : Fajar Shodiq</strong></li>
						</ul>
					</li>
					<li>
						Kelalaian pembayaran yang mengakibatkan pembatalan onsite, sepenuhnya menjadi tanggungjawab pemesan.
					</li>
					<li>Kelalaian pembayaran yang mengakibatkan pembatalan onsite, sepenuhnya menjadi tanggungjawab pemesan.</li>
					<li>
						Waktu pelaksanaan implementasi pukul 09:00 – 16:00. <br>
						Jika Implementator kami terlambat, maka Implementator wajib mengganti waktu keterlambatan hanya di hari yang sama bukan di hari lain.
					</li>
					<li>
						Kelebihan jam training akan dikenakan tarif over time Rp. 75.000 per jam (tujuh puluh lima ribu rupiah). Jika lembur, mohon di print dan diisi form lembur terlebih dahulu yang telah kami lampirkan.
					</li>
					<li>
						Pihak pemesan onsite (customer) wajib menyediakan makan siang bagi implementator.
					</li>
					<li>
						Hari kerja implementator kami adalah Senin s/d Jumat. Diluar jam kerja tersebut harus persetujuan dengan FAC Institute dan dikenakan charge sebesar Rp 200.000.
					</li>
					<li>
						Perusahaan wajib menyiapkan data saldo awal untuk membantu jalannya proses implementasi dalam format excel,  seperti:
						<ul>
							<li>Data Pelanggan/customer dan saldo piutang.</li>
							<li>Data Pemasok/Vendor dan saldo hutang.</li>
							<li>Data Akun-akun/COA dan saldonya.</li>
							<li>Data barang/jasa dan saldonya.</li>
							<li>Data Fixed Asset dan saldonya.</li>
							<li>Data Bil Of Material (bagi perusahaan manufaktur).</li>
						</ul>
					</li>
					<li>
						Jika data belum siap selama implementasi dan waktu implementasi ini digunakan untuk membantu menyelesaikan data- data yang diperlukan, maka waktu yang digunakan tersebut tidak bisa digantikan.
					</li>
					<li>
						Untuk Paket Standard dan Expert waktu pelaksanaannya harus berurutan sesuai jumlah hari dan tidak boleh dipecah-pecah. Apabila waktunya ingin terpisah maka dikenakan biaya implementasi harian.
					</li>
					<li>
						Pembatalan atau perubahan jadwal implementasi oleh Customer harus disampaikan ke FAC paling lambat 2 hari sebelum waktu pelaksanaan implementasi, apabila dilakukan 1 (satu) hari sebelumnya atau di hari pelaksanaan maka dikenakan denda Rp.250.000/hari. Selanjutnya waktu pelaksanaan akan direschedule kembali.
					</li>
					<li>
						Jadwal implementasi yang sudah disepakati dan sedang dalam pelaksanaan tidak dapat diubah.
					</li>
					<li>
						Implementasi diluar kota, biaya akomodasi, makan dan perjalanan ditanggung oleh Customer. Meliputi :
						<ul>
							<li>Tiket pergi dan pulang.</li>
							<li>Penginapan/Mess/hotel.</li>
							<li>
							Transportasi selama perjalanan dari tempat domisili implementator sampai tempat pelaksanaan implementasi dan sebaliknya.
							</li>
							<li>
								Biaya makan
							</li>
						</ul>
					</li>
					<li>
						Tugas implementator mencakup:
						<ul>
							<li>Membantu menyiapkan Setup Awal Database.</li>
							<li>
								Deliver "How To Use Accurate", yaitu :
								<ul>
									<li>
										Cara Input Transaksi modul penjualan, pembelian, persediaan, general ledger, aktiva tetap, RMA.
									</li>
									<li>
										Cara Input Transaksi modul project (khusus Deluxe Edition - Contractor).
									</li>
									<li>
										Cara Input Transaksi modul manufaktur (khusus Enterprise Edition).
									</li>
									<li>
										Membantu mengajarkan cara pembuatan template (bukan membuatkan template).
									</li>
								</ul>
							</li>
							<li>
								Jumlah hari implementasi untuk melakukan tugas implementasi diatas tergantung kemampuan user yang belajar.
							</li>
						</ul>
					</li>
					<li>
						Pertanyaan/kasus yang diajukan customer diluar lingkup tugas Implementor yang disebutkan dalam point 18 tidak menjadi kewajiban dari implementor untuk menjawab.
					</li>
					<li>
						Mengenai setting hardware, setting internet ataupun setting jaringan ini bukan kewajiban dan tugas dari Implementator.
					</li>
					<li>
						Implementator tidak menyediakan laptop/netbook/infokus ketika implementasi, untuk implementasi semua kebutuhan peralatan disediakan oleh pihak customer/perusahaan pemesan.
					</li>
					<li>
						Untuk setting template bukan merupakan tugas Implementator ataupun dibawa pulang untuk pengerjaannya.
					</li>
					<li>
						Jasa pembuatan design template adalah layanan diluar implementasi ACCURATE sehingga jika Customer meminta dibuatkan mohon terlebih dulu mengirimkan contoh formnya via email training@fac-institute.com agar pihak FAC Institute yang memutuskan bisa atau tidaknya design tersebut dibuat ke dalam ACCURATE. Setting atau desain template diluar jam training akan dikenakan biaya Rp 250.000 per masing-masing desain.
					</li>
					<li>
						Setelah selesai implementasi berdasarkan hitungan hari yang telah dibayar, klien tidak dapat meminta layanan tambahan (extra) implementasi tanpa dengan biaya.
					</li>
					<li>
						Klaim Onsite yang belum dilaksanakan oleh pihak FAC Institute, selambat-lambatnya dilaksanakan dalam waktu 1 bulan setelah tanggal terjadinya reschedule. Apabila melebihi waktu yang telah ditentukan maka klaim tersebut akan dianggap HANGUS.
					</li>
				</ol>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<p>
					Saya menyatakan telah membaca, mengerti dan bersedia mematuhi seluruh syarat dan ketentuan implementasi yang berlaku.
				</p>
			</div>
		</div>
		<br><br>
		<div class="row">
			<div class="col-md-12">
				<table border="0" style="width:100%">
					<tbody>
						<tr>
							<td style="text-align:center;width:50%;">Hormat Kami</td>
							<td style="text-align:center;width:50%;">Pemesan</td>
						</tr>
						<tr style="height:100px"><td colspan="2"><br><br><br></td></tr>
						<tr>
							<td style="text-align:center;"><strong>Fajar Shodiq</strong></td>
							<td style="text-align:center;">______________________________________</td>
						</tr>
						<tr>
							<td style="text-align:center;">Fac Institute</td>
							<td style="text-align:center;">Tanda tangan, Cap dan Nama Jelas</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		
	</div><!--end container-->
</body>
</html>';
	}

public function getTransport($value){
	if ($value=='') {
		return '';
	}elseif ($value=='relatif') {
		return '
			<li style="color:blue">
				Harga belum termasuk biaya transportasi.
				<ul>
					<li>Implementasi diluar kota, biaya akomodasi, makan dan perjalanan ditanggung oleh Customer.</li>
				</ul>
			</li>';	
	} else {
		return '
			<li style="color:blue">
				Harga belum termasuk biaya transportasi.
				<ul>
					<li>Biaya transportasi sebesar Rp '.number_format($value).'/hari.</li>
					<li>Implementasi diluar kota, biaya akomodasi, makan dan perjalanan ditanggung oleh Customer.</li>
				</ul>
			</li>';	
	}
	
}

public function tabelTransport($value){
	if ($value=='') {
		return '';
	}elseif ($value=='relatif') {
		return '
			<tr>
				<td>
					<label>Biaya Transport</label>
				</td>
				<td style="text-align:center;vertical-align:middle;">Negosiasi</td>
				<td style="text-align:center;vertical-align:middle;">Negosiasi</td>
				<td></td>
			</tr>';	
	} else {
		return '
			<tr>
				<td>
					<label>Biaya Transport</label>
				</td>
				<td style="text-align:center;vertical-align:middle;">.... Hari</td>
				<td style="text-align:center;vertical-align:middle;">Rp '.number_format($value).'.-</td>
				<td></td>
			</tr>
			';	
	}
}

public function breakLine($value){
	if ($value==''){
		return '<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>';
	}elseif ($value=='relatif'){
		return '<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>';	
	} else {
		return '<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>';	
	}
}



}

?>