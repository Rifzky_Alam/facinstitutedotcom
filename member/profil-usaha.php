<?php $page="profilusaha";?>


<?php include_once '../baseurl.php'; ?>
<?php include_once 'components/sessions.php'; ?>
<?php include_once 'components/top-nav.php'; ?>
<?php include_once 'components/meta.php'; ?>
<?php include_once 'components/links.php'; ?>
<?php include_once 'components/scripts.php'; ?>

<?php Sessions($page) ?>

<html>
<head>
    <?php Meta($page) ?>
	<title>FAC-Institute -- Member</title>
	<?php Links($page) ?>
	
	<style type="text/css">
	
	.glyphicon { margin-right:10px; }
	.panel-body { padding:0px; }
	.panel-body table tr td { padding-left: 15px }
	.panel-body .table {margin-bottom: 0px; }
    .navbar-default .navbar-nav > li > a{color:#fff}
    .navbar-default .navbar-nav > li > a:hover{color:#55f267}
    .navbar-default .navbar-nav > li:hover{background-color:#f0ff00}

	</style>
</head>
<body>
<div class="container-fluid" style="padding-right:0px;padding-left:0px;height:50px;">
</div>
<?php TopNavigation($page); ?>

<div class="container-fluid" style="padding-top:50px">
    <div class="row">
        <div class="col-sm-3 col-md-3">
            <?php include_once 'components/sidebar.php'; Sidebar($page); ?>
        </div>
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <?php if (isset($_GET['step'])&&$_GET['step']=='2'): ?>
                    <h1>FAC-Institute :: Peta Alamat Perusahaan Anda</h1>
                    Untuk kemudahan akses tim kami datang ke lokasi perusahaan anda silahkan untuk menyimpan data lokasi perusahaan anda.
                <?php else: ?>
                    <h1>FAC-Institute :: Edit data perusahaan</h1>
                    Untuk melanjutkan transaksi anda perlu melengkapi data di bawah ini.
                <?php endif ?>                
                 
            </div>

            <div class="row" style="padding-bottom: 40px;">
                <div class="col-md-12">

                <?php if (isset($_GET['step'])&&$_GET['step']=='2'): ?>
                    <?php $page='map'; ?>
                    <div class="row">

                        <div class="col-md-12">
                            <label>Cara menggunakan map:</label>
                            <ol>
                                <li>Anda bisa langsung menandai lokasi anda dengan klik ke wilayah pada map di bawah ini.</li>
                                <li>Atau anda bisa mengetik alamat yang sudah terdaftar di google map, contoh: Plaza Pondok Indah pada kolom lokasi alamat perusahaan anda yang berada di bawah map ini. (terlebih dahulu menghapus data yang berupa nomor)</li>
                            </ol>
                        </div>

                        <div class="col-md-12">
                            <div id="map-canvas" style="width:100%;height:600px;border:1px solid">Silahkan berbagi lokasi (aktifkan berbagi lokasi) pada browser anda untuk memunculkan data google map</div>
                        </div>

                        <div class="col-md-12" style="margin-top:15px">
                            <div class="form-group">
                                <label for="alamat">Lokasi Perusahaan Anda</label>
                                <input type="text" name="data[map]" id="alamatz" class="form-control" placeholder="Cari Nama Tempat atau Tandai Peta di Bawah ini">                    
                            </div>
                        </div>

                        <div class="col-md-12">
                            <button class="btn btn-lg btn-success" style="width:100%">Submit</button>
                        </div>

                    </div>


                <?php else: ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Nama Perusahaan</label>
                                <input type="text" name="in[namausaha]" class="form-control">
                            </div>
                        </div>

                        
                            <div class="col-md-12 alamatz" id="iseng">
                                <label>Alamat Perusahaan</label>
                                <div class="form-inline">
                                    <textarea name="in[alamat][]" class="form-control" style="width:50%"></textarea>
                                    <span class="btn btn-primary" id="btnalamat">
                                        <i class="glyphicon glyphicon-plus" style="margin-right:0px"></i>
                                    </span>
                                </div>
                            </div>

                            <div class="col-md-12 email" style="padding-top:15px;" id="jajal">
                                <label>Email Perusahaan</label>
                                <div class="form-inline">
                                    <input type="text" name="in[email][]" style="width:50%" class="form-control">
                                    <span class="btn btn-primary" id="btnemail">
                                        <i class="glyphicon glyphicon-plus" style="margin-right:0px"></i>
                                    </span>
                                </div>        
                            </div>
                            
                            <div class="col-md-12" style="padding-top:15px;">
                                <div class="form-group">
                                    <label>Kota</label>
                                    <input type="text" name="in[kota]" class="form-control">
                                </div>
                            </div>
                        

                        
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Provinsi</label>
                                    <input type="text" name="in[provinsi]" class="form-control">
                                </div>
                            </div>
                        
                            <div class="col-md-12 telepon" id="coba">
                                <label>Telepon</label>
                                <div class="form-inline">
                                    <input type="text" name="in[telepon]" class="form-control" style="width:50%;">
                                    <span class="btn btn-primary" id="btntelepon">
                                        <i class="glyphicon glyphicon-plus" style="margin-right:0px;"></i>
                                    </span>
                                </div>
                            </div>

                            <div class="col-md-12" style="padding-top:15px;">
                                <div class="form-group">
                                    <h4>Jenis Pengguna</h4>

                                    <div class="radio"><label><input type="radio" name="edit[jnspengguna]" value="baru">Baru</label></div>
                                    <div class="radio"><label><input type="radio" name="edit[jnspengguna]" value="lama" checked>Lama</label></div>
                                </div>
                            </div>


                            <div class="col-md-12">       
                                <div class="form-group">
                                    <h4>Accurate yang anda gunakan</h4>
                                        <div class="checkbox"><label><input type="checkbox" name="in[jnsaccurate][]" value="accurate 4 standard edition">ACCURATE 4 Standar Edition</label></div>
                                        <div class="checkbox"><label><input type="checkbox" name="in[jnsaccurate][]" value="accurate 4 deluxe edition">ACCURATE 4 Deluxe Edition</label></div>
                                        <div class="checkbox"><label><input type="checkbox" name="in[jnsaccurate][]" value="accurate 4 enterprise edition">ACCURATE 4 Enterprise Edition</label></div>
                                        <div class="checkbox"><label><input type="checkbox" name="in[jnsaccurate][]" value="accurate 5 standard edition">ACCURATE 5 Standar Edition</label></div>
                                        <div class="checkbox"><label><input type="checkbox" name="in[jnsaccurate][]" value="accurate 5 deluxe edition">ACCURATE 5 Deluxe Edition</label></div>
                                        <div class="checkbox"><label><input type="checkbox" name="in[jnsaccurate][]" value="accurate 5 enterprise edition">ACCURATE 5 Enterprise Edition</label></div>
                                        <div class="checkbox"><label><input type="checkbox" name="in[jnsaccurate][]" value="accurate cloud">ACCURATE Online/Cloud</label></div>
                                        <div class="checkbox"><label><input type="checkbox" name="in[jnsaccurate][]" value="rene point of sales">RENE Point Of Sales</label></div>
                                        <div class="checkbox"><label><input type="checkbox" name="in[jnsaccurate][]" value="rene dan accurate">Gabungan RENE dan ACCURATE</label></div>
                                </div>
                            </div>
                        
                            <div class="col-md-12" style="padding-top:15px;">
                                <div class="form-group">
                                    <label>Keterangan Jenis Usaha</label>
                                    <textarea class="form-control" name="in[keteranganusaha]"></textarea>
                                </div>        
                            </div>

                            <div class="col-md-12">
                                
                                <div class="form-group">
                                    <label>Tempat Beli Accurate</label>
                                    <input type="text" name="in[telepon]" class="form-control">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Salesman Accurate</label>
                                    <input type="text" name="in[telepon]" class="form-control">
                                </div>
                            </div>
                        
                            <div class="col-md-12">
                                <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>
                            </div>
                    </div><!--end row-->
                <?php endif ?>
                </div>
            </div>

        </div>
    </div>
</div>
    <?php Scripts($page) ?>
</body>
</html>