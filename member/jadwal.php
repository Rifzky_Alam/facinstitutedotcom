<?php $page="jadwal";?>


<?php include_once '../baseurl.php'; ?>
<?php include_once 'components/sessions.php'; ?>
<?php include_once 'components/top-nav.php'; ?>
<?php include_once 'components/meta.php'; ?>
<?php include_once 'components/links.php'; ?>
<?php include_once 'components/modal.php'; ?>
<?php include_once 'components/scripts.php'; ?>

<?php Sessions($page) ?>

<html>
<head>
    <?php Meta($page) ?>
	<title>FAC-Institute -- Member</title>
	<?php Links($page) ?>
	
	<style type="text/css">
	
	.glyphicon { margin-right:10px; }
	.panel-body { padding:0px; }
	.panel-body table tr td { padding-left: 15px }
	.panel-body .table {margin-bottom: 0px; }
    .navbar-default .navbar-nav > li > a{color:#fff}
    .navbar-default .navbar-nav > li > a:hover{color:#55f267}
    .navbar-default .navbar-nav > li:hover{background-color:#f0ff00}

	</style>
</head>
<body>
<div class="container-fluid" style="padding-right:0px;padding-left:0px;height:50px;">
    
</div>
<?php TopNavigation($page); ?>

<div class="container-fluid" style="padding-top:50px">
    <div class="row">
        <div class="col-sm-3 col-md-3">
            <?php include_once 'components/sidebar.php'; Sidebar($page); ?>
        </div>
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1>FAC-Institute :: Jadwal Training Accurate</h1> 
            </div>

            <div class="row">
                <div class="col-md-12">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Item</th>
                                <th>Agenda</th>
                                <th>Tanggal</th>
                                <th>Lokasi</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>My item 1</td>
                                <td>Agenda 1</td>
                                <td>2016-07-01</td>
                                <td>Kantor</td>
                                <td>
                                    <div class="form-inline">
                                        <a class="btn btn-primary" title="Kirim ke email" href="#modal-cari" data-toggle="modal">
                                            <span class="glyphicon glyphicon-send" aria-hidden="true" style="margin-right:0px"></span>
                                        </a>
                                        <a href="#" class="btn btn-danger" title="Cetak PDF">
                                            <span class="glyphicon glyphicon-print" aria-hidden="true" style="margin-right:0px"></span>
                                        </a>
                                    </div>
                                </td>

                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <?php Modal($page) ?>
    <?php Scripts($page) ?>
</body>
</html>