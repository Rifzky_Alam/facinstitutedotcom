<?php 
session_start();
include_once '../baseurl.php';
include_once HomeDirectory().'model/Transaksi.php';

if (isset($_GET['tr'])){

	$transaksi = new Transaksi();
	$transaksi->setID(securitycode($_GET['tr'],'d'));
	$datatrans = $transaksi->FetchTransactionForPreview();
	if (isset($_SESSION['fac_client'])) {

		if (isset($_POST['map'])) {
			include_once HomeDirectory().'model/Perusahaan.php';
			$perusahaan = new Perusahaan();
			$perusahaan->setId($_SESSION['fac_client']['idusahatransfac']);
			$perusahaan->setMap($_POST['map']);
			if ($perusahaan->EditMap()) {
				echo "<script>location.replace('".getBaseUrl().'member/lokasi-training?st=success'."');</script>";
			} else {
				echo "<script>alert('Maaf, ada kesalahan pada sistem kami.');</script>";
			}
		}



		if ($datatrans->id==$_SESSION['fac_client']['idusahatransfac']) {
			$data = ['nama_cust' => $datatrans->nama_cust];
			$data = (object)$data;
			include_once HomeDirectory().'member/view/view-editmap-usaha.php';	
		} else {
			echo "<script>alert('Maaf, Anda tidak terdaftar dalam sistem kami.');</script>";
		}
		
	} else {
		if (isset($_POST['email'])){
			if ($_POST['email']==$datatrans->email_cust) {
				$_SESSION['fac_client'] = ['idcustfac' => $datatrans->id_cust,'idusahatransfac' => $datatrans->id];
				echo "<script>location.replace('".getBaseUrl().'member/lokasi-training?tr='.$_GET['tr']."');</script>";
			} else {
				echo "<script>alert('Maaf, email yang anda masukkan tidak terdaftar.');</script>";		
			}
		}

		$data = ['nama_cust' => $datatrans->nama_cust];
		$data = (object)$data;
		include_once HomeDirectory().'member/view/view-verifikasiemail-cust.php';
	}
		
}elseif (isset($_GET['st'])&&$_GET['st']=='success') {
	unset($_SESSION['fac_client']);
	include_once HomeDirectory().'member/view/view-editmap-success.php';
}

?>