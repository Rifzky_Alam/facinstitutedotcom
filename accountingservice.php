<?php 
include_once $_SERVER['DOCUMENT_ROOT'].'/controller/homepage.controller.php';
if (isset($_SERVER['PATH_INFO'])) {
	@$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
	$key = array_shift($url_segment);
	switch ($key) {
		case 'carapesan':
			Homepagectr::AccountingServiceCaraPesan();
			break;
		case 'jenislayanan':
			Homepagectr::AccountingServiceJenisLayanan();
			break;
		default:
			header('Location: https://fac-institute.com/error/404/');
			break;
	}
}else{
	Homepagectr::AccountingService();
}
