<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/controller/homepage.controller.php';
if (isset($_SERVER['PATH_INFO'])) {
	@$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
	$key = array_shift($url_segment);
	switch ($key) {
		case 'carapesan':
			Homepagectr::CaraPesanTraining();
			break;
		case 'premium':
			Homepagectr::PremiumSupport();
			break;
	}
}else{
	Homepagectr::SupportAfterTraining();
}
