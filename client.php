<?php 

if (isset($_SERVER['PATH_INFO'])) {
	@$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
	$key = array_shift($url_segment);
	switch ($key) {
		case '':
		    include_once 'controller/client.controller.php';
			header('Location: '.Client::base_url().'error/404');
			break;
		case 'daftar-training':
		    include_once 'controller/client.controller.php';
			if (isset($url_segment[0])&&!empty($url_segment[0])) {
				Client::OrderTraining2($url_segment[0]);
			}else {
				Client::OrderTraining();
			}
			break;
		case 'daftar-accservice':
		    include_once 'controller/client.controller.php';
			Client::OrderAccService();
			break;
		case 'questionnaire':
		    include_once 'controller/client.controller.php';
			Client::Questionnaire();
			break;
		case 'tesmail':
		    include_once 'controller/client.controller.php';
			// echo Client::base_url();
			Client::SendEmail();
			break;
		case 'fetchdatacust':
		    include_once 'controller/client.controller.php';
			Client::RegisteredClientData();
			break;
		case 'registersuccess':
		    include_once 'controller/client.controller.php';
			Client::SuccessRegister();
			break;
		case 'premium-support':
		    include_once $_SERVER['DOCUMENT_ROOT'].'/controller/homepage.controller.php';
		    Homepagectr::PremiumSupport();
		    break;
		case 'penawaran':
		    include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/controller/transaksi.controller.php';
		    if(isset($url_segment[0])&&!empty($url_segment[0]))
		        Transaksi::FormPenawaranClient($url_segment[0]);
		    break;
		default:
		    include_once 'controller/client.controller.php';
			header('Location: '.Client::base_url().'error/404');
			break;
	}
}else {
    include_once 'controller/client.controller.php';
	header('Location: '.Client::base_url().'error/404');
}