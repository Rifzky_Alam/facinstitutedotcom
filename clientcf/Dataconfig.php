<?php 

class Data{

	public $homedir = '/home/facinsti/public_html/';//
	// public $homedir = $_SERVER['DOCUMENT_ROOT'].'/';
    public $base_url = "https://fac-institute.com/";
    public $company = 'fac institute';
    public $companyaddr='Jl. Pahlawan Revolusi 10GG No.3 (Lantai 2), RT.9/RW.7, Pd. Bambu, Kec. Duren Sawit, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta Indonesia 13430';
 	public $title;
 	public $subtitle;
 	public $table;
 	public $me_url;
 	public $page;

 	public function Model($file){
		include_once $this->homedir.'model/'.$file.'.php';
	}

	public function IncludeFileWithData($path,$data){
		include_once $this->homedir.$path;
	}

	public function JustInclude($path){
		include_once $this->homedir.$path.'.php';	
	}

	public function View($path,$data){
		include_once $this->homedir.'view/'.$path.'.php';
	}

	public function Lib($file){
		include_once $this->homedir.'libraries/'.$file.'.php';
	}

 	public function __construct(){
		
 	}        
}

?>