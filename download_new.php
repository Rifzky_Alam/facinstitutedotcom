<?php include_once 'baseurl.php'; ?>
<html class="no-js" lang="id">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="google-site-verification" content="EfNVtXecxElbcbugY59SeKXJuMzO67bjVvTvlHCc_CM" />
    <meta name="msvalidate.01" content="D925497D8F382164FF0CE0267D381FDA" />
    <meta name="thumbnailUrl" content="https://www.fac-institute.com/administrasi/artikel/images/7cabb38e259c0d5b53f9bb219daf410f.jpg?w=650" />
    <meta name="description" content="FAC - Kami Melayani Training Software Akuntansi ACCURATE dan Accounting Service Untuk Perusahaan Anda" />
    <meta content="First Asian Consulting. Konsultan Akuntansi & Lembaga Pendidikan Komputerisasi Akuntansi" />
    <meta name="keywords" content="Training,Akuntansi,ACCURATE,Jasa">
    <meta name="author" content="Rifzky Alam, Dino Damara">
    <title>FAC Institute</title>
    <link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/w3.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/animate.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,100,300,500,600,700c" type="text/css" />
 <style>
 .w3-top{
   font-family: "Raleway", sans-serif;
 }
 body, html, h3 {
     font-family: 'PT Sans', sans-serif;
 }
 p,li{
   font-size: 16px;
 }

 </style>

<link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/style.css">
<script src="<?php echo getBaseUrl() ?>assets/homepage/js/jquery-2.1.4.min.js"></script>

</head>
<body>
    <h4>header</h4>
    <?php include_once 'top-nav.php'; ?>
    <div class="w3-row w3-container">
    <div class=" w3-content w3-padding-64" style="max-width:1000px">

  <div class="w3-container">
    <h2>Acurrate 5</h2>
    <p><strong>ACCURATE</strong> Standard Edition</p>

    <table class="w3-table-all">
      <thead>
        <tr style="background-color:#ff1919;color:#fff">
          <th>Version</th>
          <th>32 Bit</th>
          <th>64 Bit</th>
        </tr>
      </thead>
      <tr>
        <td><b>5.0.19.1863XXX ( Latest Version ) ⬆⬆ </b></td>
        <td><a href="http://deluxeaccounting.com/download/accurate/v5/standard/ACCURATE5%20Standard-5.0.19.1863-Setup-x86.exe" target="_blank" class="w3-button w3-white w3-border w3-round-large">Download</a></td>
        <td><a href="http://deluxeaccounting.com/download/accurate/v5/standard/ACCURATE5%20Standard-5.0.19.1863-Setup-x64.exe" target="_blank" class="w3-button w3-indigo w3-border w3-round-large">Download</a></td>
      </tr>
      <tr>
        <td>5.0.16.1807</td>
        <td><a href="http://deluxeaccounting.com/download/previous/accurate/v5/standard/ACCURATE5%20Standard-5.0.16.1807-Setup-x86.exe" target="_blank" class="w3-button w3-white w3-border w3-round-large">Download</a></td>
        <td><a href="http://deluxeaccounting.com/download/previous/accurate/v5/standard/ACCURATE5%20Standard-5.0.16.1807-Setup-x64.exe" target="_blank" class="w3-button w3-indigo w3-border w3-round-large">Download</a></td>
      </tr>
      <tr>
        <td>5.0.16.1806</td>
        <td><a href="http://deluxeaccounting.com/download/previous/accurate/v5/standard/ACCURATE5%20Standard-5.0.16.1806-Setup-x86.exe" target="_blank" class="w3-button w3-white w3-border w3-round-large">Download</a></td>
        <td><a href="http://deluxeaccounting.com/download/previous/accurate/v5/standard/ACCURATE5%20Standard-5.0.16.1806-Setup-x64.exe" target="_blank" class="w3-button w3-indigo w3-border w3-round-large">Download</a></td>
      </tr>

    </table>

    <p><strong>ACCURATE</strong> Deluxe Edition</p>

    <table class="w3-table-all">
      <thead>
        <tr style="background-color:#e50000;color:#fff">
          <th>Version</th>
          <th>32 Bit</th>
          <th>64 Bit</th>
        </tr>
      </thead>
      <tr>
        <td><b>5.0.19.1863 ( Latest Version ) ⬆⬆ </b></td>
        <td><a href="http://deluxeaccounting.com/download/accurate/v5/deluxe/ACCURATE5%20Deluxe-5.0.19.1863-Setup-x86.exe" target="_blank" class="w3-button w3-white w3-border w3-round-large">Download</a></td>
        <td><a href="http://deluxeaccounting.com/download/accurate/v5/deluxe/ACCURATE5%20Deluxe-5.0.19.1863-Setup-x64.exe" target="_blank" class="w3-button w3-indigo w3-border w3-round-large">Download</a></td>
      </tr>
      <tr>
        <td>5.0.16.1807 ⬇⬇ </td>
        <td><a href="http://deluxeaccounting.com/download/previous/accurate/v5/deluxe/ACCURATE5%20Deluxe-5.0.16.1807-Setup-x86.exe" target="_blank" class="w3-button w3-white w3-border w3-round-large">Download</a></td>
        <td><a href="http://deluxeaccounting.com/download/previous/accurate/v5/deluxe/ACCURATE5%20Deluxe-5.0.16.1807-Setup-x64.exe" target="_blank" class="w3-button w3-indigo w3-border w3-round-large">Download</a></td>
      </tr>
      <tr>
        <td>5.0.16.1806</td>
        <td><a href="http://deluxeaccounting.com/download/previous/accurate/v5/deluxe/ACCURATE5%20Deluxe-5.0.16.1806-Setup-x86.exe" target="_blank" class="w3-button w3-white w3-border w3-round-large">Download</a></td>
        <td><a href="http://deluxeaccounting.com/download/previous/accurate/v5/deluxe/ACCURATE5%20Deluxe-5.0.16.1806-Setup-x64.exe" target="_blank" class="w3-button w3-indigo w3-border w3-round-large">Download</a></td>
      </tr>

    </table>

    <p><strong>ACCURATE</strong> Enterprise Edition</p>

    <table class="w3-table-all">
      <thead>
        <tr style="background-color:#cc0000;color:#fff">
          <th>Version</th>
          <th>32 Bit</th>
          <th>64 Bit</th>
        </tr>
      </thead>
      <tr>
        <td><b>5.0.19.1863 ( Latest Version ) ⬆⬆ </b></td>
        <td><a href="http://deluxeaccounting.com/download/accurate/v5/enterprise/ACCURATE5%20Enterprise-5.0.19.1863-Setup-x86.exe" target="_blank" class="w3-button w3-white w3-border w3-round-large">Download</a></td>
        <td><a href="http://deluxeaccounting.com/download/accurate/v5/enterprise/ACCURATE5%20Enterprise-5.0.19.1863-Setup-x64.exe" target="_blank" class="w3-button w3-indigo w3-border w3-round-large">Download</a></td>
      </tr>
      <tr>
        <td>5.0.16.1807 ⬇⬇ </td>
        <td><a href="http://deluxeaccounting.com/download/previous/accurate/v5/enterprise/ACCURATE5%20Enterprise-5.0.16.1807-Setup-x86.exe" target="_blank" class="w3-button w3-white w3-border w3-round-large">Download</a></td>
        <td><a href="http://deluxeaccounting.com/download/previous/accurate/v5/enterprise/ACCURATE5%20Enterprise-5.0.16.1807-Setup-x64.exe" target="_blank" class="w3-button w3-indigo w3-border w3-round-large">Download</a></td>
      </tr>
      <tr>
        <td>5.0.16.1806</td>
        <td><a href="http://deluxeaccounting.com/download/previous/accurate/v5/enterprise/ACCURATE5%20Enterprise-5.0.16.1806-Setup-x86.exe" target="_blank" class="w3-button w3-white w3-border w3-round-large">Download</a></td>
        <td><a href="http://deluxeaccounting.com/download/previous/accurate/v5/enterprise/ACCURATE5%20Enterprise-5.0.16.1806-Setup-x64.exe" target="_blank" class="w3-button w3-indigo w3-border w3-round-large">Download</a></td>
      </tr>

    </table>

    <h2>ACCURATE License Manager</h2>
    <p>Aplikasi untuk mengelola Lisensi Accurate Anda</p>
    <table class="w3-table-all">
      <thead>
        <tr style="background-color:#ff1919;color:#fff">
          <th>Version</th>
          <th>32/64 Bit</th>
        </tr>
      </thead>

      <tr>
        <td><b>1.0.0.306</b></td>
        <td><a href="http://deluxeaccounting.com/download/accurate/v5/licensemanager/ACCURATE%205%20License%20Manager-1.0.0.306-Setup.exe" target="_blank" class="w3-button w3-white w3-border w3-round-large">Download</a></td>
      </tr>


    </table>
<!--
    <h2>AOL Private Cloud</h2>
    <table class="w3-table-all">
      <thead>
        <tr style="background-color:#ff1919;color:#fff">
          <th>Version</th>
          <th>32 Bit</th>
          <th>64 Bit</th>
        </tr>
      </thead>

      <tr>
        <td><b>1.0.3721</b></td>
        <td><a href="http://deluxeaccounting.com/downloads/accurate/aol/aolprivate-1.0.3721-win-x86.exe" target="_blank" class="w3-button w3-white w3-border w3-round-large">Download</a></td>
        <td><a href="http://deluxeaccounting.com/downloads/accurate/aol/aolprivate-1.0.3721-win-x64.exe" target="_blank" class="w3-button w3-indigo w3-border w3-round-large">Download</a></td>
      </tr>
    </table>
    <h2>Accurate Desktop 4/5 to AOL Migration Tool</h2>
    <p>Data yang di-migrasi ke AOL hanya data master beserta saldo-nya dan data transaksi outstanding sales & purchase invoice, dan down payment</p>
    <table class="w3-table-all">
      <thead>
        <tr style="background-color:#ff1919;color:#fff">
          <th>32/64 Bit</th>
        </tr>
      </thead>
      <tr>
        <td><a href="http://deluxeaccounting.com/downloads/accurate/aol/migration-tool/Accurate%205%20To%20Online-1.0-Setup.exe" target="_blank" class="w3-button w3-white w3-border w3-round-large">Download</a></td>
      </tr>
    </table> -->

    <h2>Rene 2</h2>
    <table class="w3-table-all">
      <thead>
        <tr style="background-color:#ff8c19;color:#fff">
          <th>Installer</th>
          <th>32/64 Bit</th>
        </tr>
      </thead>
      <tr>
        <td><b>RENE 2 </b> Application Version 2.1.4.2440</td>
        <td><a href="http://www.deluxeaccounting.com/downloads/rene-v2/rene2setup.exe" target="_blank" class="w3-button w3-white w3-border w3-round-large">Download</a></td>
      </tr>
      <tr>
        <td><b>RENE 2 </b>Help</td>
        <td><a href="http://goo.gl/cmsTb" target="_blank" class="w3-button w3-white w3-border w3-round-large">Download</a></td>
      </tr>
      <tr>
        <td><b>RENE 2 </b>Information</td>
        <td><a href="http://goo.gl/L158w" target="_blank" class="w3-button w3-white w3-border w3-round-large">Download</a></td>
      </tr>
      <tr>
        <td>Accurate 5 Service for <b>RENE</b></td>
        <td><a href="http://deluxeaccounting.com/download/rene-v2/ACCURATE%20Service-5.0.17.1833%20Setup.exe" target="_blank" class="w3-button w3-white w3-border w3-round-large">Download</a></td>
      </tr>
    </table>

  </div>












    </div>







  </div>












<?php include_once 'modal-order.php'; ?>



</body>
</html>
