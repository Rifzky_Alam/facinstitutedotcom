<?php include_once '../baseurl.php'; ?>

<?php
$onsiteStandard=1000000;
$paketStandard=4700000;
$onsiteExpert=1300000;
$paketExpertSatu=12350000;
$paketExpertDua=23400000;
$accurateOnline=1250000;
$paketAccurateOnline=6000000;

?>
<html class="no-js" lang="id">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="google-site-verification" content="EfNVtXecxElbcbugY59SeKXJuMzO67bjVvTvlHCc_CM" />
    <meta name="msvalidate.01" content="D925497D8F382164FF0CE0267D381FDA" />
    <meta name="thumbnailUrl" content="https://www.fac-institute.com/administrasi/artikel/images/7cabb38e259c0d5b53f9bb219daf410f.jpg?w=650" />
    <meta name="description" content="FAC - Kami Melayani Training Software Akuntansi Accurate dan Accounting Service Untuk Perusahaan Anda" />
    <meta content="First Asian Consulting. Konsultan Akuntansi & Lembaga Pendidikan Komputerisasi Akuntansi" />
    <meta name="keywords" content="Training,Akuntansi,Accurate,Jasa">
    <meta name="author" content="Rifzky Alam, Dino Damara">
    <title>FAC Institute</title>
    <!-- <link rel="stylesheet" href="<?= getBaseUrl() ?>assets/homepage/css/foundation.min.css">
    <link href="<?php echo getBaseUrl() ?>assets/homepage/css/foundicons/foundation-icons.css" rel="stylesheet"> -->
    <!-- <link href='css/carousel.css' rel='stylesheet' type='text/css'> -->
    <link rel="stylesheet" href="<?php echo getBaseUrl() ?>assets/homepage/css/animate.min.css">
    <link href="https://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,100,300,500,600,700c" type="text/css" />
    <!-- <link href="<?php echo getBaseUrl() ?>assets/homepage/css/docs.css" rel="stylesheet" /> -->
    <!-- <link href='<?php echo getBaseUrl() ?>assets/homepage/css/style.css' rel='stylesheet' type='text/css'> -->
    <!-- <link rel="stylesheet" href="<?php echo getBaseUrl() ?>assets/homepage/owl-carousel/owl.carousel.css"> -->
    <!-- <link rel="stylesheet" href="<?php echo getBaseUrl() ?>assets/homepage/owl-carousel/owl.theme.css"> -->
    <!-- <link rel="stylesheet" href="<?php echo getBaseUrl() ?>assets/homepage/dist/photoswipe.css"> -->
    <!-- <link rel="stylesheet" href="<?php echo getBaseUrl() ?>assets/homepage/dist/default-skin/default-skin.css"> -->
    <!-- <script src="<?php echo getBaseUrl() ?>assets/homepage/dist/photoswipe.min.js"></script> -->
    <!-- <script src="<?= getBaseUrl() ?>assets/homepage/dist/photoswipe-ui-default.min.js"></script> -->
    <!-- <script src="<?php echo getBaseUrl() ?>assets/homepage/dist/photoswipe-ui-default.min.js"></script> -->

    <link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/w3.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/animate.min.css">
    <link href="https://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,100,300,500,600,700c" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/style.css">

    <style>
    body, html {
        height: 100%;
        font-family: "Raleway", sans-serif;
        font-size: 20px;
        line-height: 1.8;
    }
    * {
        box-sizing: border-box;
    }

    .columns {
        float: left;
        width: 33.3%;
        padding: 8px;
    }

    .price {
        list-style-type: none;
        border: 1px solid #eee;
        margin: 0;
        padding: 0;
        -webkit-transition: 0.3s;
        transition: 0.3s;
    }

    .price:hover {
        box-shadow: 0 8px 12px 0 rgba(0,0,0,0.2)
    }

    .price .header {
        background-color: #111;
        color: white;
        font-size: 25px;
    }

    .price li {
        border-bottom: 1px solid #eee;
        padding: 20px;
        text-align: center;
    }

    .price .grey {
        background-color: #eee;
        font-size: 20px;
    }

    .button {
        background-color: #4CAF50;
        border: none;
        color: white;
        padding: 10px 25px;
        text-align: center;
        text-decoration: none;
        font-size: 18px;
    }

    @media only screen and (max-width: 600px) {
        .columns {
            width: 100%;
        }
    }
    </style>


</head>

<body>
  <div id="back-top">
      <a style="font-weight: bold" href="#top">
          <img src="<?= getBaseUrl() ?>_caramel/assets/img/to-top@2x.png" />
      </a>
  </div>
  <h4>header</h4>
  <?php include_once '../top-nav.php'; ?>

  <!-- Header -->
  <header class="w3-display-container w3-content w3-center" style="max-width:1500px">
    <img class="w3-image" src="<?php echo getBaseUrl() ?>_caramel/assets/img/Untitled design_.jpg" alt="Me" width="1500" height="600">
    <div class="w3-display-middle w3-padding-large w3-border w3-wide w3-text-light-grey w3-center">
      <h1 class="w3-hide-medium w3-hide-small w3-xxxlarge">FAC</h1>
      <h5 class="w3-hide-large" style="white-space:nowrap">FAC</h5>
      <h3 class="w3-hide-medium w3-hide-small">INSTITUTE</h3>
    </div>
    <div class="w3-display-bottommiddle w3-container w3-text-white w3-padding-32 w3-hide-small">
   <p>Konsultan akuntansi & lembaga pendidikan komputerisasi akuntansi.</p>
  </div>
  </header>


  <div class="w3-content w3-padding-16 w3-margin-top" id="portfolio">
        <!-- About Container -->
  <div class="w3-container" id="about">
    <div class="w3-content" style="">
      <h5 class="w3-center w3-padding-16"><span class="w3-tag w3-wide">Training Accurate</span></h5>
<p class="w3-center" style="font-size:24px">“Kami mengucapkan terima kasih atas kepercayaan Anda memilih produk Accurate”</p>
<p>Bagi pelanggan Accurate yang membutuhkan training Accurate dapat mendaftarkan training yang diselenggarakan oleh FAC Institute.
  Training ini akan membahas mengenai setup awal program Accurate untuk memastikan program berjalan dengan baik sebelum implementasi transaksi perusahaan, dengan materi training sebagai berikut :</p>
  <ol>
  <li>Review Penggunaan Accurate/RENE</li>
  <li>Troubleshooting Accurate/RENE</li>
  <li>Setup Database Accurate/RENE</li>
  <li>Training Fitur Accurate/RENE</li>
</ol>

<p>Berikut ini adalah beragam layanan implementasi yang tersedia:</p>

<h2 class="w3-center w3-padding-16">STANDARD, DELUXE EDITION & RENE (STANDARD)</h2>

<div class="w3-row-padding" style="margin:0 -16px">
     <div class="w3-half w3-margin-bottom">
       <ul class="w3-ul w3-center w3-card-2 w3-hover-shadow">
         <li class="w3-dark-grey w3-xlarge w3-padding-32">Onsite standard</li>
         <li class="w3-padding-16">1 Orang Implementator</li>
         <li class="w3-padding-16">Max. 7 Jam Kerja</li>
         <li class="w3-padding-16">Transport Relatif</li>
         <li class="w3-padding-16">
           <h2>Rp 1,000,000 / Hari</h2>
         </li>
       </ul>
     </div>

     <div class="w3-half">
       <ul class="w3-ul w3-center w3-card-2 w3-hover-shadow">
         <li class="w3-black w3-xlarge w3-padding-32">Paket standard</li>
         <li class="w3-padding-16">1 Orang Implementator</li>
         <li class="w3-padding-16">Max. 7 Jam Kerja</li>
         <li class="w3-padding-16">Transport Relatif</li>
         <li class="w3-padding-16">
           <h2>Rp 4,700,000 / 5 Hari</h2>
         </li>
       </ul>
     </div>
   </div>


<h2 class="w3-center w3-padding-16">ENTERPRISE & DELUXE EDITION – CONTRACTOR (EXPERT)</h2>
<div class="w3-row-padding" style="margin:0 -16px">
  <div class="columns">
    <ul class="price">
      <li class="header">Onsite Expert</li>
      <li class="w3-padding-16">1 Orang Implementator</li>
      <li class="w3-padding-16">Max. 7 Jam Kerja</li>
      <li class="w3-padding-16">Transport Relatif</li>
      <li class="w3-padding-16">
        <h2>Rp 1,300,000 / Hari</h2>
      </li>
    </ul>
  </div>

  <div class="columns">
    <ul class="price">
      <li class="header" style="background-color:#4CAF50">Paket Expert 1</li>
      <li class="w3-padding-16">1 Orang Implementator</li>
      <li class="w3-padding-16">Max. 7 Jam Kerja</li>
      <li class="w3-padding-16">Transport Relatif</li>
      <li class="w3-padding-16">
        <h2>Rp 12,350,000 / 10 Hari</h2>
      </li>
    </ul>
  </div>

  <div class="columns">
    <ul class="price">
      <li class="header">Paket Expert 2</li>
      <li class="w3-padding-16">1 Orang Implementator</li>
      <li class="w3-padding-16">Max. 7 Jam Kerja</li>
      <li class="w3-padding-16">Transport Relatif</li>
      <li class="w3-padding-16">
        <h2>Rp 23,400,000 / 20 Hari</h2>
      </li>
    </ul>
  </div>
</div>


<h2 class="w3-center w3-padding-16">ACCURATE ONLINE (AOL)</h2>
<div class="w3-row-padding" style="margin:0 -16px">
     <div class="w3-half w3-margin-bottom">
       <ul class="w3-ul w3-center w3-card-2 w3-hover-shadow">
         <li class="w3-dark-grey w3-xlarge w3-padding-32">Paket Onsite Accurate Online 1</li>
         <li class="w3-padding-16">1 Orang Implementator</li>
         <li class="w3-padding-16">Max. 7 Jam Kerja</li>
         <li class="w3-padding-16">Transport Relatif</li>
         <li class="w3-padding-16">
           <h2>Rp 1,250,000 / Hari</h2>
         </li>
       </ul>
     </div>

     <div class="w3-half">
       <ul class="w3-ul w3-center w3-card-2 w3-hover-shadow">
         <li class="w3-black w3-xlarge w3-padding-32">Paket standard</li>
         <li class="w3-padding-16">1 Orang Implementator</li>
         <li class="w3-padding-16">Max. 7 Jam Kerja</li>
         <li class="w3-padding-16">Transport Relatif</li>
         <li class="w3-padding-16">
           <h2>Rp 6,000,000 / 5 Hari</h2>
         </li>
       </ul>
     </div>
   </div>

<div class="grey w3-center w3-padding-16 "><a href=<?php echo "'".getBaseUrl()."daftar-training/"."'"; ?> class="button w3-xlarge">Order Training</a></div>

<div class="w3-panel w3-padding-16 w3-leftbar w3-light-grey">
      <p class="w3-center"><i>"Kami memiliki metode pembelajaran yang dirancang khusus untuk peserta didik tanpa latar belakang akuntansi." </i></p>
      <p class="w3-center">FAC Institute</p>
</div>

<div class="w3-container" id="about">
  <div class="w3-row-padding w3-center">
    <div class="w3-half">
      <i class="fa fa-shopping-cart  w3-margin-bottom w3-jumbo w3-center"></i>
      <p class="w3-large">PESAN SEKARANG</p>
      <p><a class="w3-tag" style="text-decoration:none" href="https://fac-institute.com/daftar-training/" target="_blank">Lakukan pemesanan sekarang juga melalui order online, Klik Disini</a></p>
    </div>
    <div class="w3-half">
      <i class="fa fa-phone w3-margin-bottom w3-jumbo"></i>
      <p class="w3-large">BUTUH BANTUAN ?</p>
      <p>Hubungi kami sekarang untuk training Accurate
        <a class="w3-tag" style="text-decoration:none" href="tel://+6281290083983">+6281290083983</a>
      </p>
    </div>

  </div>
</div>


</div>
</div>
</div>
            <!-- <div class="off-canvas-content" data-off-canvas-content>
                <div id="sync1" class="owl-carousel">
                    <div class="item">
                        <img src="<?= getBaseUrl() ?>images/order/banner1.png" style="width:100%">
                        <p id="owlc1" class="owl-caption animated zoomInDown">
                            Accurate Standard Edition didesain untuk perusahaan perdagangan & jasa.
                        </p>
                    </div>
                    <div class="item">
                        <img src="<?= getBaseUrl() ?>images/order/banner2.png" style="width:100%">
                        <p class="owl-caption animated zoomInDown" style="color:#fff">
                            Accurate Deluxe Edition didesain untuk perusahaan kontraktor.
                        </p>
                    </div>
                    <div class="item">
                        <img src="<?= getBaseUrl() ?>images/order/banner3.png" style="width:100%">
                        <p class="owl-caption animated zoomInDown" style="color:#fff">
                            Accurate Enterprise Edition didesain untuk perusahaan manufaktur.
                        </p>
                    </div>
                    <div class="item">
                        <img src="<?= getBaseUrl() ?>images/order/banner4.png" style="width:100%">
                        <p id="owlc4" class="owl-caption animated zoomInDown">
                            Rene 2 POS didesain untuk usaha retail / pertokoan .
                        </p>
                    </div>

                </div>
                <div id="sync2">
                    <div class="item">
                        <h1>Accurate Standard Edition</h1>
                    </div>
                    <div class="item">
                        <h1>Accurate Deluxe Edition</h1>
                    </div>
                    <div class="item">
                        <h1>Accurate Enterprise</h1>
                    </div>
                    <div class="item">
                        <h1>Rene 2 Point of Sale</h1>
                    </div>
                </div>
    </div> -->



    <!-- <script src="<?= getBaseUrl() ?>assets/homepage/js/jquery-2.1.4.min.js"></script>
    <script src="<?= getBaseUrl() ?>assets/homepage/js/foundation.min.js"></script>
    <script src="<?php echo getBaseUrl() ?>assets/homepage/js/foundation.min.js"></script>
    <script type="text/javascript" src="<?php echo getBaseUrl() ?>assets/homepage/js/zcom.js"></script>
    <script src="<?php echo getBaseUrl() ?>assets/homepage/owl-carousel/owl.carousel.js"></script>
    <script src="<?php echo getBaseUrl() ?>assets/homepage/js/home.js"></script> -->



</body>
</html>
