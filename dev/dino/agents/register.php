<?php 
include_once 'variable.php';
?>

<!DOCTYPE html>
<html>
<head>
	<title>Registrasi Agents</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href=<?php echo "'".$baseUrl."css/bootstrap.css'"; ?>>
	<link rel="stylesheet" type="text/css" href=<?php echo "'".$baseUrl."css/agent-register.css'"; ?>>
</head>
<body>

<?php 

include_once '../model/Agents.php';

$agen = new ControlAgents();

if (isset($_POST['username'],$_POST['password'],$_POST['confPassword'],$_POST['telepon'],$_POST['email'])){
	$validation = true;
	if ($_POST['password']==$_POST['confPassword']){
		
		$dataInput = array(
							'username' => $_POST['username'], 
							'password' => $_POST['password'],
							'telepon'=> $_POST['telepon'],
							'email'=> $_POST['email'],
							'rekening'=>'-',
							'atasNama'=>'-'
							);
		$dataInput = json_encode($dataInput);
		$coba = json_decode($dataInput);

		if ($agen->insertData($coba)){
			echo "<script>";
			echo "alert('Registrasi Selesai, Silahkan Login Kembali');";
			echo "location.replace('".getBaseUrl()."agents/');";
			echo "</script>";
		}

	}else{
			echo "<script>";
			echo "alert('Password tidak sama!');";
			echo "</script>";
	}

}else{
	//echo "gagal";

}



?>


<div class="container">
    <h1 class="well">Form Registrasi</h1>
	<div class="col-lg-12 well">
	<div class="row">
				<form action="" method='post'>
					<div class="col-sm-12">
						<div class="row">
							<div class="col-sm-12 form-group">
								<label>Username</label>
								<?php if (isset($_POST['username'])&&!empty($_POST['username'])): ?>
								<input type="text" placeholder="Enter First Name Here.." class="form-control" name='username' value=<?php echo "'".$_POST['username']."'"; ?>>	
									<?php else: ?>
								<input type="text" placeholder="Enter First Name Here.." class="form-control" name='username'>
								<?php endif ?>
								
							</div>			
						<div class="col-sm-12 form-group">
							<label>Password</label>
							<input class='form-control' type='password' name='password'>
						</div>	
						<div class="col-sm-12 form-group">
							<label>Konfirmasi Password</label>
							<input class='form-control' type='password' name='confPassword'>
						</div>	

						<div class="col-sm-12 form-group">
							<label>No Telepon</label>
							<?php if (isset($_POST['telepon'])&&!empty($_POST['telepon'])): ?>
							<input class='form-control' type='text' name='telepon' placeholder='harap masukan nomor telepon yang aktif' value=<?php echo "'".$_POST['telepon']."'"; ?>>
								<?php else: ?>
							<input class='form-control' type='text' name='telepon' placeholder='harap masukan nomor telepon yang aktif'>
							<?php endif ?>
							
						</div>
				
					<div class="col-sm-12 form-group">
						<label>Email Address</label>
						<?php if (isset($_POST['email'])&&!empty($_POST['email'])): ?>
						<input type="email" placeholder="Enter Email Address Here.." class="form-control" name='email' value=<?php echo "'".$_POST['email']."'"; ?>>
							<?php else: ?>
						<input type="email" placeholder="Enter Email Address Here.." class="form-control" name='email'>	
						<?php endif ?>
						
					</div>	

					<div class='col-sm-12 form-inline'>
						<button class="btn btn-lg btn-info">Submit</button>
						<a class='btn btn-lg btn-warning' href=<?php echo "'".$baseUrl."agents'"; ?>>Kembali Ke Login</a>
					</div>
					
				</form> 					
					</div>

				
				</div>
	</div>
</div>


	<script type="text/javascript" src=<?php echo "'".$baseUrl."js/jquery.js'"; ?>></script>
	<script type="text/javascript" src=<?php echo "'".$baseUrl."js/bootstrap.min.js'"; ?>></script>
</body>
</html>