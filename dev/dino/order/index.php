<?php include_once '../baseurl.php'; ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0" />

<meta property="og:type" content="article" />

<meta property="og:site_name" content="FAC INSTITUTE" />
<meta property="og:title" content="Order Training FAC-Institute" />
<meta property="og:description" content="Order Training Accurate FAC-Institute. Kami ajari software accurate tanpa anda mengerti akuntansi" />

<meta property="og:url" content=<?php echo '"http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'"'; ?> />
<meta property="fb:app_id" content="454421948087465" />

<meta property="og:image" content="http://alix.sch.id/pendaftaran/images/logo-fac.jpg" />
<meta property="og:image:height" content="200" />
<meta property="og:image:width" content="200" />

<meta name="thumbnailUrl" content="http://alix.sch.id/pendaftaran/images/logo-fac.jpg" itemprop="thumbnailUrl" />
<meta name="description" content="Order Training Accurate FAC-Institute Kami ajari software accurate tanpa anda mengerti akuntansi" itemprop="description" />
<meta content="Order Training Accurate FAC-Institute. Kami ajari software accurate tanpa anda mengerti akuntansi" itemprop="headline" />
	<title>Order Training FAC Institute</title>
	<link rel="stylesheet" type="text/css" href="<?php echo getBaseUrl() ?>assets/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?php echo getBaseUrl() ?>assets/slidercarousel/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo getBaseUrl() ?>assets/pricingtables/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo getBaseUrl() ?>assets/quotes/style.css">
    <style type="text/css">
        #misi>li{
            list-style-type: none;
            line-height: 2em;
            font-size: 16px;
        }

        #misi>li:before{
            content: "\e013";
            font-family: 'Glyphicons Halflings';
            font-size: 12px;
            float: left;
            margin-right: 5px;
            margin-left: -17px;
            color: black;
        }

        .active{
            color: #000 !important;
        }

    </style>
</head>
<body>
<div class="container-fluid" style="padding-left:0px;padding-right:0px">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <img src="<?php echo getBaseUrl() ?>images/order/banner1.png" style="width:100%">
                <div class="carousel-caption">
                    <!--<h3 style="color:black;">Accurate Standard</h3>-->
                    <p style="color:black;text-shadow: 0 0 5px #000000;margin-bottom:-15px">
                        Accurate Standard Edition didesain untuk perusahaan perdagangan dan jasa.
                    </p>
                </div>
            </div>
            <!-- End Item -->
            <div class="item">
                <img src="<?php echo getBaseUrl() ?>images/order/banner2.png" style="width:100%">
                <div class="carousel-caption">
                    <!--<h3>Accurate Deluxe</h3>-->
                    <p style="text-shadow: 0 0 5px #404040;margin-bottom:-15px">
                        Accurate Deluxe Edition didesain untuk perusahaan kontraktor.
                    </p>
                </div>
            </div>
            <!-- End Item -->
            <div class="item">
                <img src="<?php echo getBaseUrl() ?>images/order/banner3.png" style="width:100%">
                <div class="carousel-caption">
                    <!--<h3>Accurate Enterprise</h3>-->
                    <p style="text-shadow: 0 0 5px #404040;margin-bottom:-15px">
                        Accurate Enterprise Edition didesain untuk perusahaan manufaktur.</p>
                </div>
            </div>
            <!-- End Item -->
            <div class="item">
                <img src="<?php echo getBaseUrl() ?>images/order/banner4.png" style="width:100%">
                <div class="carousel-caption">
                    <h3>RENE 2</h3>
                    <div class="row">
                        <div style="color:#ff0000">
                            Cara menggunakan RENE untuk jenis usaha berikut:
                        </div>
                        <div>
                            <ul style="color:#ff0000">
                                <li>Apotik/Toko Obat</li>
                                <li>Salon dan Bengkel</li>
                                <li>Retail Modern (Swalayan)</li>
                                <li>Resto dan Cafe</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Item -->
        </div>
        <!-- End Carousel Inner -->
        <ul class="nav nav-pills nav-justified">
            <li data-target="#myCarousel" data-slide-to="0">
            	<a href="#" style="background-color:khaki">Accurate Standard Edition</a>
            </li>
            <li data-target="#myCarousel" data-slide-to="1">
            	<a href="#" style="background-color:antiquewhite">Accurate Deluxe Edition</a>
            </li>
            <li data-target="#myCarousel" data-slide-to="2">
            	<a href="#" style="background-color:aliceblue">Accurate Enterprise</a>
            </li>
            <li data-target="#myCarousel" data-slide-to="3">
            	<a href="#" style="background-color:gainsboro">Rene 2</a>
            </li>
        </ul>
    </div>
    <!-- End Carousel -->
</div>

<div class="container" style="padding-top:30px">

    <div class="row">
        <div class="col-md-12">
            <h2 style="text-align:center;">STANDARD, DELUXE EDITION - NON KONTRAKTOR & (RENE STANDARD)</h2>
        </div>
    </div><br>

    <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding-left:78px;padding-right:78px;">
                
                    <!-- PRICE ITEM -->
                    <div class="panel price panel-red">
                        <div class="panel-heading  text-center">
                        <h3>Standard Onsite</h3>
                        </div>
                        <div class="panel-body text-center">
                            <p class="lead" style="font-size:40px"><strong>750,000 IDR / hari</strong></p>
                        </div>
                        <ul class="list-group list-group-flush text-center">
                            <li class="list-group-item"><i class="icon-ok text-danger"></i> 1 Implementator</li>
                            <li class="list-group-item"><i class="icon-ok text-danger"></i> 7 Jam kerja</li>
                            <li class="list-group-item"><i class="icon-ok text-danger"></i><small><b>Transport relatif</b></small></li>
                        </ul>
                        <div class="panel-footer">
                            <a class="btn btn-lg btn-block btn-danger" href="<?php echo getBaseUrl().'daftar-training' ?>">ORDER SEKARANG</a>
                        </div>
                    </div>
                    <!-- /PRICE ITEM -->
                    
                    
                </div>
    

    
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding-left:78px;padding-right:78px;">
                
                    <!-- PRICE ITEM -->
                    <div class="panel price panel-blue">
                        <div class="panel-heading arrow_box text-center">
                        <h3>Paket Standard</h3>
                        </div>
                        <div class="panel-body text-center">
                            <p class="lead" style="font-size:40px"><strong>3,500,000 / 5 Hari</strong></p>
                        </div>
                        <ul class="list-group list-group-flush text-center">
                            <li class="list-group-item"><i class="icon-ok text-info"></i> 1 Implementator</li>
                            <li class="list-group-item"><i class="icon-ok text-info"></i> 7 Jam kerja</li>
                            <li class="list-group-item"><i class="icon-ok text-danger"></i><small><b>Transport relatif</b></small></li>
                        </ul>
                        <div class="panel-footer">
                            <a class="btn btn-lg btn-block btn-info" href="<?php echo getBaseUrl().'daftar-training' ?>">ORDER SEKARANG</a>
                        </div>
                    </div>
                    <!-- /PRICE ITEM -->
                    
                    
                </div>
    </div><br>

    <div class="row">
        <div class="col-md-12">
            <h2 style="text-align:center;">ENTERPRISE, DELUXE EDITION - KONTRAKTOR (EXPERT)</h2>
        </div>
    </div><br>
    


    <div class="row">
                
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                
                    <!-- PRICE ITEM -->
                    <div class="panel price panel-green">
                        <div class="panel-heading arrow_box text-center">
                        <h3>Expert Onsite</h3>
                        </div>
                        <div class="panel-body text-center">
                            <p class="lead" style="font-size:40px"><strong>1,000,000 / Hari</strong></p>
                        </div>
                        <ul class="list-group list-group-flush text-center">
                            <li class="list-group-item"><i class="icon-ok text-success"></i> 1 Implementator</li>
                            <li class="list-group-item"><i class="icon-ok text-success"></i> 7 Jam kerja</li>
                            <li class="list-group-item"><i class="icon-ok text-success"></i> Transport relatif</li>
                        </ul>
                        <div class="panel-footer">
                            <a class="btn btn-lg btn-block btn-success" href="<?php echo getBaseUrl().'daftar-training' ?>">ORDER SEKARANG</a>
                        </div>
                    </div>
                    <!-- /PRICE ITEM -->
                    
                    
                </div>
    

    
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                
                    <!-- PRICE ITEM -->
                    <div class="panel price panel-grey">
                        <div class="panel-heading arrow_box text-center">
                        <h3>Paket Expert 1</h3>
                        </div>
                        <div class="panel-body text-center">
                            <p class="lead" style="font-size:40px"><small>9,500,000 / 10 Hari</small></p>
                        </div>
                        <ul class="list-group list-group-flush text-center">
                            <li class="list-group-item"><i class="icon-ok text-success"></i> 1 Implemetator</li>
                            <li class="list-group-item"><i class="icon-ok text-success"></i> 7 Jam kerja</li>
                            <li class="list-group-item"><i class="icon-ok text-success"></i> Transport relatif</li>
                        </ul>
                        <div class="panel-footer">
                            <a class="btn btn-lg btn-block btn-primary" href="<?php echo getBaseUrl().'daftar-training' ?>">ORDER SEKARANG</a>
                        </div>
                    </div>
                    <!-- /PRICE ITEM -->
                    
                    
                </div>
    
                
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                
                    <!-- PRICE ITEM -->
                    <div class="panel price panel-white">
                        <div class="panel-heading arrow_box text-center">
                        <h3>Paket Expert 2</h3>
                        </div>
                        <div class="panel-body text-center">
                            <p class="lead" style="font-size:40px"><small>18,000,000 / 20 Hari</small></p>
                        </div>
                        <ul class="list-group list-group-flush text-center">
                            <li class="list-group-item"><i class="icon-ok text-success"></i> 1 Implementator</li>
                            <li class="list-group-item"><i class="icon-ok text-success"></i> 7 Jam kerja</li>
                            <li class="list-group-item"><i class="icon-ok text-success"></i> Transport relatif</li>
                        </ul>
                        <div class="panel-footer">
                            <a class="btn btn-lg btn-block btn-default" href="<?php echo getBaseUrl().'daftar-training' ?>">ORDER SEKARANG</a>
                        </div>
                    </div>
                    <!-- /PRICE ITEM -->
                    
                    
                </div>
    </div>
                <br>

    <div class="row">
        <div class="col-md-12">
            <h2 style="text-align:center;">ACCURATE ONLINE (AOL)</h2>
        </div>
    </div><br>


    <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding-left:78px;padding-right:78px;">
                
                    <!-- PRICE ITEM -->
                    <div class="panel price panel-grey">
                        <div class="panel-heading arrow_box text-center">
                        <h3>Accurate Online 1</h3>
                        </div>
                        <div class="panel-body text-center">
                            <p class="lead" style="font-size:40px"><strong>1,250,000 / Hari</strong></p>
                        </div>
                        <ul class="list-group list-group-flush text-center">
                            <li class="list-group-item"><i class="icon-ok text-success"></i> 1 Implementator</li>
                            <li class="list-group-item"><i class="icon-ok text-success"></i> 7 Jam kerja</li>
                            <li class="list-group-item"><i class="icon-ok text-success"></i> Transport relatif</li>
                        </ul>
                        <div class="panel-footer">
                            <a class="btn btn-lg btn-block btn-default" href="<?php echo getBaseUrl().'daftar-training' ?>">ORDER SEKARANG</a>
                        </div>
                    </div>
                    <!-- /PRICE ITEM -->
                    
                    
                </div>
                    
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding-left:78px;padding-right:78px;">
                
                    <!-- PRICE ITEM -->
                    <div class="panel price panel-red">
                        <div class="panel-heading arrow_box text-center">
                        <h3>Accurate Online 2</h3>
                        </div>
                        <div class="panel-body text-center">
                            <p class="lead" style="font-size:40px"><strong>6,000,000 / 5 Hari</strong></p>
                        </div>
                        <ul class="list-group list-group-flush text-center">
                            <li class="list-group-item"><i class="icon-ok text-success"></i> 1 Implementator</li>
                            <li class="list-group-item"><i class="icon-ok text-success"></i> 7 Jam kerja</li>
                            <li class="list-group-item"><i class="icon-ok text-success"></i> Transport relatif</li>
                        </ul>
                        <div class="panel-footer">
                            <a class="btn btn-lg btn-block btn-default" href="<?php echo getBaseUrl().'daftar-training' ?>">ORDER SEKARANG</a>
                        </div>
                    </div>
                    <!-- /PRICE ITEM -->
                    
                    
                </div>
    </div>
                
                      
</div>

<br><br>

<div class="container" style="padding-top: 25px">
	<div class="col-md-5">
		<blockquote class="quote-box">
	      <p class="quotation-mark">
	        “
	      </p>
	      <p class="quote-text">
	        Kami memiliki metode pembelajaran ACCURATE tanpa Anda mahir akuntansi. 
	      </p>
	      <hr>
	      <div class="blog-post-actions">
	        <p class="blog-post-bottom pull-left">
	          Why Us?
	        </p>
	        <p class="blog-post-bottom pull-right">
	        </p>
	      </div>
	    </blockquote>
	</div>

	<div class="col-md-7">
		<h3>Cakupan Training</h3>
		<hr>
		<ul id="misi">
            <li style='text-align:left;margin-top:5px'>Training Fitur Accurate atau RENE</li>
            <li style='text-align:left;margin-top:5px'>Setup Database Accurate atau RENE</li>
            <li style='text-align:left;margin-top:5px'>Troubleshooting Accurate atau RENE</li>
            <li style='text-align:left;margin-top:5px'>Review Penggunaan Accurate atau RENE</li>
        </ul>
	</div>
</div>

<div class="container" style="padding-top:40px">
    <div class="row" id="content-node" >
        <div class="col-md-12" style="padding-left:0;">
            <div class="col-md-4" style="text-align:center;">
                <center>
                <a href=<?php echo "'".getBaseUrl()."daftar-training'"; ?>>
                <img class="img-responsive img-promo" src=<?php echo "'".getBaseUrl()."images/iconpromo/shop-now.png'";?> height="150" width="150"/>
                </a>
                </center>
                <h1 class="text-bold" style='color:purple'>Pesan Sekarang</h1>
                <p class="deskripsi-promo"><a target='_blank' href=<?php echo "'".getBaseUrl()."?page=daftar'"; ?>>Lakukan pemesanan sekarang juga melalui order online, Klik Disini</a> </p>
            </div>
            <div class="col-md-4" style="text-align:center;">
            <center>
                <a href="tel://+6281290083983">
                    <img class="img-responsive img-promo" src=<?php echo "'".getBaseUrl()."images/iconpromo/call-now.png'";?> height="150" width="150"/>
                </a>
            </center>
                <h1 class="text-bold" style='color:purple'>Hubungi Kami</h1>
                    <p class="deskripsi-promo">Hubungi kami sekarang untuk keluhan atau layanan lain dari tim kami secara langsung <br>
                        <a href="tel://+6281290083983">+6281290083983</a> / <a href="tel://+6282311944359">+6282311944359</a>
                    </p>
            </div>
            <div class="col-md-4" style="text-align:center;">
                <center>
                    <img class="img-responsive img-promo" src=<?php echo "'".getBaseUrl()."images/iconpromo/hel-now.png'";?> height="150" width="150"/>
                </center>
                <h1 class="text-bold" style='color:purple'>Butuh Bantuan ?</h1>
                    <p class="deskripsi-promo">Untuk info lebih lanjut silahkan kirim email ke:<br> <a href="mailto:admin@fac-institute.com">admin@fac-institute.com</a> / <a href="mailto:training@fac-institute.com">training@fac-institute.com</a> / <a href="mailto:training.facinstitute@gmail.com">training.facinstitute@gmail.com</a></p>
            </div>
        </div>
    </div>
</div>

<div class="container" style="padding-top: 25px;padding-bottom:40px">
    <h1 style="text-align:center;font-weight:bold;">Klien Kami</h1>
    <center>
    <div class="row" id="content-node" style="padding-top:30px;">
        <div class="col-md-12" style="padding-left:0;">
            <div class="col-md-4" style="text-align:center;">
            <a href="#"><img class="img-responsive img-promo-2 img-khusus-promo" src=<?php echo "'".getBaseUrl()."images/client/client-1.png'";?> data-toggle='tooltip' title='OPPO Smartphones' height="200" width="200"/></a>
            </div>
            <div class="col-md-4" style="text-align:center;">
                <a href="#"><img class="img-responsive img-promo-2" src=<?php echo "'".getBaseUrl()."images/client/client-2.png'";?> data-toggle='tooltip' title='PT Adaro Energy Indonesia' height="200" width="200"/></a>
            </div>
            <div class="col-md-4" style="text-align:center;">
                <a href="#"><img class="img-responsive img-promo-2" src=<?php echo "'".getBaseUrl()."images/client/client-3.png'";?> data-toggle='tooltip' title='Maya Food Sardines' height="200" width="200"/></a>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-4" style="text-align:center;">
            <a href="#"><img class="img-responsive img-promo-2" src=<?php echo "'".getBaseUrl()."images/client/client-4.png'";?> data-toggle='tooltip' title='UKM Center Mahasiswa Universitas Indonesia' height="200" width="200"/></a>
        </div>

        <div class="col-md-4" style="text-align:center;">
            <a href="#"><img class="img-responsive img-promo-2" src=<?php echo "'".getBaseUrl()."images/client/client-5.png'";?> data-toggle='tooltip' title='Bosowa Resources Group' height="200" width="200"/></a>
        </div>

        <div class="col-md-4" style="text-align:center;">
            <a href="#"><img class="img-responsive img-promo-2" src=<?php echo "'".getBaseUrl()."images/client/client-6.png'";?> data-toggle='tooltip' title='Arsari Group' height="200" width="200"/></a>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-4" style="text-align:center;">
            <a href="#"><img class="img-responsive img-promo-2" src=<?php echo "'".getBaseUrl()."images/client/client-7.png'";?> data-toggle='tooltip' title='Enkei Wheel Velg' height="200" width="200"/></a>
        </div>

        <div class="col-md-4" style="text-align:center;">
            <a href="#"><img class="img-responsive img-promo-2" src=<?php echo "'".getBaseUrl()."images/client/client-8.png'";?> data-toggle='tooltip' title='Kiddycuts' height="200" width="200"/></a>
        </div>

        <div class="col-md-4" style="text-align:center;">
            <a href="#"><img class="img-responsive img-promo-2" src=<?php echo "'".getBaseUrl()."images/client/client-9.png'";?> data-toggle='tooltip' title='Samudra Dyan Praga, Member of KOMPAS Group' height="200" width="200"/></a>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-4" style="text-align:center;">
            <a href="#"><img class="img-responsive img-promo-2" src=<?php echo "'".getBaseUrl()."images/client/ibik57.png'";?> data-toggle='tooltip' title='IBI-K57' height="200" width="200"/></a>
        </div>

        <div class="col-md-4" style="text-align:center;">
            <a href="#"><img class="img-responsive img-promo-2" src=<?php echo "'".getBaseUrl()."images/client/saungapung.png'";?> data-toggle='tooltip' title='Rumah Makan Saung Apung' height="200" width="200"/></a>
        </div>

        <div class="col-md-4" style="text-align:center;">
            <a href="#"><img class="img-responsive img-promo-2" src=<?php echo "'https://fac-institute.com/images/order/logo-REG5.png'";?> data-toggle='tooltip' title='Regenesis Medical Partner' height="200" width="200"/></a>
        </div>
    </div>

    </center>
</div>

	<script type="text/javascript" src="<?php echo getBaseUrl() ?>assets/bootstrap/js/jquery-2.1.1.min.js"></script>
	<script type="text/javascript" src="<?php echo getBaseUrl() ?>assets/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo getBaseUrl() ?>assets/slidercarousel/style.js"></script>
</body>
</html>