<?php include_once '../../../baseurl.php'; ?>
<html class="no-js" lang="id">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="google-site-verification" content="EfNVtXecxElbcbugY59SeKXJuMzO67bjVvTvlHCc_CM" />
    <meta name="msvalidate.01" content="D925497D8F382164FF0CE0267D381FDA" />
    <meta name="thumbnailUrl" content="https://www.fac-institute.com/administrasi/artikel/images/7cabb38e259c0d5b53f9bb219daf410f.jpg?w=650" />
    <meta name="description" content="FAC - Kami Melayani Training Software Akuntansi ACCURATE dan Accounting Service Untuk Perusahaan Anda" />
    <meta content="First Asian Consulting. Konsultan Akuntansi & Lembaga Pendidikan Komputerisasi Akuntansi" />
    <meta name="keywords" content="Training,Akuntansi,ACCURATE,Jasa">
    <meta name="author" content="Rifzky Alam, Dino Damara">
    <title>FAC Institute</title>
    <link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/w3.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/animate.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,100,300,500,600,700c" type="text/css" />
 <style>
 .w3-top{
   font-family: "Raleway", sans-serif;
 }
 body, html, h3 {
     font-family: 'PT Sans', sans-serif;
 }
 p,li{
   font-size: 16px;
 }

 </style>


<link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/style.css">


<script src="<?php echo getBaseUrl() ?>assets/homepage/js/jquery-2.1.4.min.js"></script>




</head>
<body>


<h4>header</h4>
<?php include_once '../../../top-nav.php'; ?>
<?php include_once '../../../modal-order.php'; ?>

<div class="w3-content w3-padding-64" style="max-width:800px">
  <form class="w3-container ">
    <h2>Daftar Kelas Kursus Akuntansi Keuangan</h2>
    <p>
    <label>Nama Lengkap  * untuk disertifikat</label>
    <input class="w3-input w3-border w3-round" type="text"></p>
    <p>
    <label>No Tlp</label>
    <input class="w3-input w3-border w3-round" type="text"></p>
    <p>
    <label>Email  </label>
    <input class="w3-input w3-border w3-round" type="text"></p>
    <label>Latar belakang pendidikan/pendidikan terakhir   </label>
    <input class="w3-input w3-border w3-round" type="text"></p>
    <label>Nama Perusahaan (jika sudah bekerja) </label>
    <input class="w3-input w3-border w3-round" type="text"></p>
    <label>Alamat Pribadi  </label>
    <input class="w3-input w3-border w3-round" type="text"></p>
    <label>Alamat Perusahaan   </label>
    <input class="w3-input w3-border w3-round" type="text"></p>
    <label>Jabatan    </label>
    <input class="w3-input w3-border w3-round" type="text"></p>

    <select class="w3-select" name="option">
        <option value="" disabled selected>Jenis pelatihan </option>
        <option value="1">a. Basic Class </option>
        <option value="2">b. Advance Class </option>
      </select>

      <p><button class="w3-btn w3-teal">Submit</button></p>
  </form>
</div>


</body>
</html>
