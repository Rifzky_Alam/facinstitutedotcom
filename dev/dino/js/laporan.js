var acc = document.getElementsByClassName("accordionz");
var i;

$(function() {

  $('#tanggal-laporan').datepicker({
    dateFormat: 'yy-mm-dd'
  });

  $('#tanggal-awal').datepicker({
    dateFormat: 'yy-mm-dd'
  });

  $('#tanggal-akhir').datepicker({
    dateFormat: 'yy-mm-dd'
  });


});

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        this.classList.toggle("active");
        this.nextElementSibling.classList.toggle("show");
  }
}

jQuery.fn.ForceNumericOnly =
function()
{
    return this.each(function()
    {
        $(this).keydown(function(e)
        {
            var key = e.charCode || e.keyCode || 0;
            // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
            // home, end, period, and numpad decimal
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
             // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
             // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
        
        });
    });
};

//$('#biaya-dinner').keypress(function(){
    //alert('okeh');
//});
$('#investasi').ForceNumericOnly();
$('#biaya-parkir').ForceNumericOnly();
$('#biaya-lunch').ForceNumericOnly();
$('#biaya-dinner').ForceNumericOnly();
$('#biaya-allowance').ForceNumericOnly();
$('#biaya-kesehatan').ForceNumericOnly();
$('#biaya-lain').ForceNumericOnly();
$('#biaya-transportasi').ForceNumericOnly();

$('#rdStatusSendiri').click(function(){
    $('input[name=rekan]').each(function(){
        $(this).removeAttr("checked");
        $(this).attr("disabled", true);
    });
});

$('#rdStatusTidakSendiri').click(function(){
    $('input[name=rekan]').each(function(){
        $(this).removeAttr("disabled");
    });
});

$('#btnSubmit').click(function(){
    var tanggal =  $('#tanggal-lapor').val();
    var aidi = $('#aidi').val();
    var peserta = $('#peserta-kegiatan').val();
    var keteranganKegiatan=$('#keteranganKegiatanz').val();
    var statuskerja = $('input[name=statusKerja]:checked').val();
    var peran = $('input[name=peran]:checked').val();
    var rekanKerja = $("input[name=rekan]:checked").map(function () {return this.value;}).get().join(" # ");
    if ($('#kegiatanzLainnya').val()=='') {
    var kegiatan = $("#kegiatan option:selected").map(function () {return this.value;}).get().join(" # ");
    } else{
    var kegiatan = $("#kegiatan option:selected").map(function () {return this.value;}).get().join(" # ") + '-'+ $('#kegiatanzLainnya').val();        
    };


    var jenisTransportasi = $('input[name=jns-transportasi]:checked').val();
    var totalBiayaTransport= $('#biaya-transportasi').val();
    var ketBiayaTransport= $('#ket-biayaTransport').val();
    var biayaParkir= $('#biaya-parkir').val();
    var biayaMakanSiang= $('#biaya-lunch').val();
    var biayaMakanMalam= $('#biaya-dinner').val();
    var biayaAllowance= $('#biaya-allowance').val();
    var biayaKesehatan= $('#biaya-kesehatan').val();
    var biayaLain= $('#biaya-lain').val();
    var ketBiayaLain= $('#ket-biayaLain').val();
    //alert(user);

    if(tanggal==''){
        alert('Tanggal Harap diisi');
        return;
    }else if (statuskerja===undefined || statuskerja==null){
        alert('status kerja harus di isi');
        return;
    }else if (peran===undefined || peran==null){
        alert('peran kerja harus di isi');
        return;
    }else if (kegiatan===undefined || kegiatan==''){
        alert('Jenis kegiatan harus di isi');
        return;
    }else if(keteranganKegiatan==''){
        alert('keterangan kegiatan harus di isi');
        return;
    }else if(jenisTransportasi==''){
        alert('Anda ke klien jalan kaki? luar biasa!!, silahkan isi jenis transportasi terlebih dahulu');
        return;
    };
    //alert(kegiatan);
    $("#btnSubmit").attr("disabled", true);
    $("#btnSubmit").html('Harap Tunggu ...');
    var pengguna = $('#pemakai').html();
    var nilaiIp = $('#aipi').html();
    //alert('pengguna: ' + pengguna);
    //alert('ip: ' + nilaiIp);
    //alert('ketKegiatan: ' +keteranganKegiatan);
    //alert('statuskerja: ' +statuskerja);
    //alert('rekanKerja: ' +rekanKerja);
    //alert('jenisTransportasi: ' +jenisTransportasi);
    //alert('totalBiayaTransport: ' +totalBiayaTransport);
    //alert('ket-biayaTransport: ' +ketBiayaTransport);
    //alert('biaya-parkir: ' +biayaParkir);
    //alert('biayaMakanSiang: ' +biayaMakanSiang);
    //alert('biayaMakanMalam: ' +biayaMakanMalam);
    //alert('biayaAllowance: ' +biayaAllowance);
    //alert('biayaKesehatan: ' +biayaKesehatan);
    //alert('biayaLain: ' +biayaLain);
    //alert('kegiatan: ' +kegiatan);
    
    var jsonDatas = {
    'peserta':peserta,
    'username': pengguna,
    'ip':nilaiIp,
    'keteranganKegiatan':keteranganKegiatan,
    'statusKerja':statuskerja,
    'peran':peran,
    'rekan':rekanKerja,
    'myKegiatan':kegiatan,
    'jenisTransportasi':jenisTransportasi,
    'biayaTransportasi':totalBiayaTransport,
    'keteranganTransportasi':ketBiayaTransport,
    'biayaParkir': biayaParkir,
    'biayaMakanSiang':biayaMakanSiang,
    'biayaMakanMalam':biayaMakanMalam,
    'biayaAllowance':biayaAllowance,
    'biayaKesehatan':biayaKesehatan,
    'biayaLain':biayaLain,
    'aidi':aidi,
    'keteranganBiayaLain':ketBiayaLain
    }
//alert(peserta);

$.ajax({
  type: "POST",
  url: "reverse",
  data: {
    'jsonData':jsonDatas
    },
  cache: false,
  success: function(data){
     alert(data);
     window.location.replace("index");
  }
}); //end ajax

});