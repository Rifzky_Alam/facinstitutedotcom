<?php 
session_start();
if (!isset($_SESSION['admin'])||empty($_SESSION['admin']['nama'])||($_SESSION['admin']['tipe']!='admin'&&$_SESSION['admin']['tipe']!='kursus')) {
    header('Location:../sessions/index.php');
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>FAC-Institute -- Freelance</title>
    <?php $page='inputJadwal' ?>
    <?php include_once 'view-support/header.php'; ?>
    <?php MetaTag($page) ?>
    <?php Links($page) ?>
    <?php Scripts($page) ?>
    <?php Styles($page) ?>
    
</head>
<body>
<?php 
include_once '../model/Kursus.php';
$kursus = new Kursus();
$nama_kursus = json_decode($kursus->getDatas('nama_kursus'));
$tempat_kursus =  json_decode($kursus->getDatas('tempat_kursus'));
?>

<?php include_once 'view-support/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <?php include_once 'view-support/sidebar.php'; ?>
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1>FAC-Institute ::  Input Jadwal Kursus</h1>
            </div>

            <div class="row">
                <?php 
                if (isset($_POST['in'])) {
                    $time = strtotime($_POST['in']['jamAwal']);
                    $time = date('h:i',$time);

                    $time2 = strtotime($_POST['in']['jamAkhir']);
                    $time2 = date('h:i',$time2);
                    // echo str_replace(',', '', $_POST['in']['investasi']);
                    $cipherz = md5(implode(" # ",$_POST['in']['tanggalPelaksanaan']));
                    $chiper = md5($_POST['in']['duration']);
                    $myID = substr($_POST['in']['namaKelas'], 0,8).$_POST['in']['lokasiKelas'].substr($cipherz, 0,8).substr($chiper,0,8);
                    $input = array(
                    'id' => $myID,
                    'nama_kelas'=>$_POST['in']['namaKelas'],
                    'lokasi'=> $_POST['in']['lokasiKelas'],
                    'investasi'=>str_replace(',', '', $_POST['in']['investasi']),
                    'hari'=>$_POST['in']['hariPelaksanaan'],
                    'tanggal'=>implode(" # ",$_POST['in']['tanggalPelaksanaan']),
                    'jam'=>$time.' - '.$time2,
                    'durasi'=> $_POST['in']['duration']
                );

                $input = (object) $input;

                /*echo $input->id."<br>";
                echo $input->nama_kelas."<br>";
                echo $input->lokasi."<br>";
                echo $input->investasi."<br>";
                echo $input->hari."<br>";
                echo $input->tanggal."<br>";
                echo $input->jam."<br>";
                echo $input->durasi."<br>";
                */
                echo $kursus->inputJadwal($input);

                }

                ?>
            </div>

            <form action="" method="post">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label><a href="input-kelas">Nama Kelas</a></label>
                        <select class="form-control" name="in[namaKelas]">
                            <option value="">--Pilih nama kelas yang terdaftar--</option>
                            <?php 
                            for ($i=0; $i < count($nama_kursus) ; $i++) { 
                                echo "<option value='".$nama_kursus[$i]->id."'>";
                                echo $nama_kursus[$i]->nama_kursus;
                                echo "</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label><a href="input-lokasi">Lokasi Kelas</a></label>
                        <select class="form-control" name="in[lokasiKelas]">
                            <option value="">--Pilih lokasi kelas yang terdaftar--</option>
                            <?php 
                            for ($i=0; $i < count($tempat_kursus) ; $i++){ 
                                echo "<option value='".$tempat_kursus[$i]->id."'>";
                                echo $tempat_kursus[$i]->lokasi_kursus;
                                echo "</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Investasi</label>
                        <input type="text" name="in[investasi]" id="txtInvestasi" class="form-control" placeholder="Biaya Kursus">
                    </div>

                    <div class="form-group">
                        <label>Tanggal Kursus</label>
                        <div class="form-inline" id="jajal">                        
                            <input type="text" id="tgl-pelaksanaan" name='in[tanggalPelaksanaan][]' class="form-control tglPelaksanaan" placeholder="klik disini untuk memilih tanggal"/>   
                            <span class='btn btn-sm btn-primary' id='tambahTombol'>Tambah Tanggal</span>
                        </div>
                        <!--
                        <div class="form-inline">                        
                            <input type="text" id="tgl-pelaksanaan" name='in[tanggalPelaksanaan][]' class="form-control tglPelaksanaan" placeholder="klik disini untuk memilih tanggal"/>   
                            <span class='btn btn-sm btn-primary' id='tambahTombol'>Tambah Tanggal</span>
                        </div>-->
                    </div>

                    <div class="form-group">
                        <label>Hari Pelaksanaan</label>
                        <input type="text" name="in[hariPelaksanaan]" class="form-control" placeholder="Misal: Hari Senin, Rabu dan Jumat">
                    </div>

                    <div class="form-group">
                        <label>Jam Awal</label>
                        <input type="text" name="in[jamAwal]" id="timepicker1" class="form-control">
                    </div>

                    <div class="form-group">
                        <label>Jam Akhir</label>
                        <input type="text" name="in[jamAkhir]" id="timepicker2" class="form-control">
                    </div>

                    <div class="form-group">
                        <label>Durasi kelas dalam sebulan</label>
                        <input type="text" name="in[duration]" class="form-control" placeholder="e.g: 3 hari, 2 minggu atau 6 minggu">
                    </div>
                    <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>                   
                </div>    
            </div>
            </form>

        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo getBaseUrl() ?>assets/jqnumber/jquery.number.js"></script>
<script type="text/javascript" src="<?php echo getBaseUrl() ?>assets/timepicker/js/timepicki.js"></script>
<script>
    $('#timepicker1').timepicki();
    $('#timepicker2').timepicki();
    $('#txtInvestasi').number( true, 0 );
</script>
<?php include_once 'view-support/date-script.php'; ?>
<?php DateScript($page) ?>

</body>
</html>