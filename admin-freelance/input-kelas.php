<html>
<head>
    <title>FAC-Institute -- Freelance</title>
    <?php $page='inputKelas' ?>
    <?php include_once 'view-support/header.php'; ?>
    <?php MetaTag($page) ?>
    <?php Links($page) ?>
    <?php Scripts($page) ?>
    <?php Styles($page) ?>
    
</head>
<body>
<?php 
include_once '../model/Kursus.php';
$kursus = new Kursus();
$nama_kursus = json_decode($kursus->getDatas('nama_kursus'));
?>

<?php include_once 'view-support/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <?php include_once 'view-support/sidebar.php'; ?>
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1>FAC-Institute ::  Input Jenis Kelas Baru</h1>
            </div>

            <div class="row">
                <?php 
                if (isset($_POST['in'])) {
                    //$time = strtotime($_POST['in']['jamPelaksanaan']);
                    echo $kursus->addNamaKursus((object)$_POST['in']);
                    echo "<script>location.replace('".basename(__FILE__, '.php')."');</script>";
                }

                ?>
            </div>

            <?php 
                if (isset($_GET['app'])&&$_GET['app']=='delete'&&isset($_GET['id'])&&!empty($_GET['id'])) {
                    
                }
            ?>

            <form action="" method="post">
            <div class="row">
                <div class="col-md-12">
                    
                    <div class="form-group">
                        <label>Nama Kelas</label>
                        <input type="text" name="in[namaKursus]" class="form-control" placeholder="Nama Kursus">
                    </div>
                    
                    <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>                   
                </div>    
            </div>
            </form>

            <div class="row">
                <div class="col-md-12">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Nama Kursus</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            for ($i=0; $i < count($nama_kursus) ; $i++) { 
                                echo "<tr>";
                                echo "<td>".$nama_kursus[$i]->nama_kursus."</td>";
                                echo "<td><a href='delete-data?ac=nk&id=".$nama_kursus[$i]->id."' class='btn btn-danger'>Hapus</a></td>";
                                echo "</tr>";
                            }

                            ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>

</body>
</html>