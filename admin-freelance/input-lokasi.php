<?php 
session_start();
if (!isset($_SESSION['admin'])||empty($_SESSION['admin']['nama'])||($_SESSION['admin']['tipe']!='admin'&&$_SESSION['admin']['tipe']!='kursus')) {
    header('Location:../sessions/index.php');
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>FAC-Institute -- Admin Kursus</title>
    <?php $page='inputLokasi' ?>
    <?php include_once 'view-support/header.php'; ?>
    <?php MetaTag($page) ?>
    <?php Links($page) ?>
    <?php Scripts($page) ?>
    <?php Styles($page) ?>
    
</head>
<body>
<?php include_once 'view-support/map-script.php'; ?>
<?php 
include_once '../model/Kursus.php';
$kursus = new Kursus();
$tempat_kursus =  json_decode($kursus->getDatas('tempat_kursus'));
?>

<?php include_once 'view-support/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <?php include_once 'view-support/sidebar.php'; ?>
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1>FAC-Institute ::  Input Jenis Kelas Baru</h1>
            </div>

            <div class="row">
                <?php 
                if (isset($_POST['in'])) {
                    //$time = strtotime($_POST['in']['jamPelaksanaan']);
                    echo $kursus->addLokasiKursus((object)$_POST['in']);
                    echo "<script>location.replace('".basename(__FILE__, '.php')."');</script>";
                }

                ?>
            </div>

            <?php 
                if (isset($_GET['app'])&&$_GET['app']=='delete'&&isset($_GET['id'])&&!empty($_GET['id'])) {
                    
                }
            ?>

            <form action="" method="post">
            <div class="row">
                <div class="col-md-12">
                    
                    <div class="form-group">
                        <label>Lokasi Kelas</label>
                        <input type="text" name="in[lokasi]" class="form-control" placeholder="Lokasi Kursus" required>
                    </div>

                    <div class="form-group">
                        <label for="alamat">Lokasi Map</label>
                        <input type="text" name="in[map]" id="alamatz" class="form-control" placeholder="Cari Nama Tempat atau Tandai Peta di Bawah ini" required>                    
                    </div>

                    <div class="form-group">
                        <div id="map-canvas" style="width:100%;height:400px;border:1px solid"></div>
                    </div>
                    
                    <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>                   
                </div>    
            </div>
            </form>

            <div class="row">
                <div class="col-md-12">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Lokasi Kursus</th>
                                <th>Map</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            for ($i=0; $i < count($tempat_kursus) ; $i++) { 
                                echo "<tr>";
                                echo "<td>".$tempat_kursus[$i]->lokasi_kursus."</td>";
                                echo "<td><a href='https://www.google.com/maps/place/".$tempat_kursus[$i]->map."'>".$tempat_kursus[$i]->map."</a></td>";
                                echo "<td><a href='delete-data?ac=lk&id=".$tempat_kursus[$i]->id."' class='btn btn-danger'>Hapus</a></td>";
                                echo "</tr>";
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>

</body>
</html>