<?php 
session_start();
if (!isset($_SESSION['admin'])||empty($_SESSION['admin']['nama'])||($_SESSION['admin']['tipe']!='admin'&&$_SESSION['admin']['tipe']!='kursus')) {
    header('Location:../sessions/index.php');
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>FAC-Institute -- Freelance</title>
    <?php include_once 'view-support/header.php'; ?>
    <?php MetaTag('Home') ?>
    <?php Links('Home') ?>
    <?php Scripts('Home') ?>
    <?php Styles('Home') ?>
    
</head>
<body>
<?php 
include_once '../model/Kursus.php';
$kursus = new Kursus();

$jadwal = json_decode($kursus->getJadwal(date('m'),date('Y')));

if (date('m')==12){
    // echo "okeeeeeeeeee";
    $thn = intval(date('Y')) + 1;
    $bln = 1;
    $jadwalBulanDepan = json_decode($kursus->getJadwal(intval($bln),intval($thn)));
    // print_r($jadwalBulanDepan);
}else{
    $bln = intval(date('m'))+1;
    $jadwalBulanDepan = json_decode($kursus->getJadwal($bln,date('Y')));
}
$sekarang = strtotime(date('Y-m-d'));
?>

<?php include_once 'view-support/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px">
    <div class="row">
        <!--sidebar is here !!!!-->
        <?php include_once 'view-support/sidebar.php'; ?>
        <!--sidebar ends here !!!!-->
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1>FAC-Institute ::  Kursus</h1>
            </div>
            <div class="row">
            	<div class="col-md-6">
            		<div class="panel panel-warning">
      					<div class="panel-heading">Data Belum terlapor</div>
      					<div class="panel-body">10</div>
    				</div>            	
    			</div>
            	<div class="col-md-6">
            		<div class="panel panel-default">
                        <div class="panel-heading">Panel Heading</div>
                        <div class="panel-body">Panel Content</div>
                    </div>
            	</div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <h3>Jadwal Kursus Bulan <?php echo $kursus->getNamaBulan(date('m')); ?></h3>    
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Lokasi</th>
                                <th>Nama Kelas</th>
                                <th>Hari Kelas</th>
                                <th>Mulai Kelas</th>
                                <th colspan="2">Durasi Kelas</th>
                                <th>Investasi</th>
                                <th>Aksi</th>                            
                            </tr>
                        </thead>
                        <tbody>
                        <?php for ($i=0; $i < count($jadwal); $i++) { ?>
                            <tr>
                                <td>
                                <a href="https://www.google.co.id/maps/search/<?php echo $jadwal[$i]->map; ?>" target="_blank">
                                    <?php echo $jadwal[$i]->lokasi_kursus; ?>
                                </a>
                            </td>
                            <td><?php echo $jadwal[$i]->nama_kursus; ?></td>
                            <td><?php echo $jadwal[$i]->hari ?></td>
                            <?php $myDate = explode('-', $jadwal[$i]->mulai_kursus); ?>
                                <td><?php echo $myDate[2].'-'.$myDate[1].'-'.$myDate[0] ?></td>
                                <td><?php echo $jadwal[$i]->hari ?></</td>
                                <td><?php echo $jadwal[$i]->durasi ?></td>
                                <td>Rp <?php echo number_format($jadwal[$i]->investasi) ?>.-</td>
                                <td>
                                    <a href="delete-data?ac=djk&id=<?php echo $jadwal[$i]->id ?>">
                                        <i class="glyphicon glyphicon-remove"></i>    
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>    
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <h3>Jadwal Kursus Bulan <?php echo $kursus->getNamaBulan(date('m')+1); ?></h3>    
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Lokasi</th>
                                <th>Nama Kelas</th>
                                <th>Hari Kelas</th>
                                <th>Mulai Kelas</th>
                                <th colspan="2">Durasi Kelas</th>
                                <th>Investasi</th>
                                <th>Aksi</th>                            
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (count($jadwalBulanDepan)==0): ?>
                                <tr><td style="text-align:center;" colspan="7">Tidak ada jadwal untuk bulan ini</td></tr>
                            <?php endif ?>
                            <?php for ($i=0; $i < count($jadwalBulanDepan); $i++) { ?>
                            <tr>
                                <td>
                                <a href="https://www.google.co.id/maps/search/<?php echo $jadwalBulanDepan[$i]->map; ?>" target="_blank">
                                    <?php echo $jadwalBulanDepan[$i]->lokasi_kursus; ?>
                                </a>
                            </td>
                            <td><?php echo $jadwalBulanDepan[$i]->nama_kursus; ?></td>
                            <td><?php echo $jadwalBulanDepan[$i]->hari ?></td>
                            <?php $myDate = explode('-', $jadwalBulanDepan[$i]->mulai_kursus); ?>
                                <td><?php echo $myDate[2].'-'.$myDate[1].'-'.$myDate[0] ?></td>
                                <td><?php echo $jadwalBulanDepan[$i]->hari ?></</td>
                                <td><?php echo $jadwalBulanDepan[$i]->durasi ?></td>
                                <td>Rp <?php echo number_format($jadwalBulanDepan[$i]->investasi) ?>.-</td>
                                <td>
                                <a href="delete-data?ac=djk&id=<?php echo $jadwalBulanDepan[$i]->id ?>">
                                    <i class="glyphicon glyphicon-remove"></i>
                                </a>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>    
            </div>


        </div>
    </div>
</div>

</body>
</html>