<?php 
session_start();
if (!isset($_SESSION['admin'])||empty($_SESSION['admin']['nama'])||($_SESSION['admin']['tipe']!='admin'&&$_SESSION['admin']['tipe']!='kursus')) {
    header('Location:../sessions/index.php');
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>FAC-Institute -- Freelance</title>
    <?php $page='displayData' ?>
    <?php include_once 'view-support/header.php'; ?>
    <?php MetaTag($page) ?>
    <?php Links($page) ?>
    <?php Scripts($page) ?>
    <?php Styles($page) ?>
    
</head>
<body>
<?php 
include_once '../model/Kursus.php';
$kursus = new Kursus();

?>

<?php include_once 'view-support/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <?php include_once 'view-support/sidebar.php'; ?>
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h3>Data Kursus</h3>
            </div>

            <div class="row">
                <?php 
                if (isset($_GET['ac'])&&$_GET['ac']=='ds') {
                    $data = json_decode($kursus->getDataAllRegister());
                }
                ?>
            </div>

            <?php 
                if (isset($_GET['ac'])&&$_GET['ac']=='nk'&&isset($_GET['id'])&&!empty($_GET['id'])) {
                    // $dataDelete = $kursus->getDataKursusByID($_GET['id']);
                }elseif (isset($_GET['ac'])&&$_GET['ac']=='lk'&&isset($_GET['id'])&&!empty($_GET['id'])) {
                    // $dataDelete = $kursus->getLokasiKursusByID($_GET['id']);
                }
            ?>

            <form action="" method="post">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-hover">
                        <thead>
                                <?php if (isset($_GET['ac'])&&$_GET['ac']=='ds'): ?>
                                    <th>Nama Siswa</th>
                                    <th>Tanggal Daftar</th>
                                    <th>Email</th>
                                    <th>Telepon</th>
                                    <th>Nama Kursus</th>
                                <?php endif ?>
                        </thead>
                        <tbody>
                            <?php if (isset($_GET['ac'])&&$_GET['ac']=='ds'): ?>
                            <?php for ($i=0; $i < count($data); $i++) { ?>
                                <?php if ($data[$i]->status!='1'): ?>
                                    <tr class="warning">
                                    <?php else: ?>
                                    <tr class="success">
                                <?php endif ?>
                        
                                        <td><?php echo $data[$i]->nama ?></td>
                                        <td><?php echo $data[$i]->tanggal_daftar ?></td>
                                        <td><?php echo $data[$i]->email ?></td>
                                        <td><?php echo $data[$i]->telepon_user ?></td>
                                        <td><?php echo $data[$i]->nama_kursus ?></td>
                                    </tr>                            
                            <?php } ?>
                        <?php endif ?>

                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>

</body>
</html>