<?php
$data = array(
        'base_url' => base_url(),
				'judul' => 'DownloadAccurate'
    );
    $data = (object) $data;
?>

<?php
	include_once '../model/Artikel.php';
	$ctrlArtikel = new Artikel();
  if (isset($_GET['page'])&&!empty($_GET['page'])) {
    $datas = json_decode($ctrlArtikel->fetchAll($_GET['page']));
  } else {
    $datas = json_decode($ctrlArtikel->fetchAll('0'));
  }


	$dataSidebar = json_decode($ctrlArtikel->selectJudulAndID());
	if (isset($_GET['app'])&&$_GET['app']=='1'&&isset($_GET['id'])&&!empty($_GET['id'])) {
		$dataArtikel=$ctrlArtikel->selectById($_GET['id']);
	}
 ?>

<?php
function base_url(){
  $currentPath = $_SERVER['PHP_SELF'];$pathInfo = pathinfo($currentPath);$hostName = $_SERVER['HTTP_HOST'];$protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https://'?'https://':'http://';
  return 'https://'.$hostName."/";
	// return 'http://localhost/facftp/';
}


?>






<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
	<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0" />
<meta property="og:type" content="article" />
<?php if (isset($_GET['app'])&&$_GET['app']=='1'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>
<meta property="og:title" content=<?php echo "'$dataArtikel->judul'" ?> />
<meta property="og:description" content=<?php echo "'$dataArtikel->judul'" ?> />
<meta property="og:image" content=<?php echo "'".base_url()."administrasi/artikel/images/".$dataArtikel->image_url."'";?> />
<meta name="description" content=<?php echo "'$dataArtikel->judul'" ?> itemprop="description" />
<meta content=<?php echo "'$dataArtikel->judul'" ?> itemprop="headline" />
<meta name="author" content=<?php echo "'$dataArtikel->author'"; ?>>
<meta name="robots" content="index, follow" />
<title><?php echo $dataArtikel->judul ?></title>
<?php else: ?>
<title>Reading Page</title>
<meta property="og:site_name" content="FAC INSTITUTE" />
<meta property="og:url" content=<?php echo '"http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'"'; ?> />
<meta property="fb:app_id" content="454421948087465" />
<meta property="og:image" content="http://fac-institute.com/images/logo-fac_header.png" />
<meta property="og:image:height" content="200" />
<meta property="og:image:width" content="200" />
<meta name="thumbnailUrl" content="http://alix.sch.id/pendaftaran/images/logo-fac.jpg" itemprop="thumbnailUrl" />
<?php endif ?>
<meta charset="utf-8">
<?php include_once '../links.php'; ?>
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
    <link rel="shortcut icon" href='https://fac-institute.com/_caramel/assets/img/g25992_9_kUd_icon.ico'>
		<link rel="stylesheet" href="https://fac-institute.com/assets/homepage2019/css/linearicons.css">
		<link rel="stylesheet" href="https://fac-institute.com/assets/homepage2019/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://fac-institute.com/assets/homepage2019/css/bootstrap.css">
		<link rel="stylesheet" href="https://fac-institute.com/assets/homepage2019/css/magnific-popup.css">
		<link rel="stylesheet" href="https://fac-institute.com/assets/homepage2019/css/nice-select.css">
		<link rel="stylesheet" href="https://fac-institute.com/assets/homepage2019/css/animate.min.css">
		<link rel="stylesheet" href="https://fac-institute.com/assets/homepage2019/css/owl.carousel.css">
		<link rel="stylesheet" href="https://fac-institute.com/assets/homepage2019/css/main.css">
		<style media="screen">
			.banner-content h1 {
			font-size: 40px;
			}
			.nav-menu {
			padding-top: 15px;
			}
			.nav-menu a {
			padding: 1px 8px 1px 8px;
			font-size: 16px;
			font-weight: 500;
			}
			.nav-menu ul li a {
			font-size: 14px;
			}
			.menu-active {
			border-bottom: 5px solid #df003a;
			}
			.navbar {
			padding: 0.2rem 1rem;
			}

		</style>
		<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">


<style>
/* body,h1,h2,h3,h4,h5,h6 {font-family: "Raleway", sans-serif} */

/* Image Center Crop Pattern CSS */
div.crop {
    position: relative;
    overflow: hidden;
    width:  100%;
    height: 200px;
}

div.crop img {
  display: block;
  margin: 0 auto;
  width: 100%;
  height: auto;
}

div.crop.crop-top img { margin: -10% auto 0; }
div.crop-bottom img { margin: 0 auto -10%; }
div.crop.crop-vertically img { margin: -9% auto; }
div.crop.crop-right img { width: 140%; }
div.crop.crop-left img { width: 140%; margin: 0 0 0 -40%; }
div.crop.crop-horizontally img { width: 180%; margin: 0 -20%; }
div.crop.crop-square img {
  width: 259.5%; /* calculate based on your original image... img-width*100/img-height */
  margin: 0 0 0 -79.75%; /* the above value minus 50 */
}

@media only screen and (max-width: 599px) {
  div.crop img {
    position: absolute;
    top: 0;
    left: 50%;
    margin-left: -300px;
  }
}


.my_input {
    width: 170px;
    box-sizing: border-box;
    /*border: 2px solid #ccc;*/
		border: none;
		border-bottom: 1px solid #ccc;
		margin-bottom: 20px;
    border-radius: 4px;
    font-size: 16px;
    background-color: white;
    background-image: url('<?php echo getBaseUrl() ?>_caramel/assets/img/searchicon.png');
    background-position: 10px 10px;
    background-repeat: no-repeat;
    padding: 12px 20px 12px 40px;
    -webkit-transition: width 0.4s ease-in-out;
    transition: width 0.4s ease-in-out;
}

.my_input:focus {
    width: 40%;
}

ul {
  list-style-type: circle;
}

.card {
    border: 0px solid rgba(0, 0, 0, 0.125);
    border-radius: 0.25rem;
}

.card-footer {
    border-top: 0px solid rgba(0, 0, 0, 0.125);
}

</style>

</head>

<body>

<?php include_once '../view/homepage/header.homepage.php'; ?>



	<!-- Header -->
	<!-- <header class="w3-display-container w3-content w3-center" style="max-width:1500px;height:600px">
		<img class="w3-image" src="<?php echo getBaseUrl() ?>_caramel/assets/img/Untitled design2.jpg" alt="Me" width="1500" height="600">
		<div class="w3-display-middle w3-padding-large w3-border w3-wide w3-text-light-grey w3-center">
			<h1 class="w3-hide-medium w3-hide-small w3-xxxlarge">FAC</h1>
			<h5 class="w3-hide-large" style="white-space:nowrap">FAC</h5>
			<h3 class="w3-hide-medium w3-hide-small">Blog</h3>

		</div>
		<div class="w3-display-bottommiddle w3-container w3-text-white w3-padding-32 w3-hide-small">
 </div>

		<div class="w3-bar w3-black w3-round w3-display-bottommiddle w3-hide-small" style="bottom:-16px">
			<a href="<?php echo getBaseUrl() ?>artikel/" class="w3-bar-item w3-button">Artikel</a>
			<a href="<?php echo getBaseUrl() ?>tutorial/" class="w3-bar-item w3-button">Tutorial</a>
		</div>
	</header> -->

	<!-- Navbar on small screens -->
	<div class="w3-center w3-light-grey w3-padding-16 w3-hide-large w3-hide-medium">
	<div class="w3-bar w3-light-grey">
		<a href="<?php echo getBaseUrl() ?>artikel/" class="w3-bar-item w3-button">Artikel</a>
		<a href="<?php echo getBaseUrl() ?>tutorial/" class="w3-bar-item w3-button">Tutorial</a>
	</div>
	</div>




<?php function SectionHtml(){?>
<div id="top_content">
<?php }?>

<?php if (isset($_GET['app'])&&$_GET['app']=='1'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>
	<?php
		$ctrlArtikel->viewerAddition($_GET['id']);
	?>

<?php SectionHtml() ?>

<!-- <div class="w3-container w3-padding-32">
	<h5 class="w3-center w3-padding-16"><span class="w3-tag w3-wide">Artikel</span></h5>
	<p class="w3-center">Kumpulan Informasi Panduan Dasar & Penyelesaian Masalah pada Accurate V3, V4, V5 & RENE</p>
</div> -->


</div>

<section class="mt-10" style='background: url("") ;background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:20px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
<!-- Page Content -->
<div class="container-fluid">
	<div class="row">
		<!-- Blog Entries Column -->
		<div class="col-md-8">
			<h1 class="my-4"><?php echo $dataArtikel->judul; ?>
			</h1>
			<!-- Blog Post -->
			<div class="card mb-4">
				<div class="row d-flex align-items-center justify-content-center">
				<img class="" width="50%"  src=<?php echo "'".getBaseUrl()."administrasi/artikel/images/".$dataArtikel->image_url."'";?> alt="Card image cap">
			  </div>
				<div class="card-body">
					<!-- <h2 class="card-title">Post Title</h2> -->
					<p class="card-text"><?php echo $dataArtikel->isi; ?></p>


				</div>
				<div class="card-footer text-muted">
					<?php if (isset($_GET['app'])&&$_GET['app']=='1'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>
							Posted on	<?php echo $dataArtikel->tanggal . " | by: " . $dataArtikel->author; ?>
									<a href="#"><strong>Total Viewer: </strong> <?php echo $dataArtikel->viewer; ?></a>

					<?php endif ?>

				</div>
			</div>
			<!-- Blog Post -->

			<!-- Pagination -->

			<!-- <ul class="pagination justify-content-center mb-4">
				<li class="page-item">
					<a class="page-link" href="#">&larr; Older</a>
				</li>
				<li class="page-item disabled">
					<a class="page-link" href="#">Newer &rarr;</a>
				</li>
			</ul> -->

		</div>
		<!-- Sidebar Widgets Column -->
		<div class="col-md-4">
			<!-- <div class="card my-4">
				<h5 class="card-header">Search</h5>
				<div class="card-body">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search for...">
						<span class="input-group-btn">
							<button class="btn btn-secondary" type="button">Go!</button>
						</span>
					</div>
				</div>
			</div> -->

			<!-- Categories Widget -->
			<!-- <div class="card my-4">
				<h5 class="card-header">Categories</h5>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-6">
							<ul class="list-unstyled mb-0">
								<li>
									<a href="#">Artikel</a>
								</li>

							</ul>
						</div>
						<div class="col-lg-6">
							<ul class="list-unstyled mb-0">
								<li>
									<a href="#">Tutorial</a>
								</li>

							</ul>
						</div>
					</div>
				</div>
			</div> -->
			<!-- Side Widget -->
			<div class="card my-4">
				<h5 class="card-header">Popular Post</h5>
				<div class="card-body">
					<ul class="w3-ul w3-hoverable w3-white">
						<?php
							for ($i=0; $i < count($dataSidebar); $i++) {
								echo "<li><a style='text-decoration:none' href='?app=1&id=".$dataSidebar[$i]->id."'>".$dataSidebar[$i]->judul."</a></li>";
							}
						?>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- /.row -->
</div>
<!-- /.container -->
</section>

<?php else: ?>





	<section class="mt-10 mt-40" style='background: url("") ;background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:20px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
	<!-- Page Content -->
	<div class="container-fluid">
		<div class="row">
			<!-- Blog Entries Column -->



			<div class="col-md-8">
				<?php
					for ($i=0; $i < count($datas); $i++){
				?>
				<h1 ><a href=<?php echo "'".getBaseUrl()."artikel/?app=1&id=".$datas[$i]->id."#top_content'";?> style="text-decoration:none"> <b><?php echo $datas[$i]->judul; ?></b></a>
				</h1>
			  <p style="font-size:14px"><?php echo $datas[$i]->tanggal ?></p>

				<!-- Blog Post -->
				<div class="card mb-4">
					<div class="row d-flex align-items-center justify-content-center">
					<img class="" width="60%"  src=<?php echo "'".getBaseUrl()."administrasi/artikel/images/".$datas[$i]->image_url."'"; ?> alt="Card image cap">
					</div>

					<div class="card-body">
						<!-- <h2 class="card-title">Post Title</h2> -->
						<p style="height:80px;font-size=12px;"><?php echo substr(strip_tags($datas[$i]->isi),0,400) . ". . . ."; ?></p>
						<a href=<?php echo "'".getBaseUrl()."artikel/?app=1&id=".$datas[$i]->id."#top_content'"; ?>>Baca selengkapnya... </a>
					</div>
					<div class="card-footer text-muted">
					</div>
				</div>

					<?php } ?>
				<!-- Blog Post -->

				<!-- Pagination -->

				<!-- <ul class="pagination justify-content-center mb-4">
					<li class="page-item">
						<a class="page-link" href="#">&larr; Older</a>
					</li>
					<li class="page-item disabled">
						<a class="page-link" href="#">Newer &rarr;</a>
					</li>
				</ul> -->


				<ul class="pagination justify-content-center mb-4">

						 <?php $countData = $ctrlArtikel->countAllArtikel();?>
						 <?php
						 	$limitPerRow = 6;
							$limit = 5;
				            if (isset($_GET['page'])) {
				               $page = $_GET['page'];
				            } else {
				               $page = 0;
				            }
				            $b = intval($page/$limit);
							$c = ($b + 1) * 5;
							$batas = intval($countData) / $limitPerRow;

							if ($page >= $limit) {
				                    if ($page == $limit) {
				                        $prev = $b * $limit - 2;
				                    } else {
				                        $prev = $b * $limit -1;
				                    }

				                    echo '<li class="page-item"><a class="page-link" href="'.getBaseUrl().'artikel/?page='.$prev.'">< Prev</a></li>';
				            }

						 ?>

						 <?php for ($i=$b*$limit-1; $i < $c; $i++) { ?>
							 	<?php $nowPage = $i + 1 ?>
							 	<?php if ($i<$batas&&$nowPage!=0): ?>
							 	 <li class="page-item"><a class="page-link" href="<?php echo getBaseUrl().'artikel/?page='.$nowPage ?>">
								 	<?php echo $i + 1 ?>
								</a></li>
								<?php endif ?>
						 <?php } ?>

						 <?php if ($c < $batas): ?>
						 	<?php $k = $c + 1; ?>
						 		<li class="page-item"><a class="page-link" href="<?php echo getBaseUrl().'artikel/?page='.$k ?>">
									Next >
								</a></li>
						<?php endif ?>


				</ul>

			</div>




			<!-- Sidebar Widgets Column -->
			<div class="col-md-4">
				<div class="card my-4">
					<h5 class="card-header">Search</h5>
					<div class="card-body">
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Search for...">
							<span class="input-group-btn">
								<button class="btn btn-secondary" type="button">Go!</button>
							</span>
						</div>
					</div>
				</div>

				<!-- Categories Widget -->
				<div class="card my-4">
					<h5 class="card-header">Categories</h5>
					<div class="card-body">
						<div class="row">
							<div class="col-lg-4">
								<ul class="list-unstyled mb-0">
									<li>
										<a href="#">Artikel</a>
									</li>

								</ul>
							</div>
							<div class="col-lg-4">
								<ul class="list-unstyled mb-0">
									<li>
										<a href="#">Tutorial</a>
									</li>

								</ul>
							</div>
						</div>
					</div>
				</div>
				<!-- Side Widget -->
				<div class="card my-4">
					<h5 class="card-header">Popular Post</h5>
					<div class="card-body">
						<ul class="w3-ul w3-hoverable w3-white">
							<?php
								for ($i=0; $i < count($dataSidebar); $i++) {
									echo "<li><a style='text-decoration:none' href='?app=1&id=".$dataSidebar[$i]->id."'>".$dataSidebar[$i]->judul."</a></li>";
								}
							?>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->
	</section>






	</div>
</section>


<?php endif ?>







<?php include_once '../view/homepage/footer.homepage.php'; ?>
		<script src="https://fac-institute.com/assets/homepage2019/js/vendor/jquery-2.2.4.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://fac-institute.com/assets/homepage2019/js/vendor/bootstrap.min.js"></script>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
		<script src="https://fac-institute.com/assets/homepage2019/js/easing.min.js"></script>
		<script src="https://fac-institute.com/assets/homepage2019/js/hoverIntent.js"></script>
		<script src="https://fac-institute.com/assets/homepage2019/js/superfish.min.js"></script>
		<script src="https://fac-institute.com/assets/homepage2019/js/jquery.ajaxchimp.min.js"></script>
		<script src="https://fac-institute.com/assets/homepage2019/js/jquery.magnific-popup.min.js"></script>
		<script src="https://fac-institute.com/assets/homepage2019/js/owl.carousel.min.js"></script>
		<script src="https://fac-institute.com/assets/homepage2019/js/jquery.sticky.js"></script>
		<script src="https://fac-institute.com/assets/homepage2019/js/jquery.nice-select.min.js"></script>
		<script src="https://fac-institute.com/assets/homepage2019/js/parallax.min.js"></script>
		<script src="https://fac-institute.com/assets/homepage2019/js/mail-script.js"></script>
		<script src="https://fac-institute.com/assets/homepage2019/js/main.js"></script>
		<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

<script>
$( document ).ready(function() {
	$("#kotak_cari").on('keyup', function (e) {
	    if (e.keyCode == 13) {
	        test();
	    }
	});
    words = $("#kotak_cari").val();
		if (words != "" ){
			test();
		}
});

$( "#cari" ).click(function() {
test();
});


 function test() {
	 var my_query = {
			 'words': $("#kotak_cari").val()
	 }
	$.ajax({
		 type: "POST",
		 url: "search",
		//  url: "search.php",
		 dataType: 'json',
		 data: {
			 'data': my_query
		 },
		 cache: false,
		 success: function(data) {
			 document.getElementById("article_list").innerHTML = "";

			 if (data.length == 0 ) {
				 document.getElementById("article_list").innerHTML += "<div class='w3-container'><h2>"+"Data tidak ditemukan"+"</h2></div>"
			 }

			 else {

			 for (i=0; i < data.length ; i++){
				 document.getElementById("article_list").innerHTML += "<div class='w3-third w3-container w3-margin-bottom'><div class='crop crop-square'><img src=" + "https://fac-institute.com/" + "administrasi/artikel/images/" + data[i].image_url + "></div><div class='w3-container w3-white'>"+
				 "<p><a style='text-decoration:none;font-weight:bold' href=" + "https://fac-institute.com/" + "artikel/?app=1&id=" + data[i].id + "#top_content" + ">" +(data[i].judul).substr(0, 40) +
				 "</a></p><p style='font-size:10px'>" + data[i].tanggal  +"</p>"+
				 "<p style='height:80px;font-size=12px;'>" +
				 ((data[i].isi).substr(0, 100)).replace(/<\/?[^>]+(>|$)/g, "") + "</p>"+
				 "<a href=" + "https://fac-institute.com/" + "artikel/?app=1&id=" +  data[i].id + "#top_content" + "> Baca selengkapnya... </a>"
				 + "<div class='w3-col m8 s12'> </div></div></div>";

			 }
			 }
		 }
 });
 }

</script>

</body>
</html>
