<?php
$data = array(
        'base_url' => base_url()
    );

    $data = (object) $data;
?>

<?php
function base_url(){
  $currentPath = $_SERVER['PHP_SELF'];$pathInfo = pathinfo($currentPath);$hostName = $_SERVER['HTTP_HOST'];$protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https://'?'https://':'http://';
  return 'https://'.$hostName."/";
}


?>

<!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="<?= $data->base_url ?>images/homepage2019/elements/fav.png">
	<!-- Author Meta -->
	<meta name="author" content="Rifzky Alam, Dino Damara">
	<!-- Meta Description -->
	<meta name="description" content="<?= $data->pagedesc ?>">
	<!-- Meta Keyword -->
	<meta name="keywords" content="<?= $data->pagekeywords ?>">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title></title>

	<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
		<!--
		CSS
		============================================= -->
		<link rel="shortcut icon" href='https://fac-institute.com/_caramel/assets/img/g25992_9_kUd_icon.ico'>
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/linearicons.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/owl.carousel.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/nice-select.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/magnific-popup.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/bootstrap.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/main.css">
		<style media="screen">

		.banner-content h1 {
				font-size: 38px;
		}
		.nav-menu a {
				padding: 15px 8px 1px 8px;
				font-size: 16px;
				font-weight: 500;
		}
		.nav-menu ul li a {
				font-size: 14px;
		}

		.menu-active {
			border-bottom: 5px solid #df003a;
		}

		.navbar {
				padding: 0.2rem 1rem;
		}


    @media (max-width: 480px) {
      .mybanner {
        height:auto;
      }
      #myh2 {
        font-size: 22px;
      }
      .myimg{
        display: none;
      }
      #mobile-nav-toggle {
        top: 35px;
      }
    }

    ol {list-style-type: decimal;}



		</style>
		<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
	</head>
	<body>





<section style='background: url("") ;background-position: center center;background-repeat: no-repeat;
padding-top:60px;padding-right:30px;padding-bottom:20px;padding-left:30px;
-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
  <div class="container-fluid">
    <div class="row ">
      <div class="col-lg-12 " style="max-width:1200px">
        <p><strong><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">SYARAT &amp; KETENTUAN ONSITE ACCURATE ONLINE</span></span></strong></p>

        <ol>
        	<li><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Pemesanan paling lambat 3 hari sebelum tanggal pelaksanaan.</span></span></li>
        	<li><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Customer wajib memiliki koneksi internet yang cukup baik, minimal kecepatan upload dan download 2 Mbps.</span></span></li>
        	<li><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Tanggal rencana pelaksanaan implementasi yang diisi pada Apps adalah jadwal sementara, mengenai kepastian jadwalnya akan dikonfirmasi via email <em>fac-training@com</em></span></span></li>
        	<li><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Jumlah peserta implementasi tak terbatas dan FAC Institute akan mengirimkan 1 orang implementator.</span></span></li>
        	<li><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Untuk keperluan bukti potong PPh23, customer wajib menginformasikan NPWP Perusahaan pada Biodata Perusahaan.</span></span></li>
        	<li><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Nilai produk sudah di gross-up dengan PPh 23 dan nilai PPh 23 dihitung dari 2% x nilai yang tercantum di Invoice.</span></span></li>
        	<li><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Customer wajib membuatkan bukti potong Pph 23 dan mengirimkan fisik bukti potong PPh 23 ke alamat dibawah ini:</span></span></li>
        </ol>

        <p style="margin-left:.5in"><strong><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">FAC Institute</span></span></strong></p>

        <p style="margin-left:.5in"><span style="font-size:10.5pt"><span style="background-color:#222222"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"><span style="color:white">Jl. Raya Jatiwaringin No. 8,</span></span></span></span></p>

        <p style="margin-left:.5in"><strong><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Jakarta Timur </span></span></strong><span style="font-size:10.5pt"><span style="background-color:#222222"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"><span style="color:white">13620</span></span></span></span><br />
        <span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><strong>Up. Fajar Shodiq</strong></span></span></p>

        <ol start="8">
        	<li><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Setelah menerima bukti potong dari Customer, Fac Institute akan mentransfer kembali sejumlah nilai PPh 23 yang tercantum di bukti potong ke rekening Customer.</span></span></li>
        	<li><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Durasi pelaksanaan implementasi selama 8 jam (termasuk istirahat makan siang selama 1 jam) dan tidak dapat diakumulasikan ke hari yang lain.</span></span></li>
        	<li><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Hari kerja implementator kami adalah Senin s/d Jumat. Diluar jam kerja tsb harus persetujuan dengan Fac Institute dengan cara email ke </span></span><a href="mailto:onsite@ultimasolusindo.com" target="_blank">fac-training@com</a></li>
        	<li><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Customer wajib menyediakan makan siang bagi implementator.</span></span></li>
        	<li><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Pilihan waktu onsite ada 2 yaitu pk 08.00 s/d pk 16.00 Atau pk 09.00 s/d pk 17.00.</span></span></li>
        	<li><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Jika implementator kami terlambat maka mereka wajib untuk mengganti kekurangan jam di hari yang sama dan bukan di hari yang lain.</span></span></li>
        	<li><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Apabila ada komplain mengenai implementator, dapat dilakukan di hari pertama sebelum jam 12 siang melalui email <a href="mailto:onsite@ultimasolusindo.com" target="_blank"><span style="color:blue">fac-training@com</span></a> dengan menginformasikan alasan yang jelas.</span></span></li>
        	<li><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Komplain tidak berlaku apabila pihak pemesan sudah menandatangani Lembar Evaluasi Kerja Impelementator dan diberi cap (stempel) perusahaan.</span></span></li>
        	<li><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Untuk membantu kelancaran proses implementasi perusahaan Wajib menyiapkan data saldo awal dalam bentuk excel seperti:</span></span></li>
        </ol>

        <p style="margin-left:.5in"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">&bull; Data piutang pelanggan</span></span></p>

        <p style="margin-left:.5in"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">&bull; Data hutang vendor</span></span></p>

        <p style="margin-left:.5in"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">&bull; Data aktiva tetap</span></span></p>

        <p style="margin-left:.5in"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">&bull; Data barang dan jasa</span></span></p>

        <p style="margin-left:.5in"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">&bull; Data Daftar akun (menggunakan nilai terakhir yang digunakan)</span></span></p>

        <ol start="17">
        	<li><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Jika data belum siap selama implementasi atau pada saat implementasi, implementator dapat diminta oleh Customer untuk membantu menyiapkan data- data yang diperlukan di point 15.</span></span></li>
        	<li><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Apabila implementasi tidak selesai tepat waktu sesuai dengan tanggal yang sudah dijadwalkan karena point 16, maka hal tsb bukan merupakan tanggung jawab implementator Fac Institute.</span></span></li>
        	<li><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Untuk Paket onsite AOL 3 hari dan 5 hari waktu pelaksanaannya harus berurutan sesuai jumlah hari dan tidak boleh dipecah-pecah.</span></span></li>
        	<li><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Produk implementasi yang sudah dibayar tidak dapat dilakukan pembatalan.</span></span></li>
        	<li><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Perubahan jadwal implementasi oleh perusahaan harus disampaikan ke Fac Institute paling lambat 3 hari (kerja) sebelum waktu pelaksanaan implementasi, apabila dilakukan 1(satu) hari sebelumnya atau di hari pelaksanaan maka dikenakan denda 30 % x hari x satuan biaya per hari. Selanjutnya waktu pelaksanaan akan direschedule kembali dan Customer boleh mereschedule onsite maksimal hanya 2 kali.</span></span></li>
        </ol>

        <p style="margin-left:.5in"><strong><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Contoh :</span></span></strong></p>

        <p style="margin-left:.5in"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Paket AOL 5 hari = Rp. 6.000.000 (5 hari), pada hari ke-2 user yang akan ditraining tiba-tiba jatuh sakit dan karena tidak ada user lain yang bisa ditraining, maka pada hari itu direschedule ke hari berikutnya, maka akan dikenakan denda = 30% x 1/5 x 6.000.000 = Rp. 360.000/hari.</span></span></p>

        <ol start="22">
        	<li><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Jadwal implementasi yang sudah disepakati dan sedang dalam pelaksanaan tidak dapat diubah.</span></span></li>
        	<li><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Biaya transportasi implementator ke kantor customer sbb:</span></span></li>
        </ol>

        <p style="margin-left:.5in"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">&bull; DKI Jakarta &ndash; Tidak ada</span></span></p>

        <p style="margin-left:.5in"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">&bull; Depok, Bogor, Cikarang, Serang , Tangerang dan Bekasi Rp 150.000/hari.</span></span></p>

        <p style="margin-left:.5in"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">&bull; Diluar daftar di atas wajib menyediakan transportasi dan akomodasi sesuai Form Fasilitas Implementator.</span></span></p>

        <ol start="24">
        	<li><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Tugas implementator mencakup:</span></span></li>
        </ol>

        <p style="margin-left:.5in"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">a) <strong>Onsite One Day Service AOL 1 hari </strong> :</span></span></p>

        <p style="margin-left:.5in"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">&bull; Implementator mentraining fitur dan modul yang ada di Accurate Online sesuai yang diisi pada field <strong>Latar Belakang Masalah</strong>.</span></span></p>

        <p style="margin-left:.5in"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">b)<strong> Onsite Package Service AOL 3 hari dan 5 hari</strong> :</span></span></p>

        <p style="margin-left:.5in"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">&bull; Membantu setup awal database(hanya berlaku untuk pembuatan 1 database).</span></span></p>

        <p style="margin-left:.5in"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">&bull; Transfer Know How mengenai:</span></span></p>

        <p style="margin-left:.5in"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">&ndash; Pengenalan fitur yang ada di Accurate Online</span></span></p>

        <p style="margin-left:.5in"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">&ndash; Cara input transaksi di modul penjualan, pembelian, persediaan, general ledger, aktiva tetap</span></span></p>

        <p style="margin-left:.5in"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">&bull; Pendampingan input transaksi</span></span></p>

        <p style="margin-left:.5in"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">&bull; Review ulang modul yang sudah diajarkan</span></span></p>

        <ol start="25">
        	<li><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Pertanyaan atau kasus di luar ruang lingkup latar belakang masalah tidak dapat menjadi kewajiban implementator untuk menjawab. Customer dapat langsung mengirimkan email ke alamat <a href="mailto:support@cpssoft.com" target="_blank"><span style="color:blue">support@cpssoft.com</span></a></span></span></li>
        	<li><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Seluruh kebutuhan penunjang implementasi seperti laptop, komputer atau infokus disediakan oleh Customer.</span></span></li>
        	<li><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Setelah selesai implementasi berdasarkan hitungan hari yang telah dibayar, Customer tidak dapat meminta layanan tambahan (extra) implementasi tanpa dengan biaya.</span></span></li>
        	<li><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Mengenai seting internet dan jaringan bukan kewajiban dan tugas dari implementator.</span></span></li>
        	<li><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Jasa pembuatan design template adalah layanan di luar implementasi Accurate, sehingga jika Customer meminta dibuatkan mohon terlebih dahulu mengirimkan contoh formulir via email <a href="mailto:onsite@ultimasolusindo.com" target="_blank"><span style="color:blue">fac-training@com</span></a> agar pihak Fac Institute dapat memastikan apakah form tsb bisa atau tidak dibuat.</span></span></li>
        	<li><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Klaim onsite yang belum dijalankan oleh Fac Institute selambat-lambatnya akan dilakukan dalam waktu 1 bulan dari tanggal reschedule. Jika melebihi dari waktu yang sudah ditentukan maka akan dianggap HANGUS.</span></span></li>
        </ol>

        <p>&nbsp;</p>

      </div>
    </div>





  </div>
</section>






		<script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/jquery-2.2.4.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/bootstrap.min.js"></script>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/easing.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/hoverIntent.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/superfish.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.ajaxchimp.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.magnific-popup.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/owl.carousel.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.sticky.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.nice-select.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/parallax.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/mail-script.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/main.js"></script>
		<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
		<script>
		AOS.init();
		$('.test1').click(function() {
				var sectionTo = $(this).attr('href');
				$('html, body').animate({
					scrollTop: $(sectionTo).offset().top
				}, 1000);
		});
		</script>
	</body>
</html>
