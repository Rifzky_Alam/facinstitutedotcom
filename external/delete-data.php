<?php 
session_start();
if (!isset($_SESSION['admin'])||empty($_SESSION['admin']['nama'])||($_SESSION['admin']['tipe']!='admin'&&$_SESSION['admin']['tipe']!='kursus')) {
    header('Location:../sessions/index.php');
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>FAC-Institute -- Freelance</title>
    <?php $page='deleteData' ?>
    <?php include_once 'view-support/header.php'; ?>
    <?php MetaTag($page) ?>
    <?php Links($page) ?>
    <?php Scripts($page) ?>
    <?php Styles($page) ?>
    
</head>
<body>
<?php 
include_once '../model/Kursus.php';
$kursus = new Kursus();

?>

<?php include_once 'view-support/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <?php include_once 'view-support/sidebar.php'; ?>
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h3>Anda Yakin ingin menghapus data ini?</h3>
            </div>

            <div class="row">
                <?php 
                if (isset($_POST['delnk'])) {
                    if (!empty($_POST['delnk']['id'])) {
                        if ($kursus->deleteDataNamaKursus($_POST['delnk']['id'])) {
                            echo "<script>alert('Data ".$_POST['delnk']['namaKursus']." has been deleted!');</script>";
                            echo "<script>location.replace('input-kelas');</script>";
                        }
                        
                    }
                }elseif (isset($_POST['dellk'])){

                    if ($kursus->deleteLokasiKursus($_POST['dellk']['id'])) {
                        echo "<script>alert('Data ".$_POST['dellk']['lokasi']." has been deleted!');</script>";
                        echo "<script>location.replace('input-kelas');</script>";   
                    }
                }elseif (isset($_POST['deldjk'])) {
                    
                    if ($kursus->deleteJadwalKursus($_POST['deldjk'])){
                        echo "<script>alert('Data has been deleted!');</script>";
                        echo "<script>location.replace('index');</script>";   
                    }

                }

                ?>
            </div>

            <?php 
                if (isset($_GET['ac'])&&$_GET['ac']=='nk'&&isset($_GET['id'])&&!empty($_GET['id'])) {
                    $dataDelete = $kursus->getDataKursusByID($_GET['id']);
                }elseif (isset($_GET['ac'])&&$_GET['ac']=='lk'&&isset($_GET['id'])&&!empty($_GET['id'])) {
                    $dataDelete = $kursus->getLokasiKursusByID($_GET['id']);
                }elseif (isset($_GET['ac'])&&$_GET['ac']=='djk'&&isset($_GET['id'])&&!empty($_GET['id'])) {
                    $dataDelete = $kursus->getJadwalKursus($_GET['id']);
                }
            ?>

            <form action="" method="post">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                            <?php if (isset($_GET['ac'])&&$_GET['ac']=='nk'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>
                                <th style="text-align:center;">Nama Kursus</th>
                            <?php elseif(isset($_GET['ac'])&&$_GET['ac']=='lk'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>
                                <th style="text-align:center;">Lokasi Kursus</th>
                                <th style="text-align:center;">Map Lokasi</th>
                            <?php elseif(isset($_GET['ac'])&&$_GET['ac']=='djk'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>
                                <th style="text-align:center;">Lokasi Kursus</th>
                                <th style="text-align:center;">Nama Kelas</th>
                                <th style="text-align:center;">Hari Kelas</th>
                                <th style="text-align:center;">Mulai Kelas</th>
                                <th style="text-align:center;" colspan="2">Durasi Kelas</th>
                                <th style="text-align:center;">Investasi</th>
                            <?php endif ?>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <?php if (isset($_GET['ac'])&&$_GET['ac']=='nk'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>
                                <td style="text-align:center;"><?php echo $dataDelete->nama_kursus ?></td>
                                <?php elseif(isset($_GET['ac'])&&$_GET['ac']=='lk'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>
                                <td><?php echo $dataDelete->lokasi_kursus ?></td>
                                <td>
                                    <a href="<?php echo 'https://www.google.com/maps/place/'.$dataDelete->map ?>">
                                        <?php echo $dataDelete->map ?>
                                    </a>
                                </td>
                                <?php elseif(isset($_GET['ac'])&&$_GET['ac']=='djk'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>
                                    <td><?php echo $dataDelete->lokasi_kursus ?></td>
                                    <td><?php echo $dataDelete->nama_kursus ?></td>
                                    <td><?php echo $dataDelete->hari ?></td>
                                    <td><?php echo $dataDelete->mulai_kursus ?></td>
                                    <td><?php echo $dataDelete->jam ?></td>
                                    <td><?php echo $dataDelete->durasi ?></td>
                                    <td>Rp <?php echo number_format($dataDelete->investasi) ?>.-</td>
                                <?php endif ?>
                            </tr>       
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row" style="display:none;">
                <div class="col-md-12">
                    <?php if (isset($_GET['ac'])&&$_GET['ac']=='nk'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>
                        <input type="text" name="delnk[namaKursus]" value="<?php echo $dataDelete->nama_kursus ?>">
                        <input type="text" name="delnk[id]" value="<?php echo $dataDelete->id ?>">
                    <?php elseif(isset($_GET['ac'])&&$_GET['ac']=='lk'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>
                        <input type="text" name="dellk[id]" value="<?php echo $dataDelete->id ?>">
                        <input type="text" name="dellk[lokasi]" value="<?php echo $dataDelete->lokasi_kursus ?>">
                    <?php elseif(isset($_GET['ac'])&&$_GET['ac']=='djk'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>
                        <input type="text" name="deldjk[id]" value="<?php echo $dataDelete->id ?>">
                    <?php endif ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <button class="btn btn-lg btn-danger" style="width:100%">Delete Data</button>
                </div>
            </div>

            </form>
        </div>
    </div>
</div>

</body>
</html>