<?php include_once '../baseurl.php'; ?>
<?php $page='login'; ?>
<?php session_start(); ?>
<?php 
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
include_once 'view-support/sessions.php';

$sesi = new Sessionz();

$sesi->login();
// if (isset($_SESSION['admin'])&&!empty($_SESSION['admin']['myuser'])) {
      // header('Location: index.php');
// }
 ?>
 <!DOCTYPE html>
<html>
<head>
  <title>Login -- External FAC</title>
  <link rel="stylesheet" type="text/css" href="<?php echo getBaseUrl().'assets/external/login/style.css' ?>">
</head>
<body>

<?php 
  if (isset($_POST['in'])) {
    include_once '../model/external.php';
    $petugas = new External();
    $petugas->setUsername($_POST['in']['username']);
    $petugas->setPassword($_POST['in']['password']);


    if ($petugas->login()) {
      $_SESSION['external'] = array(
                          'username' => $petugas->getUsername(),
                          'nama' => $petugas->getNama(),
                          'email' => $petugas->getEmail(),
                          'telepon' => $petugas->getTelepon(),
                          'token' => $petugas->getPassword(),
                          'cabang' => $petugas->getCabang()
      );
      echo "<script>alert('Selamat datang ".$petugas->getNama()."');</script>";  
      echo "<script>location.replace('index.php');</script>";  
    } else {
      echo "<script>alert('Anda tidak terdaftar dalam sistem kami.');</script>";  
    }
    
    
  }

  
?>




<div class="login-page">
  <div class="form">
    
    <form class="login-form" action="" method="POST">
      <input type="text" placeholder="username" name="in[username]" />
      <input type="password" placeholder="password" name="in[password]" />
      <button>login</button>
      </form>
  </div>
</div>

<script type="text/javascript" src="<?php echo getBaseUrl().'js/jquery-1.11.1.js' ?>"></script>
</body>
</html>