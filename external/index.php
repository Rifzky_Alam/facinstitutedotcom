<?php 
session_start();
// if (!isset($_SESSION['admin'])||empty($_SESSION['admin']['nama'])||($_SESSION['admin']['tipe']!='admin'&&$_SESSION['admin']['tipe']!='kursus')) {
    // header('Location:../sessions/index.php');
// }
$page="dashboard";
include_once 'view-support/sessions.php';
$sesi = new Sessionz();
$sesi->dashboard('');
?>
<!DOCTYPE html>
<html>
<head>
    <title>FAC-Institute -- External</title>
    <?php include_once 'view-support/header.php'; ?>
    <?php MetaTag('Home') ?>
    <?php Links('Home') ?>
    <?php Scripts('Home') ?>
    <?php Styles('Home') ?>
    
</head>
<body>
<?php 
    include_once '../model/external.php';
    $ex = new External();
    $ex->setUsername($_SESSION['external']['username']);
?>

<?php include_once 'view-support/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <!--sidebar is here !!!!-->
        <?php include_once 'view-support/sidebar.php'; ?>
        <!--sidebar ends here !!!!-->
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1>FAC-Institute ::  External-Home</h1>
            </div>
            <div class="row">
            	<div class="col-md-6">
            		<div class="panel panel-warning">
      					<div class="panel-heading">Jumlah customer terinput</div>
      					<div class="panel-body"><?php echo $ex->countCustomer(); ?></div>
    				</div>            	
    			</div>
            	<div class="col-md-6">
            		<div class="panel panel-default">
                        <div class="panel-heading">Penawaran terbuat</div>
                        <div class="panel-body"><?php echo $ex->countQuotation(); ?></div>
                    </div>
            	</div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <h3>Pemberitahuan</h3>    
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <ul>
                        <li>Di harapkan untuk memasukkan data dengan lengkap.</li>
                        <li>Untuk mengirim penawaran ikuti langkah berikut:
                            <ol>
                                <li>Isi data perusahaan customer</li>
                                <li>Isi data customer</li>
                                <li>Dari data customer klik isi penawaran</li>
                                <li>Sudah terisi maka "isi penawaran" akan berubah menjadi "kirim penawaran"</li>
                                <li>Cek kembali pengawalan yang akan anda kirim dengan preview data pdf dan email</li>
                            </ol>
                        </li>
                        <li>
                            Untuk mengirim penawaran format UTS ikuti langkah berikut:
                            <ol>
                                <li>Isi data perusahaan terlebih dahulu.</li>
                                <li>Isi data customer dan jangan pilih paket (biarkan default).</li>
                                <li>Buat penawaran (UTS) pada data customer.</li>
                                <li>Lihat dan cek kembali data penawaran sebelum mengirim.</li>
                            </ol>
                        </li>
                    </ul>
                </div>    
            </div>


        </div>
    </div>
</div>

</body>
</html>