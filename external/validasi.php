<?php 
session_start();
if (!isset($_SESSION['admin'])||empty($_SESSION['admin']['nama'])||($_SESSION['admin']['tipe']!='admin'&&$_SESSION['admin']['tipe']!='kursus')) {
    header('Location:../sessions/index.php');
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>FAC-Institute -- Admin Kursus</title>
    <?php $page='validasi' ?>
    <?php include_once 'view-support/header.php'; ?>
    <?php MetaTag($page) ?>
    <?php Links($page) ?>
    <?php Scripts($page) ?>
    <?php Styles($page) ?>
    
</head>
<body>
<?php 
include_once '../model/Kursus.php';
$kursus = new Kursus();
$tempat_kursus =  json_decode($kursus->getDatas('tempat_kursus'));
?>

<?php include_once 'view-support/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <?php include_once 'view-support/sidebar.php'; ?>
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1>FAC-Institute ::  Validasi</h1>
            </div>

            <div class="row">
                <?php 
                if (isset($_POST['in'])) {
                    //$time = strtotime($_POST['in']['jamPelaksanaan']);
                    $inp = array('id' => $_POST['in']['kode']);
                    $inp = (object) $inp;
                    if ($kursus->updateStatus($inp)) {
                        echo "<script>alert('Status Berhasil diubah.');</script>";
                        echo "<script>location.replace('".basename(__FILE__, '.php')."');</script>";
                    }else{
                        "<script>alert('Status gagal diubah.');</script>";
                    }

                }

                ?>
            </div>

            <form action="" method="post">
            <div class="row">
                <div class="col-md-12">
                    
                    <div class="form-group">
                        <label>Kode Validasi</label>
                        <input type="text" name="in[kode]" class="form-control" placeholder="Kode Validasi Siswa" required>
                    </div>
                    
                    <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>                   
                </div>    
            </div>
            </form>

        </div>
    </div>
</div>

</body>
</html>