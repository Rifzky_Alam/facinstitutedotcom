<?php 
session_start();
// if (!isset($_SESSION['admin'])||empty($_SESSION['admin']['nama'])||($_SESSION['admin']['tipe']!='admin'&&$_SESSION['admin']['tipe']!='kursus')) {
    // header('Location:../sessions/index.php');
// }
$page="kirimpenawaran";
include_once 'view-support/sessions.php';
$sesi = new Sessionz();
$sesi->dashboard('');
?>
<!DOCTYPE html>
<html>
<head>
    <title>FAC-Institute/Send Quotation</title>
    <?php include_once 'view-support/header.php'; ?>
    <?php MetaTag('Home') ?>
    <?php Links('Home') ?>
    <?php Scripts('Home') ?>
    <?php Styles('Home') ?>
    
</head>
<body>
<?php 
    
    include_once '../model/external.php';
    $ex= new External();
    
    if (isset($_GET['id'])&&!empty($_GET['id'])) {
        include_once '../model/Mypdf.php';
        $pdf = new Mypdf();

        $ex->setQuotation($_GET['id']);
        $data = $ex->getQuotationForDisplay();
        // print_r($data);

        
        $datas = array(
            'title' => $data->title,
            'no_quotation' => $data->id,
            'nama_usaha' => $data->nama_usaha,
            'nama_personal' => $data->nama,
            'email' => $data->email,
            'subjek' => $data->subjek,
            'produk' => $data->nama_item,
            'harga' => $data->harga,
            'biaya_transport' => $data->biaya_transport,
            'petugas' => $data->petugas
        );
        $datas = (object) $datas;
        $pdf->setData($datas);

        //set path and file
        $pdf->setPath(getcwd().'/pdf/');
        $pdf->setNameFile('Quotation '.$data->nama_usaha.'.pdf');    
        //create pdf file

        //check if file exists or not
        if (!file_exists('pdf/Quotation '.$data->nama_usaha.'.pdf')) {
            $pdf->Penawaran('create');    
        }

    }elseif (isset($_GET['utsq'])&&!empty($_GET['utsq'])){
        include_once '../model/Dompdf.php'; 
        include_once 'view-support/content-penawaran.php'; 

        $pdf = new Mypdf();
        $content = new Content();

        $ex->setQuotation($_GET['utsq']);
        $data = $ex->getQuotationForDisplay();
        $dataz = array(
            'perusahaan' => $data->nama_usaha,
            'nama' => $data->nama,
            'email' => $data->email,
            'transport' => $data->biaya_transport,
            'petugas' => $data->petugas
        );
        $dataz = (object) $dataz;
        // $html = 
        // $pdf->Display($content->penawaran($data));
        $pdf->setContent($content->penawaran($dataz));
        $pdf->setPath('pdf/');
        $pdf->setFilename('Penawaran Training FAC Institute - '.$dataz->perusahaan);
        if (!file_exists('pdf/Penawaran Training FAC Institute - '.$dataz->perusahaan.'.pdf')) {
            $pdf->Create();    
        }
        
    }

    



    if (isset($_POST['email'])&&!empty($_POST['email'])){
            include_once '../model/Surat.php';
            include_once '../model/Mail.php';
            $surat = new Surat();
            $mail = new FACMail();

            $objek = array(
                'nama_personal' => $data->nama,
                'subject'=> $data->subjek,
                'nama_usaha' => $data->nama_usaha,
                'produk' => $data->nama_item,
                'biaya' => $data->harga,
                'notes' => $data->notes,
                'biaya_transport' => $data->biaya_transport,
                'petugas' => $data->petugas
            );
            $objek = (object) $objek;
            $content = $surat->quotation($objek);


            $lampir = array();
            array_push($lampir, 'pdf/Quotation '.$data->nama_usaha.'.pdf');

            if (!empty($data->attachments)) {
                $lampirz = explode("#", $data->attachments);
                for ($i=0; $i < count($lampirz); $i++){ 
                    array_push($lampir, 'attachments/'.$lampirz[$i]);
                }
            }

            if (!empty($data->cc)||$data->cc!='') {
                $cece = explode(" ", $data->cc);
            } else {
                $cece = '';
            }

            $dataEmail = array(
                'to' => $_POST['email'],
                'nama'=> '',
                'subject'=> 'Penawaran '.$data->nama_usaha,
                'cc'=> $cece,
                'content'=>$content,
                'attachments'=> $lampir
            );
            $dataEmail = (object) $dataEmail;

        if ($mail->KirimJadwal($dataEmail)){
            unlink('pdf/Quotation '.$datas->nama_usaha.'.pdf');
            $quotation = array(
                            'id' => $_GET['id'],
                            'status' => 1 
            );
            $quotation = (object) $quotation;
            $ex->setQuotation($quotation);
            $ex->editStatusQuotation();

            echo "<script>alert('penawaran berhasil dikirim');</script>";
            echo "<script>location.replace('data-quotation');</script>";
        }   

        //uts quotation
    }elseif(isset($_POST['cemail'])&&!empty($_POST['cemail'])) {
        include_once '../model/Surat.php';
        include_once '../model/Mail.php';
        $surat = new Surat();
        $mail = new FACMail();

        $objek = array(
            'cp' => $data->nama,
            'np' => $data->nama_usaha,
            'notes' => $data->notes,
            'biaya_transport' => $data->biaya_transport,
            'petugas' => $data->petugas
        );
        $objek = (object) $objek;
        $isi = $surat->PenawaranUTS($objek);
        $lampir = array();
        //dataz is object data from creating UTS quotation
        array_push($lampir, 'pdf/Penawaran Training FAC Institute - '.$dataz->perusahaan.'.pdf');

        if (!empty($data->attachments)) {
                $lampirz = explode("#", $data->attachments);
                for ($i=0; $i < count($lampirz); $i++){ 
                    array_push($lampir, 'attachments/'.$lampirz[$i]);
                }
            }

            if (!empty($data->cc)||$data->cc!='') {
                $cece = explode(" ", $data->cc);
            } else {
                $cece = '';
            }

            $dataEmail = array(
                'to' => $_POST['cemail'],
                'nama'=> '',
                'subject'=> 'Penawaran '.$data->nama_usaha,
                'cc'=> $cece,
                'content'=>$isi,
                'attachments'=> $lampir
            );
            $dataEmail = (object) $dataEmail;
            if ($mail->KirimJadwal($dataEmail)){
                //delete pdf
                unlink('pdf/Penawaran Training FAC Institute - '.$dataz->perusahaan.'.pdf');
    
                $quotation = array(
                    'id' => $_GET['utsq'],
                    'status' => 11 
                );

                $quotation = (object) $quotation;
                $ex->setQuotation($quotation);
                $ex->editStatusQuotation();

                echo "<script>alert('penawaran berhasil dikirim');</script>";
                echo "<script>location.replace('data-quotation');</script>";
            }


            
    }
?>

<?php include_once 'view-support/top-nav.php'; ?>

<div class="container-fluid" style="padding-top:50px">

    <div class="row">
        <!--sidebar is here !!!!-->
        <?php include_once 'view-support/sidebar.php'; ?>
        <!--sidebar ends here !!!!-->
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1>FAC-Institute ::  Send Quotation</h1>
            </div>
            <div class="row">
            	<div class="col-md-12">
                    <?php if (file_exists('pdf/Quotation '.$data->nama_usaha.'.pdf')): ?>
                            <h4>Penawaran siap di kirim</h4>
                    <?php elseif(file_exists($pdf->getPath().$pdf->getFilename().'.pdf')): ?>
                            <h4>Penawaran dibuat & siap di kirim</h4>
                        <?php else: ?>
                            <h4>Penawaran error, harap cek data anda kembali atau hubungi administrator kami.</h4>
                    <?php endif ?>
            		
            	</div>
            </div>
            
            <div class="row" style="padding-bottom: 40px;">
                <div class="col-md-12">
                <label>Data penawaran</label>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Nama Cust</th>
                            <th>Nama Usaha</th>
                            <th>Nama Paket</th>
                        </tr>
                    </thead> 
                    <tbody>
                        <tr>
                            <td><?php echo $data->nama ?></td>
                            <td><?php echo $data->nama_usaha ?></td>
                            <td><?php echo $data->nama_item ?></td>
                        </tr>
                    </tbody>   
                </table>
                <?php $attch = explode('#', $data->attachments); ?>
                <label>Lampiran</label>
                    <ol>
                        <?php for ($i=0; $i < count($attch); $i++) { 
                            echo "<li>".$attch[$i]."</li>";
                        } ?>
                    </ol>

                    <p>
                        Pilih email yang akan di kirim penawaran ini.
                    </p>

                    <?php if (isset($_GET['utsq'])&&!empty($_GET['utsq'])): ?>
                    <form action="" method="post">
                        <?php if (!empty($data->email_usaha)||$data->email_usaha!=''): ?>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="cemail" value="<?php echo $data->email_usaha ?>"><?php echo $data->email_usaha ?>
                                </label>
                            </div>    
                        <?php endif ?>
                        
                        <?php if (!empty($data->email)||$data->email!=''): ?>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="cemail" value="<?php echo $data->email ?>"><?php echo $data->email ?>
                                </label>
                            </div>
                        <?php endif ?>
                        

                        <button class="btn btn-lg btn-primary" style="width:100%">Kirim</button>
                    </form>    


                        <?php else: ?>
                    
                    <form action="" method="post">
                        <?php if (!empty($data->email_usaha)||$data->email_usaha!=''): ?>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="email" value="<?php echo $data->email_usaha ?>"><?php echo $data->email_usaha ?>
                                </label>
                            </div>    
                        <?php endif ?>
                        
                        <?php if (!empty($data->email)||$data->email!=''): ?>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="email" value="<?php echo $data->email ?>"><?php echo $data->email ?>
                                </label>
                            </div>
                        <?php endif ?>
                        

                        <button class="btn btn-lg btn-primary" style="width:100%">Kirim</button>
                    </form>
                    <?php endif ?>
                    <br><br>

                </div>    
            </div>


        </div>
    </div>
</div>

</body>
</html>