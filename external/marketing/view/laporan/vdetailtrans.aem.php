<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?= $data->judul ?></title>
  <link rel="stylesheet" type="text/css" href='https://fac-institute.com/js/jquery-datepicker/jquery-ui.min.css'>
  <link rel="stylesheet" href="<?= $data->base_url ?>assets/bootstrap/css/bootstrap.min.css">
</head>
<body>
<?php include_once $data->basedir.'view/laporan/vtopnav.aem.php'; ?>
<div class="container" style="padding-bottom:40px;">

  <div class="row">
    <div class="col-md-12">
      <div class="page-header">
        <h3><?= $data->subtitle ?></h3>
      </div>
    </div>
  </div>

  
  <div class="row">
  	<div class="col-md-12">
  		<table class="table table-bordered">
  			<thead>
  				<tr>
  					<th>Nama Perusahaan</th>
  					<th>Nama Customer</th>
  					<th>Email Customer</th>
  					<th>Telepon Cust</th>
  				</tr>
  			</thead>
  			<tbody>
  				<?php if (count($data->listdata)=='0'): ?>
  					<tr>
  						<td colspan="4" style="text-align:center;">Tidak ada data dalam database kami</td>
  					</tr>	
  				<?php else: ?>
					<?php foreach ($data->listdata as $key): ?>
						<tr>
							<td>
								<?= $key['nama_usaha'] ?>
							</td>
              <td>
                <?= $key['nama_cust'] ?>
              </td>
              <td>
                <?= $key['email_cust'] ?>
              </td>
              <td>
                <?= $key['telp_cust'] ?>
              </td>
						</tr>  					
					<?php endforeach ?>  				
  				<?php endif ?>
  			</tbody>
  		</table>
  	</div>
  </div>

<!--End Container-->
</div>

<script src="<?= $data->base_url ?>js/jquery-1.11.1.js"></script>
<script src="<?= $data->base_url ?>assets/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript" src='https://fac-institute.com/js/jquery-ui/jquery-ui.js'></script>
    <script type="text/javascript">
    $(document).ready(function(){
        $('#tglawal').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('#tglakhir').datepicker({
            dateFormat: 'yy-mm-dd'
        });
    });
    </script>
</body>
</html>

