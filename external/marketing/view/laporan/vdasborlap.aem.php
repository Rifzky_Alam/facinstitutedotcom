<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?= $data->judul ?></title>
  <link rel="stylesheet" type="text/css" href='https://fac-institute.com/js/jquery-datepicker/jquery-ui.min.css'>
  <link rel="stylesheet" href="<?= $data->base_url ?>assets/bootstrap/css/bootstrap.min.css">
</head>
<body>
<?php include_once $data->basedir.'view/laporan/vtopnav.aem.php'; ?>
<div class="container" style="padding-bottom:40px;">

  <div class="row">
    <div class="col-md-12">
      <div class="page-header">
        <h3><?= $data->subtitle ?></h3>
      </div>
    </div>
  </div>

  <div class="row">
    <form action="" method="get" accept-charset="utf-8">
    <div class="col-md-12">
      <div class="form-inline">
        <input type="text" name="in[fd]" id="tglawal" class="form-control" required placeholder="Tanggal Awal">
        <input type="text" name="in[td]" id="tglakhir" class="form-control" required placeholder="Tanggal Akhir">        
        <button class="btn btn-primary"><span class="glyphicon glyphicon-search"></span></button>
      </div>
    </div>
    </form>
  </div>   
  <br>
  <br>
  <div class="row">
  	<div class="col-md-12">
  		<table class="table table-bordered">
  			<thead>
  				<tr>
  					<th>Nama Marketing</th>
  					<th>Jumlah Omset</th>
  					<th>Jumlah Invoice Terbayar</th>
  					<th>Jumlah Transaksi</th>
  					<th>Jumlah Traning Harian</th>
  				</tr>
  			</thead>
  			<tbody>
  				<?php if (count($data->listmarketing)=='0'): ?>
  					<tr>
  						<td colspan="5" style="text-align:center;">Tidak ada data dalam database kami</td>
  					</tr>	
  				<?php else: ?>
					<?php foreach ($data->listmarketing as $key): ?>
						<tr>
							<td>
								<a href="<?= $data->base_url.'external/marketing/admin/personal/'.$key['aidi'] ?>">
									<?= $key['nama'] ?>
								</a>
							</td>
							<td style="text-align:right;"><?= number_format($key['omset']) ?> IDR</td>
							<td style="text-align:right;"><?= number_format($key['invoice_terbayar']) ?> IDR</td>
							<td>
                <a href="<?= $data->base_url.'external/marketing/admin/detailtrans/'.$key['aidi'] ?>" title="klik untuk detail">
                  <?= number_format($key['jumlah_transaksi']) ?>
                </a>
              </td>
							<td><?= number_format($key['jumlah_hari_training']) ?></td>
						</tr>  					
					<?php endforeach ?>  				
  				<?php endif ?>
  			</tbody>
  		</table>
  	</div>
  </div>

<!--End Container-->
</div>

<script src="<?= $data->base_url ?>js/jquery-1.11.1.js"></script>
<script src="<?= $data->base_url ?>assets/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript" src='https://fac-institute.com/js/jquery-ui/jquery-ui.js'></script>
    <script type="text/javascript">
    $(document).ready(function(){
        $('#tglawal').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('#tglakhir').datepicker({
            dateFormat: 'yy-mm-dd'
        });
    });
    </script>
</body>
</html>

