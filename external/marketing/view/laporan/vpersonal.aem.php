<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?= $data->judul ?></title>
  <link rel="stylesheet" type="text/css" href='https://fac-institute.com/js/jquery-datepicker/jquery-ui.min.css'>
  <link rel="stylesheet" href="<?= $data->base_url ?>assets/bootstrap/css/bootstrap.min.css">

  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.js"></script>
    <script src="https://fac-institute.com/js/charts/util.js"></script>
  <style>
    canvas{
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
    }
    </style>
</head>
<body>
<?php include_once $data->basedir.'view/laporan/vtopnav.aem.php'; ?>
<div class="container" style="padding-bottom:40px;">

        <div class="row">
            <div class="page-header">
                <h2>Rekapitulasi Penjualan <?= $data->marketing ?></h2>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table>
                    <tbody>
                        <tr>
                            <td>Tahun</td><td>:</td><td><?= $data->tahun ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div style="width:75%;">
                    <canvas id="canvas"></canvas>
                </div> 
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <form action="" method="post">
                <div class="form-group">
                    <label for="tahun">Tahun</label>
                    <input type="text" name="thn" class="form-control" style="width:75%;" placeholder="contoh: 2017">
                </div>
                <button class="btn btn-lg btn-primary" style="width:75%;">Submit</button>
                </form>
            </div>
        </div>

<!--End Container-->
</div>

<script src="<?= $data->base_url ?>js/jquery-1.11.1.js"></script>
<script src="<?= $data->base_url ?>assets/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript" src='https://fac-institute.com/js/jquery-ui/jquery-ui.js'></script>
    <script>
        var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var config = {
            type: 'line',
            data: {
                labels: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli","Agustus","September","Oktober","November","Desember"],
                datasets: [{
                    label: "Omset",
                    backgroundColor: window.chartColors.red,
                    borderColor: window.chartColors.red,
                    data: [
                        <?= $data->omset_jan ?>,
                        <?= $data->omset_feb ?>,
                        <?= $data->omset_mar ?>,
                        <?= $data->omset_apr ?>,
                        <?= $data->omset_mei ?>,
                        <?= $data->omset_jun ?>,
                        <?= $data->omset_jul ?>,
                        <?= $data->omset_agu ?>,
                        <?= $data->omset_sep ?>,
                        <?= $data->omset_okt ?>,
                        <?= $data->omset_nov ?>,
                        <?= $data->omset_des ?>
                    ],
                    fill: false,
                }, {
                    label: "Terbayar",
                    fill: false,
                    backgroundColor: window.chartColors.blue,
                    borderColor: window.chartColors.blue,
                    data: [
                        <?= $data->net_jan ?>,
                        <?= $data->net_feb ?>,
                        <?= $data->net_mar ?>,
                        <?= $data->net_apr ?>,
                        <?= $data->net_mei ?>,
                        <?= $data->net_jun ?>,
                        <?= $data->net_jul ?>,
                        <?= $data->net_agu ?>,
                        <?= $data->net_sep ?>,
                        <?= $data->net_okt ?>,
                        <?= $data->net_nov ?>,
                        <?= $data->net_des ?>
                    ],
                }]
            },
            options: {
                responsive: true,
                title:{
                    display:true,
                    text:'Rekapitulasi Penjualan'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Bulan'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Jumlah'
                        }
                    }]
                }
            }
        };

        window.onload = function() {
            var ctx = document.getElementById("canvas").getContext("2d");
            window.myLine = new Chart(ctx, config);
        };

        

        var colorNames = Object.keys(window.chartColors);
        
    </script>
</body>
</html>

