<?php 
include_once 'controller/adminexmarketing.controller.php';
session_start();
if (isset($_SERVER['PATH_INFO'])) {
	@$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
	$key = array_shift($url_segment);
	switch ($key) {
		case 'dasbor':			
			AdminExMarketing::dasbor();
			break;
		case 'logout':
			AdminExMarketing::logout();
			break;
		case 'personal':
			AdminExMarketing::personal(@$_SESSION['aem_admin']['cabang'],@$url_segment[0]);
			break;
		case 'detailtrans':
			AdminExMarketing::DetailTransaksi(@$url_segment[0],@$_SESSION['aem_admin']['cabang']);
			break;
		case '':
			AdminExMarketing::login();
			break;
		default:
			AdminExMarketing::login();
			break;
	}
	
}else{
	AdminExMarketing::login();
}

?>