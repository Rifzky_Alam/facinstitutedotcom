<?php 
include_once 'library/Aemdv.php';
class AdminExMarketing{
    public static function dasbor(){
    	$data = new Aemdv();
		$data->Model('Querybuilder');
		$data->Model('external');
		$data->Lib('sessionz');
		ISession::OnlyAdmin($data->base_url.'external/marketing/admin/');
		$db = new QueryBuilder();
		$ex = new External();
		$data->judul='FAC-External || Laporan Marketing';
		
        if (isset($_GET['in'])) {
            $data->listmarketing = $data->CastObject($ex->DasborAsOfDate($_SESSION['aem_admin']['cabang'],$_GET['in']['fd'],$_GET['in']['td']));
            $data->subtitle='Laporan Marketing || '.$_GET['in']['fd'].' Sampai '.$_GET['in']['td'];
        }else{
            $data->subtitle='Laporan Marketing';
            $data->listmarketing = $data->CastObject($ex->Dasbor($_SESSION['aem_admin']['cabang']));
        }

		
		// print_r($data->listmarketing);
		$data->View('laporan/vdasborlap.aem',$data);
    }

    public static function personal($idcabang,$idmarketing){
    	// echo $idcabang.' ',$idmarketing;
    	$data = new Aemdv();
    	$data->Model('Querybuilder');
    	$data->Lib('sessionz');
    	ISession::OnlyAdmin($data->base_url.'external/marketing/admin/');
    	$data->Model('Marketing');
        $data->Model('Invoice');
        $marketing = new Marketing();
        $invoice = new Invoice();

        $data->judul = "External - Sales Report";
        $data->subtitle="Laporan Sales";

        if (!empty($idmarketing)) {
            $marketing->setID($idmarketing);
            $datamarketing = $marketing->FetchByID();
    
            @$data->username=$_SESSION['admin']['nama'];
            $data->marketing = $datamarketing->marketing_nama;
            $data->tahun = date('Y');

            if (isset($_POST['thn'])) {
                $data->tahun = $_POST['thn'];   
            }   
            
            $data->omset_jan = $invoice->CariUntung('omset',$idmarketing,'1',$data->tahun);
            $data->omset_feb = $invoice->CariUntung('omset',$idmarketing,'2',$data->tahun);
            $data->omset_mar = $invoice->CariUntung('omset',$idmarketing,'3',$data->tahun);
            $data->omset_apr = $invoice->CariUntung('omset',$idmarketing,'4',$data->tahun);
            $data->omset_mei = $invoice->CariUntung('omset',$idmarketing,'5',$data->tahun);
            $data->omset_jun = $invoice->CariUntung('omset',$idmarketing,'6',$data->tahun);
            $data->omset_jul = $invoice->CariUntung('omset',$idmarketing,'7',$data->tahun);
            $data->omset_agu = $invoice->CariUntung('omset',$idmarketing,'8',$data->tahun);
            $data->omset_sep = $invoice->CariUntung('omset',$idmarketing,'9',$data->tahun);
            $data->omset_okt = $invoice->CariUntung('omset',$idmarketing,'10',$data->tahun);
            $data->omset_nov = $invoice->CariUntung('omset',$idmarketing,'11',$data->tahun);
            $data->omset_des = $invoice->CariUntung('omset',$idmarketing,'12',$data->tahun);

            $data->net_jan = $invoice->CariUntung('net',$idmarketing,'1',$data->tahun);
            $data->net_feb = $invoice->CariUntung('net',$idmarketing,'2',$data->tahun);
            $data->net_mar = $invoice->CariUntung('net',$idmarketing,'3',$data->tahun);
            $data->net_apr = $invoice->CariUntung('net',$idmarketing,'4',$data->tahun);
            $data->net_mei = $invoice->CariUntung('net',$idmarketing,'5',$data->tahun);
            $data->net_jun = $invoice->CariUntung('net',$idmarketing,'6',$data->tahun);
            $data->net_jul = $invoice->CariUntung('net',$idmarketing,'7',$data->tahun);
            $data->net_agu = $invoice->CariUntung('net',$idmarketing,'8',$data->tahun);
            $data->net_sep = $invoice->CariUntung('net',$idmarketing,'9',$data->tahun);
            $data->net_okt = $invoice->CariUntung('net',$idmarketing,'10',$data->tahun);
            $data->net_nov = $invoice->CariUntung('net',$idmarketing,'11',$data->tahun);
            $data->net_des = $invoice->CariUntung('net',$idmarketing,'12',$data->tahun);
            $data->View('laporan/vpersonal.aem',$data);
        }
    }

    public static function DetailTransaksi($idmarketing,$cabang){
        $data = new Aemdv();
        $data->Model('Querybuilder');
        $data->Lib('sessionz');
        $db = new QueryBuilder();
        $db->setOrder('nama_cust','ASC');
        $data->listdata = $db->FetchJoinWhere(
            ['p.nama AS nama_usaha','nama_cust','email_cust','telp_cust'],
            'new_customer AS nc',array(
                $db->getJoin('INNER','perusahaan AS p','p.id','nc.id_perusahaan'),
                $db->getJoin('INNER','new_marketing AS m','m.marketing_id','nc.marketing_id'),
            ),array(
                $db->GetCond('m.marketing_id','=',$idmarketing),
                $db->GetCond('m.marketing_cabang','=',$cabang)
            )
            
        );
        $data->judul='FAC-External || Laporan Marketing';
        $data->subtitle = 'Detail Transaksi';

        $data->View('laporan/vdetailtrans.aem',$data);
    }

    public static function login(){
    	$data = new Aemdv();
    	$data->Model('Querybuilder');
    	$data->Lib('sessionz');
    	$db = new QueryBuilder();
    	if (isset($_POST['in'])) {
    		$user = $db->FetchWhere(['username','nama_lengkap','eu_tipe','cabang'],'external_users',
    			array($db->GetCond('username','=',$_POST['in']['username']),
    				  $db->GetCond('password','=',$data->Hash($_POST['in']['password'])),
                      $db->GetCond('eu_tipe','=','1')
                    )
    		);
    		if (count($user)=='1') {
    			$_SESSION['aem_admin']=[
                    'username' => $user[0]['username'],
                    'nama' => $user[0]['nama_lengkap'],
                    'tipe' => $user[0]['eu_tipe'],
                    'cabang' => $user[0]['cabang']
                ];
    			header('location:'.$data->base_url.'external/marketing/admin/dasbor');
    		}else{
    			echo "<script>alert('Maaf Anda tidak terdaftar di sistem kami!');</script>";
    		}
    	}

    	//if somebody has already signed in
    	if (ISession::Check()) {
            // echo $_SESSION['aem_admin']['username'];
    		if($db->HasRow('username','external_users',array($db->GetCond('username','=',$_SESSION['aem_admin']['username'])))){
    			// echo $db->HasRow('username','external_users',array($db->GetCond('username','=',$_SESSION['aem_admin']['username'])));
                header('location:'.$data->base_url.'external/marketing/admin/dasbor');
    		}
    	}
    	
    	$data->View('login/vlogin.aem',$data);
    }

    public static function logout(){
    	$data = new Aemdv();
    	session_unset($_SESSION['aem_admin']);
    	header('location:'.$data->base_url.'external/marketing/admin');
    }

}

?>