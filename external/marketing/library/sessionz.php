<?php 
class ISession{
    public static function Check(){
    	if (isset($_SESSION['aem_admin']['username'])&&!empty($_SESSION['aem_admin']['username'])) {
    		return true;
    	}else {
    		return false;
    	}
    }

    public static function OnlyAdmin($base_url){
    	if (!isset($_SESSION['aem_admin']['username'])) {
    		header('Location:'.$base_url.'logout');
    	}
    }
}

?>