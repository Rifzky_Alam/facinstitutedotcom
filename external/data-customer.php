<?php 
session_start();

?>

<!DOCTYPE html>
<html>
<head>
    <title>FAC-Institute -- External -- Data Perusahaan</title>
    <?php $page='dataCustomer' ?>
    <?php include_once 'view-support/header.php'; ?>
    <?php MetaTag($page) ?>
    <?php Links($page) ?>
    <?php Scripts($page) ?>
    <?php Styles($page) ?>
    
</head>
<body>
<?php 
include_once '../model/external.php';
include_once 'view-support/sessions.php';
$sesi = new Sessionz();
$sesi->dashboard('');

$ex = new External();
$customer = array('user' => $_SESSION['external']['username']);
$customer = (object) $customer;
$ex->setCustomer($customer);

$datas = $ex->fetchCustomer('0','30');
?>

<?php include_once 'view-support/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <?php include_once 'view-support/sidebar.php'; ?>
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h3>Data Customer</h3>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <a href="input-customer" class="btn btn-sm btn-success">Add new</a>    
                </div>
                
            </div>

            

            <form action="" method="post">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-hover">
                        <thead>
                            <th>Nama Customer</th>
                            <th>Nama Usaha</th>
                            <th>Telepon</th>
                            <th>Email</th>
                            <th>Paket Training</th>
                            <th>Agenda</th>
                            <th>Aksi</th>
                        </thead>
                        <tbody>        
                            <?php foreach ($datas as $key) { ?>
                            <tr class="success">
                                <td><?php echo $key->nama ?></td>
                                <td><?php echo $key->nama_usaha ?></td>
                                <td><?php echo $key->telepon ?></td>
                                <td><?php echo $key->email ?></td>
                                <td><?php echo $key->nama_item ?></td>
                                <td><?php echo $key->agenda_training ?></td>
                                <td><a href="<?php echo 'input-penawaran?ic='.$key->id ?>">Penawaran(FAC)</a> || <a href="<?php echo 'input-quotation?ic='.$key->id ?>">Penawaran(UTS)</a> || <a href="<?php echo 'edit-customer?id='.$key->id ?>">edit</a></td>
                            </tr>                            
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>

</body>
</html>