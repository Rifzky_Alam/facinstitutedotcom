<?php 
/*
- penawaran harus di buat setelah data customer dan perusahaan lengkap di isi
- get data customer untuk penawaran wajib untuk membuka page ini.

*/
session_start();
include_once 'view-support/sessions.php';
$sesi = new Sessionz();
$sesi->inputPenawaran($_SESSION['external']['username']);
?>
<!DOCTYPE html>
<html>
<head>
    <title>FAC-Institute -- External-Quotation</title>
    <?php $page='inputPenawaran' ?>
    <?php include_once 'view-support/header.php'; ?>
    <?php MetaTag($page) ?>
    <?php Links($page) ?>
    <?php Scripts($page) ?>
    <?php Styles($page) ?>
    
</head>
<body>
<?php 
include_once '../model/external.php';
$ex = new External();

?>

<?php include_once 'view-support/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <?php include_once 'view-support/sidebar.php'; ?>
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1>FAC-Institute ::  Input Quotation</h1>
            </div>
            <?php if (isset($_GET['ic'])&&!empty($_GET['ic'])): ?>
                <?php $ex->setCustomer($_GET['ic']) ?>
                <?php $datac = $ex->fetchCustomerByID(); ?>

            <div class="row">
                <?php 


                if (isset($_POST['in'])) {
                 
                    $quotation = array(
                        'idcustomer' => $datac->id,
                        'subjek' => '-', 
                        'title' => '-',
                        'notes' => $ex->getValue($_POST['in']['notes']),
                        'cece' => $ex->getValue($_POST['in'])['emailcc'],
                        'attach' => $ex->getArrayValue($_POST['in']['lampiran']),
                        'status' => '10',
                        'user' => $_SESSION['external']['username']
                    );
                    $quotation = (object) $quotation;
                    $ex->setQuotation($quotation);

                    if ($ex->inputQuotation()) {
                        echo "<script>alert('Data berhasil disimpan!');</script>";
                        echo "<script>location.replace('data-quotation');</script>";
                    } else {
                        echo "<script>alert('Data gagal disimpan');</script>";
                    }
                    

                }

                ?>
            </div>
            
                <?php //user yang bukan haknya tidak dapat melihat data yang di masukkan oleh pengguna lain ?>
                <?php 
                    if ($datac==''||empty($datac)) {
                        echo "<script>alert('data tidak ditemukan, harap untuk memilih ulang customer!');</script>";
                        echo "<script>location.replace('data-customer');</script>";
                    }
                ?>


            <form action="" method="post">
            <div class="row">
                <div class="col-md-12">
                    
                    <div class="form-group">
                        <label>Kepada:</label>
                        <h4><a href="#"><?php echo $datac->nama .' -- '. $datac->nama_usaha ?></a></h4>
                    </div>
                    <br>
                    
                    <div class="form-group">
                        <label>Notes</label>
                        <input type="text" name="in[notes]" class="form-control" maxlength="500" placeholder="pisahkan dengan tanda ## contoh: note1##note2##note3">
                    </div>

                    <div class="form-group">
                        <label>Email CC</label>
                        <input type="text" name="in[emailcc]" id="emailc" class="form-control" placeholder="Email Customer">
                    </div>

                    <h5><strong>Attachments</strong></h5>
                    <div class="col-md-6">
                        <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#atchonline">
                            Online
                        </button>
                          <div id="atchonline" class="collapse">
                            <div class="form-group">
                                <?php 
                                    $files = scandir('attachments/on');
                                    for ($i=0; $i < count($files); $i++) { 
                                        if ($files[$i]!='.'&&$files[$i]!='..') {
                                        
                                            echo "<div class='checkbox'><label><input type='checkbox' name='in[lampiran][]' value='on/".$files[$i]."'>".
                                            $files[$i].
                                            "</label></div>";              
                                        }
                                    }
                                ?> 
                            </div>
                          </div>
                    </div>
                    <div class="col-md-6">
                        <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#atchoffline">
                            Offline
                        </button>
                          <div id="atchoffline" class="collapse">
                            <div class="form-group">
                                <?php 
                                    $files = scandir('attachments/off');
                                    for ($i=0; $i < count($files); $i++) { 
                                        if ($files[$i]!='.'&&$files[$i]!='..') {
                                        
                                            echo "<div class='checkbox'><label><input type='checkbox' name='in[lampiran][]' value='off/".$files[$i]."'>".
                                            $files[$i].
                                            "</label></div>";              
                                        }
                                    }
                                ?> 
                            </div>
                          </div>
                    </div>
                    
                    <button class="btn btn-lg btn-primary" style="width:100%;margin-top:15px;">Submit</button>
                </div>    
            </div>
            </form>
            <?php else: ?>

                <script type="text/javascript">alert('harap untuk memilih customer terlebih dahulu di tabel customer anda!');</script>
                <script type="text/javascript">location.replace('data-customer');</script>
            <?php endif ?>

        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo getBaseUrl() ?>assets/jqnumber/jquery.number.js"></script>
<script type="text/javascript" src="<?php echo getBaseUrl() ?>assets/timepicker/js/timepicki.js"></script>
<script>
    $('#timepicker1').timepicki();
    $('#timepicker2').timepicki();
    $('#transport').number( true, 0 );
</script>
<?php include_once 'view-support/date-script.php'; ?>
<?php DateScript($page) ?>

</body>
</html>