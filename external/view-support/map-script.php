<script type="text/javascript">
var latitude;
var longitude

function getLocation(){
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else { 
        alert("Geolocation is not supported by this browser.");
    }
}

function showPosition(position) {
     var myCurrentLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
     return myCurrentLocation;
}

function cobaaa() {
    getLocation();

    return [position.coords.latitude,position.coords.longitude];
}

function showError(error){
    switch(error.code){
        case error.PERMISSION_DENIED:
            //alert("User denied the request for Geolocation.");
            
            var myCurrentLocation = new google.maps.LatLng(-6.248123, 106.907952);
            var mapProp = {
                center:myCurrentLocation,
                zoom:11,
                mapTypeId:google.maps.MapTypeId.ROADMAP
            };

            var input = document.getElementById('alamatz');
            input.value = "";
            var sBox = new google.maps.places.SearchBox(input);
                //marker
            var myMarker=new google.maps.Marker({
                position:myCurrentLocation,
            });


    
            var markers = [];

            markers.push(myMarker);



            var map = new google.maps.Map(document.getElementById("map-canvas"),mapProp);
            var infowindow = new google.maps.InfoWindow({
                content:"FAC Institute Office"
            });
            infowindow.open(map,myMarker);

            myMarker.setMap(map);

            //event listener 

            //event listener for search box
            sBox.addListener('places_changed', function() {
              var places = sBox.getPlaces();
              if (places.length == 0) {
                return;
              };
              deleteMarkers();

              // For each place, get the icon, name and location.
              var bounds = new google.maps.LatLngBounds();
              places.forEach(function(place) {
                var icon = {
                  url: place.icon,
                  size: new google.maps.Size(71, 71),
                  origin: new google.maps.Point(0, 0),
                  anchor: new google.maps.Point(17, 34),
                  scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                markers.push(new google.maps.Marker({
                  map: map,
                  //icon: icon,
                  title: place.name,
                  position: place.geometry.location
                }));

                if (place.geometry.viewport) {
                  // Only geocodes have viewport.
                  bounds.union(place.geometry.viewport);
                } else {
                  bounds.extend(place.geometry.location);
                }
              });
              map.fitBounds(bounds);

            });

            //event listener for google map
            google.maps.event.addListener(map, 'click', function(event) {
            placeMarker(event.latLng);
            });


            //marker

            // Sets the map on all markers in the array.
            function setMapOnAll(map) {
              for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
              }
            }

            function placeMarker(location){
                deleteMarkers();
                var alamatku = document.getElementById('alamatz');
                var myMarker = new google.maps.Marker({
                    position: location, 
                    map: map
                });
                alamatku.value = location;
                var myString = alamatku.value;
                myString=myString.replace('(','');myString=myString.replace(')','');myString=myString.replace(', ',',');

                alamatku.value = myString;
                markers.push(myMarker);
                //alert(markers.length);
            }
            // Deletes all markers in the array by removing references to them.
            function deleteMarkers() {
              clearMarkers();
              markers = [];
            }

            function clearMarkers() {
              setMapOnAll(null);
            }



            break;
        case error.POSITION_UNAVAILABLE:
            //x.innerHTML = "Location information is unavailable."
            alert('Location information is unavailable.');
            break;
        case error.TIMEOUT:
            alert('The request to get user location timed out.');
            break;
        case error.UNKNOWN_ERROR:
            //x.innerHTML = "An unknown error occurred."
            alert('An unknown error occurred.');
            break;
    }
}


function initialize() {



    if (navigator.geolocation) {


        navigator.geolocation.getCurrentPosition(function(position){
            var myCurrentLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
            var mapProp = {
                center:myCurrentLocation,
                zoom:16,
                mapTypeId:google.maps.MapTypeId.ROADMAP
            };

            var input = document.getElementById('alamatz');
            input.value = position.coords.latitude + ',' + position.coords.longitude;
            var sBox = new google.maps.places.SearchBox(input);
                //marker
            var myMarker=new google.maps.Marker({
                position:myCurrentLocation,
            });


    
            var markers = [];

            markers.push(myMarker);



            var map = new google.maps.Map(document.getElementById("map-canvas"),mapProp);
            var infowindow = new google.maps.InfoWindow({
                content:"Anda berada disini"
            });
            infowindow.open(map,myMarker);

            myMarker.setMap(map);

            //event listener 

            //event listener for search box
            sBox.addListener('places_changed', function() {
              var places = sBox.getPlaces();
              if (places.length == 0) {
                return;
              }
              deleteMarkers()

              // For each place, get the icon, name and location.
              var bounds = new google.maps.LatLngBounds();
              places.forEach(function(place) {
                var icon = {
                  url: place.icon,
                  size: new google.maps.Size(71, 71),
                  origin: new google.maps.Point(0, 0),
                  anchor: new google.maps.Point(17, 34),
                  scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                markers.push(new google.maps.Marker({
                  map: map,
                  //icon: icon,
                  title: place.name,
                  position: place.geometry.location
                }));

                if (place.geometry.viewport) {
                  // Only geocodes have viewport.
                  bounds.union(place.geometry.viewport);
                } else {
                  bounds.extend(place.geometry.location);
                }
              });
              map.fitBounds(bounds);

            });

            //event listener for google map
            google.maps.event.addListener(map, 'click', function(event) {
            placeMarker(event.latLng);
            });


            //marker

            // Sets the map on all markers in the array.
            function setMapOnAll(map) {
              for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
              }
            }

            function placeMarker(location){
                deleteMarkers();
                var alamatku = document.getElementById('alamatz');
                var myMarker = new google.maps.Marker({
                    position: location, 
                    map: map
                });
                alamatku.value = location;
                var myString = alamatku.value;
                myString=myString.replace('(','');myString=myString.replace(')','');myString=myString.replace(', ',',');

                alamatku.value = myString;
                markers.push(myMarker);
                //alert(markers.length);
            }
            // Deletes all markers in the array by removing references to them.
            function deleteMarkers() {
              clearMarkers();
              markers = [];
            }

            function clearMarkers() {
              setMapOnAll(null);
            }

            //end marker

      },showError); //end function

    } else { 
        alert("Geolocation is not supported by this browser.");
    }
}
    google.maps.event.addDomListener(window, 'load', initialize);

</script>