<?php include_once '../baseurl.php'; ?>

<?php function MetaTag($value){ ?>
	
<meta name="viewport" content="width=device-width, initial-scale=1">
	
<?php } ?>

<?php function Links($value){ ?>

	<?php if ($value=='inputJadwal'||$value=='inputCustomer'): ?>
			<link rel="stylesheet" type="text/css" href='<?php echo getBaseUrl() ?>css/bootstrap.css'>
			<link rel="stylesheet" type="text/css" href=<?php echo "'".getBaseUrl()."js/jquery-datepicker/jquery-ui.min.css'"; ?>>
			<link rel="stylesheet" type="text/css" href='<?php echo getBaseUrl() ?>assets/timepicker/css/style.css'>
			<link rel="stylesheet" type="text/css" href='<?php echo getBaseUrl() ?>assets/timepicker/css/timepicki.css'>
		
		<?php else: ?>
			<link rel="stylesheet" type="text/css" href='<?php echo getBaseUrl() ?>css/bootstrap.css'>
	<?php endif ?>
	
<?php } ?>

<?php function Scripts($value){ ?>
	<script type="text/javascript" src='<?php echo getBaseUrl() ?>js/jquery.js'></script>
	<script type="text/javascript" src='<?php echo getBaseUrl() ?>js/bootstrap.min.js'></script>
	<?php if ($value=='inputLokasi'): ?>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCUn6Q6sg0JVJBlgiz4tL2kF1zmVoC0ScE&libraries=places&sensor=true"></script>
	<?php endif ?>
<?php } ?>

<?php function Styles($value){ ?>
	<style type="text/css">
	.glyphicon { margin-right:10px; }
	.panel-bodyz { padding:0px; }
	.panel-bodyz table tr td { padding-left: 15px }
	.panel-bodyz .table {margin-bottom: 0px; }
	</style>
<?php } ?>