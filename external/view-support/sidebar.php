<?php $package="external"; ?>

<div class="col-sm-3 col-md-3">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-folder-close">
                            </span>Content</a>
                        </h4>
                    </div>
                    <?php if (preg_match("/input/i", $page)): ?>
                        <div id="collapseOne" class="panel-collapse collapse in">    
                        <?php else: ?>
                        <div id="collapseOne" class="panel-collapse collapse">
                    <?php endif ?>
                    
                        <div class="panel-bodyz">
                            <table class="table">
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-flash text-success"></span><a href="<?php echo getBaseUrl().$package.'/' ?>">Dashboard</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-flash text-success"></span><a href="<?php echo getBaseUrl().$package.'/input-perusahaan' ?>">Input data perusahaan</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-flash text-success"></span><a href="<?php echo getBaseUrl().$package.'/input-customer' ?>">Input data customer</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-flash text-success"></span><a href="<?php echo getBaseUrl().$package.'/input-penawaran' ?>">Input Penawaran</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><span class="glyphicon glyphicon-user">
                            </span><?php echo $_SESSION['external']['nama'] ?></a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-bodyz">
                            <table class="table">
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-bullhorn text-primary"></span><a href="ubah-password">Ubah Password</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-bullhorn text-primary"></span><a href="<?php echo getBaseUrl().$package.'/logout' ?>">Log-Out</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><span class="glyphicon glyphicon-file">
                            </span>Data</a>
                        </h4>
                    </div>
                    <?php if (preg_match("/data/i", $page)): ?>
                            <div id="collapseFour" class="panel-collapse collapse in">
                        <?php else: ?>
                            <div id="collapseFour" class="panel-collapse collapse">
                    <?php endif ?>
                    
                        <div class="panel-bodyz">
                            <table class="table">
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-usd"></span>
                                        <a href="<?php echo getBaseUrl().$package.'/data-perusahaan' ?>">Data Perusahaan</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-user"></span>
                                        <a href="<?php echo getBaseUrl().$package.'/data-customer' ?>">Data Customer</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-user"></span>
                                        <a href="<?php echo getBaseUrl().$package.'/data-quotation' ?>">Data Penawaran</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>