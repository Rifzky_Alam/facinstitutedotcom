<?php 
session_start();

?>

<!DOCTYPE html>
<html>
<head>
    <title>FAC-Institute -- External -- Data Perusahaan</title>
    <?php $page='customer' ?>
    <?php include_once 'view-support/header.php'; ?>
    <?php MetaTag($page) ?>
    <?php Links($page) ?>
    <?php Scripts($page) ?>
    <?php Styles($page) ?>
    
</head>
<body>
<?php 
include_once '../model/external.php';
$ex = new External();
if (!isset($_GET['id'])||empty($_GET['id'])) {
    echo "<script>alert('Data customer tidak ada!');</script>";
    echo "<script>location.replace('data-customer');</script>";
}
?>

<?php include_once 'view-support/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <?php include_once 'view-support/sidebar.php'; ?>
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h3>Data Customer</h3>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <a href="input-customer" class="btn btn-sm btn-success">Add transaction</a>    
                </div>
                
            </div>

            

            <form action="" method="post">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-hover">
                        <thead>
                            <th>Transaksi</th>
                            <th>Paket</th>
                            <th>Tanggal</th>
                            <th>Agenda</th>
                            <th>Aksi</th>
                        </thead>
                        <tbody>        
                            
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>

</body>
</html>