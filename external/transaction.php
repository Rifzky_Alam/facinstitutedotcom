<?php 
session_start();

?>
<!DOCTYPE html>
<html>
<head>
    <title>FAC-Institute -- External-Customer</title>
    <?php $page='inputCustomer' ?>
    <?php include_once 'view-support/header.php'; ?>
    <?php MetaTag($page) ?>
    <?php Links($page) ?>
    <?php Scripts($page) ?>
    <?php Styles($page) ?>
    
</head>
<body>
<?php 
include_once '../model/external.php';
$ex = new External();
$ex->setUsername($_SESSION['admin']['username']);
// $nama_kursus = json_decode($kursus->getDatas('nama_kursus'));
// $tempat_kursus =  json_decode($kursus->getDatas('tempat_kursus'));
?>

<?php include_once 'view-support/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <?php include_once 'view-support/sidebar.php'; ?>
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1>FAC-Institute ::  Input Data Customer</h1>
            </div>

            <div class="row">
                <?php
                $dataUsaha= $ex->fetchPerusahaanByUsername();
                // print_r($dataUsaha);
                // echo $ex->getUsername();
                if (isset($_POST['in'])) {
                    $transaction = array(
                        'usaha' => $ex->getValue($_POST['in']['usaha']),
                        'nama' => $ex->getValue($_POST['in']['nama']),
                        'email' => $ex->getValue($_POST['in']['email']),
                        'jabatan' => $ex->getValue($_POST['in']['jabatan']),
                        'telepon' => $ex->getValue($_POST['in']['telepon']),
                        'tanggaltraining' => $ex->getValue($_POST['in']['tanggalPelaksanaan']),
                        'paket' => $ex->getValue($_POST['in']['pakettraining']),
                        'transport' => $ex->getIntegerValue($_POST['in']['transport']),
                        'agenda' => $ex->getValue($_POST['in']['agenda']), 
                        'jenispengguna' => $ex->getValue($_POST['in']['jenispengguna']),
                        'user' => $ex->getValue($_SESSION['admin']['username'])
                    );
                    $transaction = (object) $transaction;
                    $ex->setTransaction($transaction);
                    // print_r($ex->getCustomer());
                    if ($ex->inputCustomer()) {
                        echo "<script>alert('Data berhasil disimpan!');</script>";
                        echo "<script>location.replace('data-customer');</script>";
                    } else {
                        echo "<script>alert('Data gagal disimpan');</script>";
                    }
                    

                }

                ?>
            </div>

            <form action="" method="post">
            <div class="row">
                <div class="col-md-12">

                    <div class="form-group">
                        <label>Tanggal Training</label>
                        <div class="form-inline" id="jajal">                        
                            <input type="text" id="tgl-pelaksanaan" name='in[tanggalPelaksanaan][]' class="form-control tglPelaksanaan" placeholder="klik disini untuk memilih tanggal"/>   
                            <span class='btn btn-sm btn-primary' id='tambahTombol'>Tambah Tanggal</span>
                        </div>
                    </div>

                    <?php $datapaket = $ex->fetchPaketTraining(); ?>
                    <div class="form-group">
                        <label>Paket Training</label>
                        <select name="in[pakettraining]" class="form-control">
                            <option value="">--pilih paket training--</option>
                            <?php foreach ($datapaket as $key ) { ?>
                                <option value="<?php echo $key->id ?>"><?php echo $key->nama_item ?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Biaya Transport</label>
                        <input type="text" name="in[transport]" class="form-control" maxlength="100">
                    </div>

                    <div class="form-group">
                        <label>Agenda Training</label>
                        <textarea class="form-control" name="in[agenda]" placeholder="Apa yang di butuhkan untuk training"></textarea>
                    </div>
                    <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>                   
                </div>    
            </div>
            </form>

        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo getBaseUrl() ?>assets/jqnumber/jquery.number.js"></script>
<script type="text/javascript" src="<?php echo getBaseUrl() ?>assets/timepicker/js/timepicki.js"></script>
<script>
    $('#timepicker1').timepicki();
    $('#timepicker2').timepicki();
    $('#txtInvestasi').number( true, 0 );
</script>
<?php include_once 'view-support/date-script.php'; ?>
<?php DateScript($page) ?>

</body>
</html>