<?php 
session_start();

?>

<!DOCTYPE html>
<html>
<head>
    <title>FAC-Institute -- External -- Data Perusahaan</title>
    <?php $page='ubahPassword' ?>
    <?php include_once 'view-support/header.php'; ?>
    <?php MetaTag($page) ?>
    <?php Links($page) ?>
    <?php Scripts($page) ?>
    <?php Styles($page) ?>
    
</head>
<body>
<?php 
include_once '../model/external.php';
$ex = new External();
$customer = array('user' => $_SESSION['external']['username']);
$customer = (object) $customer;



if (isset($_POST['in'])) {
    if ($_POST['in']['konfirmasi']!=$_POST['in']['passwordbaru']) {
        echo "<script>alert('Konfirmasi password tidak sesuai!');</script>";
    } else {
        if ($ex->getToken($_POST['in']['passwordlama'])!=$_SESSION['external']['token']) {
            echo "<script>alert('Password lama tidak sesuai!');</script>";
        } else {
            $ex->setUsername($_SESSION['external']['username']);
            $ex->setPassword($_POST['in']['passwordbaru']);
            if ($ex->ubahPassword()) {
                echo "<script>alert('Password telah diubah');</script>";         
            } else {
                echo "<script>alert('Terjadi kesalahan, hubungi admin kami!');</script>";
            }
                      
        }
        
    }
    
}


?>
<?php include_once 'view-support/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <?php include_once 'view-support/sidebar.php'; ?>
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h3>Ubah Password</h3>
            </div>
            <form action="" method="post">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Password Lama</label>
                        <input type="password" class="form-control" name="in[passwordlama]">
                    </div>
                    <div class="form-group">
                        <label>Password Baru</label>
                        <input class="form-control" type="password" name="in[passwordbaru]">
                    </div>
                    <div class="form-group">
                        <label>Konfirmasi Password baru</label>
                        <input type="password" class="form-control" name="in[konfirmasi]">
                    </div>
                    <button style="width:100%" class="btn btn-primary">Submit</button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>

</body>
</html>