<?php 
session_start();
include_once 'view-support/sessions.php';
$sesi = new Sessionz();
$sesi->inputPerusahaan($_SESSION['external']['username']);
?>

<!DOCTYPE html>
<html>
<head>
    <title>FAC-Institute -- External-Customer</title>
    <?php $page='editPerusahaan' ?>
    <?php include_once 'view-support/header.php'; ?>
    <?php MetaTag($page) ?>
    <?php Links($page) ?>
    <?php Scripts($page) ?>
    <?php Styles($page) ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCUn6Q6sg0JVJBlgiz4tL2kF1zmVoC0ScE&libraries=places&sensor=true"></script>
</head>
<body>
<?php 
include_once '../model/external.php';
$ex = new External();
$dataJenisUsaha = $ex->fetchJenisUsaha();
?>
<?php include_once '../administrasi/map-script.php'; ?>
<?php include_once 'view-support/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <?php include_once 'view-support/sidebar.php'; ?>
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h1>FAC-Institute ::  Edit Data Perusahaan</h1>
            </div>
            <?php if (isset($_GET['id'])&&!empty($_GET['id'])): ?>
                <?php $ex->setPerusahaan($_GET['id']); ?>
                <?php $data = $ex->fetchDataPerusahaanByID() ?>
            
            <div class="row">
                <?php 
                if (isset($_POST['edt'])) {
                    
                    $perusahaan = array(
                        'id' => $ex->getValue($_GET['id']),
                        'nama_usaha' => $ex->getValue($_POST['edt']['np']), 
                        'email' => $ex->getValue($_POST['edt']['email']),
                        'telepon' => $ex->getValue($_POST['edt']['telepon']),
                        'alamat' => $ex->getValue($_POST['edt']['alamat']),
                        'kota' => $ex->getValue($_POST['edt']['kota']),
                        'provinsi' => $ex->getValue($_POST['edt']['provinsi']),
                        'jenisusaha' => $ex->getArrayValue2(@$_POST['edt']['jnsUsaha']),
                        'ketjenisusaha' => $ex->getValue($_POST['edt']['ketJenisUsaha']),
                        'map' => $ex->getValue($_POST['edt']['map']),
                        'username' => $ex->getValue($_SESSION['external']['username'])
                    );
                    $perusahaan = (object) $perusahaan;
                    $ex->setPerusahaan($perusahaan);
                    if ($ex->editPerusahaan()) {
                        echo "<script>alert('Data berhasil disimpan!');</script>";
                        echo "<script>location.replace('data-perusahaan');</script>";
                    } else {
                        echo "<script>alert('Data gagal disimpan');</script>";
                    }
                    
                    // echo $ex->jajalObjek();
                    // print_r($perusahaan);

                    // $input = (object) $input;

                    // echo $ex->inputJadwal($input);

                }

                ?>
            </div>

            <form action="" method="post">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Nama Perusahaan</label>
                        <input value="<?php echo @$data->nama_usaha ?>" class="form-control" type="text" name="edt[np]" placeholder="Nama perusahaan"></input>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input value="<?php echo @$data->email_usaha ?>" class="form-control" type="email" name="edt[email]" placeholder="Email Perusahaan"></input>
                    </div>

                    <div class="form-group">
                        <label>Telepon</label>
                        <input value="<?php echo @$data->telepon_usaha ?>" type="text" name="edt[telepon]" class="form-control" placeholder="Telepon kantor">
                    </div>

                    <div class="form-group">
                        <label>Alamat</label>
                        <textarea class="form-control" name="edt[alamat]"><?php echo @$data->alamat_usaha ?></textarea>
                    </div>

                    <div class="form-group">
                        <label>Kota</label>
                        <input value="<?php echo @$data->kota_usaha ?>" type="text" name="edt[kota]" class="form-control" placeholder="Kota Perusahaan berada">
                    </div>

                    <div class="form-group">
                        <label>Provinsi</label>
                        <select id='provinsi' name="edt[provinsi]" class='form-control'>
                            <option selected value="<?php echo @$data->provinsi_usaha ?>"><?php echo @$data->provinsi_usaha ?></option>
                            <option value=''>--Pilih Provinsi--</option>
                            <option value='aceh'>Nanggroe Aceh Darussalam</option>
                            <option value='sumatera utara'>Sumatera Utara</option>
                            <option value='riau'>Riau</option>
                            <option value='kepulauan riau'>Kepulauan Riau</option>
                            <option value='sumatera barat'>Sumatera Barat</option>
                            <option value='jambi'>Jambi</option>
                            <option value='bengkulu'>Bengkulu</option>
                            <option value='bangka belitung'>Bangka Belitung</option>
                            <option value='sumatera selatan'>Sumatera Selatan</option>
                            <option value='lampung'>Lampung</option>

                            <option value='banten'>Banten</option>
                            <option value='jawa barat'>Jawa Barat</option>
                            <option value='jakarta'>Jakarta</option>
                            <option value='jawa tengah'>Jawa tengah</option>
                            <option value='jogjakarta'>Jogjakarta</option>
                            <option value='jawa timur'>Jawa Timur</option>

                            <option value='bali'>Bali</option>
                            <option value='ntb'>Nusa tenggara Barat</option>
                                                                    
                            <option value='kalbar'>Kalimantan Barat</option>
                            <option value='kaltim'>Kalimantan Timur</option>
                            <option value='kalteng'>Kalimantan Tengah</option>
                            <option value='kalsel'>Kalimantan Selatan</option>
                            <option value='kalut'>Kalimantan Utara</option>

                            <option value='sulbar'>Sulawesi Barat</option>
                            <option value='sulsel'>Sulawesi Selatan</option>
                            <option value='sulteng'>Sulawesi Tengah</option>
                            <option value='sultra'>Sulawesi Tenggara</option>
                            <option value='gorontalo'>Gorontalo</option>
                            <option value='sulut'>Sulawesi Utara</option>

                            <option value='maluku utara'>Maluku Utara</option>
                            <option value='maluku'>Maluku</option>

                            <option value='papua barat'>Papua Barat</option>
                            <option value='papua'>Papua</option>
                        </select>
                    </div>

                    <div class="form-group">
                    <h4>Jenis Usaha</h4>
                    <?php $jnsusaha = explode('#', $data->jenisusaha); ?>
                    <?php foreach ($dataJenisUsaha as $key){ ?>
                    <div class="checkbox">
                        <?php if (in_array($key->id, $jnsusaha)): ?>
                            <label><input name="edt[jnsUsaha][]" value="<?php echo $key->id ?>" checked type="checkbox"><?php echo $key->nama_jenis_usaha ?></label>       
                            <?php else: ?>
                            <label><input name="edt[jnsUsaha][]" value="<?php echo $key->id ?>" type="checkbox"><?php echo $key->nama_jenis_usaha ?></label>
                        <?php endif ?>
                        
                    </div>
                    <?php } ?>
                    <h5><a href="#">Lainnya? klik untuk input data lain.</a></h5>                            
                    </div>
                    
                    <br>

                    <div class='form-group'>
                        <label>Keterangan Jenis Usaha</label>
                        <textarea id="ketJenisUsaha" name="edt[ketJenisUsaha]" class="form-control"><?php echo @$data->ket_jenis_usaha ?></textarea>
                    </div>
                    
                    <div class="form-group">
                        <label for="alamat">Lokasi Map</label>
                        <input value="<?php echo @$data->map_usaha ?>" type="text" name="edt[map]" id="alamatz" class="form-control" placeholder="Cari Nama Tempat atau Tandai Peta di Bawah ini">                    
                    </div>

                    <div class="form-group">
                        <div id="map-canvas" style="width:100%;height:400px;border:1px solid">
                            <p>Untuk membuka map ikuti langkah berikut:</p>
                            <ol>
                                <li>
                                    aktifkan location pada browser anda (setting browser)     
                                </li>
                                <li>
                                    refresh page dan izinkan fac-institute.com untuk mengakses lokasi anda
                                </li>
                            </ol>
                            

                        </div>
                    </div>

                    <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>                   
                </div>    
            </div>
            </form>
            <?php endif ?>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo getBaseUrl() ?>assets/jqnumber/jquery.number.js"></script>
<script type="text/javascript" src="<?php echo getBaseUrl() ?>assets/timepicker/js/timepicki.js"></script>
<script>
    $('#timepicker1').timepicki();
    $('#timepicker2').timepicki();
    $('#txtInvestasi').number( true, 0 );
</script>
<?php include_once 'view-support/date-script.php'; ?>
<?php DateScript($page) ?>

</body>
</html>