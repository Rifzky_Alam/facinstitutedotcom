<?php 
session_start();
include_once 'view-support/sessions.php';
$page="preview";
$sesi = new Sessionz();
$sesi->dashboard('');

if (isset($_GET['qn'])&&!empty($_GET['qn'])){
	include_once '../model/Surat.php'; 
	include_once '../model/external.php';


	$ex = new External();
	$surat = new Surat();

	$ex->setQuotation($_GET['qn']);
	$q_data = $ex->getQuotationForDisplay();

	$objek = array(
		'nama_personal' => $q_data->nama,
		'subject'=> $q_data->subjek,
		'nama_usaha' => $q_data->nama_usaha,
		'produk' => $q_data->nama_item,
		'biaya' => $q_data->harga * $q_data->jumlah_hari,
		'notes' => $q_data->notes,
		'biaya_transport' => $q_data->biaya_transport * $q_data->transport_qty,
		'petugas' => $q_data->petugas
	);
	$objek = (object) $objek;
	echo $surat->quotation($objek);
}

if (isset($_GET['qid'])&&!empty($_GET['qid'])){
	include_once '../model/external.php';
	include_once '../model/Mypdf.php';
	$pdf = new Mypdf();
	$ex = new External();
	$ex->setQuotation($_GET['qid']);
	$q_data = $ex->getQuotationForDisplay();
	$datas = array(
		'title' => $q_data->title,
		'no_quotation' => $q_data->id,
		'nama_usaha' => $q_data->nama_usaha,
		'nama_personal' => $q_data->nama,
		'email' => $q_data->email,
		'subjek' => $q_data->subjek,
		'produk' => $q_data->nama_item,
		'jumlah_hari'=>$q_data->jumlah_hari,
		'harga' => $q_data->harga,
		'biaya_transport' => $q_data->biaya_transport,
		'petugas' => $q_data->petugas
	);
	$datas = (object) $datas;
	$pdf->setData($datas);
	$pdf->Penawaran('display');

}

if (isset($_GET['utsq'])&&!empty($_GET['utsq'])){
	include_once '../model/external.php';
	include_once '../model/Surat.php'; 
	// include_once '../model/Pendaftar.php'; 
	// $pendaftar = new Pendaftar();
	// $data = $pendaftar->getQuotationData($_GET['qn']);
	$surat = new Surat();
	$ex = new External();
	$ex->setQuotation($_GET['utsq']);
	$data = $ex->getQuotationForDisplay();
	$objek = array(
		'cp' => $data->nama,
		'np' => $data->nama_usaha,
		'notes' => $data->notes,
		'biaya_transport' => $data->biaya_transport,
		'petugas' => $data->petugas
	);
	$objek = (object) $objek;
	echo $surat->PenawaranUTS($objek);
}


?>