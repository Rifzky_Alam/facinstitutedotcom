<?php 
session_start();
include_once 'view-support/sessions.php';

$sesi = new Sessionz();

// $sesi->login();

?>

<!DOCTYPE html>
<html>
<head>
    <title>FAC-Institute -- External -- Data Perusahaan</title>
    <?php $page='dataPerusahaan' ?>
    <?php include_once 'view-support/header.php'; ?>
    <?php MetaTag($page) ?>
    <?php Links($page) ?>
    <?php Scripts($page) ?>
    <?php Styles($page) ?>
    
</head>
<body>
<?php 
include_once '../model/external.php';
$ex = new External();
$perusahaan = array('user' => $_SESSION['external']['username']);
$perusahaan = (object) $perusahaan;
$ex->setPerusahaan($perusahaan);

$datas = $ex->fetchRemovedPerusahaan('0','30');
?>

<?php 
    if (isset($_GET['essid'])&&!empty($_GET['essid'])) {
        $datausaha = array(
            'id' => $_GET['essid'],
            'status' => '1' 
        );
        $datausaha = (object) $datausaha;
        $ex->setPerusahaan($_GET['esrid']);
        $datarmv = $ex->fetchDataPerusahaanByID();

        if ($datarmv->usersource==$_SESSION['external']['username']) {
            $ex->setPerusahaan($datausaha);
            //edit status process
            if ($ex->editStatusQuotation()) {
                echo "<script>alert('data berhasil diubah');</script>";
            } else {
                echo "<script>alert('data gagal di ubah, hubungi admin kami!');</script>";    
            }//end edit status process
            
        } else {
            echo "<script>Anda tidak memiliki akses untuk edit data ini!</script>";
        }
    }//end edit status remove
?>


<?php include_once 'view-support/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <?php include_once 'view-support/sidebar.php'; ?>
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h3>Data Perusahaan</h3>
            </div>

            <div class="row">
                
            </div>

            

            <form action="" method="post">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-hover">
                        <thead>
                            <th>Nama Perusahaan</th>
                            <th>Jenis Perusahaan</th>
                            <th>Telepon</th>
                            <th>Email</th>
                            <th>Kota</th>
                            <th>Aksi</th>
                        </thead>
                        <tbody> 
                            <?php 
                            foreach ($datas as $key ) { ?>       
                            <tr class="success">
                                <td><?php echo $key->nama_usaha ?></td>
                                <td>-</td>
                                <td><?php echo $key->telepon_usaha ?></td>
                                <td><?php echo $key->email_usaha; ?></td>
                                <td><?php echo $key->kota_usaha ?></td>
                                <td><a href="<?php echo 'edit-perusahaan?id='.$key->id ?>" class="btn btn-sm btn-warning">edit</a> || <a href="#" class="btn btn-sm btn-danger">Rmv</a></td>
                            </tr>
                            <?php } ?>                            
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>

</body>
</html>