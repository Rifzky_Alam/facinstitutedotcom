<?php 
session_start();
include_once 'view-support/sessions.php';
$sesi = new Sessionz();
$sesi->inputPenawaran($_SESSION['external']['username']);
?>

<!DOCTYPE html>
<html>
<head>
    <title>FAC-Institute -- External -- Data Perusahaan</title>
    <?php $page='dataQuotation' ?>
    <?php include_once 'view-support/header.php'; ?>
    <?php MetaTag($page) ?>
    <?php Links($page) ?>
    <?php Scripts($page) ?>
    <?php Styles($page) ?>
    
</head>
<body>
<?php 
include_once '../model/external.php';
include_once 'view-support/sessions.php';
$sesi = new Sessionz();
$sesi->dashboard('');
$ex = new External();
$quotation = array('user' => $_SESSION['external']['username']);
$quotation = (object) $quotation;
$ex->setQuotation($quotation);
$datas = $ex->fetchQuotation('0','30');
?>

<?php include_once 'view-support/top-nav.php'; ?>
<div class="container-fluid" style="padding-top:50px;padding-bottom:50px;">
    <div class="row">
        <?php include_once 'view-support/sidebar.php'; ?>
        <div class="col-sm-9 col-md-9">
            <div class="well">
                <h3>Data Penawaran</h3>
            </div>

            <div class="row">

            </div>

            

            <form action="" method="post">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-hover">
                        <thead>
                            <th>Nama Perusahaan</th>
                            <th>Nama Customer</th>
                            <th>telepon</th>
                            <th>Paket Training</th>
                            <th>Agenda</th>
                            <th>Aksi</th>
                        </thead>
                        <tbody>        
                            <?php foreach ($datas as $key) { ?>
                            <?php if ($key->status=='0'||$key->status=='10'): ?>
                                <tr class="danger">
                                <td><?php echo $key->nama_usaha ?> <i>(Draft)</i></td>
                                <?php elseif($key->status=='1'||$key->status=='11'): ?>
                                <tr class="success">
                                <td><?php echo $key->nama_usaha ?> <i>Sent</i></td>
                                <?php else: ?>
                                <td><?php echo $key->nama_usaha ?></td>        
                            <?php endif ?>
                                <td><?php echo $key->nama ?></td>
                                <td><?php echo $key->telepon ?></td>
                                <td><?php echo $key->nama_item ?></td>
                                <td><?php echo $key->agenda_training ?></td>
                                <?php if ($key->status=='10'||$key->status=='11'): ?>
                                <td>
                                    <a href="<?php echo 'kirim-penawaran?utsq='.$key->id ?>">Kirim</a> || 
                                    <a target="_blank" href="<?php echo 'cetak-penawaran?qid='.$key->id ?>">pdf</a> || 
                                    <a target="_blank" href="<?php echo 'preview?utsq='.$key->id ?>">preview</a> || 
                                    <a href="<?php echo 'edit-quotation?id='.$key->id ?>">edit</a>
                                </td>
                                    <?php else: ?>
                                <td>
                                    <a href="<?php echo 'kirim-penawaran?id='.$key->id ?>">Kirim</a> || 
                                    <a target="_blank" href="<?php echo 'preview?qid='.$key->id ?>">pdf</a> || 
                                    <a target="_blank" href="<?php echo 'preview?qn='.$key->id ?>">preview</a> || 
                                    <a href="<?php echo 'edit-quotation?id='.$key->id ?>">edit</a>
                                </td>
                                <?php endif ?>
                            </tr>                            
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>

</body>
</html>