<?php include_once 'baseurl.php'; ?>
<html class="no-js" lang="id">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="google-site-verification" content="EfNVtXecxElbcbugY59SeKXJuMzO67bjVvTvlHCc_CM" />
    <meta name="msvalidate.01" content="D925497D8F382164FF0CE0267D381FDA" />

    <meta name="thumbnailUrl" content="https://www.fac-institute.com/administrasi/artikel/images/7cabb38e259c0d5b53f9bb219daf410f.jpg?w=650" />
    <meta name="description" content="FAC - Kami Melayani Training Software Akuntansi Accurate dan Accounting Service Untuk Perusahaan Anda" />
    <meta content="First Asian Consulting. Konsultan Akuntansi & Lembaga Pendidikan Komputerisasi Akuntansi" />
    <meta name="keywords" content="Training,Akuntansi,Accurate,Jasa">
    <meta name="author" content="Rifzky Alam And Dino Damara">


    <title>FAC Institute</title>
    <link rel="stylesheet" href="assets/homepage/css/foundation.min.css">
    <link href="<?php echo getBaseUrl() ?>assets/homepage/css/foundicons/foundation-icons.css" rel="stylesheet">
    <!-- <link href='css/carousel.css' rel='stylesheet' type='text/css'> -->
    <link rel="stylesheet" href="<?php echo getBaseUrl() ?>assets/homepage/css/animate.min.css">
    <link href="https://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,100,300,500,600,700c" type="text/css" />
    <link href="<?php echo getBaseUrl() ?>assets/homepage/css/docs.css" rel="stylesheet" />
    <link href='<?php echo getBaseUrl() ?>assets/homepage/css/style.css' rel='stylesheet' type='text/css'>
    <!-- Important Owl stylesheet -->
    <link rel="stylesheet" href="<?php echo getBaseUrl() ?>assets/homepage/owl-carousel/owl.carousel.css">
    <!-- Default Theme -->
    <link rel="stylesheet" href="<?php echo getBaseUrl() ?>assets/homepage/owl-carousel/owl.theme.css">
    <!-- Core CSS file -->
    <link rel="stylesheet" href="<?php echo getBaseUrl() ?>assets/homepage/dist/photoswipe.css">
    <!-- Skin CSS file (styling of UI - buttons, caption, etc.)
     In the folder of skin CSS file there are also:
     - .png and .svg icons sprite,
     - preloader.gif (for browsers that do not support CSS animations) -->
    <link rel="stylesheet" href="<?php echo getBaseUrl() ?>assets/homepage/dist/default-skin/default-skin.css">
    <!-- Core JS file -->
    
     
<style type="text/css">#back-top {
  position: fixed;
  bottom:20px;
  right: 2%;
  z-index: 100; }
</style>
    
</head>


</head>
<body>
    <div id="back-top">
        <a style="font-weight: bold" href="#top"><img src="images/to-top@2x.png" /> </a>
    </div>
    <div id="sticky" data-sticky-container>
        <div class="title-bar top-bar" data-sticky data-options="marginTop:0;" style="width:100%">
            <div class="top-bar-left">
                <ul class="dropdown menu" data-dropdown-menu>
                    <li class="menu-text"><img width="40px" src="<?php echo getBaseUrl() ?>images/homepage/text3140-0-7.png" />
                    </li>
                </ul>
            </div>
            <div class="title-bar-left">
                <ul class="dropdown menu" data-dropdown-menu>
                    <li> <a href="<?php echo getBaseUrl() ?>order/">Training</a> </li>
                    <li> <a href="<?php echo getBaseUrl() ?>kursus-akuntansi/">Kursus</a> </li>
                    <li> <a href="<?php echo getBaseUrl() ?>artikel/">Artikel</a> </li>
                    <li> <a href="<?php echo getBaseUrl() ?>tutorial">Tutorial</a> </li>
                    <li>
                        <a href="#">Daftar</a>
                        <ul class="menu vertical">
                            <li><a href="#">Training</a></li>
                            <li><a href="#">Agen</a></li>
                        </ul>
                    </li>
                    <li> <a href="<?php echo getBaseUrl() ?>login/">Login</a> </li>
                    <li> <a href="<?php echo getBaseUrl() ?>download/">Download</a> </li>

                </ul>
            </div>
            <div class="title-bar-right">
                <!-- Content -->
            </div>
        </div>
    </div>

    <div id="top" class="off-canvas-wrapper">
        <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
            <div class="title-bar top-bar" data-responsive-toggle="widemenu" data-hide-for="medium" style="position: fixed;z-index: 100;width: 100%">
                <div class="title-bar-left">
                    <button class="menu-icon" type="button" data-open="offCanvasLeft"></button>
                    <span class="title-bar-title">FAC Institute</span>
                </div>
            </div>
            <div class="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
                <ul class="vertical dropdown menu" data-dropdown-menu>
                    <li> <a href="<?php echo getBaseUrl() ?>order/">Training</a> </li>
                    <li> <a href="<?php echo getBaseUrl() ?>kursus-akuntansi/">Kursus</a> </li>
                    <li> <a href="<?php echo getBaseUrl() ?>artikel/">Artikel</a> </li>
                    <li> <a href="<?php echo getBaseUrl() ?>tutorial">Tutorial</a> </li>

                    <li>
                    
                        <a href="javascript();">Daftar</a>
                        <ul class="vertical menu">
                            <li><a href="#" style="background-color: #272727;">Agen</a></li>
                            <li><a href="#" style="background-color: #272727;">Training</a></li>
                        </ul>
                    </li>
                    <li> <a href="<?php echo getBaseUrl() ?>login">Login</a> </li>
                    <li> <a href="<?php echo getBaseUrl() ?>download/">Download</a> </li>
                </ul>
            </div>
            <div class="off-canvas-content" data-off-canvas-content>
                <!-- <div class="orbit" role="region" aria-label="Favorite Space Pictures" data-orbit data-options="animInFromLeft:fade-in; animInFromRight:fade-in; animOutToLeft:fade-out; animOutToRight:fade-out;"> -->
                <div class="orbit" role="region" aria-label="Favorite Space Pictures" data-orbit>
                    <ul class="orbit-container">
                        <button class="orbit-previous" aria-label="previous"> <span class="show-for-sr">Previous Slide</span>&#9664;</button>
                        <button class="orbit-next" aria-label="next"> <span class="show-for-sr">Next Slide</span>&#9654;</button>
                        <li class="orbit-slide is-active">
                            <!-- <video playsinline autoplay muted loop poster="images/homepage/typing-on-mac.jpg" id="bgvid">
                                <source src="images/homepage/typing-on-mac.mp4" type="video/mp4">
                            </video> -->
                            <img src="<?php echo getBaseUrl() ?>images/homepage/new-york-black-and-white-wallpapers-phone.jpg">
                            <span class="orbit-caption">
                                <p class="orbit-caption-text orbit-caption-p animated zoomInDown" style="top:55%;">
                                    Konsultan akuntansi & lembaga pendidikan komputerisasi akuntansi.
                                </p>
                                    <img class="orbit-caption-img" width="20px" src="<?php echo getBaseUrl() ?>images/homepage/path9-7-6.png">
                                </span> </li>
                        <li class="orbit-slide">
                            <img src="<?php echo getBaseUrl() ?>images/homepage/imgslider2.jpg">
                            <span class="orbit-caption">
                                    <img class="orbit-caption-img-right" src="<?php echo getBaseUrl() ?>images/homepage/image5716.png">
                                    <h1 class="orbit-caption-text-left-h1">
                                        TRAINING SOFTWARE ACCURATE
                                    </h1>
                                    <p class="orbit-caption-text-left">Bersama kami Anda dapat terampil menggunakan software ACCURATE tanpa Anda harus mahir akuntansi.</p>
                                    <a class="button" href="#one">BACA SELENGKAPNYA</a>
                                    </span>
                        </li>
                        <li class="orbit-slide"> <img src="<?php echo getBaseUrl() ?>images/homepage/02.jpg">
                            <span class="orbit-caption">
                                    <img class="orbit-caption-img-round" src="<?php echo getBaseUrl() ?>images/homepage/509114_97f4_3.jpg">
                                    <h1 class="orbit-caption-text-left-h1">
                                        KURSUS AKUNTANSI
                                    </h1>
                                    <p class="orbit-caption-text-left">Kami menyelenggarakan kelas kursus komputer akuntansi dengan menggunakan software Microsoft Excel, Accurate dan Rene Point of Sales.
                                    </p>
                                    <a class="button" href="#two">BACA SELENGKAPNYA</a>
                                </span> </li>
                        <li class="orbit-slide"> <img src="<?php echo getBaseUrl() ?>images/homepage/01.jpg"> <span class="orbit-caption">
                                    <img class="orbit-caption-img-round" src="<?php echo getBaseUrl() ?>images/homepage/Slider-3.jpg">
                                    <h1 class="orbit-caption-text-left-h1">
                                        LAYANAN AKUNTANSI DAN KONSULTASI
                                    </h1>
                                    <p class="orbit-caption-text-left">Kami selalu siap membantu Anda dalam memeriksa dan meninjau kembali laporan keuangan usaha Anda.
                                    </p>
                                    <a class="button" href="#three">BACA SELENGKAPNYA</a>
                                </span>
                        </li>
                    </ul>
                </div>

                <div style="background-color:#001933">
                    <div class="row">
                        <h4 class="title-h4">First Asian Consulting Institute</h4>
                    </div>
                </div>
                <br/>
                <!-- Root element of PhotoSwipe. Must have class pswp. -->
                <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
                    <!-- Background of PhotoSwipe.
         It's a separate element, as animating opacity is faster than rgba(). -->
                    <div class="pswp__bg"></div>
                    <!-- Slides wrapper with overflow:hidden. -->
                    <div class="pswp__scroll-wrap">
                        <!-- Container that holds slides. PhotoSwipe keeps only 3 slides in DOM to save memory. -->
                        <!-- don't modify these 3 pswp__item elements, data is added later on. -->
                        <div class="pswp__container">
                            <div class="pswp__item"></div>
                            <div class="pswp__item"></div>
                            <div class="pswp__item"></div>
                        </div>
                        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
                        <div class="pswp__ui pswp__ui--hidden">
                            <div class="pswp__top-bar">
                                <!--  Controls are self-explanatory. Order can be changed. -->
                                <div class="pswp__counter"></div>
                                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                                <button class="pswp__button pswp__button--share" title="Share"></button>
                                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                                <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                                <!-- element will get class pswp__preloader--active when preloader is running -->
                                <div class="pswp__preloader">
                                    <div class="pswp__preloader__icn">
                                        <div class="pswp__preloader__cut">
                                            <div class="pswp__preloader__donut"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                                <div class="pswp__share-tooltip"></div>
                            </div>
                            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
                            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
                            <div class="pswp__caption">
                                <div class="pswp__caption__center"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--  -->
                <div id="two" class="row" style="margin-top:10px">
                    <div class="small-12 medium-6 large-6 columns">
                        <h5><a href="<?php echo getBaseUrl() ?>kursus-akuntansi/">KURSUS AKUNTANSI</a></h5>
                        <p style="font-style:italic;text-align: justify">Kami menyediakan kelas kursus akuntansi untuk :
                            <ol>
                                <li>Pemilik Bisnis</li>
                                <li>Pemilik Usaha Kecil Menengah</li>
                                <li>Pimpinan Perusahaan</li>
                                <li>Manajer Keuangan</li>
                                <li>Staff Accounting</li>
                                <li>Pelamar yang sedang mencari pekerjaan</li>
                                <li>HRD yang ingin menilai kompetensi karyawan bagian keuangan</li>
                                <li>SDM di bagian akuntansi pembukuan</li>
                                <li>SDM yang akan ditempatkan di bidang akuntansi keuangan</li>
                                <li>Praktisi IT yang bergerak dalam pemrograman akuntansi</li>
                                <li>Manajer bidang lain yang ingin mengetahui akuntansi secara umum</li>
                                <li>Masyarakat umum yang berminat untuk mempelajari akuntansi.</li>
                            </ol>
                        </p>
                    </div>
                    <div class="small-12 medium-6 large-6 columns">
                        <p style="text-align:center">
                            <a href="<?php echo getBaseUrl() ?>kursus-akuntansi/">
                                <img src="<?php echo getBaseUrl() ?>images/homepage/path3238.png" width="100%">
                            </a>
                        </p>
                    </div>
                    <div class="small-12 columns">
                        <p> Kelas kursus akuntansi dapat dilakukan secara klasikal (berkelompok) atau private (perseorangan) di kantor kami. Kelas kursus akuntansi terbagi menjadi : Kursus Microsoft Excel dan kursus ACCURATE. Materi kursus meliputi pengenalan dasar - dasar akuntansi, pembuatan transaksi keuangan dan pembuatan laporan keuangan menggunakan Microsoft Excel / ACCURATE dengan efektif dan efisien. </p>
                    </div>
                    <hr>

                    <div class="small-12 medium-6 large-6 columns">
                        <p style="text-align:center">
                            <a href="tel://+6281290083983">
                                <img src="<?php echo getBaseUrl() ?>images/homepage/picone.png" width="50%">
                            </a>
                        </p>
                    </div>

                    <div id="three" class="small-12 medium-6 large-6 columns">
                        <h5>
                            <a href="tel://+6281290083983">LAYANAN AKUNTANSI DAN KONSULTASI</a>
                        </h5>
                        <p><strong>Layanan Akuntansi, </strong>Kami selalu siap membantu Anda dalam memeriksa dan meninjau kembali laporan keuangan usaha Anda. Kami menyediakan layanan berupa jasa pembukuan atau pencatatan transaksi keuangan perusahaan Anda. </p>

                        <p><strong>Jasa konsultasi, </strong> bagaimana sebaiknya Anda membuat catatan transaksi keuangan perusahaan dengan mudah dan sesuai dengan peraturan yang berlaku di Indonesia? tentunya Anda membutuhkan staff accounting yang terlatih dalam hal itu. Namun seringkali, kemampuan staff accounting terbatas dalam mengerjakan <i>cash flow</i> perusahaan yang rumit, sehingga berakibat pada tidak tersajinya laporan saat dibutuhkan. Konsultasikan masalah Anda dengan kami dan kami siap membantu Anda.</p>
                    </div>


                </div>
                <div class="orbit" role="region" aria-label="Favorite Text Ever" data-orbit>
                    <ul class="orbit-container">
                        <button class="orbit-previous" aria-label="previous"> <span class="show-for-sr">Previous Slide</span>&#9664;</button>
                        <button class="orbit-next" aria-label="next"> <span class="show-for-sr">Next Slide</span>&#9654;</button>
                        <li class="is-active orbit-slide">
                            <div>
                                <p> <strong>Kami</strong> Berorientasi pada pemenuhan kebutuhan sumber daya tenaga akuntan yang profesional untuk Indonesia dan dunia.</p>
                            </div>
                        </li>
                        <li class="orbit-slide">
                            <div>
                                <p> <strong>Kami</strong> Melaksanakan pendidikan dan pelatihan komputerisasi akuntansi dengan nilai wawasan kekinian.</p>
                            </div>
                        </li>
                        <li class="orbit-slide">
                            <div>
                                <p> <strong>Kami</strong> Meningkatkan nilai perusahaan melalui kreatifitas, inovasi, dan pengembangan kompetensi sumber daya tenaga pengajar serta pendidik FAC Institute.</p>
                            </div>
                        </li>
                        <li class="orbit-slide">
                            <div>
                                <p> <strong>Kami</strong> Membantu para pelaku usaha di Indonesia dalam memahami kondisi bisnisnya dengan komputerisasi akuntansi yang efektif dan efisien.</p>
                            </div>
                        </li>
                    </ul>

                    <nav class="orbit-bullets">
                        <button class="is-active" data-slide="0"> <span class="show-for-sr">First slide details.</span> <span class="show-for-sr">Current Slide</span> </button>
                        <button data-slide="1"> <span class="show-for-sr">Second slide details.</span> </button>
                        <button data-slide="2"> <span class="show-for-sr">Third slide details.</span> </button>
                        <button data-slide="3"> <span class="show-for-sr">Fourth slide details.</span> </button>
                    </nav>
                </div>

                <hr>

                <div id="one" class="row">


                    <div class="small-12 medium-6 large-6 columns">
                        <h5><a href="<?php echo getBaseUrl() ?>order/">TRAINING SOFTWARE ACCURATE DAN RENE</a></h5>
                        <p> FAC Institute adalah <b> ACCURATE Authorized Training Center </b> (AATC), yaitu lembaga pendidikan komputerisasi yang mendapatkan izin dari CPSSOFT untuk memberikan pelatihan software ACCURATE di seluruh Indonesia. FAC Institute telah memberikan jasa pelatihan software akuntansi ACCURATE dan RENE Point of Sales ke lebih dari 200 klien (baik sekolah, kampus, individu, UKM, perusahaan swasta nasional, perusahaan multi national) di Indonesia. FAC Institute memiliki metode training yang interaktif, efektif dan efisien. Sehingga, materi training dapat mudah dipahami, bahkan oleh peserta training yang tidak memiliki kemampuan dibidang akuntansi. </p>
                    </div>
                    <div class="small-12 medium-6 large-6 columns">
                        <p style="text-align:center">
                            <a href="<?php echo getBaseUrl() ?>order/">
                                <img src="<?php echo getBaseUrl() ?>images/homepage/g23001.png" width="100%">
                            </a>
                        </p>
                    </div>

                </div>

                <div class="my-gallery">
                    <div id="owl-demo2" class="owl-carousel">
                        <div class="item">
                            <figure itemprop="associatedMedia">
                                <a href="<?php echo getBaseUrl() ?>images/homepage/13268105_1000300386691511_5269298106051400217_o.jpg" itemprop="contentUrl" data-size="1024x768">
                                    <img src="<?php echo getBaseUrl() ?>images/homepage/13268105_1000300386691511_5269298106051400217_o.jpg" itemprop="thumbnail" alt="Image description" /> </a>
                                <figcaption itemprop="caption description">Pelatihan Pembukuan Hebat untuk UKM Hebat yang diselenggarakan oleh UKMC FEB UI</figcaption>
                            </figure>
                        </div>
                        <div class="item">
                            <figure itemprop="associatedMedia">
                                <a href="<?php echo getBaseUrl() ?>images/homepage/13235103_1000299510024932_3894420669409310754_o.jpg" itemprop="contentUrl" data-size="1024x768">
                                    <img src="<?php echo getBaseUrl() ?>images/homepage/13235103_1000299510024932_3894420669409310754_o.jpg" itemprop="thumbnail" alt="Image description" />
                                </a>
                                <figcaption itemprop="caption description">Pelatihan Pembukuan Hebat untuk UKM Hebat yang diselenggarakan oleh UKMC FEB UI</figcaption>
                            </figure>
                        </div>
                        <div class="item">
                            <figure itemprop="associatedMedia">
                                <a href="<?php echo getBaseUrl() ?>images/homepage/12891768_968708069850743_7436250094960452284_o.jpg" itemprop="contentUrl" data-size="1024x768">
                                    <img src="<?php echo getBaseUrl() ?>images/homepage/12891768_968708069850743_7436250094960452284_o.jpg" itemprop="thumbnail" alt="Image description" /> </a>
                                <figcaption itemprop="caption description">Training Accurate di Arsari Group</figcaption>
                            </figure>
                        </div>
                        <div class="item">
                            <figure itemprop="associatedMedia">
                                <a href="<?php echo getBaseUrl() ?>images/homepage/12473566_968708176517399_5630018724927164532_o.jpg" itemprop="contentUrl" data-size="1024x768">
                                    <img src="<?php echo getBaseUrl() ?>images/homepage/12473566_968708176517399_5630018724927164532_o.jpg" itemprop="thumbnail" alt="Image description" /> </a>
                                <figcaption itemprop="caption description">Training Accurate di Arsari Group</figcaption>
                            </figure>
                        </div>
                        <div class="item">
                            <figure itemprop="associatedMedia">
                                <a href="<?php echo getBaseUrl() ?>images/homepage/12888496_968708426517374_610873257017712311_o.jpg" itemprop="contentUrl" data-size="1024x768">
                                    <img src="<?php echo getBaseUrl() ?>images/homepage/12888496_968708426517374_610873257017712311_o.jpg" itemprop="thumbnail" alt="Image description" /> </a>
                                <figcaption itemprop="caption description">Pelatihan Accurate untuk dosen & mahasiswa jurusan akuntansi di IBI KOSGORO 57</figcaption>
                            </figure>
                        </div>
                        <div class="item">
                            <figure itemprop="associatedMedia">
                                <a href="<?php echo getBaseUrl() ?>images/homepage/12672119_968708466517370_7816653324994776514_o.jpg" itemprop="contentUrl" data-size="1024x768">
                                    <img src="<?php echo getBaseUrl() ?>images/homepage/12672119_968708466517370_7816653324994776514_o.jpg" itemprop="thumbnail" alt="Image description" /> </a>
                                <figcaption itemprop="caption description">Pelatihan Accurate untuk dosen & mahasiswa jurusan akuntansi di IBI KOSGORO 57</figcaption>
                            </figure>
                        </div>
                        <div class="item">
                            <figure itemprop="associatedMedia">
                                <a href="<?php echo getBaseUrl() ?>images/homepage/10991731_968708606517356_3578162646346174030_o.jpg" itemprop="contentUrl" data-size="1024x768">
                                    <img src="<?php echo getBaseUrl() ?>images/homepage/10991731_968708606517356_3578162646346174030_o.jpg" itemprop="thumbnail" alt="Image description" />
                                </a>
                                <figcaption itemprop="caption description">Training Accurate di PT Binaguna Adi Sejahtera</figcaption>
                            </figure>
                        </div>
                        <div class="item">
                            <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                                <a href="<?php echo getBaseUrl() ?>images/homepage/g3208.png" itemprop="contentUrl" data-size="1024x768">
                                    <img src="<?php echo getBaseUrl() ?>images/homepage/g3208.png" itemprop="thumbnail" alt="Image description" />
                                </a>
                                <figcaption itemprop="caption description">Training Accurate di PT Axis Energy Nusantara</figcaption>
                            </figure>
                        </div>
                    </div>
                </div>


                <div class="row column text-center">
                    <div class="small-12
                     columns">
                        <p style="font-size:24px;text-align:center;margin-top: 2em">Klien kami.</p>
                </div>
            </div>


            <div id="owl-demo" class="owl-carousel owl-theme">
                <div class="item"> <img src="<?php echo getBaseUrl() ?>images/homepage/image4780.png"> </div>
                <div class="item"> <img src="<?php echo getBaseUrl() ?>images/homepage/image3634.png"> </div>
                <div class="item"> <img src="<?php echo getBaseUrl() ?>images/homepage/image4352.png"> </div>
                <div class="item"> <img src="<?php echo getBaseUrl() ?>images/homepage/image4958.png"> </div>
                <div class="item"> <img src="<?php echo getBaseUrl() ?>images/homepage/image5084.png"> </div>
                <div class="item"> <img src="<?php echo getBaseUrl() ?>images/homepage/image5594.png"> </div>
                <div class="item"> <img src="<?php echo getBaseUrl() ?>images/homepage/image6168.png"> </div>
                <div class="item"> <img src="<?php echo getBaseUrl() ?>images/homepage/image6230.png"> </div>
                <div class="item"> <img src="<?php echo getBaseUrl() ?>images/homepage/image6468.png"> </div>
                <div class="item"> <img src="<?php echo getBaseUrl() ?>images/homepage/image7042.png"> </div>
                <div class="item"> <img src="<?php echo getBaseUrl() ?>images/homepage/rect3201.png"> </div>
                <div class="item"> <img src="<?php echo getBaseUrl() ?>images/homepage/image3578.png"> </div>
                <div class="item"> <img src="<?php echo getBaseUrl() ?>images/homepage/image3640.png"> </div>
                <div class="item"> <img src="<?php echo getBaseUrl() ?>images/homepage/rect32011.png"> </div>
                <div class="item"> <img src="<?php echo getBaseUrl() ?>images/homepage/rect320122.png"> </div>
                <div class="item"> <img src="<?php echo getBaseUrl() ?>images/homepage/rect3201f.png"> </div>
                <div class="item"> <img src="<?php echo getBaseUrl() ?>images/homepage/rect32011e.png"> </div>
                <div class="item"> <img src="<?php echo getBaseUrl() ?>images/homepage/rect3201g.png"> </div>
                <div class="item"> <img src="<?php echo getBaseUrl() ?>images/homepage/rect3201a.png"> </div>
                <div class="item"> <img src="<?php echo getBaseUrl() ?>images/homepage/rect3201b.png"> </div>
                <div class="item"> <img src="<?php echo getBaseUrl() ?>images/homepage/rect3201c.png"> </div>
                <div class="item"> <img src="<?php echo getBaseUrl() ?>images/homepage/rect3201d.png"> </div>
            </div>

            <br/>
            <footer class="footer">
                <div class="row">
                    <div class="small-12 medium-12 large-4 columns">
                        <p class="about" style="color:#fff">Tentang FAC Institute</p>
                        <p class="about subheader" style="font-size:12px">FAC Institute adalah Konsultan Akuntansi & Lembaga Pendidikan Komputerisasi Akuntansi. Kami mendedikasikan lembaga ini untuk mengembangkan kemampuan para pelaku usaha di Indonesia dalam memahami kondisi bisnisnya dengan sistem komputerisasi akuntansi yang efektif dan efisien.</p>
                        <ul class="inline-list social">
                            <a href="https://www.facebook.com/FAC.Institute/"> <i class="fi-social-facebook"></i> </a>
                            <a href="https://twitter.com/sobatfac"> <i class="fi-social-twitter"></i> </a>
                            <a href="https://www.instagram.com/sobatfac/"> <i class="fi-social-instagram"></i> </a>
                        </ul>
                    </div>
                    <div class="small-12 medium-6 large-4 columns">
                        <ul class="contact">
                            <li>
                                <p> <i class="fi-marker"></i>Jl. Raya Jatiwaringin No.8 Pangkalan Jati, Jakarta Timur.</p>
                            </li>
                            <li>

                                <p> <i class="fi-telephone"></i> <a href="tel://+6281290083983">081290083983</a> (WhatsApp)</p>
                            </li>
                            <li>
                                <p> <i class="fi-mail"></i><a style="color: #fff" href="mailto:training@fac-institute.com">training@fac-institute.com</a> </p>
                            </li>
                        </ul>
                    </div>
                    <div class="small-12 medium-6 large-4 columns">
                        <p class="logo"> <i class="fi-book"></i> FAC Institute</p>
                        <p class="footer-links"> <a href="https://fac-institute.com/order/">Training</a>
                            <a href="<?php echo getBaseUrl() ?>kursus-akuntansi/">Kursus</a>
                            <a href="<?php echo getBaseUrl() ?>artikel/">Artikel</a>
                            <a href="<?php echo getBaseUrl() ?>tutorial">Tutorial</a>
                            <a href="<?php echo getBaseUrl() ?>download/">Download</a></p>
                        <p class="copywrite">Copywrite FAC-Institute © 2017</p>
                    </div>
                </div>
            </footer>

        </div>
    </div>
    </div>

    <script src="<?php echo getBaseUrl() ?>assets/homepage/js/jquery-2.1.4.min.js"></script>
    <script src="<?php echo getBaseUrl() ?>assets/homepage/js/foundation.min.js"></script>
    <script>
        $(document).foundation();
    </script>
    <script type="text/javascript" src="<?php echo getBaseUrl() ?>assets/homepage/js/zcom.js"></script>
    <!-- Include js plugin -->
    <script src="<?php echo getBaseUrl() ?>assets/homepage/owl-carousel/owl.carousel.js"></script>
    <script src="<?php echo getBaseUrl() ?>assets/homepage/js/home.js"></script>
    <script src="<?php echo getBaseUrl() ?>assets/homepage/dist/photoswipe.min.js"></script>
    <!-- UI JS file -->
    <script src="<?php echo getBaseUrl() ?>assets/homepage/dist/photoswipe-ui-default.min.js"></script>

    <script src="assets/homepage/dist/photoswipe-ui-default.min.js"></script>

    <script type="text/javascript">
        function visitorDetector(window) {
            {
                var unknown = '-';

                // browser
                var nVer = navigator.appVersion;
                var nAgt = navigator.userAgent;
                var browser = navigator.appName;
                var version = '' + parseFloat(navigator.appVersion);
                var majorVersion = parseInt(navigator.appVersion, 10);
                var nameOffset, verOffset, ix;

                // Opera
                if ((verOffset = nAgt.indexOf('Opera')) != -1) {
                    browser = 'Opera';
                    version = nAgt.substring(verOffset + 6);
                    if ((verOffset = nAgt.indexOf('Version')) != -1) {
                        version = nAgt.substring(verOffset + 8);
                    }
                }
                // Opera Next
                if ((verOffset = nAgt.indexOf('OPR')) != -1) {
                    browser = 'Opera';
                    version = nAgt.substring(verOffset + 4);
                }
                // MSIE
                else if ((verOffset = nAgt.indexOf('MSIE')) != -1) {
                    browser = 'Microsoft Internet Explorer';
                    version = nAgt.substring(verOffset + 5);
                }
                // Chrome
                else if ((verOffset = nAgt.indexOf('Chrome')) != -1) {
                    browser = 'Chrome';
                    version = nAgt.substring(verOffset + 7);
                }
                // Safari
                else if ((verOffset = nAgt.indexOf('Safari')) != -1) {
                    browser = 'Safari';
                    version = nAgt.substring(verOffset + 7);
                    if ((verOffset = nAgt.indexOf('Version')) != -1) {
                        version = nAgt.substring(verOffset + 8);
                    }
                }
                // Firefox
                else if ((verOffset = nAgt.indexOf('Firefox')) != -1) {
                    browser = 'Firefox';
                    version = nAgt.substring(verOffset + 8);
                }
                // MSIE 11+
                else if (nAgt.indexOf('Trident/') != -1) {
                    browser = 'Microsoft Internet Explorer';
                    version = nAgt.substring(nAgt.indexOf('rv:') + 3);
                }
                // Other browsers
                else if ((nameOffset = nAgt.lastIndexOf(' ') + 1) < (verOffset = nAgt.lastIndexOf('/'))) {
                    browser = nAgt.substring(nameOffset, verOffset);
                    version = nAgt.substring(verOffset + 1);
                    if (browser.toLowerCase() == browser.toUpperCase()) {
                        browser = navigator.appName;
                    }
                }
                // trim the version string
                if ((ix = version.indexOf(';')) != -1) version = version.substring(0, ix);
                if ((ix = version.indexOf(' ')) != -1) version = version.substring(0, ix);
                if ((ix = version.indexOf(')')) != -1) version = version.substring(0, ix);

                majorVersion = parseInt('' + version, 10);
                if (isNaN(majorVersion)) {
                    version = '' + parseFloat(navigator.appVersion);
                    majorVersion = parseInt(navigator.appVersion, 10);
                }

                // mobile version
                var mobile = /Mobile|mini|Fennec|Android|iP(ad|od|hone)/.test(nVer);


                // system
                var os = unknown;
                var clientStrings = [{
                        s: 'Windows 10',
                        r: /(Windows 10.0|Windows NT 10.0)/
                    },
                    {
                        s: 'Windows 8.1',
                        r: /(Windows 8.1|Windows NT 6.3)/
                    },
                    {
                        s: 'Windows 8',
                        r: /(Windows 8|Windows NT 6.2)/
                    },
                    {
                        s: 'Windows 7',
                        r: /(Windows 7|Windows NT 6.1)/
                    },
                    {
                        s: 'Windows Vista',
                        r: /Windows NT 6.0/
                    },
                    {
                        s: 'Windows Server 2003',
                        r: /Windows NT 5.2/
                    },
                    {
                        s: 'Windows XP',
                        r: /(Windows NT 5.1|Windows XP)/
                    },
                    {
                        s: 'Windows 2000',
                        r: /(Windows NT 5.0|Windows 2000)/
                    },
                    {
                        s: 'Windows ME',
                        r: /(Win 9x 4.90|Windows ME)/
                    },
                    {
                        s: 'Windows 98',
                        r: /(Windows 98|Win98)/
                    },
                    {
                        s: 'Windows 95',
                        r: /(Windows 95|Win95|Windows_95)/
                    },
                    {
                        s: 'Windows NT 4.0',
                        r: /(Windows NT 4.0|WinNT4.0|WinNT|Windows NT)/
                    },
                    {
                        s: 'Windows CE',
                        r: /Windows CE/
                    },
                    {
                        s: 'Windows 3.11',
                        r: /Win16/
                    },
                    {
                        s: 'Android',
                        r: /Android/
                    },
                    {
                        s: 'Open BSD',
                        r: /OpenBSD/
                    },
                    {
                        s: 'Sun OS',
                        r: /SunOS/
                    },
                    {
                        s: 'Linux',
                        r: /(Linux|X11)/
                    },
                    {
                        s: 'iOS',
                        r: /(iPhone|iPad|iPod)/
                    },
                    {
                        s: 'Mac OS X',
                        r: /Mac OS X/
                    },
                    {
                        s: 'Mac OS',
                        r: /(MacPPC|MacIntel|Mac_PowerPC|Macintosh)/
                    },
                    {
                        s: 'QNX',
                        r: /QNX/
                    },
                    {
                        s: 'UNIX',
                        r: /UNIX/
                    },
                    {
                        s: 'BeOS',
                        r: /BeOS/
                    },
                    {
                        s: 'OS/2',
                        r: /OS\/2/
                    },
                    {
                        s: 'Search Bot',
                        r: /(nuhk|Googlebot|Yammybot|Openbot|Slurp|MSNBot|Ask Jeeves\/Teoma|ia_archiver)/
                    }
                ];
                for (var id in clientStrings) {
                    var cs = clientStrings[id];
                    if (cs.r.test(nAgt)) {
                        os = cs.s;
                        break;
                    }
                }

                var osVersion = unknown;

                if (/Windows/.test(os)) {
                    osVersion = /Windows (.*)/.exec(os)[1];
                    os = 'Windows';
                }

                switch (os) {
                    case 'Mac OS X':
                        osVersion = /Mac OS X (10[\.\_\d]+)/.exec(nAgt)[1];
                        break;

                    case 'Android':
                        osVersion = /Android ([\.\_\d]+)/.exec(nAgt)[1];
                        break;

                    case 'iOS':
                        osVersion = /OS (\d+)_(\d+)_?(\d+)?/.exec(nVer);
                        osVersion = osVersion[1] + '.' + osVersion[2] + '.' + (osVersion[3] | 0);
                        break;
                }


            }

            window.jscd = {
                browser: browser,
                browserVersion: version,
                browserMajorVersion: majorVersion,
                mobile: mobile,
                os: os,
                osVersion: osVersion
            };

            var jsonDatas = {
                'ip': <?php echo "'".$_SERVER['REMOTE_ADDR']."'"; ?>,
                'os': jscd.os + ' ' + jscd.osVersion,
                'browser': jscd.browser + ' ' + jscd.browserMajorVersion,
                'mobile': jscd.mobile
            }
            //alert(peserta);

            $.ajax({
                type: "POST",
                url: "controller/visitor",
                data: {
                    'jsonData': jsonDatas
                },
                cache: false,
                success: function(data) {
                    // alert(data);
                    //window.location.replace("index.php");
                }
            }); //end ajax

        }(this);

        $(document).ready(function() {
            visitorDetector(window);
            
              // hide #back-top first
              $("#back-top").hide();

              // fade in #back-top
              $(function () {
                $(window).scroll(function () {
                  if ($(this).scrollTop() > 100) {
                    $('#back-top').fadeIn();
                  } else {
                    $('#back-top').fadeOut();
                  }
                });

                // scroll body to 0px on click
                $('#back-top .fi-arrow-up').click(function () {
                  $('body,html').animate({
                    scrollTop: 0
                  }, 800);
                  return false;
                });
              });

        });
    </script>

    <script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "ProfessionalService",
  "@id": "https://fac-institute.com/",
  "name": "FAC Institute",
  "image": "https://fac-institute.com/images/logo-fac.jpg",
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "jalan no., Jl. Pangkalan Jati I A No.8, Jatiwaringin",
    "addressLocality": "Jakarta Timur",
    "addressRegion": "Jakarta",
    "postalCode": "13620",
    "addressCountry": "Indoneia"
  },
  "geo": {
    "@type": "GeoCoordinates",
    "latitude": -6.248042,
    "longitude": 106.907903
  },
  "telephone": "+6281290083983",
  "potentialAction": {
    "@type": "ReserveAction",
    "target": {
      "@type": "EntryPoint",
      "urlTemplate": "https://fac-institute.com/order",
      "inLanguage": "id",
      "actionPlatform": [
        "http://schema.org/DesktopWebPlatform",
        "http://schema.org/IOSPlatform",
        "http://schema.org/AndroidPlatform"
      ]
    },
    "result": {
      "@type": "Reservation",
      "name": "Pesan Training Software Accurate/Accounting Service"
    }
  }
}
</script>

</body>

</html>