<?php 
session_start();
require 'php-graph-sdk/src/Facebook/autoload.php';

$fb = new \Facebook\Facebook([
  'app_id' => '454421948087465',
  'app_secret' => 'd21953ba8b4b87e88d02e1bd616c6431',
  'default_graph_version' => 'v2.8',
  //'default_access_token' => '{access-token}', // optional
]);

$helper = $fb->getRedirectLoginHelper();

$permissions = ['email']; // Optional permissions
$loginUrl = $helper->getLoginUrl('https://fac-institute.com/facebook/callback', $permissions);

echo '<a href="' . htmlspecialchars($loginUrl) . '">Log in with Facebook!</a>';