<?php 
date_default_timezone_set("Asia/Jakarta"); 
if(isset($_POST['namaPerusahaan'],$_POST['namaPersonal'],$_POST['email'],$_POST['emailKantor'],
	$_POST['telepon'],$_POST['teleponKantor'],$_POST['alamat'],$_POST['jabatan'],
	$_POST['jnsUsaha'],$_POST['ketJnsUsaha'],$_POST['tglTraining'],$_POST['hariTraining'],
	$_POST['jnsPengguna'],$_POST['versiAccurate'],$_POST['agendaTraining'],$_POST['tempatBeli'],$_POST['salesman'],$_POST['alamatLengkap'])) {



	include_once '../model/Pendaftar.php';
	$pendaftar = new objekPendaftar();
	$control = new Pendaftar();

	$unik = md5(time().'rifzky.mail@gmail.com');

	$pendaftar->setID($unik);
	$pendaftar->setNamaKantor($_POST['namaPerusahaan']);
	$pendaftar->setNamaPersonal($_POST['namaPersonal']);
	$pendaftar->setEmailKantor($_POST['emailKantor']);
	$pendaftar->setEmailKontak($_POST['email']);
	$pendaftar->setTeleponKantor($_POST['teleponKantor']);
	$pendaftar->setTelepon($_POST['telepon']);
	$pendaftar->setAlamat($_POST['alamat']);
	$pendaftar->setAlamatLengkap($_POST['alamatLengkap']);
	$pendaftar->setJabatan($_POST['jabatan']);
	$pendaftar->setJenisUsaha($_POST['jnsUsaha']);
	$pendaftar->setKetJenisUsaha($_POST['ketJnsUsaha']);
	$pendaftar->setTanggalTraining($_POST['tglTraining']);
	$pendaftar->setJenisPengguna($_POST['jnsPengguna']);
	$pendaftar->setVersiAccurate($_POST['versiAccurate']);
	$pendaftar->setAgendaTraining($_POST['agendaTraining']);
	$pendaftar->setTmptBeliAccurate($_POST['tempatBeli']);
	$pendaftar->setSalesmanAccurate($_POST['salesman']);
	$pendaftar->setTglDaftar(date("Y-m-d",time()));
	$pendaftar->setJumlahHari($_POST['hariTraining']);


	if($control->masukData($pendaftar)){
		echo "Data has been saved!";
	}else{
		echo "Data cannot be saved!";
	}	
}

if (isset($_GET['coba'])) {
	include_once '../model/Pendaftar.php';
	$pendaftar = new Pendaftar();

	$pendaftar->setNamaKantor('rifzky');
	$pendaftar->setEmailKantor('email kantor');
	$pendaftar->setEmailKontak('email kontak');
	$pendaftar->setTeleponKantor('telepon kantor');
	$pendaftar->setTelepon('telepon biasa');
	$pendaftar->setAlamat('alamat kantor');
	$pendaftar->setJabatan('jabatanku');
	$pendaftar->setJenisUsaha('jenis usaha');
	$pendaftar->setKetJenisUsaha('keterangan jenis usaha');
	$pendaftar->setTanggalTraining('2016-03-07');
	$pendaftar->setJenisPengguna('jenis pengguna');
	$pendaftar->setVersiAccurate('versi accurate');
	$pendaftar->setAgendaTraining('agenda training');
	$pendaftar->setTmptBeliAccurate('tempat beli accurate');
	$pendaftar->setSalesmanAccurate('si anu');
	$pendaftar->setTglDaftar('02-05-1990');
	$pendaftar->setJumlahHari('13');

	/*
	echo "nama kantor:" . " " . $pendaftar->getNamaKantor() . "<br>";
	echo "email kantor:" . " " . $pendaftar->getEmailKantor() . "<br>";
	echo "email kontak:" . " " . $pendaftar->getEmailKontak() . "<br>";
	echo "telepon kantor:" . " " . $pendaftar->getTeleponKantor() . "<br>";
	echo "telepon kontak:" . " " . $pendaftar->getTelepon() . "<br>";
	echo "alamat:" . " " . $pendaftar->getAlamat() . "<br>";
	echo "jabatan:" . " " . $pendaftar->getJabatan() . "<br>";
	echo "jenisUsaha:" . " " . $pendaftar->getJenisUsaha() . "<br>";
	echo "keterangan jenisUsaha:" . " " . $pendaftar->getKetJenisUsaha() . "<br>";
	echo "tanggal training:" . " " . $pendaftar->getTanggalTraining() . "<br>";
	echo "jenis pengguna:" . " " . $pendaftar->getJenisPengguna() . "<br>";
	echo "versi accurate:" . " " . $pendaftar->getVersiAccurate() . "<br>";
	echo "agenda training:" . " " . $pendaftar->getAgendaTraining() . "<br>";
	echo "tempatBeli:" . " " . $pendaftar->getTmptBeliAccurate() . "<br>";
	echo "salesman:" . " " . $pendaftar->getSalesmanAccurate() . "<br>";
	echo "tanggal daftar:" . " " . $pendaftar->getTglDaftar() . "<br>";
	*/
	if ($pendaftar->masukData()) {
		echo "Data berhasil disimpan";
	}else{
		echo "data gagal disimpan";
	}


}

if (isset($_POST['inputDataJSON'])){
    include_once '../model/Pendaftar.php';
    $ctrlPendaftar = new Pendaftar();
    
    $objek = json_encode($_POST['inputDataJSON']);
    $objek = json_decode($objek);
    if ($ctrlPendaftar->masukDataJSON($objek)){
        mail("training@fac-institute.com , admin@fac-institute.com , training.facinstitute@gmail.com , rifzky.mail@gmail.com", "Order Training Accurate From Client Page", "System has detected that there was a client trying to have an accurate training by applying from order page at ". date('H:i:s').".");
    	echo "Terimakasih Telah Mendaftar, Harap Tunggu Informasi Selanjutnya Dari Admin Kami";
    }else{
        echo "Data Gagal Disimpan, Harap Hubungi Admin Kami atau kirim Email ke: admin@fac-institute.com";
    }
}


?>