<?php 
include_once $_SERVER['DOCUMENT_ROOT'].'/clientcf/Dataconfig.php';
class Homepagectr{
    
    // @url /
    public static function Indexhomepage(){
    	$data = new Data();
    	// print_r($data);
    	$data->judul='home';
    	$data->View('homepage/index.homepage',$data);
    }

    // @url /team
    public static function TeamHomepage(){
    	$data = new Data();
    	$data->Model('Petugas');
		$trainer = new ControlPetugas();
		
		if(date('m')=='1'){
    	    $data->tahun= date("Y",strtotime("-1 year"));
    	    $isjan = '1';
    	}else{
    	    $data->tahun= date('Y');  
    	    $isjan='0';
    	}
    	
		
    	$idata = $trainer->GetBestTrainerClientSide();
    	$data->namabulan = date("F",strtotime("-1 month")); // "F" is month name
    	if(@$idata['nama']==''){
    	    $idata = $trainer->GetBestTrainerClientSideLastMonth($isjan);
    	    $data->namabulan = date("F",strtotime("-2 month"));
    	}
    	$data->listsupports = $trainer->GetListCustSupport();
    	$data->listmarketing = $trainer->GetListMarketing();
    	$data->listtrainer = $trainer->GetListTrainers();
    	
    	
    	$data->namabest = $idata['nama'];
    	$data->fotobest=$idata['picture'];
    	
    	$data->judul='teamhome';
    	$data->title = 'FAC Institute - Team FAC';
    	$data->pagedesc = 'Kami adalah bagian dari staff FAC Institute saat ini.';
    	$data->pagekeywords = 'teamfac';
    	$data->View('homepage/facteam/index.facteam',$data);
    }
    

    // @url /training
    public static function Traininghomepage(){
    	$data = new Data();
    	// print_r($data);
    	$data->judul='traininghome';
    	$data->title = 'FAC Institute - Training';
    	$data->pagedesc = '';
    	$data->pagekeywords = '';
    	$data->View('homepage/training/index.training',$data);
    }

    // @url /training/carapesan
    public static function CaraPesanTraining(){
    	$data = new Data();
    	
    	$data->judul = 'trainingcarapesan';
    	$data->title = 'Training - Langkah Order Training';
    	$data->pagedesc = '';
    	$data->pagekeywords = '';
    	$data->View('homepage/training/carapesan.training',$data);
    }

    // @url /training/daftarharga
    public static function DaftarHargaTraining(){
    	$data = new Data();
    	
    	$data->judul = 'daftarharga';
    	$data->title = 'Training - Daftar Harga';
    	$data->pagedesc = '';
    	$data->pagekeywords = '';
    	$data->View('homepage/training/harga.training',$data);
    }

    // @url /accountingservice
    public static function AccountingService(){
    	$data = new Data();
    	
    	$data->judul = 'dasbor';
    	$data->title = 'Accounting Service - FAC-Institute';
    	$data->pagedesc = '';
    	$data->pagekeywords = '';
    	$data->View('homepage/accountingservice/index.accsvc',$data);
    }

    // @url /accountingservice/carapesan
    public static function AccountingServiceCaraPesan(){
    	$data = new Data();
    	
    	$data->judul = 'carapesan';
    	$data->title = 'Pesan Accounting Service - FAC-Institute';
    	$data->pagedesc = '';
    	$data->pagekeywords = '';
    	$data->View('homepage/accountingservice/carapesan.accsvc',$data);
    }

    // @url /accountingservice/jenislayanan
    public static function AccountingServiceJenisLayanan(){
    	$data = new Data();
    	
    	$data->judul = 'jenislayanan';
    	$data->title = 'Layanan Accounting Service - FAC-Institute';
    	$data->pagedesc = '';
    	$data->pagekeywords = '';
    	$data->View('homepage/accountingservice/jenislayanan.accsvc',$data);
    }


    public static function KursusAccurate(){
    	$data = new Data();
    	$data->judul = 'kursusaccurate';
    	$data->title = 'Kursus Accurate - FAC-Institute';
    	$data->pagedesc = '';
    	$data->pagekeywords = '';
    	$data->View('homepage/kursus/index.kursus',$data);
    }

    public static function JadwalKursusAccurate(){
    	$data = new Data();
    	$data->Model('Kursus');
    	
    	$course = new Kursus();
    	
    	$data->listcurmon = $course->getJadwal(date('m'),date('Y'));
    	$data->listnexmon = $course->getJadwal(date('m')+1,date('Y'));
    	
    
    	
    	$data->judul = 'jadwalkursusaccurate';
    	$data->title = 'Jadwal Kursus Accurate - FAC-Institute';
    	$data->pagedesc = '';
    	$data->pagekeywords = '';
    	$data->View('homepage/kursus/jadwal.kursus',$data);
    }

    public static function HargaKursusAccurate(){
    	$data = new Data();
    	$data->judul = 'hargakursusaccurate';
    	$data->title = 'Harga Kursus Accurate - FAC-Institute';
    	$data->pagedesc = '';
    	$data->pagekeywords = '';
    	$data->View('homepage/kursus/harga.kursus',$data);
    }

    public static function OrderKursusAccurate(){
    	$data = new Data();
    	$data->judul = 'orderkursusaccurate';
    	$data->title = 'Order Kursus Accurate - FAC-Institute';
    	$data->pagedesc = '';
    	$data->pagekeywords = '';
    	$data->View('homepage/kursus/carapesan.kursus',$data);
    }



    public static function SupportAfterTraining(){
    	$data = new Data();
    	$data->judul = 'SupportAfterTraining';
    	$data->title = 'Support After Training - FAC-Institute';
    	$data->pagedesc = '';
    	$data->pagekeywords = '';
    	$data->View('homepage/support/aftertraining.support',$data);
    }

    public static function PremiumSupport(){
    	$data = new Data();
    	$data->judul = 'Premium Support';
    	$data->title = 'Premium Support - FAC-Institute';
    	$data->pagedesc = '';
    	$data->pagekeywords = '';
    	$data->View('homepage/support/premium.support',$data);
    }

    public static function MyLocation(){
    	$data = new Data();
    	$data->judul = 'mylocation';
    	$data->title = 'FAC Location - FAC-Institute';
    	$data->pagedesc = '';
    	$data->pagekeywords = '';
    	$data->View('homepage/callus/fac.location',$data);
    }


    public static function Download(){
    	$data = new Data();
    	$data->judul = 'DownloadAccurate';
    	$data->title = 'FAC Download - FAC-Institute';
    	$data->pagedesc = '';
    	$data->pagekeywords = '';
    	$data->View('homepage/support/download.support',$data);
    }


    public static function TrainingFeedback($idtrans,$trainer=''){
    	$data = new Data();
    	$data->Model('Googlecalendarmodel');
    	$gocal = new Googlecalendarmodel();
    	
    	$data->JustInclude('administrasi/functions/Fsecurity');
    	
    	$iddd = AES256($idtrans,'d');
    	$idata = $gocal->GetStaffUtama($iddd);
    	
    // 	print_r($idata);
    	
    	
    	if(isset($_POST['in'])){
    	    $data->Model('Querybuilder');
    	    $db = $db = new QueryBuilder();
    	   // print_r($_POST['in']);
        	$r =   $db->InsertData('trainer_rating',[
        	       'ssource'=>$iddd,
        	       'stoken'=>md5($_POST['in']['u'].$iddd),
        	       'strainer'=>$_POST['in']['u'],
        	       'imastery'=>$_POST['in']['mas'],
        	       'idelivery'=>$_POST['in']['del'],
        	       'iattd'=>$_POST['in']['atd'],
        	       'iattair'=>$_POST['in']['atr']
        	 ]);
        	 
        	 if($r){
        	     echo "<script>alert('Terimakasih atas feedback yang anda berikan, customer adalah prioritas kami.');</script>";
        	     echo "<script>location.replace('".$data->base_url."');</script>";
        	 }else{
        	     echo "<script>alert('Terdapat Kesalahan sistem.');</script>";
        	     echo "<script>location.replace('".$data->base_url."');</script>";
        	 }
        	 
    	}
    	
    	$data->nama_staff = $idata[0]['nama'];
    	$data->user_staff = $idata[0]['username'];
    	$data->memberpic = $idata[0]['gambar'];
    	$data->judul = 'Training Feedback';
    	$data->title = 'Training Feedback - FAC-Institute';
    	$data->pagedesc = 'Penilaian layanan atas jasa training ACCURATE FAC - Institute';
    	$data->pagekeywords = '';
    	$data->View('homepage/training/feedback.training',$data);
    }


}