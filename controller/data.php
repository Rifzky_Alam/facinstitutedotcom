<?php 

class Data{
    public $homedir = '/home/facinsti/public_html/';//
	// public $homedir = '/Applications/XAMPP/xamppfiles/htdocs/facinstitute/';
    public $base_url = "https://fac-institute.com/";//"http://localhost/facinstitute/";
 	public $title;
 	public $subtitle;
 	public $table;
 	public $me_url;
 	public $page;

 	public function Model($file){
		include_once $this->homedir.'model/'.$file.'.php';
	}

	public function IncludeFileWithData($path,$data){
		include_once $this->homedir.$path;
	}

	public function JustInclude($path){
		include_once $this->homedir.$path.'.php';	
	}

	public function View($path,$data){
		include_once $this->homedir.'view/'.$path.'.php';
	}

	public function Lib($file){
		include_once $this->homedir.'library/'.$file.'.php';
	}

 	public function __construct(){
		// $currentPath = $_SERVER['PHP_SELF'];
	  	// $pathInfo = pathinfo($currentPath);
  		// $hostName = $_SERVER['HTTP_HOST'];
  		// $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https://'?'https://':'http://';
 		// $this->base_url = 'https://'.$hostName."/";
 		// $this->base_url = 'http://'.$hostName."/rikza/";
 	}
}