<?php 
include_once 'data.php';
class Client{
    // unfinished module
    // * sending email when a client has succesfully registered
    // * export data to existing perusahaan and customer
    // * soft delete temp_cust and temp_usaha
    public static function OrderTraining(){
    	$data = new Data();   	
    	$data->Model('Querybuilder');
    	$db = new QueryBuilder();
    	$data->JustInclude('libraries/functions/security.func');
    	if (isset($_POST['in'])) {
    		$idusaha = md5($_POST['in']['namausaha'].$_POST['in']['namausaha']);
    		$jenisusaha = "";
    		$varian = "";

    		if (isset($_POST['in']['jnsUsaha'])) {
    			$jenisusaha = implode('#', $_POST['in']['jnsUsaha']);
    		}

    		if (isset($_POST['in']['jnsAccurate'])) {
    			$varian = implode('#', $_POST['in']['jnsAccurate']);
    		}

    		$arr = [
    			'tu_id' => $idusaha,
    			'tu_nama' => $_POST['in']['namausaha'],
    			'tu_email' => $_POST['in']['email'],
    			'tu_telepon' => $_POST['in']['telepon'],
    			'tu_alamat' => $_POST['in']['alamat'],
    			'tu_kota' => $_POST['in']['kota'],
    			'tu_provinsi' => $_POST['in']['provinsi'],
    			'tu_ju' => $jenisusaha,
    			'tu_kju' => $_POST['in']['ketJenisUsaha'],
    			'tu_va' => $varian,
    			'tu_map' => $_POST['in']['map'],
    		];
    		$r = $db->InsertData('temp_usaha',$arr);

    		if ($r) {
    			header('Location: '.$data->base_url.'client/daftar-training/'.securitycode($idusaha,'e'));
    		} else {
    			echo "<script>alert('Perusahaan telah terdaftar');</script>";
    			echo "<script>location.replace('".$data->base_url."client/daftar-training/".securitycode('-','e')."');</script>";
    		}
    	}

    	$data->View('daftaronline/training-accurate/vreg1.trainingacc',$data);
    }

    public static function OrderTraining2($idusaha){
        $data = new Data();
        $data->Model('Querybuilder');
        

        $db = new QueryBuilder();
        $data->JustInclude('libraries/functions/security.func');
        

        if (isset($_POST['in'])) {
            // preparation
            $data->Model('Mail'); //include mail system
            // email body
            $data->JustInclude('view/email/vmaster.mail');
            $data->JustInclude('view/email/notifdaftar');
            $content = HeadMail();
            $content .= EmailContent();
            $content .= Footer();
            // end email body

            $arr = [
                'tc_id' => md5($_POST['in']['email']),//md5($_POST['in']['namausaha'].$_POST['in']['namausaha']),
                'id_usaha' => securitycode($idusaha,'d'),
                'tc_nama' => $_POST['in']['nama'],
                'tc_email' => $_POST['in']['email'],
                'tc_telepon' => $_POST['in']['telepon'],
                'tc_jabatan' => $_POST['in']['jabatan'],
                'tc_marketing' => $_POST['in']['marketing'],
                'tc_agenda' => $_POST['in']['agenda'],
                'tc_jp' => $_POST['in']['pengguna']
            ];
            $r = $db->InsertData('temp_cust',$arr);

            if ($r) {
                // sending email
                 #code here
                $surat = new FACMail();
                $dataEmail = array(
                    'to' => $_POST['in']['email'],
                    'nama'=> '',
                    'subject'=> 'FAC - Accurate Online Registration System of '.$_POST['in']['nama'],
                    'content'=> $content
                );
                $dataEmail = (object) $dataEmail;
                $surat->sendMail($dataEmail);

                // redirect
                echo "<script>location.replace('".$data->base_url."client/registersuccess');</script>";
            } else {
                echo "<script>alert('Terjadi kesalahan atau anda sudah terdaftar dalam sistem kami.');</script>";
                echo "<script>location.replace('".$data->base_url."');</script>";
            }

        }

        // echo securitycode($idusaha,'d');

        $data->idusaha = $idusaha;
        $data->View('daftaronline/training-accurate/vreg2.trainingacc',$data);
    }

    public static function SuccessRegister(){
    	$data = new Data();
    	$data->View('daftaronline/training-accurate/vregsuccess.trainingacc',$data);
    }

    public static function RegisteredClientData(){
    	$data = new Data();
    	$data->Model('Querybuilder');
    	$data->JustInclude('administrasi/session/session-class');
    	$sesi = new Sessionz();
    	$db = new QueryBuilder();
    	$sesi->AdminMarketing();
    	$arr = ['tu_id','tu_nama','tu_email','tu_telepon','tu_alamat','tu_kota','tu_provinsi','tu_ju','tu_kju','tu_va','tu_map','tc_id','tc_nama','tc_email','tc_telepon','tc_jabatan','tc_marketing','tc_agenda','tc_jp'];
    	$data->judul = 'FAC || Admin - Registrasi Online';
    	$data->username = $_SESSION['admin']['nama'];
    	$data->headertext = 'Data Customer Register Online';
    	$data->lists = $db->FetchJoin(['*'],'temp_cust',array($db->getJoin('LEFT','temp_usaha','temp_usaha.tu_token','temp_cust.reg_token')));//$db->FetchJoin($arr,'temp_usaha',array($db->getJoin('LEFT','temp_cust','tu_id','id_usaha')));
    	$data->IncludeFileWithData('administrasi/view/registeronline/vlistcust.ro.php',$data);
    	// print_r(json_encode($data->lists));
    }

    public static function DetailRegCust($id){
    	$data = new Data();
    	$data->Model('Querybuilder');
    	$data->JustInclude('administrasi/session/session-class');
        $db = new QueryBuilder();
        $sesi = new Sessionz();
        $sesi->AdminMarketing();

        // post customer
        if (isset($_POST['in'])) {
            print_r($_POST['in']);
        }
        // end post customer

        // post usaha
        if (isset($_POST['inu'])) {
            print_r($_POST['inu']);
        }
        // end post usaha 
        $db->setOrder('marketing_nama','ASC');
        $data->listmarketing = $db->FetchColumns(['marketing_id','marketing_nama'],'new_marketing');
        // print_r($data->listmarketing);
        $db->setOrder('','');
    	$arr = ['*'];
    	$r = $db->FetchJoinWhereSingleRow($arr,'temp_cust',
            array($db->getJoin('LEFT', 'temp_usaha','temp_usaha.tu_token','temp_cust.reg_token')),
            array($db->GetCond('tc_id','=',$id)));
        $data->jenisusahalist = $db->Fetch('jenis_usaha');
        $data->versiaccuratelist = $db->Fetch('versi_accurate');
        //FetchOneRow($arr,'temp_cust',array($db->GetCond('tc_id','=',$id)));
    	// print_r(json_encode($r));
    	$data->nama_cust = $r['tc_nama'];
    	$data->email_cust = $r['tc_email'];
    	$data->telp_cust = $r['tc_telepon'];
    	$data->jabatan = $r['tc_jabatan'];
        $marketingraw = explode('#', $r['tc_marketing']);
        $data->cabang = $marketingraw[0];

        $cabang = $db->FetchJoinWhereSingleRow(['nama'],'perusahaan',array($db->getJoin('INNER','cabang','cabang.id_perusahaan','perusahaan.id')),array($db->GetCond('cabang.id_cabang','=',$data->cabang)));
        // print_r($cabang);
        $data->cabang = $cabang['nama'];
    	$data->marketing = $marketingraw[1];
    	$data->agenda = @$r['tc_agenda'];
    	$data->jenis_pengguna = $r['tc_jp'];
    	$data->timestamp = $r['updated_at'];
        $data->namausaha = $r['tu_nama'];
        $data->teleponusaha = $r['tu_telepon'];
        $data->emailusaha = $r['tu_email'];
        $data->alamatusaha = $r['tu_alamat'];
        $data->provinsi = $r['tu_provinsi'];
        $data->kota = $r['tu_kota'];
        $data->kecamatan = $r['tu_kecamatan'];
        $data->jenisusaha = $r['tu_ju'];
        if ($r['tu_ju']=='') {
            $data->jenisusahaids = array();
        }else{
            $data->jenisusahaids = explode('#', $r['tu_ju']);
        }

        $data->ketjenisusaha = $r['tu_kju'];
        $data->versiaccurate = $r['tu_va'];
        if ($r['tu_va']=='') {
            $data->vaids = array();
        } else {
            $data->vaids = explode('#', $r['tu_va']);
        }
        $data->mapusaha = $r['tu_map'];

        $data->subtitle = 'Export Data Registrasi Online';
    	$data->judul = 'FAC || Admin - Registrasi Online';
    	$data->username = $_SESSION['admin']['nama'];
    	$data->headertext = 'Detail  Customer Register Online';
    	$data->IncludeFileWithData('administrasi/view/registeronline/vdetailcust.ro.php',$data);
    }

    public static function DetailRegCompany($id){
    	$data = new Data();
    	$data->Model('Querybuilder');
    	$data->JustInclude('administrasi/session/session-class');
        $db = new QueryBuilder();
        $sesi = new Sessionz();
        $sesi->AdminMarketing();
    	$arr = ['tu_id','tu_nama','tu_email','tu_telepon','tu_alamat','tu_kota','tu_provinsi','tu_ju','tu_kju','tu_va','tu_va','tu_map','tu_exported'];
    	$r = $db->FetchOneRow($arr,'temp_usaha',array($db->GetCond('tu_id','=',$id)));
    	// print_r(json_encode($r));

        if (isset($_POST['in'])) {
            print_r($_POST['in']);
        }

        $data->judul = 'FAC || Admin - Registrasi Online';
        $data->username = $_SESSION['admin']['nama'];
        $data->headertext = 'Detail Usaha Register Online';
        $data->idusaha = $r['tu_id'];
        $data->nama=$r['tu_nama'];
        $data->email=$r['tu_email'];
        $data->telepon=$r['tu_telepon'];
        $data->kota = $r['tu_kota'];
        $data->jenisusaha=$r['tu_ju'];
        $data->ketjenisusaha=$r['tu_kju'];
        $data->provinsi=$r['tu_provinsi'];
        $data->alamat=$r['tu_alamat'];
        $data->varian = $r['tu_va'];
        $data->map = $r['tu_map'];
        $data->alamat_training = $r['tu_alamat'];

        $data->IncludeFileWithData('administrasi/view/registeronline/vdetailcompany.ro.php',$data);
    }

    public static function SendEmail(){
    	$data = new Data();
    	$data->JustInclude('view/email/vmaster.mail');
    	$data->JustInclude('view/email/notifdaftar');
    	$content = HeadMail();
    	$content .= EmailContent();
    	$content .= Footer();
    	 $content;
    }

    public static function Kuisioner($idtransaksi){
        $data = new Data();
        echo 'oke!';
    }
    

    public static function OrderAccService(){
    	echo 'Order Accounting Service';
    }

    public static function Questionnaire(){
    	echo 'Questionnaire';
    }

    public static function base_url(){
    	$data = new Data();
    	return $data->base_url;
    }

    public static function Coba(){
    	return 0;
    }

}