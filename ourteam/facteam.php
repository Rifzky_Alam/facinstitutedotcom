<?php include_once 'baseurl.php'; ?>
<html class="no-js" lang="id">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="google-site-verification" content="EfNVtXecxElbcbugY59SeKXJuMzO67bjVvTvlHCc_CM" />
    <meta name="msvalidate.01" content="D925497D8F382164FF0CE0267D381FDA" />
    <meta name="thumbnailUrl" content="https://www.fac-institute.com/administrasi/artikel/images/7cabb38e259c0d5b53f9bb219daf410f.jpg?w=650" />
    <meta name="description" content="FAC - Kami Melayani Training Software Akuntansi ACCURATE dan Accounting Service Untuk Perusahaan Anda" />
    <meta content="First Asian Consulting. Konsultan Akuntansi & Lembaga Pendidikan Komputerisasi Akuntansi" />
    <meta name="keywords" content="Training,Akuntansi,ACCURATE,Jasa">
    <meta name="author" content="Rifzky Alam, Dino Damara">
    <title>FAC Institute</title>
    <link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/w3.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/animate.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,100,300,500,600,700c" type="text/css" />
 <style>
 .w3-top{
   font-family: "Raleway", sans-serif;
 }
 body, html, h3 {
     font-family: 'PT Sans', sans-serif;
 }
 p,li{
   font-size: 16px;
 }

 .centered {
margin: 0 auto;
}

 </style>


<link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/style.css">


<script src="<?php echo getBaseUrl() ?>assets/homepage/js/jquery-2.1.4.min.js"></script>




</head>
<body>


    <h4>header</h4>
    <?php include_once 'top-nav.php'; ?>


<div class="w3-row w3-container">

<div class=" w3-content w3-padding-16" style="max-width:1500px">


<div class="w3-container">


  <!-- Team Container -->
  <div class="w3-container w3-padding-64 w3-center" id="team">
  <h2><b>OUR TEAM</b></h2>
  <h3>Professional Trainer</h3>

  <div class="w3-row w3-padding-16"><br>

  <div class="w3-quarter">
    <img src="<?php echo getBaseUrl() ?>_hazelnut/img/ridho_Fotor.jpg" alt="Boss" style="width:45%" class="w3-circle w3-hover-opacity">
    <p>RIZKY RIDHO HANDIKA</p>
  </div>
  <div class="w3-quarter">
    <img src="<?php echo getBaseUrl() ?>_caramel/assets/img/trainer/DSC_0060_Fotor.jpg" alt="Boss" style="width:45%" class="w3-circle w3-hover-opacity">
    <p>EDWIN</p>
  </div>
  <div class="w3-quarter">
    <img src="<?php echo getBaseUrl() ?>_hazelnut/img/lukman_Fotor.jpg" alt="Boss" style="width:45%" class="w3-circle w3-hover-opacity">
    <p>LUKMAN BIJAK BESTARI</p>
  </div>
  <div class="w3-quarter">
    <img src="<?php echo getBaseUrl() ?>_hazelnut/img/lulu.jpg" alt="Boss" style="width:45%" class="w3-circle w3-hover-opacity">
    <p>LULU NAZIFA R</p>
  </div>
  <div class="w3-quarter">
    <img src="<?php echo getBaseUrl() ?>_caramel/assets/img/trainer/DSC_0299.jpg" alt="Boss" style="width:45%" class="w3-circle w3-hover-opacity">
    <p>RIZKY RIDHO HANDIKA</p>
  </div>
  <div class="w3-quarter">
    <img src="<?php echo getBaseUrl() ?>_hazelnut/img/seindy_Fotor.jpg" alt="Boss" style="width:45%" class="w3-circle w3-hover-opacity">
    <p>ANNISA SEINDY</p>
  </div>
  <div class="w3-quarter">
    <img src="<?php echo getBaseUrl() ?>_caramel/assets/img/trainer/yg.jpg" alt="Boss" style="width:45%" class="w3-circle w3-hover-opacity">
    <p>PRAYUGO RAHARJO</p>
  </div>
  <div class="w3-quarter">
    <img src="<?php echo getBaseUrl() ?>_hazelnut/img/iman_Fotor.jpg" alt="Boss" style="width:45%" class="w3-circle w3-hover-opacity">
    <p>IMAN</p>
  </div>

  <div class="w3-quarter">
    <img src="<?php echo getBaseUrl() ?>_hazelnut/img/bani_Fotor.jpg" alt="Boss" style="width:45%" class="w3-circle w3-hover-opacity">
    <p>BANI AKBAR KHATAMI</p>
  </div>

  <div class="w3-quarter">
    <img src="<?php echo getBaseUrl() ?>_hazelnut/img/fauzi_Fotor.jpg" alt="Boss" style="width:45%" class="w3-circle w3-hover-opacity">
    <p>MUHAMMAD RIZKY FAUZI</p>
  </div>

  <div class="w3-quarter">
    <img src="<?php echo getBaseUrl() ?>_hazelnut/img/ade_Fotor.jpg" alt="Boss" style="width:45%" class="w3-circle w3-hover-opacity">
    <p>ADE MAULANA SAPUTRA</p>
  </div>



  <!-- <div class="centered" style="align: center;max-width:800px">
  <div class="w3-third">
    <img src="<?php echo getBaseUrl() ?>_caramel/assets/img/trainer/bst.jpg" alt="Boss" style="width:120px" class="w3-circle w3-hover-opacity">
    <p>BUSTOMI ISMAIL</p>
  </div>
  <div class="w3-third">
    <img src="<?php echo getBaseUrl() ?>_caramel/assets/img/trainer/DSC_0305 - Copy.jpg" alt="Boss" style="width:120px" class="w3-circle w3-hover-opacity">
    <p>RINALDI</p>
  </div>
  <div class="w3-third">
    <img src="<?php echo getBaseUrl() ?>_caramel/assets/img/trainer/19984522_110520756257503_4719306856504229888_n.jpg" alt="Boss" style="width:120px" class="w3-circle w3-hover-opacity">
    <p>IMAN</p>
  </div>
</div> -->

</div>



  <h3>Marketing</h3>
  <div class="w3-row">
  <div class="w3-half">
    <img src="<?php echo getBaseUrl() ?>_caramel/assets/img/trainer/DSC_0068.jpg" alt="Boss" style="width:200px" class="w3-circle w3-hover-opacity">
    <p>LINDA KARTINAH</p>
  </div>
  <div class="w3-half">
    <img src="<?php echo getBaseUrl() ?>_caramel/assets/img/trainer/CIMG7065_Fotor.jpg" alt="Boss" style="width:200px" class="w3-circle w3-hover-opacity">
    <p>SITI NURHASANAH</p>
  </div>
  </div>

<br>
  <h3>Customer Support</h3>
  <div class="w3-row">
  <div class="">
    <img src="<?php echo getBaseUrl() ?>_caramel/assets/img/trainer/DSC_0064.jpg" alt="Boss" style="width:200px" class="w3-circle w3-hover-opacity">
    <p>JULIANA DWI ASRI P</p>
  </div>
  </div>

<br>
  <h3>General Support</h3>

  <div class="w3-row">
  <div class="w3-half">
    <img src="<?php echo getBaseUrl() ?>_caramel/assets/img/trainer/DSC_0072 copy.jpg" alt="Boss" style="width:200px" class="w3-circle w3-hover-opacity">
    <p>DINO DAMARA P</p>
      <!-- <p style="margin-top:-10px;font-style:italic">Web Designer</p> -->
  </div>
  <div class="w3-half">
    <img src="<?php echo getBaseUrl() ?>_caramel/assets/img/trainer/DSC_0236.jpg" alt="Boss" style="width:200px" class="w3-circle w3-hover-opacity">
    <p>RIFZKY ALAM</p>
      <!-- <p style="margin-top:-10px;font-style:italic">Programmer</p> -->
  </div>
  </div>







  </div>
</div>



</div>
  </div>












<?php include_once 'modal-order.php'; ?>



</body>
</html>
