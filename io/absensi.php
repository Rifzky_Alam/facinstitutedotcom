<?php

if (isset($_POST['dataAbsen'])) {
	header("Content-Type: application/x-www-form-encoded");
	include_once '../model/Petugas.php';
	$absen = new ControlPetugas();
	$objInput = array(
		'id' =>$_POST['dataAbsen']['id'] ,
		'username'=>$_POST['dataAbsen']['username'],
		'absen_masuk'=>$_POST['dataAbsen']['absenMasuk'],
		'lokasi_masuk'=>$_POST['dataAbsen']['lokasiMasuk'],
		'absen_pulang'=>$_POST['dataAbsen']['absenPulang'],
		'lokasi_pulang'=>$_POST['dataAbsen']['lokasiPulang'],
		'tanggal_absen'=>$_POST['dataAbsen']['tanggalAbsen'],
		'nama_perusahaan'=>$_POST['dataAbsen']['namaPerusahaan']
					 );

	$objInput = (object) $objInput;
	if ($absen->absenIO($objInput)) {
		echo "{'response':'1','message':'Anda berhasil absen masuk untuk hari ini'}";
	}else{
		echo "{'response':'0','message':'Anda gagal absen :('}";
	}
	
}

if (isset($_GET['rq'])&&$_GET['rq']=='1') {
	include_once '../model/Petugas.php';
	$control = new ControlPetugas();
	echo $control->getDataPendaftarToday(date('Y-m-d'));
}

if (isset($_GET['rq'])&&$_GET['rq']=='2') {
	include_once '../model/Pendaftar.php';
	$control = new Pendaftar();
	echo $control->autoCompleteNamaPerusahaan();
}	

?>