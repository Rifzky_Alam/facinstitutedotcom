<?php 
include_once 'Koneksi.php';
date_default_timezone_set("Asia/Jakarta"); 

class Perusahaan extends Koneksi{
	private $id;
	private $namausaha;
	private $alamat;
	private $email;
	private $telepon;
	private $kota;
	private $provinsi;
	private $kecamatan;
	private $jnsusaha;
	private $ketjnsusaha;
	private $accurate;
	private $map;

	public function getId(){
	    return $this->id;
	}

	public function setId($id){
	    $this->id = $id;
	}

	public function getNamausaha(){
	    return $this->namausaha;
	}

	public function setNamausaha($namausaha){
	    $this->namausaha = $namausaha;
	}

	public function getAlamat(){
	    return $this->alamat;
	}

	public function setAlamat($alamat){
	    $this->alamat = $alamat;
	}

	public function getEmail(){
	    return $this->email;
	}

	public function setEmail($email){
	    $this->email = $email;
	}

	public function getKota(){
	    return $this->kota;
	}

	public function setKota($value){
	    $this->kota = $value;
	}

	public function getProvinsi(){
	    return $this->provinsi;
	}

	public function setProvinsi($value){
	    $this->provinsi = $value;
	}

	public function getKecamatan(){
        return $this->kecamatan;
    }

    public function setKecamatan($kecamatan){
        $this->kecamatan = $kecamatan;
    }

	public function getTelepon(){
	    return $this->telepon;
	}

	public function setTelepon($telepon){
	    $this->telepon = $telepon;
	}

	public function getJnsusaha(){
	    return $this->jnsusaha;
	}

	public function setJnsusaha($jnsusaha){
	    $this->jnsusaha = $jnsusaha;
	}

	public function getKetjnsusaha(){
	    return $this->ketjnsusaha;
	}

	public function setKetjnsusaha($ketjnsusaha){
	    $this->ketjnsusaha = $ketjnsusaha;
	}

	public function getMap(){
	    return $this->map;
	}

	public function setMap($map){
	    $this->map = $map;
	}

	public function getAccurate(){
	    return $this->accurate;
	}

	public function setAccurate($value){
	    $this->accurate = $value;
	}

	
	function __construct(){
		$this->dbHost = $this->bukaKoneksi();
	}
	
	public function FetchDataPerusahaanSupport(){
	    $query = "SELECT perusahaan.id AS id,perusahaan.nama AS nama_usaha,ket_jenis_usaha,
        IFNULL((
	        SELECT GROUP_CONCAT(versi_accurate.va_nama SEPARATOR ' # ') 
            FROM versi_accurate 
            INNER JOIN usaha_versi_accurate ON usaha_versi_accurate.uva_id_varian=versi_accurate.va_id
            WHERE usaha_versi_accurate.uva_source=perusahaan.id
        ),'') AS versiaccurate,
        IFNULL((
            SELECT GROUP_CONCAT(jenis_usaha.jenis_usaha SEPARATOR ' # ')
            FROM jenis_usaha
            INNER JOIN perusahaan_jns_usaha ON perusahaan_jns_usaha.id_jenis_usaha=jenis_usaha.id
            WHERE perusahaan_jns_usaha.id_company=perusahaan.id
        ),'') AS jenisusaha,
        IFNULL(users.nama,'') AS pic
        FROM `perusahaan` 
        LEFT JOIN cs_suppdet ON cs_suppdet.sfkusaha=perusahaan.id
        LEFT JOIN users ON users.username=cs_suppdet.spic
        WHERE `p_status`='51' ORDER BY perusahaan.p_created_at DESC";
        
        $statement=$this->dbHost->prepare($query);
        // @$statement->bindParam(':idusaha',$idusahaa, PDO::PARAM_STR,35);
		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function DownloadzDataSupportForMeeting($src='',$offset='',$limit=''){
	    if ($src!='') { // not finished yet!
    	    $query="WITH cust_baru AS (SELECT new_customer.id_cust AS id
                FROM `new_customer`
                INNER JOIN new_transaksi ON new_transaksi.trans_cust_id=new_customer.id_cust
                INNER JOIN perusahaan ON perusahaan.id = new_transaksi.trans_id_usaha
                INNER JOIN new_invoice ON new_invoice.inv_id_trans=new_transaksi.trans_id
                WHERE new_invoice.inv_status > '0'
                AND new_customer.id_cust NOT IN (
                SELECT new_customer.id_cust
                FROM `new_customer`
                INNER JOIN new_transaksi ON new_transaksi.trans_cust_id=new_customer.id_cust
                INNER JOIN perusahaan ON perusahaan.id = new_transaksi.trans_id_usaha
                INNER JOIN new_invoice ON new_invoice.inv_id_trans=new_transaksi.trans_id
                WHERE YEAR(new_transaksi.trans_date) < YEAR(CURDATE())
                AND new_invoice.inv_status > '0'
                )
                AND new_transaksi.trans_date BETWEEN '".$src['fd']."' AND '".$src['td']."')
            SELECT `sticket`, DATE_FORMAT(`dcurdate`,'%d/%m/%Y') AS tanggal, `perusahaan`.`nama` AS `nama_usaha`, new_customer.nama_cust AS `sname`, new_customer.telp_cust AS `sphone`, new_customer.email_cust AS `semail`, `sisstitle`, `ssolution`, desc_tbl.dt_desc AS `media`,`iflmedia`, users.nama AS `spic`, `istatus`, `snotes`,CASE WHEN cust_baru.id IS NULL THEN 'old' ELSE 'new' END AS status_cust,cs_suppdet.sfkusaha AS idusaha,
                    IFNULL(
                        (
                        SELECT users.nama
                        FROM users
                        INNER JOIN absensi ON absensi.username=users.username
                        INNER JOIN laporan_harian ON laporan_harian.id_absen = absensi.id
                        WHERE laporan_harian.peran='1'
                        AND absensi.nama_perusahaan=idusaha
                        ORDER BY absensi.tanggal_absen DESC
                        LIMIT 0,1
                    ),'-') AS last_trainer,
                    (
                        SELECT GROUP_CONCAT(users.nama SEPARATOR '#') 
                        FROM users
                        INNER JOIN absensi ON absensi.username=users.username
                        WHERE absensi.nama_perusahaan=idusaha
                    ) AS trainers,
                    (
                        SELECT COUNT(fac_calendar.id) 
                         FROM fac_calendar
                        INNER JOIN new_transaksi ON new_transaksi.trans_id=fac_calendar.acara
                        WHERE new_transaksi.trans_id_usaha=idusaha
                    ) AS totaltraining
                    FROM `cs_suppdet`
                    INNER JOIN users ON users.username=cs_suppdet.spic
                    INNER JOIN perusahaan
                    ON perusahaan.id = cs_suppdet.sfkusaha
                    INNER JOIN desc_tbl
                    ON desc_tbl.dt_relate_tbl='cs_suppdet'
                    AND desc_tbl.dt_flag=cs_suppdet.iflmedia
                    INNER JOIN new_customer ON new_customer.id_cust=cs_suppdet.sfkclient
                 LEFT JOIN cust_baru ON cust_baru.id=new_customer.id_cust
                    WHERE cs_suppdet.dcurdate BETWEEN '".$src['fd']."' AND '".$src['td']."'
                ORDER BY cs_suppdet.dcurdate ASC";
	        $statement=$this->dbHost->prepare($query);
		    $statement->execute();
		    return $statement->fetchAll(PDO::FETCH_ASSOC);
	        
	    }else{
	        return array();
	    }
    	    
	}
	
	public function FetchDataSupport($src='',$offset='',$limit=''){
	    $query = "SELECT `sticket`, DATE_FORMAT(`dcurdate`,'%d/%m/%Y') AS tanggal, `perusahaan`.`nama` AS `nama_usaha`, new_customer.nama_cust AS `sname`, new_customer.telp_cust AS `sphone`, new_customer.email_cust AS `semail`, `sisstitle`, `ssolution`, desc_tbl.dt_desc AS `media`,`iflmedia`, users.nama AS `spic`, `istatus`, `snotes` 
        FROM `cs_suppdet` 
        INNER JOIN users ON users.username=cs_suppdet.spic
        INNER JOIN perusahaan 
        ON perusahaan.id = cs_suppdet.sfkusaha
        INNER JOIN desc_tbl
        ON desc_tbl.dt_relate_tbl='cs_suppdet' 
        AND desc_tbl.dt_flag=cs_suppdet.iflmedia
		INNER JOIN new_customer ON new_customer.id_cust=cs_suppdet.sfkclient";
        if ($src!='') {
        	$query .= " WHERE perusahaan.nama LIKE '%".$src['namausaha']."%' 
        	AND new_customer.nama_cust LIKE '%".$src['klien']."%' 
        	AND cs_suppdet.sisstitle LIKE '%".$src['topik']."%' ";
        	if ($src['petugas']!='') {
        		$query .= " AND cs_suppdet.spic='".$src['petugas']."'";
        	}
        	
        	if($src['fd']!=''&&$src['td']!=''){
        	    $query.= " AND cs_suppdet.dcurdate BETWEEN '".$src['fd']."' AND '".$src['td']."' ";
        	}
        }
        $query.=" ORDER BY cs_suppdet.sticket DESC";
        if($offset!=''&&$limit!=''){
            $query .=" LIMIT $offset,$limit";
        }
        $statement=$this->dbHost->prepare($query);
        // @$statement->bindParam(':idusaha',$idusahaa, PDO::PARAM_STR,35);
		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

    public function GetDataVersiAccTidakSinkron($idusahaa){
        $query = "SELECT trans_id,trans_va,uva_id_varian from perusahaan
        INNER JOIN usaha_versi_accurate
        ON perusahaan.id=usaha_versi_accurate.uva_source
        LEFT JOIN new_transaksi ON perusahaan.id=new_transaksi.trans_id_usaha
        WHERE usaha_versi_accurate.uva_id_varian <> new_transaksi.trans_va
        AND perusahaan.id=:idusaha";
        $statement=$this->dbHost->prepare($query);
        @$statement->bindParam(':idusaha',$idusahaa, PDO::PARAM_STR,35);
		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

	public function GetDiagramLaporanByJenisAcc($id){
		$query = "SELECT `users`.`username` AS `username`,`users`.`nama` AS `nama_staff`,
		COUNT(`usaha_versi_accurate`.`uva_id`) AS `jumlah` 
		FROM `users` 
		INNER JOIN `absensi` ON `absensi`.`username`=`users`.`username` 
		INNER JOIN `laporan_harian` ON `laporan_harian`.`id_absen`=`absensi`.`id` 
		INNER JOIN `perusahaan` ON `perusahaan`.`id`=`absensi`.`nama_perusahaan` 
		LEFT JOIN `usaha_versi_accurate` ON `usaha_versi_accurate`.`uva_source`=`perusahaan`.`id` 
		WHERE `usaha_versi_accurate`.`uva_id_varian`='$id'
		AND perusahaan.id NOT IN ('f4e745e1015af1c15ebcb70b77f1426c','d5eb8c090d191587bb8a4b875bae4fed','08964cc6406ac24b00bc774eceb799c1')
		GROUP BY `users`.`username`;";
		$statement=$this->dbHost->prepare($query);

		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}	

	public function GetDiagramLaporanByJenisUsaha($idjenisusaha){
		$query = "SELECT `users`.`username` AS `username`,`users`.`nama` AS `nama_staff`,
		COUNT(`perusahaan_jns_usaha`.`uju_num`) AS `jumlah` 
		FROM `users` 
		INNER JOIN `absensi` ON `absensi`.`username`=`users`.`username` 
		INNER JOIN `laporan_harian` ON `laporan_harian`.`id_absen`=`absensi`.`id` 
		INNER JOIN `perusahaan` ON `perusahaan`.`id`=`absensi`.`nama_perusahaan` 
		LEFT JOIN `perusahaan_jns_usaha` ON `perusahaan_jns_usaha`.`id_company`=`perusahaan`.`id` 
		WHERE perusahaan_jns_usaha.id_jenis_usaha='$idjenisusaha'
		AND perusahaan.id NOT IN ('f4e745e1015af1c15ebcb70b77f1426c','d5eb8c090d191587bb8a4b875bae4fed','08964cc6406ac24b00bc774eceb799c1')
		GROUP BY `users`.`username`;";
		$statement=$this->dbHost->prepare($query);

		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}


	public function NewData(){
		$query = "INSERT INTO `perusahaan` (`id`, `nama`, `email`, `alamat`, `kota`, `provinsi`,`kec`, `telepon`, `ket_jenis_usaha`, `map`) 
		VALUES (:id, :namausaha, :email, :alamat, :kota, :provinsi, :kec, :telepon, :ketjenisusaha, :map);";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':id',$this->id, PDO::PARAM_STR,35);
		@$statement->bindParam(':namausaha',$this->namausaha, PDO::PARAM_STR,75);
		@$statement->bindParam(':email',$this->email, PDO::PARAM_STR,255);
		@$statement->bindParam(':alamat',$this->alamat, PDO::PARAM_STR,500);
		@$statement->bindParam(':kec',$this->kecamatan, PDO::PARAM_STR,40);
		@$statement->bindParam(':kota',$this->kota, PDO::PARAM_STR,40);
		@$statement->bindParam(':provinsi',$this->provinsi, PDO::PARAM_STR,75);
		@$statement->bindParam(':telepon',$this->telepon, PDO::PARAM_STR,20);
		@$statement->bindParam(':ketjenisusaha',$this->ketjnsusaha, PDO::PARAM_STR,75);
		@$statement->bindParam(':map',$this->map, PDO::PARAM_STR,75);

		if ($statement->execute()) {	
			return true;
		}else{
			return false;
		}
	}


	public function LaporanJumlahJenisUsaha(){
		$query = "SELECT `jenis_usaha`.`id` AS `id`, `jenis_usaha`,
		(SELECT COUNT(`nama`)
		FROM `perusahaan_jns_usaha` 
		INNER JOIN `perusahaan` ON `perusahaan`.`id`=`perusahaan_jns_usaha`.`id_company`
		WHERE `perusahaan_jns_usaha`.`id_jenis_usaha` = `jenis_usaha`.`id`) AS `jumlah`
		FROM `jenis_usaha`;";
		$statement=$this->dbHost->prepare($query);

		$statement->execute();
		
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function LaporanUsahaByVersiAccurate(){
		$query = "SELECT `id`, `nama`, `email`, `alamat`, `kota`, `provinsi`, `telepon`, `jenis_usaha`, `ket_jenis_usaha`, `map`,`va_nama`
		FROM `perusahaan` 
		INNER JOIN `usaha_versi_accurate` ON `usaha_versi_accurate`.`uva_source`=`perusahaan`.`id`
		INNER JOIN `versi_accurate` ON `usaha_versi_accurate`.`uva_id_varian`=`versi_accurate`.`va_id`
		WHERE `usaha_versi_accurate`.`uva_id_varian`=:versiaccurate";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':versiaccurate',$this->accurate, PDO::PARAM_STR,35);
		$statement->execute();
		
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);

	}

	public function LaporanUsahaByJenisUsaha(){
		$query = "SELECT `perusahaan`.`id` AS `id`, `nama`, `email`, `alamat`, `kota`, `provinsi`, `telepon`, `ket_jenis_usaha`, `map`,`jenis_usaha`.`jenis_usaha` AS `jenis_usaha`
		FROM `perusahaan` 
		INNER JOIN `perusahaan_jns_usaha` 
        ON `perusahaan_jns_usaha`.`id_company`= `perusahaan`.`id`
        INNER JOIN `jenis_usaha`
        ON `jenis_usaha`.`id` = `perusahaan_jns_usaha`.`id_jenis_usaha`
        WHERE `jenis_usaha`.`id`=:jenisusaha;";
        $statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':jenisusaha',$this->jnsusaha, PDO::PARAM_STR,35);
		$statement->execute();
		
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function LaporanJenisAccurate(){
		$query = "SELECT `va_id`, `va_nama`,
		(SELECT COUNT(`nama`) 
		FROM `usaha_versi_accurate`
		INNER JOIN `perusahaan` 
		ON `perusahaan`.`id`=`usaha_versi_accurate`.`uva_source`
		WHERE `usaha_versi_accurate`.`uva_id_varian`=`va_id`) AS `jumlah`
		FROM `versi_accurate`;";
		$statement=$this->dbHost->prepare($query);

		$statement->execute();
		
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function EditUsaha(){
		$query="UPDATE `perusahaan` SET `nama`=:namausaha,`email`=:email,`alamat`=:alamat,`kota`=:kota,`kec`=:kec,`provinsi`=:provinsi,`telepon`=:telepon,`ket_jenis_usaha`=:ketjenisusaha,`map`=:map WHERE `id`=:id;";
		$statement=$this->dbHost->prepare($query);
		
		@$statement->bindParam(':id',$this->id, PDO::PARAM_STR,35);
		@$statement->bindParam(':namausaha',$this->namausaha, PDO::PARAM_STR,75);
		@$statement->bindParam(':email',$this->email, PDO::PARAM_STR,255);
		@$statement->bindParam(':alamat',$this->alamat, PDO::PARAM_STR,500);
		@$statement->bindParam(':kec',$this->kecamatan, PDO::PARAM_STR,40);
		@$statement->bindParam(':kota',$this->kota, PDO::PARAM_STR,40);
		@$statement->bindParam(':provinsi',$this->provinsi, PDO::PARAM_STR,75);
		@$statement->bindParam(':telepon',$this->telepon, PDO::PARAM_STR,20);
		@$statement->bindParam(':ketjenisusaha',$this->ketjnsusaha, PDO::PARAM_STR,75);
		@$statement->bindParam(':map',$this->map, PDO::PARAM_STR,75);

		if ($statement->execute()) {	
			return true;
		}else{
			return false;
		}
	}

	public function EditMap(){
		$query="UPDATE `perusahaan` 
		SET `map`=:map 
		WHERE `id`=:id;";
		$statement=$this->dbHost->prepare($query);
		
		@$statement->bindParam(':id',$this->id, PDO::PARAM_STR,35);
		@$statement->bindParam(':map',$this->map, PDO::PARAM_STR,75);

		if ($statement->execute()) {	
			return true;
		}else{
			return false;
		}
	}

	public function NewJenisUsaha(){
		$query = "INSERT INTO `perusahaan_jns_usaha` (`id_company`, `id_jenis_usaha`) 
		VALUES (:source, :jenisusaha);";
		$statement=$this->dbHost->prepare($query);

		$statement->bindParam(':source',$this->id, PDO::PARAM_STR,35);
		$statement->bindParam(':jenisusaha',$this->jnsusaha, PDO::PARAM_STR,35);

		if ($statement->execute()) {	
			return true;
		}else{
			return false;
		}

	}

	public function NewVersiAccurate(){
		$query = "INSERT INTO `usaha_versi_accurate`(`uva_source`, `uva_id_varian`) VALUES (:source,:varian);";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':source',$this->id, PDO::PARAM_STR,35);
		@$statement->bindParam(':varian',$this->accurate, PDO::PARAM_STR,35);

		if ($statement->execute()) {	
			return true;
		}else{
			return false;
		}

	}

	public function FetchVersiAccurateUsaha(){
		$query ="SELECT `uva_id`,`va_nama` 
		FROM `versi_accurate`,`perusahaan`,`usaha_versi_accurate` 
		WHERE `versi_accurate`.`va_id`=`usaha_versi_accurate`.`uva_id_varian` 
		AND `perusahaan`.`id`=`usaha_versi_accurate`.`uva_source` 
		AND `perusahaan`.`id`=:id;";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':id',$this->id, PDO::PARAM_STR,35);

		$statement->execute();
		
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}
	
	// @url /administrasi/new-usaha?dva={val} (data yang mau di delete)
    public function FetchVersiAccurateUsahaa($idtblacc){
		$query ="SELECT `uva_id`,`va_nama` 
		FROM `versi_accurate`,`perusahaan`,`usaha_versi_accurate` 
		WHERE `versi_accurate`.`va_id`=`usaha_versi_accurate`.`uva_id_varian` 
		AND `perusahaan`.`id`=`usaha_versi_accurate`.`uva_source` 
		AND `perusahaan`.`id`=:id
		AND usaha_versi_accurate.uva_id=:uvaid;";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':id',$this->id, PDO::PARAM_STR,35);
		@$statement->bindParam(':uvaid',$idtblacc, PDO::PARAM_STR,35);

		$statement->execute();
		
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}
	
	public function FetchJenisUsahaPerusahaann($idontable){
		$query ="SELECT `uju_num`,`jenis_usaha`.`jenis_usaha` AS `jenis_usaha` 
		FROM `perusahaan_jns_usaha`,`jenis_usaha`,`perusahaan` WHERE perusahaan.id=perusahaan_jns_usaha.id_company
		AND perusahaan_jns_usaha.id_jenis_usaha=jenis_usaha.id
		AND perusahaan.id=:id
		AND perusahaan_jns_usaha.uju_num=:idontable;";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':id',$this->id, PDO::PARAM_STR,35);
        @$statement->bindParam(':idontable',$idontable, PDO::PARAM_STR,35);

		$statement->execute();
		
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}
	
	public function FetchJenisUsahaPerusahaan(){
		$query ="SELECT `uju_num`,`jenis_usaha`.`jenis_usaha` AS `jenis_usaha` 
		FROM `perusahaan_jns_usaha`,`jenis_usaha`,`perusahaan` WHERE perusahaan.id=perusahaan_jns_usaha.id_company
		AND perusahaan_jns_usaha.id_jenis_usaha=jenis_usaha.id
		AND perusahaan.id=:id;";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':id',$this->id, PDO::PARAM_STR,35);


		$statement->execute();
		
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function FetchCustServByID($id){
		$query = "SELECT `id`, `nama`, `email`, `alamat`, `kota`, `provinsi`, `telepon`, `jenis_usaha`, `ket_jenis_usaha`, `map`,
		(SELECT GROUP_CONCAT(`jenis_usaha` SEPARATOR ' # ') FROM `jenis_usaha` WHERE `id` IN (SELECT `id_jenis_usaha` FROM `perusahaan_jns_usaha` AS `uju` WHERE `uju`.`id_company`=`perusahaan`.id)) AS `jenis_usaha`,
		(SELECT GROUP_CONCAT(`va_nama` SEPARATOR ' # ') FROM `versi_accurate` WHERE `va_id` IN (SELECT `uva_id_varian` FROM `usaha_versi_accurate` AS `uva` WHERE `uva`.`uva_source`=`perusahaan`.id)) AS `versi_accurate`,
        IFNULL((SELECT GROUP_CONCAT(CONCAT(new_customer.id_cust,'|*|',new_customer.nama_cust) SEPARATOR ' # ') FROM `new_customer` WHERE new_customer.id_perusahaan=`perusahaan`.`id`),'') AS `customers`,
        CASE WHEN p_status='1' THEN 'Transaksi' WHEN p_status='51' THEN 'support' ELSE 'alien' END AS `status`
		FROM `perusahaan` 
		WHERE `perusahaan`.`id`=:id";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':id',$id, PDO::PARAM_STR,35);

		$statement->execute();
		
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function FetchSearchCs(){
		$namaoesaha = '%'.$this->namausaha.'%';
		$query = "SELECT `id`, `nama`, `email`, `alamat`, `kota`, `provinsi`, `telepon`, `jenis_usaha`, `ket_jenis_usaha`, `map`,
		(SELECT GROUP_CONCAT(`jenis_usaha` SEPARATOR ' # ') FROM `jenis_usaha` WHERE `id` IN (SELECT `id_jenis_usaha` FROM `perusahaan_jns_usaha` AS `uju` WHERE `uju`.`id_company`=`perusahaan`.id)) AS `jenis_usaha`,
		(SELECT GROUP_CONCAT(`va_nama` SEPARATOR ' # ') FROM `versi_accurate` WHERE `va_id` IN (SELECT `uva_id_varian` FROM `usaha_versi_accurate` AS `uva` WHERE `uva`.`uva_source`=`perusahaan`.id)) AS `versi_accurate`,
        IFNULL((SELECT GROUP_CONCAT(CONCAT(new_customer.id_cust,'|*|',new_customer.nama_cust) SEPARATOR ' # ') FROM `new_customer` WHERE new_customer.id_perusahaan=`perusahaan`.`id`),'') AS `customers`,
        CASE WHEN p_status='1' THEN 'Transaksi' WHEN p_status='51' THEN 'support' ELSE 'alien' END AS `status`
		FROM `perusahaan` 
		WHERE `nama` LIKE :namausaha";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':namausaha',$namaoesaha, PDO::PARAM_STR,35);

		$statement->execute();
		
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function FetchNewSearch(){
		$namaoesaha = '%'.$this->namausaha.'%';
		$query = "SELECT `id`, `nama`, `email`, `alamat`, `kota`, `provinsi`, `telepon`, `jenis_usaha`, `ket_jenis_usaha`, `map`,
		(SELECT GROUP_CONCAT(`jenis_usaha` SEPARATOR ' # ') FROM `jenis_usaha` WHERE `id` IN (SELECT `id_jenis_usaha` FROM `perusahaan_jns_usaha` AS `uju` WHERE `uju`.`id_company`=`perusahaan`.id)) AS `jenis_usaha`,
		(SELECT GROUP_CONCAT(`va_nama` SEPARATOR ' # ') FROM `versi_accurate` WHERE `va_id` IN (SELECT `uva_id_varian` FROM `usaha_versi_accurate` AS `uva` WHERE `uva`.`uva_source`=`perusahaan`.id)) AS `versi_accurate`,p_status AS status
		FROM `perusahaan` 
		WHERE `nama` LIKE :namausaha;";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':namausaha',$namaoesaha, PDO::PARAM_STR,35);

		$statement->execute();
		
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function FetchSelectedJenisUsaha(){
		$query = "SELECT * FROM `perusahaan_jns_usaha` WHERE `uju_num`=:idjenisusaha;";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':idjenisusaha',$this->jnsusaha,PDO::PARAM_STR,35);

		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
		
	}

	public function FetchSelectedVersiAccurate(){
		$query = "SELECT `uva_id`, `uva_source`, `uva_id_varian` FROM `usaha_versi_accurate` WHERE `uva_id`=:acc;";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':acc',$this->accurate,PDO::PARAM_STR,35);

		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);

	}

	public function DeleteJenisUsaha(){
		$query = "DELETE FROM `perusahaan_jns_usaha`
		WHERE `uju_num`=:idjenisusaha;";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':idjenisusaha',$this->jnsusaha, PDO::PARAM_STR,35);
		// @$statement->bindParam(':varian',$this->accurate, PDO::PARAM_STR,35);

		if ($statement->execute()) {	
			return true;
		}else{
			return false;
		}
	}

	public function DeleteVersiAccurate(){
		$query = "DELETE FROM `usaha_versi_accurate`
		WHERE `uva_id`=:acc;";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':acc',$this->accurate, PDO::PARAM_STR,35);
		// @$statement->bindParam(':varian',$this->accurate, PDO::PARAM_STR,35);

		if ($statement->execute()) {	
			return true;
		}else{
			return false;
		}
	}


	public function FetchVersiAccurate(){
		$query = "SELECT `va_id`, `va_nama` FROM `versi_accurate`;";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz = json_encode($results);
		return json_decode($dataz);
	}

	public function FetchJenisUsaha(){
		$query = "SELECT * FROM jenis_usaha;";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}


	public function InputJenisUsaha($id,$jenisusaha){
		$query = "INSERT INTO `perusahaan_jns_usaha` (`id_company`, `id_jenis_usaha`) VALUES ";

		for ($i=0; $i < count($jenisusaha) ; $i++) { 
			if ($i == count($jenisusaha)-1) {
				$query .= "('".$id."', '".$jenisusaha[$i]."')";
			} else {
				$query .= "('".$id."', '".$jenisusaha[$i]."'), ";	
			}
		}
		$query .= ";";

		$statement=$this->dbHost->prepare($query);

		// echo $query;		
		if ($statement->execute()) {	
			return true;
		}else{
			return false;
		}
	}

	public function CountCabang(){
		$query = "SELECT COUNT(`id_cabang`) AS `jumlah` FROM `cabang`;";
		$statement=$this->dbHost->prepare($query);

		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_OBJ);
		return $result->jumlah;
	}

	public function NewCabang(){
		$query = "INSERT INTO `cabang`(`id_cabang`, `id_perusahaan`) VALUES (:idcabang,:idusaha);";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':idcabang',$this->CountCabang(),PDO::PARAM_STR,35);
		@$statement->bindParam(':idusaha',$this->id,PDO::PARAM_STR,35);	

		if ($statement->execute()) {	
			return true;
		}else{
			return false;
		}		

	}

	public function InputVersiAccurate($id,$versiaccurate){
		$query = "INSERT INTO `usaha_versi_accurate` (`uva_source`, `uva_id_varian`) VALUES ";

		for ($i=0; $i < count($versiaccurate) ; $i++) { 
			if ($i == count($versiaccurate)-1) {
				$query .= "('".$id."', '".$versiaccurate[$i]."')";
			} else {
				$query .= "('".$id."', '".$versiaccurate[$i]."'), ";	
			}
		}
		$query .= ";";

		$statement=$this->dbHost->prepare($query);

		// echo $query;		
		if ($statement->execute()) {	
			return true;
		}else{
			return false;
		}
	}

	public function selectNamaPerusahaan(){
		$query="SELECT DISTINCT nama_perusahaan,email_perusahaan,alamat_lengkap,telepon_kantor,jenis_usaha,keterangan_jenis_usaha,alamat_perusahaan FROM pendaftar";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;		
	}

	public function selectByNamaPerusahaan($objek){
		$query = "SELECT * FROM `perusahaan` WHERE `nama` COLLATE UTF8_GENERAL_CI LIKE '%".$objek->np."%' ";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;	
	}

	public function editRow($objek){
		$query="UPDATE `perusahaan` SET `nama`='".$objek->np."',`email`='".$objek->email."',`alamat`='".$objek->alamat."',`kota`='".$objek->kota."',`provinsi`='".$objek->provinsi."',`telepon`='".$objek->telepon."',`jenis_usaha`='".implode(" # ", $objek->jnsUsaha)."',`ket_jenis_usaha`='".$objek->ketJenisUsaha."',`map`='".$objek->map."' WHERE `id`='".$objek->id."'";
		$statement=$this->dbHost->prepare($query);

		if ($statement->execute()) {
			return "<script>alert('Data ".$objek->np." berhasil di ubah');</script>";
		}else{
			return "<script>alert('Data ".$objek->np." gagal di ubah');</script>";
		}

	}

	public function selectByID($id){
		$query = "SELECT * FROM `perusahaan`
		LEFT JOIN `daftar_prov` ON `daftar_prov`.`prov_id`=`perusahaan`.`provinsi`
		LEFT JOIN `daftar_kota` ON `daftar_kota`.`kota_id`=`perusahaan`.`kota`
		LEFT JOIN `daftar_kecamatan` ON `daftar_kecamatan`.`kec_id`=`perusahaan`.`kec`
		WHERE `id`=:id";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':id',$id,PDO::PARAM_STR,35);

		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);	
	}

	public function selectNamaUsahaByID($id){
		$query = "SELECT nama FROM `perusahaan` WHERE `id`=:id";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':id',$id,PDO::PARAM_STR,35);

		$statement->execute();
		$hasil = $statement->fetch(PDO::FETCH_OBJ);
		return @$hasil->nama;	
	}	

	public function insertData($objek){
		$query="INSERT INTO perusahaan (id,nama,email,alamat,kota,provinsi,telepon,jenis_usaha,ket_jenis_usaha,map) 
		VALUES('".$objek->id."','".$objek->nama."','".$objek->email."','".$objek->alamat."','".$objek->kota."','".$objek->provinsi."','".$objek->telepon."','".$objek->jenisUsaha."','".$objek->keteranganJenisUsaha."', '".$objek->map."')";
		$statement=$this->dbHost->prepare($query);

		if ($statement->execute()) {
			return "<script>alert('Data ".$objek->nama." berhasil dimasukan');</script>";
		}else{
			return "<script>alert('Data ".$objek->nama." gagal dimasukan');</script>";
		}		
	}

}

?>