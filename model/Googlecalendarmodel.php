<?php 
include_once 'Koneksi.php';
class Googlecalendarmodel extends Koneksi{
	var $dbHost;
	private $idtransaksi;

	//constructor
	function __construct(){
		$this->dbHost = $this->bukaKoneksi();
	}

	public function FetchForBackupEmailTrainer($idgocal){
		$query = "SELECT g_row,g_summary,g_agendas,g_description, g_status,gca_user_email,gca_user_role,
		users.nama AS trainer,pk_peran,trans_id,(
			SELECT GROUP_CONCAT(CONCAT(nama,' <small>- ',pk_peran,'</small>') SEPARATOR '#') FROM users
		    INNER JOIN google_cal_attendees ON google_cal_attendees.gca_user_email=users.email
		    LEFT JOIN daftar_peran_kerja ON daftar_peran_kerja.pk_id=google_cal_attendees.gca_user_role
		    WHERE google_cal_attendees.gca_source=g_row
		    ORDER BY google_cal_attendees.gca_user_role ASC
		) AS attendees,IFNULL((
			SELECT GROUP_CONCAT( nama_agenda SEPARATOR '#') 
		    FROM daftar_agenda
		    INNER JOIN new_trans_agendas ON new_trans_agendas.agd_training=daftar_agenda.id
			WHERE new_trans_agendas.agd_id_trans=trans_id
		),'-') AS agendas,
		DATE_FORMAT(fac_calendar.tanggal,'%W, %d %b %Y') AS tanggal_agenda,
		trans_id,perusahaan.nama AS nama_usaha,nama_cust,email_cust,telp_cust,marketing_nama,
		trans_lokasi, petugas.nama AS petugas,perusahaan.id AS aidi,(
		    SELECT nama 
		    FROM users
		    INNER JOIN absensi ON absensi.username=users.username
		    WHERE absensi.nama_perusahaan=aidi
		    ORDER BY absensi.tanggal_absen DESC
		    LIMIT 0,1
		) AS last_trainer,tipepengguna.dt_desc AS tipepengguna,va_nama,(
		    SELECT GROUP_CONCAT(jenis_usaha.jenis_usaha SEPARATOR ' # ') 
			FROM jenis_usaha 
			INNER JOIN perusahaan_jns_usaha ON perusahaan_jns_usaha.id_jenis_usaha=jenis_usaha.id
			INNER JOIN perusahaan ON perusahaan.id=perusahaan_jns_usaha.id_company
			WHERE perusahaan.id=aidi
		) AS jenis_usaha,
        (
        	SELECT GROUP_CONCAT(DATE_FORMAT(fac_calendar.tanggal,'%W, %d %b %Y') SEPARATOR '#')
            FROM fac_calendar
            WHERE fac_calendar.acara=trans_id
        ) AS tanggalz, perusahaan.map AS map_usaha
		FROM `google_cal_data`
		LEFT JOIN google_cal_attendees ON google_cal_attendees.gca_source=google_cal_data.g_row
		LEFT JOIN users ON users.email=google_cal_attendees.gca_user_email
		LEFT JOIN daftar_peran_kerja ON daftar_peran_kerja.pk_id=google_cal_attendees.gca_user_role
		LEFT JOIN fac_calendar ON fac_calendar.id=google_cal_data.g_id_cal
		LEFT JOIN new_transaksi ON fac_calendar.acara=new_transaksi.trans_id
		LEFT JOIN perusahaan ON perusahaan.id = new_transaksi.trans_id_usaha
		LEFT JOIN new_customer ON new_customer.id_cust=new_transaksi.trans_cust_id
		LEFT JOIN new_marketing ON new_marketing.marketing_id=new_customer.marketing_id
		LEFT JOIN versi_accurate ON versi_accurate.va_id=new_transaksi.trans_va
		LEFT JOIN (
			SELECT username AS usr, nama
		    FROM users
		) AS petugas ON petugas.usr=new_transaksi.trans_petugas
		LEFT JOIN (
		    SELECT dt_flag,dt_desc 
			FROM `desc_tbl`
			WHERE desc_tbl.dt_relate_tbl='jenispengguna'
		) AS tipepengguna ON tipepengguna.dt_flag=new_customer.jenis_pengguna_cust
		WHERE g_id_cal = :rownum";

		$statement = $this->dbHost->prepare($query);
        $statement->bindParam(':rownum',$idgocal,PDO::PARAM_STR,255);
        
        $statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function FetchDetailRepeatTraining($idusaha){
		$query = "SELECT perusahaan.nama AS nama_usaha, new_transaksi.trans_id,
		DATE_FORMAT(new_transaksi.trans_date, '%a %e %b %Y %T') AS trans_date, new_invoice.inv_no_invoice AS no_inv,
		users.nama,DATE_FORMAT(fac_calendar.tanggal,'%a %e %b %Y') AS tanggal_training
		FROM perusahaan 
		INNER JOIN new_transaksi ON new_transaksi.trans_id_usaha=perusahaan.id
		INNER JOIN new_invoice ON new_invoice.inv_id_trans=new_transaksi.trans_id
		INNER JOIN fac_calendar ON fac_calendar.acara=new_transaksi.trans_id
		INNER JOIN google_cal_data ON google_cal_data.g_id_cal=fac_calendar.id
		INNER JOIN google_cal_attendees ON google_cal_attendees.gca_source=google_cal_data.g_row
		INNER JOIN users ON users.email=google_cal_attendees.gca_user_email
		WHERE perusahaan.id=:idusaha
		AND google_cal_attendees.gca_user_role='1'
		ORDER BY new_transaksi.trans_date,fac_calendar.tanggal ASC";
		$statement = $this->dbHost->prepare($query);
        $statement->bindParam(':idusaha',$idusaha,PDO::PARAM_STR,255);
        // $statement->bindParam(':tglakhir',$tglakhir,PDO::PARAM_STR,255);
        
        $statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);

	}

	public function FetchDataRepeatTrainerByTgl($tglawal,$tglakhir){
		$query = "WITH data_repeat AS(
    	SELECT DISTINCT perusahaan.id,perusahaan.nama AS nama_usaha,new_transaksi.trans_id AS idtrans,
		users.email AS trainer_utama, google_cal_attendees.gca_user_role, new_transaksi.trans_date AS tanggal_trans
		FROM `new_transaksi`
		INNER JOIN perusahaan ON perusahaan.id=new_transaksi.trans_id_usaha
		INNER JOIN new_invoice ON new_invoice.inv_id_trans=new_transaksi.trans_id
		INNER JOIN fac_calendar ON fac_calendar.acara=new_transaksi.trans_id
		INNER JOIN google_cal_data ON google_cal_data.g_id_cal=fac_calendar.id
		INNER JOIN google_cal_attendees ON google_cal_attendees.gca_source=google_cal_data.g_row
		INNER JOIN users ON users.email=google_cal_attendees.gca_user_email
		WHERE google_cal_attendees.gca_user_role='1'
		AND new_transaksi.trans_jenis='1'
    	AND perusahaan.id NOT IN('-')
		ORDER BY perusahaan.nama ASC
		)
		SELECT DISTINCT users.nama, perusahaan.nama AS nama_usaha,new_transaksi.trans_id AS transsid, 
		(
			SELECT COUNT(fac_calendar.id) 
    		FROM fac_calendar
    		INNER JOIN google_cal_data ON google_cal_data.g_id_cal=fac_calendar.id
    		INNER JOIN google_cal_attendees ON google_cal_attendees.gca_source=google_cal_data.g_row
    		WHERE fac_calendar.acara=transsid
    		AND fac_calendar.tanggal BETWEEN '".$tglawal."' AND '".$tglakhir."'
		) AS total_hari_training,perusahaan.id AS idusahaa
		FROM fac_calendar 
		INNER JOIN new_transaksi ON new_transaksi.trans_id=fac_calendar.acara
		INNER JOIN perusahaan ON perusahaan.id=new_transaksi.trans_id_usaha
		INNER JOIN google_cal_data ON google_cal_data.g_id_cal=fac_calendar.id
		INNER JOIN google_cal_attendees ON google_cal_attendees.gca_source=google_cal_data.g_row
		INNER JOIN users ON users.email=google_cal_attendees.gca_user_email
		INNER JOIN data_repeat ON data_repeat.trainer_utama=users.email AND data_repeat.id=new_transaksi.trans_id_usaha
		WHERE google_cal_attendees.gca_user_role='1'
		AND fac_calendar.tanggal BETWEEN '".$tglawal."' AND '".$tglakhir."'
		AND data_repeat.idtrans<>new_transaksi.trans_id
		AND new_transaksi.trans_date>data_repeat.tanggal_trans";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function FetchDataRepeatTrainer($menu='curmonth'){
	    if (date('m')=='1') {
			$filter= " MONTH(DATE_SUB(CURDATE(), INTERVAL 1 MONTH)) AND YEAR(fac_calendar.tanggal)=YEAR(DATE_SUB(CURDATE(), INTERVAL 1 YEAR))";
		} else {
			$filter= " MONTH(DATE_SUB(CURDATE(), INTERVAL 1 MONTH)) AND YEAR(fac_calendar.tanggal)=YEAR(CURDATE())";
		}
	    
	    if($menu=='curmonth'){
	        $queryadd = " MONTH(CURDATE()) AND YEAR(fac_calendar.tanggal)=YEAR(CURDATE())";
	    }else{
	       $queryadd = $filter;
	    }
	    
		$query = "WITH data_repeat AS(
    	SELECT DISTINCT perusahaan.id,perusahaan.nama AS nama_usaha,new_transaksi.trans_id AS idtrans,
		users.email AS trainer_utama, google_cal_attendees.gca_user_role, new_transaksi.trans_date AS tanggal_trans
		FROM `new_transaksi`
		INNER JOIN perusahaan ON perusahaan.id=new_transaksi.trans_id_usaha
		INNER JOIN new_invoice ON new_invoice.inv_id_trans=new_transaksi.trans_id
		INNER JOIN fac_calendar ON fac_calendar.acara=new_transaksi.trans_id
		INNER JOIN google_cal_data ON google_cal_data.g_id_cal=fac_calendar.id
		INNER JOIN google_cal_attendees ON google_cal_attendees.gca_source=google_cal_data.g_row
		INNER JOIN users ON users.email=google_cal_attendees.gca_user_email
		WHERE google_cal_attendees.gca_user_role='1'
		AND new_transaksi.trans_jenis='1'
    	AND perusahaan.id NOT IN('-')
		ORDER BY perusahaan.nama ASC
		)
		SELECT DISTINCT users.nama, perusahaan.nama AS nama_usaha,new_transaksi.trans_id AS transsid, 
		(
			SELECT COUNT(fac_calendar.id) 
    		FROM fac_calendar
    		INNER JOIN google_cal_data ON google_cal_data.g_id_cal=fac_calendar.id
    		INNER JOIN google_cal_attendees ON google_cal_attendees.gca_source=google_cal_data.g_row
    		WHERE fac_calendar.acara=transsid
    		AND MONTH(fac_calendar.tanggal)=".$queryadd."
		) AS total_hari_training,perusahaan.id AS idusahaa
		FROM fac_calendar 
		INNER JOIN new_transaksi ON new_transaksi.trans_id=fac_calendar.acara
		INNER JOIN perusahaan ON perusahaan.id=new_transaksi.trans_id_usaha
		INNER JOIN google_cal_data ON google_cal_data.g_id_cal=fac_calendar.id
		INNER JOIN google_cal_attendees ON google_cal_attendees.gca_source=google_cal_data.g_row
		INNER JOIN users ON users.email=google_cal_attendees.gca_user_email
		INNER JOIN data_repeat ON data_repeat.trainer_utama=users.email AND data_repeat.id=new_transaksi.trans_id_usaha
		WHERE google_cal_attendees.gca_user_role='1'
		AND MONTH(fac_calendar.tanggal)=".$queryadd."
		AND data_repeat.idtrans<>new_transaksi.trans_id
		AND new_transaksi.trans_date>data_repeat.tanggal_trans";
		$statement = $this->dbHost->prepare($query);
        // $statement->bindParam(':tglawal',$tglawal,PDO::PARAM_STR,255);
        // $statement->bindParam(':tglakhir',$tglakhir,PDO::PARAM_STR,255);
        
        $statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function DaftarFeedback($idtrans){
		$query = "SELECT trans_id,perusahaan.nama AS nama_usaha, new_customer.nama_cust AS namacust
		FROM new_transaksi
		INNER JOIN new_customer ON new_customer.id_cust=new_transaksi.trans_cust_id
		INNER JOIN perusahaan ON perusahaan.id=new_transaksi.trans_id_usaha
		WHERE new_transaksi.trans_id=:trans";
		$statement = $this->dbHost->prepare($query);
        $statement->bindParam(':trans',$idtrans,PDO::PARAM_STR,255);

		$statement->execute();
		return $statement->fetch(PDO::FETCH_ASSOC);
	}

	public function FeedbackTraining($id,$username){
	    $query = "SELECT DISTINCT users.username AS username, users.nama AS nama FROM `fac_calendar` 
        INNER JOIN google_cal_data ON google_cal_data.g_id_cal=fac_calendar.id
        INNER JOIN google_cal_attendees ON google_cal_attendees.gca_source=google_cal_data.g_row
        INNER JOIN users ON users.email=google_cal_attendees.gca_user_email
        WHERE fac_calendar.acara=:trans
        AND google_cal_attendees.gca_user_role='1'
        AND users.username=:username";
        $statement = $this->dbHost->prepare($query);
        $statement->bindParam(':trans',$id,PDO::PARAM_STR,255);
        $statement->bindParam(':username',$username,PDO::PARAM_STR,255);
        
        $statement->execute();
		return $statement->fetch(PDO::FETCH_ASSOC);
	}
	
	public function GetStaffUtama($id){
	    $query = "SELECT DISTINCT users.username AS username, users.nama AS nama,users.email AS email_user, new_customer.telp_cust AS telp_cust, users.picture AS gambar  
		FROM `fac_calendar` 
        INNER JOIN google_cal_data ON google_cal_data.g_id_cal=fac_calendar.id
        INNER JOIN google_cal_attendees ON google_cal_attendees.gca_source=google_cal_data.g_row
        INNER JOIN users ON users.email=google_cal_attendees.gca_user_email
        LEFT JOIN new_transaksi ON new_transaksi.trans_id=fac_calendar.acara
        LEFT JOIN new_customer ON new_customer.id_cust=new_transaksi.trans_cust_id
        WHERE fac_calendar.acara=:trans
        AND google_cal_attendees.gca_user_role='1'";
        $statement = $this->dbHost->prepare($query);
        $statement->bindParam(':trans',$id,PDO::PARAM_STR,255);
        
        $statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function TableAsPerMonth($tanggalawal='',$tanggalakhir=''){
		$query = "SELECT `trans_id`,`tanggal`,`p`.`nama` AS `nama_usaha`,`gocal`.`g_row` AS `grow`,
		IFNULL((
    		SELECT GROUP_CONCAT(users.nama SEPARATOR ' # ') 
 			FROM users
			INNER JOIN google_cal_attendees
    		ON google_cal_attendees.gca_user_email=users.email
    		WHERE `google_cal_attendees`.`gca_source`=`grow`
		),'') AS `trainer`, users.nama AS `marketing`
		FROM `fac_calendar` 
		INNER JOIN `google_cal_data` AS `gocal` 
		ON `gocal`.`g_id_cal`=`fac_calendar`.`id`
		INNER JOIN `google_cal_verf`
		ON `google_cal_verf`.`gcv_src`=`gocal`.`g_row`
		INNER JOIN `users` ON `users`.`username`=`google_cal_verf`.`gcv_username`
		LEFT JOIN `new_transaksi`
		ON `new_transaksi`.`trans_id`=`fac_calendar`.`acara`
		LEFT JOIN `perusahaan` AS `p`
		ON `p`.`id`=`new_transaksi`.`trans_id_usaha`
		WHERE MONTH(`fac_calendar`.`tanggal`)=MONTH(CURDATE())
		ORDER BY `fac_calendar`.`tanggal` ASC";

		$statement = $this->dbHost->prepare($query);
		// $statement->bindParam(':tgl',$tanggal,PDO::PARAM_STR,255);

		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function UpcomingEvents($tanggal){
		$query = "SELECT `trans_id`,`nama`,`tanggal`,`cal_status`,IFNULL(`g_status`,'0') AS `g_status` FROM `fac_calendar` 
		INNER JOIN `new_transaksi`
		ON `new_transaksi`.`trans_id`=`fac_calendar`.`acara`
		LEFT JOIN `perusahaan`
		ON `perusahaan`.`id` = `new_transaksi`.`trans_id_usaha`
        LEFT JOIN `google_cal_data`
        ON `google_cal_data`.`g_id_cal`=`fac_calendar`.`id`
		WHERE fac_calendar.tanggal = DATE_ADD(:tgl, INTERVAL 1 DAY)";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':tgl',$tanggal,PDO::PARAM_STR,255);

		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function DeleteAll($idrow){
		$query = "DELETE google_cal_data.*,google_cal_attendees.*,google_cal_verf.* FROM google_cal_data
		LEFT JOIN google_cal_attendees
		ON google_cal_attendees.gca_source = google_cal_data.g_row
		LEFT JOIN google_cal_verf
		ON google_cal_verf.gcv_src=google_cal_data.g_row
		WHERE google_cal_data.g_row = :row ;";
		$statement = $this->dbHost->prepare($query);

		$statement->bindParam(':row',$idrow,PDO::PARAM_STR,255);
		
		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function GetUtamaPendampingPeninjau($idgooglecal){
		$query = "SELECT 
		IFNULL(`u`.`nama`,'') AS staff_utama, 
		IFNULL(`pendamping`.`nama`,'') AS `staff_pendamping`,
		IFNULL(`peninjau`.`nama`,'') AS `staff_peninjau`
		FROM google_cal_data 
		LEFT JOIN (
			SELECT google_cal_attendees.gca_source AS `aidi`,`nama`
    		FROM google_cal_attendees
    		INNER JOIN `users` 
    		ON users.email=google_cal_attendees.gca_user_email
    		WHERE `google_cal_attendees`.`gca_user_role`='1'
		) AS `u`
		ON `u`.`aidi`=google_cal_data.g_row
		LEFT JOIN (
			SELECT google_cal_attendees.gca_source AS `aidi`,`nama`
    		FROM google_cal_attendees
    		INNER JOIN `users` 
    		ON users.email=google_cal_attendees.gca_user_email
    		WHERE `google_cal_attendees`.`gca_user_role`='2'
		) AS `pendamping`
		ON `pendamping`.`aidi`=google_cal_data.g_row
		LEFT JOIN (
			SELECT google_cal_attendees.gca_source AS `aidi`,GROUP_CONCAT(`nama` SEPARATOR '#') AS `nama`
    		FROM google_cal_attendees
    		INNER JOIN `users` 
    		ON users.email=google_cal_attendees.gca_user_email
    		WHERE `google_cal_attendees`.`gca_user_role`='3'
    		GROUP BY google_cal_attendees.gca_source
		) AS `peninjau`
		ON `peninjau`.`aidi`=google_cal_data.g_row
		WHERE google_cal_data.g_row=:id";
		$statement = $this->dbHost->prepare($query);

		$statement->bindParam(':id',$idgooglecal,PDO::PARAM_STR,255);

		$statement->execute();
		return $statement->fetch(PDO::FETCH_ASSOC);
	}
	
	public function GetLastTrainer($transid){
	    $query = "SELECT IFNULL(users.nama,'-') AS trainer 
        FROM new_transaksi
        LEFT JOIN absensi ON absensi.nama_perusahaan=new_transaksi.trans_id_usaha
        LEFT JOIN users ON users.username=absensi.username
        WHERE new_transaksi.trans_id=:transid
        ORDER BY absensi.tanggal_absen DESC
        LIMIT 0,1";
        
        $statement = $this->dbHost->prepare($query);
        $statement->bindParam(':transid',$transid,PDO::PARAM_STR,255);
        $statement->execute();
		return $statement->fetch(PDO::FETCH_ASSOC);
	}

	public function Newagendasummary($idtransaksi){
		$query = "SELECT `trans_id`,`trans_waktu`,`va_nama`,`ket_jenis_usaha`,
		IFNULL((
    		SELECT GROUP_CONCAT(`nama_agenda` SEPARATOR '#') 
			FROM `new_trans_agendas`
    		INNER JOIN `daftar_agenda` 
			ON `daftar_agenda`.`id`=`new_trans_agendas`.`agd_training` 
    		WHERE `new_trans_agendas`.`agd_id_trans`=`trans_id`
		),'') AS `agenda`,
		`nama_cust`,`telp_cust`,`perusahaan`.`email` AS `email`,`email_cust`,marketing_nama, branch.nama_usaha AS nama_cabang,
		users.nama AS m_fac,masterdet.dt_desc AS jns_pgn,status_cust,
		(
			SELECT GROUP_CONCAT(fac_calendar.tanggal SEPARATOR '#')
			FROM fac_calendar
			WHERE `fac_calendar`.`acara`=`trans_id`
		) AS `tgl_trans`
		FROM `new_transaksi` 
		INNER JOIN `perusahaan` 
		ON `perusahaan`.`id`=`new_transaksi`.`trans_id_usaha`
		INNER JOIN `new_customer`
		ON `new_customer`.`id_cust`=`new_transaksi`.`trans_cust_id`
		INNER JOIN `versi_accurate`
		ON `versi_accurate`.`va_id`=`new_transaksi`.`trans_va`
		LEFT JOIN `new_marketing` 
		ON `new_marketing`.`marketing_id`=`new_customer`.`marketing_id`
		LEFT JOIN `users`
		ON `users`.`username`=`new_transaksi`.`trans_petugas`
		LEFT JOIN (
			SELECT id_cabang,id_perusahaan,perusahaan.nama AS nama_usaha 
			FROM `cabang`
			INNER JOIN perusahaan ON `perusahaan`.`id`=`cabang`.`id_perusahaan`
		) AS branch
		ON branch.id_cabang=new_marketing.marketing_cabang
        LEFT JOIN (
        	SELECT dt_flag,dt_desc FROM `desc_tbl` 
            WHERE `desc_tbl`.`dt_relate_tbl`='jenispengguna'
        ) AS masterdet
        ON masterdet.dt_flag=new_customer.status_cust
		WHERE `new_transaksi`.`trans_id`=:transaksi";

		$statement = $this->dbHost->prepare($query);

		$statement->bindParam(':transaksi',$idtransaksi,PDO::PARAM_STR,255);

		$statement->execute();
		return $statement->fetch(PDO::FETCH_ASSOC);
	}

	// @url: /administrasi/googlecal/summarylist
	public function DisplayTableSummary(){
		$query="SELECT `tanggal`,`g_row`, `gcv_hash`, `g_id_cal`, `g_summary`, `g_description`, `cal_status`,
		(
			SELECT GROUP_CONCAT( `nama` SEPARATOR ' # ') 
    		FROM `users`
    		INNER JOIN `google_cal_verf` ON `google_cal_verf`.`gcv_username`=`users`.`username`
    		WHERE `google_cal_verf`.`gcv_src`=`g_row`
		) AS `petugas_verifikasi`,
        IFNULL((
        	SELECT GROUP_CONCAT(`nama` SEPARATOR ' # ')
            FROM users
            INNER JOIN google_cal_attendees ON google_cal_attendees.gca_user_email=users.email
            WHERE google_cal_attendees.gca_source=g_row
        ),'-') AS peserta
		FROM `google_cal_data` 
		INNER JOIN `fac_calendar` ON `fac_calendar`.`id`=`google_cal_data`.`g_id_cal`
        LEFT JOIN `google_cal_verf` ON `google_cal_verf`.`gcv_src`=`google_cal_data`.`g_row`
		WHERE `fac_calendar`.`acara`=:transaksi";
		$statement = $this->dbHost->prepare($query);

		$statement->bindParam(':transaksi',$this->idtransaksi,PDO::PARAM_STR,255);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	}




    public function getIdtransaksi(){
        return $this->idtransaksi;
    }

    public function setIdtransaksi($idtransaksi){
        $this->idtransaksi = $idtransaksi;
    }
}

?>