<?php 
include_once 'Koneksi.php';
date_default_timezone_set("Asia/Jakarta"); 
class Calender extends Koneksi{
	
	private $bulan;
	private $tahun;
	private $data;


	function __construct(){
		$this->dbHost = $this->bukaKoneksi();
	}
	
	public function GetOutstandingDayFromTransaction($search=''){
	    if($search==''){
	        $additional = " AND MONTH(new_transaksi.trans_date)=MONTH(CURDATE()) 
	        AND YEAR(new_transaksi.trans_date)=YEAR(CURDATE())";
	    }else{
	       $additional = " AND MONTH(new_transaksi.trans_date)='".$search['m']."'  
	        AND YEAR(new_transaksi.trans_date)='".$search['y']."' "; 
	    }
	    
	    $query = "WITH outstanding_table AS (
        SELECT trans_id,perusahaan.nama AS nama_usaha, 
	        (
    	        SELECT COUNT(fac_calendar.tanggal) 
    	        FROM fac_calendar 
    	        WHERE fac_calendar.acara=trans_id
	        ) AS total_hari,
	        (
		        SELECT SUM(paket_training.paket_hari) 
    	        FROM paket_training
    	        INNER JOIN new_trans_items
    	        ON new_trans_items.itm_paket=paket_training.id
    	        WHERE new_trans_items.itm_trans_id=trans_id
	        ) AS expected_day
	        FROM `new_transaksi` 
            INNER JOIN perusahaan ON perusahaan.id = new_transaksi.trans_id_usaha
	        WHERE new_transaksi.trans_jenis='1' 
	        ".$additional."
        )
        SELECT a.trans_id AS id,a.nama_usaha,a.total_hari,a.expected_day, a.total_hari-a.expected_day AS outday 
        FROM outstanding_table AS a
        WHERE a.total_hari < a.expected_day ";
	    $statement = $this->dbHost->prepare($query);
		
		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function GetAvailableDateForTrans($idtrans){
		$query = "SELECT SUM(paket_training.paket_hari) AS jumlah 
		FROM `new_trans_items`
		INNER JOIN paket_training
		ON paket_training.id=new_trans_items.itm_paket
		WHERE new_trans_items.itm_trans_id=:idtrans";
		$statement = $this->dbHost->prepare($query);
		@$statement->bindParam(':idtrans',$idtrans,PDO::PARAM_STR,45);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);
		
		return $result['jumlah'];
	}

	public function insertData($tanggal,$acara){
		$query = "INSERT INTO `fac_calendar` (`tanggal`, `acara`) VALUES (:tanggal, :acara)";
		$statement = $this->dbHost->prepare($query);

		//$statement->bindParam(':id',$id,PDO::PARAM_STR,35);
		$statement->bindParam(':tanggal',$tanggal,PDO::PARAM_STR,15);
		$statement->bindParam(':acara',$acara,PDO::PARAM_STR,255);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}

	}

	public function DisplayTransaction(){
		$query = "SELECT `id`, `perusahaan` AS `acara`, `transaksi`, `alamat`, `cal_id`, `tanggal`, `acara`, `petugas`, `nama_agenda` FROM `acara_harian` WHERE MONTH(tanggal)=:bulan AND YEAR(tanggal)=:tahun ORDER BY `tanggal` ASC;";
		$statement = $this->dbHost->prepare($query);

		@$statement->bindParam(':bulan',$this->bulan,PDO::PARAM_STR,15);
		@$statement->bindParam(':tahun',$this->tahun,PDO::PARAM_STR,25);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function SearchFromDailyTransaction(){
		$usaha = '%'.$this->data->usaha.'%';
		$agenda = '%'.$this->data->agenda.'%';
		$petugas = '%'.$this->data->petugas.'%';

		$query = "SELECT `id`, `perusahaan`, `transaksi`, `alamat`, `cal_id`, `tanggal`, `acara`, `petugas`, `nama_agenda` 
		FROM `acara_harian` 
		WHERE `perusahaan` LIKE :usaha
		AND `nama_agenda` COLLATE utf8_general_ci LIKE :agenda
		AND `petugas` COLLATE utf8_general_ci LIKE :petugas;";
		$statement = $this->dbHost->prepare($query);

		@$statement->bindParam(':usaha',$usaha,PDO::PARAM_STR,75);
		@$statement->bindParam(':agenda',$agenda,PDO::PARAM_STR,255);
		@$statement->bindParam(':petugas',$petugas,PDO::PARAM_STR,255);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);	
	}

	public function FetchTransactionsDateForAbsensi(){
		$query = "SELECT `perusahaan`.`id` AS `id`,`tanggal`, `nama`,`acara` 
		FROM `fac_calendar`,`new_transaksi`,`new_customer`,`perusahaan` 
		WHERE `fac_calendar`.`acara`=`new_transaksi`.`trans_id`
		AND `new_transaksi`.`trans_cust_id` = `new_customer`.`id_cust`
		AND `new_customer`.`id_perusahaan` = `perusahaan`.`id`
		AND `fac_calendar`.`tanggal`= CURDATE();";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function editData($id){
		$query = "";
	}

	public function selectByID($by){
		if ($by->cond=='forDelete') {
			$query="SELECT `id`, `tanggal`, cek_acara(`acara`) AS acara FROM fac_calendar WHERE id = ".$by->id;
		} else {
			$query="SELECT * WHERE id = :id";
		}
		$statement = $this->dbHost->prepare($query);
		$this->dbHost->prepare($query);
		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function deleteData($id){
		$query = "DELETE FROM fac_calendar WHERE id=$id";
		$statement = $this->dbHost->prepare($query);

		if ($statement->execute()) {
			return "<script>alert('Data berhasil dihapus');</script>";
		}else{
			return "<script>alert('Data berhasil dihapus');</script>";
		}
	}

	public function getNamaPerusahaan($value){
		$query="SELECT cari_perusahaan(nama_perusahaan) AS nama FROM `pendaftar` WHERE `pendaftar`.`id`='$value'";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);		
	}

	public function getAllDates($value){
		$query="SELECT `tanggal` FROM `fac_calendar` WHERE `acara`='$value'";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function deleteDataByAcara($acara){
		$query = "DELETE FROM fac_calendar WHERE acara='$acara'";
		$statement = $this->dbHost->prepare($query);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function selectToday(){
		$query="SELECT `id`, `tanggal`, cek_acara(`acara`) AS acara FROM `fac_calendar`";

		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function agendaHariIni(){
		//$query="SELECT * FROM `acara_harian` WHERE `tanggal`='".date('Y-m-d')."'";
		$query="SELECT * FROM acara_harian WHERE tanggal ='".date('Y-m-d')."'";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function agendaByDate($tanggal){
		//$query="SELECT * FROM `acara_harian` WHERE `tanggal`='".date('Y-m-d')."'";
		$query="SELECT * FROM agenda_harian WHERE tanggal ='$tanggal'";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}


    public function searchData($by){
        if ($by->tanggalAwal==''&&$by->tanggalAkhir=='') {
            $query="SELECT `id`, `tanggal`, cek_acara(`acara`) AS acara FROM `fac_calendar` WHERE cek_acara(`acara`) LIKE '%".$by->acara."%' ORDER BY tanggal ASC";
        } else {
            $query="SELECT `id`, `tanggal`, cek_acara(`acara`) AS acara FROM `fac_calendar` WHERE cek_acara(`acara`) LIKE '%".$by->acara."%' AND tanggal BETWEEN '".$by->tanggalAwal."' AND '".$by->tanggalAkhir."' ORDER BY tanggal ASC";
        }
        $statement = $this->dbHost->prepare($query);
        $statement->execute();
        $results=$statement->fetchAll(PDO::FETCH_ASSOC);
        $dataz=json_encode($results);
        return $dataz;
    }

	public function selectThisMonth(){
		$query="SELECT `id`,tanggal,cek_acara(`acara`) AS acara FROM `fac_calendar` WHERE MONTH(tanggal) = ".intval(date('m'))." ORDER BY tanggal ASC";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}


    /**
     * @return mixed
     */
    public function getBulan()
    {
        return $this->bulan;
    }

    /**
     * @param mixed $bulan
     *
     * @return self
     */
    public function setBulan($bulan)
    {
        $this->bulan = $bulan;
    }

    /**
     * @return mixed
     */
    public function getTahun()
    {
        return $this->tahun;
    }

    /**
     * @param mixed $tahun
     *
     * @return self
     */
    public function setTahun($tahun)
    {
        $this->tahun = $tahun;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     *
     * @return self
     */
    public function setData($data)
    {
        $this->data = $data;
    }
}
?>