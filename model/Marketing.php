<?php 
include_once 'Koneksi.php';
class Marketing extends Koneksi{
	var $dbHost;
	//attributes
	private $id;
	private $nama;
	private $email;
	private $telepon;
	private $cabang;

	public function getID(){
		return $this->id;
	}

	public function setID($value){
		 $this->id = $value;
	}

	// nama
	public function getNama(){
		return $this->nama;
	}

	public function setNama($value){
		 $this->nama = $value;
	}

	// email 
	public function getEmail(){
		return $this->email;
	}

	public function setEmail($value){
		 $this->email = $value;
	}

	// telepon
	public function getTelepon(){
		return $this->telepon;
	}

	public function setTelepon($value){
		 $this->telepon = $value;
	}

	// cabang
	public function getCabang(){
		return $this->cabang;
	}

	public function setCabang($value){
		 $this->cabang = $value;
	}

	//constructor
	function __construct(){
		$this->dbHost = $this->bukaKoneksi();
	}

	function CreateID($value){
		return md5($value);
	}


	public function FetchAllTransItem(){
		$query = "SELECT `id`, `nama_item`, `harga`, `price_for`, `paket_hari`, dt_desc AS `kategori`, `status_paket` 
		FROM `paket_training` 
		INNER JOIN desc_tbl ON desc_tbl.dt_flag=paket_training.kategori
		AND desc_tbl.dt_relate_tbl='pktitemkat'";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();
		
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function NewData(){
		$query = "INSERT INTO `new_marketing` (`marketing_id`, `marketing_nama`, `marketing_telp`, `marketing_email`, `marketing_cabang`) 
		VALUES (:id, :nama, :telepon, :email, :cabang);";
		$statement = $this->dbHost->prepare($query);


		@$statement->bindParam(':id',$this->CreateID($this->email),PDO::PARAM_STR,35);
		@$statement->bindParam(':nama',$this->nama,PDO::PARAM_STR,75);
		@$statement->bindParam(':telepon',$this->telepon,PDO::PARAM_STR,20);
		@$statement->bindParam(':email',$this->email,PDO::PARAM_STR,125);
		@$statement->bindParam(':cabang',$this->cabang,PDO::PARAM_STR,2);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function EditData(){
		$query = "UPDATE `new_marketing` SET
		`marketing_nama`=:nama,
		`marketing_telp`=:telepon,
		`marketing_email`=:email,
		`marketing_cabang`=:cabang 
		WHERE `marketing_id`=:id;";
		$statement = $this->dbHost->prepare($query);


		@$statement->bindParam(':id',$this->id,PDO::PARAM_STR,35);
		@$statement->bindParam(':nama',$this->nama,PDO::PARAM_STR,75);
		@$statement->bindParam(':telepon',$this->telepon,PDO::PARAM_STR,20);
		@$statement->bindParam(':email',$this->email,PDO::PARAM_STR,125);
		@$statement->bindParam(':cabang',$this->cabang,PDO::PARAM_STR,2);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}

	}

	public function FetchCabang(){
		$query = "SELECT `id_cabang`,`nama` AS `nama_usaha` 
		FROM `cabang`,`perusahaan` 
		WHERE `cabang`.`id_perusahaan`=`perusahaan`.`id`;";
		
		$statement = $this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}
	
	public function CountInvoiceBulanLalu(){
	    
	    if (date('m')=='1') {
			$filter= " WHERE MONTH(new_invoice.inv_date) = MONTH(DATE_SUB(CURDATE(), INTERVAL 1 MONTH)) AND YEAR(new_invoice.inv_date)=YEAR(DATE_SUB(CURDATE(), INTERVAL 1 YEAR))";
		} else {
			$filter= " WHERE MONTH(new_invoice.inv_date) = MONTH(DATE_SUB(CURDATE(), INTERVAL 1 MONTH)) AND YEAR(new_invoice.inv_date)=YEAR(CURDATE())";
		}
	    
        $query = "SELECT `new_marketing`.`marketing_id` AS `aidi`,`new_marketing`.`marketing_nama` AS `nama`, 
		(SELECT COUNT(`new_invoice`.`inv_no_invoice`) FROM `new_marketing` INNER JOIN `new_customer` ON `new_customer`.`marketing_id`=`new_marketing`.`marketing_id`
		INNER JOIN `new_transaksi` ON `new_transaksi`.`trans_cust_id` = `new_customer`.`id_cust`
		INNER JOIN `new_invoice` ON `new_invoice`.`inv_id_trans` = `new_transaksi`.`trans_id` 
		".$filter."
        AND `new_marketing`.`marketing_id`=`aidi`
		AND (`new_invoice`.`inv_status`='2' OR `new_invoice`.`inv_status`='3')
        ) AS `jumlah`,
		(SELECT 
		SUM(harga*itm_qty - ROUND(harga*itm_qty*trans_ppn) - ROUND(harga*itm_qty*trans_discount) - `trans_dp`) AS `jumlah` 
		FROM `new_invoice` 
        INNER JOIN `new_transaksi` 
        ON `new_transaksi`.`trans_id` = `new_invoice`.`inv_id_trans`
        INNER JOIN `new_trans_items` 
        ON `new_trans_items`.`itm_trans_id` = `new_transaksi`.`trans_id`
        INNER JOIN `paket_training` 
        ON `new_trans_items`.`itm_paket` = `paket_training`.`id`
        INNER JOIN `new_customer` ON `new_customer`.`id_cust` = `new_transaksi`.`trans_cust_id`
        INNER JOIN `new_marketing` 
        ON `new_marketing`.`marketing_id`=`new_customer`.`marketing_id`
		".$filter."
        AND (`new_invoice`.`inv_status` = '2' OR `new_invoice`.`inv_status` = '3')
        AND `new_marketing`.`marketing_id`=`aidi`) AS `invoice_terbayar`,
        (SELECT 
		SUM(harga*itm_qty - ROUND(harga*itm_qty*trans_ppn) - ROUND(harga*itm_qty*trans_discount) - `trans_dp`) AS `jumlah` 
		FROM `new_invoice` 
        INNER JOIN `new_transaksi` 
        ON `new_transaksi`.`trans_id` = `new_invoice`.`inv_id_trans`
        INNER JOIN `new_trans_items` 
        ON `new_trans_items`.`itm_trans_id` = `new_transaksi`.`trans_id`
        INNER JOIN `paket_training` 
        ON `new_trans_items`.`itm_paket` = `paket_training`.`id`
        INNER JOIN `new_customer` ON `new_customer`.`id_cust` = `new_transaksi`.`trans_cust_id`
        INNER JOIN `new_marketing` 
        ON `new_marketing`.`marketing_id`=`new_customer`.`marketing_id`
		".$filter."
        AND `new_invoice`.`inv_status` > '0'
        AND `new_marketing`.`marketing_id`=`aidi`) AS `omset`
		FROM `new_marketing` HAVING invoice_terbayar>0 OR omset>0;";
		#echo $query;
		$statement = $this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
        
    }

    public function CountInvoiceByDate($fromdate,$todate){
	    
	    $filter= " WHERE new_invoice.inv_date BETWEEN '$fromdate' AND '$todate'";
		
	    
        $query = "SELECT `new_marketing`.`marketing_id` AS `aidi`,`new_marketing`.`marketing_nama` AS `nama`, 
		(SELECT COUNT(`new_invoice`.`inv_no_invoice`) FROM `new_marketing` INNER JOIN `new_customer` ON `new_customer`.`marketing_id`=`new_marketing`.`marketing_id`
		INNER JOIN `new_transaksi` ON `new_transaksi`.`trans_cust_id` = `new_customer`.`id_cust`
		INNER JOIN `new_invoice` ON `new_invoice`.`inv_id_trans` = `new_transaksi`.`trans_id` 
		".$filter."
        AND `new_marketing`.`marketing_id`=`aidi`
		AND (`new_invoice`.`inv_status`='2' OR `new_invoice`.`inv_status`='3')
        ) AS `jumlah`,
		(SELECT 
		SUM(harga*itm_qty - ROUND(harga*itm_qty*trans_ppn) - ROUND(harga*itm_qty*trans_discount) - `trans_dp`) AS `jumlah` 
		FROM `new_invoice` 
        INNER JOIN `new_transaksi` 
        ON `new_transaksi`.`trans_id` = `new_invoice`.`inv_id_trans`
        INNER JOIN `new_trans_items` 
        ON `new_trans_items`.`itm_trans_id` = `new_transaksi`.`trans_id`
        INNER JOIN `paket_training` 
        ON `new_trans_items`.`itm_paket` = `paket_training`.`id`
        INNER JOIN `new_customer` ON `new_customer`.`id_cust` = `new_transaksi`.`trans_cust_id`
        INNER JOIN `new_marketing` 
        ON `new_marketing`.`marketing_id`=`new_customer`.`marketing_id`
		".$filter."
        AND (`new_invoice`.`inv_status` = '2' OR `new_invoice`.`inv_status` = '3')
        AND `new_marketing`.`marketing_id`=`aidi`) AS `invoice_terbayar`,
        (SELECT 
		SUM(harga*itm_qty - ROUND(harga*itm_qty*trans_ppn) - ROUND(harga*itm_qty*trans_discount) - `trans_dp`) AS `jumlah` 
		FROM `new_invoice` 
        INNER JOIN `new_transaksi` 
        ON `new_transaksi`.`trans_id` = `new_invoice`.`inv_id_trans`
        INNER JOIN `new_trans_items` 
        ON `new_trans_items`.`itm_trans_id` = `new_transaksi`.`trans_id`
        INNER JOIN `paket_training` 
        ON `new_trans_items`.`itm_paket` = `paket_training`.`id`
        INNER JOIN `new_customer` ON `new_customer`.`id_cust` = `new_transaksi`.`trans_cust_id`
        INNER JOIN `new_marketing` 
        ON `new_marketing`.`marketing_id`=`new_customer`.`marketing_id`
		".$filter."
        AND `new_invoice`.`inv_status` > '0'
        AND `new_marketing`.`marketing_id`=`aidi`) AS `omset`
		FROM `new_marketing` HAVING invoice_terbayar>0 OR omset>0;";
		#echo $query;
		$statement = $this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
        
    }

    public function CountInvoiceBulanan(){
        $query = "SELECT `new_marketing`.`marketing_id` AS `aidi`,`new_marketing`.`marketing_nama` AS `nama`, 
		(SELECT COUNT(`new_invoice`.`inv_no_invoice`) FROM `new_marketing` INNER JOIN `new_customer` ON `new_customer`.`marketing_id`=`new_marketing`.`marketing_id`
		INNER JOIN `new_transaksi` ON `new_transaksi`.`trans_cust_id` = `new_customer`.`id_cust`
		INNER JOIN `new_invoice` ON `new_invoice`.`inv_id_trans` = `new_transaksi`.`trans_id` 
		WHERE MONTH(`new_invoice`.`inv_date`)=MONTH(CURDATE()) 
        AND YEAR(`new_invoice`.`inv_date`)=YEAR(CURDATE())
        AND `new_marketing`.`marketing_id`=`aidi`
		AND (`new_invoice`.`inv_status`='2' OR `new_invoice`.`inv_status`='3')
        ) AS `jumlah`,
		IFNULL((SELECT 
		SUM(harga*itm_qty - ROUND(harga*itm_qty*trans_ppn) - ROUND(harga*itm_qty*trans_discount) - `trans_dp`)
		FROM `new_invoice` 
        INNER JOIN `new_transaksi` 
        ON `new_transaksi`.`trans_id` = `new_invoice`.`inv_id_trans`
        INNER JOIN `new_trans_items` 
        ON `new_trans_items`.`itm_trans_id` = `new_transaksi`.`trans_id`
        INNER JOIN `paket_training` 
        ON `new_trans_items`.`itm_paket` = `paket_training`.`id`
        INNER JOIN `new_customer` ON `new_customer`.`id_cust` = `new_transaksi`.`trans_cust_id`
        INNER JOIN `new_marketing` 
        ON `new_marketing`.`marketing_id`=`new_customer`.`marketing_id`
		WHERE MONTH(`new_invoice`.`inv_date`)=MONTH(CURDATE()) 
        AND YEAR(`new_invoice`.`inv_date`)=YEAR(CURDATE())
        AND (`new_invoice`.`inv_status` = '2' OR `new_invoice`.`inv_status` = '3')
        AND `new_marketing`.`marketing_id`=`aidi`),'0') AS `invoice_terbayar`,
        IFNULL((SELECT 
		SUM(harga*itm_qty - ROUND(harga*itm_qty*trans_ppn) - ROUND(harga*itm_qty*trans_discount) - `trans_dp`) AS `jumlah` 
		FROM `new_invoice` 
        INNER JOIN `new_transaksi` 
        ON `new_transaksi`.`trans_id` = `new_invoice`.`inv_id_trans`
        INNER JOIN `new_trans_items` 
        ON `new_trans_items`.`itm_trans_id` = `new_transaksi`.`trans_id`
        INNER JOIN `paket_training` 
        ON `new_trans_items`.`itm_paket` = `paket_training`.`id`
        INNER JOIN `new_customer` ON `new_customer`.`id_cust` = `new_transaksi`.`trans_cust_id`
        INNER JOIN `new_marketing` 
        ON `new_marketing`.`marketing_id`=`new_customer`.`marketing_id`
		WHERE MONTH(`new_invoice`.`inv_date`)=MONTH(CURDATE()) 
        AND YEAR(`new_invoice`.`inv_date`)=YEAR(CURDATE())
        AND `new_invoice`.`inv_status` > '0'
        AND `new_marketing`.`marketing_id`=`aidi`),'0') AS `omset`
		FROM `new_marketing` HAVING invoice_terbayar>0 OR omset>0;";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
        
    }

    public function CountInvoiceBulanLaluByCabang(){
	    
	    if (date('m')=='1') {
			$filter= " WHERE MONTH(new_invoice.inv_date) = MONTH(DATE_SUB(CURDATE(), INTERVAL 1 MONTH)) AND YEAR(new_invoice.inv_date)=YEAR(DATE_SUB(CURDATE(), INTERVAL 1 YEAR))";
		} else {
			$filter= " WHERE MONTH(new_invoice.inv_date) = MONTH(DATE_SUB(CURDATE(), INTERVAL 1 MONTH)) AND YEAR(new_invoice.inv_date)=YEAR(CURDATE())";
		}
	    
        $query = "SELECT `id_cabang`,`nama`,
		(SELECT COUNT(`new_invoice`.`inv_no_invoice`) 
		FROM `new_marketing` 
		INNER JOIN `new_customer` ON `new_customer`.`marketing_id`=`new_marketing`.`marketing_id`
		INNER JOIN `new_transaksi` ON `new_transaksi`.`trans_cust_id` = `new_customer`.`id_cust`
		INNER JOIN `new_invoice` ON `new_invoice`.`inv_id_trans` = `new_transaksi`.`trans_id` 
		".$filter." 
        AND `new_marketing`.`marketing_cabang`=`id_cabang`
		AND (`new_invoice`.`inv_status`='2' OR `new_invoice`.`inv_status`='3')
        ) AS `jumlah`,
		IFNULL((SELECT 
		SUM(harga*itm_qty - ROUND(harga*itm_qty*trans_ppn) - ROUND(harga*itm_qty*trans_discount) - `trans_dp`)
		FROM `new_invoice` 
        INNER JOIN `new_transaksi` 
        ON `new_transaksi`.`trans_id` = `new_invoice`.`inv_id_trans`
        INNER JOIN `new_trans_items` 
        ON `new_trans_items`.`itm_trans_id` = `new_transaksi`.`trans_id`
        INNER JOIN `paket_training` 
        ON `new_trans_items`.`itm_paket` = `paket_training`.`id`
        INNER JOIN `new_customer` ON `new_customer`.`id_cust` = `new_transaksi`.`trans_cust_id`
        INNER JOIN `new_marketing` 
        ON `new_marketing`.`marketing_id`=`new_customer`.`marketing_id`
		".$filter." 
        AND (`new_invoice`.`inv_status` = '2' OR `new_invoice`.`inv_status` = '3')
        AND `new_marketing`.`marketing_cabang`=`id_cabang`),'0') AS `invoice_terbayar`,
        IFNULL((SELECT 
		SUM(harga*itm_qty - ROUND(harga*itm_qty*trans_ppn) - ROUND(harga*itm_qty*trans_discount) - `trans_dp`)
		FROM `new_invoice` 
        INNER JOIN `new_transaksi` 
        ON `new_transaksi`.`trans_id` = `new_invoice`.`inv_id_trans`
        INNER JOIN `new_trans_items` 
        ON `new_trans_items`.`itm_trans_id` = `new_transaksi`.`trans_id`
        INNER JOIN `paket_training` 
        ON `new_trans_items`.`itm_paket` = `paket_training`.`id`
        INNER JOIN `new_customer` ON `new_customer`.`id_cust` = `new_transaksi`.`trans_cust_id`
        INNER JOIN `new_marketing` 
        ON `new_marketing`.`marketing_id`=`new_customer`.`marketing_id`
		".$filter." 
        AND `new_invoice`.`inv_status` > '0'
        AND `new_marketing`.`marketing_cabang`=`id_cabang`),'0') AS `omset`
		FROM `cabang` 
		INNER JOIN `perusahaan` ON `perusahaan`.`id`=`cabang`.`id_perusahaan`";
		#echo $query;
		$statement = $this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
        
    }

    public function CountInvoiceCabangByDate($fromdate,$todate){
	    
		$filter= " WHERE new_invoice.inv_date BETWEEN '$fromdate' AND '$todate'";
		
	    
        $query = "SELECT `id_cabang`,`nama`,
		(SELECT COUNT(`new_invoice`.`inv_no_invoice`) 
		FROM `new_marketing` 
		INNER JOIN `new_customer` ON `new_customer`.`marketing_id`=`new_marketing`.`marketing_id`
		INNER JOIN `new_transaksi` ON `new_transaksi`.`trans_cust_id` = `new_customer`.`id_cust`
		INNER JOIN `new_invoice` ON `new_invoice`.`inv_id_trans` = `new_transaksi`.`trans_id` 
		".$filter." 
        AND `new_marketing`.`marketing_cabang`=`id_cabang`
		AND (`new_invoice`.`inv_status`='2' OR `new_invoice`.`inv_status`='3')
        ) AS `jumlah`,
		IFNULL((SELECT 
		SUM(harga*itm_qty - ROUND(harga*itm_qty*trans_ppn) - ROUND(harga*itm_qty*trans_discount) - `trans_dp`)
		FROM `new_invoice` 
        INNER JOIN `new_transaksi` 
        ON `new_transaksi`.`trans_id` = `new_invoice`.`inv_id_trans`
        INNER JOIN `new_trans_items` 
        ON `new_trans_items`.`itm_trans_id` = `new_transaksi`.`trans_id`
        INNER JOIN `paket_training` 
        ON `new_trans_items`.`itm_paket` = `paket_training`.`id`
        INNER JOIN `new_customer` ON `new_customer`.`id_cust` = `new_transaksi`.`trans_cust_id`
        INNER JOIN `new_marketing` 
        ON `new_marketing`.`marketing_id`=`new_customer`.`marketing_id`
		".$filter." 
        AND (`new_invoice`.`inv_status` = '2' OR `new_invoice`.`inv_status` = '3')
        AND `new_marketing`.`marketing_cabang`=`id_cabang`),'0') AS `invoice_terbayar`,
        IFNULL((SELECT 
		SUM(harga*itm_qty - ROUND(harga*itm_qty*trans_ppn) - ROUND(harga*itm_qty*trans_discount) - `trans_dp`)
		FROM `new_invoice` 
        INNER JOIN `new_transaksi` 
        ON `new_transaksi`.`trans_id` = `new_invoice`.`inv_id_trans`
        INNER JOIN `new_trans_items` 
        ON `new_trans_items`.`itm_trans_id` = `new_transaksi`.`trans_id`
        INNER JOIN `paket_training` 
        ON `new_trans_items`.`itm_paket` = `paket_training`.`id`
        INNER JOIN `new_customer` ON `new_customer`.`id_cust` = `new_transaksi`.`trans_cust_id`
        INNER JOIN `new_marketing` 
        ON `new_marketing`.`marketing_id`=`new_customer`.`marketing_id`
		".$filter." 
        AND `new_invoice`.`inv_status` > '0'
        AND `new_marketing`.`marketing_cabang`=`id_cabang`),'0') AS `omset`
		FROM `cabang` 
		INNER JOIN `perusahaan` ON `perusahaan`.`id`=`cabang`.`id_perusahaan`";
		#echo $query;
		$statement = $this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
        
    }

    public function CountInvoiceBulananByCabang(){
    	$query = "SELECT `id_cabang`,`nama`,
		(SELECT COUNT(`new_invoice`.`inv_no_invoice`) 
         FROM `new_marketing` 
         INNER JOIN `new_customer` 
         ON `new_customer`.`marketing_id`=`new_marketing`.`marketing_id`
		INNER JOIN `new_transaksi` 
		ON `new_transaksi`.`trans_cust_id` = `new_customer`.`id_cust`
		INNER JOIN `new_invoice` ON `new_invoice`.`inv_id_trans` = `new_transaksi`.`trans_id` 
		WHERE MONTH(`new_invoice`.`inv_date`)=MONTH(CURDATE()) 
        AND YEAR(`new_invoice`.`inv_date`)=YEAR(CURDATE())
        AND `new_marketing`.`marketing_cabang`=`id_cabang`
		AND (`new_invoice`.`inv_status`='2' OR `new_invoice`.`inv_status`='3')
        ) AS `jumlah`,
		IFNULL((SELECT 
		SUM(harga*itm_qty - ROUND(harga*itm_qty*trans_ppn) - ROUND(harga*itm_qty*trans_discount) - `trans_dp`)
		FROM `new_invoice` 
        INNER JOIN `new_transaksi` 
        ON `new_transaksi`.`trans_id` = `new_invoice`.`inv_id_trans`
        INNER JOIN `new_trans_items` 
        ON `new_trans_items`.`itm_trans_id` = `new_transaksi`.`trans_id`
        INNER JOIN `paket_training` 
        ON `new_trans_items`.`itm_paket` = `paket_training`.`id`
        INNER JOIN `new_customer` 
        ON `new_customer`.`id_cust` = `new_transaksi`.`trans_cust_id`
        INNER JOIN `new_marketing` 
        ON `new_marketing`.`marketing_id`=`new_customer`.`marketing_id`
		WHERE MONTH(`new_invoice`.`inv_date`)=MONTH(CURDATE()) 
        AND YEAR(`new_invoice`.`inv_date`)=YEAR(CURDATE())
        AND (`new_invoice`.`inv_status` = '2' OR `new_invoice`.`inv_status` = '3')
        AND `new_marketing`.`marketing_cabang`=`id_cabang`),'0') AS `invoice_terbayar`,
        IFNULL((SELECT 
		SUM(harga*itm_qty - ROUND(harga*itm_qty*trans_ppn) - ROUND(harga*itm_qty*trans_discount) - `trans_dp`) AS `jumlah` 
		FROM `new_invoice` 
        INNER JOIN `new_transaksi` 
        ON `new_transaksi`.`trans_id` = `new_invoice`.`inv_id_trans`
        INNER JOIN `new_trans_items` 
        ON `new_trans_items`.`itm_trans_id` = `new_transaksi`.`trans_id`
        INNER JOIN `paket_training` 
        ON `new_trans_items`.`itm_paket` = `paket_training`.`id`
        INNER JOIN `new_customer` 
        ON `new_customer`.`id_cust` = `new_transaksi`.`trans_cust_id`
        INNER JOIN `new_marketing` 
        ON `new_marketing`.`marketing_id`=`new_customer`.`marketing_id`
		WHERE MONTH(`new_invoice`.`inv_date`)=MONTH(CURDATE()) 
        AND YEAR(`new_invoice`.`inv_date`)=YEAR(CURDATE())
        AND `new_invoice`.`inv_status` > '0'
        AND `new_marketing`.`marketing_cabang`=`id_cabang`),'0') AS `omset`
		FROM `cabang` 
		INNER JOIN `perusahaan` 
		ON `perusahaan`.`id`=`cabang`.`id_perusahaan`";	
		$statement = $this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
    }

	public function CountInvoice(){
		$query = "SELECT `new_marketing`.`marketing_id` AS `aidi`,`new_marketing`.`marketing_nama` AS `nama`, 
		(SELECT COUNT(`new_invoice`.`inv_no_invoice`) FROM `new_marketing` INNER JOIN `new_customer` ON `new_customer`.`marketing_id`=`new_marketing`.`marketing_id`
		INNER JOIN `new_transaksi` ON `new_transaksi`.`trans_cust_id` = `new_customer`.`id_cust`
		INNER JOIN `new_invoice` ON `new_invoice`.`inv_id_trans` = `new_transaksi`.`trans_id` 
		WHERE `new_marketing`.`marketing_id`=`aidi`
		AND `new_invoice`.`inv_status`='2' 
		OR `new_marketing`.`marketing_id`=`aidi`
		AND `new_invoice`.`inv_status`='3') AS `jumlah`,
		(SELECT 
		SUM(harga*itm_qty - ROUND(harga*itm_qty*trans_ppn) - ROUND(harga*itm_qty*trans_discount) - `trans_dp`) AS `jumlah` 
		FROM `new_invoice` 
        INNER JOIN `new_transaksi` 
        ON `new_transaksi`.`trans_id` = `new_invoice`.`inv_id_trans`
        INNER JOIN `new_trans_items` 
        ON `new_trans_items`.`itm_trans_id` = `new_transaksi`.`trans_id`
        INNER JOIN `paket_training` 
        ON `new_trans_items`.`itm_paket` = `paket_training`.`id`
        INNER JOIN `new_customer` ON `new_customer`.`id_cust` = `new_transaksi`.`trans_cust_id`
        INNER JOIN `new_marketing` 
        ON `new_marketing`.`marketing_id`=`new_customer`.`marketing_id`
		WHERE `new_invoice`.`inv_status` = '2'
        AND `new_marketing`.`marketing_id`=`aidi` 
        OR `new_invoice`.`inv_status` = '3'
        AND `new_marketing`.`marketing_id`=`aidi`) AS `invoice_terbayar`,
        (SELECT 
		SUM(harga*itm_qty - ROUND(harga*itm_qty*trans_ppn) - ROUND(harga*itm_qty*trans_discount) - `trans_dp`) AS `jumlah` 
		FROM `new_invoice` 
        INNER JOIN `new_transaksi` 
        ON `new_transaksi`.`trans_id` = `new_invoice`.`inv_id_trans`
        INNER JOIN `new_trans_items` 
        ON `new_trans_items`.`itm_trans_id` = `new_transaksi`.`trans_id`
        INNER JOIN `paket_training` 
        ON `new_trans_items`.`itm_paket` = `paket_training`.`id`
        INNER JOIN `new_customer` ON `new_customer`.`id_cust` = `new_transaksi`.`trans_cust_id`
        INNER JOIN `new_marketing` 
        ON `new_marketing`.`marketing_id`=`new_customer`.`marketing_id`
		WHERE `new_invoice`.`inv_status` > '0'
        AND `new_marketing`.`marketing_id`=`aidi`) AS `omset`
		FROM `new_marketing` HAVING invoice_terbayar>0 AND omset>0;";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);

	}

	public function CountInvoiceForCabang(){
		$query = "SELECT `id_cabang`,`nama`,
		(SELECT COUNT(`new_invoice`.`inv_no_invoice`) FROM `new_marketing` INNER JOIN `new_customer` ON `new_customer`.`marketing_id`=`new_marketing`.`marketing_id`
		INNER JOIN `new_transaksi` ON `new_transaksi`.`trans_cust_id` = `new_customer`.`id_cust`
		INNER JOIN `new_invoice` ON `new_invoice`.`inv_id_trans` = `new_transaksi`.`trans_id` 
		WHERE `new_marketing`.`marketing_cabang`=`id_cabang`
		AND (`new_invoice`.`inv_status`='2' OR `new_invoice`.`inv_status`='3')) AS `jumlah`,
		(SELECT 
		SUM(harga*itm_qty - ROUND(harga*itm_qty*trans_ppn) - ROUND(harga*itm_qty*trans_discount) - `trans_dp`) AS `jumlah` 
		FROM `new_invoice` 
        INNER JOIN `new_transaksi` 
        ON `new_transaksi`.`trans_id` = `new_invoice`.`inv_id_trans`
        INNER JOIN `new_trans_items` 
        ON `new_trans_items`.`itm_trans_id` = `new_transaksi`.`trans_id`
        INNER JOIN `paket_training` 
        ON `new_trans_items`.`itm_paket` = `paket_training`.`id`
        INNER JOIN `new_customer` ON `new_customer`.`id_cust` = `new_transaksi`.`trans_cust_id`
        INNER JOIN `new_marketing` 
        ON `new_marketing`.`marketing_id`=`new_customer`.`marketing_id`
		WHERE `new_marketing`.`marketing_cabang`=`id_cabang`
        AND (`new_invoice`.`inv_status` = '2' 
        OR `new_invoice`.`inv_status` = '3')) AS `invoice_terbayar`,
        (SELECT 
		SUM(harga*itm_qty - ROUND(harga*itm_qty*trans_ppn) - ROUND(harga*itm_qty*trans_discount) - `trans_dp`) AS `jumlah` 
		FROM `new_invoice` 
        INNER JOIN `new_transaksi` 
        ON `new_transaksi`.`trans_id` = `new_invoice`.`inv_id_trans`
        INNER JOIN `new_trans_items` 
        ON `new_trans_items`.`itm_trans_id` = `new_transaksi`.`trans_id`
        INNER JOIN `paket_training` 
        ON `new_trans_items`.`itm_paket` = `paket_training`.`id`
        INNER JOIN `new_customer` ON `new_customer`.`id_cust` = `new_transaksi`.`trans_cust_id`
        INNER JOIN `new_marketing` 
        ON `new_marketing`.`marketing_id`=`new_customer`.`marketing_id`
		WHERE `new_invoice`.`inv_status` > '0'
        AND `new_marketing`.`marketing_cabang`=`id_cabang`) AS `omset`
		FROM `cabang` 
		INNER JOIN `perusahaan` 
		ON `perusahaan`.`id`=`cabang`.`id_perusahaan`";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);

	}

	public function ViewTableData(){
		$query = "SELECT `marketing_id`, `marketing_nama`, `marketing_telp`, `marketing_email`, `nama` AS `nama_usaha` 
		FROM `new_marketing`,`cabang`,`perusahaan`
		WHERE new_marketing.marketing_cabang=cabang.id_cabang 
		AND cabang.id_perusahaan=perusahaan.id;";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);

	}

	public function FetchByID(){
		$query ="SELECT `marketing_id`, `marketing_nama`, `marketing_telp`, `marketing_email`, `marketing_cabang` FROM `new_marketing` WHERE `marketing_id`=:id;";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':id',$this->id, PDO::PARAM_STR,35);

		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);

	}

	public function FetchAllMarketing(){
		$query = "SELECT `marketing_id`, `marketing_nama`, `marketing_telp`, `marketing_email`, `marketing_cabang` 
		FROM `new_marketing` ORDER BY `marketing_nama` ASC;";
		$statement=$this->dbHost->prepare($query);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);

	}

	public function JsMessage($value){
		return "<script>alert('".$value."');<script>";
	}

	public function JsDirect($value){
		return "<script>location.replace('".$value."');<script>";
	}


}

?>