<?php 
include_once 'Koneksi.php';
class ControlAgents extends Koneksi{
	
	function __construct(){
		$this->dbHost = $this->bukaKoneksi();
	}

	public function insertData($objek){
		$query = "INSERT INTO `agents`(`username`, `password`, `no_telepon`, `rekening`, `atas_nama`,`email`, `rating`) 
		VALUES (:username,:passw,:noTelp,:rekening,:atasNama,:email,0)";
		$statement=$this->dbHost->prepare($query);

		$statement->bindParam(':username',$objek->username,PDO::PARAM_STR,50);
		$statement->bindParam(':passw',@md5($objek->password),PDO::PARAM_STR,75);
		$statement->bindParam(':noTelp',$objek->telepon,PDO::PARAM_STR,20);
		$statement->bindParam(':email',$objek->email,PDO::PARAM_STR,125);
		$statement->bindParam(':rekening',$objek->rekening,PDO::PARAM_STR,25);
		$statement->bindParam(':atasNama',$objek->atasNama,PDO::PARAM_STR,75);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
	}


	public function fetchAllDatas(){
		$query = "SELECT * FROM agents";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function fetchByID($objek){
		$query = "SELECT * FROM agents WHERE username=:username";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':username',$objek->username,PDO::PARAM_STR,50);
		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_OBJ);


	}


	public function login($objek){
		$query = "SELECT * FROM agents WHERE username=:username AND password=:password";

		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':username',$objek->username,PDO::PARAM_STR,50);
		$statement->bindParam(':password',$objek->password,PDO::PARAM_STR,50);
		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function updateData(){
		
	}

}



?>