<?php 
include_once 'Koneksi.php';
class QueryBuilder extends Koneksi{
    private $order;
    private $limit;

    public function __construct(){
        $this->dbHost = $this->bukaKoneksi();
    }

    public function CheckTable($namatabel){
    	// default table = u2032818_data
    	$query = "SELECT COUNT(table_name) AS `jumlah` 
    	FROM information_schema.tables 
    	WHERE table_schema='facinsti_data' 
    	AND table_name=:namatabel";
    	$statement = $this->dbHost->prepare($query);
    	$statement->bindParam(':namatabel',$namatabel);
    	$statement->execute();
    	$result = $statement->fetch(PDO::FETCH_ASSOC);

    	return $result['jumlah'];
    }

    public function ExecuteUpdateSQLQuery($query){
    	$statement=$this->dbHost->prepare($query);
    	if ($statement->execute()) {
			return true;
		}else{
			return false;
		}	
    }

    public function ExecuteSQLQuery($query){
    	$statement=$this->dbHost->prepare($query);

		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function GetCond($atr,$cond,$key){
    	return ['atr' => $atr, 'cond' => $cond, 'keyid' => $key];
    }

    public function getJoin($type,$tbl,$atr,$link){
    	return ['type'=>$type,'tabel'=>$tbl,'atr' => $atr,'link' => $link];
    }

    public function InsertData($tabel,$arr){ //array assoc(atr=>value) as input
		$num = 1;
		$query = "INSERT INTO $tabel ".$this->queryinsertvalue($arr);
		$statement=$this->dbHost->prepare($query);
		
		foreach ($arr as $key => &$value) {
			$statement->bindParam(':'.$this->removedot($key),$value);
		}
		
		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function UpdateData($tabel,$arr,$cond){ //array assoc(atr=>value) as input
		$query = "UPDATE $tabel SET ".
		$this->supportupdate($arr).
		" ".$this->swhere($cond);
		$statement=$this->dbHost->prepare($query);

		foreach ($arr as $key => &$value) {
			$statement->bindParam(':'.$this->removedot($key),$value);
		}

		$this->sstmcond($cond,$statement);

		if ($statement->execute()) {
			return true;
		}else{
			return false;//'nothing changed';
		}
	}

	public function DeleteWhere($tabel,$cond){
		$query = "DELETE FROM $tabel ".$this->swhere($cond);
		$statement=$this->dbHost->prepare($query);
		
		$this->sstmcond($cond,$statement);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function HasRow($atr,$tabel,$cond){
		$query = "SELECT COUNT($atr) AS `jumlah` FROM $tabel ".
		$this->swhere($cond).";";
		$statement=$this->dbHost->prepare($query);
		$this->sstmcond($cond,$statement);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);
		if ($result['jumlah']=='1') {
			return true;
		} else {
			return false;
		}
	}
	
	public function OnlyFetchCount($atr,$tabel){
		$query = "SELECT COUNT($atr) AS `jumlah` FROM $tabel ";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);
		
		return $result['jumlah'];

	}

	public function FetchCount($atr,$tabel,$cond){
		$query = "SELECT COUNT($atr) AS `jumlah` FROM $tabel ".
		$this->swhere($cond).";";
		$statement=$this->dbHost->prepare($query);
		$this->sstmcond($cond,$statement);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);
		
		return $result['jumlah'];

	}

	public function Fetch($tabel,$offset='0',$limit='0'){
		$query = "SELECT * FROM $tabel".$this->sorder($this->order).$this->slimit($offset,$limit).";";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();
		// return $query;
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function FetchSomeColumns($arr,$tabel,$offset='0',$limit='0'){
		$query = "SELECT ".$this->sfetch($arr)." FROM $tabel".$this->sorder($this->order).$this->slimit($offset,$limit).";";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();
		// return $query;
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function FetchColumns($arr,$tabel,$offset='0',$limit='0'){
		$query = "SELECT ".$this->sfetch($arr).
		" FROM $tabel".
		$this->sorder($this->order).
		$this->slimit($offset,$limit).";";

		$statement=$this->dbHost->prepare($query);
		$statement->execute();
		// return $query;
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function FetchWhere($arr,$tabel,$cond,$offset='0',$limit='0'){
		$query = "SELECT ".$this->sfetch($arr)." FROM ".$tabel." ".
		$this->swhere($cond).
		$this->sorder($this->order).
		$this->slimit($offset,$limit).";";
		$statement=$this->dbHost->prepare($query);

		$this->sstmcond($cond,$statement);

		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function FetchOneRow($arr,$tabel,$cond){
		$query = "SELECT ".$this->sfetch($arr)." FROM ".$tabel." ".$this->swhere($cond);
		$statement=$this->dbHost->prepare($query);

		$this->sstmcond($cond,$statement);		

		$statement->execute();
		return $statement->fetch(PDO::FETCH_ASSOC);
	}

	public function FetchJoin($arr,$tabel,$arrjoin,$offset='0',$limit='0'){
		$query = "SELECT ".$this->sfetch($arr)." FROM $tabel ".
		$this->sjoin($arrjoin).
		$this->sorder($this->order).
		$this->slimit($offset,$limit).";";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function FetchJoinWhere($arr,$tabel,$arrjoin,$cond,$offset='0',$limit='0'){
		$query = "SELECT ".$this->sfetch($arr)." FROM $tabel ".
		$this->sjoin($arrjoin)." ".
		$this->swhere($cond).
		$this->sorder($this->order).
		$this->slimit($offset,$limit).";";
		$statement=$this->dbHost->prepare($query);

		$this->sstmcond($cond,$statement);
		// echo $query;
		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function FetchJoinWhereSingleRow($arr,$tabel,$arrjoin,$cond){
		$query = "SELECT ".$this->sfetch($arr)." FROM $tabel ".
		$this->sjoin($arrjoin)." ".
		$this->swhere($cond).";";
		$statement=$this->dbHost->prepare($query);
		
		$this->sstmcond($cond,$statement);
		
		$statement->execute();
		return $statement->fetch(PDO::FETCH_ASSOC);
	}

	function sstmcond($cond,$stm){
		for ($i = 0; $i < count($cond); $i++) {
			$stm->bindParam(':'.$this->removedot($cond[$i]['atr']),$cond[$i]['keyid']);
		}
	}

	function sjoin($arr){
		$query = "";
		if (count($arr)=='1') {
			$query.=$arr[0]['type'].' JOIN '.$arr[0]['tabel'].' ON '.$arr[0]['atr'].'='.$arr[0]['link'];
		}else{
			$query.=$arr[0]['type'].' JOIN '.$arr[0]['tabel'].' ON '.$arr[0]['atr'].'='.$arr[0]['link'];
			for ($i = 1; $i < count($arr); $i++) {
				$query.= ' '.$arr[$i]['type'].' JOIN '.$arr[$i]['tabel'].' ON '.$arr[$i]['atr'].'='.$arr[$i]['link'];
			}
		}
		return $query;
	}

	function slimit($offset,$limit){
		if ($offset=='0'&&$limit=='0') {
			return '';
		}else{
			return " LIMIT $offset,$limit" ;
		}
	}

	function sorder($order=''){
		if ($order=='') {
			return '';
		} else {
			$query=" ORDER BY ".$order['atr']." ".$order['cond'];
			return $query;
		}
		
	}

	function swhere($cond){
		$query = "WHERE ";
		if (count($cond)=='1') {
			$query.=$cond[0]['atr'].' '.$cond[0]['cond'].' '.":".$this->removedot($cond[0]['atr']);
		}else{
			$query.=$cond[0]['atr'].' '.$cond[0]['cond']." :".$this->removedot($cond[0]['atr']);
			for ($i = 1; $i < count($cond); $i++) {
				$query.= " AND ".$cond[$i]['atr'].' '.$cond[$i]['cond'].' '.":".$this->removedot($cond[$i]['atr']);
			}
		}
		return $query;
	}

	function sfetch($arr){
		$query = "";
		foreach ($arr as $key) {
			$query .= $key.',';		
		}
		$query = substr($query, 0,-1);
		return $query;
	}

	function supportupdate($arr){
		$query = "";
		foreach ($arr as $key => $value) {
			$query .= "$key = :".$this->removedot($key).", ";
		}
		$query = substr($query, 0,-2);
		return $query;
	}

	function removedot($value){
		return str_replace('.', '', $value);
	}

	public function queryinsertvalue($arr){
		$query = "(";
		foreach ($arr as $key => $value) {
			$query .= $key.',';		
		}
		$query .=') VALUES(';

		foreach ($arr as $key => $value) {
			$query .= ":".$this->removedot($key).",";		
		}
		$query .=');';
		$query = str_replace(',)', ')', $query);
		return $query;
	}


    public function getOrder(){
        return $this->order;
    }

    public function setOrder($atr,$cond){
    	if ($atr==''||$cond=='') {
    		$this->order = '';
    	} else {
    		$this->order = ['atr'=>$atr,'cond'=>$cond];	
    	}
        
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setLimit($limit)
    {
        $this->limit = $limit;

        return $this;
    }
}

?>