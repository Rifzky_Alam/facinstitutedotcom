<?php 

class Surat{
	
	public function penawaran($data){


		return '<table style="margin:0;padding:0" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr style="background-color: #0066ff;">
                            <td style="color:white;margin:0 auto;font-size:22px" align="center" valign="middle" height="60"><strong>FAC-Institute</strong></td>
                        </tr>
                    </tbody>
                </table>            
            </td>
        </tr>
        <tr>
            <table  align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tbody>
            <tr>
            <td style="width:50%"><center><img src="http://fac-institute.com/images/logoaccurate5.png" style="width:50%;height:60px"></center></td>
            <td style="width:50%"><center><img src="http://fac-institute.com/images/logo-fac.jpg" style="width:50%;height:100px"></center></td>
            </tr>
            </tbody>
            </table>
        </tr>
        <tr>
            <td valign="middle" height="60"><h4>Dear '.$data->nama.',</h4></td>
        </tr>
        <tr>
            <td>
                <p>
                    Kami dari FAC Institute sebagai divisi onsite training Accurate. Terimakasih telah memilih Accurate sebagai program akuntansi di <strong>'.$data->perusahaan.'</strong>.
                     Terimakasih pula atas order menggunakan jasa FAC Institute dalam mengimplementasikan Accurate.<br> 
                    Berikut ini kami kirimkan jadwal dan invoice training Accurate : 
                </p>
                <br>
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr>
                            <td style="width:50%;">
                            <label><strong>WAKTU & TEMPAT PELAKSANAAN</strong></label>
                            <table style="padding-top:10px">
                            <tbody>
                            <tr>
                                <td style="width:150px;vertical-align:top;">Tanggal</td>
                                <td style="vertical-align:top;">:</td>
                                <td>
                                    <ul type="none" style="padding-left:10px;margin-top:0px;">
                                        '.$this->getDateOfTraining($data->tanggalz).'
                                    </ul>
                                </td>
                            </tr>
                            <tr><td>Waktu</td><td>:</td><td>'.$data->waktu.'</td></tr>
                            <tr><td>Tempat</td><td>:</td><td>'.$data->tempat.'</td></tr>
                            <tr><td>Alamat</td><td>:</td><td>'.$data->lokasi.'</td></tr>
                            </tbody>
                            </table>
                            </td>
                            <td style="text-align:center;vertical-align:top;">
                            <label><strong>AGENDA KEGIATAN</strong></label>
                            <table style="padding-top:10px">
                            <tbody>
                            <tr>
                                <td style="text-align: left;">
                                    <ol>
                                        '.$this->getListOfAgenda($data->agenda).'
                                    </ol>
                                </td>
                            </tr>
                            
                            </tbody>
                            </table>
                            </td>
                        </tr>
                        
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td><br>
            <table valign="top" align="center" border="0" style="padding-bottom:30;" bgcolor="#f9ffee" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                    <tr>
                        <td>
                            <table valign="top" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tbody>
                                    <tr><td><center><h3><strong>BIAYA</strong></h3></center></td></tr>
                                    <tr>
                                    <td style="margin-bottom:10px">
                                    <table border="1" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Keterangan</th>
                                        <th>Biaya</th>
                                        <th>Qty</th>
                                        <th>Jumlah</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>'.$data->item.'</td>
                                        <td style="text-align:right">'.number_format($data->harga).' IDR</td>
                                        <td style="text-align:center">'.$data->x.'</td>
                                        <td style="text-align:right">'.number_format(intval($data->harga)*intval($data->x)).' IDR</td>
                                    </tr>
                                    <tr>
                                        <td>Biaya Transport</td>
                                        <td style="text-align:right">'.number_format($data->transport).' IDR</td>
                                        <td style="text-align:center">'.$data->jmlhari.'</td>
                                        <td style="text-align:right"> '.number_format(intval($data->transport)*intval($data->jmlhari)).' IDR</td>
                                    </tr>
                                    <tr>
                                    <td colspan="3"><strong>Total</strong></td>
                                    <td style="text-align:right"><strong> '.number_format(intval($data->harga)*intval($data->x)+intval($data->transport)*intval($data->jmlhari)).' IDR</strong></td>
                                    </tr>
                                    </tbody>
                                    </table>
                                    </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>

            </table>
                
            </td>
        </tr>
        <tr>
            <td>
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                <tr><td>
                <h3>PEMBAYARAN</h3>
                '.$this->syaratBayar($data->metodeBayar).'
                <ul type="square">
                    <li><strong>Bank BCA KCU Bekasi No Acc. 0663162851 a/n : Fajar Shodiq</strong></li>
                    <li><strong>Bank Mandiri KCP Bekasi Ahmad Yani No. Acc. 1670001377901 a/n : Fajar Shodiq</strong></li>
                </ul></td>
                </tr>
                </tbody>
                </table> 
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr style="background-color: #660000;">
                            <td style="color:white;margin:0 auto" align="center" valign="middle" height="40">SYARAT DAN KETENTUAN</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                '.$this->syaratDanKetentuan('').'
            </td>
        </tr>
        <tr>
            <td>
                <p>Demikianlah semoga menjadi maklum adanya. Terimakasih atas perhatian dan kerjasamanya.</p>
                <p>
                Jika ada hal-hal yang ingin ditanyakan, jangan sungkan untuk senantiasa menghubungi kami. Terimakasih, <br><br>Salam <br><br>
                </p>
                <p><strong>Linda Kartinah</strong> <br> <strong>Officer of Marketing Division</strong></p>
            </td>
        </tr>
        <tr>
            <td style="padding:0 0 0 0" align="center" bgcolor="#0066ff">
          <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:#0066ff">
            <tbody>
              <tr>
                <td>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr height="30">
                        <td align="center" width="100%" height="16"></td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr>
                        <td align="center" valign="top">
                          <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                              <tr>
                                <td align="left" width="20"></td>
                                <td align="left" valign="top">
                                  <table align="center" border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                      <tr height="15">
                                        <td align="left" height="15">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td style="line-height:18px" align="center" valign="top">
                                          <span style="font-family:'."'"."Open Sans"."'".',sans-serif;color:white;font-size:12px;text-align:center">Email ini dikirim oleh: <strong><a href="http://www.fac-institute.com" style="color:white">FAC-Institute</a></strong><br>
                                          Jl. Jatiwaringin No.8 Pangkalan Jati, Jakarta Timur<br>
                                          Telp. <a href="tel://+6281290083983" style="color:white">+6281290083983</a></span><br>
                                          <br>
                                            <a href="http://www.fac-institute.com" style="font-family:'."'"."Open Sans"."'".',sans-serif;font-size:22px;color:white">www.fac-institute.com</a><br><br>
                                            <p style="margin:0 0 1em 0;font-family:'."'"."Open Sans"."'".',sans-serif;font-size:16px;line-height:26px;color:white;font-weight:300;text-align:center">Konsultan Akuntansi dan Lembaga Pendidikan Komputerisasi Akuntansi</p>
                                        </td>
                                      </tr>
                                      <tr height="15">
                                        <td align="left" height="15">&nbsp;</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                                <td align="left" width="20"></td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr height="30">
                        <td align="left" width="100%" height="16"></td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
        </tr>
    </tbody>
</table>';
	}

public function kirimPenawaran2($data){
    $onsiteStandard=1000000;
    $paketStandard=4700000;
    $onsiteExpert=1300000;
    $paketEnterpriseSatu=12350000;
    $paketEnterpriseDua=23400000;
    $accurateOnline=1250000;
    $paketAccurateOnline=6000000;


    if ($data->marketer=='mm'){
        $judul = array(
            'atas' => 'STANDARD, DELUXE EDITION & RENE (STANDARD)', 
            'atas2' => 'ENTERPRISE (EXPERT)'
        );
        $hargaOS='1.000.000.00';
        $hargaPS='4.500.000.00';        
    }else{
        $judul = array(
            'atas' => 'STANDARD, DELUXE EDITION - NON KONTRAKTOR & RENE (STANDARD)', 
            'atas2' => 'ENTERPRISE, DELUXE EDITION - KONTRAKTOR (EXPERT)'
        );
        $hargaOS='750.000.00';
        $hargaPS='3.500.000.00';
    }

        return '<table style="margin:0;padding:0" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr style="background-color: #0066ff;">
                            <td style="color:white;margin:0 auto;font-size:22px" align="center" valign="middle" height="60"><strong>FAC-Institute</strong></td>
                        </tr>
                    </tbody>
                </table>            
            </td>
        </tr>
        <tr>
            <table  align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tbody>
            <tr>
            <td style="width:50%"><center><img src="http://fac-institute.com/images/logoaccurate5.png" style="width:50%;height:60px"></center></td>
            <td style="width:50%"><center><img src="http://fac-institute.com/images/logo-fac.jpg" style="width:50%;height:100px"></center></td>
            </tr>
            </tbody>
            </table>
        </tr>
        <tr>
            <td valign="middle" height="60"><h4>Kepada Yth,</h4></td>
        </tr>
        <tr>
            <td valign="middle" height="30"><p><strong>'.$data->np.'</strong></p></td>
        </tr>
        <tr>
            <td valign="middle" height="30"><div><label>UP: </label>'.$data->cp.'</div></td>
        </tr>
        <tr>
            <td>
                <p>
                    Dengan Hormat, <br>
                    Terimakasih atas ketertarikan menggunakan program Accurate sebagai program akuntansi di <strong>'.$data->np.'</strong>.
                    untuk <strong>Jasa Training Accurate</strong> kami memberikan penawaran terbaik berupa.<br> 
                </p>
                <br>
            </td>
        </tr>
        <tr>
            <td><br>
            <table valign="top" align="center" border="0" style="padding-bottom:30;" bgcolor="#f9ffee" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                    <td>
                        <table valign="top" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                                <tr><td><center><h3><strong>IMPLEMENTASI SOFTWARE ACCURATE & RENE</strong></h3></center></td></tr>
                                <tr>
                                <td style="margin-bottom:10px">
                                <table border="1" width="100%">
                                <thead><tr><th>Nama Produk</th><th>Qty</th><th>Harga</th></tr></thead>
                                <tbody>
                                    <tr>
                                    <td colspan="3" style="text-align:center">
                                        <strong>'.$judul['atas'].'</strong>
                                    </td>
                                    </tr>
                                    <tr>
                                        <td>Onsite Standard (OS)</td>
                                        <td style="text-align:center">....Hari</td>
                                        <td style="text-align:right">Rp '.number_format($onsiteStandard).'</td>
                                    </tr>
                                    <tr>
                                        <td>Paket Standard (PS)</td>
                                        <td style="text-align:center">....x 5 Hari</td>
                                        <td style="text-align:right">Rp '.number_format($paketStandard).'</td>
                                    </tr>

                                    <tr>
                                    <td colspan="3" style="text-align:center">
                                        <strong>'.$judul['atas2'].'</strong>
                                    </td>
                                    </tr>
                                    <tr>
                                        <td>Onsite Expert (OE)</td>
                                        <td style="text-align:center">....Hari</td>
                                        <td style="text-align:right">Rp '.number_format($onsiteExpert).'</td>
                                    </tr>
                                    <tr>
                                        <td>Paket Expert 1 (PE 1)</td>
                                        <td style="text-align:center">....x 10 Hari</td>
                                        <td style="text-align:right">Rp '.number_format($paketEnterpriseSatu).'</td>
                                    </tr>
                                    <tr>
                                        <td>Paket Expert 2 (PE 2)</td>
                                        <td style="text-align:center">....x 20 Hari</td>
                                        <td style="text-align:right">Rp '.number_format($paketEnterpriseDua).'</td>
                                    </tr>

                                    <tr>
                                    <td colspan="3" style="text-align:center">
                                        <strong>ACCURATE ONLINE (AOL)</strong>
                                    </td>
                                    </tr>
                                    <tr>
                                        <td>Accurate Online 1 (AOL 1)</td>
                                        <td style="text-align:center">....Hari</td>
                                        <td style="text-align:right">Rp '.number_format($accurateOnline).'</td>
                                    </tr>
                                    <tr>
                                        <td>Accurate Online 2 (AOL 2)</td>
                                        <td style="text-align:center">....x 5 Hari</td>
                                        <td style="text-align:right">Rp '.number_format($paketAccurateOnline).'</td>
                                    </tr>                             

                                </tbody>
                                </table>
                                </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tbody>

            </table>
                
            </td>
        </tr>
        <br>
        '.$this->myNotes('').'
        <br>

        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr style="background-color: #660000;">
                            <td style="color:white;margin:0 auto" align="center" valign="middle" height="40">PEMBAYARAN</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        '.$this->syaratBayar('').'
        <tr>
            <td>
                <ul>
                    <li><strong>Bank BCA KCU Bekasi No Acc. 0663162851 a/n : Fajar Shodiq</strong></li>
                    <li><strong>Bank Mandiri KCP Bekasi Ahmad Yani No. Acc. 1670001377901 a/n : Fajar Shodiq</strong></li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <p>Silahkan untuk mengisi data yang ada di lampiran dan membalas email ini dengan data yang sudah terisi ini atau anda dapat <a href="http://fac-institute.com/daftar-training">mengisi secara online</a></p>
                <p>
                Jika ada hal-hal yang ingin ditanyakan, jangan sungkan untuk senantiasa menghubungi kami. Terimakasih, <br><br>Salam <br><br>
                </p>
                <p><strong>Linda Kartinah</strong> <br> <strong>Marketing Division Staff of FAC Institute</strong></p>
            </td>
        </tr>
        <tr>
            <td style="padding:0 0 0 0" align="center" bgcolor="#0066ff">
          <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:#0066ff">
            <tbody>
              <tr>
                <td>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr height="30">
                        <td align="center" width="100%" height="16"></td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr>
                        <td align="center" valign="top">
                          <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                              <tr>
                                <td align="left" width="20"></td>
                                <td align="left" valign="top">
                                  <table align="center" border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                      <tr height="15">
                                        <td align="left" height="15">&nbsp;</td>
                                      </tr>
                                      <tr>
                                      <td style="line-height:18px" align="center" valign="top">
                                          <span style="font-family:'."'"."Open Sans"."'".',sans-serif;color:white;font-size:12px;text-align:center">Email ini dikirim oleh: <strong><a href="http://www.fac-institute.com" style="color:white">FAC-Institute</a></strong><br>
                                          Jl. Jatiwaringin No.8 Pangkalan Jati, Jakarta Timur<br>
                                          Telp. <a href="tel://+6281290083983" style="color:white">+6281290083983</a></span><br>
                                          <br>
                                            <a href="http://www.fac-institute.com" style="font-family:'."'"."Open Sans"."'".',sans-serif;font-size:22px;color:white">www.fac-institute.com</a><br><br>
                                            <p style="margin:0 0 1em 0;font-family:'."'"."Open Sans"."'".',sans-serif;font-size:16px;line-height:26px;color:white;font-weight:300;text-align:center">Konsultan Akuntansi dan Lembaga Pendidikan Komputerisasi Akuntansi</p>
                                        </td>
                                      </tr>
                                      <tr height="15">
                                        <td align="left" height="15">&nbsp;</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                                <td align="left" width="20"></td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr height="30">
                        <td align="left" width="100%" height="16"></td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
        </tr>
    </tbody>
</table>';
    }

    public function PenawaranUTS($data){
    $onsiteStandard=1000000;
    $paketStandard=4700000;
    $onsiteExpert=1300000;
    $paketEnterpriseSatu=12350000;
    $paketEnterpriseDua=23400000;
    $accurateOnline=1250000;
    $paketAccurateOnline=6000000;


        return '<table style="margin:0;padding:0" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr style="background-color: #0066ff;">
                            <td style="color:white;margin:0 auto;font-size:22px" align="center" valign="middle" height="60"><strong>FAC-Institute</strong></td>
                        </tr>
                    </tbody>
                </table>            
            </td>
        </tr>
        <tr>
            <table  align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tbody>
            <tr>
            <td style="width:50%"><center><img src="http://fac-institute.com/images/logoaccurate5.png" style="width:50%;height:60px"></center></td>
            <td style="width:50%"><center><img src="http://fac-institute.com/images/logo-fac.jpg" style="width:50%;height:100px"></center></td>
            </tr>
            </tbody>
            </table>
        </tr>
        <tr>
            <td valign="middle" height="60"><h4>Kepada Yth,</h4></td>
        </tr>
        <tr>
            <td valign="middle" height="30"><p><strong>'.$data->np.'</strong></p></td>
        </tr>
        <tr>
            <td valign="middle" height="30"><div><label>UP: </label>'.$data->cp.'</div></td>
        </tr>
        <tr>
            <td>
                <p>
                    Dengan Hormat, <br>
                    Terimakasih atas ketertarikan menggunakan program Accurate sebagai program akuntansi di <strong>'.$data->np.'</strong>.
                    untuk <strong>Jasa Training Accurate</strong> kami memberikan penawaran terbaik berupa.<br> 
                </p>
                <br>
            </td>
        </tr>
        <tr>
            <td><br>
            <table valign="top" align="center" border="0" style="padding-bottom:30;" bgcolor="#f9ffee" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                    <td>
                        <table valign="top" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                                <tr><td><center><h3><strong>IMPLEMENTASI SOFTWARE ACCURATE & RENE</strong></h3></center></td></tr>
                                <tr>
                                <td style="margin-bottom:10px">
                                <table border="1" width="100%">
                                <thead><tr><th>Nama Produk</th><th>Qty</th><th>Harga</th></tr></thead>
                                <tbody>
                                    <tr>
                                    <td colspan="3" style="text-align:center">
                                        <strong>STANDARD, DELUXE EDITION & RENE (STANDARD)</strong>
                                    </td>
                                    </tr>
                                    <tr>
                                        <td>Onsite Standard (OS)</td>
                                        <td style="text-align:center">....Hari</td>
                                        <td style="text-align:right">Rp '.number_format($onsiteStandard).'</td>
                                    </tr>
                                    <tr>
                                        <td>Paket Standard (PS)</td>
                                        <td style="text-align:center">....x 5 Hari</td>
                                        <td style="text-align:right">Rp '.number_format($paketStandard).'</td>
                                    </tr>

                                    <tr>
                                    <td colspan="3" style="text-align:center">
                                        <strong>ENTERPRISE (EXPERT)</strong>
                                    </td>
                                    </tr>
                                    <tr>
                                        <td>Onsite Expert (OE)</td>
                                        <td style="text-align:center">....Hari</td>
                                        <td style="text-align:right">Rp '.number_format($onsiteExpert).'</td>
                                    </tr>
                                    <tr>
                                        <td>Paket Expert 1 (PE 1)</td>
                                        <td style="text-align:center">....x 10 Hari</td>
                                        <td style="text-align:right">Rp '.number_format($paketEnterpriseSatu).'</td>
                                    </tr>
                                    <tr>
                                        <td>Paket Expert 2 (PE 2)</td>
                                        <td style="text-align:center">....x 20 Hari</td>
                                        <td style="text-align:right">Rp '.number_format($paketEnterpriseDua).'</td>
                                    </tr>

                                    <tr>
                                    <td colspan="3" style="text-align:center">
                                        <strong>ACCURATE ONLINE (AOL)</strong>
                                    </td>
                                    </tr>
                                    <tr>
                                        <td>Accurate Online 1 (AOL 1)</td>
                                        <td style="text-align:center">....Hari</td>
                                        <td style="text-align:right">Rp '.number_format($accurateOnline).'</td>
                                    </tr>
                                    <tr>
                                        <td>Accurate Online 2 (AOL 2)</td>
                                        <td style="text-align:center">....x 5 Hari</td>
                                        <td style="text-align:right">Rp '.number_format($paketAccurateOnline).'</td>
                                    </tr>                             

                                </tbody>
                                </table>
                                </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tbody>

            </table>
                
            </td>
        </tr>
        <br>
        '.$this->myNotes('').'
        <br>

        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr style="background-color: #660000;">
                            <td style="color:white;margin:0 auto" align="center" valign="middle" height="40">PEMBAYARAN</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li><strong>Bank BCA KCU Bekasi No Acc. 0663162851 a/n : Fajar Shodiq</strong></li>
                    <li><strong>Bank Mandiri KCP Bekasi Ahmad Yani No. Acc. 1670001377901 a/n : Fajar Shodiq</strong></li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <p>Silahkan untuk mengisi data yang ada di lampiran dan membalas email ini dengan data yang sudah terisi ini atau anda dapat <a href="http://fac-institute.com/daftar-training">mengisi secara online</a></p>
                <p>
                Jika ada hal-hal yang ingin ditanyakan, jangan sungkan untuk senantiasa menghubungi kami. Terimakasih, <br><br>Salam <br><br>
                </p>
                <p><strong>'.$data->petugas.'</strong> <br> <strong>Marketing Division Staff of FAC Institute</strong></p>
            </td>
        </tr>
        <tr>
            <td style="padding:0 0 0 0" align="center" bgcolor="#0066ff">
          <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:#0066ff">
            <tbody>
              <tr>
                <td>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr height="30">
                        <td align="center" width="100%" height="16"></td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr>
                        <td align="center" valign="top">
                          <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                              <tr>
                                <td align="left" width="20"></td>
                                <td align="left" valign="top">
                                  <table align="center" border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                      <tr height="15">
                                        <td align="left" height="15">&nbsp;</td>
                                      </tr>
                                      <tr>
                                      <td style="line-height:18px" align="center" valign="top">
                                          <span style="font-family:'."'"."Open Sans"."'".',sans-serif;color:white;font-size:12px;text-align:center">Email ini dikirim oleh: <strong><a href="http://www.fac-institute.com" style="color:white">FAC-Institute</a></strong><br>
                                          Jl. Jatiwaringin No.8 Pangkalan Jati, Jakarta Timur<br>
                                          Telp. <a href="tel://+6281290083983" style="color:white">+6281290083983</a></span><br>
                                          <br>
                                            <a href="http://www.fac-institute.com" style="font-family:'."'"."Open Sans"."'".',sans-serif;font-size:22px;color:white">www.fac-institute.com</a><br><br>
                                            <p style="margin:0 0 1em 0;font-family:'."'"."Open Sans"."'".',sans-serif;font-size:16px;line-height:26px;color:white;font-weight:300;text-align:center">Konsultan Akuntansi dan Lembaga Pendidikan Komputerisasi Akuntansi</p>
                                        </td>
                                      </tr>
                                      <tr height="15">
                                        <td align="left" height="15">&nbsp;</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                                <td align="left" width="20"></td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr height="30">
                        <td align="left" width="100%" height="16"></td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
        </tr>
    </tbody>
</table>';
    }

    public function quotation($objek){
        return '<table style="margin:0;padding:0" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr style="background-color: #0066ff;">
                            <td style="color:white;margin:0 auto;font-size:22px" align="center" valign="middle" height="60"><strong>FAC-Institute</strong></td>
                        </tr>
                    </tbody>
                </table>            
            </td>
        </tr>
        <tr>
            <table  align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tbody>
            <tr>
            <td style="width:50%"><center><img src="http://fac-institute.com/images/logoaccurate5.png" style="width:50%;height:60px"></center></td>
            <td style="width:50%"><center><img src="http://fac-institute.com/images/logo-fac.jpg" style="width:50%;height:100px"></center></td>
            </tr>
            </tbody>
            </table>
        </tr>
        <tr>
            <td valign="middle" height="60"><h4>Dear '.$objek->nama_personal.',</h4></td>
        </tr>
        <tr>
            <td>
                <p>
                    Dengan Hormat, <br>
                    Terimakasih atas ketertarikan menggunakan program Accurate sebagai program akuntansi di <strong>'.$objek->nama_usaha.'</strong>.
                    untuk <strong>'.$objek->subject.'</strong> kami memberikan penawaran terbaik berupa.<br> 
                </p>                
                <br>
            </td>
        </tr>

        <tr>
            <td><br>
            <table valign="top" align="center" border="0" style="padding-bottom:30;" bgcolor="#f9ffee" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                    <td>
                        <table valign="top" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                                <tr><td><center><h3><strong>PRODUK/LAYANAN</strong></h3></center></td></tr>
                                <tr>
                                <td style="margin-bottom:10px">
                                <table border="1" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Keterangan</th>
                                            <th>Biaya</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>'.$objek->produk.'</td>
                                        <td>Rp '.number_format($objek->biaya).'.-</td>
                                    </tr>
                                    <tr>
                                        <td>Biaya transport</td>
                                        <td>Rp '.number_format($objek->biaya_transport).'.-</td>
                                    </tr>
                                    </tbody>
                                </table>
                                </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tbody>

            </table>
                
            </td>
        </tr>
        '.$this->myNotes($objek->notes).'
        <br>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr style="background-color:#660066;">
                            <td style="color:white;margin:0 auto" align="center" valign="middle" height="40">PEMBAYARAN</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>

        '.$this->syaratBayar('').'

        <tr>
            <td>
                <ul>
                    <li><strong>Bank BCA KCU Bekasi No Acc. 0663162851 a/n : Fajar Shodiq</strong></li>
                    <li><strong>Bank Mandiri KCP Bekasi Ahmad Yani No. Acc. 1670001377901 a/n : Fajar Shodiq</strong></li>
                </ul>
            </td>
        </tr>

        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr style="background-color: #660000;">
                            <td style="color:white;margin:0 auto" align="center" valign="middle" height="40">SYARAT DAN KETENTUAN</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                '.$this->syaratDanKetentuan('').'
            </td>
        </tr>
        <tr>
            <td>
                <p>Demikianlah semoga menjadi maklum adanya. Terimakasih atas perhatian dan kerjasamanya.</p>
                <p>
                Jika ada hal-hal yang ingin ditanyakan, jangan sungkan untuk senantiasa menghubungi kami. <br>Terimakasih, <br><br>Salam <br><br>
                </p>
                <p><strong>'.$objek->petugas.'</strong> <br> <strong>Officer of Marketing Division</strong></p>
            </td>
        </tr>
        <tr>
            <td style="padding:0 0 0 0" align="center" bgcolor="#0066ff">
          <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:#0066ff">
            <tbody>
              <tr>
                <td>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr height="30">
                        <td align="center" width="100%" height="16"></td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr>
                        <td align="center" valign="top">
                          <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                              <tr>
                                <td align="left" width="20"></td>
                                <td align="left" valign="top">
                                  <table align="center" border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                      <tr height="15">
                                        <td align="left" height="15">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td style="line-height:18px" align="center" valign="top">
                                          <span style="font-family:'."'"."Open Sans"."'".',sans-serif;color:white;font-size:12px;text-align:center">Email ini dikirim oleh: <strong><a href="http://www.fac-institute.com" style="color:white">FAC-Institute</a></strong><br>
                                          Jl. Jatiwaringin No.8 Pangkalan Jati, Jakarta Timur<br>
                                          Telp. <a href="tel://+6281290083983" style="color:white">+6281290083983</a></span><br>
                                          <br>
                                            <a href="http://www.fac-institute.com" style="font-family:'."'"."Open Sans"."'".',sans-serif;font-size:22px;color:white">www.fac-institute.com</a><br><br>
                                            <p style="margin:0 0 1em 0;font-family:'."'"."Open Sans"."'".',sans-serif;font-size:16px;line-height:26px;color:white;font-weight:300;text-align:center">Konsultan Akuntansi dan Lembaga Pendidikan Komputerisasi Akuntansi</p>
                                        </td>
                                      </tr>
                                      <tr height="15">
                                        <td align="left" height="15">&nbsp;</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                                <td align="left" width="20"></td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr height="30">
                        <td align="left" width="100%" height="16"></td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
        </tr>
    </tbody>
</table>';

    }

    public function KirimJadwal($oke,$petugas){
        
        $data = array(
            'nama' => $oke->nama_personal,
            'namaUsaha' => $oke->nama,
            'tanggalz' => explode(" # ", $oke->tanggal),
            'waktu' => $oke->waktu,
            'lokasi' => $oke->alamat,
            'tempat' => $oke->tempat,
            'agenda' => json_decode($oke->agenda)
        );

        $data = (object) $data;

        return '<table style="margin:0;padding:0" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr style="background-color: #0066ff;">
                            <td style="color:white;margin:0 auto;font-size:22px" align="center" valign="middle" height="60"><strong>FAC-Institute</strong></td>
                        </tr>
                    </tbody>
                </table>            
            </td>
        </tr>
        <tr>
            <table  align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tbody>
            <tr>
            <td style="width:50%"><center><img src="http://fac-institute.com/images/logoaccurate5.png" style="width:50%;height:60px"></center></td>
            <td style="width:50%"><center><img src="http://fac-institute.com/images/logo-fac.jpg" style="width:50%;height:100px"></center></td>
            </tr>
            </tbody>
            </table>
        </tr>
        <tr>
            <td valign="middle" height="60"><h4>Dear '.$data->nama.',</h4></td>
        </tr>
        <tr>
            <td>
                <p>
                    Kami dari FAC Institute sebagai divisi onsite training Accurate.<br> Terimakasih telah memilih Accurate sebagai program akuntansi di <strong>'.$data->namaUsaha.'</strong>.
                     Terimakasih pula atas order menggunakan jasa FAC Institute dalam mengimplementasikan Accurate.<br> 
                    Berikut ini kami kirimkan jadwal training Accurate : 
                </p>
                <br>
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr>
                            <td style="width:50%">
                            <label><strong>WAKTU & TEMPAT PELAKSANAAN</strong></label>
                            <table style="padding-top:10px">
                            <tbody>
                            <tr>
                                <td style="width:150px;vertical-align:top;">Tanggal</td>
                                <td style="vertical-align:top;">:</td>
                                <td>
                                    <ul type="none" style="padding-left:10px;margin-top:0px;">
                                        '.$this->getDateOfTraining($data->tanggalz).'
                                    </ul>
                                </td>
                            </tr>
                            <tr><td>Waktu</td><td>:</td><td>'.$data->waktu.'</td></tr>
                            <tr><td>Tempat</td><td>:</td><td>'.$data->tempat.'</td></tr>
                            <tr><td>Alamat</td><td>:</td><td>'.$data->lokasi.'</td></tr>
                            </tbody>
                            </table>
                            </td>
                            <td style="text-align:center;vertical-align:top;">
                            <label><strong>AGENDA KEGIATAN</strong></label>
                            <table style="padding-top:10px">
                            <tbody>
                            <tr>
                                <td style="text-align: left;">
                                    <ol>
                                        '.$this->getAgenda($data->agenda).'
                                    </ol>
                                </td>
                            </tr>
                            
                            </tbody>
                            </table>
                            </td>
                        </tr>
                        
                    </tbody>
                </table>
            </td>
        </tr>
        <br>
       
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr style="background-color: #660000;">
                            <td style="color:white;margin:0 auto" align="center" valign="middle" height="40">SYARAT DAN KETENTUAN</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                '.$this->syaratDanKetentuan('').'
            </td>
        </tr>
        <tr>
            <td>
                <p>Demikianlah semoga menjadi maklum adanya. Terimakasih atas perhatian dan kerjasamanya.</p>
                <p>
                Jika ada hal-hal yang ingin ditanyakan, jangan sungkan untuk senantiasa menghubungi kami. <br>Terimakasih, <br><br>Salam <br><br>
                </p>
                <p><strong>'.$petugas.'</strong> <br> <strong>Officer of Marketing Division</strong></p>
            </td>
        </tr>
        <tr>
            <td style="padding:0 0 0 0" align="center" bgcolor="#0066ff">
          <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:#0066ff">
            <tbody>
              <tr>
                <td>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr height="30">
                        <td align="center" width="100%" height="16"></td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr>
                        <td align="center" valign="top">
                          <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                              <tr>
                                <td align="left" width="20"></td>
                                <td align="left" valign="top">
                                  <table align="center" border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                      <tr height="15">
                                        <td align="left" height="15">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td style="line-height:18px" align="center" valign="top">
                                          <span style="font-family:'."'"."Open Sans"."'".',sans-serif;color:white;font-size:12px;text-align:center">Email ini dikirim oleh: <strong><a href="http://www.fac-institute.com" style="color:white">FAC-Institute</a></strong><br>
                                          Jl. Jatiwaringin No.8 Pangkalan Jati, Jakarta Timur<br>
                                          Telp. <a href="tel://+6281290083983" style="color:white">+6281290083983</a></span><br>
                                          <br>
                                            <a href="http://www.fac-institute.com" style="font-family:'."'"."Open Sans"."'".',sans-serif;font-size:22px;color:white">www.fac-institute.com</a><br><br>
                                            <p style="margin:0 0 1em 0;font-family:'."'"."Open Sans"."'".',sans-serif;font-size:16px;line-height:26px;color:white;font-weight:300;text-align:center">Konsultan Akuntansi dan Lembaga Pendidikan Komputerisasi Akuntansi</p>
                                        </td>
                                      </tr>
                                      <tr height="15">
                                        <td align="left" height="15">&nbsp;</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                                <td align="left" width="20"></td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr height="30">
                        <td align="left" width="100%" height="16"></td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
        </tr>
    </tbody>
</table>';

    }




    public function kirimPenawaran($obj){
        return '<table style="margin:0;padding:0" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr style="background-color: #0066ff;">
                            <td style="color:white;margin:0 auto;font-size:22px" align="center" valign="middle" height="60"><strong>FAC-Institute</strong></td>
                        </tr>
                    </tbody>
                </table>            
            </td>
        </tr>
        <tr>
            <table  align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tbody>
            <tr>
            <td style="width:50%"><center><img src="http://fac-institute.com/images/logoaccurate5.png" style="width:50%;height:60px"></center></td>
            <td style="width:50%"><center><img src="http://fac-institute.com/images/logo-fac.jpg" style="width:50%;height:100px"></center></td>
            </tr>
            </tbody>
            </table>
        </tr>
        <tr>
            <td valign="middle" height="60"><h4>Kepada Yth,</h4></td>
        </tr>
        <tr>
            <td valign="middle" height="60"><h4>'.$obj->np.'</h4></td>
        </tr>
        <tr>
            <td valign="middle" height="60"><div><label>UP: </label>'.$obj->cp.'</div></td>
        </tr>
        <tr>
            <td>
                <p>
                    Degan Hormat, <br>
                    Terimakasih atas ketertarikan menggunakan program Accurate sebagai program akuntansi di <strong>'.$obj->np.'</strong>.
                    untuk <strong>Jasa Training Accurate</strong> kami memberikan penawaran terbaik berupa.<br> 
                </p>
                <br>
            </td>
        </tr>
        <tr>
            <td><br>
            <table valign="top" align="center" border="0" style="padding-bottom:30;" bgcolor="#f9ffee" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                    <td>
                        <table valign="top" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                                <tr><td><center><h3><strong>BIAYA</strong></h3></center></td></tr>
                                <tr>
                                <td style="margin-bottom:10px">
                                <table border="1" width="100%">
                                <thead><tr><th>Keterangan</th><th>Biaya</th></tr></thead>
                                <tbody><tr><td>Biaya Training per hari</td><td>Rp '.$obj->hargaTraining.'.-</td></tr><tr><td>Biaya transport per hari</td><td>Rp '.$obj->hargaTransport.'.-</td></tr></tbody>
                                </table>
                                </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tbody>

            </table>
                
            </td>
        </tr>
        <tr>
            <td>
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                <tr><td>
                <h3>PEMBAYARAN</h3>
                <ul type="square">
                    <li><strong>Bank BCA KCU Bekasi No Acc. 0663162851 a/n : Fajar Shodiq</strong></li>
                    <li><strong>Bank Mandiri KCP Bekasi Ahmad Yani No. Acc. 1670001377901 a/n : Fajar Shodiq</strong></li>
                </ul></td>
                </tr>
                </tbody>
                </table> 
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr style="background-color: #660000;">
                            <td style="color:white;margin:0 auto" align="center" valign="middle" height="40">SYARAT DAN KETENTUAN</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                '.$this->syaratDanKetentuan('').'
            </td>
        </tr>
        <tr>
            <td>
                <p>Silahkan untuk mengisi data yang ada di lampiran dan membalas email ini dengan data yang sudah terisi ini atau anda dapat <a href="http://www.fac-institute.com/daftar-training">mengisi secara online</a></p>
                <p>
                Jika ada hal-hal yang ingin ditanyakan, jangan sungkan untuk senantiasa menghubungi kami. Terimakasih, <br><br>Salam <br><br>
                </p>
                <p><strong>'.$obj->petugas.'</strong> <br> <strong>Marketing Division Staff of FAC Institute</strong></p>
            </td>
        </tr>
        <tr>
            <td style="padding:0 0 0 0" align="center" bgcolor="#0066ff">
          <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:#0066ff">
            <tbody>
              <tr>
                <td>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr height="30">
                        <td align="center" width="100%" height="16"></td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr>
                        <td align="center" valign="top">
                          <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                              <tr>
                                <td align="left" width="20"></td>
                                <td align="left" valign="top">
                                  <table align="center" border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                      <tr height="15">
                                        <td align="left" height="15">&nbsp;</td>
                                      </tr>
                                      <tr>
                                      <td style="line-height:18px" align="center" valign="top">
                                          <span style="font-family:'."'"."Open Sans"."'".',sans-serif;color:white;font-size:12px;text-align:center">Email ini dikirim oleh: <strong><a href="http://www.fac-institute.com" style="color:white">FAC-Institute</a></strong><br>
                                          Jl. Jatiwaringin No.8 Pangkalan Jati, Jakarta Timur<br>
                                          Telp. <a href="tel://+6281290083983" style="color:white">+6281290083983</a></span><br>
                                          <br>
                                            <a href="http://www.fac-institute.com" style="font-family:'."'"."Open Sans"."'".',sans-serif;font-size:22px;color:white">www.fac-institute.com</a><br><br>
                                            <p style="margin:0 0 1em 0;font-family:'."'"."Open Sans"."'".',sans-serif;font-size:16px;line-height:26px;color:white;font-weight:300;text-align:center">Konsultan Akuntansi dan Lembaga Pendidikan Komputerisasi Akuntansi</p>
                                        </td>
                                      </tr>
                                      <tr height="15">
                                        <td align="left" height="15">&nbsp;</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                                <td align="left" width="20"></td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr height="30">
                        <td align="left" width="100%" height="16"></td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
        </tr>
    </tbody>
</table>';
    }


    public function getAgenda($arr){
        include_once 'Pendaftar.php';
        $pendaftar = new Pendaftar();
        $hasil = "";
        $data = json_decode($pendaftar->fetchAllAgenda());

        for ($i=0; $i < count($data); $i++){ 
            for ($j=0; $j < count($arr); $j++){ 
                if ($arr[$j]==$data[$i]->id){ 
                    $hasil .= "<li>".$data[$i]->nama_agenda."</li>";
                }
            }
        }
        return $hasil;
    }

    public function getBulan($value){
        if ($value=='1') {
            return "Januari";
        }elseif ($value=='2') {
            return "Februari";
        }elseif ($value=='3') {
            return "Maret";
        }elseif ($value=='4') {
            return "April";
        }elseif ($value=='5') {
            return "Mei";
        }elseif ($value=='6') {
            return "Juni";
        }elseif ($value=='7') {
            return "Juli";
        }elseif ($value=='8') {
            return "Agustus";
        }elseif ($value=='9') {
            return "September";
        }elseif ($value=='10') {
            return "Oktober";
        }elseif ($value=='11') {
            return "November";
        }elseif ($value=='12') {
            return "Desember";
        }
    }


    public function Kursus($id){
        include_once 'Kursus.php';
        $kursus = new Kursus();
        $data = $kursus->getDataForEmail($id);
        $data = json_decode($data);
        $tgl = explode('-', $data[0]->mulai_kursus);
        return '<table style="margin:0;padding:0" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr style="background-color: #0066ff;">
                            <td style="color:white;margin:0 auto;font-size:22px" align="center" valign="middle" height="60"><strong>FAC-Institute</strong></td>
                        </tr>
                    </tbody>
                </table>            
            </td>
        </tr>
        <tr>
            <table  align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tbody>
            <tr>
            <td style="width:50%"><center><img src="http://fac-institute.com/images/logoaccurate5.png" style="width:50%;height:60px"></center></td>
            <td style="width:50%"><center><img src="http://fac-institute.com/images/logo-fac.jpg" style="width:50%;height:100px"></center></td>
            </tr>
            </tbody>
            </table>
        </tr>
        <tr>
            <td valign="middle" height="60"><h4>Kepada Yth,</h4></td>
        </tr>
        <tr>
            <td valign="middle" height="60"><h4>'.$data[0]->nama.'</h4></td>
        </tr>
        <tr>
            <td>
                <p>
                    Degan Hormat, <br>
                    Terimakasih, Anda telah mendaftar di kelas kursus <strong>FAC Institute & Bakat Cendekia</strong>.
                     Kami merupakan partner dari pihak Accurate Accounting Software, sebagai Accurate Authorized Training Center (AATC) yang secara resmi menyelenggarakan Pelatihan Accurate.<br> 
                </p>
                <br>
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr>
                            <td>
                            <label><strong>WAKTU & TEMPAT PELAKSANAAN</strong></label>
                            <table style="padding-top:10px">
                            <tbody>
                            <tr><td>Tanggal</td><td>:</td><td>'.$tgl[2].' '.$this->getBulan($tgl[1]).' '.$tgl[0].'</td></tr>
                            <tr><td>Waktu</td><td>:</td><td>'.$data[0]->jam.'</td></tr>
                            <tr><td>Tempat</td><td>:</td><td>'.$data[0]->lokasi_kursus.'</td></tr>
                            <tr><td>Nama Kursus</td><td>:</td><td>'.$data[0]->nama_kursus.'</td></tr>
                            <tr><td>Kode</td><td>:</td><td>#'.$data[0]->status.'</td></tr>
                            </tbody>
                            </table>
                            </td>
                        </tr>
                        
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td><br>
            <table valign="top" align="center" border="0" style="padding-bottom:30;" bgcolor="#f9ffee" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                    <td>
                        <table valign="top" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                                <tr><td><center><h3><strong>BIAYA</strong></h3></center></td></tr>
                                <tr>
                                <td style="margin-bottom:10px">
                                <table border="1" width="100%">
                                <thead><tr><th>Keterangan</th><th>Biaya</th></tr></thead>
                                <tbody><tr><td>'.$data[0]->nama_kursus.'</td><td>Rp '.number_format($data[0]->investasi).'.-</td></tr></tbody>
                                </table>
                                </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tbody>

            </table>
                
            </td>
        </tr>
        <tr><br></tr>
        <tr>
            <td>
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                <tr><td>
                <h3>PEMBAYARAN</h3>
                <ul type="square">
                    <li><strong>Bank BCA KCU Bekasi No Acc. 0663162851 a/n : Fajar Shodiq</strong></li>
                    <li><strong>Bank Mandiri KCP Bekasi Ahmad Yani No. Acc. 1670001377901 a/n : Fajar Shodiq</strong></li>
                </ul></td>
                </tr>
                </tbody>
                </table> 
            </td>
        </tr>
        
        <tr>
            <td>
                <p>
                Jika ada hal-hal yang ingin ditanyakan, jangan sungkan untuk senantiasa menghubungi kami. <br>Terimakasih, <br><br>Salam <br><br>
                </p>
                <p><strong>Rifzky Alam</strong> <br> <strong>Training Division Staff of FAC Institute</strong></p>
            </td>
        </tr>
        <tr>
            <td style="padding:0 0 0 0" align="center" bgcolor="#0066ff">
          <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:#0066ff">
            <tbody>
              <tr>
                <td>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr height="30">
                        <td align="center" width="100%" height="16"></td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr>
                        <td align="center" valign="top">
                          <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                              <tr>
                                <td align="left" width="20"></td>
                                <td align="left" valign="top">
                                  <table align="center" border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                      <tr height="15">
                                        <td align="left" height="15">&nbsp;</td>
                                      </tr>
                                      <tr>
                                      <td style="line-height:18px" align="center" valign="top">
                                          <span style="font-family:'."'"."Open Sans"."'".',sans-serif;color:white;font-size:12px;text-align:center">Email ini dikirim oleh: <strong><a href="http://www.fac-institute.com" style="color:white">FAC-Institute</a></strong><br>
                                          Jl. Jatiwaringin No.8 Pangkalan Jati, Jakarta Timur<br>
                                          Telp. <a href="tel://+6281290083983" style="color:white">+6281290083983</a></span><br>
                                          <br>
                                            <a href="http://www.fac-institute.com" style="font-family:'."'"."Open Sans"."'".',sans-serif;font-size:22px;color:white">www.fac-institute.com</a><br><br>
                                            <p style="margin:0 0 1em 0;font-family:'."'"."Open Sans"."'".',sans-serif;font-size:16px;line-height:26px;color:white;font-weight:300;text-align:center">Konsultan Akuntansi dan Lembaga Pendidikan Komputerisasi Akuntansi</p>
                                        </td>
                                      </tr>
                                      <tr height="15">
                                        <td align="left" height="15">&nbsp;</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                                <td align="left" width="20"></td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr height="30">
                        <td align="left" width="100%" height="16"></td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
        </tr>
    </tbody>
</table>';
    }

    public function getNamaBulan($bulan){
        $bulan=intval($bulan);
        if ($bulan==1) {
            return " Januari ";
        }elseif ($bulan==2) {
            return " Februari ";
        }elseif ($bulan==3) {
            return " Maret ";
        }elseif ($bulan==4) {
            return " April ";
        }elseif ($bulan==5) {
            return " Mei ";
        }elseif ($bulan==6) {
            return " Juni ";
        }elseif ($bulan==7) {
            return " Juli ";
        }elseif ($bulan==8) {
            return " Agustus ";
        }elseif ($bulan==9) {
            return " September ";
        }elseif ($bulan==10) {
            return " Oktober ";
        }elseif ($bulan==11) {
            return " November ";
        }elseif ($bulan==12) {
            return " Desember ";
        }else{
            return $bulan;
        }

    }

    function syaratBayar($value){

        if ($value=='term-1') {
            return'
            <p>Harga diatas adalah tarif nett yang diterima oleh kami (tidak dipotong pph)<br>Kami bukan PKP(Pengusaha Kena Pajak), tidak ada faktur pajak. Juga tidak ada NPWP untuk bukti potong PPh 23.<br>Pembayaran dimuka 50% yaitu sebelum implementasi dilaksanakan dan pembayaran di bayarkan ke No Rekening di bawah ini.<br><br>Pembayaran dimuka dilakukan sebelum training dilaksanakan dan pembayaran di bayarkan ke No Rekening di bawah ini.</p>
            ';
        }elseif($value=='term-2'){
            return'
            <p>Harga diatas adalah tarif nett yang diterima oleh kami (tidak dipotong pph)<br>Kami bukan PKP(Pengusaha Kena Pajak), tidak ada faktur pajak. Juga tidak ada NPWP untuk bukti potong PPh 23.<br>Pembayaran dimuka 50% yaitu sebelum implementasi dilaksanakan dan pembayaran di bayarkan ke No Rekening di bawah ini.<br><br>Pembayaran dimuka dilakukan sebelum training dilaksanakan dan pembayaran di bayarkan ke No Rekening di bawah ini.</p>
            ';
        }else {
            return'
            <p>Harga diatas adalah tarif nett yang diterima oleh kami (tidak dipotong pph)<br>Kami bukan PKP(Pengusaha Kena Pajak), tidak ada faktur pajak. Juga tidak ada NPWP untuk bukti potong PPh 23.<br><br>Pembayaran dimuka dilakukan sebelum training dilaksanakan dan pembayaran di bayarkan ke No Rekening di bawah ini.</p>
            ';
        }

        
    }

    function syaratDanKetentuan($value){

        return '
                <ol>
                    <li>Training dan implementasi bersifat time oriented, bukan result oriented.</li>
                    <li>Training dan implementasi berfokus pada kegiatan mentransfer <strong>How To UseACCURATE</strong>.</li>
                    <li>Training dan implementasi terdiri dari 1 implementator dengan waktu kerja maksimal 7 jam (termasuk 1 jam istirahat), yaitu jam 09.00 s/d 16.00 atau jam 10.00 s/d 17.00.</li>
                    <li>Jika lembur, mohon di print dan diisi form lembur terlebih dahulu yang telah kami lampirkan.</li>
                    <li>Menyediakan makan siang bagi implementator (Jabodetabek)</li>
                    <li>Implementasi diluar Jabodetabek, akomodasi perjalanan, makan dan penginapan ditanggung customer.</li>
                    <li>Kelebihan jam training akan dikenakan tarif overtime Rp 75.000.- per jam</li>
                    <li>Pembatalan atau perubahan jadwal implementasi oleh Customer harus disampaikan ke FAC paling lambat 2 hari sebelum waktu pelaksanaan implementasi, apabila dilakukan 1 (satu) hari sebelumnya atau di hari pelaksanaan maka dikenakan denda Rp.250.000/hari. Selanjutnya waktu pelaksanaan akan direschedule kembali.</li>
                    <li>Jika anda pengguna awal atau pengguna lama tapi ingin membuat database baru, pada email ini kami lampirkan format excel untuk saldo awal meliputi daftar akun, daftar pelanggan, daftar pemasok, daftar barang dan daftar fix asset. Harap disiapkan sebelum hari training.</li>
                    <li>Training di hari sabtu, minggu dan hari libur nasional dikenakan biaya Rp 250.000.-</li>
                </ol>
                ';
    }

    public function getDateOfTraining($value){
        $hasil = "";

        for ($i=0; $i < count($value) ; $i++) { 
            if ($value!='') {
                $tgl = explode('-', $value[$i]);
                $hasil .= "<li>".$tgl[2].$this->getNamaBulan($tgl[1]).$tgl[0]."</li>";                
            }

        }
        return $hasil;
    }

	public function getListOfAgenda($value){
		$hasil = "";
		for ($i=0; $i < count($value) ; $i++) { 
			$hasil .= "<li>".$value[$i]."</li>";
		}
		return $hasil;
	}

    function getList($value){
        $value = explode('##', $value);
        $hasil='';
        for ($i=0; $i < count($value); $i++) { 
            $hasil.='<li>'.$value[$i].'</li>';
        }
        return $hasil;
    }

    function myNotes($value){
        if ($value==''||empty($value)) {
            $hasil = '';
        } else {
            $hasil='
            <tr>
                <td>
                    <table>
                        <tbody>
                            <tr>
                                <td><h4>Notes:</h4></td>
                            </tr>
                            <tr>
                                <td>
                                    <ul>
                                        '.$this->getList($value).'
                                    </ul>
                                </td>
                            </tr>
                        </tbody>
                    </table>    
                </td>
            </tr>
            ';
        }
        
        return $hasil;
    }

}


?>