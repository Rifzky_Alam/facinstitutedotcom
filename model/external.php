<?php 
include_once 'Koneksi.php';
class External extends Koneksi{
	
	function __construct(){
		$this->dbHost = $this->bukaKoneksi();
	}

	//petugas

	public function getToken($value){
		return md5($value);
	}

	public function ubahPassword(){
		$query = "UPDATE `external_users` SET `password`=:password WHERE `username`=:user;";
		$statement=$this->dbHost->prepare($query);

		$statement->bindParam(':user',$this->username,PDO::PARAM_STR,40);
		@$statement->bindParam(':password',md5($this->password),PDO::PARAM_STR,35);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}

	}

	public function inputPetugas(){
		$query = "INSERT INTO `external_users` (`username`, `password`, `email`, `nama_lengkap`, `telepon`, `cabang`) 
		VALUES (:username, :password, :email, :namalengkap, :telepon, :cabang);";
		$statement=$this->dbHost->prepare($query);

		$statement->bindParam(':username',$this->username,PDO::PARAM_STR,40);
		@$statement->bindParam(':password',md5($this->password),PDO::PARAM_STR,35);
		$statement->bindParam(':email',$this->email,PDO::PARAM_STR,125);
		$statement->bindParam(':namalengkap',$this->nama,PDO::PARAM_STR,75);
		$statement->bindParam(':telepon',$this->telepon,PDO::PARAM_STR,15);
		$statement->bindParam(':cabang',$this->perusahaan,PDO::PARAM_INT,3);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
	}

	public function getPetugasById(){
		
	}

	public function fetchPetugas(){
		$query = "SELECT * FROM `external_users`;";
		$statement=$this->dbHost->prepare($query);

		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function login(){
		$query = "SELECT * FROM `external_users` WHERE `username` = :username AND `password` = :password;";
		$statement=$this->dbHost->prepare($query);
		$password = md5($this->getPassword());
		@$statement->bindParam(':username',$this->getUsername(),PDO::PARAM_STR,40);
		@$statement->bindParam(':password',$password,PDO::PARAM_STR,35);

		$statement->execute();
		$hasil = $statement->fetch(PDO::FETCH_OBJ);

		if (@$hasil->username!='') {
			$this->setUsername($hasil->username);
			$this->setNama($hasil->nama_lengkap);
			$this->setEmail($hasil->email);
			$this->setTelepon($hasil->telepon);
			$this->setPassword($hasil->password);
			$this->setCabang($hasil->cabang);
			return true;
		} else {
			return false;
		}
		
	}
	//end petugas

	//perusahaan
	public function inputPerusahaan(){
		$aidi = md5($this->perusahaan->nama_usaha . '-' . $this->perusahaan->username);
		$query = "INSERT INTO `external_perusahaan` (`id`, `nama_usaha`, `email_usaha`, `alamat_usaha`, `kota_usaha`, `provinsi_usaha`, `telepon_usaha`, `ket_jenis_usaha`, `map_usaha`, `user_source`) 
		VALUES (:id, :perusahaan, :email, :alamat, :kota, :provinsi, :telepon, :keteranganusaha, :map, :username);";
		$statement=$this->dbHost->prepare($query);
		
		$statement->bindParam(':id',$aidi,PDO::PARAM_STR,35);
		$statement->bindParam(':perusahaan',$this->perusahaan->nama_usaha,PDO::PARAM_STR,125);
		$statement->bindParam(':email',$this->perusahaan->email,PDO::PARAM_STR,125);
		$statement->bindParam(':alamat',$this->perusahaan->alamat,PDO::PARAM_STR,500);
		$statement->bindParam(':kota',$this->perusahaan->kota,PDO::PARAM_STR,75);
		$statement->bindParam(':provinsi',$this->perusahaan->provinsi,PDO::PARAM_STR,50);
		$statement->bindParam(':telepon',$this->perusahaan->telepon,PDO::PARAM_STR,15);
		$statement->bindParam(':keteranganusaha',$this->perusahaan->ketjenisusaha,PDO::PARAM_STR,255);
		$statement->bindParam(':map',$this->perusahaan->map,PDO::PARAM_STR,300);
		$statement->bindParam(':username',$this->perusahaan->username,PDO::PARAM_STR,75);

		if ($statement->execute()){
			$this->inputJenisUsaha($aidi,$this->perusahaan->jenisusaha);
			return true;
		}else{
			return false;
		}
	}

	public function jajalObjek(){
		return $this->perusahaan->nama_usaha;
	}

	public function inputJenisUsaha($id,$arr){
		$query = "INSERT INTO `external_jenis_usaha` (`source_perusahaan`, `jenis_usaha`) 
		VALUES ";
		for ($i=0; $i < count($arr) ; $i++) { 
			if ($i==count($arr)-1) {
				$query .= "('".$id."', '".$arr[$i]."')";
			}else {
				$query .= "('".$id."', '".$arr[$i]."'),";	
			}
		}
		$statement=$this->dbHost->prepare($query);


		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
		
	}

	
	public function fetchDataPerusahaanByID(){
		$query = "SELECT `id`, `nama_usaha`, `email_usaha`, `alamat_usaha`, `kota_usaha`, `provinsi_usaha`, `telepon_usaha`,GROUP_CONCAT(external_jenis_usaha.jenis_usaha SEPARATOR '#') AS jenisusaha, `ket_jenis_usaha`, `user_source`,`map_usaha` FROM `external_perusahaan`, `external_jenis_usaha` WHERE `id`=:id AND `external_perusahaan`.`id`= `external_jenis_usaha`.`source_perusahaan`";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':id',$this->perusahaan,PDO::PARAM_STR,35);

		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function fetchAllDataPerusahan($offset,$limit){
		if (@$this->perusahaan->user!=''){
			$query = "SELECT `id`, `nama_usaha`, `email_usaha`, `alamat_usaha`, `kota_usaha`, `provinsi_usaha`, `telepon_usaha`,(SELECT GROUP_CONCAT(`nama_jenis_usaha`) FROM `daftar_jenis_usaha` WHERE `id` IN (SELECT `jenis_usaha` FROM `external_jenis_usaha` WHERE `source_perusahaan` = `external_perusahaan`.`id`)) AS `jns_usaha`, `ket_jenis_usaha`, `map_usaha` FROM `external_perusahaan` WHERE `user_source`=:user;";	
		}else{
			$query = "SELECT `id`, `nama_usaha`, `email_usaha`, `alamat_usaha`, `kota_usaha`, `provinsi_usaha`, `telepon_usaha`,(SELECT GROUP_CONCAT(`nama_jenis_usaha`) FROM `daftar_jenis_usaha` WHERE `id` IN (SELECT `jenis_usaha` FROM `external_jenis_usaha` WHERE `source_perusahaan` = `external_perusahaan`.`id`)) AS `jns_usaha`, `ket_jenis_usaha`, `map_usaha` FROM `external_perusahaan`;";
		}
		

		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':user',$this->perusahaan->user,PDO::PARAM_STR,75);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);

	}

	public function fetchPerusahaan($offset,$limit){
		$offset= $limit * $offset;
		if (@$this->perusahaan->user!='') {//untuk anak-anak external
			$query = "SELECT `id`, `nama_usaha`, `email_usaha`, `alamat_usaha`, `kota_usaha`, `provinsi_usaha`, `telepon_usaha`, `ket_jenis_usaha`, `map_usaha`, `nama_lengkap`, `status_data`,`user_source` FROM `external_perusahaan`,`external_users` WHERE `user_source`=:user AND `external_perusahaan`.`user_source`=`external_users`.`username` LIMIT $offset,$limit";
			$statement = $this->dbHost->prepare($query);
			$statement->bindParam(':user',$this->perusahaan->user,PDO::PARAM_STR,75);	
		} else { //untuk admin fac
			$query = "SELECT `id`, `nama_usaha`, `email_usaha`, `alamat_usaha`, `kota_usaha`, `provinsi_usaha`, `telepon_usaha`, `ket_jenis_usaha`, `map_usaha`, `nama_lengkap`, `status_data`,`dibuat` FROM `external_perusahaan`,`external_users` WHERE `external_perusahaan`.`user_source`=`external_users`.`username` LIMIT $offset,$limit";
			$statement = $this->dbHost->prepare($query);
		}
		
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function fetchRemovedPerusahaan(){
		$offset= $limit * $offset;
		
			$query = "SELECT * FROM `external_perusahaan` WHERE `user_source`=:user AND `status_data`='0' LIMIT $offset,$limit";
			$statement = $this->dbHost->prepare($query);
			$statement->bindParam(':user',$this->perusahaan->user,PDO::PARAM_STR,75);	
		
		
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function fetchPerusahaanByUsername(){
		// $offset= $limit * $offset;
		$query = "SELECT * FROM `external_perusahaan` WHERE user_source = :username ";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':username',$this->username,PDO::PARAM_STR,75);

		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);

	}

	public function fetchJenisUsaha(){
		$query = "SELECT * FROM `daftar_jenis_usaha`";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);	
	}

	public function editJenisUsaha($id,$arr){
		$query = "DELETE FROM `external_jenis_usaha` WHERE `source_perusahaan`='".$id."';";
		// $query2 = "";
		for ($i=0; $i < count($arr); $i++) { 
			$query .= "INSERT INTO `external_jenis_usaha` (`source_perusahaan`, `jenis_usaha`) VALUES('".$id."','".$arr[$i]."');";
		}
		// $query.=";";
		$statement=$this->dbHost->prepare($query);


		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
		
	}

	public function editPerusahaan(){
		$query = "UPDATE `external_perusahaan` SET `nama_usaha`=:namausaha,`email_usaha`=:email,`alamat_usaha`=:alamat,`kota_usaha`=:kota,`provinsi_usaha`=:provinsi,`telepon_usaha`=:telepon,`ket_jenis_usaha`=:keteranganusaha,`map_usaha`=:map WHERE `id`=:id;";
		$statement=$this->dbHost->prepare($query);

				
		$statement->bindParam(':id',$this->perusahaan->id,PDO::PARAM_STR,35);
		$statement->bindParam(':namausaha',$this->perusahaan->nama_usaha,PDO::PARAM_STR,125);
		$statement->bindParam(':email',$this->perusahaan->email,PDO::PARAM_STR,125);
		$statement->bindParam(':alamat',$this->perusahaan->alamat,PDO::PARAM_STR,500);
		$statement->bindParam(':kota',$this->perusahaan->kota,PDO::PARAM_STR,75);
		$statement->bindParam(':provinsi',$this->perusahaan->provinsi,PDO::PARAM_STR,50);
		$statement->bindParam(':telepon',$this->perusahaan->telepon,PDO::PARAM_STR,15);
		$statement->bindParam(':keteranganusaha',$this->perusahaan->ketjenisusaha,PDO::PARAM_STR,255);
		$statement->bindParam(':map',$this->perusahaan->map,PDO::PARAM_STR,300);
		// $statement->bindParam(':username',$this->perusahaan->username,PDO::PARAM_STR,75);

		if ($statement->execute()){
			$this->editJenisUsaha($this->perusahaan->id,$this->perusahaan->jenisusaha);
			return true;
		}else{
			return false;
		}

	}

	public function editStatusPerusahaan(){
		$query = "UPDATE `external_perusahaan` SET `status_data`=:status WHERE `id`=:id";

		$statement=$this->dbHost->prepare($query);

		$statement->bindParam(':status',$this->perusahaan->status,PDO::PARAM_STR,1);
		$statement->bindParam(':id',$this->perusahaan->id,PDO::PARAM_STR,35);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}

	}

	//end perusahaan

	//customer

	public function countCustomer(){
		$query = "SELECT COUNT(`id`) AS `jumlah` FROM `eksternal_customer` WHERE `user_source`=:user";
		$statement = $this->dbHost->prepare($query);

		$statement->bindParam(':user',$this->username,PDO::PARAM_STR,75);
		$statement->execute();
		$hasil = $statement->fetch(PDO::FETCH_OBJ);
		return $hasil->jumlah;
	}

	public function inputCustomer(){
		$aidi=md5($this->customer->nama.'-'.$this->customer->user);
		$query = "INSERT INTO `eksternal_customer` (`id`, `nama`, `email`, `telepon`, `id_usaha`, `jabatan`, `paket_training`, `biaya_transport`, `agenda_training`, `jenis_pengguna`,`user_source`,`jumlah_hari`) 
		VALUES (:id, :nama, :email, :telepon, :idusaha, :jabatan, :paket, :transport, :agenda, :jenispengguna,:user,:qtypaket);";
		$statement=$this->dbHost->prepare($query);
		
		$statement->bindParam(':id',$aidi,PDO::PARAM_STR,35);
		$statement->bindParam(':nama',$this->customer->nama,PDO::PARAM_STR,75);
		$statement->bindParam(':email',$this->customer->email,PDO::PARAM_STR,125);
		$statement->bindParam(':telepon',$this->customer->telepon,PDO::PARAM_STR,15);
		$statement->bindParam(':idusaha',$this->customer->usaha,PDO::PARAM_STR,35);
		$statement->bindParam(':jabatan',$this->customer->jabatan,PDO::PARAM_STR,75);
		$statement->bindParam(':paket',$this->customer->paket,PDO::PARAM_STR,35);
		$statement->bindParam(':transport',$this->customer->transport,PDO::PARAM_INT,9);
		$statement->bindParam(':agenda',$this->customer->agenda,PDO::PARAM_STR,500);
		$statement->bindParam(':jenispengguna',$this->customer->jenispengguna,PDO::PARAM_STR,1);
		$statement->bindParam(':user',$this->customer->user,PDO::PARAM_STR,75);
		$statement->bindParam(':qtypaket',$this->customer->qtypaket,PDO::PARAM_INT,3);
		
		if ($statement->execute()){
			$this->inputTanggalTraining($aidi,$this->customer->tanggaltraining);
			return true;
		}else{
			return false;
		}
	}

	public function fetchDataCustomerByID(){
		$query = "SELECT  `eksternal_customer`.`id` AS `id`, `nama`,`email`,`telepon`,`id_usaha`,`jabatan`,`paket_training`,`biaya_transport`,`agenda_training`,`jenis_pengguna`,GROUP_CONCAT(`external_calendar`.`tanggal` SEPARATOR '#') AS tanggals, `eksternal_customer`.`user_source` AS `user_source`,`nama_usaha`,`jumlah_hari` 
		FROM `eksternal_customer`,`external_perusahaan`,`external_calendar` 
		WHERE `eksternal_customer`.`id_usaha`=`external_perusahaan`.`id` AND `eksternal_customer`.`id`=`external_calendar`.`source` AND `eksternal_customer`.`id`=:id;";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':id',$this->customer,PDO::PARAM_STR,35);

		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function inputTanggalTraining($id,$arr){
		$query = "INSERT INTO `external_calendar` (`source`, `tanggal`) VALUES ";
		for ($i=0; $i < count($arr) ; $i++) { 
			if ($i==count($arr)-1) {
				$query .= "('".$id."', '".$arr[$i]."')";
			}else {
				$query .= "('".$id."', '".$arr[$i]."'),";	
			}
		}
		$statement=$this->dbHost->prepare($query);
		if ($statement->execute()){
			return true;
		}else{
			return false;
		}	
	}

	public function fetchCustomer($offset,$limit){
		$offset= $limit * $offset;
		if (@$this->customer->user!='') {
			$query = "SELECT `eksternal_customer`.`id` AS `id`, `nama`, `email`, `telepon`, `nama_usaha`, `jabatan`, `nama_item`, `biaya_transport`, `agenda_training`, `jenis_pengguna`, `external_perusahaan`.`user_source` AS `user_source` FROM `eksternal_customer`,`paket_training`,`external_perusahaan` WHERE `eksternal_customer`.`paket_training`=`paket_training`.`id` AND `external_perusahaan`.`id`=`eksternal_customer`.`id_usaha` AND `eksternal_customer`.`user_source`=:user LIMIT $offset,$limit;";
			$statement = $this->dbHost->prepare($query);	
			$statement->bindParam(':user',$this->customer->user,PDO::PARAM_STR,35);

		}else{//untuk admin fac
			$query = "SELECT `eksternal_customer`.`id` AS `id`, `nama`, `nama_lengkap`, `external_users`.`email` AS `email`, `external_users`.`telepon` AS `telepon`, `nama_usaha`, `jabatan`, `nama_item`, `biaya_transport`, `agenda_training`, `jenis_pengguna`, `external_perusahaan`.`user_source` AS `user_source`,`time_created` FROM `external_users`,`eksternal_customer`,`paket_training`,`external_perusahaan` WHERE `external_users`.`username`=`eksternal_customer`.`user_source` AND `eksternal_customer`.`paket_training`=`paket_training`.`id` AND `external_perusahaan`.`id`=`eksternal_customer`.`id_usaha` LIMIT $offset,$limit;";
			$statement = $this->dbHost->prepare($query);				
		}
		
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);

	}

	public function fetchCustomerByID(){
		$query = "SELECT  `eksternal_customer`.`id` AS id, `nama`, `eksternal_customer`.`user_source` AS `user_source`,`nama_usaha` 
		FROM `eksternal_customer`,`external_perusahaan` 
		WHERE `eksternal_customer`.`id_usaha`=`external_perusahaan`.`id` AND `eksternal_customer`.`id`=:id";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':id',$this->customer,PDO::PARAM_STR,35);

		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function editTanggalTraining($id,$arr){
		$query = "DELETE FROM `external_calendar` WHERE `source`='".$id."';";
		for ($i=0; $i < count($arr) ; $i++) { 
			if ($arr[$i]!='') {
				$query .= "INSERT INTO `external_calendar` (`source`, `tanggal`) VALUES ('".$id."','".$arr[$i]."');";	
			}				
		}
		$statement=$this->dbHost->prepare($query);
		if ($statement->execute()){
			return true;
		}else{
			return false;
		}	
	}

	public function editCustomer(){
		$query = "UPDATE `eksternal_customer` SET `nama`=:nama,`email`=:email,`telepon`=:telepon,`id_usaha`=:idusaha,`jabatan`=:jabatan,`paket_training`=:paket,`biaya_transport`=:transport,`agenda_training`=:agenda,`jenis_pengguna`=:jenispengguna, `jumlah_hari`=:qtypaket 
		WHERE `id`=:id;";
		$statement=$this->dbHost->prepare($query);
		
		$statement->bindParam(':id',$this->customer->id,PDO::PARAM_STR,35);
		$statement->bindParam(':nama',$this->customer->nama,PDO::PARAM_STR,75);
		$statement->bindParam(':email',$this->customer->email,PDO::PARAM_STR,125);
		$statement->bindParam(':telepon',$this->customer->telepon,PDO::PARAM_STR,15);
		$statement->bindParam(':idusaha',$this->customer->usaha,PDO::PARAM_STR,35);
		$statement->bindParam(':jabatan',$this->customer->jabatan,PDO::PARAM_STR,75);
		$statement->bindParam(':paket',$this->customer->paket,PDO::PARAM_STR,35);
		$statement->bindParam(':transport',$this->customer->transport,PDO::PARAM_INT,9);
		$statement->bindParam(':agenda',$this->customer->agenda,PDO::PARAM_STR,500);
		$statement->bindParam(':jenispengguna',$this->customer->jenispengguna,PDO::PARAM_STR,1);
		$statement->bindParam(':qtypaket',$this->customer->qtypaket,PDO::PARAM_INT,3);
		// $statement->bindParam(':user',$this->customer->user,PDO::PARAM_STR,75);
		
		if ($statement->execute()){
			$this->editTanggalTraining($this->customer->id,$this->customer->tanggaltraining);
			return true;
		}else{
			return false;
		}
	}

	public function fetchPaketTraining(){
		$query="SELECT * FROM `paket_training` WHERE `status_paket`='3'";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	//end customer

	//quotation

	public function countQuotation(){
		$query = "SELECT COUNT(`id`) AS `jumlah` FROM `external_quotation` WHERE `user_from`=:user";
		$statement = $this->dbHost->prepare($query);
		
		$statement->bindParam(':user',$this->username,PDO::PARAM_STR,75);
		
		$statement->execute();
		$hasil = $statement->fetch(PDO::FETCH_OBJ);
		return $hasil->jumlah;
	}

	public function inputQuotation(){
		$aidi = md5($this->quotation->user.'-'.$this->quotation->subjek.'-'.$this->quotation->idcustomer);
		$query = "INSERT INTO `external_quotation` (`id`, `id_customer`, `subjek`, `title`, `notes`, `cc`, `attachments`, `status`, `user_from`, `time_sent`) 
		VALUES (:id, :idcustomer, :subjek, :title, :notes, :cece, :attachments, :status, :user, CURRENT_TIMESTAMP);";
		$statement = $this->dbHost->prepare($query);

		$statement->bindParam(':id',$aidi,PDO::PARAM_STR,35);
		$statement->bindParam(':idcustomer',$this->quotation->idcustomer,PDO::PARAM_STR,35);
		$statement->bindParam(':subjek',$this->quotation->subjek,PDO::PARAM_STR,75);
		$statement->bindParam(':title',$this->quotation->title,PDO::PARAM_STR,75);
		$statement->bindParam(':notes',$this->quotation->notes,PDO::PARAM_STR,255);
		$statement->bindParam(':cece',$this->quotation->cece,PDO::PARAM_STR,300);
		$statement->bindParam(':attachments',$this->quotation->attach,PDO::PARAM_STR,500);
		$statement->bindParam(':status',$this->quotation->status,PDO::PARAM_INT,2);
		$statement->bindParam(':user',$this->quotation->user,PDO::PARAM_STR,75);

		if ($statement->execute()){
			// $this->inputTanggalTraining($aidi,$this->customer->tanggaltraining);
			return true;
		}else{
			return false;
		}

	}

	public function fetchQuotation($offset,$limit){
		$offset= $limit * $offset;
		if (@$this->quotation->user!='') {
			$query = "SELECT external_quotation.id AS `id`, `nama`,`nama_usaha`, `email`, `telepon`, `ket_jenis_usaha`, `jabatan`, `agenda_training`, `nama_item`,`external_quotation`.`status` AS `status` FROM `eksternal_customer`,`external_perusahaan`,`external_quotation`,`paket_training` WHERE  eksternal_customer.id=external_quotation.id_customer AND eksternal_customer.id_usaha = external_perusahaan.id AND `paket_training`.`id` = `eksternal_customer`.`paket_training` AND `user_from`=:user LIMIT 0, 30";
			$statement = $this->dbHost->prepare($query);
			$statement->bindParam(':user',$this->quotation->user,PDO::PARAM_STR,75);
		}else{ //untuk admin fac
			$query = "SELECT `external_quotation`.`id` AS `id`, `nama`,`nama_usaha`, `eksternal_customer`.`email` AS `email`, `eksternal_customer`.`telepon` AS `telepon`, `ket_jenis_usaha`, `jabatan`, `agenda_training`, `nama_item`,`nama_lengkap`,`time_sent` FROM `external_users`,`eksternal_customer`,`external_perusahaan`,`external_quotation`,`paket_training` WHERE  `external_users`.`username`=`external_quotation`.`user_from` AND `eksternal_customer`.`id`=`external_quotation`.`id_customer` AND `eksternal_customer`.`id_usaha` = `external_perusahaan`.`id` AND `paket_training`.`id` = `eksternal_customer`.`paket_training` LIMIT $offset, $limit";
			$statement = $this->dbHost->prepare($query);
			$statement->bindParam(':user',$this->username,PDO::PARAM_STR,75);
		}
		
		
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);

	}

	public function editQuotation(){
		$query = "UPDATE `external_quotation` SET `subjek`=:subjek,`title`=:title,`notes`=:notes,`cc`=:cece,`attachments`=:attach WHERE `id`=:id;";
		$statement = $this->dbHost->prepare($query);

		$statement->bindParam(':id',$this->quotation->id,PDO::PARAM_STR,35);
		$statement->bindParam(':subjek',$this->quotation->subjek,PDO::PARAM_STR,75);
		$statement->bindParam(':title',$this->quotation->title,PDO::PARAM_STR,75);
		$statement->bindParam(':notes',$this->quotation->notes,PDO::PARAM_STR,255);
		$statement->bindParam(':cece',$this->quotation->cece,PDO::PARAM_STR,300);
		$statement->bindParam(':attach',$this->quotation->attach,PDO::PARAM_STR,500);

		if ($statement->execute()){
			// $this->inputTanggalTraining($aidi,$this->customer->tanggaltraining);
			return true;
		}else{
			return false;
		}
	}

	public function fetchQuotationForEdit(){
		$query = "SELECT `external_quotation`.`id` AS `id`, `nama`,`nama_usaha`,`id_customer`, `subjek`, `title`, `notes`, `cc`, `attachments`, `status`, `user_from`, `time_sent` FROM `external_quotation`,`eksternal_customer`,`external_perusahaan` WHERE `external_quotation`.`id_customer`=`eksternal_customer`.`id` AND `eksternal_customer`.`id_usaha`=`external_perusahaan`.`id` AND `external_quotation`.`id`=:id";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':id',$this->quotation,PDO::PARAM_STR,35);

		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function editStatusQuotation(){
		$query = "UPDATE `external_quotation` SET `status` = :status WHERE `external_quotation`.`id` = :id;";
		$statement = $this->dbHost->prepare($query);

		$statement->bindParam(':status',$this->quotation->status,PDO::PARAM_STR,1);
		$statement->bindParam(':id',$this->quotation->id,PDO::PARAM_STR,35);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}

	}

	public function getQuotationForDisplay(){
		/*$query = "SELECT `external_quotation`.`id` AS `id`, `eksternal_customer`.`nama` AS `nama`, `nama_usaha`, `eksternal_customer`.`email` AS `email`, `email_usaha`,`nama_item`, `harga`,`biaya_transport`,`subjek`, `title`, `notes`, `cc`, `attachments`, `status`, `external_users`.`nama_lengkap` AS `petugas`, `time_sent` 
			FROM `external_quotation`, `eksternal_customer`,`external_perusahaan`,`paket_training`,`external_users` 
			WHERE `external_quotation`.`id_customer`=`eksternal_customer`.`id` AND `eksternal_customer`.`id_usaha`=`external_perusahaan`.`id` AND `eksternal_customer`.`paket_training`=`paket_training`.`id` AND `external_quotation`.`id`=:id;";*/
			$query = "SELECT `external_quotation`.`id` AS `id`, `eksternal_customer`.`nama` AS `nama`, `nama_usaha`, `eksternal_customer`.`email` AS `email`, `email_usaha`,`nama_item`, `harga`,`biaya_transport`,`subjek`, `title`, `notes`, `cc`, `attachments`, `status`, `external_users`.`nama_lengkap` AS `petugas`, `time_sent`,`jumlah_hari`,`transport_qty` 
			FROM `external_quotation`, `eksternal_customer`,`external_perusahaan`,`paket_training`,`external_users` 
			WHERE `external_quotation`.`id_customer`=`eksternal_customer`.`id` AND `eksternal_customer`.`id_usaha`=`external_perusahaan`.`id` AND `eksternal_customer`.`paket_training`=`paket_training`.`id` AND `external_users`.`username`=`external_quotation`.`user_from` AND `external_quotation`.`id`=:id";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':id',$this->quotation,PDO::PARAM_STR,35);

		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}
	//end quotation

	// admin external
	public function Dasbor($cabang){
		$query = "SELECT `new_marketing`.`marketing_id` AS `aidi`,`new_marketing`.`marketing_nama` AS `nama`, 
		(SELECT COUNT(`new_invoice`.`inv_no_invoice`) FROM `new_marketing` INNER JOIN `new_customer` ON `new_customer`.`marketing_id`=`new_marketing`.`marketing_id`
		INNER JOIN `new_transaksi` ON `new_transaksi`.`trans_cust_id` = `new_customer`.`id_cust`
		INNER JOIN `new_invoice` ON `new_invoice`.`inv_id_trans` = `new_transaksi`.`trans_id` 
		WHERE `new_marketing`.`marketing_id`=`aidi`
		AND `new_invoice`.`inv_status`='2') AS `jumlah`,
		(SELECT 
		SUM(harga*itm_qty - ROUND(harga*itm_qty*trans_ppn) - ROUND(harga*itm_qty*trans_discount) - `trans_dp`) AS `jumlah` 
		FROM `new_invoice` 
        INNER JOIN `new_transaksi` 
        ON `new_transaksi`.`trans_id` = `new_invoice`.`inv_id_trans`
        INNER JOIN `new_trans_items` 
        ON `new_trans_items`.`itm_trans_id` = `new_transaksi`.`trans_id`
        INNER JOIN `paket_training` 
        ON `new_trans_items`.`itm_paket` = `paket_training`.`id`
        INNER JOIN `new_customer` ON `new_customer`.`id_cust` = `new_transaksi`.`trans_cust_id`
        INNER JOIN `new_marketing` 
        ON `new_marketing`.`marketing_id`=`new_customer`.`marketing_id`
		WHERE `new_invoice`.`inv_status` = '2'
        AND `new_marketing`.`marketing_id`=`aidi`) AS `invoice_terbayar`,
        (SELECT 
		SUM(harga*itm_qty - ROUND(harga*itm_qty*trans_ppn) - ROUND(harga*itm_qty*trans_discount) - `trans_dp`) AS `jumlah` 
		FROM `new_invoice` 
        INNER JOIN `new_transaksi` 
        ON `new_transaksi`.`trans_id` = `new_invoice`.`inv_id_trans`
        INNER JOIN `new_trans_items` 
        ON `new_trans_items`.`itm_trans_id` = `new_transaksi`.`trans_id`
        INNER JOIN `paket_training` 
        ON `new_trans_items`.`itm_paket` = `paket_training`.`id`
        INNER JOIN `new_customer` ON `new_customer`.`id_cust` = `new_transaksi`.`trans_cust_id`
        INNER JOIN `new_marketing` 
        ON `new_marketing`.`marketing_id`=`new_customer`.`marketing_id`
		WHERE `new_invoice`.`inv_status` > '0'
        AND `new_marketing`.`marketing_id`=`aidi`) AS `omset`,
        (
    		SELECT COUNT(`new_transaksi`.`trans_id`) FROM `new_invoice`
    		INNER JOIN `new_transaksi` ON `new_transaksi`.`trans_id` = `new_invoice`.`inv_id_trans`
    		INNER JOIN `new_customer` ON `new_customer`.`id_cust` = `new_transaksi`.`trans_cust_id`
    		INNER JOIN `new_marketing` ON `new_marketing`.`marketing_id`=`new_customer`.`marketing_id`
    		WHERE `new_marketing`.`marketing_id`=`aidi`
		) AS `jumlah_transaksi`,
		(
    		SELECT COUNT(`fac_calendar`.`acara`) FROM `new_invoice`
    		INNER JOIN `new_transaksi` ON `new_transaksi`.`trans_id` = `new_invoice`.`inv_id_trans`
    		INNER JOIN `new_customer` ON `new_customer`.`id_cust` = `new_transaksi`.`trans_cust_id`
    		INNER JOIN `new_marketing` ON `new_marketing`.`marketing_id`=`new_customer`.`marketing_id`
    		INNER JOIN `fac_calendar` ON `new_transaksi`.`trans_id`=`fac_calendar`.`acara`
    		WHERE `new_marketing`.`marketing_id`=`aidi`
		) AS `jumlah_hari_training`
		FROM `new_marketing`
		WHERE `new_marketing`.`marketing_cabang`=:cabang;";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':cabang',$cabang,PDO::PARAM_STR,35);
		$statement->execute();

		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function DasborAsOfDate($cabang,$from,$to){
		$query = "SELECT `new_marketing`.`marketing_id` AS `aidi`,`new_marketing`.`marketing_nama` AS `nama`, 
		(
            SELECT COUNT(`new_invoice`.`inv_no_invoice`) FROM `new_marketing` INNER JOIN `new_customer` ON `new_customer`.`marketing_id`=`new_marketing`.`marketing_id`
		INNER JOIN `new_transaksi` ON `new_transaksi`.`trans_cust_id` = `new_customer`.`id_cust`
		INNER JOIN `new_invoice` ON `new_invoice`.`inv_id_trans` = `new_transaksi`.`trans_id` 
		WHERE `new_marketing`.`marketing_id`=`aidi`
		AND `new_invoice`.`inv_status`='2' 
        AND `new_invoice`.`inv_date` BETWEEN '$from' AND '$to'
        ) AS `jumlah`,
		(
            SELECT 
		SUM(harga*itm_qty - ROUND(harga*itm_qty*trans_ppn) - ROUND(harga*itm_qty*trans_discount) - `trans_dp`) AS `jumlah` 
		FROM `new_invoice` 
        INNER JOIN `new_transaksi` 
        ON `new_transaksi`.`trans_id` = `new_invoice`.`inv_id_trans`
        INNER JOIN `new_trans_items` 
        ON `new_trans_items`.`itm_trans_id` = `new_transaksi`.`trans_id`
        INNER JOIN `paket_training` 
        ON `new_trans_items`.`itm_paket` = `paket_training`.`id`
        INNER JOIN `new_customer` ON `new_customer`.`id_cust` = `new_transaksi`.`trans_cust_id`
        INNER JOIN `new_marketing` 
        ON `new_marketing`.`marketing_id`=`new_customer`.`marketing_id`
		WHERE `new_invoice`.`inv_status` = '2'
        AND `new_invoice`.`inv_date` BETWEEN '$from' AND '$to'
        AND `new_marketing`.`marketing_id`=`aidi`
        ) AS `invoice_terbayar`,
        (SELECT 
		SUM(harga*itm_qty - ROUND(harga*itm_qty*trans_ppn) - ROUND(harga*itm_qty*trans_discount) - `trans_dp`) AS `jumlah` 
		FROM `new_invoice` 
        INNER JOIN `new_transaksi` 
        ON `new_transaksi`.`trans_id` = `new_invoice`.`inv_id_trans`
        INNER JOIN `new_trans_items` 
        ON `new_trans_items`.`itm_trans_id` = `new_transaksi`.`trans_id`
        INNER JOIN `paket_training` 
        ON `new_trans_items`.`itm_paket` = `paket_training`.`id`
        INNER JOIN `new_customer` ON `new_customer`.`id_cust` = `new_transaksi`.`trans_cust_id`
        INNER JOIN `new_marketing` 
        ON `new_marketing`.`marketing_id`=`new_customer`.`marketing_id`
		WHERE `new_invoice`.`inv_status` > '0'
        AND `new_invoice`.`inv_date` BETWEEN '$from' AND '$to'
        AND `new_marketing`.`marketing_id`=`aidi`) AS `omset`,
        (
    		SELECT COUNT(`new_transaksi`.`trans_id`) FROM `new_invoice`
    		INNER JOIN `new_transaksi` ON `new_transaksi`.`trans_id` = `new_invoice`.`inv_id_trans`
    		INNER JOIN `new_customer` ON `new_customer`.`id_cust` = `new_transaksi`.`trans_cust_id`
    		INNER JOIN `new_marketing` ON `new_marketing`.`marketing_id`=`new_customer`.`marketing_id`
    		WHERE `new_marketing`.`marketing_id`=`aidi`
            AND `new_invoice`.`inv_date` BETWEEN '$from' AND '$to'
		) AS `jumlah_transaksi`,
		(
    		SELECT COUNT(`fac_calendar`.`acara`) FROM `new_invoice`
    		INNER JOIN `new_transaksi` ON `new_transaksi`.`trans_id` = `new_invoice`.`inv_id_trans`
    		INNER JOIN `new_customer` ON `new_customer`.`id_cust` = `new_transaksi`.`trans_cust_id`
    		INNER JOIN `new_marketing` ON `new_marketing`.`marketing_id`=`new_customer`.`marketing_id`
    		INNER JOIN `fac_calendar` ON `new_transaksi`.`trans_id`=`fac_calendar`.`acara`
    		WHERE `new_marketing`.`marketing_id`=`aidi`
            AND `fac_calendar`.`tanggal` BETWEEN '$from' AND '$to'
		) AS `jumlah_hari_training`
		FROM `new_marketing`
		WHERE `new_marketing`.`marketing_cabang`=:cabang;";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':cabang',$cabang,PDO::PARAM_STR,35);
		$statement->execute();

		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function DasborTransaksi(){
		
	}

	public function getValue($value){
		if ($value==''||empty($value)) {
			return "";
		} else {
			return $value;
		}
	}

	public function getIntegerValue($value){
		if ($value==''||empty($value)) {
			return "0";
		} else {
			return $value;
		}
	}

	public function getArrayValue($value){
		if (!is_array($value)||$value==''||empty($value)) {
			return "";
		} else {
			return implode("#", $value);
		}
	}

	public function getArrayValue2($value){
		if (!is_array($value)||$value==''||empty($value)) {
			return "";
		} else {
			return $value;
		}
	}


	private $username;
	private $password;
	private $cabang;
	private $nama;
	private $email;
	private $telepon;
	private $perusahaan;
	private $customer;
	private $quotation;

	public function getUsername(){
	    return $this->username;
	}

	public function setUsername($username){
	    $this->username = $username;
	}

	public function getPassword(){
	    return $this->password;
	}

	public function setPassword($password){
	    $this->password = $password;
	}

	public function getCabang(){
	    return $this->cabang;
	}

	public function setCabang($cabang){
	    $this->cabang = $cabang;
	}

	public function getNama(){
	    return $this->nama;
	}

	public function setNama($nama){
	    $this->nama = $nama;
	}

	public function getEmail(){
	    return $this->email;
	}

	public function setEmail($email){
	    $this->email = $email;
	}

	public function getTelepon(){
	    return $this->telepon;
	}

	public function setTelepon($telepon){
	    $this->telepon = $telepon;
	}

	public function getPerusahaan(){
	    return $this->perusahaan;
	}

	public function setPerusahaan($perusahaan){
	    $this->perusahaan = $perusahaan;
	}

	public function getCustomer(){
	    return $this->customer;
	}

	public function setCustomer($customer){
	    $this->customer = $customer;
	}

	public function getQuotation(){
	    return $this->quotation;
	}

	public function setQuotation($quotation){
	    $this->quotation = $quotation;
	}
}


?>