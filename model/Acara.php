<?php 
include_once 'Koneksi.php';
date_default_timezone_set("Asia/Jakarta"); 

class Acara extends Koneksi{
	
	function __construct(){
		$this->dbHost = $this->bukaKoneksi();
	}


	public function inputData($array){
		$array = (object)$array;
		$query="INSERT INTO `fac_calendar`(`tanggal`, `acara`) VALUES ('".$array->tanggal."','".$array->acara."')";
		$statement=$this->dbHost->prepare($query);

		if ($statement->execute()) {
			return "Data ".$array->acara." berhasil dimasukan";
		}else{
			return "Data ".$array->acara." gagal dimasukan";
		}
	}

	public function inputSimple($objek){
		$query="INSERT INTO `fac_calendar` (`id`, `tanggal`, `acara`) VALUES (NULL, '".$objek->tanggal[0]."', '".$objek->acara."')";

		for ($i=1; $i < count($objek->tanggal) ; $i++) { 
			if ($objek->tanggal[$i]!='') {
				$query .= ", (NULL, '".$objek->tanggal[$i]."', '".$objek->acara."')";
			}
		}
		
		$statement=$this->dbHost->prepare($query);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	



}



?>