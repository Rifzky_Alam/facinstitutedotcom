<?php 
include_once 'Koneksi.php';
include_once 'Pendaftar.php';

class ControlLaporan extends Koneksi{
	var $dbHost;

	private $username;
	private $absen;

	public function getUsername(){
        return $this->username;
    }

    public function setUsername($username){
        $this->username = $username;
    }

    public function getAbsen(){
        return $this->absen;
    }

    public function setAbsen($absen){
        $this->absen = $absen;
    }

	function __construct(){
		$this->dbHost = $this->bukaKoneksi();
	}
	
	public function DaftarListReimburse($username,$bulan,$tahun){
	    $query = "WITH data_laporan AS (
        SELECT laporan_harian.id AS id,laporan_harian.username AS username, laporan_harian.tanggal, users.nama AS nama, biaya_transport + biaya_parkir + biaya_lunch + biaya_dinner + biaya_kesehatan + biaya_allowance + biaya_lain AS reimbursement 
	    FROM `laporan_harian` 
	    INNER JOIN users ON users.username=laporan_harian.username
	    WHERE MONTH(laporan_harian.tanggal)=:bulan 
	    AND YEAR(laporan_harian.tanggal)=:tahun
	    AND laporan_harian.username=:username
	    AND laporan_harian.status_laporan < 5
	    ORDER BY laporan_harian.tanggal ASC
        )
        SELECT * FROM data_laporan
        WHERE data_laporan.reimbursement > 0 
        ORDER BY data_laporan.tanggal ASC";
        $statement = $this->dbHost->prepare($query);
        @$statement->bindParam(':bulan',$bulan,PDO::PARAM_STR,75);
        @$statement->bindParam(':tahun',$tahun,PDO::PARAM_STR,75);
        @$statement->bindParam(':username',$username,PDO::PARAM_STR,75);
		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function DaftarYangAdaReimburse($bln='',$thn=''){
	    $additional = "";
	    if($bln!=''&&$thn!=''){
	        $additional = " MONTH(laporan_harian.tanggal)='$bln' 
	    AND YEAR(laporan_harian.tanggal)='$thn' ";
	    }else{
	       $additional = " MONTH(laporan_harian.tanggal)=MONTH(CURDATE()) 
	    AND YEAR(laporan_harian.tanggal)=YEAR(CURDATE()) ";
	    }
	    $query = "WITH data_laporan AS (
        SELECT laporan_harian.id AS id,laporan_harian.username AS username, laporan_harian.tanggal, users.nama AS nama, biaya_transport + biaya_parkir + biaya_lunch + biaya_dinner + biaya_kesehatan + biaya_allowance + biaya_lain AS reimbursement 
	    FROM `laporan_harian` 
	    INNER JOIN users ON users.username=laporan_harian.username
	    WHERE ".$additional."
	    AND laporan_harian.status_laporan < 5
	    ORDER BY laporan_harian.tanggal ASC
        )
        SELECT data_laporan.username,data_laporan.nama,COUNT(data_laporan.id) AS jumlah FROM data_laporan
        WHERE data_laporan.reimbursement > 0
        GROUP BY data_laporan.username";
        $statement = $this->dbHost->prepare($query);
        
		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}
	
	    

    public function BuktiReimburse($id){
        $query ="SELECT `iseqid`, `sfklaphar`, `sfilename`, `itype`, `istatus`, `dupdate_at`, IFNULL(desc_tbl.dt_desc,'') AS desk 
        FROM `reim_upload` 
        LEFT JOIN desc_tbl ON desc_tbl.dt_flag=reim_upload.itype AND desc_tbl.dt_relate_tbl='reim_upload'
        WHERE reim_upload.sfklaphar=:id";
        $statement = $this->dbHost->prepare($query);
		@$statement->bindParam(':id',$id,PDO::PARAM_STR,75);
		
		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
        
    }
	
	public function DetailLaporanReimbursement($id){
	    $query = "SELECT `laporan_harian`.`id` AS `id`, DATE_FORMAT(laporan_harian.tanggal, '%W, %e %M %Y') AS `tanggal`,laporan_harian.tanggal AS tgl, laporan_harian.username AS `username`, `users`.`nama` AS `nama`, perusahaan.nama AS nama_usaha, perusahaan.alamat AS alamat, `biaya_transport`, `ket_transport`, `biaya_parkir`, `biaya_lunch`, `biaya_dinner`, `biaya_kesehatan`, `biaya_allowance`, `biaya_lain`, `ket_biaya_lain`, `status_laporan`, `waktu_lapor` 
        FROM `laporan_harian` 
        INNER JOIN users ON laporan_harian.username=users.username
        LEFT JOIN absensi ON absensi.id=laporan_harian.id_absen
        LEFT JOIN perusahaan ON perusahaan.id = absensi.nama_perusahaan
        WHERE `laporan_harian`.`id`=:id";
        $statement = $this->dbHost->prepare($query);
		@$statement->bindParam(':id',$id,PDO::PARAM_STR,75);
		
		$statement->execute();
		return $statement->fetch(PDO::FETCH_ASSOC);
	}
	
	public function LapBulananByAgendaTrans($menu){
	    if($menu=='lastmonth'){
	        if(date('m')=='1'){
	            $filter= " WHERE MONTH(new_transaksi.trans_date)=MONTH(DATE_SUB(CURDATE(), INTERVAL 1 MONTH))
	            AND YEAR(DATE_SUB(CURDATE(), INTERVAL 1 YEAR))";
	        }else{
	            $filter= " WHERE MONTH(new_transaksi.trans_date)=MONTH(DATE_SUB(CURDATE(), INTERVAL 1 MONTH))
	            AND YEAR(new_transaksi.trans_date)=YEAR(CURDATE())";
	        }
	    }elseif($menu=='curmonth'){
	        $filter=" WHERE MONTH(new_transaksi.trans_date)=MONTH(CURDATE())
	        AND YEAR(new_transaksi.trans_date)=YEAR(CURDATE())";
	    }else{
	    	$filter = "";
	    }
	    
	    $query = "SELECT nama, COUNT(new_trans_agendas.agd_id_trans) AS jumlah FROM `desc_agenda_kategori`
        INNER JOIN daftar_agenda ON daftar_agenda.ag_kategori=desc_agenda_kategori.dak_id
        INNER JOIN new_trans_agendas ON new_trans_agendas.agd_training=daftar_agenda.id
        INNER JOIN new_transaksi ON new_transaksi.trans_id=new_trans_agendas.agd_id_trans
        INNER JOIN new_invoice ON new_invoice.inv_id_trans=new_transaksi.trans_id
        ".$filter."
        GROUP BY desc_agenda_kategori.nama";
        $statement = $this->dbHost->prepare($query);
		
		$statement->execute();

		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	// belum implementasi
	public function GeneralLaporanTransaksiByAgenda($menu){
		if($menu=='lastmonth'){
	        if(date('m')=='1'){
	            $filter= "AND MONTH(new_transaksi.trans_date)=MONTH(DATE_SUB(CURDATE(), INTERVAL 1 MONTH))
	            AND YEAR(DATE_SUB(CURDATE(), INTERVAL 1 YEAR))";
	        }else{
	            $filter= "AND MONTH(new_transaksi.trans_date)=MONTH(DATE_SUB(CURDATE(), INTERVAL 1 MONTH))
	            AND YEAR(new_transaksi.trans_date)=YEAR(CURDATE())";
	        }
	    }elseif($menu=='curmonth'){
	        $filter="AND MONTH(new_transaksi.trans_date)=MONTH(CURDATE())
	        AND YEAR(new_transaksi.trans_date)=YEAR(CURDATE())";
	    }else{
	    	$filter = "";
	    }

		$query = "SELECT `dak_id`, `nama`,
		(
			SELECT COUNT(new_trans_agendas.agd_id_trans) FROM `new_trans_agendas` 
			INNER JOIN daftar_agenda
			ON daftar_agenda.id=new_trans_agendas.agd_training
    		INNER JOIN new_transaksi
    		ON new_transaksi.trans_id=new_trans_agendas.agd_id_trans
			WHERE daftar_agenda.ag_kategori=`dak_id`
    		".$filter." 
		) AS jumlah
		FROM `desc_agenda_kategori`";

        $statement = $this->dbHost->prepare($query);
		
		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function GeneralLaporanByWilayah($idwil,$menu){
		if($menu=='lastmonth'){
	        if(date('m')=='1'){
	            $filter= "MONTH(laporan_harian.tanggal)=MONTH(DATE_SUB(CURDATE(), INTERVAL 1 MONTH))
	            AND YEAR(DATE_SUB(CURDATE(), INTERVAL 1 YEAR))";
	        }else{
	            $filter= "MONTH(laporan_harian.tanggal)=MONTH(DATE_SUB(CURDATE(), INTERVAL 1 MONTH))
	            AND YEAR(laporan_harian.tanggal)=YEAR(CURDATE())";
	        }
	    }else{
	        $filter="MONTH(laporan_harian.tanggal)=MONTH(CURDATE())
	        AND YEAR(laporan_harian.tanggal)=YEAR(CURDATE())";
	    }

		$query = "SELECT `users`.`username` AS `usr`,`users`.`nama` AS `nama_staff`,
		(
            SELECT COUNT(laporan_harian.id)
			FROM `laporan_harian`
			INNER JOIN `intensif_summary`
			ON `intensif_summary`.`is_id_laporan`=`laporan_harian`.`id`
			INNER JOIN `new_transaksi` 
			ON `new_transaksi`.`trans_id`=`intensif_summary`.`is_id_trans`
			INNER JOIN `intensif_dawil`
            ON `intensif_dawil`.`ind_id`=`intensif_summary`.`is_dawil`
			WHERE `intensif_dawil`.`ind_id`=:idwilayah
			AND laporan_harian.username=`usr`
			AND ".$filter."
        ) AS `jumlah`
        FROM `users`
        WHERE `users`.`status`='aktif'
        AND (`users`.`team`='4' OR `users`.`team`='5')";

        $statement = $this->dbHost->prepare($query);
		@$statement->bindParam(':idwilayah',$idwil,PDO::PARAM_STR,75);
		
		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function GenLaporanByJnsAccurate($va_id,$menu){
		if($menu=='lastmonth'){
	        if(date('m')=='1'){
	            $filter= "MONTH(laporan_harian.tanggal)=MONTH(DATE_SUB(CURDATE(), INTERVAL 1 MONTH))
	            AND YEAR(DATE_SUB(CURDATE(), INTERVAL 1 YEAR))";
	        }else{
	            $filter= "MONTH(laporan_harian.tanggal)=MONTH(DATE_SUB(CURDATE(), INTERVAL 1 MONTH))
	            AND YEAR(laporan_harian.tanggal)=YEAR(CURDATE())";
	        }
	    }else{
	        $filter="MONTH(laporan_harian.tanggal)=MONTH(CURDATE())
	        AND YEAR(laporan_harian.tanggal)=YEAR(CURDATE())";
	    }

		$query = "SELECT `users`.`username` AS `usr`,`users`.`nama` AS `nama_staff`,
		(
            SELECT COUNT(laporan_harian.id)
			FROM `laporan_harian`
			INNER JOIN `intensif_summary`
			ON `intensif_summary`.`is_id_laporan`=`laporan_harian`.`id`
			INNER JOIN `new_transaksi` 
			ON `new_transaksi`.`trans_id`=`intensif_summary`.`is_id_trans`
			INNER JOIN `perusahaan` 
			ON `perusahaan`.`id`=`new_transaksi`.`trans_id_usaha`
			WHERE `new_transaksi`.`trans_va`=:idacc
			AND laporan_harian.username=`usr`
			AND ".$filter." 
        ) AS `jumlah`
        FROM `users`
        WHERE `users`.`status`='aktif'
        AND (`users`.`team`='4' OR `users`.`team`='5')";

        $statement = $this->dbHost->prepare($query);
		@$statement->bindParam(':idacc',$va_id,PDO::PARAM_STR,75);
		
		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function GeneralReportByJnsUsaha($idjnsusaha,$menu){
		if($menu=='lastmonth'){
	        if(date('m')=='1'){
	            $filter= "MONTH(laporan_harian.tanggal)=MONTH(DATE_SUB(CURDATE(), INTERVAL 1 MONTH))
	            AND YEAR(DATE_SUB(CURDATE(), INTERVAL 1 YEAR))";
	        }else{
	            $filter= "MONTH(laporan_harian.tanggal)=MONTH(DATE_SUB(CURDATE(), INTERVAL 1 MONTH))
	            AND YEAR(laporan_harian.tanggal)=YEAR(CURDATE())";
	        }
	    }else{
	        $filter="MONTH(laporan_harian.tanggal)=MONTH(CURDATE())
	        AND YEAR(laporan_harian.tanggal)=YEAR(CURDATE())";
	    }

		$query = "SELECT `users`.`username` AS `usr`,`users`.`nama` AS `nama_staff`,
		(
            SELECT COUNT(`perusahaan_jns_usaha`.`uju_num`)
			FROM `absensi` 
			INNER JOIN `laporan_harian` ON `laporan_harian`.`id_absen`=`absensi`.`id` 
			INNER JOIN `perusahaan` ON `perusahaan`.`id`=`absensi`.`nama_perusahaan` 
			LEFT JOIN `perusahaan_jns_usaha` 
            ON `perusahaan_jns_usaha`.`id_company`=`perusahaan`.`id` 
			WHERE perusahaan_jns_usaha.id_jenis_usaha=:idjenisusaha
        	AND ".$filter." 
            AND `absensi`.`username`=`usr`
			AND perusahaan.id NOT IN (
                'f4e745e1015af1c15ebcb70b77f1426c',
                'd5eb8c090d191587bb8a4b875bae4fed',
                '08964cc6406ac24b00bc774eceb799c1'
            )
        ) AS `jumlah`
        FROM `users`
        WHERE `users`.`status`='aktif'
        AND (`users`.`team`='4' OR `users`.`team`='5')";
		$statement = $this->dbHost->prepare($query);
		@$statement->bindParam(':idjenisusaha',$idjnsusaha,PDO::PARAM_STR,75);
		// echo $query;
		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}
	
	// @url /administrasi/laporan/staff/bulanan/bytransport
	public function GeneralReportByTrans($idtrans,$menu){
	    if($menu=='lastmonth'){
	        if(date('m')=='1'){
	            $filter= "MONTH(laporan_harian.tanggal)=MONTH(DATE_SUB(CURDATE(), INTERVAL 1 MONTH))
	            AND YEAR(DATE_SUB(CURDATE(), INTERVAL 1 YEAR))";
	        }else{
	            $filter= "MONTH(laporan_harian.tanggal)=MONTH(DATE_SUB(CURDATE(), INTERVAL 1 MONTH))
	            AND YEAR(laporan_harian.tanggal)=YEAR(CURDATE())";
	        }
	    }else{
	        $filter="MONTH(laporan_harian.tanggal)=MONTH(CURDATE())
	        AND YEAR(laporan_harian.tanggal)=YEAR(CURDATE())";
	    }
		$query = "SELECT users.username AS usr, users.nama AS nama_staff,
        (
	        SELECT COUNT(laporan_harian.id) 
            FROM laporan_harian
            INNER JOIN absensi ON absensi.id=laporan_harian.id_absen
            WHERE ".$filter." 
            AND laporan_harian.jns_transportasi=:trans
            AND laporan_harian.username=usr
            AND absensi.nama_perusahaan NOT IN (
    		    'f4e745e1015af1c15ebcb70b77f1426c',
    		    'd5eb8c090d191587bb8a4b875bae4fed',
    		    '08964cc6406ac24b00bc774eceb799c1'
	        )
        ) AS jumlah
        FROM users
        WHERE users.status='aktif'
        AND (users.team='4' OR users.team='5')";
		$statement = $this->dbHost->prepare($query);
		@$statement->bindParam(':trans',$idtrans,PDO::PARAM_STR,75);
		// echo $query;
		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}
	
	// @url /administrasi/laporan/staff/bulanan/bystatuskerja
	public function GeneralReportByStatusKerja($idstatus,$menu){
	    if($menu=='lastmonth'){
	        if(date('m')=='1'){
	            $filter= "MONTH(laporan_harian.tanggal)=MONTH(DATE_SUB(CURDATE(), INTERVAL 1 MONTH))
	            AND YEAR(DATE_SUB(CURDATE(), INTERVAL 1 YEAR))";
	        }else{
	            $filter= "MONTH(laporan_harian.tanggal)=MONTH(DATE_SUB(CURDATE(), INTERVAL 1 MONTH))
	            AND YEAR(laporan_harian.tanggal)=YEAR(CURDATE())";
	        }
	    }else{
	        $filter="MONTH(laporan_harian.tanggal)=MONTH(CURDATE())
	        AND YEAR(laporan_harian.tanggal)=YEAR(CURDATE())";
	    }
		$query = "SELECT users.username AS usr, users.nama AS nama_staff,
        (
	        SELECT COUNT(laporan_harian.id) 
            FROM laporan_harian
            INNER JOIN absensi ON absensi.id=laporan_harian.id_absen
            WHERE ".$filter." 
            AND laporan_harian.status_kerja=:status
            AND laporan_harian.username=usr
            AND absensi.nama_perusahaan NOT IN (
    		    'f4e745e1015af1c15ebcb70b77f1426c',
    		    'd5eb8c090d191587bb8a4b875bae4fed',
    		    '08964cc6406ac24b00bc774eceb799c1'
	        )
        ) AS jumlah
        FROM users
        WHERE users.status='aktif'
        AND (users.team='4' OR users.team='5')";
		$statement = $this->dbHost->prepare($query);
		@$statement->bindParam(':status',$idstatus,PDO::PARAM_STR,75);
		// echo $query;
		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}
	
	// /administrasi/laporan/staff/bulanan/peran
	public function GeneralReportByPeran($idperan,$menu){
	    if($menu=='lastmonth'){
	        if(date('m')=='1'){
	            $filter= "MONTH(laporan_harian.tanggal)=MONTH(DATE_SUB(CURDATE(), INTERVAL 1 MONTH))
	            AND YEAR(DATE_SUB(CURDATE(), INTERVAL 1 YEAR))";
	        }else{
	            $filter= "MONTH(laporan_harian.tanggal)=MONTH(DATE_SUB(CURDATE(), INTERVAL 1 MONTH))
	            AND YEAR(laporan_harian.tanggal)=YEAR(CURDATE())";
	        }
	    }else{
	        $filter="MONTH(laporan_harian.tanggal)=MONTH(CURDATE())
	        AND YEAR(laporan_harian.tanggal)=YEAR(CURDATE())";
	    }
		$query = "SELECT users.username AS usr, users.nama AS nama_staff,
        (
	        SELECT COUNT(laporan_harian.id) 
            FROM laporan_harian
            INNER JOIN absensi ON absensi.id=laporan_harian.id_absen
            WHERE ".$filter." 
            AND laporan_harian.peran=:peran
            AND laporan_harian.username=usr
            AND absensi.nama_perusahaan NOT IN (
    		    'f4e745e1015af1c15ebcb70b77f1426c',
    		    'd5eb8c090d191587bb8a4b875bae4fed',
    		    '08964cc6406ac24b00bc774eceb799c1'
	        )
        ) AS jumlah
        FROM users
        WHERE users.status='aktif'
        AND (users.team='4' OR users.team='5')";
		$statement = $this->dbHost->prepare($query);
		@$statement->bindParam(':peran',$idperan,PDO::PARAM_STR,75);
		// echo $query;
		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function GetLaporanPerBulan($bulan,$tahun){
		$query = "SELECT `laporan_harian`.`id` AS `id`,`laporan_harian`.`status_kerja` AS `statuskerja`,`perusahaan`.`nama` AS `nama`,
		 `laporan_harian`.`tanggal` AS `tanggal`,`laporan_harian`.`peran` AS `peran`,
		IFNULL(`tarif_peran`,0) AS `tarif_peran`,
        IFNULL(`tarif_accurate`,0) AS `tarif_accurate`,
        IFNULL(`ijt_acc`,0) AS `ijt_acc`,
        IFNULL(`tarif_hari_kerja`,0) AS `tarif_hari_kerja`,
        IFNULL(`ijt_hari`,0) AS `ijt_hari`,
        IFNULL(`tarif_wilayah`,0) AS `tarif_wilayah`,
        IFNULL(`ijt_wilayah`,0) AS `ijt_wilayah`,
        IFNULL(`is_ttl_attnd`,1) AS is_ttl_attnd,status_laporan
        FROM `laporan_harian`
		LEFT JOIN `absensi` ON `absensi`.`id`=`laporan_harian`.`id_absen`
		LEFT JOIN `perusahaan` ON `perusahaan`.`id`=`absensi`.`nama_perusahaan`
        LEFT JOIN `laporan_view` ON `laporan_view`.`id`=`laporan_harian`.`id`
		WHERE MONTH(`laporan_harian`.`tanggal`)='".$bulan."' 
		AND YEAR(`laporan_harian`.`tanggal`)='".$tahun."'
		AND `laporan_harian`.`username`=:username 
		ORDER BY `tanggal` DESC";
		$statement = $this->dbHost->prepare($query);
		@$statement->bindParam(':username',$this->username,PDO::PARAM_STR,75);
		// echo $query;
		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function getDataPendingAttendance(){
		$query = "SELECT a.id AS id,tanggal_absen,u.nama AS nama_user,p.nama AS nama_usaha,absen_masuk,absen_pulang,absen_status,absen_ket
		FROM absensi AS a
		INNER JOIN users AS u ON u.username=a.username
		INNER JOIN perusahaan AS p ON p.id = a.nama_perusahaan
		WHERE a.absen_status = '-1' AND a.id NOT IN (SELECT laporan_harian.id_absen FROM laporan_harian)";
		$statement = $this->dbHost->prepare($query);
		
		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function searchLaporanForMarketing($objek){
		$query="SELECT `nama`,`nama_perusahaan`,`keterangan_jenis_usaha`,`ket_kegiatan`, `rekan`, `peserta_kegiatan`
		FROM `laporan_view` 
		WHERE `nama_perusahaan` LIKE '%".$objek->usaha."%' AND `nama` LIKE '%".$objek->nama."%' AND `ket_kegiatan` LIKE '%".$objek->kk."%';";
		$statement = $this->dbHost->prepare($query);
		//bind parameter
		// $statement->bindParam(':username',$username,PDO::PARAM_STR,75);
		
		//execute
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$datas=json_encode($results);
				
		return json_decode($datas);
	}

	public function selectByID($id){
		$query="SELECT `laporan_harian`.`id` AS id, `laporan_harian`.`username` AS username,cari_perusahaan(absensi.nama_perusahaan) AS nama_perusahaan, `jns_hari_kerja`, `hari`, `tanggal`, `jumlah_overtime`, `id_absen`,`kegiatan`, `ket_kegiatan`, `peserta_kegiatan`, `status_kerja`, `peran`, `rekan`, `jns_transportasi`, `biaya_transport`, `ket_transport`, `biaya_parkir`, `biaya_lunch`, `biaya_dinner`, `biaya_kesehatan`, `biaya_allowance`, `biaya_lain`, `ket_biaya_lain`, `waktu_lapor` FROM `laporan_harian`,`absensi` WHERE laporan_harian.id_absen=absensi.id AND laporan_harian.id = :id";
		$statement = $this->dbHost->prepare($query);
		$this->dbHost->prepare($query);
		$statement->bindParam(':id',$id,PDO::PARAM_STR,40);
		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function selectTanggal($username){
		$query = "SELECT id, tanggal FROM laporan_harian WHERE username = :username ORDER BY tanggal DESC";
		$statement = $this->dbHost->prepare($query);
		//bind parameter
		$statement->bindParam(':username',$username,PDO::PARAM_STR,75);
		
		//execute
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$datas=json_encode($results);
				
		return $datas;
	}


	public function searchLaporan($tanggalAwal,$tanggalAkhir,$username,$team){
		$query="SELECT * FROM laporan_raw WHERE tanggal BETWEEN '$tanggalAwal' AND '$tanggalAkhir' AND team = '$team' AND username = '$username' ORDER BY tanggal ASC";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;

	}

	public function dataHarian($tanggalAwal,$tanggalAkhir,$team){
		$query = "SELECT * FROM laporan_raw WHERE team LIKE '%%' AND tanggal BETWEEN '$tanggalAwal' AND '$tanggalAkhir'";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function searchLaporanAdmin($tanggalAwal,$tanggalAkhir,$username){
		$query="SELECT * FROM laporan_raw WHERE username = '$username' AND tanggal BETWEEN '$tanggalAwal' AND '$tanggalAkhir' ORDER BY tanggal ASC";

		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function downloadLaporanAdmin($tanggalAwal,$tanggalAkhir,$username){
		$query="SELECT nama, `jns_hari_kerja`, `hari`, `tanggal`, `jam_mulai`, `jam_selesai`, `jumlah_overtime`, `id_klien`, `ket_kegiatan`, `peserta_kegiatan`, `status_kerja`, `peran`, `rekan`, `jns_transportasi`, `biaya_transport`, `ket_transport`, `biaya_parkir`, `biaya_lunch`, `biaya_dinner`, `biaya_kesehatan`, `biaya_allowance`, `biaya_lain`, `ket_biaya_lain`, `waktu_lapor` FROM `laporan_harian`,`users` WHERE  tanggal BETWEEN '2016-06-02' AND '2016-06-10' AND users.username=laporan_harian.username";
		for ($i=0; $i < count($username) ; $i++) { 
			if ($i>0) {
				$query .= " OR users.username='".$username[$i]."' AND tanggal BETWEEN '".$tanggalAwal."' AND '".$tanggalAkhir."' AND users.username=laporan_harian.username";
			}else{
				$query .= " AND users.username='".$username[0]."'";
			}
		}
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function SetLocalFormat(){
		$query = "SET @@lc_time_names;";
		$statement=$this->dbHost->prepare($query);

		// echo $query;
		$statement->execute();
	}

	public function NewDataHarian($username='',$tanggalawal='',$tanggalakhir=''){
		$query ="SELECT laporan_harian.id AS id,laporan_harian.username AS username, users.nama AS nama,
		team,int_hk_hari AS jns_hari_kerja,hari,
        DATE_FORMAT(laporan_harian.tanggal, '%W, %e %M %Y') AS tanggal,
        jumlah_overtime,absen_masuk,absen_pulang,
		cari_perusahaan(absensi.nama_perusahaan) AS klien,ket_kegiatan,
		peserta_kegiatan, dsk_desc AS status_kerja,pk_peran AS peran,rekan,
        lt_ket AS jns_transportasi,biaya_transport,ket_transport,
		biaya_parkir,biaya_lunch,biaya_dinner,
		biaya_kesehatan,biaya_allowance,biaya_lain,ket_biaya_lain,
        DATE_FORMAT(waktu_lapor,'%W, %e %M %Y %T') AS `timestamp`,
		`ip_tariff` AS `tarif_peran`, `ihk_tariff` AS `tarif_hari_kerja`, 
        `ija_tariff` AS `tarif_accurate`, `ind_tariff` AS `tarif_wilayah`, 
        IFNULL(`ijt_acc`,0) AS `ijt_acc`, IFNULL(`ijt_hari`,0) AS `ijt_hari`, 
        IFNULL(`ijt_wilayah`,0) AS `ijt_wilayah`, IFNULL(is_ttl_attnd,1) AS is_ttl_attnd, status_laporan as status,
        `itrans`, `ipark`, `ilunch`, `idnr`, `imed`, `iallow`, `iothr`
		FROM laporan_harian
		INNER JOIN users ON laporan_harian.username=users.username
		LEFT JOIN absensi ON laporan_harian.id_absen=absensi.id
		LEFT JOIN daftar_status_kerja ON daftar_status_kerja.dsk_id=laporan_harian.status_kerja
		LEFT JOIN daftar_peran_kerja ON daftar_peran_kerja.pk_id=laporan_harian.peran
		LEFT JOIN intensif_jns_hr_kerja
		ON intensif_jns_hr_kerja.int_hk_id=laporan_harian.jns_hari_kerja
		LEFT JOIN daftar_alat_trans ON daftar_alat_trans.lt_id=laporan_harian.jns_transportasi
		LEFT JOIN `intensif_summary`
        ON `laporan_harian`.`id`=`intensif_summary`.`is_id_laporan`
		LEFT JOIN `intensif_peran`
		ON `intensif_peran`.`ip_id`=`intensif_summary`.`is_peran`
		LEFT JOIN `intensif_jenis_acc`
		ON `intensif_jenis_acc`.`ija_id`=`intensif_summary`.`is_jenisacc`
		LEFT JOIN `intensif_hari_kerja`
		ON `intensif_hari_kerja`.`ihk_id`=`intensif_summary`.`is_harikerja`
		LEFT JOIN `intensif_dawil`
		ON `intensif_dawil`.`ind_id`=`intensif_summary`.`is_dawil`
		LEFT JOIN `new_transaksi`
		ON `new_transaksi`.`trans_id`=`intensif_summary`.`is_id_trans`
		LEFT JOIN `intensif_jns_trans` 
		ON `intensif_jns_trans`.`ijt_jns_trans`=`new_transaksi`.`trans_jenis` 
		AND `intensif_jns_trans`.`ijt_status`='1'
		LEFT JOIN `reim_denial`
        ON reim_denial.sfkid=laporan_harian.id
        WHERE ";

        if ($username==''&&$tanggalawal==''&&$tanggalakhir=='') {
        	$query .= "laporan_harian.tanggal=CURDATE()";
        }

        if ($username!='') {
        	$query .= " laporan_harian.username='".$username."'"; // add multiple user here using (cond or cond or ..) if count(data)>1 then use multiple user
        }

        if ($tanggalawal!=''&&$tanggalakhir!='') {
	        if ($username!='') {
	        	$query .= " AND `laporan_harian`.`tanggal` BETWEEN '".$tanggalawal."' AND '".$tanggalakhir."' ";
	        }else{
	        	$query .= " `laporan_harian`.`tanggal` BETWEEN '".$tanggalawal."' AND '".$tanggalakhir."' ";
	        }
        }

        if ($username==''&&$tanggalawal==''&&$tanggalakhir=='') {
        	$query .= " ORDER BY laporan_harian.tanggal DESC ";
        }else{
        	$query .= " ORDER BY laporan_harian.tanggal ASC ";
        }

        $query .=";";
        
		$statement=$this->dbHost->prepare($query);

		
		
		$statement->execute();
		$hasil = $statement->fetchAll(PDO::FETCH_ASSOC);
		return $hasil;
	}


	public function jumlahData($column,$username,$tanggal){
		$query="SELECT COUNT($column) AS jumlah FROM `laporan_harian` WHERE `tanggal`='$tanggal' AND username='$username'";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function SelectByTanggalAndID($username,$tanggal){
		$query ="SELECT laporan_harian.id AS id,laporan_harian.username AS username,nama,jns_hari_kerja,hari,tanggal,jumlah_overtime,peserta_kegiatan,ket_kegiatan,status_kerja,peran,rekan,jns_transportasi,biaya_transport,ket_transport,biaya_parkir,biaya_lunch,biaya_dinner,biaya_kesehatan,biaya_allowance,biaya_lain,ket_biaya_lain FROM laporan_harian,users WHERE laporan_harian.username=:username AND tanggal=:tanggal AND laporan_harian.username=users.username";
		$statement = $this->dbHost->prepare($query);

		$statement->bindParam(':username',$username,PDO::PARAM_STR,75);
		$statement->bindParam(':tanggal',$tanggal,PDO::PARAM_STR,15);

		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function downloadLaporan($username,$tanggalAwal,$tanggalAkhir){
		$query = "SELECT * FROM `laporan_view` WHERE `username` = '$username' AND `tanggal` BETWEEN '$tanggalAwal' AND '$tanggalAkhir' ORDER BY tanggal ASC";
		$statement = $this->dbHost->prepare($query);

		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;

	}

	public function FetchKeteranganKegiatan(){
		$query = "SELECT * FROM `lap_ket_kegiatan`;";
		$statement = $this->dbHost->prepare($query);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function CekKeterangan($value,$stack){
		$value = explode(' # ', $value);
		$retval = array();
		for ($i = 0; $i < count($value); $i++) {
			for ($j = 0; $j < count($stack); $j++) {
				if ($value[$i]==$stack[$j]->lkk_id) {
					array_push($retval,$stack[$j]->lkk_ket);
				}elseif (preg_match('/9-/i', $value[$i])) {
					$value[$i] = str_replace('9-', 'Lainnya-', $value[$i]);
					array_push($retval,$value[$i]);
				}
			}
		}
		if (count($retval)=='0') {
			return "";
		}else{
			return implode(' # ', $retval);
		}
	}

	public function fetchAllData(){
		$query="SELECT * FROM laporan_harian";
		$statement = $this->dbHost->prepare($query);

		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;		
	}



	public function updateLaporanByAdmin($objek){
		$query = "UPDATE `laporan_harian` SET 
		`username`=:username,
		`jns_hari_kerja`=:jnsHariKerja,
		`jam_mulai`=:jamMulai,
		`jam_selesai`=:jamSelesai,
		`jumlah_overtime`=:overtime,
		`id_klien`=:idKlien,
		`ket_kegiatan`=:ketKegiatan,
		`status_kerja`=:statusKerja,
		`peran`=:peran,
		`rekan`=:rekan,
		`jns_transportasi`=:jnsTransportasi,
		`biaya_transport`=:biayaTransport,
		`ket_transport`=:ketTransport,
		`biaya_parkir`=:biayaParkir,
		`biaya_lunch`=:biayaLunch,
		`biaya_dinner`=:biayaDinner,
		`biaya_kesehatan`=:biayaKesehatan,
		`biaya_allowance`=:biayaAllowance,
		`biaya_lain`=:biayaLain,
		`ket_biaya_lain`=:ketBiayaLain,
		`waktu_lapor`=CURRENT_TIMESTAMP 
		WHERE `username`=:username  AND `tanggal`=:tanggal";
		$objek = json_decode($objek);
		
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':username',$objek->username,PDO::PARAM_STR,75);
		$statement->bindParam(':tanggal',$objek->tanggal,PDO::PARAM_STR,75);
		$statement->bindParam(':jnsHariKerja',$objek->jenisHariKerja,PDO::PARAM_STR,40);
		$statement->bindParam(':jamMulai',$objek->jamMasuk,PDO::PARAM_STR,15);
		$statement->bindParam(':jamSelesai',$objek->jamPulang,PDO::PARAM_STR,15);
		$statement->bindParam(':overtime',$objek->overtime,PDO::PARAM_INT,75);
		$statement->bindParam(':idKlien',$objek->namaKlien,PDO::PARAM_STR,35);
		$statement->bindParam(':ketKegiatan',$objek->keteranganKegiatan,PDO::PARAM_STR,800);
		$statement->bindParam(':statusKerja',$objek->statusKerja,PDO::PARAM_STR,15);
		$statement->bindParam(':peran',$objek->peran,PDO::PARAM_STR,75);
		$statement->bindParam(':rekan',$objek->rekan,PDO::PARAM_STR,75);
		$statement->bindParam(':jnsTransportasi',$objek->jenisTransportasi,PDO::PARAM_STR,75);
		$statement->bindParam(':biayaTransport',$this->getDefaultNumber($objek->biayaTransportasi),PDO::PARAM_INT,15);
		$statement->bindParam(':ketTransport',$objek->keteranganTransportasi,PDO::PARAM_STR,500);
		$statement->bindParam(':biayaParkir',$this->getDefaultNumber($objek->biayaParkir),PDO::PARAM_INT,15);
		$statement->bindParam(':biayaLunch',$this->getDefaultNumber($objek->biayaLunch),PDO::PARAM_INT,75);
		$statement->bindParam(':biayaDinner',$this->getDefaultNumber($objek->biayaDinner),PDO::PARAM_INT,15);
		$statement->bindParam(':biayaKesehatan',$this->getDefaultNumber($objek->biayaKesehatan),PDO::PARAM_INT,15);
		$statement->bindParam(':biayaAllowance',$this->getDefaultNumber($objek->biayaAllowance),PDO::PARAM_INT,15);
		$statement->bindParam(':biayaLain',$this->getDefaultNumber($objek->biayaLain),PDO::PARAM_INT,75);
		$statement->bindParam(':ketBiayaLain',$objek->keteranganBiayaLain,PDO::PARAM_STR,255);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}


	}

	public function updateLaporan($objek){	

		$query = "UPDATE laporan_harian SET peserta_kegiatan=:peserta, ket_kegiatan=:ketKegiatan, kegiatan=:kegiatanz, status_kerja=:statusKerja, peran=:peran, rekan=:rekan, jns_transportasi=:jnsTrans,biaya_transport=:biayaTrans, ket_transport=:ketTrans, biaya_parkir=:biayaParkir, biaya_lunch=:biayaLunch, biaya_dinner=:biayaDinner, biaya_kesehatan=:biayaKes,biaya_allowance=:biayaAllowance, biaya_lain=:biayaLain, ket_biaya_lain=:ketBiayaLain, waktu_lapor=CURRENT_TIMESTAMP WHERE id=:id";
		$objek = json_decode($objek);
		$statement = $this->dbHost->prepare($query);

		$statement->bindParam(':peserta',$objek->peserta,PDO::PARAM_STR,255);
		$statement->bindParam(':ketKegiatan',$objek->keteranganKegiatan,PDO::PARAM_STR,500);
		$statement->bindParam(':kegiatanz',$objek->myKegiatan,PDO::PARAM_STR,255);
		$statement->bindParam(':statusKerja',$objek->statusKerja,PDO::PARAM_STR,14);

		$statement->bindParam(':peran',$objek->peran,PDO::PARAM_STR,10);
		$statement->bindParam(':rekan',$objek->rekan,PDO::PARAM_STR,270);
		$statement->bindParam(':jnsTrans',$objek->jenisTransportasi,PDO::PARAM_STR,20);
		$statement->bindParam(':biayaTrans',$objek->biayaTransportasi,PDO::PARAM_INT,7);

		$statement->bindParam(':ketTrans',$objek->keteranganTransportasi,PDO::PARAM_STR,150);
		$statement->bindParam(':biayaParkir',$objek->biayaParkir,PDO::PARAM_INT,5);
		$statement->bindParam(':biayaLunch',$objek->biayaMakanSiang,PDO::PARAM_INT,7);
		$statement->bindParam(':biayaDinner',$objek->biayaMakanMalam,PDO::PARAM_INT,5);

		$statement->bindParam(':biayaKes',$objek->biayaKesehatan,PDO::PARAM_INT,7);
		$statement->bindParam(':biayaAllowance',$objek->biayaAllowance,PDO::PARAM_INT,7);
		$statement->bindParam(':biayaLain',$objek->biayaLain,PDO::PARAM_INT,7);
		$statement->bindParam(':ketBiayaLain',$objek->keteranganBiayaLain,PDO::PARAM_STR,225);
		
		$statement->bindParam(':id',$objek->aidi,PDO::PARAM_STR,75);


		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}

	}

	public function cariDataBelumLaporan($username){
		$query = "SELECT COUNT(`username`) AS jumlah FROM `laporan_harian` WHERE `ket_kegiatan` IS NULL AND `username`=:username";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':username',$username,PDO::PARAM_STR,75);
		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function insertLaporan($objek){
		$query = "INSERT INTO laporan_harian(id,username, jns_hari_kerja, hari, tanggal, id_absen, jumlah_overtime) 
		VALUES(:id, :username, :jnsHariKerja, :hari, :tgl, :idAbseen, :jumlahOvertime)";

		$id=md5('L-'.$objek->getID().date('Y-m-d'));
		$jnshariKerja = $this->cariHariKerjaNasional(date('l'));
		$hari = $this->cariNamaHari(date('l'));
		$overtime = $this->carioverTime($objek->jamMulai,$objek->jamSelesai);
		if ($overtime<0) {
			$overtime = 0;
		}

		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':id',$id,PDO::PARAM_STR,40);
		$statement->bindParam(':username',$objek->getID(),PDO::PARAM_STR,75);
		$statement->bindParam(':jnsHariKerja',$jnshariKerja,PDO::PARAM_STR,40);
		$statement->bindParam(':hari',$hari,PDO::PARAM_STR,8);
		$statement->bindParam(':tgl',date('Y-m-d'),PDO::PARAM_STR,12);
		$statement->bindParam(':idAbseen',$objek->idAbsen,PDO::PARAM_STR,9);
		$statement->bindParam(':jumlahOvertime',$overtime,PDO::PARAM_INT,2);

		if ($statement->execute()) {
			//$dbHost = null;
			return $jnshariKerja;
		}else{
			return $overtime;
		}
	}


	public function RejectRequestEdit($idabsen){
		$query = "UPDATE `absensi` SET `absen_pulang` = NULL,`absen_status`='0',`absen_ket`='-' WHERE `id` = :idabsen;";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':idabsen',$idabsen,PDO::PARAM_STR,40);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}


	public function NewLaporan(){
		$query = "INSERT INTO laporan_harian(id,username, jns_hari_kerja, hari, tanggal, id_absen, jumlah_overtime) 
		VALUES(:id, :username, :jnsHariKerja, :hari, :tgl, :idAbseen, :jumlahOvertime)";

		$id=md5('L-'.$this->absen->username.date('Y-m-d'));
		$jnshariKerja = $this->cariHariKerjaNasional(date('l'));
		$hari = $this->cariNamaHari(date('l'));
		$overtime = $this->carioverTime($this->absen->jamMulai,$this->absen->jamSelesai);
		if ($overtime<0) {
			$overtime = 0;
		}

		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':id',$id,PDO::PARAM_STR,40);
		$statement->bindParam(':username',$this->absen->username,PDO::PARAM_STR,75);
		$statement->bindParam(':jnsHariKerja',$jnshariKerja,PDO::PARAM_STR,40);
		$statement->bindParam(':hari',$hari,PDO::PARAM_STR,8);
		@$statement->bindParam(':tgl',date('Y-m-d'),PDO::PARAM_STR,12);
		$statement->bindParam(':idAbseen',$this->absen->idAbsen,PDO::PARAM_STR,9);
		$statement->bindParam(':jumlahOvertime',$overtime,PDO::PARAM_INT,2);

		if ($statement->execute()) {
			//$dbHost = null;
			return true;
		}else{
			return false;
		}

	}

	public function insertLaporanz($objek){
		$query = "INSERT INTO laporan_harian(username, jns_hari_kerja, hari, tanggal, jam_mulai, jam_selesai,id_klien, jumlah_overtime) 
		VALUES(:username, 'jnsHariKerja', 'hari', :tgl, ':jamMulai', ':jamSelesai', ':namaPerusahaan', '0')";

		$jnshariKerja = $this->cariHariKerjaNasional(date('l'));
		$hari = $this->cariNamaHari(date('l'));
		$overtime = $this->carioverTime($objek->jamMulai,$objek->jamSelesai);
		if ($overtime<0){
			$overtime = 0;
		}else{
			$overtime = intval($overtime);
		}

		$statement = $this->dbHost->prepare($query);

		$statement->bindParam(':username',$objek->id,PDO::PARAM_STR,75);
		//$statement->bindParam(':jnsHariKerja',$jnshariKerja,PDO::PARAM_STR,40);
		//$statement->bindParam(':hari',$hari,PDO::PARAM_STR,8);
		$statement->bindParam(':tgl',date('Y-m-d'),PDO::PARAM_STR,12);
		//$statement->bindParam(':jamMulai',$objek->jamMulai,PDO::PARAM_STR,9);
		//$statement->bindParam(':jamSelesai',$objek->jamSelesai,PDO::PARAM_STR,9);
		//$statement->bindParam(':namaPerusahaan',$objek->namaKantor,PDO::PARAM_STR,50);
		//$statement->bindParam(':jumlahOvertime',$overtime,PDO::PARAM_INT,2);

		
		if ($statement->execute()) {
			//$dbHost = null;
			return "Data Saved!";
		}else{
			return "Error saving datas";
		}
	}

	public function insertLaporanForEdit($objek){
		$query = "INSERT INTO laporan_harian(id,username, jns_hari_kerja, hari, tanggal, id_absen, jumlah_overtime) 
		VALUES(:id,:username, :jnsHariKerja, :hari, :tgl, :idAbsen, :jumlahOvertime)";
		$aidi=md5('coba');
		$jnshariKerja = $this->cariHariKerjaNasionaldua(date('l', strtotime($objek->tanggal)),$objek->tanggal);
		$hari = $this->cariNamaHari(date('l', strtotime($objek->tanggal)));
		$overtime = $this->carioverTime($objek->absenMasuk,$objek->absenPulang);
		if ($overtime<0) {
			$overtime = 0;
		}

		$statement = $this->dbHost->prepare($query);

		@$statement->bindParam(':id',@md5('L-'.$objek->username.$objek->tanggal),PDO::PARAM_STR,75);
		$statement->bindParam(':username',$objek->username,PDO::PARAM_STR,75);
		$statement->bindParam(':jnsHariKerja',$jnshariKerja,PDO::PARAM_STR,40);
		$statement->bindParam(':hari',$hari,PDO::PARAM_STR,8);
		$statement->bindParam(':tgl',$objek->tanggal,PDO::PARAM_STR,12);
		$statement->bindParam(':idAbsen',$objek->id,PDO::PARAM_STR,50);
		$statement->bindParam(':jumlahOvertime',$overtime,PDO::PARAM_INT,2);

		if ($statement->execute()) {
			//$dbHost = null;
			return $jnshariKerja;
		}else{
			return $overtime;
		}
	}

	public function CariHariLibur($tanggal){
		$query = "SELECT COUNT(hlb_id) AS jumlah
		FROM `harlibnas`  
		WHERE `harlibnas`.`hlb_tanggal`=:tanggal";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':tanggal',$tanggal,PDO::PARAM_STR,75);
		$statement->execute();
		$hasil = $statement->fetch(PDO::FETCH_OBJ);
		return $hasil->jumlah;
	}

public function cariNamaHari($value){
    if ($value=='Sunday') {
        return 'ahad';
    } else if($value=='Monday'){
        return 'senin';
    } else if($value=='Tuesday'){
        return 'selasa';
    } else if($value=='Wednesday'){
        return 'rabu';
    } else if($value=='Thursday'){
        return 'kamis';
    } else if($value=='Friday'){
        return 'jumat';
    } else if($value=='Saturday'){
        return 'sabtu';
    }
}

public function cariHariKerjaNasionaldua($value,$tanggal){
	if (@$this->CariHariLibur($tanggal)=='1') {
		return '4';
	}else{
		if ($value=='Sunday') {
	        return '3';
	    } else if($value=='Monday'){
	        return '1';
	    } else if($value=='Tuesday'){
	        return '1';
	    } else if($value=='Wednesday'){
	        return '1';
	    } else if($value=='Thursday'){
	        return '1';
	    } else if($value=='Friday'){
	        return '1';
	    } else if($value=='Saturday'){
	        return '2';
	    }
	}
}

public function cariHariKerjaNasional($value){

	if (@$this->CariHariLibur(date('Y-m-d'))=='1') {
		return '4';
	}else{
		if ($value=='Sunday') {
	        return '3';
	    } else if($value=='Monday'){
	        return '1';
	    } else if($value=='Tuesday'){
	        return '1';
	    } else if($value=='Wednesday'){
	        return '1';
	    } else if($value=='Thursday'){
	        return '1';
	    } else if($value=='Friday'){
	        return '1';
	    } else if($value=='Saturday'){
	        return '2';
	    }
	}
	    
}

public function carioverTime($value1,$value2){
    return ((strtotime($value2)-strtotime($value1))/3600)-8;
}

function getDefaultNumber($value){
	if ($value=='') {
		return '0';
	} else {
		return $value;
	}
	
}


	public function updateIdAbsen($user,$tanggal){
		$value=md5('L-'.$user.'-'.$tanggal);
		$query= "UPDATE laporan_harian SET id='$value' WHERE username='$user' AND tanggal='$tanggal'";
		$statement = $this->dbHost->prepare($query);
		if ($statement->execute()) {
			
			return 'data berhasil di ubah';
		}else{
			return 'data gagal di ubah';
		}
	}


}


class objekLaporan extends objekPendaftar{
	
	function __construct(){
	}
	var $tglLapor;
	var $hari;
	var $jnsHariKerja;
	var $jamMulai;
	var $jamSelesai;
	var $jamOvertime;
	var $idKlien;
	var $idAbsen;
	var $ketKegiatan;
	var $statusKerja;
	var $peran;
	var $rekan;
	var $jnsTransport;
	var $biayaTransport;
	var $ketTransport;
	var $biayaParkir;
	var $biayaLunch;
	var $biayaDinner;
	var $biayaKesehatan;
	var $biayaAllowance;
	var $biayaLain;
	var $ketBiayaLain;
	var $user;

	public function getUser(){
		return $this->user;
	}

	public function setUser($value){
		$this->user=$value;
	}

	public function getTanggalLapor(){
		return $this->tglLapor;
	}

	public function setTanggalLapor($value){
		$this->tglLapor=$value;
	}	

	public function getHari(){
		return $this->hari;
	}

	public function setHari($value){
		$this->hari=$value;
	}	

	public function getJnsHariKerja(){
		return $this->jnsHariKerja;
	}

	public function setJnsHariKerja($value){
		$this->jnsHariKerja=$value;
	}	

	public function getJamMulai(){
		return $this->jamMulai;
	}

	public function setJamMulai($value){
		$this->jamMulai=$value;
	}

	public function getJamSelesai(){
		return $this->jamSelesai;
	}

	public function setIdAbsen($value){
		$this->idAbsen=$value;
	}

	public function getIdAbsen($value){
		return $this->idAbsen;
	}


	public function setJamSelesai($value){
		$this->jamSelesai=$value;
	}

	public function getJamOvertime(){
		return $this->jamOvertime;
	}

	public function setJamOvertime($value){
		$this->jamOvertime=$value;
	}	

	public function getIdClient(){
		return $this->idKlien;
	}

	public function setIdClient($value){
		$this->idKlien=$value;
	}

	public function getKetKegiatan(){
		return $this->ketKegiatan;
	}

	public function setKetKegiatan($value){
		$this->ketKegiatan=$value;
	}

	public function getStatusKerja(){
		return $this->statusKerja;
	}

	public function setStatusKerja($value){
		$this->statusKerja=$value;
	}

	public function getPeran(){
		return $this->peran;
	}

	public function setPeran($value){
		$this->peran=$value;
	}

	public function getRekan(){
		return $this->rekan;
	}

	public function setRekan($value){
		$this->rekan=$value;
	}

	public function getJnsTransport(){
		return $this->jnsTransport;
	}

	public function setJnsTransport($value){
		$this->jnsTransport=$value;
	}

	public function getBiayaTransport(){
		return $this->biayaTransport;
	}

	public function setBiayaTransport($value){
		$this->biayaTransport=$value;
	}//edit

	public function getKetTransport(){
		return $this->ketTransport;
	}

	public function setKetTransport($value){
		$this->ketTransport=$value;
	}

	public function getBiayaParkir(){
		return $this->biayaParkir;
	}

	public function setBiayaParkir($value){
		$this->biayaParkir=$value;
	}

	public function getBiayaDinner(){
		return $this->biayaDinner;
	}

	public function setBiayaDinner($value){
		$this->biayaDinner=$value;
	}

	public function getBiayaLunch(){
		return $this->biayaLunch;
	}

	public function setBiayaLunch($value){
		$this->biayaLunch=$value;
	}

	public function getBiayaKesehatan(){
		return $this->biayaKesehatan;
	}

	public function setBiayaKesehatan($value){
		$this->biayaKesehatan=$value;
	}

	public function getBiayaLain(){
		return $this->biayaLain;
	}

	public function setBiayaLain($value){
		$this->biayaLain=$value;
	}

	public function getKetBiayaLain(){
		return $this->ketBiayaLain;
	}

	public function setBiayaAllowance($value){
		$this->biayaAllowance=$value;
	}

	public function getBiayaAllowance(){
		return $this->biayaAllowance;
	}

	public function setKetBiayaLain($value){
		$this->ketBiayaLain=$value;
	}

}

?>