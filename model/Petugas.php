<?php 
include_once 'Koneksi.php';
date_default_timezone_set("Asia/Jakarta"); 

class ControlPetugas extends Koneksi{
	var $dbHost;
	private $username;
	private $password;
	private $nama;
	private $absen;
	private $alamat;
	private $telepon;
	private $status;
	private $tipe;
	private $email;
	private $team;
	private $picture;

	public function getAbsen(){
	    return $this->absen;
	}

	public function setAbsen($absen){
	    $this->absen = $absen;
	}

	public function getAlamat(){
	    return $this->alamat;
	}

	public function setAlamat($alamat){
	    $this->alamat = $alamat;
	}

	public function getPicture(){
	    return $this->picture;
	}

	public function setPicture($picture){
	    $this->picture = $picture;
	}

	public function getUsername(){
	    return $this->username;
	}

	public function setUsername($username){
	    $this->username = $username;
	}

	public function getPassword(){
	    return $this->password;
	}

	public function setPassword($password){
	    $this->password = $password;
	}

	public function getNama(){
	    return $this->Nama;
	}

	public function setNama($Nama){
	    $this->nama = $Nama;
	}

	public function getTelepon(){
	    return $this->telepon;
	}

	public function setTelepon($telepon){
	    $this->telepon = $telepon;
	}

	public function getStatus(){
	    return $this->status;
	}

	public function setStatus($status){
	    $this->status = $status;
	}

	public function getTeam(){
		return $this->team;
	}

	public function setTeam($value){
		$this->team = $value;	
	}

	public function getTipe(){
	    return $this->tipe;
	}
	public function setTipe($tipe){
	    $this->tipe = $tipe;
	}

	public function getEmail(){
		return $this->email;
	}

	public function setEmail($value){
		$this->email=$value;
	}

	function __construct(){
		$this->dbHost = $this->bukaKoneksi();
	}
	
	public function GetListCustSupport(){
	    $query = "SELECT username, nama, team, picture
        FROM users
        WHERE users.status='aktif'
        AND users.tipe='admin'
        AND users.team NOT IN ('5','4','3','6')
        AND users.picture IS NOT NULL";
        $statement = $this->dbHost->prepare($query);
		
    	$statement->execute();
    	return $statement->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function GetListMarketing(){
	    $query = "SELECT username, nama, team, picture
        FROM users
        WHERE users.status='aktif'
        AND (users.team='3' OR users.tipe='marketing')
        AND users.picture IS NOT NULL";
        $statement = $this->dbHost->prepare($query);
		
    	$statement->execute();
    	return $statement->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function GetListTrainers(){
	    $query = "SELECT username, nama, team, picture
        FROM users
        WHERE users.status='aktif'
        AND (users.team='4' OR users.team='5')
        AND users.picture IS NOT NULL
        ORDER BY users.created_at ASC";
        $statement = $this->dbHost->prepare($query);
		
    	$statement->execute();
    	return $statement->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function GetBestTrainerClientSideLastMonth($isjan=''){
	    if($isjan=='1'){
	        $queryadd="MONTH(trainer_best.ddate)=MONTH(DATE_SUB(CURDATE(), INTERVAL 1 MONTH)) AND YEAR(trainer_best.ddate)=YEAR(CURDATE(), INTERVAL 1 YEAR)";
	    }else{
	        $queryadd="MONTH(trainer_best.ddate)=MONTH(DATE_SUB(CURDATE(), INTERVAL 1 MONTH)) AND YEAR(trainer_best.ddate)=YEAR(CURDATE())";
	    }
	    $query = "SELECT username,nama,email,DATE_FORMAT(`lahir`,'%a %b %e %Y') AS tanggal_lahir,telepon,
		users_ket_team.ukt_ket AS team,intensif_lvl_trainer.intr_level AS leveluser,picture,
		CASE WHEN status='aktif' THEN 'Aktif' ELSE 'Tidak Aktif' END AS statususer,
        CASE WHEN bestie.tgl IS NULL THEN '0' ELSE '1' END AS besttrainer
		FROM `users` 
		INNER JOIN users_ket_team ON users_ket_team.ukt_id=users.team
		INNER JOIN intensif_lvl_trainer ON intensif_lvl_trainer.intr_id=users.usr_level
        LEFT JOIN (
        	SELECT trainer_best.sfkusername AS user, trainer_best.ddate AS tgl
            FROM trainer_best
            WHERE ".$queryadd."
        ) AS bestie ON bestie.user=users.username
		WHERE bestie.tgl IS NOT NULL
		ORDER BY users.nama ASC";
		$statement = $this->dbHost->prepare($query);
		
    	$statement->execute();
    	return $statement->fetch(PDO::FETCH_ASSOC);
	}
	
	public function GetBestTrainerClientSide(){
	    $query = "SELECT username,nama,email,DATE_FORMAT(`lahir`,'%a %b %e %Y') AS tanggal_lahir,telepon,
		users_ket_team.ukt_ket AS team,intensif_lvl_trainer.intr_level AS leveluser,picture,
		CASE WHEN status='aktif' THEN 'Aktif' ELSE 'Tidak Aktif' END AS statususer,
        CASE WHEN bestie.tgl IS NULL THEN '0' ELSE '1' END AS besttrainer
		FROM `users` 
		INNER JOIN users_ket_team ON users_ket_team.ukt_id=users.team
		INNER JOIN intensif_lvl_trainer ON intensif_lvl_trainer.intr_id=users.usr_level
        LEFT JOIN (
        	SELECT trainer_best.sfkusername AS user, trainer_best.ddate AS tgl
            FROM trainer_best
            WHERE MONTH(trainer_best.ddate)=MONTH(CURDATE()) AND YEAR(trainer_best.ddate)=YEAR(CURDATE())
        ) AS bestie ON bestie.user=users.username
		WHERE bestie.tgl IS NOT NULL
		ORDER BY users.nama ASC";
		$statement = $this->dbHost->prepare($query);
		
    	$statement->execute();
    	return $statement->fetch(PDO::FETCH_ASSOC);
		
	}    
    
    public function EditBestTrainer($username){
        $query = "UPDATE `trainer_best` SET trainer_best.sfkusername='".$username."' WHERE MONTH(trainer_best.ddate)=MONTH(CURDATE()) AND YEAR(trainer_best.ddate)=YEAR(CURDATE())";
        $statement = $this->dbHost->prepare($query);
        
        if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
        
    }
    
    public function InsertBestTrainer($username){
        $query = "INSERT INTO `trainer_best`(`sfkusername`, `ddate`) VALUES ('".$username."',CURDATE())";
        $statement = $this->dbHost->prepare($query);
        
        if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
        
    }
    
    public function GetBestTrainerCurdate(){
        $query = "SELECT * FROM trainer_best WHERE MONTH(trainer_best.ddate)=MONTH(CURDATE()) AND YEAR(trainer_best.ddate)=YEAR(CURDATE())";
        $statement = $this->dbHost->prepare($query);
        $statement->execute();
    	return $statement->fetchAll(PDO::FETCH_ASSOC);
    }    

    public function GetDetailForAPI($username){
        $query = "SELECT users.nama AS nama_staff, users_ket_team.ukt_ket AS team,
        users.picture AS ava,DATE_FORMAT(users.lahir,'%e %M %Y') AS ttl,
        DATE_FORMAT(users.created_at,'%e %M %Y') AS joindate
        FROM `users` 
        INNER JOIN users_ket_team
        ON users_ket_team.ukt_id=users.team
        WHERE users.username=:username";
        $statement = $this->dbHost->prepare($query);
    	$statement->bindParam(':username',$username);
    	$statement->execute();
    	return $statement->fetch(PDO::FETCH_ASSOC);
    }

	public function GetTotalPendingAbsen(){
		$query = "SELECT COUNT(id) AS jumlah 
		FROM absensi 
		WHERE absensi.absen_status='-1' 
		AND (absensi.absen_pulang='' OR absensi.absen_pulang IS NULL)";

		$statement = $this->dbHost->prepare($query);
    	// $statement->bindParam(':namatabel',$namatabel);
    	$statement->execute();
    	$result = $statement->fetch(PDO::FETCH_ASSOC);

    	return $result['jumlah'];
	}

	public function GetReimbursementData($usernamee){
		$query = "SELECT SUM(laporan_harian.biaya_kesehatan) AS med_reim 
		FROM laporan_harian
		INNER JOIN users ON users.username=laporan_harian.username
		WHERE laporan_harian.username=:username
		AND YEAR(laporan_harian.tanggal)=YEAR(CURDATE())";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':username',$usernamee, PDO::PARAM_STR,75);
		$statement->execute();
		return $statement->fetch(PDO::FETCH_ASSOC);
	}

	public function GetUserDetail($usernamee){
		$query = "SELECT username,nama,email,DATE_FORMAT(`lahir`,'%a %b %e %Y') AS tanggal_lahir,telepon,
		users_ket_team.ukt_ket AS team,intensif_lvl_trainer.intr_level AS leveluser,picture,
		CASE WHEN status='aktif' THEN 'Aktif' ELSE 'Tidak Aktif' END AS statususer
		FROM `users` 
		INNER JOIN users_ket_team ON users_ket_team.ukt_id=users.team
		INNER JOIN intensif_lvl_trainer ON intensif_lvl_trainer.intr_id=users.usr_level
		WHERE username=:username";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':username',$usernamee, PDO::PARAM_STR,75);
		$statement->execute();
		return $statement->fetch(PDO::FETCH_ASSOC);
	}

	public function GetListUser(){
		$query = "SELECT username,nama,email,DATE_FORMAT(`lahir`,'%a %b %e %Y') AS tanggal_lahir,telepon,
		users_ket_team.ukt_ket AS team,intensif_lvl_trainer.intr_level AS leveluser,picture,
		CASE WHEN status='aktif' THEN 'Aktif' ELSE 'Tidak Aktif' END AS statususer,
        CASE WHEN bestie.tgl IS NULL THEN '0' ELSE '1' END AS besttrainer
		FROM `users` 
		INNER JOIN users_ket_team ON users_ket_team.ukt_id=users.team
		INNER JOIN intensif_lvl_trainer ON intensif_lvl_trainer.intr_id=users.usr_level
        LEFT JOIN (
        	SELECT trainer_best.sfkusername AS user, trainer_best.ddate AS tgl
            FROM trainer_best
            WHERE MONTH(trainer_best.ddate)=MONTH(CURDATE()) AND YEAR(trainer_best.ddate)=YEAR(CURDATE())
        ) AS bestie ON bestie.user=users.username
		WHERE users.status='aktif' 
		ORDER BY users.nama ASC";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function GetSumLaporan($kolom){
		$query = "SELECT SUM($kolom) AS `jumlah` FROM `laporan_harian` WHERE `laporan_harian`.`username`=:username";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':username',$this->username, PDO::PARAM_STR,75);
		$statement->execute();
		$hasil = $statement->fetch(PDO::FETCH_OBJ);
		return @$hasil->jumlah;
	}

	public function GetSumLaporanByDate($kolom,$dt,$kt){
		$query = "SELECT SUM($kolom) AS `jumlah` 
		FROM `laporan_harian` 
		WHERE `laporan_harian`.`username`=:username 
		AND `laporan_harian`.`tanggal` BETWEEN '$dt' AND '$kt';";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':username',$this->username, PDO::PARAM_STR,75);
		$statement->execute();
		$hasil = $statement->fetch(PDO::FETCH_OBJ);
		return @$hasil->jumlah;
	}

	public function LaporanKegiatanStaff(){
		$query = "SELECT `lkk_id`,`lkk_ket`, 
		(
			SELECT COUNT(`id`) 
 			FROM `laporan_harian` 
 			WHERE FIND_IN_SET(`lkk_id`,`kegiatan`)
 			AND `laporan_harian`.`username`= :username
		) AS `jumlah`
		FROM `lap_ket_kegiatan`;";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':username',$this->username, PDO::PARAM_STR,75);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);

	}

	public function LaporanKegiatanStaffByDate($fd,$td){
		$query = "SELECT `lkk_id`,`lkk_ket`, 
		(
			SELECT COUNT(`id`) 
 			FROM `laporan_harian` 
 			WHERE FIND_IN_SET(`lkk_id`,`kegiatan`)
 			AND `laporan_harian`.`username`= :username
            AND `laporan_harian`.`tanggal` BETWEEN '$fd' AND '$td'
		) AS `jumlah`
		FROM `lap_ket_kegiatan`;";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':username',$this->username, PDO::PARAM_STR,75);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);

	}

	public function LaporanJenisTransportasi(){
		$query = "SELECT `lt_id`, `lt_ket`,(
			SELECT COUNT(laporan_harian.id) 
    		FROM laporan_harian
    		WHERE laporan_harian.username=:username
    		AND laporan_harian.jns_transportasi=lt_id
		) AS jumlah
		FROM `daftar_alat_trans`;";	
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':username',$this->username, PDO::PARAM_STR,75);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function LaporanJenisTransportasiByDate($fd,$td){
		$query = "SELECT `lt_id`, `lt_ket`,(
			SELECT COUNT(laporan_harian.id) 
    		FROM laporan_harian
    		WHERE laporan_harian.username=:username
    		AND laporan_harian.jns_transportasi=lt_id
    		AND `laporan_harian`.`tanggal` BETWEEN '$fd' AND '$td'
		) AS jumlah
		FROM `daftar_alat_trans`;";	
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':username',$this->username, PDO::PARAM_STR,75);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}


	public function MarketingEmailSystem($id_usaha,$tanggal){
		$query = "SELECT perusahaan,nama_cust,marketing_email AS email,petugas, marketing_nama
		FROM `acara_harian`
		INNER JOIN `new_transaksi` ON `new_transaksi`.`trans_id` = `acara_harian`.`transaksi`
		INNER JOIN `new_customer` ON `new_customer`.`id_cust`= `new_transaksi`.`trans_cust_id`
		INNER JOIN `new_marketing` ON `new_marketing`.`marketing_id` = `new_customer`.`marketing_id`
		WHERE `acara_harian`.`id` = :idusaha AND `acara_harian`.`tanggal` = :tanggal";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':idusaha',$id_usaha, PDO::PARAM_STR,75);
		$statement->bindParam(':tanggal',$tanggal, PDO::PARAM_STR,75);
		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}


	public function LaporanJenisTransaksi()	{ // 134
		$query = "SELECT `jt_id`, `jt_ket`,
		(
			SELECT COUNT(laporan_harian.id) AS jumlah
			FROM laporan_harian
			INNER JOIN users ON users.username=laporan_harian.username
			INNER JOIN absensi ON absensi.id=laporan_harian.id_absen
			INNER JOIN (
				SELECT fac_calendar.tanggal AS tanggal,
    			perusahaan.id AS id,
    			perusahaan.nama AS nama_usaha, new_transaksi.trans_jenis AS trans_jenis 
    			FROM `fac_calendar` 
				INNER JOIN new_transaksi ON `new_transaksi`.`trans_id`=fac_calendar.acara
				INNER JOIN perusahaan ON perusahaan.id=new_transaksi.trans_id_usaha
			) AS acaraharian  
			ON acaraharian.tanggal=laporan_harian.tanggal AND absensi.nama_perusahaan=acaraharian.id
			INNER JOIN new_jenis_transaksi 
			ON new_jenis_transaksi.jt_id=acaraharian.trans_jenis
			WHERE laporan_harian.username=:nama AND new_jenis_transaksi.jt_id= A.jt_id
		) AS jumlah FROM `new_jenis_transaksi` AS A
		GROUP BY A.jt_id;";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':nama',$this->username, PDO::PARAM_STR,75);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function LaporanJenisTransaksiByDate($dt,$st){
		$query = "SELECT `jt_id`, `jt_ket`,
		(
			SELECT COUNT(laporan_harian.id) AS jumlah
			FROM laporan_harian
			INNER JOIN users ON users.username=laporan_harian.username
			INNER JOIN absensi ON absensi.id=laporan_harian.id_absen
			INNER JOIN (
				SELECT fac_calendar.tanggal AS tanggal,
    			perusahaan.id AS id,
    			perusahaan.nama AS nama_usaha, new_transaksi.trans_jenis AS trans_jenis 
    			FROM `fac_calendar` 
				INNER JOIN new_transaksi ON `new_transaksi`.`trans_id`=fac_calendar.acara
				INNER JOIN perusahaan ON perusahaan.id=new_transaksi.trans_id_usaha
			) AS acaraharian  
			ON acaraharian.tanggal=laporan_harian.tanggal AND absensi.nama_perusahaan=acaraharian.id
			INNER JOIN new_jenis_transaksi 
			ON new_jenis_transaksi.jt_id=acaraharian.trans_jenis
			WHERE laporan_harian.username=:nama 
			AND new_jenis_transaksi.jt_id= A.jt_id
			AND laporan_harian.tanggal BETWEEN '$dt' AND '$st'
		) AS jumlah FROM `new_jenis_transaksi` AS A
		GROUP BY A.jt_id;";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':nama',$this->username, PDO::PARAM_STR,75);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function LaporanKegiatan(){
		$query = "SELECT `lkk_id`, `lkk_ket`,
		(SELECT COUNT(`id`) FROM `laporan_harian` WHERE FIND_IN_SET(`lkk_id`,`laporan_harian`.`kegiatan`)) AS `jumlah`
		FROM `lap_ket_kegiatan`;";
		$statement=$this->dbHost->prepare($query);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function NotifDasbor($username){
		$query = "SELECT `nd_content`, `nd_status` FROM `notif_dasbor` WHERE `nd_username`=:usernamez";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':usernamez',$username, PDO::PARAM_STR,75);
		
		$statement->execute();
		return $statement->fetch(PDO::FETCH_ASSOC);
	}

	public function LaporanAlatTransCurrentMonth($idtransport){
		$query = "SELECT users.nama AS nama_staff, COUNT(laporan_harian.id) AS jumlah
		FROM laporan_harian 
		INNER JOIN users 
		ON users.username=laporan_harian.username
		WHERE MONTH(laporan_harian.tanggal) = MONTH(CURDATE())
		AND YEAR(laporan_harian.tanggal) = YEAR(CURDATE())
		AND laporan_harian.jns_transportasi = '$idtransport'
		AND laporan_harian.username <> ''
		AND (users.team='4' OR users.team='5')
		GROUP BY users.username";
		$statement=$this->dbHost->prepare($query);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	}	

	public function LaporanStatusKerjaCurrentMonth($idstat){
		$query = "SELECT users.nama AS nama_staff, COUNT(laporan_harian.id) AS jumlah
		FROM laporan_harian 
		INNER JOIN users 
		ON users.username=laporan_harian.username
		WHERE MONTH(laporan_harian.tanggal) = MONTH(CURDATE())
		AND YEAR(laporan_harian.tanggal) = YEAR(CURDATE())
		AND laporan_harian.status_kerja = '$idstat'
		AND laporan_harian.username <> ''
		AND (users.team='4' OR users.team='5')
		GROUP BY users.username";
		$statement=$this->dbHost->prepare($query);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	}

	public function LaporanPeranbyCurrentMonth($idperan,$onlytrainer='1'){
		$query = "SELECT users.nama AS nama_staff, COUNT(laporan_harian.id) AS jumlah
		FROM laporan_harian 
		INNER JOIN users 
		ON users.username=laporan_harian.username
		WHERE MONTH(laporan_harian.tanggal) = MONTH(CURDATE())
		AND YEAR(laporan_harian.tanggal) = YEAR(CURDATE())
		AND laporan_harian.peran = '$idperan'
		AND laporan_harian.username <> ''
		AND (users.team='4' OR users.team='5')
		GROUP BY users.username";
		$statement=$this->dbHost->prepare($query);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	}

	public function LaporanPeranBulanini(){
		$query = "SELECT `A`.`username` AS `aidi`,`users`.`nama` AS `nama`, 
		IFNULL(utama.jumlah,'0') AS jumlah_utama, IFNULL(pendamping.jumlah,'0') AS jumlah_pendamping,
		IFNULL(peninjau.jumlah,'0') AS jumlah_peninjau
    FROM `laporan_harian` AS A 
		INNER JOIN `users` ON `users`.`username`=`A`.`username`
        LEFT JOIN (
    		SELECT `username` AS user,COUNT(id) AS `jumlah` FROM `laporan_harian`
    		WHERE `laporan_harian`.`peran`='1'
            AND MONTH(laporan_harian.tanggal) = MONTH(CURDATE())
			AND YEAR(laporan_harian.tanggal) = YEAR(CURDATE())
			GROUP BY username
   		) AS `utama` ON `utama`.`user`= A.username
        LEFT JOIN (
            SELECT `username` AS user,COUNT(id) AS `jumlah` FROM `laporan_harian`
    		WHERE `laporan_harian`.`peran`='2'
            AND MONTH(laporan_harian.tanggal) = MONTH(CURDATE())
			AND YEAR(laporan_harian.tanggal) = YEAR(CURDATE())
			GROUP BY username
        ) AS pendamping ON pendamping.user=A.username
        LEFT JOIN (
        	SELECT `username` AS user,COUNT(id) AS `jumlah` FROM `laporan_harian`
    		WHERE `laporan_harian`.`peran`='3'
            AND MONTH(laporan_harian.tanggal) = MONTH(CURDATE())
			AND YEAR(laporan_harian.tanggal) = YEAR(CURDATE())
			GROUP BY username
        ) AS peninjau ON peninjau.user=A.username
		WHERE NOT `A`.`username` = ''
        AND users.status='aktif'
        AND (users.team='4' OR users.team='5')
		GROUP BY `A`.`username`";
		$statement=$this->dbHost->prepare($query);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function LaporanPeran(){
		$query = "SELECT `A`.`username` AS `aidi`,`users`.`nama` AS `nama`, 
		IFNULL(utama.jumlah,'0') AS jumlah_utama, IFNULL(pendamping.jumlah,'0') AS jumlah_pendamping,
		IFNULL(peninjau.jumlah,'0') AS jumlah_peninjau
    FROM `laporan_harian` AS A 
		INNER JOIN `users` ON `users`.`username`=`A`.`username`
        LEFT JOIN (
    		SELECT `username` AS user,COUNT(id) AS `jumlah` FROM `laporan_harian`
    		WHERE `laporan_harian`.`peran`='1'
			GROUP BY username
   		) AS `utama` ON `utama`.`user`= A.username
        LEFT JOIN (
            SELECT `username` AS user,COUNT(id) AS `jumlah` FROM `laporan_harian`
    		WHERE `laporan_harian`.`peran`='2'
			GROUP BY username
        ) AS pendamping ON pendamping.user=A.username
        LEFT JOIN (
        	SELECT `username` AS user,COUNT(id) AS `jumlah` FROM `laporan_harian`
    		WHERE `laporan_harian`.`peran`='3'
			GROUP BY username
        ) AS peninjau ON peninjau.user=A.username
		WHERE NOT `A`.`username` = ''
		GROUP BY `A`.`username`;";
		$statement=$this->dbHost->prepare($query);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function LaporanKegiatanByDate($fd,$td){
		$query = "SELECT `lkk_id`, `lkk_ket`,
		(SELECT COUNT(`id`) FROM `laporan_harian` WHERE FIND_IN_SET(`lkk_id`,`laporan_harian`.`kegiatan`) AND `laporan_harian`.`tanggal` BETWEEN :tglAwal AND :tglAkhir) AS `jumlah`
		FROM `lap_ket_kegiatan`";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':tglAwal',$fd, PDO::PARAM_STR,15);
		$statement->bindParam(':tglAkhir',$td, PDO::PARAM_STR,15);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function LaporanPeranByDate($fd,$td){
		$query = "SELECT `A`.`username` AS `aidi`,`users`.`nama` AS `nama`, 
		IFNULL(utama.jumlah,'0') AS jumlah_utama, 
		IFNULL(pendamping.jumlah,'0') AS jumlah_pendamping,
		IFNULL(peninjau.jumlah,'0') AS jumlah_peninjau
    	FROM `laporan_harian` AS A 
		INNER JOIN `users` ON `users`.`username`=`A`.`username`
        LEFT JOIN (
    		SELECT `username` AS user,COUNT(id) AS `jumlah` FROM `laporan_harian`
    		WHERE `laporan_harian`.`peran`='1'
            AND `laporan_harian`.`tanggal` BETWEEN :tglAwal AND :tglAkhir
			GROUP BY username
   		) AS `utama` ON `utama`.`user`= A.username
        LEFT JOIN (
            SELECT `username` AS user,COUNT(id) AS `jumlah` FROM `laporan_harian`
    		WHERE `laporan_harian`.`peran`='2'
            AND `laporan_harian`.`tanggal` BETWEEN :fd AND :td
			GROUP BY username
        ) AS pendamping ON pendamping.user=A.username
        LEFT JOIN (
        	SELECT `username` AS user,COUNT(id) AS `jumlah` FROM `laporan_harian`
    		WHERE `laporan_harian`.`peran`='3'
            AND `laporan_harian`.`tanggal` BETWEEN :dt AND :st
			GROUP BY username
        ) AS peninjau ON peninjau.user=A.username
		WHERE NOT `A`.`username` = ''
		GROUP BY `A`.`username`;";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':tglAwal',$fd, PDO::PARAM_STR,15);
		$statement->bindParam(':tglAkhir',$td, PDO::PARAM_STR,15);
		$statement->bindParam(':fd',$fd, PDO::PARAM_STR,15);
		$statement->bindParam(':td',$td, PDO::PARAM_STR,15);
		$statement->bindParam(':dt',$fd, PDO::PARAM_STR,15);
		$statement->bindParam(':st',$td, PDO::PARAM_STR,15);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function GetBranchName(){
		$query = "SELECT `id_cabang`,`nama` 
		FROM `cabang`,`perusahaan` 
		WHERE `perusahaan`.`id`=`cabang`.`id_perusahaan`;";
		$statement=$this->dbHost->prepare($query);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}


	public function CountBelumAbsen(){
		$query = "SELECT COUNT(`id`) AS `jumlah` 
		FROM `absensi` 
		WHERE `absen_pulang` IS NULL 
		AND `username`=:username
		OR `absen_pulang` =''
		AND `username`=:username1";	
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':username',$this->username, PDO::PARAM_STR,75);
		$statement->bindParam(':username1',$this->username, PDO::PARAM_STR,75);
		$statement->execute();
		$hasil = $statement->fetch(PDO::FETCH_OBJ);
		return @$hasil->jumlah;
	}


	public function DataBelumAbsenPulang(){
		$query = "SELECT `tanggal_absen`,`absensi`.`id` AS `id`,`nama` AS `nama_usaha` 
		FROM `absensi` 
		LEFT JOIN `perusahaan` ON `perusahaan`.`id`=`absensi`.`nama_perusahaan`
		WHERE `absen_pulang` IS NULL 
		AND `username`=:username
		OR `absen_pulang` =''
		AND `username`=:username1
		ORDER BY `absensi`.`tanggal_absen` DESC;";
		$statement = $this->dbHost->prepare($query);
		
		$statement->bindParam(':username',$this->username, PDO::PARAM_STR,75);
		$statement->bindParam(':username1',$this->username, PDO::PARAM_STR,75);
		
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function countSubscriber($date){
		$query = "SELECT COUNT(`time_of_subcription`) AS jumlah FROM `subscribers` WHERE `time_of_subcription` BETWEEN '$date 00:00:00' AND '$date 23:59:59'";
		$statement = $this->dbHost->prepare($query);
		// $statement->execute();

		$statement->execute();
		$hasil = $statement->fetch(PDO::FETCH_OBJ);
		// $hasil = json_decode($dataz);
		return $hasil->jumlah;

	}


	public function getAllTeleponUser(){
		$query="SELECT `no`, `source`, `no_telepon`, `status_telp` FROM `telepon` WHERE `source` = :user AND status_telp='1'";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':user',$this->getUsername(), PDO::PARAM_STR,75);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function getAllAlamatUser(){
		$query="SELECT * FROM `alamat` WHERE `source`=:user AND status_alamat='1'";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':user',$this->getUsername(), PDO::PARAM_STR,75);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function insertAlamat(){
		$query="INSERT INTO `alamat` (`source`, `alamat`) 
		VALUES (:username, :alamat);";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':username',$this->getUsername(), PDO::PARAM_STR,35);
		@$statement->bindParam(':alamat',$this->getAlamat(), PDO::PARAM_STR,750);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function InputLaporanFolMarketing($objek){
		$query="INSERT INTO `laporan_followup` (`tanggal`, `customer`, `agenda`, `status`, `jumlah_hari`, `deskripsi_hari`, `petugas`) 
		VALUES (:tanggal, :customer, :agenda, :status, :jumlahhari, :deskripsihari, :petugas);";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':tanggal',$objek->tanggal, PDO::PARAM_STR,15);
		@$statement->bindParam(':customer',$objek->customer, PDO::PARAM_STR,75);
		@$statement->bindParam(':agenda',$objek->agenda, PDO::PARAM_STR,500);
		@$statement->bindParam(':status',$objek->status, PDO::PARAM_INT,1);
		@$statement->bindParam(':jumlahhari',$objek->jumlahhari, PDO::PARAM_INT,3);
		@$statement->bindParam(':deskripsihari',$objek->deskripsihari, PDO::PARAM_STR,255);
		@$statement->bindParam(':petugas',$objek->petugas, PDO::PARAM_STR,75);	

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}

	}

	public function fetchAllFollowUp(){
		$query="SELECT `laporan_followup`.`num` AS `num`,`tanggal`, `customer`, `agenda`, `laporan_followup`.`status` AS `status`, `jumlah_hari`, `deskripsi_hari`,`nama`,`keterangan`, `laporan_followup`.`date_created` AS `date_created`
		FROM `laporan_followup`
		INNER JOIN `users`
		ON `users`.`username`=`laporan_followup`.`petugas`
		INNER JOIN `status_followup`
		ON `status_followup`.`num`=`laporan_followup`.`status` 
        WHERE `laporan_followup`.`status` != '3'
        ORDER BY `laporan_followup`.`tanggal` DESC;";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':bulan',date('m'), PDO::PARAM_STR,15);
		@$statement->bindParam(':tahun',date('Y'), PDO::PARAM_STR,15);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function fetchLaporanFuByID($aidi){
		$query="SELECT `num`, `tanggal`, `customer`, `agenda`, `status`, `jumlah_hari`, `deskripsi_hari`, `petugas` FROM `laporan_followup` WHERE `num`=:id";

		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':id',$aidi, PDO::PARAM_INT,3);

		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);

	}

	public function editFollowUp($objek){
		$query="UPDATE `laporan_followup` SET `tanggal`=:tanggal,`customer`=:customer,`agenda`=:agenda,`status`=:status,`jumlah_hari`=:jumlah,`deskripsi_hari`=:deskripsi WHERE `num`=:id;";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':id',$objek->id, PDO::PARAM_INT,3);	
		@$statement->bindParam(':tanggal',$objek->tanggal, PDO::PARAM_STR,15);
		@$statement->bindParam(':customer',$objek->customer, PDO::PARAM_STR,75);
		@$statement->bindParam(':agenda',$objek->agenda, PDO::PARAM_STR,500);
		@$statement->bindParam(':status',$objek->status, PDO::PARAM_INT,1);
		@$statement->bindParam(':jumlah',$objek->jumlahhari, PDO::PARAM_INT,3);
		@$statement->bindParam(':deskripsi',$objek->deskripsihari, PDO::PARAM_STR,255);	
		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
	}

	public function CountFollowUPThisMonth(){
		$query="SELECT COUNT(`customer`) AS `jumlah` FROM `laporan_followup` WHERE MONTH(`tanggal`)=:bulan AND YEAR(`tanggal`)=:tahun;";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':bulan',date('m'), PDO::PARAM_STR,15);
		@$statement->bindParam(':tahun',date('Y'), PDO::PARAM_STR,15);
		$statement->execute();
		$data = $statement->fetch(PDO::FETCH_OBJ);
		return @$data->jumlah;

	}

	public function editAlamat(){
		$data = $this->getAlamat();
		$query="UPDATE `alamat` 
		SET `alamat` = :alamat 
		WHERE `alamat`.`no` = :no;";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':no',$data['id'], PDO::PARAM_INT,4);
		@$statement->bindParam(':alamat',$data['alamat'], PDO::PARAM_STR,750);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
	}

	public function editStatusAlamat(){
		$data = $this->getAlamat();
		$query="UPDATE `alamat` 
		SET `status_alamat` = :status 
		WHERE `alamat`.`no` = :no;";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':no',$data['id'], PDO::PARAM_INT,4);
		@$statement->bindParam(':status',$data['status'], PDO::PARAM_STR,750);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
	}

	public function insertTelepon(){
		$query="INSERT INTO `telepon` (`source`, `no_telepon`) 
		VALUES ( :username, :telepon);";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':username',$this->getUsername(), PDO::PARAM_STR,35);
		@$statement->bindParam(':telepon',$this->getTelepon(), PDO::PARAM_STR,25);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}

	}

	public function editStatusTelp(){
		$data=$this->getTelepon();
		$query="UPDATE `telepon` 
		SET `status_telp` = :status 
		WHERE `telepon`.`no` = :no;";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':status',$data['status'], PDO::PARAM_STR,25);
		@$statement->bindParam(':no',$data['id'], PDO::PARAM_INT,4);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function editNoTelepon(){
		$data=$this->getTelepon();
		$query="UPDATE `telepon` 
		SET `no_telepon` = :notelp 
		WHERE `telepon`.`no` = :id;";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':notelp',$data['telp'], PDO::PARAM_STR,25);
		@$statement->bindParam(':id',$data['id'], PDO::PARAM_INT,4);

		if ($statement->execute()) {
			//$dbHost = null;
			return true;
		}else{
			return false;
		}
	}

	public function getEmailPetugas(){
		$tipe = $this->getTipe();
		if ($tipe=='1'){
			$query = "SELECT `email` FROM `users` WHERE `status`='aktif' AND `team`='b' OR `status`='aktif' AND `team`='c';";
		} elseif($tipe=='2'||$tipe=='3'||$tipe=='5'||$tipe=='6'){
			$query = "SELECT `email` FROM `users` WHERE `status`='aktif' AND `tipe`='admin';";
		}
		$statement=$this->dbHost->prepare($query);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);

	}

	public function FetchSubsribers(){
		$query = "SELECT * FROM `subscribers`;";
		$statement=$this->dbHost->prepare($query);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);

	}

	public function getMapData($value){
		$namaUsaha = '%'.$value.'%';
		$query="SELECT `tanggal_absen`, `username`,`nama`, `lokasi_absen_masuk`, `nama_perusahaan`, `lokasi_absen_pulang` FROM `absensi_harian_view` WHERE `nama_perusahaan` LIKE :namaUsaha";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':namaUsaha',$namaUsaha, PDO::PARAM_STR,125);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function fetchJabatan(){
		$query = "SELECT * FROM `jabatan`;";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function insertNewStaff($arr){
		$query="INSERT INTO `users` (`username`, `password`, `nama`, `email`, `lahir`, `telepon`, `status`, `tipe`, `team`) 
		VALUES ('".$arr->username."', '".$arr->password."', '".$arr->fullName."', '".$arr->email."', '".$arr->dob."', '".$arr->telepon."', '".$arr->status."', '".$arr->tipe."', '".$arr->team."')";
		$statement=$this->dbHost->prepare($query);

		if ($statement->execute()) {
			$msg="There was an account added by ".$arr->petugas.", please check it by visiting http://www.fac-institute.com/administrasi/new-users.php";
			mail("rifzky.mail@gmail.com, admin@fac-institute.com, fajarfifa@gmail.com,fajar@fac-institute.com", "New Account Confirmation", $msg);
			return "<script>alert('Password berhasil di ubah!')</script>";
		}else{
			return "<script>alert('Password gagal di ubah!')</script>";
		}
	}


	public function marketingSumInvoice($data){
		if (isset($data['tglAwal'])&&isset($data['tglAkhir'])&&!empty($data['tglAwal'])&&!empty($data['tglAkhir'])) {
			$query="SELECT COUNT(pendaftar.id) AS jumlah FROM `pendaftar`,`invoice` WHERE invoice.id_pendaftar=pendaftar.id AND invoice.status_invoice=2 AND invoice.time_created BETWEEN '".$data['tglAwal']." 00:00:00' AND '".$data['tglAkhir']." 23:59:59'";
		} else {
			$query="SELECT COUNT(pendaftar.id) AS jumlah FROM `pendaftar`,`invoice` WHERE invoice.id_pendaftar=pendaftar.id AND invoice.status_invoice=2";
		}
		
						
		$statement=$this->dbHost->prepare($query);
		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function unreportedData($username){
		$query="SELECT COUNT(id) AS jumlah FROM `laporan_harian` WHERE `ket_kegiatan` IS NULL AND `username`='$username'";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function DataBelumDilaporkan(){
		$query = "SELECT `laporan_harian`.`id` AS `id`,`tanggal`,`perusahaan`.`nama` AS `nama_usaha`, `users`.`nama` AS `petugas` FROM `laporan_harian` 
		INNER JOIN `absensi` ON `absensi`.`id` = `laporan_harian`.`id_absen`
		INNER JOIN `users` ON `users`.`username`=`absensi`.`username`
		INNER JOIN `perusahaan` ON `perusahaan`.`id`=`absensi`.`nama_perusahaan`
		WHERE `ket_kegiatan` IS NULL AND `laporan_harian`.`username`=:username
		ORDER BY laporan_harian.tanggal ASC;";
		$statement = $this->dbHost->prepare($query);
		
		$statement->bindParam(':username',$this->username, PDO::PARAM_STR,75);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function totalOmset($value){
		$hasil=0;
		if ($value!='') {
			$query="SELECT no_invoice,harga,paket_hari,qty,qty_trans,biaya_trans,status_invoice FROM `paket_training`,`pendaftar`,`invoice` WHERE invoice.id_pendaftar=pendaftar.id AND pendaftar.id_paket=paket_training.id AND invoice.status_invoice>0 AND invoice.time_created BETWEEN '".$value['tglAwal']." 00:00:00' AND '".$value['tglAkhir']." 23:59:59'";
		} else {
			$query="SELECT no_invoice,harga,paket_hari,qty,qty_trans,biaya_trans,status_invoice FROM `paket_training`,`pendaftar`,`invoice` WHERE invoice.id_pendaftar=pendaftar.id AND pendaftar.id_paket=paket_training.id AND invoice.status_invoice>0 AND invoice.time_created BETWEEN '".date('Y-m')."-01 00:00:00' AND '".date('Y-m-d')." 23:59:59'";
		}
		
		

		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		$dataz=json_decode($dataz);

		for ($i=0; $i < count($dataz); $i++){ 
			$jml1 = intval($dataz[$i]->harga)*intval($dataz[$i]->qty);
			$jml2 = intval($dataz[$i]->qty_trans)*intval($dataz[$i]->biaya_trans);
			$total = intval($jml1)+intval($jml2);
			$hasil = $hasil + $total;
		}

		return $hasil;
	}

	public function totalOmsetPerTahun($value){
		$hasil=0;
		$query="SELECT no_invoice,harga,paket_hari,qty,qty_trans,biaya_trans,status_invoice FROM `paket_training`,`pendaftar`,`invoice` WHERE invoice.id_pendaftar=pendaftar.id AND pendaftar.id_paket=paket_training.id AND invoice.status_invoice>0 AND YEAR(invoice.time_created) = '$value'";

		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		$dataz=json_decode($dataz);

		for ($i=0; $i < count($dataz); $i++){ 
			$jml1 = intval($dataz[$i]->harga)*intval($dataz[$i]->qty);
			$jml2 = intval($dataz[$i]->qty_trans)*intval($dataz[$i]->biaya_trans);
			$total = intval($jml1)+intval($jml2);
			$hasil = $hasil + $total;
		}

		return $hasil;
	}

	public function totalBiayaInvoiceTerbayar($data){
		$hasil=0;
		if (isset($data['tglAwal'])&&isset($data['tglAkhir'])&&!empty($data['tglAwal'])&&!empty($data['tglAkhir'])) {
			$query="SELECT harga,paket_hari,qty,qty_trans,biaya_trans FROM `paket_training`,`pendaftar`,`invoice` WHERE invoice.id_pendaftar=pendaftar.id AND pendaftar.id_paket=paket_training.id AND invoice.status_invoice=2 AND invoice.time_created BETWEEN '".$data['tglAwal']." 00:00:00' AND '".$data['tglAkhir']." 23:59:59'";
		}elseif(isset($data['t'])&&$data['t']=='omset'&&isset($data['tglAwal'])&&isset($data['tglAkhir'])&&!empty($data['tglAwal'])&&!empty($data['tglAkhir'])){
			$query = "SELECT no_invoice,harga,paket_hari,qty,qty_trans,biaya_trans,status_invoice FROM `paket_training`,`pendaftar`,`invoice` WHERE invoice.id_pendaftar=pendaftar.id AND pendaftar.id_paket=paket_training.id AND invoice.status_invoice>0 AND invoice.time_created BETWEEN '2017-01-01 00:00:00' AND '2017-01-09 23:59:59'";
		}else {
			$query="SELECT harga,paket_hari,qty,qty_trans,biaya_trans FROM `paket_training`,`pendaftar`,`invoice` WHERE invoice.id_pendaftar=pendaftar.id AND pendaftar.id_paket=paket_training.id AND invoice.status_invoice=2";
		}
		
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		$dataz=json_decode($dataz);

		for ($i=0; $i < count($dataz); $i++){ 
			$jml1 = intval($dataz[$i]->harga)*intval($dataz[$i]->qty);
			$jml2 = intval($dataz[$i]->qty_trans)*intval($dataz[$i]->biaya_trans);
			$total = intval($jml1)+intval($jml2);
			$hasil = $hasil + $total;
		}

		return $hasil;
	}

	public function paidInvoiceInYear($tahun){
		$hasil=0;
		$query="SELECT harga,paket_hari,qty,qty_trans,biaya_trans,harga*qty + qty_trans * biaya_trans AS jumlah FROM `paket_training`,`pendaftar`,`invoice` WHERE invoice.id_pendaftar=pendaftar.id AND pendaftar.id_paket=paket_training.id AND invoice.status_invoice=2 AND YEAR(invoice.time_created)='$tahun'";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		$dataz=json_decode($dataz);

		for ($i=0; $i < count($dataz); $i++){ 
			// $jml1 = intval($dataz[$i]->harga)*intval($dataz[$i]->paket_hari)*intval($dataz[$i]->qty);
			// $jml1 = intval($dataz[$i]->harga)*intval($dataz[$i]->qty);
			// $jml2 = intval($dataz[$i]->qty_trans)*intval($dataz[$i]->biaya_trans);
			// $total = intval($jml1)+intval($jml2);
			$hasil = $hasil + $dataz[$i]->jumlah;
		}

		return $hasil;
	}

	public function deleteRequestedAccount($user){
		$prevData = $this->selectByID($user);
		if ($prevData->status=='waiting') {
			$query = "DELETE FROM users WHERE username=:username";
			$statement=$this->dbHost->prepare($query);
			$statement->bindParam(':username',$user, PDO::PARAM_STR,75);
			$statement->execute();
			return "<script>alert('Data berhasil dihapus!')</script>";
		}else{
			return "<script>alert('Maaf anda tidak bisa menghapus data ini!')</script>";
		}
	}

	public function confirmRequestedAccount($user){
		$prevData = $this->selectByID($user);
		$query = "UPDATE users SET password=:pass, status='pending' WHERE username=:username";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':username',$user, PDO::PARAM_STR,75);
		@$statement->bindParam(':pass',@md5($prevData->password), PDO::PARAM_STR,75);
		$statement->execute();
		$pesan="Anda telah di konfirmasi di sistem FAC-Institute, silahkan klik link http://www.fac-institute.com/login/register/validasi.php?kode=".$prevData->username."&t=".@md5($prevData->username.'-'.date('Y-m-d')).", dan login dengan:\n\n username:\t ".$prevData->username."\npassword:\t ".$prevData->password;
		if(mail($prevData->email, "Konfirmasi Akun FAC-Institute", $pesan)){
			return "<script>alert('Akun baru telah di kirim email untuk aktifasi login!')</script>";
		}
	}

	public function fetchAllProfile(){
		$query = "SELECT `username`, `password`, `nama`, `email`, `lahir`, `telepon`, `status`, `tipe`, `nama_jabatan`, GROUP_CONCAT(alamat.alamat SEPARATOR ' # ') AS `alamat`,`picture` 
		FROM `users`,`jabatan`,`alamat` 
		WHERE `users`.`team`=`jabatan`.`no` AND `users`.`username`=`alamat`.`source` AND `users`.`username`=:username;";
		$statement = $this->dbHost->prepare($query);
		@$statement->bindParam(':username',$this->getUsername(), PDO::PARAM_STR,75);
		
		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function getDataWaiting(){
		$query= "SELECT * FROM users WHERE status='waiting'";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function DetailLaporanJabodetabekByDate($fd,$td){
		$query = "SELECT nama,tanggal_absen,absen_masuk
		FROM `absensi` 
        LEFT JOIN perusahaan ON perusahaan.id=absensi.nama_perusahaan
		WHERE `absensi`.`tanggal_absen` BETWEEN '$fd' AND '$td'
		AND `absensi`.`username`= :username
		AND `absensi`.`nama_perusahaan` IN (
			SELECT `perusahaan`.`id`
		FROM `perusahaan` 
		WHERE `perusahaan`.`kota` LIKE '%bogor%' OR `perusahaan`.`kota` LIKE '%jakarta%' OR `perusahaan`.`kota` LIKE '%depok%' OR `perusahaan`.`kota` LIKE '%tangerang%' OR `perusahaan`.`kota` LIKE '%bekasi%' AND `perusahaan`.`kota` NOT LIKE '-'
		)";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':username',$this->username, PDO::PARAM_STR,75);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function DetailLaporanJabodetabek(){
		$query = "SELECT nama,tanggal_absen,absen_masuk
		FROM `absensi` 
        LEFT JOIN perusahaan ON perusahaan.id=absensi.nama_perusahaan
		WHERE `absensi`.`username`= :username
		AND `absensi`.`nama_perusahaan` IN (
			SELECT `perusahaan`.`id`
		FROM `perusahaan` 
		WHERE `perusahaan`.`kota` LIKE '%bogor%' OR `perusahaan`.`kota` LIKE '%jakarta%' OR `perusahaan`.`kota` LIKE '%depok%' OR `perusahaan`.`kota` LIKE '%tangerang%' OR `perusahaan`.`kota` LIKE '%bekasi%' AND `perusahaan`.`kota` NOT LIKE '-'
		)";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':username',$this->username, PDO::PARAM_STR,75);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function DetailLaporanLuarKotaByDate($fd,$td){
		$query = "SELECT nama,tanggal_absen,absen_masuk
		FROM `absensi` 
        LEFT JOIN perusahaan ON perusahaan.id=absensi.nama_perusahaan
		WHERE `absensi`.`tanggal_absen` BETWEEN '$fd' AND '$td'
		AND `absensi`.`username`=:username
		AND `absensi`.`nama_perusahaan` IN (
			SELECT `perusahaan`.`id` 
		FROM `perusahaan` 
		WHERE `perusahaan`.`kota` NOT LIKE '%bogor%' AND `perusahaan`.`kota` NOT LIKE '%jakarta%' AND `perusahaan`.`kota` NOT LIKE '%depok%' AND `perusahaan`.`kota` NOT LIKE '%tangerang%' AND `perusahaan`.`kota` NOT LIKE '%bekasi%' AND `perusahaan`.`kota` NOT LIKE '-'
		)";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':username',$this->username, PDO::PARAM_STR,75);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function DetailLaporanLuarKota(){
		$query = "SELECT nama,tanggal_absen,absen_masuk
		FROM `absensi` 
        LEFT JOIN perusahaan ON perusahaan.id=absensi.nama_perusahaan
		WHERE `absensi`.`username`=:username
		AND `absensi`.`nama_perusahaan` IN (
			SELECT `perusahaan`.`id` 
		FROM `perusahaan` 
		WHERE `perusahaan`.`kota` NOT LIKE '%bogor%' AND `perusahaan`.`kota` NOT LIKE '%jakarta%' AND `perusahaan`.`kota` NOT LIKE '%depok%' AND `perusahaan`.`kota` NOT LIKE '%tangerang%' AND `perusahaan`.`kota` NOT LIKE '%bekasi%' AND `perusahaan`.`kota` NOT LIKE '-'
		)";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':username',$this->username, PDO::PARAM_STR,75);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function DetailLaporanAccurate($varian)	{
		$query = "SELECT `perusahaan`.`id` AS `id_usaha`,`laporan_harian`.`id` AS `id`, `absensi`.`username` AS `username`, `nama`, `int_hk_hari` AS `jns_hari_kerja`, `hari`, `tanggal`, `jumlah_overtime`, `id_absen`, `kegiatan`, `ket_kegiatan`, `peserta_kegiatan`, `dsk_desc` AS `status_kerja`, `pk_peran` AS `perankerja`,`peran`, `rekan`, `lt_ket` AS `jns_transportasi`, `biaya_transport`, `ket_transport`, `biaya_parkir`, `biaya_lunch`, `biaya_dinner`, `biaya_kesehatan`, `biaya_allowance`, `biaya_lain`, `ket_biaya_lain`, `waktu_lapor` 
		FROM `laporan_harian` 
		INNER JOIN `absensi` ON `absensi`.`id`=`laporan_harian`.`id_absen`
		INNER JOIN `perusahaan` ON `perusahaan`.`id` = `absensi`.`nama_perusahaan`
		INNER JOIN `usaha_versi_accurate` ON `usaha_versi_accurate`.`uva_source`=`absensi`.`nama_perusahaan`
		LEFT JOIN `daftar_status_kerja` ON `daftar_status_kerja`.`dsk_id`=`laporan_harian`.`status_kerja`
		LEFT JOIN `daftar_peran_kerja` ON `daftar_peran_kerja`.`pk_id`=`laporan_harian`.`peran`
		LEFT JOIN `daftar_alat_trans` ON `daftar_alat_trans`.`lt_id`=`laporan_harian`.`jns_transportasi`
		LEFT JOIN `intensif_jns_hr_kerja` ON `intensif_jns_hr_kerja`.`int_hk_id`=`laporan_harian`.`jns_hari_kerja` 
		WHERE `laporan_harian`.`username`=:username AND `usaha_versi_accurate`.`uva_id_varian`=:varian;";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':username',$this->username, PDO::PARAM_STR,75);
		$statement->bindParam(':varian',$varian, PDO::PARAM_STR,75);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function LaporanTotalTrainingJabodetabekByDate($fd,$ld){
		$query = "SELECT COUNT(`nama_perusahaan`) AS `jumlah` 
		FROM `absensi` 
		WHERE `absensi`.`username`=:username
		AND `absensi`.`tanggal_absen` BETWEEN '$fd' AND '$ld'
		AND `absensi`.`nama_perusahaan` IN (
			SELECT `perusahaan`.`id`
			FROM `perusahaan` 
			WHERE perusahaan.kota NOT LIKE '-' 
        	AND (
                `perusahaan`.`kota` LIKE '%bogor%' 
                OR `perusahaan`.`kota` LIKE '%jakarta%' 
                OR `perusahaan`.`kota` LIKE '%depok%' 
                OR `perusahaan`.`kota` LIKE '%tangerang%' 
                OR `perusahaan`.`kota` LIKE '%bekasi%') 
		)
		UNION ALL
		SELECT COUNT(`nama_perusahaan`) AS `jumlah` 
		FROM `absensi` 
		WHERE `absensi`.`username`=:username
		AND `absensi`.`nama_perusahaan` IN (
			SELECT perusahaan.id AS id 
			FROM `intensif_tbl_wil`
			INNER JOIN `perusahaan`
			ON `perusahaan`.`provinsi` = `intensif_tbl_wil`.`itw_provinsi`
			WHERE `intensif_tbl_wil`.`itw_wil_kode`='3'";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':username',$this->username, PDO::PARAM_STR,75);
		$statement->execute();
		$results=$statement->fetch(PDO::FETCH_OBJ);
		if (count($results)=='0') {
			return 0;
		}else{
			return $results[0]['jumlah']+$results[1]['jumlah'];
		}
	}

	public function LaporanTotalTrainingJabodetabek(){
		$query = "SELECT COUNT(`nama_perusahaan`) AS `jumlah` 
		FROM `absensi` 
		WHERE `absensi`.`username`=:username
		AND `absensi`.`nama_perusahaan` IN (
			SELECT `perusahaan`.`id`
			FROM `perusahaan` 
			WHERE perusahaan.kota NOT LIKE '-' 
        	AND (
                `perusahaan`.`kota` LIKE '%bogor%' 
                OR `perusahaan`.`kota` LIKE '%jakarta%' 
                OR `perusahaan`.`kota` LIKE '%depok%' 
                OR `perusahaan`.`kota` LIKE '%tangerang%' 
                OR `perusahaan`.`kota` LIKE '%bekasi%') 
		)
		UNION ALL
		SELECT COUNT(`nama_perusahaan`) AS `jumlah` 
		FROM `absensi` 
		WHERE `absensi`.`username`=:username2
		AND `absensi`.`nama_perusahaan` IN (
			SELECT perusahaan.id AS id 
			FROM `intensif_tbl_wil`
			INNER JOIN `perusahaan`
			ON `perusahaan`.`provinsi` = `intensif_tbl_wil`.`itw_provinsi`
			WHERE `intensif_tbl_wil`.`itw_wil_kode`='3'
		)";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':username',$this->username, PDO::PARAM_STR,75);
		$statement->bindParam(':username2',$this->username, PDO::PARAM_STR,75);
		
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		// print_r($results);
		if (count($results)=='0') {
			return 0;
		}else{
			return $results[0]['jumlah']+$results[1]['jumlah'];
		}
	}

	public function LaporanLuarKotaByDate($fd,$ld){
		$query = "SELECT COUNT(`nama_perusahaan`) AS `jumlah` 
		FROM `absensi` 
		WHERE `absensi`.`tanggal_absen` BETWEEN '$fd' AND '$ld'
		AND `absensi`.`username`=:username
		AND `absensi`.`nama_perusahaan` IN (
			SELECT `perusahaan`.`id` 
		FROM `perusahaan` 
		WHERE `perusahaan`.`kota` NOT LIKE '%bogor%' AND `perusahaan`.`kota` NOT LIKE '%jakarta%' AND `perusahaan`.`kota` NOT LIKE '%depok%' AND `perusahaan`.`kota` NOT LIKE '%tangerang%' AND `perusahaan`.`kota` NOT LIKE '%bekasi%' AND `perusahaan`.`kota` NOT LIKE '-'
		);";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':username',$this->username, PDO::PARAM_STR,75);
		$statement->execute();
		$results=$statement->fetch(PDO::FETCH_OBJ);
		return $results->jumlah;
	}

	public function LaporanLuarKota(){
		$query = "SELECT COUNT(`nama_perusahaan`) AS `jumlah` 
		FROM `absensi` 
		WHERE `absensi`.`username`=:username
		AND `absensi`.`nama_perusahaan` IN (
			SELECT `perusahaan`.`id` 
		FROM `perusahaan` 
		WHERE `perusahaan`.`kota` NOT LIKE '%bogor%' 
            AND `perusahaan`.`kota` NOT LIKE '%jakarta%' 
            AND `perusahaan`.`kota` NOT LIKE '%depok%' 
            AND `perusahaan`.`kota` NOT LIKE '%tangerang%' 
            AND `perusahaan`.`kota` NOT LIKE '%bekasi%' 
            AND `perusahaan`.`kota` NOT LIKE '-'
            AND `perusahaan`.`provinsi` NOT REGEXP '^[0-9]+$'
		)
		UNION ALL
		SELECT COUNT(`nama_perusahaan`) AS `jumlah` 
		FROM `absensi` 
		WHERE `absensi`.`username`=:username
		AND `absensi`.`nama_perusahaan` IN (
			SELECT perusahaan.id AS id 
			FROM `intensif_tbl_wil`
			INNER JOIN `perusahaan`
			ON `perusahaan`.`provinsi` = `intensif_tbl_wil`.`itw_provinsi`
			WHERE `intensif_tbl_wil`.`itw_wil_kode` <> '3' 
            AND `intensif_tbl_wil`.`itw_wil_kode`<>'0' 
		)";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':username',$this->username, PDO::PARAM_STR,75);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		// print_r($results);
		if (count($results)=='0') {
			return 0;
		}else{
			return $results[0]['jumlah']+$results[1]['jumlah'];
		}
	}

	public function DetailLaporanJenisUsahaByDate($ju,$fd,$ld){
		$query = "SELECT `perusahaan`.`id` AS `id_usaha`,`laporan_harian`.`id` AS `id`, `absensi`.`username` AS `username`, `nama`, `jns_hari_kerja`, `hari`, `tanggal`, `jumlah_overtime`, `id_absen`, `kegiatan`, `ket_kegiatan`, `peserta_kegiatan`, `status_kerja`, `peran`, `rekan`, `jns_transportasi`,int_hk_hari,lt_ket,dsk_desc,pk_peran, `biaya_transport`, `ket_transport`, `biaya_parkir`, `biaya_lunch`, `biaya_dinner`, `biaya_kesehatan`, `biaya_allowance`, `biaya_lain`, `ket_biaya_lain`, `waktu_lapor` 
		FROM `laporan_harian` 
		INNER JOIN `absensi` ON `absensi`.`id`=`laporan_harian`.`id_absen`
		INNER JOIN `perusahaan` ON `perusahaan`.`id` = `absensi`.`nama_perusahaan`
		INNER JOIN `perusahaan_jns_usaha` ON `perusahaan_jns_usaha`.`id_company`=`absensi`.`nama_perusahaan`
		LEFT JOIN `daftar_status_kerja` ON `daftar_status_kerja`.`dsk_id`=`laporan_harian`.`status_kerja`
		LEFT JOIN `daftar_peran_kerja` ON `daftar_peran_kerja`.`pk_id`=`laporan_harian`.`peran`
		LEFT JOIN `daftar_alat_trans` ON `daftar_alat_trans`.`lt_id`=`laporan_harian`.`jns_transportasi`
		LEFT JOIN `intensif_jns_hr_kerja` ON `intensif_jns_hr_kerja`.`int_hk_id`=`laporan_harian`.`jns_hari_kerja`
		WHERE `laporan_harian`.`username`=:username 
        AND `perusahaan_jns_usaha`.`id_jenis_usaha`=:ju
        AND `absensi`.`tanggal_absen` BETWEEN '$fd' AND '$ld' ORDER BY `absensi`.`tanggal_absen` ASC;";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':username',$this->username, PDO::PARAM_STR,75);
		$statement->bindParam(':ju',$ju, PDO::PARAM_STR,75);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function DetailLaporanJenisUsaha($ju){
		$query = "SELECT `perusahaan`.`id` AS `id_usaha`,`laporan_harian`.`id` AS `id`, `absensi`.`username` AS `username`, `nama`, `jns_hari_kerja`, `hari`, `tanggal`, `jumlah_overtime`, `id_absen`, `kegiatan`, `ket_kegiatan`, `peserta_kegiatan`, `status_kerja`, `peran`, `rekan`,int_hk_hari,lt_ket,dsk_desc,pk_peran, `jns_transportasi`, `biaya_transport`, `ket_transport`, `biaya_parkir`, `biaya_lunch`, `biaya_dinner`, `biaya_kesehatan`, `biaya_allowance`, `biaya_lain`, `ket_biaya_lain`, `waktu_lapor` 
		FROM `laporan_harian` 
		INNER JOIN `absensi` ON `absensi`.`id`=`laporan_harian`.`id_absen`
		INNER JOIN `perusahaan` ON `perusahaan`.`id` = `absensi`.`nama_perusahaan`
		INNER JOIN `perusahaan_jns_usaha` ON `perusahaan_jns_usaha`.`id_company`=`absensi`.`nama_perusahaan`
		LEFT JOIN `daftar_status_kerja` ON `daftar_status_kerja`.`dsk_id`=`laporan_harian`.`status_kerja`
		LEFT JOIN `daftar_peran_kerja` ON `daftar_peran_kerja`.`pk_id`=`laporan_harian`.`peran`
		LEFT JOIN `daftar_alat_trans` ON `daftar_alat_trans`.`lt_id`=`laporan_harian`.`jns_transportasi`
		LEFT JOIN `intensif_jns_hr_kerja` ON `intensif_jns_hr_kerja`.`int_hk_id`=`laporan_harian`.`jns_hari_kerja`
		WHERE `laporan_harian`.`username`=:username 
        AND `perusahaan_jns_usaha`.`id_jenis_usaha`=:ju ORDER BY `absensi`.`tanggal_absen` ASC;";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':username',$this->username, PDO::PARAM_STR,75);
		$statement->bindParam(':ju',$ju, PDO::PARAM_STR,75);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function DetailLaporanAccurateByDate($varian,$firstdate,$lastdate){
		$query = "SELECT `laporan_harian`.`id` AS `id`, `absensi`.`username` AS `username`, `nama`, `jns_hari_kerja`, `hari`, `tanggal`, `jumlah_overtime`, `id_absen`, `kegiatan`, `ket_kegiatan`, `peserta_kegiatan`, `status_kerja`, `peran`, `rekan`, `jns_transportasi`, `biaya_transport`, `ket_transport`, `biaya_parkir`, `biaya_lunch`, `biaya_dinner`, `biaya_kesehatan`, `biaya_allowance`, `biaya_lain`, `ket_biaya_lain`, `waktu_lapor` 
		FROM `laporan_harian` 
		INNER JOIN `absensi` ON `absensi`.`id`=`laporan_harian`.`id_absen`
		INNER JOIN `perusahaan` ON `perusahaan`.`id` = `absensi`.`nama_perusahaan`
		INNER JOIN `usaha_versi_accurate` ON `usaha_versi_accurate`.`uva_source`=`absensi`.`nama_perusahaan`
		WHERE `laporan_harian`.`username`=:username AND `usaha_versi_accurate`.`uva_id_varian`=:varian
        AND `laporan_harian`.`tanggal` BETWEEN '$firstdate' AND '$lastdate' ORDER BY `laporan_harian`.`tanggal` ASC;";	
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':username',$this->username, PDO::PARAM_STR,75);
		$statement->bindParam(':varian',$varian, PDO::PARAM_STR,75);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function LaporanJenisAccurate(){
		$query = "SELECT `va_id`, `va_nama`,
		(SELECT COUNT(`laporan_harian`.`id`) AS `jumlah` 
		FROM `laporan_harian` 
		INNER JOIN `absensi` ON `absensi`.`id`=`laporan_harian`.`id_absen`
		INNER JOIN `perusahaan` ON `perusahaan`.`id` = `absensi`.`nama_perusahaan`
		INNER JOIN `usaha_versi_accurate` ON `usaha_versi_accurate`.`uva_source`=`absensi`.`nama_perusahaan`
		WHERE `laporan_harian`.`username`=:username AND `usaha_versi_accurate`.`uva_id_varian`=`va_id`) AS `jumlah`
		FROM `versi_accurate`;";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':username',$this->username, PDO::PARAM_STR,75);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function LaporanJenisUsahaByDate($dt,$kt){
		$query = "SELECT `jenis_usaha`.`id` AS `aidi`, `jenis_usaha`,
		(
        SELECT COUNT(`nama_perusahaan`) 
			FROM `absensi` 
			INNER JOIN `perusahaan`
			ON `perusahaan`.`id`=`absensi`.`nama_perusahaan`  
			WHERE `absensi`.`username`=:username AND `absensi`.`tanggal_absen` BETWEEN '$dt' AND '$kt'
            AND perusahaan.id IN (
                SELECT perusahaan_jns_usaha.id_company 
                FROM perusahaan_jns_usaha
				WHERE perusahaan_jns_usaha.id_jenis_usaha = `aidi`            
            )
        ) AS `jumlah`
		FROM `jenis_usaha`;";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':username',$this->username, PDO::PARAM_STR,75);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function LaporanJenisUsaha(){
		$query = "SELECT `jenis_usaha`.`id` AS `aidi`, `jenis_usaha`,
		(
        SELECT COUNT(`nama_perusahaan`) 
			FROM `absensi` 
			INNER JOIN `perusahaan`
			ON `perusahaan`.`id`=`absensi`.`nama_perusahaan`  
			WHERE `absensi`.`username`=:username
            AND perusahaan.id IN (
                SELECT perusahaan_jns_usaha.id_company 
                FROM perusahaan_jns_usaha
				WHERE perusahaan_jns_usaha.id_jenis_usaha = `aidi`            
            )
        ) AS `jumlah`
		FROM `jenis_usaha`;";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':username',$this->username, PDO::PARAM_STR,75);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function LaporanJenisAccurateByDate($dt,$kt){ //dt = dari waktu, kt = ke waktu
		//query editted on 27-04-2018 before meeting
		$query = "SELECT `va_id`, `va_nama`,
		(
        SELECT COUNT(`nama_perusahaan`) 
			FROM `absensi` 
			INNER JOIN `perusahaan`
			ON `perusahaan`.`id`=`absensi`.`nama_perusahaan`  
			WHERE `absensi`.`username`=:username AND `absensi`.`tanggal_absen` BETWEEN '$dt' AND '$kt'
            AND perusahaan.id IN (
                SELECT usaha_versi_accurate.uva_source 
                FROM usaha_versi_accurate
				WHERE `usaha_versi_accurate`.`uva_id_varian` = `va_id`            
            )
        ) AS `jumlah`
		FROM `versi_accurate`;";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':username',$this->username, PDO::PARAM_STR,75);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function GetLaporanUser(){ // query is pretty fast
		$query = "SELECT `users`.`username` AS `aidi`,`nama`, `created_at`, `email`, `lahir`, `telepon`, `status`, `tipe`,`ukt_ket`,
		(SELECT COUNT(`id`) FROM `absensi` WHERE `absensi`.`username`=`aidi`) AS `total_absen`,
		(SELECT COUNT(`id`) FROM `laporan_harian` WHERE `laporan_harian`.`username`=`aidi`) AS `total_laporan`,
		(SELECT SUM(`laporan_harian`.`jumlah_overtime`) FROM `laporan_harian` WHERE `laporan_harian`.`username`=`aidi`) AS `total_overtime`,
		(SELECT COUNT(`id`) FROM `laporan_harian` WHERE `laporan_harian`.`username`=`aidi` AND `laporan_harian`.`status_kerja`='sendiri') AS `total_sendiri`,
		(SELECT COUNT(`id`) FROM `laporan_harian` WHERE `laporan_harian`.`username`=`aidi` AND `laporan_harian`.`status_kerja`='tidak sendiri') AS `total_ts`,
		(SELECT SUM(`biaya_transport`+`biaya_parkir`+`biaya_lunch`+`biaya_dinner`+`biaya_kesehatan`+`biaya_allowance`+`biaya_lain`) AS `jumlah` FROM `laporan_harian` WHERE `laporan_harian`.`username`=`aidi`) AS `total_reimbursement`
		FROM `users_ket_team` 
		INNER JOIN `users` ON `users`.`team`=`users_ket_team`.`ukt_id`
		WHERE `users`.`username`=:username;";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':username',$this->username, PDO::PARAM_STR,75);
		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function GetLaporanUserByDate($fd,$td){
		$query = "SELECT `users`.`username` AS `aidi`,`nama`, `created_at`, `email`, `lahir`, `telepon`, `status`, `tipe`,`ukt_ket`,
		(SELECT COUNT(`id`) FROM `absensi` WHERE `absensi`.`username`=`aidi` AND `absensi`.`tanggal_absen` BETWEEN '$fd' AND '$td') AS `total_absen`,
		(SELECT COUNT(`id`) FROM `laporan_harian` WHERE `laporan_harian`.`username`=`aidi` AND `laporan_harian`.`tanggal` BETWEEN '$fd' AND '$td') AS `total_laporan`,
		(SELECT SUM(`laporan_harian`.`jumlah_overtime`) FROM `laporan_harian` WHERE `laporan_harian`.`username`=`aidi` AND `laporan_harian`.`tanggal` BETWEEN '$fd' AND '$td') AS `total_overtime`,
		(SELECT COUNT(`id`) FROM `laporan_harian` WHERE `laporan_harian`.`username`=`aidi` AND `laporan_harian`.`status_kerja`='sendiri' AND `laporan_harian`.`tanggal` BETWEEN '$fd' AND '$td') AS `total_sendiri`,
		(SELECT COUNT(`id`) FROM `laporan_harian` WHERE `laporan_harian`.`username`=`aidi` AND `laporan_harian`.`status_kerja`='tidak sendiri' AND `laporan_harian`.`tanggal` BETWEEN '$fd' AND '$td') AS `total_ts`,
		(SELECT SUM(`biaya_transport`+`biaya_parkir`+`biaya_lunch`+`biaya_dinner`+`biaya_kesehatan`+`biaya_allowance`+`biaya_lain`) AS `jumlah` FROM `laporan_harian` WHERE `laporan_harian`.`username`=`aidi` AND `laporan_harian`.`tanggal` BETWEEN '$fd' AND '$td') AS `total_reimbursement`
		FROM `users_ket_team` 
		INNER JOIN `users` ON `users`.`team`=`users_ket_team`.`ukt_id`
		WHERE `users`.`username`=:username;";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':username',$this->username, PDO::PARAM_STR,75);
		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function selectByID($id){
		$query = "SELECT * FROM `users` WHERE username=:username";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':username',$id, PDO::PARAM_STR,75);
		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function changePassword($objek){
		$query = "UPDATE `users` SET password=:myPassword WHERE username=:username";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':myPassword',$objek->myPassword, PDO::PARAM_STR,75);
		$statement->bindParam(':username',$objek->username, PDO::PARAM_STR,75);

		if ($statement->execute()) {
			return "<script>alert('Password berhasil di ubah!')</script>";
		}else{
			return "<script>alert('Password gagal di ubah!')</script>";
		}
	}

	public function editPicture(){
		$query="UPDATE `users` SET `picture` = :pic WHERE `users`.`username` = :username;";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':username',$this->getUsername(), PDO::PARAM_STR,75);
		@$statement->bindParam(':pic',$this->getPicture(), PDO::PARAM_STR,255);
		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function aktifasiLogin($username,$status){
		$query = "UPDATE `users` SET `status`=:status WHERE username=:username";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':username',$username, PDO::PARAM_STR,75);
		$statement->bindParam(':status',$status, PDO::PARAM_STR,75);

		
		if ($statement->execute()) {
			//$dbHost = null;
			return true;
		}else{
			return false;
		}
	}

	public function daftarHadirByParameter($tanggalAwal,$tanggalAkhir,$username){
		// $username = '%'.$username.'%';
		$query = "SELECT * FROM absensi_harian_view 
		WHERE `tanggal_absen` BETWEEN :tanggalAwal AND :tanggalAkhir AND username = :username";
		$statement=$this->dbHost->prepare($query);

		$statement->bindParam(':tanggalAwal',$tanggalAwal, PDO::PARAM_STR,12);
		$statement->bindParam(':tanggalAkhir',$tanggalAkhir, PDO::PARAM_STR,12);
		$statement->bindParam(':username',$username, PDO::PARAM_STR,75);

		$datas=array();
		$statement->execute();
		while ($result=$statement->fetch(PDO::FETCH_OBJ)) {
			$objek = new Absenz();
			$objek->setNama($result->nama);
			$objek->setJamMasuk($result->absen_masuk);
			$objek->setLokasiMasuk($result->lokasi_absen_masuk);
			$objek->setJamPulang($result->absen_pulang);
			$objek->setLokasiPulang($result->lokasi_absen_pulang);
			$objek->setTanggal($result->tanggal_absen);
            $objek->setNamaPerusahaan($result->nama_perusahaan);
			$objek->setUsername($result->username);
			array_push($datas, $objek);
		}
		return $datas;
	}

	public function daftarHadirWithoutUsername($tanggalAwal,$tanggalAkhir,$namaPerusahaan){
		if ($tanggalAwal==''&&$tanggalAkhir=='') {
			$query="SELECT * FROM `absensi_harian_view` WHERE `nama_perusahaan` LIKE '%$namaPerusahaan%' ORDER BY `tanggal_absen` ASC ";
		} else {
			$query="SELECT * FROM `absensi_harian_view` WHERE `tanggal_absen` BETWEEN '$tanggalAwal' AND '$tanggalAkhir' AND `nama_perusahaan` LIKE '%$namaPerusahaan%' ORDER BY `tanggal_absen` ASC ";
		}
		
		$statement=$this->dbHost->prepare($query);
		$datas=array();
		$statement->execute();
		while ($result=$statement->fetch(PDO::FETCH_OBJ)) {
			$objek = new Absenz();
			$objek->setNama($result->nama);
			$objek->setJamMasuk($result->absen_masuk);
			$objek->setLokasiMasuk($result->lokasi_absen_masuk);
			$objek->setJamPulang($result->absen_pulang);
			$objek->setLokasiPulang($result->lokasi_absen_pulang);
			$objek->setTanggal($result->tanggal_absen);
            $objek->setNamaPerusahaan($result->nama_perusahaan);
			$objek->setUsername($result->username);
			array_push($datas, $objek);
		}
		return $datas;
	}

	public function getRequestPerusahaan($id){
		$query="SELECT `nama`, `nama_perusahaan` FROM `absensi`,`users` WHERE users.username=absensi.username AND `id`='$id'";
		$statement=$this->dbHost->prepare($query);
		$statement=$this->dbHost->prepare($query);
		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function daftarHadirByParameterAdmin($tanggalAwal,$tanggalAkhir,$username,$namaPerusahaan){
		//$username = '%'.$username.'%';
		$namaPerusahaan = '%'.$namaPerusahaan.'%';
		
		$query = "SELECT * FROM absensi_harian_view 
		WHERE tanggal_absen BETWEEN :tanggalAwal AND :tanggalAkhir AND nama_perusahaan LIKE :namaPerusahaan";

		for ($i=0; $i < count($username) ; $i++) { 
			if ($i>0) {
				$query .= " OR username='".$username[$i]."' AND tanggal_absen BETWEEN :tanggalAwal AND :tanggalAkhir AND nama_perusahaan LIKE :namaPerusahaan";
			}else{
				$query = "SELECT * FROM absensi_harian_view WHERE tanggal_absen BETWEEN :tanggalAwal AND :tanggalAkhir AND nama_perusahaan LIKE :namaPerusahaan AND username='".$username[0]."'";
			}			
		}//end for loop

		$query .= " ORDER BY tanggal_absen ASC";
		
		$statement=$this->dbHost->prepare($query);

		$statement->bindParam(':tanggalAwal',$tanggalAwal, PDO::PARAM_STR,12);
		$statement->bindParam(':tanggalAkhir',$tanggalAkhir, PDO::PARAM_STR,12);
		//$statement->bindParam(':username',$username, PDO::PARAM_STR,75);
		$statement->bindParam(':namaPerusahaan',$namaPerusahaan, PDO::PARAM_STR,125);

		$datas=array();
		$statement->execute();
		while ($result=$statement->fetch(PDO::FETCH_OBJ)) {
			$objek = new Absenz();
			$objek->setNama($result->nama);
			$objek->setJamMasuk($result->absen_masuk);
			$objek->setLokasiMasuk($result->lokasi_absen_masuk);
			$objek->setJamPulang($result->absen_pulang);
			$objek->setLokasiPulang($result->lokasi_absen_pulang);
			$objek->setTanggal($result->tanggal_absen);
            $objek->setNamaPerusahaan($result->nama_perusahaan);
			$objek->setUsername($result->username);
			array_push($datas, $objek);
		}
		return $datas;
	}

	public function fetchUsernameAndName(){
		$query = "SELECT username, nama FROM users WHERE `status`='aktif'";
		$statement=$this->dbHost->prepare($query);
		$datas=array();
		$statement->execute();
		while ($result=$statement->fetch(PDO::FETCH_OBJ)) {
			$objek = new Petugas();
			$objek->setNama($result->nama);
			$objek->setUsername($result->username);
			array_push($datas, $objek);
		}
		return $datas;
	}

	public function fetchUsernameAndName2(){
		$query = "SELECT username, nama FROM users WHERE `status`='aktif'";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function fetchActiveUsers(){
		$query = "SELECT * FROM users WHERE `status`='aktif'";
		$statement=$this->dbHost->prepare($query);
		
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function fetchUsernameAndNameByTeam($team){
		$query = "SELECT username, nama FROM users WHERE team='$team' AND users.status='aktif'";
		$statement=$this->dbHost->prepare($query);
		$datas=array();
		$statement->execute();
		while ($result=$statement->fetch(PDO::FETCH_OBJ)){
			$objek = new Petugas();
			$objek->setNama($result->nama);
			$objek->setUsername($result->username);
			array_push($datas, $objek);
		}
		return $datas;
	}

	public function updatePerusahaan($arr){
		$query = "UPDATE `absensi` SET `nama_perusahaan` = '".$arr['val']."' WHERE `absensi`.`id` = '".$arr['id']."'";
		$statement=$this->dbHost->prepare($query);
		
		if ($statement->execute()) {
			//$dbHost = null;
			return "<script>alert('data berhasil diubah');</script>";
		}else{
			return "<script>alert('data gagal diubah');</script>";
		}
	}

	public function fetchAlertRequest(){
		$query="SELECT * FROM `request_alert`;";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function dataRequest($type){
		if ($type=='forEdit') {
			$query ="SELECT id,nama,nama_perusahaan,tanggal_absen FROM `absensi`,`users` WHERE nama_perusahaan LIKE '%eques%' AND absensi.username=users.username AND status='aktif' ORDER BY tanggal_absen DESC";
		}else{
			$query="SELECT absensi.id AS id, tanggal_absen, nama, nama_perusahaan FROM `absensi`,`users` WHERE users.username=absensi.username AND `nama_perusahaan` LIKE '%eques%' ORDER BY tanggal_absen DESC; ";
		}
		
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function getDataRequestByUsername($username){
		$query = "SELECT id,nama,nama_perusahaan,tanggal_absen FROM `absensi`,`users` WHERE nama_perusahaan LIKE '%eques%' AND absensi.username=users.username AND status='aktif' AND absensi.username='$username' ORDER BY tanggal_absen DESC";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function jumlahDataRequest($username){
		$query="SELECT COUNT(absensi.id) AS jumlah FROM `absensi`,`users` WHERE nama_perusahaan LIKE '%eques%' AND absensi.username='$username' AND absensi.username=users.username AND status='aktif'";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_OBJ);
		return $result->jumlah;
	}
	
	public function daftarHadir($tanggal,$username){
		//$query="SELECT nama,absen_masuk,lokasi_absen_masuk,cari_perusahaan(nama_perusahaan) AS nama_perusahaan,absen_pulang,lokasi_absen_pulang,tanggal_absen,absensi.username AS username,tanggal_absen FROM absensi,users WHERE tanggal_absen LIKE :tanggal AND absensi.username LIKE :username AND absensi.username=users.username ORDER BY tanggal_absen DESC LIMIT 50";
		$query="SELECT * FROM absensi_harian_view WHERE tanggal_absen LIKE :tanggal AND username LIKE :username ORDER BY tanggal_absen DESC LIMIT 50";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':tanggal',$tanggal, PDO::PARAM_STR,25);
		$statement->bindParam(':username',$username, PDO::PARAM_STR,75);

		$datas=array();
		$statement->execute();
		while ($result=$statement->fetch(PDO::FETCH_OBJ)) {
			$objek = new Absenz();
			$objek->setNama($result->nama);
			$objek->setJamMasuk($result->absen_masuk);
			$objek->setLokasiMasuk($result->lokasi_absen_masuk);
			$objek->setJamPulang($result->absen_pulang);
			$objek->setLokasiPulang($result->lokasi_absen_pulang);
			$objek->setTanggal($result->tanggal_absen);
			$objek->setUsername($result->username);
			$objek->setNamaPerusahaan($result->nama_perusahaan);
			array_push($datas, $objek);
		}
		return $datas;
	}

	public function daftarHadirByTeam($tanggal,$username,$team){
		$query="SELECT * FROM absensi_harian_view WHERE tanggal_absen LIKE :tanggal AND username LIKE :username AND team='$team' ORDER BY tanggal_absen DESC LIMIT 50";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':tanggal',$tanggal, PDO::PARAM_STR,25);
		$statement->bindParam(':username',$username, PDO::PARAM_STR,75);
		$datas=array();
		$statement->execute();
		while ($result=$statement->fetch(PDO::FETCH_OBJ)) {
			$objek = new Absenz();
			$objek->setNama($result->nama);
			$objek->setJamMasuk($result->absen_masuk);
			$objek->setLokasiMasuk($result->lokasi_absen_masuk);
			$objek->setJamPulang($result->absen_pulang);
			$objek->setLokasiPulang($result->lokasi_absen_pulang);
			$objek->setTanggal($result->tanggal_absen);
			$objek->setUsername($result->username);
			$objek->setNamaPerusahaan($result->nama_perusahaan);
			array_push($datas, $objek);
		}
		return $datas;
	}


	
	//absen
	public function sesiAbsen($username){
		$query = "SELECT * 
		FROM `absensi` 
		INNER JOIN users ON users.username=absensi.username
		WHERE `absensi`.`username` = '".$username."' AND `tanggal_absen` = '".date('Y-m-d')."'";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function selectEditAbsen($username,$tanggal){
		$query="SELECT id,`absensi`.`username` AS username, nama, `absen_masuk`, `absen_pulang`, `lokasi_absen_pulang`,`email`, `tanggal_absen`, `nama_perusahaan`,cari_perusahaan(`nama_perusahaan`) AS perusahaannya FROM `absensi`,`users` WHERE `absensi`.`username` = '$username' AND `tanggal_absen` = '$tanggal' AND absensi.username=users.username";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function NewRequestAbsenPulang(){
		$query = "INSERT INTO `absensi_edit` (`id_absen`,`jam_pulang`,`ae_alasan`)
		VALUES (:idabsen,:jampulang,:alasan);";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':idabsen',$this->absen->id, PDO::PARAM_STR,75);
		$statement->bindParam(':jampulang',$this->absen->jampulang, PDO::PARAM_STR,75);
		$statement->bindParam(':alasan',$this->absen->alasan, PDO::PARAM_STR,500);
		// $statement->bindParam(':tanggal',$objek->tanggal, PDO::PARAM_STR,75);
		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function SelectAbsenByID(){
		$query = "SELECT `absensi`.`id` AS `id`, `absen_masuk`, `lokasi_absen_masuk`, `absen_pulang`, `lokasi_absen_pulang`, `tanggal_absen`, `perusahaan`.`nama` AS `nama_usaha`, `users`.`nama` AS `nama_user` 
		FROM `absensi` 
		LEFT JOIN `perusahaan` ON `perusahaan`.`id` = `absensi`.`nama_perusahaan`
		INNER JOIN `users` ON `users`.`username` = `absensi`.`username` 
		WHERE `absensi`.`id`=:id;";
		$statement=$this->dbHost->prepare($query);

		$statement->bindParam(':id',$this->absen->id, PDO::PARAM_STR,75);
		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function updateAbsen($objek){
		$query="UPDATE absensi SET `absen_masuk`=:absenMasuk,`absen_pulang`=:absenPulang,`nama_perusahaan`=:namaPerusahaan, `absen_status`='3' WHERE `username`=:username AND `tanggal_absen`=:tanggal";
		//$objek = json_decode($objek);
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':absenMasuk',$objek->absenMasuk, PDO::PARAM_STR,15);
		$statement->bindParam(':absenPulang',$objek->absenPulang, PDO::PARAM_STR,15);
		$statement->bindParam(':namaPerusahaan',$objek->namaPerusahaan, PDO::PARAM_STR,125);
		
		$statement->bindParam(':username',$objek->username, PDO::PARAM_STR,75);
		$statement->bindParam(':tanggal',$objek->tanggal, PDO::PARAM_STR,75);
		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
		
	}

	public function updateNamaPerusahaan($objek){
		$query="UPDATE absensi SET nama_perusahaan=:nama_perusahaan WHERE id=:id";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':nama_perusahaan',$objek->namaPerusahaan, PDO::PARAM_STR,40);
		$statement->bindParam(':id',$objek->idAbsen, PDO::PARAM_STR,40);
		
		if ($statement->execute()) {
			return "<script>alert('Nama perusahaan berhasil di ubah')</script>";
		}else{
			return "<script>alert('Nama perusahaan gagal di ubah')</script>";
		}		
	}

	public function AbsenMasukk(){
		$query = "INSERT INTO `absensi` (`id`,`username`, `absen_masuk`, `lokasi_absen_masuk`, `tanggal_absen`,`nama_perusahaan`) 
		VALUES (:aidi,:username, :absenMasuk, :lokasiAbsenMasuk, :tanggal,:namaPerusahaan)";
		$timestr =  date('H:i:s');
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':aidi',@md5($this->absen->username.'-'.date('Y-m-d')), PDO::PARAM_STR,75);
		$statement->bindParam(':username',$this->absen->username, PDO::PARAM_STR,75);
		$statement->bindParam(':absenMasuk',$timestr, PDO::PARAM_STR,15);
		$statement->bindParam(':lokasiAbsenMasuk',$this->absen->lokasimasuk, PDO::PARAM_STR,75);
		@$statement->bindParam(':tanggal',date('Y-m-d'), PDO::PARAM_STR,25);
		$statement->bindParam(':namaPerusahaan',$this->absen->usaha, PDO::PARAM_STR,75);

		if ($statement->execute()) {
			//$dbHost = null;
			return true;
		}else{
			return false;
		}
	}

	public function absenPulangg(){
		$query ="UPDATE `absensi` SET `absen_pulang`=:waktuPulang,`lokasi_absen_pulang`=:lokasiPulang WHERE `tanggal_absen` = :tanggal AND `username` = :username";
		$timestr =  date('H:i:s');
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':username',$this->absen->username, PDO::PARAM_STR,75);
		$statement->bindParam(':waktuPulang',$timestr, PDO::PARAM_STR,15);
		$statement->bindParam(':lokasiPulang',$this->absen->lokasipulang, PDO::PARAM_STR,75);
		@$statement->bindParam(':tanggal',date('Y-m-d'), PDO::PARAM_STR,25);

        $waktu = explode(':',$timestr);
        // print_r($waktu);
        if(@$waktu[0]==''||@$waktu[1]==''||@$waktu[2]==''){
    		return false;
        }else{
            if ($statement->execute()) {
    			//$dbHost = null;
    			return true;
    		}else{
    			return false;
    		}
        }
	}

	public function absenMasuk($objek){
		$query = "INSERT INTO `absensi` (`id`,`username`, `absen_masuk`, `lokasi_absen_masuk`, `tanggal_absen`,`nama_perusahaan`) 
		VALUES (:aidi,:username, :absenMasuk, :lokasiAbsenMasuk, :tanggal,:namaPerusahaan)";
		$timestr =  date('H:i:s');
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':aidi',@md5($objek->username.'-'.date('Y-m-d')), PDO::PARAM_STR,75);
		$statement->bindParam(':username',$objek->username, PDO::PARAM_STR,75);
		$statement->bindParam(':absenMasuk',$timestr, PDO::PARAM_STR,15);
		$statement->bindParam(':lokasiAbsenMasuk',$objek->lokasi, PDO::PARAM_STR,75);
		@$statement->bindParam(':tanggal',date('Y-m-d'), PDO::PARAM_STR,25);
		$statement->bindParam(':namaPerusahaan',$objek->namaPerusahaan, PDO::PARAM_STR,75);
		
		$waktu = explode(':',$timestr);
        if(@$waktu[0]==''||@$waktu[1]==''||@$waktu[2]==''){
    		if ($statement->execute()) {
    			//$dbHost = null;
    			return true;
    		}else{
    			return false;
    		}
        }else{
            return false;
        }
	}

	public function editInputAbsen($objek){

		$query = "INSERT INTO `absensi` (`id`,`username`, `absen_masuk`, `lokasi_absen_masuk`, `tanggal_absen`,`nama_perusahaan`,`absen_pulang`,`lokasi_absen_pulang`) 
		VALUES (:aidi,:username, :absenMasuk, :lokasiAbsenMasuk, :tanggal, :namaPerusahaan, :waktuPulang, :lokasiPulang )";
		
		$statement=$this->dbHost->prepare($query);
		
		@$statement->bindParam(':aidi',@md5($objek->username.'-'.$objek->tanggal), PDO::PARAM_STR,75);
		$statement->bindParam(':username',$objek->username, PDO::PARAM_STR,75);
		$statement->bindParam(':absenMasuk',$objek->absenMasuk, PDO::PARAM_STR,15);
		$statement->bindParam(':lokasiAbsenMasuk',$objek->lokasiMasuk, PDO::PARAM_STR,75);
		$statement->bindParam(':tanggal',$objek->tanggal, PDO::PARAM_STR,25);
		$statement->bindParam(':namaPerusahaan',$objek->namaPerusahaan, PDO::PARAM_STR,75);
		$statement->bindParam(':waktuPulang',$objek->absenPulang, PDO::PARAM_STR,15);
		$statement->bindParam(':lokasiPulang',$objek->lokasiPulang, PDO::PARAM_STR,75);

		if ($statement->execute()) {
			//$dbHost = null;
			return true;
		}else{
			return false;
		}
	}

	public function absenIO($obj){
		$query="INSERT INTO `absensi` (`id` ,`username` ,`absen_masuk` ,`lokasi_absen_masuk` ,`absen_pulang` ,`lokasi_absen_pulang` ,`tanggal_absen` ,`nama_perusahaan`) 
		VALUES ('$obj->id', '$obj->username', '$obj->absen_masuk', '$obj->lokasi_masuk', '$obj->absen_pulang', '$obj->lokasi_pulang', '$obj->tanggal_absen', '$obj->nama_perusahaan')";

		$statement=$this->dbHost->prepare($query);
		if ($statement->execute()) {
			//$dbHost = null;
			return true;
		}else{
			return false;
		}
	}

	public function absenPulang($objek){
		$query ="UPDATE `absensi` SET `absen_pulang`=:waktuPulang,`lokasi_absen_pulang`=:lokasiPulang WHERE `tanggal_absen` = :tanggal AND `username` = :username";
		$timestr =  date('H:i:s');
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':username',$objek->username, PDO::PARAM_STR,75);
		$statement->bindParam(':waktuPulang',$timestr, PDO::PARAM_STR,15);
		$statement->bindParam(':lokasiPulang',$objek->lokasi, PDO::PARAM_STR,75);
		@$statement->bindParam(':tanggal',date('Y-m-d'), PDO::PARAM_STR,25);

		if ($statement->execute()) {
			//$dbHost = null;
			return true;
		}else{
			return false;
		}
	}
	
	public function kirimEmail($tipe,$username){
    		if ($tipe=='masuk') {
 
        	  $query="SELECT absen_masuk,email FROM absensi,users WHERE absensi.username=:username AND absensi.username=users.username AND tanggal_absen=:tanggal";
        	  $statement=$this->dbHost->prepare($query);
        	  $statement->bindParam(':username', $username, PDO::PARAM_STR,25);
        	  @$statement->bindParam(':tanggal',date('Y-m-d') , PDO::PARAM_STR,25);
        	  $datas=array();
        	  $statement->execute();

        	  while ($result=$statement->fetch(PDO::FETCH_OBJ)) {
            	     mail($result->email, 'Absen Masuk', 'Anda telah absen masuk di sistem kami pada pukul: '.$result->absen_masuk);
        	  }

    		}elseif ($tipe=='keluar') {

        	  $query="SELECT absen_pulang, email FROM absensi,users WHERE absensi.username=:username AND absensi.username=users.username AND tanggal_absen=:tanggal";
        	  $statement=$this->dbHost->prepare($query);
        	  $statement->bindParam(':username', $username, PDO::PARAM_STR,25);
        	  @$statement->bindParam(':tanggal',date('Y-m-d') , PDO::PARAM_STR,25);
        	  $datas=array();
        	  $statement->execute();

        	  while ($result=$statement->fetch(PDO::FETCH_OBJ)) {
            	     mail($result->email, 'Absen Pulang', 'Anda telah absen pulang di sistem kami pada pukul: '.$result->absen_pulang);
        	  }

    		}
	}

	public function getDataPendaftarToday($tanggal){
		$query= "SELECT * FROM acara_harian WHERE tanggal='$tanggal'";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function fetchAbsenAll(){
		$query= "SELECT * FROM absensi";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function updateID($value,$user,$tanggal){
		$query= "UPDATE absensi SET id='$value' WHERE username='$user' AND tanggal_absen='$tanggal'";
		$statement = $this->dbHost->prepare($query);
		if ($statement->execute()) {
			//$dbHost = null;
			return 'data berhasil di ubah';
		}else{
			return 'data gagal di ubah';
		}
	}	
	
	
	//end absen

	public function login($objek){
		$query = "SELECT nama,username,email,status,tipe,team FROM `users` WHERE username=:username AND password=:password";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':username',$objek->username, PDO::PARAM_STR,75);
		$statement->bindParam(':password',$objek->password, PDO::PARAM_STR,75);
		$statement->execute();
		while ($result=$statement->fetch(PDO::FETCH_OBJ)) {
			//$row = $result->jumlah;
			$objek->setNama($result->nama);
			$objek->setUsername($result->username);
			$objek->setEmail($result->email);
			$objek->setStatus($result->status);
			$objek->setTipe($result->tipe);
			$objek->setTeam($result->team);
			//array_push($datas, array($result->nama_perusahaan,$result->alamat_perusahaan,$result->telepon,$result->versi_accurate,$result->agenda_training,$result->jumlah_hari_training,$result->rencana_tanggal_pelaksanaan));
			return $objek;
		}
		
	}


	public function masukData($pendaftar){
		$query="INSERT INTO `users` (`username`, `password`, `nama`, `email`, `lahir`, `telepon`, `status`) 
		VALUES (:username, :password, :nama, :email, :lahir, :telepon, :status)";
		$statement = $this->dbHost->prepare($query);

		$statement->bindParam(':username',$pendaftar->username,PDO::PARAM_STR,75);
		$statement->bindParam(':password',$pendaftar->password,PDO::PARAM_STR,75);

		$statement->bindParam(':nama',$pendaftar->nama,PDO::PARAM_STR,50);
		$statement->bindParam(':email',$pendaftar->email,PDO::PARAM_STR,125);

		$statement->bindParam(':lahir',$pendaftar->lahir,PDO::PARAM_STR,50);
		$statement->bindParam(':telepon',$pendaftar->telepon,PDO::PARAM_STR,20);
		$statement->bindParam(':status',$pendaftar->status,PDO::PARAM_STR,8);

		if ($statement->execute()) {
			//$dbHost = null;
			return true;
		}else{
			return false;
		}

	}

}



class Petugas{
	var $nama;
	var $email;
	var $username;
	var $password;
	var $lahir;
	var $telepon;
	var $status;
	var $tipe;
	var $team;

	public function getNama(){
		return $this->nama;
	}

	public function setNama($value){
		$this->nama = $value;
	}

	public function getEmail(){
		return $this->email;
	}

	public function setEmail($value){
		$this->email = $value;
	}

	public function getUsername(){
		return $this->username;
	}

	public function setUsername($value){
		$this->username = $value;
	}

	public function getPassword(){
		return $this->password;
	}

	public function setPassword($value){
		$this->password = $value;
	}

	public function getLahir(){
		return $this->lahir;
	}

	public function setLahir($value){
		$this->lahir = $value;
	}

	public function getTelepon(){
		return $this->telepon;
	}

	public function setTelepon($value){
		$this->telepon = $value;
	}

	public function getStatus(){
		return $this->status;
	}

	public function setStatus($value){
		$this->status = $value;
	}

	public function getTipe(){
		return $this->tipe;
	}

	public function setTipe($value){
		$this->tipe = $value;
	}

	public function getTeam(){
		return $this->team;
	}

	public function setTeam($value){
		$this->team = $value;
	}

}


class Absenz extends Petugas{
	var $jamMasuk;
	var $jamPulang;
	var $lokasi;
	var $lokasiPulang;
	var $tanggal;
	var $namaPerusahaan;

	public function getJamMasuk(){
		return $this->jamMasuk;
	}

	public function setJamMasuk($value){
		$this->jamMasuk = $value;
	}

	public function getJamPulang(){
		return $this->jamPulang;
	}

	public function setJamPulang($value){
		$this->jamPulang = $value;
	}

	public function getLokasiMasuk(){
		return $this->lokasi;
	}

	public function setLokasiMasuk($value){
		$this->lokasi = $value;
	}

	public function getLokasiPulang(){
		return $this->lokasiPulang;
	}

	public function setLokasiPulang($value){
		$this->lokasiPulang = $value;
	}

	public function getTanggal(){
		return $this->tanggal;
	}

	public function setTanggal($value){
		$this->tanggal = $value;
	}
	
	public function getNamaPerusahaan(){
		return $this->namaPerusahaan;
	}

	public function setNamaPerusahaan($value){
		$this->namaPerusahaan = $value;
	}

}

?>