<?php
include_once 'Koneksi.php';
class Pendaftar extends Koneksi{
	var $dbHost;

	function __construct(){
		$this->dbHost = $this->bukaKoneksi();
	}
	
	public function getSubcribers($date){
		$query = "SELECT * FROM `subscribers` WHERE `time_of_subcription` BETWEEN '$date 00:00:00' AND '$date 23:59:59'";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function inputSubscriber($objek){
		$query="INSERT INTO `subscribers` (`email`, `phone`, `name`, `time_of_subcription`) 
		VALUES (:email, :phone, :nama, CURRENT_TIMESTAMP);";
		$statement = $this->dbHost->prepare($query);

		$statement->bindParam(':email',$objek['email'],PDO::PARAM_STR,35);
		$statement->bindParam(':phone',$objek['phone'],PDO::PARAM_STR,15);
		$statement->bindParam(':nama',$objek['name'],PDO::PARAM_STR,75);
		
		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
	}

	public function editStatusPaket($status,$id){
		$query="UPDATE `paket_training` SET `status_paket` = :status WHERE `paket_training`.`id` = :id;";
		$statement = $this->dbHost->prepare($query);

		$statement->bindParam(':id',$id,PDO::PARAM_STR,35);
		$statement->bindParam(':status',$status,PDO::PARAM_INT,2);
		if ($statement->execute()){
			return true;
		}else{
			return false;
		}

	}

	public function fetchDataPaketTraining(){
		$query="SELECT `id`, `nama_item`, `harga`, `price_for`, `paket_hari`, `status_paket`,`keterangan`,`keterangan_lain` FROM `paket_training`,`status_paket` WHERE `paket_training`.`status_paket`=`status_paket`.`status_id`";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function fetchDeskripsiAgenda(){
		$query="SELECT * FROM `desc_agenda_kategori`";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function getJumlahCustQuot(){
		$query = "SELECT COUNT(id) AS jumlah FROM `quotation` WHERE `id` LIKE 'WS/FAC/C-%'";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();

		$result = $statement->fetch(PDO::FETCH_OBJ);
		return $result->jumlah;
	}

	public function updateQuotationStatus($id){
		$query="UPDATE `quotation` SET `status` = '1' WHERE `quotation`.`id` = :id;";
		$statement = $this->dbHost->prepare($query);

		$statement->bindParam(':id',$id,PDO::PARAM_STR,35);
		if ($statement->execute()){
			return true;
		}else{
			return false;
		}

	}

	public function quotationInput($objek){
		$query="INSERT INTO `quotation` (`id`, `nama_personal`, `nama_usaha`, `subjek`, `produk_id`, `biaya_transport`, `quotation_title`,`q_notes`, `email`, `email_cc`, `attachments`, `petugas_id`, `time_sent`,`jumlah_hari`,`transport_qty`)
		VALUES (:id, :personal, :usaha, :subjek, :produkID, :transport, :quotTitle, :notes, :email, :cc, :lampiran, :petugas, CURRENT_TIMESTAMP,:qty,:transqty);";

		$statement = $this->dbHost->prepare($query);

		$statement->bindParam(':id',$objek->id,PDO::PARAM_STR,35);
		$statement->bindParam(':personal',$objek->nama_personal,PDO::PARAM_STR,40);
		$statement->bindParam(':usaha',$objek->nama_usaha,PDO::PARAM_STR,120);
		$statement->bindParam(':subjek',$objek->subjek,PDO::PARAM_STR,75);
		$statement->bindParam(':produkID',$objek->paket,PDO::PARAM_STR,75);
		$transport = str_replace(',', '', $objek->biaya_transport);
		$statement->bindParam(':transport',$transport,PDO::PARAM_STR,9);
		$statement->bindParam(':quotTitle',$objek->q_title,PDO::PARAM_STR,100);
		$statement->bindParam(':notes',$objek->notes,PDO::PARAM_STR,700);
		$statement->bindParam(':email',$objek->email,PDO::PARAM_STR,125);
		$statement->bindParam(':lampiran',$objek->lampiran,PDO::PARAM_STR,125);
		$statement->bindParam(':cc',$objek->cc,PDO::PARAM_STR,400);
		$statement->bindParam(':petugas',$objek->petugas,PDO::PARAM_STR,125);
		$statement->bindParam(':qty',$objek->myqty,PDO::PARAM_INT,3);
		$statement->bindParam(':transqty',$objek->qtytrans,PDO::PARAM_INT,3);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}

	}


	public function addSales($objek){
		$query="INSERT INTO `data_sales` (`id`, `nama`, `department`, `telepon`, `email`)
		VALUES (:id, :nama, :department, :telepon, :email)";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':id',$objek->id,PDO::PARAM_STR,35);
		$statement->bindParam(':nama',$objek->nama,PDO::PARAM_STR,40);
		$statement->bindParam(':department',$objek->institusi,PDO::PARAM_STR,40);
		$statement->bindParam(':telepon',$objek->telepon,PDO::PARAM_STR,15);
		$statement->bindParam(':email',$objek->email,PDO::PARAM_INT,125);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}

	}


	public function inputDataPaket($objek){
		$tauah="custom";
		$query = "INSERT INTO `paket_training`(`id`, `nama_item`, `harga`, `price_for`, `paket_hari`) VALUES (:id,:namaItem,:harga,:priceFor,:paketHari)";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':id',$objek->id,PDO::PARAM_STR,12);
		$statement->bindParam(':namaItem',$objek->namaPaket,PDO::PARAM_STR,80);
		$statement->bindParam(':harga',$objek->harga,PDO::PARAM_INT,10);
		$statement->bindParam(':priceFor',$tauah,PDO::PARAM_STR,25);
		$statement->bindParam(':paketHari',$objek->paketHari,PDO::PARAM_INT,2);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
	}

	public function editQuotation($objek){
		$query="UPDATE `quotation` SET `nama_personal`='".$objek->nama_personal."',`nama_usaha`='".$objek->nama_usaha."',`subjek`='".$objek->subjek."',`produk_id`='".$objek->paket."',`biaya_transport`=".str_replace(',', '', $objek->biaya_transport).",`quotation_title`='".$objek->title."',`q_notes`='".$objek->notes."',`email`='".$objek->email."',`email_cc`='".$objek->cc."',`attachments`='".$objek->lampiran."',`petugas_id`='".$objek->petugas."', `jumlah_hari`='".$objek->myqty."',`transport_qty`='".$objek->qtytrans."' WHERE `id`='".$objek->id."';";
		$statement = $this->dbHost->prepare($query);
		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
	}

	public function editDataRequest($objek){
		$query="UPDATE order_request SET `nama_perusahaan`='$objek->namaUsaha',`usaha`='$objek->usaha',`contact_person`='$objek->cp',`telepon`='$objek->telepon',`email`='$objek->email',`jabatan`='$objek->jabatan',`pengguna`='$objek->pengguna',`versi`='$objek->versi',`agenda`='$objek->agenda',`tanggal_pelaksanaan`='$objek->tanggalPelaksanaan',`waktu`='$objek->waktu',`tempat`='$objek->tempat',`alamat`='$objek->alamat',`tempat_beli`='$objek->tempatBeli',`sales`='$objek->sales' WHERE `id`='$objek->id'";
		$statement = $this->dbHost->prepare($query);
		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
	}

	public function getNumberInvoice(){
		$query="SELECT COUNT(`no_invoice`) AS jumlah FROM `invoice`";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();

		$result = $statement->fetch(PDO::FETCH_OBJ);
		return $result->jumlah;
	}

	public function sendingInvoiceAndEmail($id){
		$query = "SELECT no_invoice, temp_agenda.id AS id_agenda, pendaftar.id AS id_daftar, tanggal, tempat,temp_agenda.alamat AS alamat, nama, agenda, temp_agenda.email AS cc, waktu, attachment, nama_personal, email_kontak, pendaftar.telepon AS telepon, nama_item, harga, paket_hari, qty, qty_trans, biaya_trans, deskripsi, metode_bayar, dp, diskon, temp_agenda.status AS status_agenda
			FROM `perusahaan`,`temp_agenda`,`invoice`,`pendaftar`,`paket_training`
			WHERE paket_training.id=pendaftar.id_paket AND perusahaan.id=pendaftar.nama_perusahaan AND pendaftar.id=temp_agenda.id_pendaftar AND temp_agenda.id_invoice=invoice.no_invoice AND temp_agenda.id_invoice =:id";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':id',$id,PDO::PARAM_STR,25);
		$statement->execute();

		return $statement->fetch(PDO::FETCH_OBJ);
	}


	public function getQuotationData($id){
		$query="SELECT `quotation`.`id` AS `id`, `nama_personal`, `nama_usaha`, `subjek`, `nama_item`, `jumlah_hari`, `harga`, `biaya_transport`, `transport_qty`, `quotation_title`, `q_notes`, `quotation`.`email` AS `email`, `email_cc`, `attachments`, `nama` AS `petugas`
		FROM `quotation`,`paket_training`,`users`
		WHERE quotation.produk_id=paket_training.id AND users.username=quotation.petugas_id AND quotation.id=:id;";

		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':id',$id,PDO::PARAM_STR,35);
		$statement->execute();

		return $statement->fetch(PDO::FETCH_OBJ);

	}

	public function getQuotationByID($id){
		$query = "SELECT * FROM `quotation` WHERE `id`='$id'";
		$statement = $this->dbHost->prepare($query);
		// $statement->bindParam(':id',$id,PDO::PARAM_STR,35);
		$statement->execute();

		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function fetchAllQuotation($num){
		$lower= ($num-1) * 30;
		$higher = $num * 30;

		$query="SELECT `quotation`.`id` AS `id`, `nama_personal`, `nama_usaha`, `subjek`, `nama_item`, `harga`, `biaya_transport`, `quotation_title`, `quotation`.`email` AS `email`, `email_cc`, `nama` AS `petugas`,`time_sent`, `quotation`.`status` AS `status`
		FROM `quotation`,`paket_training`,`users`
		WHERE quotation.produk_id=paket_training.id AND users.username=quotation.petugas_id ORDER BY time_sent DESC LIMIT 0,30;";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;

	}

	public function getJadwal2($id){
		$query="SELECT nama_item,qty,paket_hari FROM `pendaftar`,`paket_training` WHERE `pendaftar`.`id`='$id' AND pendaftar.id_paket=paket_training.id";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();

		return $statement->fetch(PDO::FETCH_OBJ);

	}

	public function getJadwal($id){
		$query = "SELECT `temp_agenda`.`id` AS `id`,`nama`,`nama_personal`,`id_pendaftar`,`email_kontak`,`tanggal`, `tempat`, `temp_agenda`.`alamat` AS `alamat`, `agenda`, `temp_agenda`.`email` AS cc, `waktu`,`attachment`,`temp_agenda`.`status` AS `status`,`temp_agenda`.`id_invoice` AS id_invoice FROM `pendaftar`,`perusahaan`,`temp_agenda` WHERE perusahaan.id=pendaftar.nama_perusahaan AND pendaftar.id=temp_agenda.id_pendaftar AND temp_agenda.id='$id'";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();

		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function fetchDataAgendaForSendingMail($id){
		$query="SELECT nama, email_kontak, tanggal, tempat, temp_agenda.alamat AS alamat, agenda, temp_agenda.email AS cc, waktu, attachment FROM temp_agenda,perusahaan,pendaftar WHERE temp_agenda.id='$id' AND temp_agenda.id_pendaftar=pendaftar.id AND pendaftar.nama_perusahaan=perusahaan.id";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();

		return $statement->fetch(PDO::FETCH_OBJ);

	}

	public function getPaketByID($id){
		$query="SELECT * FROM `paket_training` WHERE `id`='$id'";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();

		return $statement->fetch(PDO::FETCH_OBJ);

	}

	public function editStatusAgenda($id){
		$query="UPDATE `temp_agenda` SET `status` = '1' WHERE `temp_agenda`.`id` = '$id'";
		$statement = $this->dbHost->prepare($query);
		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
	}

	public function editStatAgendaAndInvoice($id,$ni){
		$query = "UPDATE invoice SET `status_invoice`='1', `sent_date`=NOW() WHERE `no_invoice`=:nomorInvoice;
				  UPDATE `temp_agenda` SET `status` = '1' WHERE `temp_agenda`.`id` = :aidi;";
		$statement = $this->dbHost->prepare($query);

		$statement->bindParam(':nomorInvoice',$ni,PDO::PARAM_STR,35);
		$statement->bindParam(':aidi',$id,PDO::PARAM_STR,35);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
	}

	public function fetchDataAgendaByID($id){
		$query = "SELECT * FROM temp_agenda WHERE id='$id'";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();

		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function fetchAllAgenda(){

		$query = "SELECT * FROM `daftar_agenda`";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function editTempJadwal($objek){
		$query = "UPDATE `temp_agenda` SET `tanggal`=:tanggal,`tempat`=:tempat,`alamat`=:alamat,`email`=:email,`waktu`=:waktu,`attachment`=:attachments WHERE `id`=:id";
		$statement = $this->dbHost->prepare($query);
		$tanggal=$this->removeLastString($objek->tanggal);
		$statement->bindParam(':tanggal',$tanggal,PDO::PARAM_STR,350);
		$statement->bindParam(':tempat',$objek->tempat,PDO::PARAM_STR,75);
		$statement->bindParam(':alamat',$objek->alamat,PDO::PARAM_STR,255);
		$statement->bindParam(':email',$objek->email,PDO::PARAM_STR,600);
		$statement->bindParam(':waktu',$objek->waktu,PDO::PARAM_STR,20);
		$statement->bindParam(':attachments',$objek->attachments,PDO::PARAM_STR,500);
		$statement->bindParam(':id',$objek->id,PDO::PARAM_STR,35);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}

	}

	public function jadwalWithInvoicePreview($id){
		$query = "SELECT no_invoice,tanggal, pendaftar.telepon AS telepon, tempat,temp_agenda.alamat AS alamat, nama, agenda, temp_agenda.email AS cc, waktu, attachment, nama_personal, email_kontak, nama_item, harga, paket_hari, qty, qty_trans, biaya_trans, deskripsi, metode_bayar, dp, diskon,sent_date
		FROM `perusahaan`,`temp_agenda`,`invoice`,`pendaftar`,`paket_training`
		WHERE paket_training.id=pendaftar.id_paket AND perusahaan.id=pendaftar.nama_perusahaan AND pendaftar.id=temp_agenda.id_pendaftar AND temp_agenda.id_invoice=invoice.no_invoice AND temp_agenda.id =:id";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':id',$id,PDO::PARAM_STR,35);

		$statement->execute();

		return $statement->fetch(PDO::FETCH_OBJ);

	}


	public function updateStatusInvoice($id,$value){
		$query = "UPDATE `invoice` SET `status_invoice` = '$value' WHERE `invoice`.`no_invoice` = :id";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':id',$id,PDO::PARAM_STR,25);
		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
	}

	public function getDaftarPendingInvoice($num){
		$cumi = $num-1;
		$lower= $cumi * 30;
		$higher = $num * 30;

		$query = "SELECT no_invoice, temp_agenda.id AS id_agenda, cari_hari_training(invoice.id_pendaftar) AS jumlah_training, pendaftar.id AS id_daftar,nama, deskripsi, temp_agenda.email AS cc, attachment, nama_personal, nama_item, harga, paket_hari, biaya_trans, qty, qty_trans, metode_bayar,status_invoice,`sent_date`
			FROM `perusahaan`,`temp_agenda`,`invoice`,`pendaftar`,`paket_training`
			WHERE paket_training.id=pendaftar.id_paket AND perusahaan.id=pendaftar.nama_perusahaan AND pendaftar.id=temp_agenda.id_pendaftar AND temp_agenda.id_invoice=invoice.no_invoice ORDER BY invoice.time_created DESC LIMIT $lower, 30";
			//LIMIT $lowerBound, $totalRowsSelected
		$statement = $this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function notSentInvoice(){
		$query = "SELECT no_invoice, temp_agenda.id AS id_agenda, cari_hari_training(invoice.id_pendaftar) AS jumlah_training, pendaftar.id AS id_daftar,nama, deskripsi, temp_agenda.email AS cc, attachment, nama_personal, nama_item, harga, paket_hari, biaya_trans, qty, qty_trans, metode_bayar,status_invoice,`sent_date`
			FROM `perusahaan`,`temp_agenda`,`invoice`,`pendaftar`,`paket_training`
			WHERE paket_training.id=pendaftar.id_paket AND perusahaan.id=pendaftar.nama_perusahaan AND pendaftar.id=temp_agenda.id_pendaftar AND temp_agenda.id_invoice=invoice.no_invoice AND status_invoice='0' ORDER BY invoice.time_created DESC";
			//LIMIT $lowerBound, $totalRowsSelected
		$statement = $this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function getPendingInvoice($id){
		$query = "SELECT no_invoice, temp_agenda.id AS id_agenda, cari_hari_training(invoice.id_pendaftar) AS jumlah_training, pendaftar.id AS id_daftar,nama, deskripsi, temp_agenda.email AS cc, attachment, nama_personal, nama_item, harga, paket_hari, biaya_trans, qty, qty_trans, metode_bayar,status_invoice,`sent_date`
			FROM `perusahaan`,`temp_agenda`,`invoice`,`pendaftar`,`paket_training`
			WHERE paket_training.id=pendaftar.id_paket AND perusahaan.id=pendaftar.nama_perusahaan AND pendaftar.id=temp_agenda.id_pendaftar AND temp_agenda.id_invoice=invoice.no_invoice AND invoice.no_invoice=:id ORDER BY invoice.time_created";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':id',$id,PDO::PARAM_STR,35);

		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);

	}

	public function srcPendingInvoice($objek){
		// $lower= ($num-1) * 30;
		// $higher = $num * 30;
		if (@$objek->st!='-') {
			$query = "SELECT no_invoice, temp_agenda.id AS id_agenda, pendaftar.id AS id_daftar,nama, deskripsi, temp_agenda.email AS cc, attachment, nama_personal, nama_item, harga, paket_hari, biaya_trans, qty, qty_trans, metode_bayar,status_invoice,`sent_date`
			FROM `perusahaan`,`temp_agenda`,`invoice`,`pendaftar`,`paket_training`
			WHERE perusahaan.nama LIKE '%$objek->np%' AND pendaftar.nama_personal LIKE '%$objek->nc%' AND paket_training.nama_item LIKE '%$objek->ni%' AND invoice.deskripsi LIKE '%$objek->di%' AND status_invoice='$objek->st' AND paket_training.id=pendaftar.id_paket AND perusahaan.id=pendaftar.nama_perusahaan AND pendaftar.id=temp_agenda.id_pendaftar AND temp_agenda.id_invoice=invoice.no_invoice ORDER BY invoice.time_created DESC;";
		} else {
			$query = "SELECT no_invoice, temp_agenda.id AS id_agenda, pendaftar.id AS id_daftar,nama, deskripsi, temp_agenda.email AS cc, attachment, nama_personal, nama_item, harga, paket_hari, biaya_trans, qty, qty_trans, metode_bayar,status_invoice,`sent_date`
			FROM `perusahaan`,`temp_agenda`,`invoice`,`pendaftar`,`paket_training`
			WHERE perusahaan.nama LIKE '%$objek->np%' AND pendaftar.nama_personal LIKE '%$objek->nc%' AND paket_training.nama_item LIKE '%$objek->ni%' AND invoice.deskripsi LIKE '%$objek->di%' AND paket_training.id=pendaftar.id_paket AND perusahaan.id=pendaftar.nama_perusahaan AND pendaftar.id=temp_agenda.id_pendaftar AND temp_agenda.id_invoice=invoice.no_invoice ORDER BY invoice.time_created DESC;";	
		}
		
		

		$statement = $this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;

	}

	public function fetchAllAgendaByPendaftarID($id){
		$query = "SELECT `temp_agenda`.`id` AS `id`,`id_invoice`, `nama`,`nama_personal`,`tanggal`, `agenda`,`temp_agenda`.`status` AS `status` FROM `temp_agenda`,`pendaftar`,`perusahaan` WHERE temp_agenda.id_pendaftar=pendaftar.id AND pendaftar.nama_perusahaan=perusahaan.id AND id_pendaftar='$id'";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function fetchAllAgendas(){

		$query = "SELECT `temp_agenda`.`id` AS `id`, `id_pendaftar`,`id_invoice`, `nama`,`nama_personal`,`tanggal`, `agenda`,`temp_agenda`.`status` AS `status` FROM `temp_agenda`,`pendaftar`,`perusahaan` WHERE temp_agenda.id_pendaftar=pendaftar.id AND pendaftar.nama_perusahaan=perusahaan.id";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function fetchAgendaByPerusahaanName($key) {
		$query = "SELECT `temp_agenda`.`id` AS `id`, `id_pendaftar`,`id_invoice`, `nama`,`nama_personal`,`tanggal`, `agenda`,`temp_agenda`.`status` AS `status` FROM `temp_agenda`,`pendaftar`,`perusahaan` WHERE perusahaan.nama LIKE '%$key%' AND temp_agenda.id_pendaftar=pendaftar.id AND pendaftar.nama_perusahaan=perusahaan.id";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function addDaftarAgenda($agenda,$kategori){
		$query = "INSERT INTO `daftar_agenda` (`nama_agenda`,`ag_kategori`) VALUES (:agenda,:kategori)";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':agenda',$agenda,PDO::PARAM_STR,500);
		$statement->bindParam(':kategori',$kategori,PDO::PARAM_STR,4);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
	}

	public function editAgenda($objek){
		if (isset($objek->status)&&!empty($objek->status)) {
			$query = "UPDATE `temp_agenda` SET `agenda` = :agenda, `status`='".$objek->status."' WHERE `temp_agenda`.`id` = :id";
		} else {
			$query = "UPDATE `temp_agenda` SET `agenda` = :agenda WHERE `temp_agenda`.`id` = :id";
		}

		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':id',$objek->id,PDO::PARAM_STR,35);
		$statement->bindParam(':agenda',$objek->agenda,PDO::PARAM_STR,75);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
	}

	public function addJadwalbaru($objek){
		$query ="INSERT INTO `temp_agenda` (`id`, `id_pendaftar`, `tanggal`, `tempat`, `alamat`, `email`, `waktu`, `attachment`)
		VALUES (:id, :idpendaftar, :tanggal, :tempat, :alamat, :email, :waktu, :attachments)";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':id',$objek->id,PDO::PARAM_STR,35);
		$statement->bindParam(':idpendaftar',$objek->idpendaftar,PDO::PARAM_STR,35);
		@$statement->bindParam(':tanggal',$this->removeLastString($objek->tanggal),PDO::PARAM_STR,350);
		$statement->bindParam(':tempat',$objek->tempat,PDO::PARAM_STR,75);
		$statement->bindParam(':alamat',$objek->alamat,PDO::PARAM_STR,255);
		$statement->bindParam(':email',$objek->cc,PDO::PARAM_STR,600);
		$statement->bindParam(':waktu',$objek->waktu,PDO::PARAM_STR,20);
		$statement->bindParam(':attachments',$objek->attachments,PDO::PARAM_STR,500);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
	}

	public function getDaftarAgenda($value){
		$query="SELECT * FROM `daftar_agenda` WHERE `nama_agenda` LIKE '%$value%'";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;

	}

	public function getPaketForWeb(){
		$query="SELECT * FROM `paket_training` WHERE `id` LIKE '%web%' ORDER BY harga ASC";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function completeRequest($objek){
		$query="INSERT INTO perusahaan (id,nama,email,alamat,kota,provinsi,telepon,jenis_usaha,ket_jenis_usaha,map)
		VALUES (:idPerusahaan,:namaUsaha,:emailUsaha,:alamat,:kota,:provinsi,:telepon,:jenisUsaha,:ketJenisUsaha,:map);
		UPDATE pendaftar SET nama_perusahaan = :aidiPerusahaan, status_transaksi=4 WHERE id=:idPendaftar;";
		$statement = $this->dbHost->prepare($query);

		$statement->bindParam(':idPerusahaan',$objek->id,PDO::PARAM_STR,40);
		$statement->bindParam(':namaUsaha',$objek->nama_usaha,PDO::PARAM_STR,150);
		$statement->bindParam(':emailUsaha',$objek->email_usaha,PDO::PARAM_STR,75);
		$statement->bindParam(':alamat',$objek->alamat,PDO::PARAM_STR,500);
		$statement->bindParam(':kota',$objek->kota,PDO::PARAM_STR,75);
		$statement->bindParam(':provinsi',$objek->provinsi,PDO::PARAM_STR,50);
		$statement->bindParam(':telepon',$objek->telepon,PDO::PARAM_STR,20);
		$statement->bindParam(':jenisUsaha',$objek->jenis_usaha,PDO::PARAM_STR,50);
		$statement->bindParam(':ketJenisUsaha',$objek->ket_jenis_usaha,PDO::PARAM_STR,255);
		$statement->bindParam(':map',$objek->map,PDO::PARAM_STR,255);
		$statement->bindParam(':aidiPerusahaan',$objek->id_usaha,PDO::PARAM_STR,255);
		$statement->bindParam(':idPendaftar',$objek->id_pendaftar,PDO::PARAM_STR,50);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}


	}

	public function editInvoiceByID($objek){
		$query="UPDATE `invoice` SET `deskripsi`=:deskripsi,`metode_bayar`=:metodebayar,`dp`=:dp,`diskon`=:diskon,`status_invoice`=:statusinvoice WHERE `no_invoice` =:noinvoice";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':deskripsi',$objek->deskripsi,PDO::PARAM_STR,75);
		$statement->bindParam(':metodebayar',$objek->metodebayar,PDO::PARAM_STR,25);
		$statement->bindParam(':dp',$objek->dp,PDO::PARAM_INT,9);
		$statement->bindParam(':diskon',$objek->diskon,PDO::PARAM_INT,8);
		$statement->bindParam(':statusinvoice',$objek->statusinvoice,PDO::PARAM_INT,1);
		$statement->bindParam(':noinvoice',$objek->noinvoice,PDO::PARAM_STR,25);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
	}

	public function fetchAllInvoiceData($id){
		if ($id=='') {
			$query="SELECT `no_invoice`, `id_pendaftar`, `nama_perusahaan`, `nama_paket`, `qty`, `lama_hari`, `deskripsi`, `metode_bayar`, `dp`, `diskon`, `status_invoice` FROM `invoice`,`pendaftar` WHERE `pendaftar`.`id`=`invoice`.`id_pendaftar`";
		} else {
			$query="SELECT `no_invoice`, `id_pendaftar`, `nama_perusahaan`, `nama_paket`, `qty`, `lama_hari`, `deskripsi`, `metode_bayar`, `dp`, `diskon`, `status_invoice` FROM `invoice`,`pendaftar` WHERE `pendaftar`.`id`=`invoice`.`id_pendaftar` AND `invoice`.`id_pendaftar`='$id'";
		}

		$statement=$this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function getInvoiceByIDandStatus($id){
		$query = "SELECT `no_invoice`, `nama_item`, `harga`, `id_pendaftar`, `nama_paket`, `qty`, `lama_hari`, `deskripsi`, `metode_bayar`, `dp`, `diskon`, `status_invoice` FROM `invoice`,`paket_training` WHERE `paket_training`.`id`=`invoice`.`nama_paket` AND `no_invoice`=:id";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':id',$id,PDO::PARAM_STR,25);

		$statement->execute();

		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function getInvoiceByID($id){
		$query = "SELECT * FROM `invoice` WHERE `no_invoice`=:id ";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':id',$id,PDO::PARAM_STR,25);


		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}


	public function countInvoiceNumber(){
		$jumlah = 0;
		$query = "SELECT COUNT(no_invoice) + 1 AS jumlah FROM invoice";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();

		$obj = $statement->fetch(PDO::FETCH_OBJ);
		$jumlah = intval($obj->jumlah);
		return $jumlah;
	}

	public function addInvoice($objek){

		$invoiceNum = "FAC/W/".date('Y').'/'.date('m').'/'.$this->countInvoiceNumber();
		$query = "INSERT INTO `invoice` (`no_invoice`, `id_pendaftar`,`deskripsi`, `metode_bayar`, `dp`, `diskon`, `status_invoice`)
		VALUES (:id, :idPendaftar, :deskripsi, :metodeBayar, :dp, :diskon, '0');
		UPDATE `temp_agenda` SET `id_invoice` = :id WHERE `temp_agenda`.`id` = :idJadwal;
		";

		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':id',$invoiceNum,PDO::PARAM_STR,25);
		$statement->bindParam(':idPendaftar',$objek->id,PDO::PARAM_STR,35);
		$statement->bindParam(':idJadwal',$objek->idJadwal,PDO::PARAM_STR,35);
		$statement->bindParam(':deskripsi',$objek->deskripsi,PDO::PARAM_STR,75);
		$statement->bindParam(':metodeBayar',$objek->metode,PDO::PARAM_STR,25);
		$statement->bindParam(':dp',$objek->dp,PDO::PARAM_INT,2);
		$statement->bindParam(':diskon',$objek->diskon,PDO::PARAM_INT,2);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
	}

	public function fetchPaketTraining(){
		$query="SELECT * FROM `paket_training` WHERE `status_paket`='1' OR `status_paket`='3'";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function getPaketTraining($id){
		$query = "SELECT * FROM `paket_training` WHERE `id`= '$id'";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}


	public function editForInvoice($objek){
		$tempData = array(
			'nama' => $objek->perusahaan,
			'usaha'=>$objek->usaha,
			'email'=>$objek->email,
			'alamat'=>$objek->alamatTraining,
			'paket'=>$objek->paket
		);
		$tempJadwal = array('waktu' => $objek->jam,'tanggal'=> $objek->tanggal,'tempat'=>$objek->tempat);
		$tempinvoice = array(
			'paket'=>$objek->paket,
			'jml_hari' => $objek->jmlhari,
			'transport'=> $objek->biayatransport,
			'x'=> $objek->x,
			'attach'=> $objek->attachments
		);


		$query="UPDATE pendaftar SET `nama_perusahaan`=:namaUsaha,`nama_personal`=:namaPersonal,`email_kontak`=:email,`alamat_perusahaan`=:alamat,`telepon`=:telp,`jabatan`=:jabatan,`data_jadwal`=:datajadwal,`invoice`=:invoice,`status_transaksi`=:statustrans,`jenis_pengguna`=:jnspengguna,`agenda_training`=:agenda,`tempat_beli_accurate`=:tempatbeli,`salesman_accurate`=:sales,`temp_invoice`=:tempinvoice WHERE id =:id";

		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':id',$objek->id,PDO::PARAM_STR,50);
		@$statement->bindParam(':namaUsaha',json_encode($tempData),PDO::PARAM_STR,500);
		$statement->bindParam(':namaPersonal',$objek->personal,PDO::PARAM_STR,50);
		$statement->bindParam(':email',$objek->email_kontak,PDO::PARAM_STR,75);
		$statement->bindParam(':alamat',$objek->alamat,PDO::PARAM_STR,255);
		$statement->bindParam(':telp',$objek->telepon,PDO::PARAM_STR,40);
		$statement->bindParam(':jabatan',$objek->jabatan,PDO::PARAM_STR,75);
		@$statement->bindParam(':datajadwal',json_encode($tempJadwal),PDO::PARAM_STR,255);
		$statement->bindParam(':invoice',$objek->invoice,PDO::PARAM_STR,25);
		$statement->bindParam(':statustrans',$objek->statustrans,PDO::PARAM_INT,1);
		$statement->bindParam(':jnspengguna',$objek->jenis_pengguna,PDO::PARAM_STR,5);
		//$statement->bindParam(':versi',$objek->versi,PDO::PARAM_STR,300);
		$statement->bindParam(':agenda',$objek->agenda,PDO::PARAM_STR,500);
		$statement->bindParam(':tempatbeli',$objek->tempatbeli,PDO::PARAM_STR,125);
		$statement->bindParam(':sales',$objek->sales,PDO::PARAM_STR,50);
		@$statement->bindParam(':tempinvoice',json_encode($tempinvoice),PDO::PARAM_STR,500);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}

	}

	public function masukDataRequest($objek){
		$tempData = array(
			'nama' => $objek->perusahaan,
			'usaha'=>$objek->usaha,
			'email'=>$objek->email,
			'alamat'=>$objek->alamatTraining,
			'paket'=>$objek->paket
		);
		$tempJadwal = array('waktu' => $objek->jam,'tanggal'=> $objek->tanggal);
		#
		$query="INSERT INTO `pendaftar` (id,nama_perusahaan,nama_personal,telepon,email_kontak,jabatan,jenis_pengguna,versi_accurate,agenda_training,data_jadwal,alamat_perusahaan,tempat_beli_accurate,salesman_accurate,tanggal_daftar,status_transaksi) VALUES('".md5($objek->perusahaan.'#'.date('Y-m-d'))."','".json_encode($tempData)."','".$objek->namacp."','".$objek->telepon."','".$objek->email."','".$objek->jabatan."','".$objek->pengguna."','".$objek->versi."','".$objek->agenda."','".json_encode($tempJadwal)."','".$objek->alamatTraining."','".$objek->tempatbeli."','".$objek->sales."','".date('Y-m-d')."',1)";

		$statement = $this->dbHost->prepare($query);
		if ($statement->execute()){
			return true;
		}else{
			return false;
		}

	}

	public function fetchDataRequestByID($id){
		$query = "SELECT * FROM pendaftar WHERE id='$id'";
		$statement = $this->dbHost->prepare($query);

		$statement=$this->dbHost->prepare($query);
		$statement->execute();

		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function fetchDataRequest($status){
		$query = "SELECT * FROM pendaftar WHERE status_transaksi=$status";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function masukData($objPendaftar){

		$query="INSERT INTO pendaftar(id,nama_perusahaan,nama_personal,email_perusahaan,email_kontak,alamat_perusahaan,alamat_lengkap,telepon,telepon_kantor,jabatan,jenis_usaha,rencana_tanggal_pelaksanaan,keterangan_jenis_usaha,jumlah_hari_training,jenis_pengguna,versi_accurate,agenda_training,tempat_beli_accurate,salesman_accurate,tanggal_daftar)
		VALUES(:id,:namaUsaha,:namaPersonal,:emailUsaha,:emailKontak,:alamat,:alamatLengkap,:teleponKontak,:teleponKantor,:jabatan,:jenisUsaha,:tanggalTraining,:ketJenisUsaha,:jumlahHari,:jenisPengguna,:versiAccurate,:agendaTraining,:tempatBeli,:salesman,:tanggalDaftar)";

		$statement = $this->dbHost->prepare($query);

		$statement->bindParam(':id',$objPendaftar->id,PDO::PARAM_STR,50);
		$statement->bindParam(':namaUsaha',$objPendaftar->namaKantor,PDO::PARAM_STR,50);
		$statement->bindParam(':namaPersonal',$objPendaftar->namaPersonal,PDO::PARAM_STR,50);

		$statement->bindParam(':emailUsaha',$objPendaftar->emailKantor,PDO::PARAM_STR,125);
		$statement->bindParam(':emailKontak',$objPendaftar->emailKontak,PDO::PARAM_STR,100);

		$statement->bindParam(':alamat',$objPendaftar->alamat,PDO::PARAM_STR,100);
		$statement->bindParam(':alamatLengkap',$objPendaftar->alamatLengkap,PDO::PARAM_STR,500);

		$statement->bindParam(':teleponKontak',$objPendaftar->telepon,PDO::PARAM_STR,20);
		$statement->bindParam(':teleponKantor',$objPendaftar->teleponKantor,PDO::PARAM_STR,20);

		$statement->bindParam(':jabatan',$objPendaftar->jabatan,PDO::PARAM_STR,75);
		$statement->bindParam(':jenisUsaha',$objPendaftar->jenisUsaha,PDO::PARAM_STR,50);
		$statement->bindParam(':ketJenisUsaha',$objPendaftar->ketJenisUsaha,PDO::PARAM_STR,500);

		$statement->bindParam(':tanggalTraining',$objPendaftar->tanggalTraining,PDO::PARAM_STR,12);
		$statement->bindParam(':agendaTraining',$objPendaftar->agendaTraining,PDO::PARAM_STR,40);
		$statement->bindParam(':jumlahHari',$objPendaftar->jumlahHari,PDO::PARAM_INT,3);
		$statement->bindParam(':jenisPengguna',$objPendaftar->jenisPengguna,PDO::PARAM_STR,5);
		$statement->bindParam(':versiAccurate',$objPendaftar->versiAccurate,PDO::PARAM_STR,300);
		$statement->bindParam(':tempatBeli',$objPendaftar->tmptBeliAccurate,PDO::PARAM_STR,125);
		$statement->bindParam(':salesman',$objPendaftar->salesmanAccurate,PDO::PARAM_STR,50);
		$statement->bindParam(':tanggalDaftar',$objPendaftar->tglDaftar,PDO::PARAM_STR,50);
		if ($statement->execute()) {
			//$dbHost = null;
			return true;
		}else{
			return false;
		}
	}

	public function fetchDataTraining($by){
		if ($by->cond=='month') {
			$query = "SELECT * FROM transaksi_harian WHERE MONTH(tanggal_daftar)=".intval($by->val);
		}elseif ($by->cond=='search') {
			if ($by->tanggalAwal==''&&$by->tanggalAkhir=='') {
				$query="SELECT * FROM `transaksi_harian` WHERE `nama_perusahaan` COLLATE UTF8_GENERAL_CI LIKE '%".$by->perusahaan."%' AND `usaha` LIKE '%".$by->usaha."%' AND `jenis_pengguna` LIKE '%".$by->pengguna."%' AND `versi_accurate` LIKE '%".$by->versi."%'";
			}else{
				$query="SELECT * FROM `transaksi_harian` WHERE `nama_perusahaan` COLLATE UTF8_GENERAL_CI LIKE '%".$by->perusahaan."%' AND `usaha` LIKE '%".$by->usaha."%' AND `jenis_pengguna` LIKE '%".$by->pengguna."%' AND `versi_accurate` LIKE '%".$by->versi."%' AND tanggal_daftar BETWEEN '".$by->tanggalAwal."' AND '".$by->tanggalAkhir."'";
			}

		}elseif ($by->cond=='jadwal') {
			$query = "SELECT * FROM `transaksi_harian` WHERE id='".$by->id."'";
		}else{
			$query = "SELECT * FROM transaksi_harian";
		}

		$statement=$this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function fetchDataTrainingCopy($by){
		if ($by->cond=='month') {
			$query = "SELECT * FROM transaksi_harian MONTH( `tanggal_daftar` )=".intval($by->val);
		}else{
			$query = "SELECT * FROM transaksi_harian";
		}

		return $query;
	}

	public function masukDataTransaksi($objek){


		$query="INSERT INTO `pendaftar` (`id`, `nama_perusahaan`, `nama_personal`, `email_kontak`,`telepon`, `jabatan`, `jenis_pengguna`, `versi_accurate`, `tempat_beli_accurate`, `salesman_accurate`, `tanggal_daftar`,`status_transaksi`,`id_paket`,`qty`,`biaya_trans`,`qty_trans`)
		VALUES ('".$objek->id."', '".$objek->namaPerusahaan."', '".$objek->namaPersonal."', '".$objek->email."', '".$objek->telepon."', '".$objek->jabatan."', '".$objek->jenisPengguna."', '".$this->removeLastString($objek->versiAccurate)."', '".$objek->sales."', '".$objek->tempatBeli."', '".$objek->tanggalDaftar."',".$objek->statusTransaksi.",'".$objek->paket."',".$objek->qty.",".$objek->transport.",".$objek->qtyTrans.")";
		$statement = $this->dbHost->prepare($query);
		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function masukDataJSON($objek){
		include_once 'Calendar.php';
		$kalender = new Calender();
		$tanggalz = explode(' # ', $objek->tglTraining);


		$id = md5(date('Y-m-d').'#'.$objek->namaPerusahaan);
		$query="INSERT INTO pendaftar(id,nama_perusahaan,nama_personal,email_perusahaan,email_kontak,alamat_perusahaan,alamat_lengkap,kota,provinsi,telepon,telepon_kantor,jabatan,jenis_usaha,rencana_tanggal_pelaksanaan,keterangan_jenis_usaha,jumlah_hari_training,jenis_pengguna,versi_accurate,agenda_training,tempat_beli_accurate,salesman_accurate,tanggal_daftar)
		VALUES(:id,:namaUsaha,:namaPersonal,:emailUsaha,:emailKontak,:alamat,:alamatLengkap,:kota,:provinsi,:teleponKontak,:teleponKantor,:jabatan,:jenisUsaha,:tanggalTraining,:ketJenisUsaha,:jumlahHari,:jenisPengguna,:versiAccurate,:agendaTraining,:tempatBeli,:salesman,:tanggalDaftar)";

		$statement = $this->dbHost->prepare($query);

		$statement->bindParam(':id',$id,PDO::PARAM_STR,50);
		$statement->bindParam(':namaUsaha',$objek->namaPerusahaan,PDO::PARAM_STR,50);
		$statement->bindParam(':namaPersonal',$objek->namaPersonal,PDO::PARAM_STR,50);

		$statement->bindParam(':emailUsaha',$objek->emailKantor,PDO::PARAM_STR,125);
		$statement->bindParam(':emailKontak',$objek->email,PDO::PARAM_STR,100);

		$statement->bindParam(':alamat',$objek->alamat,PDO::PARAM_STR,100);
		$statement->bindParam(':alamatLengkap',$objek->alamatLengkap,PDO::PARAM_STR,500);

		$statement->bindParam(':kota',$objek->kota,PDO::PARAM_STR,75);
		$statement->bindParam(':provinsi',$objek->provinsi,PDO::PARAM_STR,50);



		$statement->bindParam(':teleponKontak',$objek->telepon,PDO::PARAM_STR,60);
		$statement->bindParam(':teleponKantor',$objek->teleponKantor,PDO::PARAM_STR,60);

		$statement->bindParam(':jabatan',$objek->jabatan,PDO::PARAM_STR,75);
		$statement->bindParam(':jenisUsaha',$objek->jnsUsaha,PDO::PARAM_STR,50);
		$statement->bindParam(':ketJenisUsaha',$objek->ketJnsUsaha,PDO::PARAM_STR,500);

		$statement->bindParam(':tanggalTraining',$id,PDO::PARAM_STR,255);
		$statement->bindParam(':agendaTraining',$objek->agendaTraining,PDO::PARAM_STR,255);
		$statement->bindParam(':jumlahHari',$objek->hariTraining,PDO::PARAM_INT,3);
		$statement->bindParam(':jenisPengguna',$objek->jnsPengguna,PDO::PARAM_STR,5);
		$statement->bindParam(':versiAccurate',$objek->versiAccurate,PDO::PARAM_STR,300);
		$statement->bindParam(':tempatBeli',$objek->tempatBeli,PDO::PARAM_STR,125);
		$statement->bindParam(':salesman',$objek->salesman,PDO::PARAM_STR,50);
		$statement->bindParam(':tanggalDaftar',date('Y-m-d'),PDO::PARAM_STR,50);
		if ($statement->execute()) {

			for ($i=0; $i < count($tanggalz) ; $i++) {
				$kalender->insertData(date('Y-m-d'),$id);
			}

			return true;
		}else{
			return false;
		}
	}

	public function editData($objek){
		$query = "UPDATE pendaftar SET nama_personal='".$objek->namaPersonal."',
		telepon='".$objek->telepon."',
		email_kontak='".$objek->email."',
		jabatan='".$objek->jabatan."',
		jenis_pengguna='".$objek->jnspengguna."',
		versi_accurate='".$this->removeLastString(implode(" # ", $objek->jnsaccurate))."',
		id_paket='".$objek->paket."',
		qty=".$objek->qty.",
		biaya_trans=".str_replace(',', '', $objek->transport).",
		qty_trans=".$objek->qtyTrans.",
		tempat_beli_accurate='".$objek->tempatbeli."',
		salesman_accurate='".$objek->sales."'
		WHERE id='".$objek->id."'";
		$statement=$this->dbHost->prepare($query);

		if ($statement->execute()){
			//$dbHost = null;
			return true;
		}else{
			return false;
		}
	}



	public function autoCompleteNamaPerusahaan(){
		$query = "SELECT DISTINCT nama AS nama_perusahaan FROM perusahaan";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function selectByNamaPerusahaan($objek){
		//$query = "SELECT * FROM `pendaftar` WHERE `id`= 'cf5e6265c951d660b73306a97669a581'";
		$query = "SELECT DISTINCT `alamat_lengkap`,`alamat_perusahaan`,`email_perusahaan`,`jenis_usaha`,`keterangan_jenis_usaha`
		FROM pendaftar
		WHERE `nama_perusahaan` = '".$objek->namaPerusahaan."' AND `alamat_lengkap` <> ''";

		$statement=$this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function dataCombobox(){
		$date = strtotime(date('Y-m-d H:i:s') . ' -30 day');
		$query = "SELECT id,nama_perusahaan,rencana_tanggal_pelaksanaan AS tgl FROM pendaftar WHERE `rencana_tanggal_pelaksanaan` BETWEEN '".date('Y-m-d', $date)."' AND '".date('Y-m-d')."'";
		//SELECT * FROM `pendaftar` WHERE `rencana_tanggal_pelaksanaan` BETWEEN '2016-03-01' AND '2016-03-16'
		//$dbHost=$this->bukaKoneksi();
		$statement=$this->dbHost->prepare($query);

		$statement->execute();
		$datas=array();
		while ($result=$statement->fetch(PDO::FETCH_OBJ)) {
			$objek = new objekPendaftar();

			$objek->setID($result->id);
			$objek->setNamaKantor($result->nama_perusahaan);
			$objek->setTanggalTraining($result->tgl);
			array_push($datas, $objek);

		}
		return $datas;
	}

	public function deleteByID($id){
		$query="DELETE FROM pendaftar WHERE id='$id'";

		$statement=$this->dbHost->prepare($query);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}

	}

	public function selectDataPendaftarByThisMonth(){
		$query="SELECT * FROM `pendaftar` WHERE `rencana_tanggal_pelaksanaan` LIKE '%".date('Y-m')."%' ORDER BY rencana_tanggal_pelaksanaan DESC";

		$statement=$this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function dataPendaftar(){
		$query = "SELECT id,nama_perusahaan, nama_personal, alamat_perusahaan, alamat_lengkap, telepon, versi_accurate, jenis_pengguna, agenda_training, jumlah_hari_training, rencana_tanggal_pelaksanaan FROM pendaftar WHERE `rencana_tanggal_pelaksanaan` LIKE '%".date('Y-m-d')."%'";
		//$dbHost=$this->bukaKoneksi();
		$statement=$this->dbHost->prepare($query);

		$statement->execute();
		$datas=array();

		while ($result=$statement->fetch(PDO::FETCH_OBJ)) {
			$objek = new objekPendaftar();
			//$row = $result->jumlah;
			$objek->setID($result->id);
			$objek->setNamaKantor($result->nama_perusahaan);
			$objek->setNamaPersonal($result->nama_personal);

			$objek->setTelepon($result->telepon);
			$objek->setAlamat($result->alamat_perusahaan);
			$objek->setAlamatLengkap($result->alamat_lengkap);
			$objek->setTanggalTraining($result->rencana_tanggal_pelaksanaan);
			$objek->setJenisPengguna($result->jenis_pengguna);
			$objek->setVersiAccurate($result->versi_accurate);
			$objek->setAgendaTraining($result->agenda_training);
			$objek->setJumlahHari($result->jumlah_hari_training);

			array_push($datas, $objek);
			//array_push($datas, array($result->nama_perusahaan,$result->alamat_perusahaan,$result->telepon,$result->versi_accurate,$result->agenda_training,$result->jumlah_hari_training,$result->rencana_tanggal_pelaksanaan));
		}
		return $datas;
	}

	public function selectByID($id){
		$query = "SELECT * FROM pendaftar WHERE id = :id";

		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':id',$id, PDO::PARAM_STR,40);

		$statement->execute();

		return $statement->fetch(PDO::FETCH_OBJ) ;

	}

	public function selectByID2($id){
		$query = "SELECT * FROM transaksi_harian WHERE id = :id";

		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':id',$id, PDO::PARAM_STR,40);

		$statement->execute();

		return $statement->fetch(PDO::FETCH_OBJ) ;

	}

	public function selectForEditByID($id){
		$query = "SELECT `id`, cari_perusahaan(nama_perusahaan) AS `nama_perusahaan`, `nama_personal`,`email_kontak`, `telepon`, `jabatan`, `jenis_pengguna`, `versi_accurate`, `id_paket`,`qty`,`biaya_trans`, `qty_trans`, `tempat_beli_accurate`, `salesman_accurate` FROM `pendaftar` WHERE `id`=:id";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':id',$id, PDO::PARAM_STR,40);

		$statement->execute();

		return $statement->fetch(PDO::FETCH_OBJ) ;
	}

	public function selectForRequestByID($id){
		$query = "SELECT id,nama_perusahaan,cari_perusahaan(nama_perusahaan) AS nama_asli FROM pendaftar WHERE id = :id";

		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':id',$id, PDO::PARAM_STR,40);

		$statement->execute();

		return $statement->fetch(PDO::FETCH_OBJ) ;

	}

	public function searchPendaftar($nama,$kontak,$tanggal){
		$query = "SELECT id,nama_perusahaan,nama_personal,telepon,rencana_tanggal_pelaksanaan,alamat_perusahaan,alamat_lengkap,jenis_pengguna,versi_accurate,agenda_training,jumlah_hari_training FROM pendaftar
		WHERE nama_perusahaan LIKE :nama
		AND telepon LIKE :telepon
		AND rencana_tanggal_pelaksanaan LIKE :tanggal";

		//$dbHost=$this->bukaKoneksi();
		$statement=$this->dbHost->prepare($query);

		$statement->bindParam(':nama',$nama, PDO::PARAM_STR,125);
		$statement->bindParam(':telepon',$kontak, PDO::PARAM_STR,20);
		$statement->bindParam(':tanggal',$tanggal, PDO::PARAM_STR,20);

		$statement->execute();
		$datas=array();

		while ($result=$statement->fetch(PDO::FETCH_OBJ)) {
			$objek = new objekPendaftar();
			//$row = $result->jumlah;

			$objek->setID($result->id);
			$objek->setTelepon($result->telepon);
			$objek->setAlamat($result->alamat_perusahaan);
			$objek->setAlamatLengkap($result->alamat_lengkap);
			$objek->setNamaKantor($result->nama_perusahaan);
			$objek->setNamaPersonal($result->nama_personal);
			$objek->setTanggalTraining($result->rencana_tanggal_pelaksanaan);
			$objek->setJenisPengguna($result->jenis_pengguna);
			$objek->setVersiAccurate($result->versi_accurate);
			$objek->setAgendaTraining($result->agenda_training);
			$objek->setJumlahHari($result->jumlah_hari_training);

			array_push($datas, $objek);
			//array_push($datas, array($result->nama_perusahaan,$result->alamat_perusahaan,$result->telepon,$result->versi_accurate,$result->agenda_training,$result->jumlah_hari_training,$result->rencana_tanggal_pelaksanaan));
		}
		return $datas;

	}


	public function searchByMonth($bulan){
		$query="SELECT `pendaftar`.`id` AS id, perusahaan.nama AS `nama_perusahaan`, `nama_personal`, `email_kontak`, `pendaftar`.`telepon` AS telepon, `jabatan`, `jenis_pengguna`, `versi_accurate`, `agenda_training`, `tempat_beli_accurate`, `salesman_accurate`, `tanggal_daftar`,`agenda` AS `agenda`
		FROM `pendaftar`,`perusahaan`,temp_agenda
		WHERE perusahaan.id=pendaftar.nama_perusahaan AND temp_agenda.id_pendaftar=pendaftar.id AND MONTH(`tanggal_daftar`) = ".$bulan;

		$statement=$this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}


	public function testz(){
		$query="SELECT `id` , `rencana_tanggal_pelaksanaan` AS my_tanggal FROM `pendaftar` WHERE `nama_perusahaan` NOT LIKE '%fac%'";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

    public function removeLastString($value){
		if (substr($value,strlen($value)-3)==' # ') {
            return substr($value, 0,strlen($value)-3);
		}else{
			return $value;
		}
    }


}

class objekPendaftar{

	var $id;
	var $namaKantor;
	var $namaPersonal;
	var $emailKantor;
	var $emailKontak;
	var $teleponKantor;
	var $telepon;
	var $alamat;
	var $alamatLengkap;
	var $jabatan;
	var $jenisUsaha;
	var $ketJenisUsaha;
	var $tanggalTraining;
	var $jenisPengguna;
	var $versiAccurate;
	var $agendaTraining;
	var $tmptBeliAccurate;
	var $salesmanAccurate;
	var $tglDaftar;
	var $jumlahHari;

	public function getID(){
		return $this->id;
	}

	public function setID($value){
		$this->id = $value;
	}

	public function getNamaKantor(){
		return $this->namaKantor;
	}

	public function setNamaKantor($value){
		$this->namaKantor = $value;
	}

	public function getNamaPersonal(){
		return $this->namaPersonal;
	}

	public function setNamaPersonal($value){
		$this->namaPersonal = $value;
	}

	public function getEmailKantor(){
		return $this->emailKantor;
	}

	public function setEmailKantor($value){
		$this->emailKantor = $value;
	}


	public function getEmailKontak(){
		return $this->emailKontak;
	}

	public function setEmailKontak($value){
		$this->emailKontak = $value;
	}

	public function getTeleponKantor(){
		return $this->teleponKantor;
	}

	public function setTeleponKantor($value){
		$this->teleponKantor = $value;
	}

	public function getTelepon(){
		return $this->telepon;
	}

	public function setTelepon($value){
		$this->telepon = $value;
	}

	public function getAlamat(){
		return $this->alamat;
	}

	public function setAlamat($value){
		$this->alamat = $value;
	}

	public function getAlamatLengkap(){
		return $this->alamatLengkap;
	}

	public function setAlamatLengkap($value){
		$this->alamatLengkap = $value;
	}

	public function getJabatan(){
		return $this->jabatan;
	}

	public function setJabatan($value){
		$this->jabatan = $value;
	}

	public function getJenisUsaha(){
		return $this->jenisUsaha;
	}

	public function setJenisUsaha($value){
		$this->jenisUsaha = $value;
	}

	public function getKetJenisUsaha(){
		return $this->ketJenisUsaha;
	}

	public function setKetJenisUsaha($value){
		$this->ketJenisUsaha = $value;
	}

	public function getTanggalTraining(){
		return $this->tanggalTraining;
	}

	public function setTanggalTraining($value){
		$this->tanggalTraining = $value;
	}

	public function getJenisPengguna(){
		return $this->jenisPengguna;
	}

	public function setJenisPengguna($value){
		$this->jenisPengguna = $value;
	}

	public function getVersiAccurate(){
		return $this->versiAccurate;
	}

	public function setVersiAccurate($value){
		$this->versiAccurate = $value;
	}

	public function getAgendaTraining(){
		return $this->agendaTraining;
	}

	public function setAgendaTraining($value){
		$this->agendaTraining = $value;
	}

	public function getTmptBeliAccurate(){
		return $this->tmptBeliAccurate;
	}

	public function setTmptBeliAccurate($value){
		$this->tmptBeliAccurate = $value;
	}

	public function getSalesmanAccurate(){
		return $this->salesmanAccurate;
	}

	public function setSalesmanAccurate($value){
		$this->salesmanAccurate = $value;
	}

	public function getTglDaftar(){
		return $this->tglDaftar;
	}

	public function setTglDaftar($value){
		$this->tglDaftar = $value;
	}

	public function getJumlahHari(){
		return $this->jumlahHari;
	}

	public function setJumlahHari($value){
		$this->jumlahHari = $value;
	}

}

?>
