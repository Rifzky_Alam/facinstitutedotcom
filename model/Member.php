<?php 
include_once 'Koneksi.php';

class Member extends Koneksi{
	var $dbHost;

	function __construct(){
		$this->dbHost = $this->bukaKoneksi();
	}

	public function fetchHargaItem(){
		$query="SELECT `harga`,`paket_hari` FROM `paket_training` WHERE `id`=:id;";

		$statement = $this->dbHost->prepare($query);
		@$statement->bindParam(':id',$this->getItem(),PDO::PARAM_STR,12);
		$statement->execute();

		$hasil = $statement->fetch(PDO::FETCH_OBJ);
		$data = array('price' => $hasil->harga, 'hari' => $hasil->paket_hari );
		$this->setItem($data);
	}

	public function fetchNamaPaket(){
		$query="SELECT `nama_item`, `id` FROM `paket_training` WHERE `status_paket`='2';";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_OBJ);
		
		return $results;
	}

	public function ActivateClient(){
		$query = "UPDATE `client_user` SET `status` = '1' WHERE `client_user`.`username` = :username;";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':username',$this->getUsername(),PDO::PARAM_STR,35);
		if ($statement->execute()){
			return true;
		}else{
			return false;
		}

	}

	public function Verifikasi(){
		$query="SELECT * FROM `client_user`
		WHERE `password` = :password
		AND `status`=:status;";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':password',$this->getPassword(),PDO::PARAM_STR,35);
		@$statement->bindParam(':status',$this->getStatus(),PDO::PARAM_INT,1);

		$statement->execute();

		$results=$statement->fetch(PDO::FETCH_OBJ);
		return $results;

	}

	public function InputData(){
		$query="INSERT INTO `client_user` (`username`, `password`, `nama_client`, `jabatan`, `status`, `perusahaan`) 
		VALUES (:username, :password, :namalengkap, :jabatan, '0', '-');";
		$statement = $this->dbHost->prepare($query);
		@$statement->bindParam(':username',$this->getUsername(),PDO::PARAM_STR,60);
		@$statement->bindParam(':password',md5($this->getPassword()),PDO::PARAM_STR,35);
		@$statement->bindParam(':namalengkap',$this->getNamalengkap(),PDO::PARAM_STR,60);
		@$statement->bindParam(':jabatan',$this->getJabatan(),PDO::PARAM_STR,75);
		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
	}



	private $username;
	private $password;
	private $namausaha;
	private $alamatusaha;
	private $emailusaha;
	private $kota;
	private $provinsi;
	private $telepon;
	private $jenispengguna;
	private $versiaccurate;
	private $keteranganjenisusaha;
	private $tempatbeli;
	private $salesman;
	private $item;
	private $notif;
	private $ordernum;
	private $invoice;
	private $jadwal;
	private $namalengkap;
	private $jabatan;
	private $status;

	public function getUsername(){
	    return $this->username;
	}

	public function setUsername($username){
	    $this->username = $username;
	}

	public function getPassword(){
	    return $this->password;
	}

	public function setPassword($password){
	    $this->password = $password;
	}

	public function getNamausaha(){
	    return $this->namausaha;
	}

	public function setNamausaha($namausaha){
	    $this->namausaha = $namausaha;
	}

	public function getAlamatusaha(){
	    return $this->alamatusaha;
	}

	public function setAlamatusaha($alamatusaha){
	    $this->alamatusaha = $alamatusaha;
	}

	public function getEmailusaha(){
	    return $this->emailusaha;
	}

	public function setEmailusaha($emailusaha){
	    $this->emailusaha = $emailusaha;
	}

	public function getKota(){
	    return $this->kota;
	}

	public function setKota($kota){
	    $this->kota = $kota;
	}

	public function getProvinsi(){
	    return $this->provinsi;
	}

	public function setProvinsi($provinsi){
	    $this->provinsi = $provinsi;
	}

	public function getTelepon(){
	    return $this->telepon;
	}

	public function setTelepon($telepon){
	    $this->telepon = $telepon;
	}

	public function getJenispengguna(){
	    return $this->jenispengguna;
	}

	public function setJenispengguna($jenispengguna){
	    $this->jenispengguna = $jenispengguna;
	}

	public function getVersiaccurate(){
	    return $this->versiaccurate;
	}

	public function setVersiaccurate($versiaccurate){
	    $this->versiaccurate = $versiaccurate;
	}

	public function getKeteranganjenisusaha(){
	    return $this->keteranganjenisusaha;
	}

	public function setKeteranganjenisusaha($keteranganjenisusaha){
	    $this->keteranganjenisusaha = $keteranganjenisusaha;
	}

	public function getTempatbeli(){
	    return $this->tempatbeli;
	}

	public function setTempatbeli($tempatbeli){
	    $this->tempatbeli = $tempatbeli;
	}

	public function getSalesman(){
	    return $this->salesman;
	}

	public function setSalesman($salesman){
	    $this->salesman = $salesman;
	}

	public function getItem(){
	    return $this->item;
	}

	public function setItem($item){
	    $this->item = $item;
	}

	public function getNotif(){
	    return $this->notif;
	}

	public function setNotif($notif){
	    $this->notif = $notif;
	}

	public function getOrdernum(){
	    return $this->ordernum;
	}

	public function setOrdernum($ordernum){
	    $this->ordernum = $ordernum;
	}

	public function getInvoice(){
	    return $this->invoice;
	}

	public function setInvoice($invoice){
	    $this->invoice = $invoice;
	}

	public function getJadwal(){
	    return $this->jadwal;
	}

	public function setJadwal($jadwal){
	    $this->jadwal = $jadwal;
	}

	public function getNamalengkap(){
	    return $this->namalengkap;
	}

	public function setNamalengkap($namalengkap){
		$this->namalengkap = $namalengkap;
	}

	public function getJabatan(){
	    return $this->jabatan;
	}

	public function setJabatan($jabatan){
	    $this->jabatan = $jabatan;
	}

	public function getStatus(){
	    return $this->status;
	}

	public function setStatus($status){
	    $this->status = $status;
	}
	
}

?>