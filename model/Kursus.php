<?php 
include_once 'Koneksi.php';
date_default_timezone_set("Asia/Jakarta"); 

class Kursus extends Koneksi{
	
	function __construct(){
		$this->dbHost = $this->bukaKoneksi();
	}



	public function deleteJadwalKursus($value){
		$query="DELETE FROM `kursus_calendar` WHERE `id_jadwal`=:idjadwalcal;
				DELETE FROM `jadwal_kursus` WHERE `id_jadwal`=:idjadwal;";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':idjadwalcal',$value,PDO::PARAM_STR,35);
		@$statement->bindParam(':idjadwal',$value,PDO::PARAM_STR,35);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}

	}

	public function addStudent($objek){

		if ($objek->wa=='') {
			$objek->wa='n';
		}



		$query = "INSERT INTO `students` (`id`, `id_kursus`,`tanggal_daftar`, `nama`, `tempat_lahir`, `status`, `tanggal_lahir`, `gender`, `alamat`, `telepon_user`, `wa`, `email`, `pendidikan`, `perusahaan`, `alamat_usaha`) VALUES ('".$objek->id."', '".$objek->id_kursus."','".date('Y-m-d')."', '".$objek->nama."', '".$objek->tempat_lahir."','".$objek->status."', '".$objek->tanggal_lahir."', '".$objek->gender."', '".$objek->alamat."', '".$objek->telepon."', '".$objek->wa."', '".$objek->email."', '".$objek->pendidikan."', '".$objek->nama_usaha."', '".$objek->alamat_usaha."')";
		$statement=$this->dbHost->prepare($query);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}

	}

	public function updateStatus($obj){
		$query="UPDATE students SET status = '1' WHERE status='$obj->id'";
		$statement=$this->dbHost->prepare($query);
		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
	}

	public function addNamaKursus($objek){
		$query="INSERT INTO `kursus` (`id`,`nama_kursus`) VALUES ('".@md5($objek->namaKursus)."','".$objek->namaKursus."');";
		$statement=$this->dbHost->prepare($query);

		if ($statement->execute()) {
			return "<script>alert('Data ".$objek->namaKursus." berhasil disimpan!');</script>";
		}else{
			return "<script>alert('Data ".$objek->namaKursus." gagal disimpan');</script>";
		}		
	}

	public function addLokasiKursus($objek){
		$query="INSERT INTO `tempat_kursus` (`lokasi_kursus`,`map`) VALUES ('".$objek->lokasi."','".$objek->map."');";
		$statement=$this->dbHost->prepare($query);

		if ($statement->execute()) {
			return "<script>alert('Data ".$objek->lokasi." berhasil disimpan!');</script>";
		}else{
			return "<script>alert('Data ".$objek->lokasi." gagal disimpan');</script>";
		}
	}

	public function getDatas($jenis){
		if ($jenis=='nama_kursus') {
			$query="SELECT * FROM kursus";
		}elseif ($jenis=='tempat_kursus') {
			$query="SELECT * FROM tempat_kursus";
		}
		$statement=$this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function getJadwalKursus($value){
		$query="SELECT jadwal_kursus.id_jadwal AS id ,nama_kursus,lokasi_kursus,map,hari,jam,investasi,durasi,mulai_kursus 
        FROM jadwal_kursus
        INNER JOIN kursus ON kursus.id=jadwal_kursus.id_kursus
        INNER JOIN tempat_kursus ON tempat_kursus.id=jadwal_kursus.id_lokasi
        WHERE jadwal_kursus.id_jadwal= '$value'";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);

	}

	public function getArrayOfCal($id){
		$query="SELECT tanggal_jadwal FROM `kursus_calendar` WHERE `id_jadwal` = '$id'";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();
		$hasil = array();
		while ($foo=$statement->fetch(PDO::FETCH_OBJ)) {
			array_push($hasil, $this->benerinTanggal($foo->tanggal_jadwal));
		}

		return $hasil;		
	}

	public function inputKalender($id,$tanggal){
		$query="INSERT INTO `kursus_calendar` (`id_jadwal`, `tanggal_jadwal`) VALUES ('$id', '$tanggal')";
		$statement=$this->dbHost->prepare($query);
		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function inputJadwal($objek){
		$tgl = explode(" # ", $objek->tanggal);
		$query="INSERT INTO `jadwal_kursus` (`id_jadwal`, `id_kursus`, `id_lokasi`, `jam`, `investasi`, `durasi`,`hari`,`mulai_kursus`) VALUES ('".$objek->id."', '".$objek->nama_kelas."', '".$objek->lokasi."', '".$objek->jam."', '".$objek->investasi."', '".$objek->durasi."',:hari,'".$tgl[0]."')";

		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':hari',$objek->hari,PDO::PARAM_STR,75);

		if ($statement->execute()) {

			for ($i=0; $i < count($tgl); $i++) { 
				$this->inputKalender($objek->id,$tgl[$i]);
			}
				
			return "<script>alert('Data berhasil disimpan!');</script>";
		}else{
			return "<script>alert('Data gagal disimpan');</script>";
		}
	}


	public function getJadwal($bulan,$tahun){

		$query="SELECT jadwal_kursus.id_jadwal AS id ,nama_kursus,lokasi_kursus,map,hari,jam,investasi,durasi,DATE_FORMAT(mulai_kursus, '%W, %e %M %Y') AS mulai_kursus
        FROM jadwal_kursus
        INNER JOIN kursus ON kursus.id = jadwal_kursus.id_kursus
        INNER JOIN tempat_kursus ON tempat_kursus.id=jadwal_kursus.id_lokasi
        WHERE MONTH(jadwal_kursus.mulai_kursus)='".intval($bulan)."' AND YEAR(jadwal_kursus.mulai_kursus)='".intval($tahun)."' ORDER BY mulai_kursus ASC";
		
		$statement=$this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		//$dataz=json_encode($results);
		return $results;

	}

	public function getJadwalByID($id){
		$query="SELECT * FROM `jadwal_kursus` WHERE `id_jadwal`='$id'";
		$statement=$this->dbHost->prepare($query);
		//$statement->bindParam(':id',$id,PDO::PARAM_STR,35);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;

	}

	public function getDataForEmail($id){
		$query="SELECT `students`.`id` AS id, `nama_kursus` , `nama` , `status` , `tempat_lahir` , `tanggal_lahir` , `mulai_kursus` , `alamat` , `telepon_user` , `jam` , `lokasi_kursus`, `investasi` , `email` , `pendidikan` , `perusahaan` , `alamat_usaha` FROM `students` , `kursus` , `jadwal_kursus` , `tempat_kursus` WHERE students.id_kursus = jadwal_kursus.id_jadwal AND jadwal_kursus.id_kursus = kursus.id AND tempat_kursus.id = jadwal_kursus.id_lokasi AND students.id = '".$id."'";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function getDataRegister($id){
		$query="SELECT `students`.`id` AS id, `nama_kursus`, `nama`, `status`, `tempat_lahir`, `tanggal_lahir`, `mulai_kursus`, `gender`, `alamat`, `telepon_user`,`jam`, `wa`, `email`, `pendidikan`, `perusahaan`, `alamat_usaha` FROM `students`,`kursus`,`jadwal_kursus` WHERE students.id_kursus = jadwal_kursus.id_jadwal AND jadwal_kursus.id_kursus=kursus.id AND students.id='".$id."'";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function getDataKursusByID($id){
		$query = "SELECT * FROM kursus WHERE id=:id";
		$statement=$this->dbHost->prepare($query);

		$statement->bindParam(':id',$id,PDO::PARAM_STR,35);
		$statement->execute();

		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function getLokasiKursusByID($id){
		$query = "SELECT * FROM tempat_kursus WHERE id=:id";
		$statement=$this->dbHost->prepare($query);

		$statement->bindParam(':id',$id,PDO::PARAM_INT,4);
		$statement->execute();

		return $statement->fetch(PDO::FETCH_OBJ);
	}


	public function deleteDataNamaKursus($id){
		$query = "DELETE FROM kursus WHERE id=:id";
		$statement=$this->dbHost->prepare($query);

		$statement->bindParam(':id',$id,PDO::PARAM_STR,35);
		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function deleteLokasiKursus($id){
		$query = "DELETE FROM tempat_kursus WHERE id=:id";
		$statement=$this->dbHost->prepare($query);

		$statement->bindParam(':id',$id,PDO::PARAM_INT,4);
		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}


	public function getDataAllRegister(){
		$query="SELECT `students`.`id` AS id, `status`, `nama_kursus`, `nama`,`tanggal_daftar`, `tempat_lahir`, `tanggal_lahir`, `gender`, `alamat`, `telepon_user`, `wa`, `email`, `pendidikan`, `perusahaan`, `alamat_usaha` FROM `students`,`kursus`,`jadwal_kursus` WHERE students.id_kursus = jadwal_kursus.id_jadwal AND jadwal_kursus.id_kursus=kursus.id";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function benerinTanggal($str){
		$hasil = explode('-', $str);
		return $hasil[2].'-'.$hasil[1].'-'.$hasil[0];
	}

	public function getNamaBulan($bulan){
		$bulan=intval($bulan);
		$tahunDepan = date('Y') + 1;
		if ($bulan==1) {
			return "Januari ".date('Y');
		}elseif ($bulan==2) {
			return "Februari ".date('Y');
		}elseif ($bulan==3) {
			return "Maret ".date('Y');
		}elseif ($bulan==4) {
			return "April ".date('Y');
		}elseif ($bulan==5) {
			return "Mei ".date('Y');
		}elseif ($bulan==6) {
			return "Juni ".date('Y');
		}elseif ($bulan==7) {
			return "Juli ".date('Y');
		}elseif ($bulan==8) {
			return "Agustus ".date('Y');
		}elseif ($bulan==9) {
			return "September ".date('Y');
		}elseif ($bulan==10) {
			return "Oktober ".date('Y');
		}elseif ($bulan==11) {
			return "November ".date('Y');
		}elseif ($bulan==12) {
			return "Desember ".date('Y');
		}elseif ($bulan == 13) {
			return "Januari ". $tahunDepan;
		}else{
			return "Error reading current month!";
		}

	}




}


 ?>