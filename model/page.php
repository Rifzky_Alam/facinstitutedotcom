<?php 
date_default_timezone_set("Asia/Jakarta"); 
include_once 'Koneksi.php';
class Page extends Koneksi{
	
	function __construct(){
		$this->dbHost = $this->bukaKoneksi();
	}


	public function fetchLogData(){
		$tanggal = date('Y-m-d');
		$query="SELECT nama,log_action,log_ip,log_time FROM log_data,users WHERE users.username=log_data.username AND log_time BETWEEN '$tanggal 00:00:00' AND '$tanggal 23:59:59'";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function sendMarketingMessage($by){
		if ($by->sesi=='1') {
			# code...
		}elseif ($by->sesi=='2') {
			# code...
		}
		$query="SELECT COUNT(`log_action`) AS jumlah FROM `log_data` WHERE `log_action`='added a new transaction' AND log_time BETWEEN '".date('Y-m-d')." 09:00:00' AND '".date('Y-m-d')." 13:00:00'";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function fetchSearchLogData($user,$tanggalAwal,$tanggalAkhir){
		if ($user=='') {
			$query="SELECT nama,log_action,log_ip,log_time FROM log_data,users WHERE users.username=log_data.username AND log_data.username LIKE '%$user%' AND log_time BETWEEN '$tanggalAwal 00:00:00' AND '$tanggalAkhir 23:59:59'";
		}else{
			$query="SELECT nama,log_action,log_ip,log_time FROM log_data,users WHERE users.username=log_data.username AND log_data.username='$user' AND log_time BETWEEN '$tanggalAwal 00:00:00' AND '$tanggalAkhir 23:59:59'";
		}

		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function fetchAll(){
		$tanggal = date('Y-m-d');
		$query="SELECT * FROM viewer WHERE `time_stamp` BETWEEN '$tanggal 00:00:00' AND '$tanggal 23:59:59'";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function addViewer($objek){
		$query="INSERT INTO `viewer` (`ip`,`os`,`browser`, `mobile`, `city`, `region`, `country`, `time_stamp`) VALUES (:ip, :os, :browser, :mobile, :city, :region, :country, CURRENT_TIMESTAMP)";
		$statement=$this->dbHost->prepare($query);

		$statement->bindParam(':ip',$objek->ip,PDO::PARAM_STR,65);
		$statement->bindParam(':os',$objek->os,PDO::PARAM_STR,65);
		$statement->bindParam(':browser',$objek->browser,PDO::PARAM_STR,65);
		$statement->bindParam(':mobile',$objek->mobile,PDO::PARAM_STR,65);
		$statement->bindParam(':city',$objek->city,PDO::PARAM_STR,75);
		$statement->bindParam(':region',$objek->region,PDO::PARAM_STR,75);
		$statement->bindParam(':country',$objek->country,PDO::PARAM_STR,40);
		
		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
	}


	public function addLogForLogin($user,$actionz,$ip){
		$query="INSERT INTO log_data (username,log_action,log_ip) VALUES(:user,:actionz,:myIp)";
		$statement=$this->dbHost->prepare($query);

		$statement->bindParam(':user',$user,PDO::PARAM_STR,65);
		$statement->bindParam(':actionz',$actionz,PDO::PARAM_STR,65);
		$statement->bindParam(':myIp',$ip,PDO::PARAM_STR,65);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
	}


	public function uploadFile($tmp_name,$folder,$fileName){
		if(move_uploaded_file($tmp_name, $folder.$fileName)){
			return true;
		}else{
			return false;
		}
	
	}

	public function isOpen(){

		if (!(date('H') >= 6) || (date('H') >= 10) ) {
			return "Closed";
		}else{
			return "Open";
		}
	}

}


?>