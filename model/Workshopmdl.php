<?php 
date_default_timezone_set("Asia/Jakarta"); 
include_once 'Koneksi.php';
class Workshopmdl extends Koneksi{
	public static $instance = null;
	private $dbHost;
	// public static function getTodomdlInstance(){
 //    	if(self::$instance==null){
 //    		self::$instance = new Todomdl();
 //    	}
 //    	return self::$instance;
 //    }

	function __construct(){
		$this->dbHost = $this->bukaKoneksi();
	}

	public function getWorkshopname($name){
		// $tobesearched = "%".$name."%";
		$query = "SELECT p.nama,p.id,p.alamat,p.ket_jenis_usaha 
		FROM perusahaan p 
		WHERE p.p_status='23'
		AND p.nama LIKE '%".$name."%'";
        $statement=$this->dbHost->prepare($query);
        // echo $query;
        // $statement->bindParam(':nama',$tobesearched,PDO::PARAM_STR,35);
		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function GetDataToInsertNewTrans($idcust){
		$query = "SELECT nama_cust, p.nama AS nama_usaha,telp_cust,email_cust,marketing_nama
		FROM new_customer nc
		LEFT JOIN new_marketing nm ON nm.marketing_id=nc.marketing_id
		LEFT JOIN perusahaan p ON p.id=nc.id_perusahaan
		WHERE nc.id_cust=:custid";
		$statement=$this->dbHost->prepare($query);

        // $statement->bindParam(':iditem',$idpaket,PDO::PARAM_STR,35);
        $statement->bindParam(':custid',$idcust,PDO::PARAM_STR,35);
        $statement->execute();
        return $statement->fetch(PDO::FETCH_ASSOC);
	}

	public function getCustomer($value){
		$query = "SELECT id_cust 
		FROM new_customer nc
		WHERE (nc.email_cust='".$value."' OR nc.telp_cust='".$value."')";	
		$statement=$this->dbHost->prepare($query);
		$statement->execute();
		$hasil = $statement->fetch(PDO::FETCH_ASSOC);
		return $hasil['id_cust'];
	}

}


?>