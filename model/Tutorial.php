<?php

include_once 'Koneksi.php';


class ControlTutorial extends Koneksi{

	function __construct(){
		$this->dbHost = $this->bukaKoneksi();
	}


	public function countAllTutorial(){
		$query="SELECT COUNT(`id`) AS jumlah FROM `tutorial`";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();

		$hasil = $statement->fetch(PDO::FETCH_OBJ);
		return $hasil->jumlah;
	}

	public function GetInstalledTags($source){
		$query = "SELECT `fi_t_id`,`fi_t_source`,`fi_t_idtag`,`tags_desc`.`tags_txt` FROM `tags`
		INNER JOIN `tags_desc`
		ON `tags_desc`.`tags_id`=`tags`.`fi_t_idtag`
		WHERE `tags`.`fi_t_source`='$source'";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function GetTagName($tagname){
		$query = "SELECT `tags_id`, `tags_txt` FROM `tags_desc` WHERE `tags_txt` LIKE '%$tagname%';";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	}

	// @API (url="/api/tutsvc?tagname={value}")
	public function GetTagNameAPI($tagname){
		$query = "SELECT `id`,`judul`,SUBSTR(removehtml(`tutorial`.`isi`),1,300) AS `konten`,`tanggal`,`tags_txt`, `thumbnail`,
		(
    		SELECT GROUP_CONCAT(tags_desc.tags_txt SEPARATOR ' # ')
    		FROM tags
    		INNER JOIN tags_desc
    		ON tags.fi_t_idtag=tags_desc.tags_id
    		WHERE tags.fi_t_source=id
		) AS all_tags
		FROM `tags_desc` 
		INNER JOIN tags
		ON tags.fi_t_idtag=tags_desc.tags_id
		INNER JOIN tutorial
		ON tutorial.id=tags.fi_t_source
		WHERE tags_txt = :tagname;";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':tagname',$tagname,PDO::PARAM_STR,1200);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	}

	//@url: "/api/tutsvc"
	public function ListTagsAPI(){
		$query = "SELECT `tags_id`,`tags_txt`,
		(
			SELECT COUNT(`fi_t_id`) FROM `tags` WHERE `tags`.`fi_t_idtag`=`tags_id`
		) AS `total_of_use`
		FROM `tags_desc`
		ORDER BY `total_of_use` DESC";

		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	}

	public function inserData($objek){
		include_once 'MyXML.php';
		$xml = new MyXML();
		$xml->setDir('/home/facinsti/public_html/');
		$xml->setNamafile('sitemap.xml');

		$query = "INSERT INTO `tutorial` (`id`,`judul`, `brief_desc`, `isi`, `tanggal`, `tags`)
		 VALUES (:aidi,:judul, :bdesc, :isi, :tanggal, :tag)";
		$aidi = md5($objek['judul'].'-'.date('Y-m-d'));
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':aidi',$aidi,PDO::PARAM_STR,500);
		$statement->bindParam(':judul',$objek['judul'],PDO::PARAM_STR,500);
		$statement->bindParam(':bdesc',$objek['bdesc'],PDO::PARAM_STR,255);
		$statement->bindParam(':isi',$objek['isi'],PDO::PARAM_STR,8000);
		@$statement->bindParam(':tanggal',date('Y-m-d'),PDO::PARAM_STR,15);
		$statement->bindParam(':tag',$objek['tags'],PDO::PARAM_STR,255);

		if ($statement->execute()){
			$xml->SitemapAddTutorial($aidi);
			return true;
		}else{
			return false;
		}

	}

	public function coba(){
		return "coba";
	}

	public function updateTutorialwithimage($objek){ //@url value="/administarasi/cms/edit-tutorial"
		$query = "UPDATE `tutorial` SET `judul`=:judul,`isi`=:isi,`tanggal_edit`=:tanggal,`brief_desc`=:bdesc,`thumbnail`=:thb WHERE `id`=:id";
		$statement = $this->dbHost->prepare($query);

		$statement->bindParam(':judul',$objek['judul'],PDO::PARAM_STR,500);
		$statement->bindParam(':isi',$objek['isi'],PDO::PARAM_STR,8000);
		@$statement->bindParam(':tanggal',date('Y-m-d'),PDO::PARAM_STR,15);
		$statement->bindParam(':bdesc',$objek['bdesc'],PDO::PARAM_STR,255);
		$statement->bindParam(':thb',$objek['thumbnail'],PDO::PARAM_STR,255);
		$statement->bindParam(':id',$objek['id'],PDO::PARAM_INT,8);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
	}

	public function updateTutorial($objek){ //@url value="/administarasi/cms/edit-tutorial"
		$query = "UPDATE `tutorial` SET `judul`=:judul,`isi`=:isi,`tanggal_edit`=:tanggal,`brief_desc`=:bdesc WHERE `id`=:id";
		$statement = $this->dbHost->prepare($query);

		$statement->bindParam(':judul',$objek['judul'],PDO::PARAM_STR,500);
		$statement->bindParam(':isi',$objek['isi'],PDO::PARAM_STR,8000);
		@$statement->bindParam(':tanggal',date('Y-m-d'),PDO::PARAM_STR,15);
		$statement->bindParam(':bdesc',$objek['bdesc'],PDO::PARAM_STR,255);
		$statement->bindParam(':id',$objek['id'],PDO::PARAM_INT,8);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
	}

	public function deleteTutorial($id){
		$query = "DELETE FROM tutorial WHERE id=:id";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':id',$id,PDO::PARAM_INT,8);
		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
	}

	public function selectAllData($offset,$limit){

		$start = $offset * $limit;
		$query = "SELECT `id` AS `aidi`, `judul`, `isi`, `tanggal`, brief_desc  AS `tags`, `page_counter`, `tanggal_edit`,
		(
			SELECT GROUP_CONCAT(tags_desc.tags_txt SEPARATOR ', ') 
    		FROM `tutorial`
    		INNER JOIN `tags` ON `tags`.`fi_t_source`=`tutorial`.`id`
    		INNER JOIN `tags_desc` ON `tags_desc`.`tags_id`=`tags`.`fi_t_idtag`
    		WHERE `tags`.`fi_t_source`=`aidi`
		) AS `new_tags`
		FROM `tutorial`
		ORDER BY `tutorial`.`tanggal` DESC LIMIT $start, $limit";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function searchAllData($key){
		$key = '%'.$key.'%';
		$query="SELECT `id` AS `aidi`, `judul`, `isi`, `tanggal`, `tags`, `page_counter`, `tanggal_edit` FROM `tutorial` WHERE isi LIKE :isi OR judul LIKE :key ORDER BY `tanggal` DESC ";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':isi',$key,PDO::PARAM_STR,255);
		$statement->bindParam(':key',$key,PDO::PARAM_STR,255);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	// @url: /api/tutsvc?src
	public function searchAllDataAPI($key){
		$key = '%'.$key.'%';
		$query="SELECT `id` AS `aidi`, `judul`, `isi`, `tanggal`, `tags`, `page_counter`, `thumbnail`,`tanggal_edit` FROM `tutorial` WHERE isi LIKE :isi OR judul LIKE :key ORDER BY `tanggal` DESC ";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':isi',$key,PDO::PARAM_STR,255);
		$statement->bindParam(':key',$key,PDO::PARAM_STR,255);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	}

	public function searchJudul($key){
		$key = '%'.$key.'%';
		$query="SELECT `id` AS `aidi`, `judul`, `isi`, `tanggal`, `brief_desc` AS `tags`, `page_counter`, `tanggal_edit`,
		(
			SELECT GROUP_CONCAT(tags_desc.tags_txt SEPARATOR ', ') 
    		FROM `tutorial`
    		INNER JOIN `tags` ON `tags`.`fi_t_source`=`tutorial`.`id`
    		INNER JOIN `tags_desc` ON `tags_desc`.`tags_id`=`tags`.`fi_t_idtag`
    		WHERE `tags`.`fi_t_source`=`aidi`
		) AS `new_tags`
		FROM `tutorial`
		WHERE judul LIKE :key ORDER BY `tanggal` DESC ";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':key',$key,PDO::PARAM_STR,255);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}


	public function fetchAPI(){
		$query="SELECT id,tanggal,judul FROM tutorial";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}
	public function fetchIsiAPI($id){
		$query="SELECT isi FROM tutorial where id=$id";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function selectById($id){
		$query = "SELECT `id`, `judul`, `brief_desc`, `isi`, `thumbnail`, `tanggal`, `tags`, `page_counter`, `tanggal_edit` FROM tutorial WHERE id='$id'";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();

		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function selectJudulAndID(){
		$query="SELECT id,judul FROM tutorial ORDER BY page_counter ASC LIMIT 0, 5";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

}


?>
