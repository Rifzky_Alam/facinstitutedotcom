<?php 
include_once 'Koneksi.php';
date_default_timezone_set("Asia/Jakarta"); 

class Quotation extends Koneksi{
	
	private $noid;
	private $title;
	private $subject;
	private $notes;
	private $idtransaksi;
	private $tanggal;
	private $petugas;
	private $cc;
	private $data;
	private $folder;

	function __construct(){
		$this->dbHost = $this->bukaKoneksi();
	}


	function GetIDQuotation(){
		$query = "SELECT COUNT(`qtn_id`) AS jumlah FROM `new_quotation`";

		$statement=$this->dbHost->prepare($query);

		$statement->execute();
		$hasil = $statement->fetch(PDO::FETCH_OBJ);

		return 'FAC/WQ'.date('y').'/'.date('m').'/'.$this->GetNumberQuotation($hasil->jumlah);
	}

	function GetNumberQuotation($value){
		$value = intval($value) + 1;
		if ($value<10) {
			return '00000'.$value;
		}elseif ($value<100) {
			return '0000'.$value;
		}elseif ($value<1000) {
			return '000'.$value;
		}elseif ($value<10000) {
			return '00'.$value;
		}elseif ($value<100000) {
			return '0'.$value;
		}elseif ($value<1000000) {
			return $value;
		}
	}
	
	public function GetItemTransaksi($idtrans){
	    $query = "SELECT paket_training.nama_item,new_trans_items.itm_paket AS id_item,new_trans_items.no AS id,paket_training.paket_hari AS total_hari
        FROM new_trans_items 
        INNER JOIN paket_training ON paket_training.id=new_trans_items.itm_paket
        WHERE new_trans_items.itm_trans_id=:transid";
        $statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':transid',$idtrans, PDO::PARAM_STR,35);
	    $statement->execute();
		
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function GetDataTransaksi($idtrans){
	    $query = "SELECT perusahaan.nama AS nama_usaha, nama_cust, email_cust, new_customer.gender_cust,new_customer.marketing_id,
        new_transaksi.trans_id
        FROM new_transaksi
        INNER JOIN new_customer ON new_customer.id_cust=new_transaksi.trans_cust_id
        INNER JOIN perusahaan ON perusahaan.id = new_transaksi.trans_id_usaha
        WHERE new_transaksi.trans_id=:transid";
	    $statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':transid',$idtrans, PDO::PARAM_STR,35);
	    $statement->execute();
		
		return $statement->fetch(PDO::FETCH_ASSOC);
	}

	public function TableQuotations(){
		$query = "SELECT `qtn_id`, `qtn_id_trans`,`new_customer`.`nama_cust` AS `nama_cust`, `nama` AS `nama_usaha`, `qtn_subject`, `qtn_title`, `qtn_notes`, `qtn_date`, `qtn_status`, `inv_no_invoice`,
		(
    		SELECT GROUP_CONCAT(nama_item SEPARATOR '##') 
    		FROM paket_training 
    		INNER JOIN `new_trans_items` ON `new_trans_items`.`itm_paket`=`paket_training`.`id`
    		WHERE `new_trans_items`.`itm_trans_id`=`qtn_id_trans`
		) AS `items`
		FROM `new_quotation` 
		INNER JOIN `new_transaksi` 
		ON `new_transaksi`.`trans_id`=`new_quotation`.`qtn_id_trans`
		INNER JOIN `perusahaan` 
		ON `new_transaksi`.`trans_id_usaha`=`perusahaan`.`id`
		INNER JOIN `new_customer` 
		ON `new_transaksi`.`trans_cust_id` = `new_customer`.`id_cust`
		LEFT JOIN `view_data_invoice` 
		ON `view_data_invoice`.`inv_id_trans`=`new_quotation`.`qtn_id_trans`
		ORDER BY `qtn_date` DESC;";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();
		// return $query;
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function SearchQuotation($data){
		// $data = json_decode($data);
		$query = "SELECT `qtn_id`, `qtn_id_trans`,`new_customer`.`nama_cust` AS `nama_cust`, `nama` AS `nama_usaha`, `qtn_subject`, `qtn_title`, `qtn_notes`, `qtn_date`, `qtn_status`, `inv_no_invoice`,
		(
    		SELECT GROUP_CONCAT(nama_item SEPARATOR '##') 
    		FROM paket_training 
    		INNER JOIN `new_trans_items` ON `new_trans_items`.`itm_paket`=`paket_training`.`id`
    		WHERE `new_trans_items`.`itm_trans_id`=`qtn_id_trans`
		) AS `items`
		FROM `new_quotation` 
		INNER JOIN `new_transaksi` 
		ON `new_transaksi`.`trans_id`=`new_quotation`.`qtn_id_trans`
		INNER JOIN `perusahaan` 
		ON `new_transaksi`.`trans_id_usaha`=`perusahaan`.`id`
		INNER JOIN `new_customer` 
		ON `new_transaksi`.`trans_cust_id` = `new_customer`.`id_cust`
		LEFT JOIN `view_data_invoice` 
		ON `view_data_invoice`.`inv_id_trans`=`new_quotation`.`qtn_id_trans`
        WHERE `qtn_id` LIKE '%".$data->id."%' 
        AND `new_customer`.`nama_cust` LIKE '%".$data->nama_cust."%'
        AND `perusahaan`.`nama` LIKE '%".$data->nama_usaha."%' ";
        if (count($data->tgl)>0) {
        	$query .= "AND `qtn_date` BETWEEN '".$data->tgl[0]."' AND '".$data->tgl[1]."' ";
        }
        if ($data->items!='') {
        	$query .= "HAVING `items` LIKE '%".$data->items."%' ";
        }
        $query .= "ORDER BY `qtn_date`;";
        $statement=$this->dbHost->prepare($query);
		$statement->execute();
		// return $query;
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function InputData(){
		$query = "INSERT INTO `new_quotation` (`qtn_id`, `qtn_id_trans`, `qtn_subject`, `qtn_title`, `qtn_notes`, `qtn_date`) 
		VALUES (:id, :idtrans, :subjek, :title, :notes, :tanggal);";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':id',$this->GetIDQuotation(), PDO::PARAM_STR,35);
		@$statement->bindParam(':idtrans',$this->idtransaksi, PDO::PARAM_STR,35);
		@$statement->bindParam(':subjek',$this->subject, PDO::PARAM_STR,75);
		@$statement->bindParam(':title',$this->title, PDO::PARAM_STR,75);
		@$statement->bindParam(':notes',$this->notes, PDO::PARAM_STR,800);
		@$statement->bindParam(':tanggal',$this->tanggal, PDO::PARAM_STR,35);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function EditData(){
		$query = "UPDATE `new_quotation` 
		SET `qtn_subject`=:subjek,`qtn_title`=:title,`qtn_notes`=:notes,`qtn_date`=:tanggal 
		WHERE `qtn_id`=:id;";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':id',$this->noid, PDO::PARAM_STR,35);
		@$statement->bindParam(':subjek',$this->subject, PDO::PARAM_STR,75);
		@$statement->bindParam(':title',$this->title, PDO::PARAM_STR,75);
		@$statement->bindParam(':notes',$this->notes, PDO::PARAM_STR,800);
		@$statement->bindParam(':tanggal',$this->tanggal, PDO::PARAM_STR,35);
		

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function EditStatus(){
		$query = "UPDATE `new_quotation` SET `qtn_status` = '1' WHERE `new_quotation`.`qtn_id` = :id;";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':id',$this->noid, PDO::PARAM_STR,35);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function DisplayPDF(){
		$query = "SELECT `perusahaan`.`nama` AS `nama_usaha`,`nama_cust`, `qtn_id_trans`,`trans_jenis`, `perusahaan`.`email` AS `email`, 
		`email_cust`, `qtn_id`, `qtn_subject`, `qtn_title`, `qtn_notes`, `qtn_date`, `users`.`nama` AS `petugas`,`trans_id` 
		FROM `new_quotation`
        INNER JOIN `new_transaksi` ON `new_transaksi`.`trans_id`=`new_quotation`.`qtn_id_trans`
        INNER JOIN `new_customer` ON `new_customer`.`id_cust`=`new_transaksi`.`trans_cust_id`
        INNER JOIN `perusahaan` ON `perusahaan`.`id` = `new_transaksi`.`trans_id_usaha`
        INNER JOIN `users` ON `users`.`username`=`new_transaksi`.`trans_petugas`
		WHERE  new_quotation.qtn_id=:id;";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':id',$this->noid, PDO::PARAM_STR,35);
		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function ViewTable($offset,$limit){

		$query = "SELECT `qtn_id`, `qtn_id_trans`, `nama` AS `nama_usaha`,`nama_cust`, `qtn_date`, `qtn_status`,
		(SELECT GROUP_CONCAT(`nama_item` SEPARATOR ' # ') FROM
 		`paket_training`,`new_trans_items`,`new_transaksi` 
 		WHERE `new_transaksi`.`trans_id`=`qtn_id_trans` 
 		AND `new_transaksi`.`trans_id`=`new_trans_items`.`itm_trans_id` 
 		AND `paket_training`.`id`=`new_trans_items`.`itm_paket`) AS `items`
		FROM `new_quotation`,`perusahaan`,`new_customer`,`new_transaksi`
		WHERE `new_quotation`.`qtn_id_trans`=`new_transaksi`.`trans_id` 
		AND `new_transaksi`.`trans_cust_id`=`new_customer`.`id_cust`
		AND `new_customer`.`id_perusahaan` = `perusahaan`.`id` LIMIT $offset,$limit;";
		$statement=$this->dbHost->prepare($query);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);

	}

	public function FetchItems(){
		$query="SELECT `nama_item`, `harga`, `itm_qty` 
		FROM `new_trans_items` AS `item`,`paket_training` AS `barang`,`new_quotation` AS `quot` 
		WHERE `item`.`itm_paket`=`barang`.`id` AND `item`.`itm_trans_id`= `quot`.`qtn_id_trans` AND `quot`.`qtn_id` = :id;";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':id',$this->noid, PDO::PARAM_STR,35);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function FetchByID(){
		$query = "SELECT * FROM `new_quotation` WHERE `qtn_id`=:id;";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':id',$this->noid, PDO::PARAM_STR,35);
		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);

	}

	public function FetchDataSurat(){
		$query = "SELECT `emd_to`, `emd_cc`, `emd_attachments` 
		FROM `new_mail_data` 
		WHERE `emd_source`= :id;";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':id',$this->noid,PDO::PARAM_STR,35);
		
		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);

	}

	public function EditSurat(){
		$query = "UPDATE `new_mail_data` SET `emd_to`=:to,`emd_cc`=:cc,`emd_attachments`=:attachments WHERE `emd_source`=:id;";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':id',$this->noid,PDO::PARAM_STR,45);
		@$statement->bindParam(':to',$this->data->to,PDO::PARAM_STR,125);
		@$statement->bindParam(':cc',$this->data->cc,PDO::PARAM_STR,300);
		@$statement->bindParam(':attachments',$this->data->attachments,PDO::PARAM_STR,500);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function InputSurat(){
		$query = "INSERT INTO `new_mail_data` (`emd_source`, `emd_to`, `emd_cc`, `emd_attachments`) 
		VALUES (:noinvoice, :to, :cc, :attachments);";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':noinvoice',$this->noid,PDO::PARAM_STR,45);
		@$statement->bindParam(':to',$this->data->to,PDO::PARAM_STR,125);
		@$statement->bindParam(':cc',$this->data->cc,PDO::PARAM_STR,300);
		@$statement->bindParam(':attachments',$this->data->attachments,PDO::PARAM_STR,500);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}	

	public function CekLampiran($value){
		if ($value=='') {
			return '';
		} else {
			$result = implode('##', $value);
			return $result;
		}
	}

	public function CekDataLampiran($value){
		if ($value=='') {
			return '';
		} else {
			$result = explode("##", $value);
			return $result;
		}
	}

	// @url: "/administrasi/quotation/api/?req=table"
	public function ListTable(){
		$query = "SELECT `qtn_id`, `qtn_id_trans`, `qtn_subject`, `qtn_title`, `qtn_notes`, `qtn_date`, `qtn_status`,`inv_no_invoice`,`inv_status`
		FROM `new_quotation` 
		INNER JOIN `new_transaksi` ON `new_transaksi`.`trans_id`=`new_quotation`.`qtn_id_trans`
		LEFT JOIN `new_invoice` ON `new_invoice`.`inv_id_trans`=`new_transaksi`.`trans_id`;";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function CountAttachment(){
		$query = "SELECT COUNT(`emd_source`) AS `jumlah` FROM `new_mail_data` WHERE `emd_source`=:nopenawaran";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':nopenawaran',$this->noid,PDO::PARAM_STR,35);
		
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_OBJ);
		return $result->jumlah;
	}

	public function getID(){
		return $this->noid;
	}

	public function setID($value){
		return $this->noid = $value;
	}

	public function getTitle(){
		return $this->title;
	}

	public function setTitle($value){
		return $this->title = $value;
	}

	public function getSubject(){
		return $this->subject;
	}

	public function setSubject($value){
		return $this->subject = $value;
	}

	public function getNotes(){
		return $this->notes;
	}

	public function setNotes($value){
		return $this->notes = $value;
	}

	public function getIdtransaksi(){
		return $this->idtransaksi;
	}

	public function setIdtransaksi($value){
		return $this->idtransaksi = $value;
	}

	public function getPetugas(){
		return $this->petugas;
	}

	public function setPetugas($value){
		return $this->petugas = $value;
	}

	public function getCC(){
		return $this->cc;
	}

	public function setCC($value){
		return $this->cc = $value;
	}

	public function getDate(){
		return $this->tanggal;
	}

	public function setDate($value){
		return $this->tanggal = $value;
	}

	public function getData(){
		return $this->data;
	}

	public function setData($value){
		return $this->data = $value;
	}


	public function getFolder(){
		return 'quotations/';
	}


}



?>