<?php 
include_once 'Koneksi.php';
class Forum extends Koneksi{
	var $id;
	var $judul;
	var $question;
	var $answer;
	var $tag;
	var $kategori;
	var $timestamp;
	var $vote;
	var $attachments;
	var $petugas;
	var $id_answer;

	// ID answer
	public function getIdAnswer(){
		return $this->id_answer;
	}

	public function setIdAnswer($value){
		$this->id_answer = $value;	
	}


	// petugas
	public function getPetugas(){
		return $this->petugas;
	}

	public function setPetugas($value){
		$this->petugas = $value;	
	}

	// attachments
	public function getAttachments(){
		return $this->id;
	}

	public function setAttachments($value){
		$this->attachments = $value;	
	}

	// ID
	public function getID(){
		return $this->id;
	}

	public function setID($value){
		$this->id = $value;	
	}

	// judul
	public function getJudul(){
		return $this->judul;
	}

	public function setJudul($value){
		$this->judul=$value;	
	}

	// question
	public function getQuestion(){
		return $this->question;
	}

	public function setQuestion($value){
		$this->question=$value;	
	}

	// answer
	public function getAnswer(){
		return $this->answer;
	}

	public function setAnswer($value){
		$this->answer=$value;	
	}

	//tag
	public function getTag(){
		return $this->tag;
	}

	public function setTag($value){
		$this->tag=$value;	
	}

	//kategori
	public function getKategori(){
		return $this->kategori;
	}

	public function setKategori($value){
		$this->kategori=$value;	
	}

	// timestamp
	public function getTimestamp(){
		return $this->timestamp;
	}

	public function setTimestamp($value){
		$this->timestamp=$value;	
	}

	//vote
	public function getVote(){
		return $this->vote;
	}

	public function setVote($value){
		$this->vote=$value;
	}

	function __construct(){
		$this->dbHost = $this->bukaKoneksi();
	}

	public function fillAttr($value){
		$this->setID(@$value['id']);
		$this->setJudul(@$value['judul']);
		$this->setQuestion(@$value['question']);
		$this->setTag(@$value['tag']);
		$this->setAnswer(@$value['answer']);
		$this->setKategori(@$value['kategori']);
		$this->setTimestamp(@$value['timestamp']);
		$this->setVote(@$value['vote']);
		$this->setAttachments(@$value['attachments']);
		$this->setPetugas(@$value['petugas']);
	}

	public function EditAnswer(){
		$query = "UPDATE `faq_answers` SET `answer` = :answer WHERE `faq_answers`.`id` = :id;";
		$statement = $this->dbHost->prepare($query);

		@$statement->bindParam(':answer',$this->getAnswer(),PDO::PARAM_STR,8000);
		@$statement->bindParam(':id',$this->getIdAnswer(),PDO::PARAM_STR,35);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}

	}

	public function insertNewRow(){
		$query="INSERT INTO `faq` (`id`, `title`, `question`, `tag`, `categories`, `attachments`, `petugas_id`)
		VALUES (:id, :title, :question, :tag, :categories, :attachments,:petugas);";
		$statement = $this->dbHost->prepare($query);

		@$statement->bindParam(':id',$this->getID(),PDO::PARAM_STR,500);
		@$statement->bindParam(':title',$this->getJudul(),PDO::PARAM_STR,250);
		@$statement->bindParam(':question',$this->getQuestion(),PDO::PARAM_STR,8000);
		@$statement->bindParam(':tag',$this->getTag(),PDO::PARAM_STR,100);
		@$statement->bindParam(':categories',$this->getKategori(),PDO::PARAM_INT,75);
		@$statement->bindParam(':attachments',$this->getAttachments(),PDO::PARAM_STR,125);
		@$statement->bindParam(':petugas',$this->getPetugas(),PDO::PARAM_STR,40);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
	}

	public function addVote(){
		$objek= $this->getVote();
		$query="INSERT INTO `faq_vote` (`id`, `id_question`, `user`) VALUES (:id, :idQuestion, :petugas);";
		$statement = $this->dbHost->prepare($query);

		$id=md5($objek->petugas.'-'.$objek->id_question);
		@$statement->bindParam(':id',$id,PDO::PARAM_STR,35);
		@$statement->bindParam(':idQuestion',$objek->id_question,PDO::PARAM_STR,35);
		@$statement->bindParam(':petugas',$objek->petugas,PDO::PARAM_STR,8000);
		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
	}

	public function addAnswer(){
		$objek= $this->getAnswer();
		$query="INSERT INTO `faq_answers` (`id`, `id_faq`, `answer`, `user`) 
		VALUES (:id, :aidi, :answer, :petugas);";
		$statement = $this->dbHost->prepare($query);

		$id=md5($objek->petugas.'-'.$objek->jawaban);
		@$statement->bindParam(':id',$id,PDO::PARAM_STR,35);
		@$statement->bindParam(':aidi',$objek->id_question,PDO::PARAM_STR,35);
		@$statement->bindParam(':answer',$objek->jawaban,PDO::PARAM_STR,8000);
		@$statement->bindParam(':petugas',$objek->petugas,PDO::PARAM_STR,40);
		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
	}

	public function fetchAllCategories(){
		$query="SELECT * FROM `forum_kategori`";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function fetchAllData(){
		$query="SELECT `id`, `title`, `question`, `tag`, `nama_kategori` AS `categories`, `attachments`, `views`, `time_post`, `nama` 
		FROM `faq`,`users`,`forum_kategori` 
		WHERE faq.petugas_id=users.username AND faq.categories=forum_kategori.id_kategori ORDER BY `time_post` DESC;";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function searchForum(){
		$jdl = '%'.$this->getJudul().'%';
		$tagg = '%'.$this->getTag().'%';
		$kaateegoorii = '%'.$this->getKategori().'%';
		$naamaa = '%'.$this->getPetugas().'%';

		$query="SELECT `id`, `title`, `question`, `tag`, `nama_kategori` AS `categories`, `attachments`, `views`, `time_post`, `nama` 
		FROM `faq`,`users`,`forum_kategori` 
		WHERE faq.petugas_id=users.username AND `title` LIKE :title AND `tag` LIKE :tags AND `categories` LIKE :kategori AND `nama` LIKE :nama ORDER BY `time_post` DESC;";
		$statement = $this->dbHost->prepare($query);
		@$statement->bindParam(':title',$jdl,PDO::PARAM_STR,250);
		@$statement->bindParam(':tags',$tagg,PDO::PARAM_STR,100);
		@$statement->bindParam(':kategori',$kaateegoorii,PDO::PARAM_STR,75);
		@$statement->bindParam(':nama',$naamaa,PDO::PARAM_STR,50);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function getAllAnswer(){
		$query = "SELECT `id`, `id_faq`, `answer`, `user`, `nama`, `time_replied` 
		FROM `faq_answers`,`users` 
		WHERE `faq_answers`.`id_faq`=:id AND `faq_answers`.`user`=`users`.`username` ORDER BY `time_replied` ASC;";
		$statement = $this->dbHost->prepare($query);
		@$statement->bindParam(':id',$this->getAnswer(),PDO::PARAM_STR,35);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function getEditAnswer(){
		$query = "SELECT `faq_answers`.`id` AS `id`,`title`,`question`, `tag`, `categories`,`id_faq`, `answer`, `time_post`,`nama`,`user` 
		FROM `faq_answers`,`faq`,`users` 
		WHERE `faq_answers`.`id`=:id AND `faq_answers`.`id_faq`=`faq`.`id` AND `users`.`username`=`faq`.`petugas_id`; ";
		$statement = $this->dbHost->prepare($query);
		@$statement->bindParam(':id',$this->getAnswer(),PDO::PARAM_STR,35);

		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
		
	}

	public function getJumlahJawaban(){
		$query="SELECT COUNT(id_faq) AS jumlah FROM `faq_answers` WHERE `id_faq`=:id;";
		$statement = $this->dbHost->prepare($query);
		@$statement->bindParam(':id',$this->getAnswer(),PDO::PARAM_STR,35);

		$statement->execute();
		$hasil = $statement->fetch(PDO::FETCH_OBJ);
		return $hasil->jumlah;
	}

	public function getJumlahVote(){
		$query="SELECT COUNT(id) AS jumlah FROM `faq_vote` WHERE `id_question`=:id;";
		$statement = $this->dbHost->prepare($query);
		@$statement->bindParam(':id',$this->getAnswer(),PDO::PARAM_STR,35);

		$statement->execute();
		$hasil = $statement->fetch(PDO::FETCH_OBJ);
		return $hasil->jumlah;
	}

	public function selectById(){
		$query = "SELECT `id`, `title`, `question`, `tag`, `categories`, `attachments`, `views`, `time_post`, `nama`,`email` 
		FROM `faq`,`users` WHERE `faq`.`id`=:id AND `faq`.`petugas_id`=`users`.`username`;"; //petugas non aktif belum di tambahkan
		$statement = $this->dbHost->prepare($query);
		@$statement->bindParam(':id',$this->getID(),PDO::PARAM_STR,35);

		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

}


?>