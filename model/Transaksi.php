<?php 
include_once 'Koneksi.php';
class Transaksi extends Koneksi{
	var $dbHost;
	private $transid;
	private $custid;
	private $usahaid;
	private $accurateid;
	private $jenistransaksi;
	private $dp;
	private $ppn;
	private $utklokasi;
	private $utkwaktu;
	private $discount;
	private $notes;
	private $status;
	private $tanggal;
	private $agenda;
	private $items;
	private $petugas;
	private $folder;
	
	private $provinsi;
	private $kota;
	private $kecamatan;
	
	private $data;
	

	//constructor
	function __construct(){
		$this->dbHost = $this->bukaKoneksi();
	}
	
	public function getDataPembayaranInvoice($idinv){
	    $query = "SELECT `lseqid`, `ssource`, `ipayamt`, `ipaymeth`, masdet.ket, `ddate`, `istatus`, `dupdate_at`, users.nama AS pic 
        FROM `new_inv_payment` 
        LEFT JOIN users ON users.username=new_inv_payment.sstaff
        LEFT JOIN (
            SELECT `dt_flag` AS id, `dt_desc` AS ket FROM desc_tbl WHERE desc_tbl.dt_relate_tbl='new_invoice.inv_py_mtd'
        ) AS masdet ON masdet.id=new_inv_payment.ipaymeth
        WHERE new_inv_payment.ssource=:id";
        $statement=$this->dbHost->prepare($query);

        $statement->bindParam(':id',$idinv,PDO::PARAM_STR,35);
		$statement->execute();
		// return $query;
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function LaporanFollowUpMarketing($search='',$offset='0',$limit='30'){

		if ($search=='') {
			$filter = "WHERE laporan_followup.status<>'3'";
		} elseif($search['fd']!=''&&$search['ld']!='') {
			$filter = "WHERE laporan_followup.tanggal BETWEEN '".$search['fd']."' AND '".$search['ld']."' AND laporan_followup.customer LIKE '%".$search['np']."%' AND laporan_followup.status<>'3'";
		}else{
		    $filter = "WHERE laporan_followup.status<>'3' AND laporan_followup.customer LIKE '%".$search['np']."%'";
		}

		$query = "SELECT `num`, `tanggal`, `customer`, `agenda`, `desc_tbl`.`dt_desc` AS desc_stat, `jumlah_hari`, `deskripsi_hari`, `users`.`nama` AS `petugas`, `date_created` 
		FROM `laporan_followup` 
		INNER JOIN users ON users.username=laporan_followup.petugas
		INNER JOIN desc_tbl ON desc_tbl.dt_relate_tbl='lapfolup'
		AND desc_tbl.dt_flag=laporan_followup.status ".$filter." 
		ORDER BY laporan_followup.tanggal DESC";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();
		// return $query;
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function LapBulananByItemTrans($menu){
	    if($menu=='lastmonth'){
	        if(date('m')=='1'){
	            $filter= "AND MONTH(new_transaksi.trans_date)=MONTH(DATE_SUB(CURDATE(), INTERVAL 1 MONTH))
	            AND YEAR(DATE_SUB(CURDATE(), INTERVAL 1 YEAR))";
	        }else{
	            $filter= "AND MONTH(new_transaksi.trans_date)=MONTH(DATE_SUB(CURDATE(), INTERVAL 1 MONTH))
	            AND YEAR(new_transaksi.trans_date)=YEAR(CURDATE())";
	        }
	    }elseif($menu=='curmonth'){
	        $filter="AND MONTH(new_transaksi.trans_date)=MONTH(CURDATE())
	        AND YEAR(new_transaksi.trans_date)=YEAR(CURDATE())";
	    }else{
	    	$filter = "";
	    }
	}
	
	

	//quotation
	public function FetchQuotation(){
		$query ="SELECT `qtn_id`, `qtn_date` 
		FROM `new_quotation` 
		WHERE `qtn_id_trans`=:id;";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':id',$this->transid,PDO::PARAM_STR,35);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);

	}
	//end quotation

	// tanggal 
	public function CekTanggal(){
		$query = "SELECT COUNT(id) AS `jumlah` FROM `fac_calendar` WHERE `acara`=:acara AND `tanggal`=:tanggal";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':tanggal',$this->tanggal,PDO::PARAM_STR,15);
		$statement->bindParam(':acara',$this->transid,PDO::PARAM_STR,255);

		$statement->execute();
		$results=$statement->fetch(PDO::FETCH_OBJ);

		if ($results->jumlah=='0') {
			return true;
		}else{
			return false;
		}

	}


	public function AddTanggal(){
		$query = "INSERT INTO `fac_calendar` (`tanggal`, `acara`) 
		VALUES (:tanggal, :acara);";
		$statement = $this->dbHost->prepare($query);

		//$statement->bindParam(':id',$id,PDO::PARAM_STR,35);
		$statement->bindParam(':tanggal',$this->tanggal,PDO::PARAM_STR,15);
		$statement->bindParam(':acara',$this->transid,PDO::PARAM_STR,255);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function FetchTanggalByTransaction(){
		$query="SELECT * FROM `fac_calendar` WHERE `acara`=:transaksi";
		$statement = $this->dbHost->prepare($query);

		$statement->bindParam(':transaksi',$this->transid,PDO::PARAM_STR,255);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function FetchByIdTanggal(){
		$query = "SELECT * FROM `fac_calendar` WHERE `id`=:id;";

		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':id',$this->tanggal, PDO::PARAM_STR,35);

		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function deleteTanggal(){
		$query = "DELETE FROM `fac_calendar` WHERE `id`=:id";
		$statement = $this->dbHost->prepare($query);

		//$statement->bindParam(':id',$id,PDO::PARAM_STR,35);
		$statement->bindParam(':id',$this->tanggal,PDO::PARAM_STR,15);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	// end tanggal


	// agenda

	public function FetchAgendaForInvoice(){
		$query = "SELECT `nama_agenda` 
		FROM `new_trans_agendas` AS `agd`, `daftar_agenda` AS `d_agd` 
		WHERE `agd`.`agd_training`=`d_agd`.`id` 
		AND `agd`.`agd_id_trans`=:id;";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':id',$this->transid,PDO::PARAM_STR,15);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);

	}

	public function InputAgenda(){
		$query = "INSERT INTO `new_trans_agendas`(`agd_id_trans`, `agd_training`) 
		VALUES (:idtrans, :agendatraining);";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':idtrans',$this->agenda->transaksi, PDO::PARAM_STR,35);
		@$statement->bindParam(':agendatraining',$this->agenda->training, PDO::PARAM_STR,35);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function DeleteAgenda(){
		$query = "DELETE FROM `new_trans_agendas` WHERE `new_trans_agendas`.`agd_id` = :id";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':id',$this->agenda, PDO::PARAM_STR,10);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function FetchNamaAgenda(){
		$value = '%'.$this->agenda.'%';
		$query="SELECT * FROM `daftar_agenda` WHERE `nama_agenda` LIKE :namaagenda";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':namaagenda',$value,PDO::PARAM_STR,75);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);

	}

	public function FetchAgendaByID(){
		$query = "SELECT `agd_id`, `agd_id_trans`, `nama_agenda` 
		FROM `new_trans_agendas`,`daftar_agenda` 
		WHERE `daftar_agenda`.`id`=`new_trans_agendas`.`agd_training` AND `new_trans_agendas`.`agd_id`=:id;";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':id',$this->agenda,PDO::PARAM_STR,35);
		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function FetchAgendasByTransaction(){
		$query = "SELECT `agd_id`, `agd_id_trans`, `nama_agenda` 
		FROM `new_trans_agendas`,`daftar_agenda` 
		WHERE `daftar_agenda`.`id`=`new_trans_agendas`.`agd_training` AND `new_trans_agendas`.`agd_id_trans`=:id;";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':id',$this->transid,PDO::PARAM_STR,35);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}


	// end agenda

	// items

	public function FetchItemsForInvoice(){
		$query = "SELECT `nama_item`, `harga`, `itm_qty` 
		FROM `new_trans_items` AS `item`,`paket_training` AS `barang` 
		WHERE `item`.`itm_paket`=`barang`.`id` AND `item`.`itm_trans_id`=:id;";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':id',$this->transid,PDO::PARAM_STR,15);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function FetchItems(){
		$query = "SELECT `paket_training`.`id` AS `aidi`,`no`, `itm_trans_id`, `nama_item`, `harga`, `itm_qty` 
		FROM `new_trans_items`,`paket_training` 
		WHERE `paket_training`.`id`=`new_trans_items`.`itm_paket` AND `itm_trans_id`=:id;";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':id',$this->transid,PDO::PARAM_STR,15);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);

	}

	public function FetchItemsByID(){
		$query = "SELECT `no`, `itm_trans_id`, `nama_item`, `itm_qty`,`harga` 
		FROM `new_trans_items`,`paket_training` 
		WHERE `paket_training`.`id`=`new_trans_items`.`itm_paket` AND `no`=:id;";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':id',$this->items, PDO::PARAM_STR,35);

		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function FetchPaketTraining(){
		$query="SELECT * FROM `paket_training` WHERE `status_paket`='1' OR `status_paket`='3' ORDER BY `nama_item` ASC";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function InputNewItem(){
		$query = "INSERT INTO `new_trans_items` (`itm_trans_id`, `itm_paket`, `itm_qty`) 
		VALUES (:idtrans, :paket, :qty);";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':idtrans',$this->transid, PDO::PARAM_STR,10);
		@$statement->bindParam(':paket',$this->items->paket, PDO::PARAM_STR,10);
		@$statement->bindParam(':qty',$this->items->qty, PDO::PARAM_STR,10);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function DeleteItem(){
		$query = "DELETE FROM `new_trans_items` WHERE `no`=:id ";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':id',$this->items, PDO::PARAM_STR,11);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	// end item

	// transaction
	public function InputNewTransaction(){
		$query ="INSERT INTO `new_transaksi` (`trans_id`, `trans_jenis`, `trans_id_usaha`, `trans_cust_id`, `trans_va`, `trans_ppn`, `trans_discount`, `trans_notes`, `trans_petugas`, `trans_date`,`trans_dp`,`trans_lokasi`,`trans_waktu`,`trans_prov`,`trans_kota`,`trans_kec`) 
		VALUES (:id, :trjenis, :usaha, :customer, :va, :ppn, :discount, :notes, :petugas, CURRENT_TIMESTAMP,:depe,:tempat,:waktu,:provinsi,:kota,:kecamatan);";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':id',$this->transid, PDO::PARAM_STR,35);
		@$statement->bindParam(':customer',$this->custid, PDO::PARAM_STR,35);
		@$statement->bindParam(':usaha',$this->usahaid, PDO::PARAM_STR,35);
		@$statement->bindParam(':va',$this->accurateid, PDO::PARAM_STR,35);
		@$statement->bindParam(':ppn',$this->ppn, PDO::PARAM_STR,5);
		@$statement->bindParam(':discount',$this->discount, PDO::PARAM_STR,5);
		@$statement->bindParam(':notes',$this->notes, PDO::PARAM_STR,20);
		@$statement->bindParam(':petugas',$this->petugas, PDO::PARAM_STR,75);
		@$statement->bindParam(':trjenis',$this->jenistransaksi, PDO::PARAM_STR,1);
		@$statement->bindParam(':depe',$this->dp, PDO::PARAM_STR,9);
		@$statement->bindParam(':tempat',$this->utklokasi, PDO::PARAM_STR,125);
		@$statement->bindParam(':waktu',$this->utkwaktu, PDO::PARAM_STR,15);
		
		@$statement->bindParam(':provinsi',$this->provinsi, PDO::PARAM_STR,15);
		@$statement->bindParam(':kota',$this->kota, PDO::PARAM_STR,15);
		@$statement->bindParam(':kecamatan',$this->kecamatan, PDO::PARAM_STR,15);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function EditTransaction(){
		$query = "UPDATE `new_transaksi` 
		SET `trans_ppn`=:ppn,`trans_discount`=:discount,`trans_notes`=:notes,`trans_jenis`=:jtr,`trans_va`=:versiaccurate,`trans_dp`=:depe,`trans_waktu`=:waktu,`trans_lokasi`=:tempat,
		`trans_prov`=:provinsi,`trans_kota`=:kota,`trans_kec`=:kecamatan
		WHERE `trans_id`=:id;";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':id',$this->transid, PDO::PARAM_STR,35);
		@$statement->bindParam(':ppn',$this->ppn, PDO::PARAM_STR,5);
		@$statement->bindParam(':discount',$this->discount, PDO::PARAM_STR,5);
		@$statement->bindParam(':notes',$this->notes, PDO::PARAM_STR,20);
		@$statement->bindParam(':jtr',$this->jenistransaksi, PDO::PARAM_STR,1);
		@$statement->bindParam(':versiaccurate',$this->accurateid, PDO::PARAM_STR,4);
		@$statement->bindParam(':depe',$this->dp, PDO::PARAM_STR,5);
		@$statement->bindParam(':tempat',$this->utklokasi, PDO::PARAM_STR,125);
		@$statement->bindParam(':waktu',$this->utkwaktu, PDO::PARAM_STR,15);
        @$statement->bindParam(':provinsi',$this->provinsi, PDO::PARAM_STR,15);
		@$statement->bindParam(':kota',$this->kota, PDO::PARAM_STR,15);
		@$statement->bindParam(':kecamatan',$this->kecamatan, PDO::PARAM_STR,15);
		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function FetchTransactionDataById(){
		$query = "SELECT `trans_id`,`trans_cust_id`,`trans_id_usaha`,`trans_va`,`trans_date`,`nama_cust`, `email_cust`, `telp_cust`, `jabatan_cust`, 
		`jenis_pengguna_cust`,`nama`,`alamat`,`trans_jenis`,`trans_ppn`,`trans_dp`,`trans_discount`,`trans_notes`,`trans_lokasi`,`trans_waktu`,
		`trans_prov`,`trans_kota`,`trans_kec`
		FROM `new_transaksi`
        INNER JOIN `new_customer` ON `new_customer`.`id_cust`=`new_transaksi`.`trans_cust_id`
        INNER JOIN `perusahaan` ON `perusahaan`.`id` = `new_transaksi`.`trans_id_usaha` 
		WHERE `new_transaksi`.`trans_id`=:id";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':id',$this->transid, PDO::PARAM_STR,35);

		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function CountTransactionData(){
		$query = "SELECT COUNT(`trans_id`) AS `jumlah` FROM `new_transaksi`;";
		$statement=$this->dbHost->prepare($query);

		$statement->execute();
	 	$result = $statement->fetch(PDO::FETCH_OBJ);
 		return $result->jumlah;

	}

	public function ListTable($offset,$limit){
		$query = "SELECT `trans_id`,`nama_cust`,`email_cust`,`telp_cust`,`nama` AS `nama_usaha`,`jt_ket`,`nama_cust`,kalender.tgl, itmtbl.items AS items, agd.agendas AS agendas,
		kalender.jml_hari AS jumlah_hari
		FROM `new_transaksi` AS `nt`
        INNER JOIN `new_customer` AS `nc` ON `nc`.`id_cust`=`nt`.`trans_cust_id`
        INNER JOIN `perusahaan` AS `p` ON nt.trans_id_usaha=p.id
        INNER JOIN `new_jenis_transaksi` AS `jt` ON `jt`.`jt_id` = `nt`.`trans_jenis`
        LEFT JOIN (
            SELECT fac_calendar.acara AS idtrans,GROUP_CONCAT(fac_calendar.tanggal SEPARATOR '##') AS tgl, COUNT(fac_calendar.tanggal) AS jml_hari
            FROM fac_calendar
            GROUP BY fac_calendar.acara
        ) AS kalender
        ON kalender.idtrans=nt.trans_id
        LEFT JOIN (
        	SELECT new_trans_items.itm_trans_id AS aidi,GROUP_CONCAT(nama_item SEPARATOR '##') AS items
            FROM paket_training
         	INNER JOIN new_trans_items ON paket_training.id=new_trans_items.itm_paket  
            GROUP BY new_trans_items.itm_trans_id
        ) AS itmtbl
        ON itmtbl.aidi=nt.trans_id
		LEFT JOIN (
			SELECT new_trans_agendas.agd_id_trans AS aidi,GROUP_CONCAT(`nama_agenda` SEPARATOR '##') AS agendas FROM `new_trans_agendas`
         	INNER JOIN `daftar_agenda` ON `daftar_agenda`.`id`=`new_trans_agendas`.`agd_training` 
            GROUP BY new_trans_agendas.agd_id_trans
		) AS agd
		ON agd.aidi=nt.trans_id
		ORDER BY `trans_date` DESC LIMIT $offset,$limit";
		$statement=$this->dbHost->prepare($query);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function FetchJenisTransaksi(){
		$query = "SELECT `jt_id`, `jt_ket` FROM `new_jenis_transaksi`;";
		$statement=$this->dbHost->prepare($query);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		// echo $query;
		$dataz=json_encode($results);
		return json_decode($dataz);

	}

	public function FetchJenisAccurateTransaksiById(){
		$query = "SELECT `va_id`,`va_nama` FROM `new_transaksi`
		INNER JOIN `perusahaan` ON `perusahaan`.`id` = `new_transaksi`.`trans_id_usaha`
		INNER JOIN `usaha_versi_accurate` ON `usaha_versi_accurate`.`uva_source`=`perusahaan`.`id`
		INNER JOIN `versi_accurate` ON `versi_accurate`.`va_id`=`usaha_versi_accurate`.`uva_id_varian`
		WHERE `new_transaksi`.`trans_id`=:idtrans";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':idtrans',$this->transid,PDO::PARAM_STR,75);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	}

	public function FetchJenisAccurateTransaksi(){
		$query = "SELECT `va_id`,`va_nama` FROM `new_customer`
		INNER JOIN `perusahaan` ON `perusahaan`.`id` = `new_customer`.`id_perusahaan`
		LEFT JOIN `usaha_versi_accurate` ON `usaha_versi_accurate`.`uva_source`=`perusahaan`.`id`
		LEFT JOIN `versi_accurate` ON `versi_accurate`.`va_id`=`usaha_versi_accurate`.`uva_id_varian`
		WHERE `new_customer`.`id_cust`=:idcust";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':idcust',$this->custid,PDO::PARAM_STR,75);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	}

	public function SearchData(){
		$namausaha = "%".$this->data->nama_usaha."%";
		$namacustomer = "%".$this->data->nama_cust."%";

		$query = "SELECT `trans_id`,`nama_cust`,`email_cust`,`telp_cust`,`nama` AS `nama_usaha`,`jt_ket`,`nama_cust`, 
		(SELECT GROUP_CONCAT(nama_item SEPARATOR '##') FROM paket_training,new_trans_items WHERE paket_training.id=new_trans_items.itm_paket AND new_trans_items.itm_trans_id=nt.trans_id) AS items,
		(SELECT GROUP_CONCAT(`nama_agenda` SEPARATOR '##') FROM `new_trans_agendas`,`daftar_agenda` WHERE `agd_id_trans`=nt.trans_id AND `daftar_agenda`.`id`=`new_trans_agendas`.`agd_training`) AS agendas,
		(SELECT GROUP_CONCAT(`tanggal` SEPARATOR '##') FROM `fac_calendar` WHERE `acara` = `nt`.`trans_id`) AS `tgl`,
		(SELECT COUNT(acara) AS jumlah_hari FROM fac_calendar WHERE fac_calendar.acara=nt.trans_id) AS jumlah_hari
		FROM `new_transaksi` AS `nt`
        INNER JOIN `new_customer` AS `nc` ON `nc`.`id_cust`=`nt`.`trans_cust_id`
        INNER JOIN `perusahaan` AS `p` ON `nt`.`trans_id_usaha`=`p`.`id`
        INNER JOIN `new_jenis_transaksi` AS `jt` ON `nt`.`trans_jenis`=`jt`.`jt_id` 
        WHERE `nama_cust` LIKE :namacustomer
		AND `p`.`nama` LIKE :namausaha";

		if ($this->data->tanggal_awal!=''&&$this->data->tanggal_akhir) {
			$query .= " AND `nt`.`trans_date` BETWEEN '".$this->data->tanggal_awal." 00:00:00' AND '".$this->data->tanggal_akhir." 23:59:59'";
		}

		$query .= ";";

		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':namacustomer',$namacustomer,PDO::PARAM_STR,75);
		@$statement->bindParam(':namausaha',$namausaha,PDO::PARAM_STR,75);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		// echo $query;
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function FetchTransactionForPreview(){
		$query = "SELECT `trans_waktu`,`id_cust`,`nama_cust`, `perusahaan`.`email` AS `email`,`email_cust`,`telp_cust`,`id`, `perusahaan`.`alamat` AS `alamat`, `trans_lokasi`, `perusahaan`.`nama` AS `nama`, `trans_jenis`,`trans_ppn`,`trans_dp`,`trans_discount`,`trans_notes`,`trans_lokasi`,`trans_waktu`,
		(SELECT GROUP_CONCAT(`nama_agenda` SEPARATOR '##') FROM `new_trans_agendas`,`daftar_agenda` WHERE `agd_id_trans`=`new_transaksi`.`trans_id` AND `daftar_agenda`.`id`=`new_trans_agendas`.`agd_training`) AS `nama_agenda`,
		(SELECT GROUP_CONCAT(`tanggal` SEPARATOR '##') FROM `fac_calendar` WHERE `acara` = `new_transaksi`.`trans_id`) AS `tgl`,
		`users`.`nama` AS `petugas`
		FROM `new_transaksi`
        INNER JOIN `new_customer` 
        ON `new_transaksi`.`trans_cust_id` = `new_customer`.`id_cust`
        INNER JOIN `perusahaan`
        ON `new_transaksi`.`trans_id_usaha`=`perusahaan`.`id`
        INNER JOIN `users` ON `new_transaksi`.`trans_petugas`=`users`.`username`
		WHERE `new_transaksi`.`trans_id`=:id;";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':id',$this->transid, PDO::PARAM_STR,35);

		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function FetchForDetail(){
		$query = "SELECT `id`,`perusahaan`.`nama` AS `nama_usaha`,`email`,`alamat`,`map`,`telepon`,`ket_jenis_usaha`,`nama_cust`,`email_cust`,`telp_cust`,`jabatan_cust`,`jenis_pengguna_cust`,`marketing_nama`,`trans_date`,`trans_lokasi`,
		(SELECT GROUP_CONCAT(`jenis_usaha` SEPARATOR ' # ') FROM `jenis_usaha` WHERE `id` IN (SELECT `id_jenis_usaha` FROM `perusahaan_jns_usaha` AS `uju` WHERE `uju`.`id_company`=`perusahaan`.id)) AS `jenis_usaha`,
		(SELECT GROUP_CONCAT(`va_nama` SEPARATOR ' # ') FROM `versi_accurate` WHERE `va_id` IN (SELECT `uva_id_varian` FROM `usaha_versi_accurate` AS `uva` WHERE `uva`.`uva_source`=`perusahaan`.id)) AS `versi_acc`,
		(SELECT GROUP_CONCAT(`nama_agenda` SEPARATOR ' # ') FROM `new_trans_agendas`,`daftar_agenda` WHERE `agd_id_trans`=`new_transaksi`.`trans_id` AND `daftar_agenda`.`id`=`new_trans_agendas`.`agd_training`) AS `nama_agenda`,
		(SELECT COUNT(acara) AS jumlah_hari FROM fac_calendar WHERE `fac_calendar`.`acara`=`new_transaksi`.`trans_id`) AS jumlah_hari
		FROM `new_transaksi`
		INNER JOIN `perusahaan` ON `new_transaksi`.`trans_id_usaha`=`perusahaan`.`id`
		INNER JOIN `new_customer` ON `new_transaksi`.`trans_cust_id` = `new_customer`.`id_cust`
		INNER JOIN `new_marketing` ON `new_customer`.`marketing_id` = `new_marketing`.`marketing_id`
		WHERE `new_transaksi`.`trans_id` = :id";

		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':id',$this->transid, PDO::PARAM_STR,35);

		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function GetVersiAccurate($id){
		$query = "SELECT GROUP_CONCAT(`va_nama` SEPARATOR ' # ') AS `versi_accurate`
		FROM versi_accurate,usaha_versi_accurate 
		WHERE usaha_versi_accurate.uva_source = :id
		AND usaha_versi_accurate.uva_id_varian = versi_accurate.va_id;";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':id',$id, PDO::PARAM_STR,35);

		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_OBJ);
		return $result->versi_accurate;
	}

	public function FetchTransactionDataByIdCustomer(){
		$query = "SELECT `nama`,`trans_id`, `trans_cust_id`, `trans_ppn`, `trans_discount`, `trans_notes`, `trans_petugas`, `trans_date`
		 FROM new_transaksi,users 
		 WHERE trans_cust_id = :idcustomer AND `users`.`username`=`new_transaksi`.`trans_petugas`;";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':idcustomer',$this->custid, PDO::PARAM_STR,35);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function FetchTransactionDataByIdTransaction(){
		$query = "SELECT `nama`,`trans_id`, `trans_cust_id`, `trans_ppn`, `trans_discount`, `trans_notes`, `trans_petugas`, `trans_date`
		 FROM new_transaksi
         INNER JOIN users ON `users`.`username`=`new_transaksi`.`trans_petugas`
		 WHERE trans_id = :idtrans";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':idtrans',$this->transid, PDO::PARAM_STR,35);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function FetchAllMarketing(){
		$query = "SELECT `marketing_id`, `marketing_nama`, `marketing_telp`, `marketing_email`, `marketing_cabang` 
		FROM `new_marketing` WHERE new_marketing.ivisib='1' ORDER BY `marketing_nama` ASC;";
		$statement=$this->dbHost->prepare($query);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);

	}

	public function Sql(){
		$query = "ALTER TABLE `new_transaksi` ADD `catatan` VARCHAR(2000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '-' AFTER `trans_date`;";

		$statement=$this->dbHost->prepare($query);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	// end transaction

    public function getProvinsi(){
		return $this->provinsi;
	}

	public function setProvinsi($value){
		$this->provinsi = $value;
	}
	
	public function getKota(){
		return $this->kota;
	}

	public function setKota($value){
		$this->kota = $value;
	}
	
	public function getKecamatan(){
		return $this->kecamatan;
	}

	public function setKecamatan($value){
		$this->kecamatan = $value;
	}

	public function getID(){
		return $this->transid;
	}

	public function setID($value){
		$this->transid = $value;
	}

	public function getCustomer(){
		return $this->custid;
	}

	public function setCustomer($value){
		$this->custid = $value;
	}

	public function getDP(){
		return $this->dp;
	}

	public function setDP($value){
		$this->dp = $value;
	}


	public function getPPN(){
		return $this->ppn;
	}

	public function setPPN($value){
		$this->ppn = $value;
	}

	public function getTempat(){
		return $this->utklokasi;
	}

	public function setTempat($value){
		$this->utklokasi = $value;
	}

	public function getWaktu(){
		return $this->utkwaktu;
	}

	public function setWaktu($value){
		$this->utkwaktu = $value;
	}

	public function getAccurate(){
		return $this->accurateid;
	}

	public function setAccurate($value){
		$this->accurateid = $value;
	}


	public function getJenistransaksi(){
		return $this->jenistransaksi;
	}

	public function setJenistransaksi($value){
		$this->jenistransaksi = $value;
	}


	public function getDiscount(){
		return $this->discount;
	}

	public function setDiscount($value){
		$this->discount = $value;
	}

	public function getNotes(){
		return $this->notes;
	}

	public function setNotes($value){
		$this->notes = $value;
	}

	public function getStatus(){
		return $this->status;
	}

	public function setStatus($value){
		$this->status = $value;
	}

	public function getTanggal(){
		return $this->tanggal;
	}

	public function setTanggal($value){
		$this->tanggal = $value;
	}

	public function getAgenda(){
		return $this->agenda;
	}

	public function setAgenda($value){
		$this->agenda = $value;
	}

	public function getItems(){
		return $this->items;
	}

	public function setItems($value){
		$this->items = $value;
	}

	public function getPetugas(){
		return $this->petugas;
	}

	public function setPetugas($value){
		$this->petugas = $value;
	}

	public function getFolder(){
		return 'attachments/';
	}

	public function getData(){
		return $this->data;
	}

	public function setData($value){
		return $this->data = $value;
	}

    public function getUsahaid(){
        return $this->usahaid;
    }

    public function setUsahaid($usahaid){
        $this->usahaid = $usahaid;
    }
}

?>