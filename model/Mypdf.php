<?php 
require '../fpdf/fpdf.php';
class Mypdf{
	
	private $path;
	private $nameFile;
	private $data;
	private $tempdata;

	public function Create(){
		$pdf = new PDF_HTML();
		$pdf->AddPage();
		$pdf->SetFont('Arial','B',16);
		$pdf->Cell(40,10,'Hello World!');
		$pdf->Output();
	}

	public function CreateFile(){
		$pdf = new PDF_HTML();
		$pdf->AddPage();
		$pdf->SetFont('Arial','B',16);
		$pdf->Cell(40,10,'Hello World!');
		$pdf->Output($this->path.$this->nameFile,'F');
	}

    

	public function Invoice($command){
		$pdf = new PDF_HTML();
		$pdf->AddPage('p','A4',0);
		$pdf->SetAuthor('Rifzky Alam - Software Dev');
		$pdf->SetAutoPageBreak(false,1);

		//image FAC Institute
		$pdf->Image('../images/facnew.png',10,10,30,25);

		$pdf->setXY(160,27);
		$pdf->SetFont('Arial','B',26);

		$pdf->Cell(30,5,'INVOICE');

		$pdf->setXY(159,35);
		$pdf->SetFont('Arial','B',12);
		$pdf->Cell(30,5,$this->data->no_invoice);


		$pdf->SetFont('Arial','',13);
		$pdf->SetTextColor(255,255,255);
		$pdf->SetFillColor(33,150,243);
		$pdf->Rect(10,40,190,10,'F');
		//pembayaran
		// $pdf->Rect(10,52,190,62,'D');
		//info transaksi
		// $pdf->Rect(10,118,190,35,'D');
		//rincian pembayaran
		// $pdf->Rect(10,157,190,45,'D');


		$pdf->setXY(15,44);
		$pdf->Cell(45,3,'DETAIL PEMBAYARAN',0,0,'L');
		$pdf->setXY(157,44);
		$pdf->SetFont('Arial','',12);
		$pdf->Cell(30,3,$this->getTanggal($this->data->tanggal),0,0,'L');

		
		$pdf->SetFont('Arial','B',12);
		$pdf->SetTextColor(0,0,0);
		$pdf->setXY(15,55);
		$this->InvPerusahaan($pdf,$this->data->nama_usaha);
		$pdf->Ln();
		$pdf->SetFont('Arial','',8);
		$pdf->setX(15);
		$pdf->MultiCell(120,5,$this->data->alamat,0,'L');
		$pdf->Ln();

		$pdf->setX(15);
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(30,5,$this->data->nama_cust,0,0,'L');
		$pdf->Ln();

		$pdf->setX(15);
		$pdf->SetFont('Arial','',8);
		$pdf->Cell(30,5,$this->data->telepon_cust,0,0,'L');
		$pdf->Ln();		

		$pdf->setX(15);
		$pdf->Cell(30,5,$this->data->email_cust,0,0,'L');
		$pdf->Ln();
		$pdf->Ln();
		$pdf->Ln();
		$pdf->SetFont('Arial','B',10);
		$pdf->setX(15);
		$pdf->Cell(30,5,'Deskripsi',0,0,'L');
		$gap = 15;
		$pdf->Cell($gap);
		$pdf->Cell(2,5,':',0,0,'L');
		$pdf->Cell(5);
		$pdf->SetFont('Arial','',9);
		$pdf->Cell(30,5,$this->data->deskripsi,0,0,'L');
		$pdf->Ln();
		// $pdf->Ln();

		$pdf->SetFont('Arial','B',10);
		$pdf->setX(15);
		$pdf->Cell(30,5,'Metode Pembayaran',0,0,'L');
		$pdf->Cell($gap);
		$pdf->Cell(2,5,':',0,0,'L');
		$pdf->Cell(5);
		$pdf->SetFont('Arial','',9);
		$pdf->Cell(30,5,$this->data->metode_bayar,0,0,'L');
		$pdf->Ln();
		$pdf->Ln();
		$pdf->Ln();

		$pdf->SetFont('Arial','B',13);
		$pdf->setX(15);
		$pdf->Cell(45,3,'BIAYA JASA/PRODUK',0,0,'L');
		$pdf->Ln();
		$pdf->Ln();

		$pdf->SetFont('Arial','B',10);
		$pdf->setX(15);
		$pdf->Cell(90,5,'Nama Produk',1,0,'C');
		$pdf->Cell(50,5,'Harga',1,0,'C');
		$pdf->Cell(10,5,'Qty',1,0,'C');
		$pdf->Cell(30,5,'Jumlah',1,0,'C');
		$pdf->Ln();
		$this->ListItems($pdf,$this->data->items);
		$pdf->Ln();
		$pdf->Ln();
		$pdf->setX(15);
		$pdf->Cell(30,5,'Transfer Bank',0,0,'L');
		
		$pdf->SetFont('Arial','B',10);
		$pdf->setX(135);
		$pdf->Cell(30,5,'Sub Total',0,0,'L');
		$pdf->Cell(30,5,'Rp '. number_format($this->tempdata),0,0,'R');
		$pdf->Ln();
		
		$pdf->setX(15);
		$pdf->SetFont('Arial','',9);
		$pdf->Cell(30,5,'1. Bank BCA KCU Bekasi No Acc. 0663162851 a/n : Fajar Shodiq',0,0,'L');

		/*
		$pdf->SetFont('Arial','B',10);
		$pdf->SetTextColor(255, 0, 0);
		$pdf->setX(135);
		$pdf->Cell(30,5,'Diskon ('. $this->data->diskon * 100 . '%)',0,0,'L');
		$pdf->Cell(30,5,'Rp -'. number_format($this->tempdata * $this->data->diskon),0,0,'R');
		$pdf->Ln();
		*/
		$this->IsDiscount($pdf,$this->data->diskon);
		$pdf->Ln();


		$pdf->setX(15);
		$pdf->SetFont('Arial','',8);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->Cell(30,5,'2. Bank Mandiri KCP Bekasi Ahmad Yani No. Acc. 1670001377901 a/n : Fajar Shodiq',0,0,'L');

		$pdf->setX(135);
		$pdf->SetFont('Arial','B',10);
		$pdf->SetTextColor(255, 0, 0);
		$pdf->Cell(30,5,'DP',0,0,'L');
		$pdf->Cell(30,5,'Rp -'. number_format($this->data->dp),0,0,'R');
		$pdf->SetTextColor(0, 0, 0);
		$pdf->Ln();
		$pdf->Ln();
		$pdf->setX(135);
		$pdf->SetFont('Arial','B',12);
		$pdf->Cell(30,5,'Total Biaya',0,0,'L');
		$pdf->Cell(30,5,'Rp '. number_format($this->tempdata - $this->data->dp - ($this->tempdata * $this->data->diskon)),0,0,'R');

		$y = 275;
		$pdf->SetFillColor(33,150,243);
		$pdf->Rect(10,$y,190,10,'F');
		$pdf->SetTextColor(255,255,255);
		$pdf->SetFont('Arial','',10);
		$pdf->setXY(10,$y + 3);
		$pdf->Cell(180,5,'FAC Institute: Jl Jatiwaringin No 8 Pangkalan Jati Jakarta Timur - 13620.',0,0,'C');
		$pdf->setXY(15,$y + 10);
		$pdf->SetTextColor(0,0,0);
		$pdf->SetFont('Arial','',8);
		$pdf->Cell(180,5,'Apabila memerlukan bantuan, silahkan hubungi Customer Service: +6281290083983, E-Mail: training@fac-institute.com',0,0,'C');

		if ($command!='display') {
			$pdf->Output($this->path.$this->nameFile,'F');
		}else{
			$pdf->Output();
		}
	}

	public function Penawaran($command){
		$pdf = new PDF_HTML();
		$pdf->AddPage('p','A4',0);
		$pdf->Image('../images/logo-fac.jpg',35,10,50,30);
		$pdf->Image('../images/logoaccurate5.png',105,15,70,20);
		// $pdf->Image('/home/facinsti/public_html/docs/stempelfac.jpg',40,205,30,20);
		// $pdf->Image('stempelfac.jpg',40,205,30,20);
		$pdf->setY(42);
		$pdf->SetFont('Arial','BU',12.5);
		$pdf->Cell(185, 7, $this->data->title, 0, 0, "C");
		$pdf->Ln();
		$pdf->SetFont('Arial','',8);
		$pdf->Cell(180, 2, 'Nomor : '.$this->data->no_quotation, 0, 2, 'C');
		$pdf->Ln();
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(10);
		$pdf->Cell(30,5,'Kepada Yth,');
		$pdf->Cell(85);
		$pdf->Cell(30,5,'Jakarta, '.date('d-m-Y'));
		$pdf->Ln();
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10);

		$pdf->Cell(30,5,$this->data->nama_usaha);
		
		$pdf->Ln();
		$pdf->Cell(10);
		$pdf->Cell(10,5,'UP');
		$pdf->Cell(5);
		$pdf->Cell(5,5,':');
		//$pdf->Cell(5);
		$pdf->Cell(30,5,$this->data->nama_personal);
		$pdf->Ln();
		$pdf->Cell(10);
		$pdf->Cell(10,5,'Email');
		$pdf->Cell(5);
		$pdf->Cell(5,5,':');
		//$pdf->Cell(5);
		$pdf->Cell(30,5,$this->data->email);
		$pdf->Ln();
		$pdf->Cell(10);
		$pdf->Cell(30,5,'Dengan hormat,');
		$pdf->Ln();

		$pdf->SetLeftMargin(20);
		$pdf->WriteHTML('Sebelumnya kami ucapkan terimakasih atas ketertarikan menggunakan program ACCURATE di <b>'.$this->data->nama_usaha.'</b> Untuk <b>'.$this->data->subjek.'</b>, Kami memberikan penawaran <b>TERBAIK</b> berupa:');
		
		$pdf->Ln();
		$pdf->Ln();

		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(60,5,'Nama Produk',1,0,'C');
		$pdf->Cell(60,5,'Harga',1,0,'C');
		$pdf->Cell(20,5,'Qty',1,0,'C');
		$pdf->Cell(30,5,'Jumlah',1,0,'C');
		$pdf->Ln();
		$pdf->SetFont('Arial','',10);
		$pdf->SetFillColor(255,255,255); //set background color for multi cell

		$pdf->SetWidths(array(60,60,20,30));
		$pdf->SetAligns(array('L','R','C','C'));
		if ($this->data->jumlah_hari!='1') {
			$pdf->Row(array($this->data->produk,'Rp '.number_format($this->data->harga),$this->data->jumlah_hari." hari",'Rp '. number_format($this->data->harga*$this->data->jumlah_hari)));
		} else {
			$pdf->Row(array($this->data->produk,'Rp '.number_format($this->data->harga),"....hari",""));	
		}
		
		

		//$pdf->MultiCell(60,5,'Rp 1.000.000.-',1,'C');
		//$pdf->MultiCell(20,5,'....hari',1,'C');
		//$pdf->Cell(30,5,'',1,0,'C');
		//$pdf->Ln();
		$pdf->Cell(60,5,'Biaya Transportasi per Hari',1,0,'L');
		if ($this->data->jumlah_hari!='1') {
			$pdf->Cell(60,5,'Rp '.number_format($this->data->biaya_transport),1,0,'R');
			$pdf->Cell(20,5,$this->data->jumlah_hari.' hari',1,0,'C');
			$pdf->Cell(30,5,'Rp '.number_format($this->data->jumlah_hari * $this->data->biaya_transport),1,0,'C');
		}else{
			$pdf->Cell(60,5,'Rp '.number_format($this->data->biaya_transport),1,0,'R');
			$pdf->Cell(20,5,'....hari',1,0,'C');
			$pdf->Cell(30,5,'',1,0,'C');
		}
		
		$pdf->Ln();
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(60,5,'Total',1,0,'L');
		$pdf->Cell(60,5,'',1,0,'C');
		$pdf->Cell(20,5,'',1,0,'C');
		$pdf->Cell(30,5,'',1,0,'C');
		
		$pdf->Ln();
		$pdf->SetFont('Arial','B',8);

		$pdf->Cell(100,5,'- Harga diatas adalah tarif nett yang diterima oleh kami (tidak dipotong pph).',0,1);
		$pdf->Cell(100,5,'- Kami bukan PKP(Pengusaha Kena Pajak), tidak ada faktur pajak. Juga tidak ada NPWP untuk bukti potong PPh 23.',0,1);
		$pdf->SetFont('Arial','B',10);
		$pdf->Ln();
		$pdf->WriteHTML('<b>[  ] Transfer via Bank BCA KCU Bekasi No. Acc. 0663162851  a/n : Fajar Shodiq</b><br>');
		$pdf->WriteHTML('<b>[  ] Transfer via Bank Mandiri KCP Bekasi Ahmad Yani No. Acc. 1670001377901  a/n : Fajar Shodiq</b><br>');
		//formquotation
		$pdf->WriteHTML('<b>Untuk kebutuhan informasi, mohon diisi data berikut dengan lengkap.</b><br>');
		$pdf->Ln();
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(10,5,'Nama Perusahaan');
		$pdf->Cell(35);
		$pdf->Cell(5,5,':');
		$pdf->Cell(100,5,'............................................................................................................');
		$pdf->Ln();
		$pdf->Cell(10,5,'Jenis Usaha');
		$pdf->Cell(35);
		$pdf->Cell(5,5,':');
		$pdf->Cell(100,5,'............................................................................................................');
		$pdf->Ln();
		$pdf->Cell(10,5,'Alamat Perusahaan');
		$pdf->Cell(35);
		$pdf->Cell(5,5,':');
		$pdf->Cell(100,5,'............................................................................................................');
		$pdf->Ln();
		$pdf->Cell(10,5,'Waktu Pelaksanaan');
		$pdf->Cell(35);
		$pdf->Cell(5,5,':');
		$pdf->Cell(100,5,'............................................................................................................');
		$pdf->Ln();
		$pdf->Cell(10,5,'Varians Accurate');
		$pdf->Cell(35);
		$pdf->Cell(5,5,':');
		$pdf->Cell(30,5,'Standar/Deluxe/Enterprise');
		$pdf->SetFont('Arial','B',5);
		$pdf->Cell(15);
		$pdf->Cell(30,5,'* Coret yang tidak perlu');
		$pdf->Ln();
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(10,5,'Agenda/Kegiatan Training');
		$pdf->Cell(35);
		$pdf->Cell(5,5,':');
		$pdf->Cell(100,5,'1 ............................................................................................................',0,1);
		$pdf->Cell(50);
		$pdf->Cell(100,5,'2 ............................................................................................................',0,1);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,5,'Notes');
		$pdf->SetFont('Arial','',8);
		$pdf->Ln();

		$pdf->Cell(100,5,'- Mendapatkan support by email.',0,1);
		$pdf->Cell(100,5,'- Pembayaran dilakukan sebelum implementasi dimulai.',0,1);
		$pdf->Cell(100,5,'- Menyediakan makan siang bagi implementator.',0,1);
		$pdf->Cell(100,5,'- Implementasi maksimal 7 jam termasuk istirahat makan siang, training bersifat time oriented, bukan result oriented.',0,1);
		$pdf->Cell(100,5,'- Implementasi diluar Jabodetabek, transport dan akomodasi ditanggung customer.',0,1);
		$pdf->Ln();

		$pdf->Cell(87.5,5,'Hormat  kami,',0,0,'C');
		$pdf->Cell(87.5,5,'Pemesan,',0,0,'C');
		$pdf->Ln();
		$pdf->Ln();

		$pdf->Ln();
		$pdf->SetFont('Arial','BU',10);
		$pdf->Cell(87.5,5,$this->data->petugas,0,0,'C');
		$pdf->Cell(87.5,5,$this->data->nama_usaha.',',0,0,'C');
		$pdf->Ln();
		$pdf->SetFont('Arial','B',6);
		$pdf->Cell(87.5,2,'FAC Institute',0,0,'C');
		$pdf->Ln();
		$pdf->Ln();
		$pdf->Ln();
		if ($command!='display') {
			$pdf->Output($this->path.$this->nameFile,'F');
		}else{
			$pdf->Output();
		}
	}

	function InvPerusahaan($pdf,$perusahaan){
		if ($perusahaan=='-') {
			$pdf->Cell(30,5,$this->data->nama_cust,0,0,'L');
		} else {
			$pdf->Cell(30,5,$perusahaan,0,0,'L');
		}
		
		
	}

	function IsDiscount($pdf,$discount){
		if ($discount!=0) {
			$pdf->SetFont('Arial','B',10);
			$pdf->SetTextColor(255, 0, 0);
			$pdf->setX(135);
			$pdf->Cell(30,5,'Diskon ('. $discount * 100 . '%)',0,0,'L');
			$pdf->Cell(30,5,'Rp -'. number_format($this->tempdata * $discount),0,0,'R');	
		} else {
			
		}
		
	}


	function getTanggal($value){
		$tgl = explode('-', $value);

        $bulan=intval($tgl[1]);
        if ($bulan==1) {
            return $tgl[2]." Januari ".$tgl[0];
        }elseif ($bulan==2) {
            return $tgl[2]." Februari ".$tgl[0];
        }elseif ($bulan==3) {
            return $tgl[2]." Maret ".$tgl[0];
        }elseif ($bulan==4) {
            return $tgl[2]." April ".$tgl[0];
        }elseif ($bulan==5) {
            return $tgl[2]." Mei ".$tgl[0];
        }elseif ($bulan==6) {
            return $tgl[2]." Juni ".$tgl[0];
        }elseif ($bulan==7) {
            return $tgl[2]." Juli ".$tgl[0];
        }elseif ($bulan==8) {
            return $tgl[2]." Agustus ".$tgl[0];
        }elseif ($bulan==9) {
            return $tgl[2]." September ".$tgl[0];
        }elseif ($bulan==10) {
            return $tgl[2]." Oktober ".$tgl[0];
        }elseif ($bulan==11) {
            return $tgl[2]." November ".$tgl[0];
        }elseif ($bulan==12) {
            return $tgl[2]." Desember ".$tgl[0];
        }else{
            return $this->getTanggal(date('Y-m-d'));
        }
    }

	public function Quotation($command){
		$pdf = new PDF_HTML();
		$pdf->AddPage('p','A4',0);
		$pdf->Image('../images/facnew.png',20,12,30,25);
		$pdf->Image('../images/logoaccurate5.png',120,15,70,20);
		$this->TandaTangan($pdf,$this->data->items);
		// $pdf->Image('../stempelfac.jpg',40,205,30,20);
		$pdf->setY(42);
		$pdf->SetFont('Arial','BU',12.5);
		$pdf->Cell(185, 7, $this->data->subject, 0, 0, "C");
		$pdf->Ln();
		$pdf->SetFont('Arial','',8);
		$pdf->Cell(180, 2, 'Nomor : '.$this->data->quot_num, 0, 2, 'C');
		$pdf->Ln();
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(10);
		$pdf->Cell(30,5,'Kepada Yth,');
		$pdf->Cell(85);
		$pdf->Cell(30,5,'Jakarta, '.$this->data->tanggal);
		$pdf->Ln();
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10);
		
		$this->QuotYth($pdf,$this->data->nama_usaha,$this->data->customer);
		
		$pdf->Ln();
		$pdf->Cell(10);
		$pdf->Cell(10,5,'UP');
		$pdf->Cell(5);
		$pdf->Cell(5,5,':');
		//$pdf->Cell(5);
		$pdf->Cell(30,5,$this->data->customer);
		$pdf->Ln();
		$pdf->Cell(10);
		$pdf->Cell(10,5,'Email');
		$pdf->Cell(5);
		$pdf->Cell(5,5,':');
		//$pdf->Cell(5);
		$pdf->Cell(30,5,$this->data->email);
		$pdf->Ln();
		$pdf->Cell(10);
		$pdf->Cell(30,5,'Dengan hormat,');
		$pdf->Ln();

		$pdf->SetLeftMargin(20);
		$this->pembukaan($pdf,$this->data->jenistr,$this->data->nama_usaha,$this->data->subject);
		
		$pdf->Ln();
		$pdf->Ln();

		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(60,5,'Nama Produk',1,0,'C');
		$pdf->Cell(60,5,'Harga',1,0,'C');
		$pdf->Cell(20,5,'Qty',1,0,'C');
		$pdf->Cell(30,5,'Jumlah',1,0,'C');
		$pdf->Ln();
		$pdf->SetFont('Arial','',10);
		$pdf->SetFillColor(255,255,255); //set background color for multi cell

		$this->ListQuotationData($pdf,$this->data->items);		
		
		$pdf->Ln();
		$pdf->SetFont('Arial','B',8);

		$pdf->Cell(100,5,'- Harga diatas adalah tarif nett yang diterima oleh kami (tidak dipotong pph).',0,1);
		$pdf->Cell(100,5,'- Kami bukan PKP(Pengusaha Kena Pajak), tidak ada faktur pajak. Juga tidak ada NPWP untuk bukti potong PPh 23.',0,1);
		$pdf->SetFont('Arial','B',10);
		$pdf->Ln();
		$pdf->WriteHTML('<b>[  ] Transfer via Bank BCA KCU Bekasi No. Acc. 0663162851  a/n : Fajar Shodiq</b><br>');
		$pdf->WriteHTML('<b>[  ] Transfer via Bank Mandiri KCP Bekasi Ahmad Yani No. Acc. 1670001377901  a/n : Fajar Shodiq</b><br>');
		
		$this->QuotForm($pdf,$this->data->jenistr);
		
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,5,'Notes');
		$pdf->SetFont('Arial','',8);
		$pdf->Ln();

		//notes are here
		$this->NotesQuotation($pdf,$this->data->jenistr);

		$pdf->Cell(87.5,5,'Hormat  kami,',0,0,'C');
		$pdf->Cell(87.5,5,'Pemesan,',0,0,'C');
		$pdf->Ln();
		$pdf->Ln();
		$pdf->Ln();


		$pdf->Ln();
		$pdf->SetFont('Arial','BU',10);
		$pdf->Cell(87.5,5,$this->data->petugas,0,0,'C');
		$pdf->Cell(87.5,5,$this->data->nama_usaha,0,0,'C');
		$pdf->Ln();
		$pdf->SetFont('Arial','B',6);
		$pdf->Cell(87.5,2,'FAC Institute',0,0,'C');
		$pdf->Ln();
		$pdf->Ln();
		$pdf->Ln();
		if ($command!='display') {
			$pdf->Output($this->path.$this->nameFile,'F');
		}else{
			$pdf->Output();
		}
	}
	
	
	public function QuotationComparation($command){
		$pdf = new PDF_HTML();
		$pdf->AddPage('p','A4',0);
		$pdf->Image('../images/facnew.png',20,12,30,25);
		$pdf->Image('../images/logoaccurate5.png',120,15,70,20);
		$this->TandaTangan($pdf,$this->data->items);
		// $pdf->Image('../stempelfac.jpg',40,205,30,20);
		$pdf->setY(42);
		$pdf->SetFont('Arial','BU',12.5);
		$pdf->Cell(185, 7, $this->data->subject, 0, 0, "C");
		$pdf->Ln();
		$pdf->SetFont('Arial','',8);
		$pdf->Cell(180, 2, 'Nomor : '.$this->data->quot_num, 0, 2, 'C');
		$pdf->Ln();
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(10);
		$pdf->Cell(30,5,'Kepada Yth,');
		$pdf->Cell(85);
		$pdf->Cell(30,5,'Jakarta, '.$this->data->tanggal);
		$pdf->Ln();
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10);
		
		$this->QuotYth($pdf,$this->data->nama_usaha,$this->data->customer);
		
		$pdf->Ln();
		$pdf->Cell(10);
		$pdf->Cell(10,5,'UP');
		$pdf->Cell(5);
		$pdf->Cell(5,5,':');
		//$pdf->Cell(5);
		$pdf->Cell(30,5,$this->data->customer);
		$pdf->Ln();
		$pdf->Cell(10);
		$pdf->Cell(10,5,'Email');
		$pdf->Cell(5);
		$pdf->Cell(5,5,':');
		//$pdf->Cell(5);
		$pdf->Cell(30,5,$this->data->email);
		$pdf->Ln();
		$pdf->Cell(10);
		$pdf->Cell(30,5,'Dengan hormat,');
		$pdf->Ln();

		$pdf->SetLeftMargin(20);
		$this->pembukaan($pdf,$this->data->jenistr,$this->data->nama_usaha,$this->data->subject);
		
		$pdf->Ln();
		$pdf->Ln();

		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(60,5,'Nama Produk',1,0,'C');
		$pdf->Cell(60,5,'Harga',1,0,'C');
		$pdf->Cell(20,5,'Qty',1,0,'C');
		$pdf->Cell(30,5,'Jumlah',1,0,'C');
		$pdf->Ln();
		$pdf->SetFont('Arial','',10);
		$pdf->SetFillColor(255,255,255); //set background color for multi cell

		$this->ListQuotationDataComparation($pdf,$this->data->items);		
		
		$pdf->Ln();
		$pdf->SetFont('Arial','B',8);

		$pdf->Cell(100,5,'- Harga diatas adalah tarif nett yang diterima oleh kami (tidak dipotong pph).',0,1);
		$pdf->Cell(100,5,'- Kami bukan PKP(Pengusaha Kena Pajak), tidak ada faktur pajak. Juga tidak ada NPWP untuk bukti potong PPh 23.',0,1);
		$pdf->SetFont('Arial','B',10);
		$pdf->Ln();
		$pdf->WriteHTML('<b>[  ] Transfer via Bank BCA KCU Bekasi No. Acc. 0663162851  a/n : Fajar Shodiq</b><br>');
		$pdf->WriteHTML('<b>[  ] Transfer via Bank Mandiri KCP Bekasi Ahmad Yani No. Acc. 1670001377901  a/n : Fajar Shodiq</b><br>');
		
		$this->QuotForm($pdf,$this->data->jenistr);
		
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,5,'Notes');
		$pdf->SetFont('Arial','',8);
		$pdf->Ln();

		//notes are here
		$this->NotesQuotation($pdf,$this->data->jenistr);

		$pdf->Cell(87.5,5,'Hormat  kami,',0,0,'C');
		$pdf->Cell(87.5,5,'Pemesan,',0,0,'C');
		$pdf->Ln();
		$pdf->Ln();
		$pdf->Ln();


		$pdf->Ln();
		$pdf->SetFont('Arial','BU',10);
		$pdf->Cell(87.5,5,$this->data->petugas,0,0,'C');
		$pdf->Cell(87.5,5,$this->data->nama_usaha,0,0,'C');
		$pdf->Ln();
		$pdf->SetFont('Arial','B',6);
		$pdf->Cell(87.5,2,'FAC Institute',0,0,'C');
		$pdf->Ln();
		$pdf->Ln();
		$pdf->Ln();
		if ($command!='display') {
			$pdf->Output($this->path.$this->nameFile,'F');
		}else{
			$pdf->Output();
		}
	}

	function pembukaan($pdf,$jenistr,$namausaha,$subjek){
		if ($jenistr=='1') {
			if ($namausaha=='-') {
				$pdf->WriteHTML('Sebelumnya kami ucapkan terimakasih atas ketertarikan menggunakan program ACCURATE di perusahaan anda Untuk <b>'.$subjek.'</b>, Kami memberikan penawaran <b>TERBAIK</b> berupa:');
			} else {
				$pdf->WriteHTML('Sebelumnya kami ucapkan terimakasih atas ketertarikan menggunakan program ACCURATE di <b>'.$namausaha.'</b> Untuk <b>'.$subjek.'</b>, Kami memberikan penawaran <b>TERBAIK</b> berupa:');	
			}
						
		}elseif($jenistr=='2'){
			if ($namausaha=='-') {
				$pdf->WriteHTML('Kami dari <b>FAC Institute</b> sebagai divisi Accounting Service. Terimakasih telah memilih FAC Institute sebagai penyedia jasa accounting service di perusahaan anda. Kami selalu berusaha untuk memberikan pelayanan terbaik di setiap jasa yang kami berikan.');
			} else {
				$pdf->WriteHTML('Kami dari <b>FAC Institute</b> sebagai divisi Accounting Service. Terimakasih telah memilih FAC Institute sebagai penyedia jasa accounting service di <b>'.$namausaha.'</b>. Kami selalu berusaha untuk memberikan pelayanan terbaik di setiap jasa yang kami berikan.');
			}
			
			
		}elseif ($jenistr=='3') {
			$pdf->WriteHTML('Kami dari <b>FAC Institute & Bakat Cendekia</b>. Kami merupakan partner dari pihak Accurate Accounting Software, sebagai Accurate Authorized Training Center (AATC) yang secara resmi menyelenggarakan Pelatihan Accurate. Kami memberikan penawaran <b>TERBAIK</b> berupa:');
		}else{
			$pdf->WriteHTML('Sebelumnya kami ucapkan terimakasih atas ketertarikan menggunakan program ACCURATE di <b>'.$namausaha.'</b> Untuk <b>'.$subjek.'</b>, Kami memberikan penawaran <b>TERBAIK</b> berupa:');
		}
	}

	function QuotYth($pdf,$namausaha,$namacust){
		if ($namausaha=='-') {
			$pdf->Cell(30,5,$namacust);
		} else {
			$pdf->Cell(30,5,$namausaha);
		}
		
	}

	function TandaTangan($pdf,$items){
		$y = 210;
		if (count($items)=='0') {
			
		} else {
			foreach ($items as $key) {
				$y += 5;
			}
		}
		// images/inv/ttd_rika.png
		$pdf->Image('/home/facinsti/public_html/images/inv/ttd_rika.png',49,$y,45,35);
// 		$pdf->Image('/home/facinsti/public_html/docs/stempelfac.jpg',49,$y,30,20);
	}

	function NotesQuotation($pdf,$jenistr){
		if ($jenistr=='1') {
			$pdf->Cell(100,5,'- Mendapatkan support by email.',0,1);
			$pdf->Cell(100,5,'- Pembayaran dilakukan sebelum implementasi dimulai.',0,1);
			$pdf->Cell(100,5,'- Menyediakan makan siang bagi implementator.',0,1);
			$pdf->Cell(100,5,'- Implementasi maksimal 7 jam termasuk istirahat makan siang, training bersifat time oriented, bukan result oriented.',0,1);
			$pdf->Cell(100,5,'- Implementasi diluar Jabodetabek, transport dan akomodasi ditanggung customer.',0,1);
			$pdf->Ln();
		}elseif ($jenistr=='2') {
			$pdf->Cell(100,5,'- Entry data bersifat result oriented, bukan time oriented.',0,1);
			$pdf->Cell(100,5,'- Kami diberi akses ke sumber data yang akan dientry.',0,1);
			$pdf->Ln();
		}elseif ($jenistr=='3') {
			$pdf->Cell(100,5,'- Mohon membawa Laptop Sendiri untuk Instalasi Aplikasi Edukasi Accurate-5',0,1);
			$pdf->Cell(100,5,'- Mendaftar kelas yang sama & bersamaan,',0,1);
			$pdf->Cell(10);
			$pdf->Cell(70,5,'> Potongan 10% per peserta (untuk 2-peserta)',0,1);
			$pdf->Cell(10);
			$pdf->Cell(70,5,'> Potongan 15% per peserta (untuk 3-peserta)',0,1);

			$pdf->Ln();
		} else {
			$pdf->Cell(100,5,'- Mendapatkan support by email.',0,1);
			$pdf->Cell(100,5,'- Pembayaran dilakukan sebelum implementasi dimulai.',0,1);
			$pdf->Cell(100,5,'- Menyediakan makan siang bagi implementator.',0,1);
			$pdf->Cell(100,5,'- Implementasi maksimal 7 jam termasuk istirahat makan siang, training bersifat time oriented, bukan result oriented.',0,1);
			$pdf->Cell(100,5,'- Implementasi diluar Jabodetabek, transport dan akomodasi ditanggung customer.',0,1);
		$pdf->Ln();
		}
		
	}
	
	public function ListItems($pdf,$data){
		$totalamount = 0;
		if (count($data)=='0') {
			
		} else {
			$totalamount = 0;
			foreach ($data as $key) {
				$pdf->SetFont('Arial','',9);
				$total = $key->harga * $key->itm_qty;
				$totalamount += $total;
				$pdf->setX(15);
				$pdf->SetWidths(array(90,50,10,30));
				$pdf->SetAligns(array('L','R','C','R'));
				$pdf->Row(array($key->nama_item,number_format($key->harga),$key->itm_qty,number_format($total)));
			}
			$pdf->SetFont('Arial','B',11);
			$pdf->setX(15);
			$pdf->Cell(150,5,'Total',1,0,'L');
			$pdf->Cell(30,5,'Rp '.number_format($totalamount),1,0,'R');
			$this->setTempdata($totalamount);

		}
		
	}

	public function ListQuotationData($pdf,$data){
		$totalamount = 0;
		if (count($data)=='0') {
			
		} else {
			foreach ($data as $key) {
				$total = $key->harga * $key->itm_qty;
				$totalamount += $total;
				$pdf->Cell(60,5,$key->nama_item,1,0,'L');
				$pdf->Cell(60,5, number_format($key->harga),1,0,'R');
				$pdf->Cell(20,5,$key->itm_qty,1,0,'C');
				$pdf->Cell(30,5,number_format($key->harga*$key->itm_qty),1,0,'R');
				$pdf->Ln();
			}
		}
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(140,5,'Total',1,0,'L');
		// $pdf->Cell(60,5,'',1,0,'C');
		// $pdf->Cell(20,5,'',1,0,'C');
		$pdf->Cell(30,5,'Rp   '.number_format($totalamount),1,0,'R');
	}
	
	private function ListQuotationDataComparation($pdf,$data){
		$totalamount = 0;
		if (count($data)=='0') {
			
		} else {
			foreach ($data as $key) {
				$total = $key->harga * $key->itm_qty;
				$totalamount += $total;
				$pdf->Cell(60,5,$key->nama_item,1,0,'L');
				$pdf->Cell(60,5, number_format($key->harga),1,0,'R');
				$pdf->Cell(20,5,'...',1,0,'C');
				$pdf->Cell(30,5,'...',1,0,'C');
				$pdf->Ln();
			}
		}
// 		$pdf->SetFont('Arial','B',10);
// 		$pdf->Cell(140,5,'Total',1,0,'L');
// 		$pdf->Cell(60,5,'',1,0,'C');
// 		$pdf->Cell(20,5,'',1,0,'C');
// 		$pdf->Cell(30,5,'Rp   '.number_format($totalamount),1,0,'R');
	}

	public function QuotForm($pdf,$data){
		if ($data=='1') {
			
			//formquotation
			$pdf->WriteHTML('<b>Untuk kebutuhan informasi, mohon diisi data berikut dengan lengkap.</b><br>');
			$pdf->Ln();
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(10,5,'Nama Perusahaan');
			$pdf->Cell(35);
			$pdf->Cell(5,5,':');
			$pdf->Cell(100,5,'............................................................................................................');
			$pdf->Ln();
			$pdf->Cell(10,5,'Jenis Usaha');
			$pdf->Cell(35);
			$pdf->Cell(5,5,':');
			$pdf->Cell(100,5,'............................................................................................................');
			$pdf->Ln();
			$pdf->Cell(10,5,'Alamat Perusahaan');
			$pdf->Cell(35);
			$pdf->Cell(5,5,':');
			$pdf->Cell(100,5,'............................................................................................................');
			$pdf->Ln();
			$pdf->Cell(10,5,'Waktu Pelaksanaan');
			$pdf->Cell(35);
			$pdf->Cell(5,5,':');
			$pdf->Cell(100,5,'............................................................................................................');
			$pdf->Ln();
			$pdf->Cell(10,5,'Varians Accurate');
			$pdf->Cell(35);
			$pdf->Cell(5,5,':');
			$pdf->Cell(30,5,'Standar/Deluxe/Enterprise');
			$pdf->SetFont('Arial','B',5);
			$pdf->Cell(15);
			$pdf->Cell(30,5,'* Coret yang tidak perlu');
			$pdf->Ln();
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(10,5,'Agenda/Kegiatan Training');
			$pdf->Cell(35);
			$pdf->Cell(5,5,':');
			$pdf->Cell(100,5,'1 ............................................................................................................',0,1);
			$pdf->Cell(50);
			$pdf->Cell(100,5,'2 ............................................................................................................',0,1);
		} elseif ($data=='2') {
			
			$pdf->WriteHTML('<b>Untuk kebutuhan informasi, mohon diisi data berikut dengan lengkap.</b><br>');
			$pdf->Ln();
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(10,5,'Nama Perusahaan');
			$pdf->Cell(35);
			$pdf->Cell(5,5,':');
			$pdf->Cell(100,5,'............................................................................................................');
			$pdf->Ln();
			$pdf->Cell(10,5,'Jenis Usaha');
			$pdf->Cell(35);
			$pdf->Cell(5,5,':');
			$pdf->Cell(100,5,'............................................................................................................');
			$pdf->Ln();
			$pdf->Cell(10,5,'Alamat Perusahaan');
			$pdf->Cell(35);
			$pdf->Cell(5,5,':');
			$pdf->Cell(100,5,'............................................................................................................');
			$pdf->Ln();
			$pdf->Cell(10,5,'Waktu Pelaksanaan');
			$pdf->Cell(35);
			$pdf->Cell(5,5,':');
			$pdf->Cell(100,5,'............................................................................................................');
			
			$pdf->Ln();
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(10,5,'Agenda/Kegiatan');
			$pdf->Cell(35);
			$pdf->Cell(5,5,':');
			$pdf->Cell(100,5,'1 ............................................................................................................',0,1);
			$pdf->Cell(50);
			$pdf->Cell(100,5,'2 ............................................................................................................',0,1);
			$pdf->Cell(50);
			$pdf->Cell(100,5,'3 ............................................................................................................',0,1);
			$pdf->Cell(50);
			$pdf->Cell(100,5,'4 ............................................................................................................',0,1);


		}elseif ($data=='3') {
			
			$pdf->WriteHTML('<b>Untuk kebutuhan informasi, mohon diisi data berikut dengan lengkap.</b><br>');
			$pdf->Ln();
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(10,5,'Nama Lengkap');
			$pdf->Cell(35);
			$pdf->Cell(5,5,':');
			$pdf->Cell(100,5,'............................................................................................................');
			$pdf->Ln();
			$pdf->Cell(10,5,'Pendidikan Terakhir');
			$pdf->Cell(35);
			$pdf->Cell(5,5,':');
			$pdf->Cell(100,5,'............................................................................................................');
			$pdf->Ln();
			$pdf->Cell(10,5,'Email');
			$pdf->Cell(35);
			$pdf->Cell(5,5,':');
			$pdf->Cell(100,5,'............................................................................................................');
			$pdf->Ln();
			$pdf->Cell(10,5,'Perusahaan');
			$pdf->Cell(35);
			$pdf->Cell(5,5,':');
			$pdf->Cell(100,5,'............................................................................................................');
			$pdf->Ln();
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(10,5,'Alamat');
			$pdf->Cell(35);
			$pdf->Cell(5,5,':');
			$pdf->Cell(100,5,'.............................................................................................................',0,1);
			$pdf->Cell(50);
			$pdf->Cell(100,5,'.............................................................................................................',0,1);
		}else {
			
		}
		
	}


	public function getPath(){
		return $this->path;
	}

	public function setPath($path){
		$this->path = $path;
	}

	public function getNameFile(){
		return $this->nameFile;
	}

	public function setNameFile($nameFile){
		$this->nameFile = $nameFile;
	}

	public function getData(){
		return $this->data;
	}

	public function setData($data){
		$this->data = $data;
	}

	public function getTempdata(){
		return $this->tempdata;
	}

	public function setTempdata($data){
		$this->tempdata = $data;
	}


}

class PDF_HTML extends FPDF{
	var $B=0;
	var $I=0;
	var $U=0;
	var $HREF='';
	var $ALIGN='';
	var $widths;
	var $aligns;

	function WriteHTML($html)
	{
		//HTML parser
		$html=str_replace("\n",' ',$html);
		$a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
		foreach($a as $i=>$e)
		{
			if($i%2==0)
			{
				//Text
				if($this->HREF)
					$this->PutLink($this->HREF,$e);
				elseif($this->ALIGN=='center')
					$this->Cell(0,5,$e,0,1,'C');
				else
					$this->Write(5,$e);
			}
			else
			{
				//Tag
				if($e[0]=='/')
					$this->CloseTag(strtoupper(substr($e,1)));
				else
				{
					//Extract properties
					$a2=explode(' ',$e);
					$tag=strtoupper(array_shift($a2));
					$prop=array();
					foreach($a2 as $v)
					{
						if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
							$prop[strtoupper($a3[1])]=$a3[2];
					}
					$this->OpenTag($tag,$prop);
				}
			}
		}
	}

	function OpenTag($tag,$prop)
	{
		//Opening tag
		if($tag=='B' || $tag=='I' || $tag=='U')
			$this->SetStyle($tag,true);
		if($tag=='A')
			$this->HREF=$prop['HREF'];
		if($tag=='BR')
			$this->Ln(5);
		if($tag=='P')
			$this->ALIGN=$prop['ALIGN'];
		if($tag=='HR')
		{
			if( !empty($prop['WIDTH']) )
				$Width = $prop['WIDTH'];
			else
				$Width = $this->w - $this->lMargin-$this->rMargin;
			$this->Ln(2);
			$x = $this->GetX();
			$y = $this->GetY();
			$this->SetLineWidth(0.4);
			$this->Line($x,$y,$x+$Width,$y);
			$this->SetLineWidth(0.2);
			$this->Ln(2);
		}
	}

	function CloseTag($tag)
	{
		//Closing tag
		if($tag=='B' || $tag=='I' || $tag=='U')
			$this->SetStyle($tag,false);
		if($tag=='A')
			$this->HREF='';
		if($tag=='P')
			$this->ALIGN='';
	}

	function SetStyle($tag,$enable)
	{
		//Modify style and select corresponding font
		$this->$tag+=($enable ? 1 : -1);
		$style='';
		foreach(array('B','I','U') as $s)
			if($this->$s>0)
				$style.=$s;
		$this->SetFont('',$style);
	}

	function PutLink($URL,$txt)
	{
		//Put a hyperlink
		$this->SetTextColor(0,0,255);
		$this->SetStyle('U',true);
		$this->Write(5,$txt,$URL);
		$this->SetStyle('U',false);
		$this->SetTextColor(0);
	}

	//start mc_table
function SetWidths($w){
	//Set the array of column widths
	$this->widths=$w;
}

function SetAligns($a){
	//Set the array of column alignments
	$this->aligns=$a;
}

function Row($data){
	//Calculate the height of the row
	$nb=0;
	for($i=0;$i<count($data);$i++)
		$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
	$h=5*$nb;
	//Issue a page break first if needed
	$this->CheckPageBreak($h);
	//Draw the cells of the row
	for($i=0;$i<count($data);$i++){
		$w=$this->widths[$i];
		$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
		//Save the current position
		$x=$this->GetX();
		$y=$this->GetY();
		//Draw the border
		$this->Rect($x,$y,$w,$h);
		//Print the text
		$this->MultiCell($w,5,$data[$i],0,$a);
		//Put the position to the right of the cell
		$this->SetXY($x+$w,$y);
	}
	//Go to the next line
	$this->Ln($h);
}

function CheckPageBreak($h){
	//If the height h would cause an overflow, add a new page immediately
	if($this->GetY()+$h>$this->PageBreakTrigger)
		$this->AddPage($this->CurOrientation);
}

function NbLines($w,$txt){
	//Computes the number of lines a MultiCell of width w will take
	$cw=&$this->CurrentFont['cw'];
	if($w==0)
		$w=$this->w-$this->rMargin-$this->x;
	$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
	$s=str_replace("\r",'',$txt);
	$nb=strlen($s);
	if($nb>0 and $s[$nb-1]=="\n")
		$nb--;
	$sep=-1;
	$i=0;
	$j=0;
	$l=0;
	$nl=1;
	while($i<$nb)
	{
		$c=$s[$i];
		if($c=="\n")
		{
			$i++;
			$sep=-1;
			$j=$i;
			$l=0;
			$nl++;
			continue;
		}
		if($c==' ')
			$sep=$i;
		$l+=$cw[$c];
		if($l>$wmax){
			if($sep==-1){
				if($i==$j)
					$i++;
			}
			else
				$i=$sep+1;
			$sep=-1;
			$j=$i;
			$l=0;
			$nl++;
		}
		else
			$i++;
	}
	return $nl;
}

function myFooter(){
    // Go to 1.5 cm from bottom
    $this->SetY(-15);
    // Select Arial italic 8
    $this->SetFont('Arial','',10);
    // Print centered page number
    $this->SetTextColor(255, 0, 0);
	$this->Cell(175,3,'FAC Institute',0,1,'L');
	$this->SetTextColor(0, 0, 0);
	$this->Cell(175,3,'Lembaga Pendidikan Komputer Sistem Akuntansi ACCURATE',0,1,'L');
	$this->Cell(175,3,'Jl. Raya Jatiwaringin, No. 8 Pangkalan Jati Jakarta Timur',0,1,'L');
	$this->Cell(175,3,'Telp. 0812 900 83983 / 0821 220 48075 /​ 0823 1194 4359',0,1,'L');
	$this->SetTextColor(0, 0, 255);
	$this->Cell(175,3,'Email: training.facinstitute@gmail.com, training@fac-institute.com, cs.facinstitute@gmail.com',0,1,'L');
	$this->Cell(175,3,'Website: http://fac-institute.com, http://fac-institute.blogspot.com',0,1,'L');
}


}


class PDF_MC_Table extends FPDF
{
var $widths;
var $aligns;

function SetWidths($w){
	//Set the array of column widths
	$this->widths=$w;
}

function SetAligns($a){
	//Set the array of column alignments
	$this->aligns=$a;
}

function Row($data){
	//Calculate the height of the row
	$nb=0;
	for($i=0;$i<count($data);$i++)
		$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
	$h=5*$nb;
	//Issue a page break first if needed
	$this->CheckPageBreak($h);
	//Draw the cells of the row
	for($i=0;$i<count($data);$i++){
		$w=$this->widths[$i];
		$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
		//Save the current position
		$x=$this->GetX();
		$y=$this->GetY();
		//Draw the border
		$this->Rect($x,$y,$w,$h);
		//Print the text
		$this->MultiCell($w,5,$data[$i],0,$a);
		//Put the position to the right of the cell
		$this->SetXY($x+$w,$y);
	}
	//Go to the next line
	$this->Ln($h);
}

function CheckPageBreak($h){
	//If the height h would cause an overflow, add a new page immediately
	if($this->GetY()+$h>$this->PageBreakTrigger)
		$this->AddPage($this->CurOrientation);
}

function NbLines($w,$txt){
	//Computes the number of lines a MultiCell of width w will take
	$cw=&$this->CurrentFont['cw'];
	if($w==0)
		$w=$this->w-$this->rMargin-$this->x;
	$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
	$s=str_replace("\r",'',$txt);
	$nb=strlen($s);
	if($nb>0 and $s[$nb-1]=="\n")
		$nb--;
	$sep=-1;
	$i=0;
	$j=0;
	$l=0;
	$nl=1;
	while($i<$nb)
	{
		$c=$s[$i];
		if($c=="\n")
		{
			$i++;
			$sep=-1;
			$j=$i;
			$l=0;
			$nl++;
			continue;
		}
		if($c==' ')
			$sep=$i;
		$l+=$cw[$c];
		if($l>$wmax)
		{
			if($sep==-1)
			{
				if($i==$j)
					$i++;
			}
			else
				$i=$sep+1;
			$sep=-1;
			$j=$i;
			$l=0;
			$nl++;
		}
		else
			$i++;
	}
	return $nl;
}


}



?>