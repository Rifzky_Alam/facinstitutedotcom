<?php
include_once 'Koneksi.php';
class Customer extends Koneksi{
	var $dbHost;
	private $id;
	private $perusahaan;
	private $nama;
	private $gender;
	private $email;
	private $telp;
	private $jabatan;
	private $jenispengguna;
	private $marketingid;

	//constructor
	function __construct(){
		$this->dbHost = $this->bukaKoneksi();
	}
	
	public function SetLocalFormat(){
		$query = "SET @@lc_time_names;";
		$statement=$this->dbHost->prepare($query);

		// echo $query;
		$statement->execute();
	}
	
	public function DetailSupportClient($id){
	    $query = "SELECT `sticket`, DATE_FORMAT(`dcurdate`, '%W, %e %M %Y') AS tanggal, perusahaan.nama AS nama_usaha,new_customer.nama_cust AS `sname`, new_customer.telp_cust AS `sphone`, new_customer.email_cust AS `semail`, `sisstitle`, `ssolution`, media.dt_desc AS via, users.nama AS pic, `istatus`, `snotes` 
        FROM `cs_suppdet`
        INNER JOIN `perusahaan` ON perusahaan.id=cs_suppdet.sfkusaha
        INNER JOIN desc_tbl AS media
        ON media.dt_relate_tbl='cs_suppdet' AND media.dt_flag = cs_suppdet.iflmedia
        INNER JOIN users ON users.username=cs_suppdet.spic
        INNER JOIN new_customer ON new_customer.id_cust=cs_suppdet.sfkclient
        WHERE cs_suppdet.sticket=:id";
        $statement=$this->dbHost->prepare($query);
        @$statement->bindParam(':id',$id, PDO::PARAM_STR,35);
		$statement->execute();
		return $statement->fetch(PDO::FETCH_ASSOC);
	}


	public function InputNewCustomer(){
		$query = "INSERT INTO `new_customer` (`id_cust`, `id_perusahaan`, `nama_cust`, `gender_cust`, `email_cust`, `telp_cust`, `jabatan_cust`, `jenis_pengguna_cust`, `marketing_id`)
		VALUES (:id, :idperusahaan, :namacust, :gender, :emailcust, :telpcust, :jabatancust, :jp, :mi);";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':id',$this->id, PDO::PARAM_STR,35);
		@$statement->bindParam(':idperusahaan',$this->perusahaan, PDO::PARAM_STR,35);
		@$statement->bindParam(':namacust',$this->nama, PDO::PARAM_STR,75);
		@$statement->bindParam(':gender',$this->gender, PDO::PARAM_STR,75);
		@$statement->bindParam(':emailcust',$this->email, PDO::PARAM_STR,125);
		@$statement->bindParam(':telpcust',$this->telp, PDO::PARAM_STR,20);
		@$statement->bindParam(':jabatancust',$this->jabatan, PDO::PARAM_STR,75);
		@$statement->bindParam(':jp',$this->jenispengguna, PDO::PARAM_STR,2);
		@$statement->bindParam(':mi',$this->marketingid, PDO::PARAM_STR,35);


		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}

	}


	public function EditPerusahaanCustomer(){
		$query = "UPDATE `new_customer` 
		SET `id_perusahaan` = :idusaha 
		WHERE `new_customer`.`id_cust` = :idcust;";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':idcust',$this->id, PDO::PARAM_STR,35);
		@$statement->bindParam(':idusaha',$this->perusahaan, PDO::PARAM_STR,35);	

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function EditCustomer(){
		$query = "UPDATE `new_customer`
		SET `id_perusahaan`=:idperusahaan,`nama_cust`=:namacust, `gender_cust`=:gender,`email_cust`=:emailcust,`telp_cust`=:telpcust,`jabatan_cust`=:jabatancust,`jenis_pengguna_cust`=:jp,`marketing_id`=:mi
		WHERE `id_cust`=:id;";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':id',$this->id, PDO::PARAM_STR,35);
		@$statement->bindParam(':idperusahaan',$this->perusahaan, PDO::PARAM_STR,35);
		@$statement->bindParam(':namacust',$this->nama, PDO::PARAM_STR,75);
		@$statement->bindParam(':gender',$this->gender, PDO::PARAM_STR,75);
		@$statement->bindParam(':emailcust',$this->email, PDO::PARAM_STR,125);
		@$statement->bindParam(':telpcust',$this->telp, PDO::PARAM_STR,20);
		@$statement->bindParam(':jabatancust',$this->jabatan, PDO::PARAM_STR,75);
		@$statement->bindParam(':jp',$this->jenispengguna, PDO::PARAM_STR,2);
		@$statement->bindParam(':mi',$this->marketingid, PDO::PARAM_STR,35);
		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function SearchCustomer(){
		$myname="%".$this->nama."%";
		$query = "SELECT `id_cust`, `nama` AS `nama_usaha`, `nama_cust`, `email_cust`, `telp_cust`, (SELECT GROUP_CONCAT(`trans_id` SEPARATOR ' # ') FROM `new_transaksi` WHERE `new_transaksi`.`trans_cust_id`=`id_cust`) AS `data_tr` FROM `new_customer`,`perusahaan` WHERE `new_customer`.`id_perusahaan` = `perusahaan`.`id` AND `nama_cust` LIKE :nama";
		if ($this->marketingid!='') {
			$query .= " AND `marketing_id`='".$this->marketingid."'";
		}
		$query .= ";";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':nama',$myname,PDO::PARAM_STR,35);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function DeleteCustomer(){
		$query = "DELETE FROM `new_customer` WHERE `id_cust`=:id;";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':id',$this->id,PDO::PARAM_STR,35);
		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function FetchSelectedID(){
		$query = "SELECT * FROM `new_customer` WHERE `id_cust`=:id";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':id',$this->id,PDO::PARAM_STR,35);

		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function FetchSelectedID2(){
		$query = "SELECT `id_cust` AS `id`, `id_perusahaan`, `nama_cust` AS `nama`, `email_cust` AS `email`, `telp_cust` AS `telepon`, `jabatan_cust` AS `jabatan`, `jenis_pengguna_cust` AS `jenis_pengguna`, `marketing_id`,`status_cust`
		FROM `new_customer` WHERE `id_cust`=:id;";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':id',$this->id,PDO::PARAM_STR,35);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function FetchAll($offset,$limit){
		$query = "SELECT `id_cust` AS `id`, `id_perusahaan`, `nama_cust` AS `nama`, `email_cust` AS `email`, `telp_cust` AS `telepon`, `jabatan_cust` AS `jabatan`, `jenis_pengguna_cust` AS `jenis_pengguna`, `marketing_id`,`status_cust`
		FROM `new_customer` WHERE `id_perusahaan`=:perusahaan LIMIT $offset, $limit;";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':perusahaan',$this->perusahaan, PDO::PARAM_STR,35);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}


	public function CountCustData(){
		$query = "SELECT COUNT(`id_cust`) AS `jumlah` FROM `new_customer`;";
		$statement=$this->dbHost->prepare($query);

		$statement->execute();
		$hasil = $statement->fetch(PDO::FETCH_OBJ);
		return $hasil->jumlah;
	}


	public function TableCustomerList($offset,$limit){
		$query = "SELECT `id_cust`, `nama` AS `nama_usaha`, `nama_cust`, `email_cust`, `telp_cust`, (SELECT GROUP_CONCAT(`trans_id` SEPARATOR ' # ') FROM `new_transaksi` WHERE `new_transaksi`.`trans_cust_id`=`id_cust`) AS `data_tr` FROM `new_customer`,`perusahaan` WHERE `new_customer`.`id_perusahaan` = `perusahaan`.`id` LIMIT $offset,$limit;";
		$statement=$this->dbHost->prepare($query);


		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function ReportCustomerPerMarketing(){
		$query = "SELECT `marketing_id`,`marketing_nama`,
		(SELECT COUNT(id_cust)
		FROM `new_customer` AS `nc`
		WHERE `nc`.`marketing_id`=`new_marketing`.`marketing_id`) AS `total_cust`
		FROM `new_marketing`;";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':idmarketing',$value,PDO::PARAM_STR,35);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function getID(){
		return $this->id;
	}

	public function setID($value){
		$this->id = $value;
	}

	public function getPerusahaan(){
		return $this->perusahaan;
	}

	public function setPerusahaan($value){
		$this->perusahaan = $value;
	}

	public function getNama(){
		return $this->nama;
	}

	public function setNama($value){
		$this->nama = $value;
	}
	
	public function getGender(){
		return $this->gender;
	}

	public function setGender($value){
		$this->gender = $value;
	}

	public function getEmail(){
		return $this->email;
	}

	public function setEmail($value){
		$this->email = $value;
	}

	public function getTelepon(){
		return $this->telp;
	}

	public function setTelepon($value){
		$this->telp = $value;
	}

	public function getJabatan(){
		return $this->jabatan;
	}

	public function setJabatan($value){
		$this->jabatan = $value;
	}

	public function getJenispengguna(){
		return $this->jenispengguna;
	}

	public function setJenispengguna($value){
		$this->jenispengguna = $value;
	}

	public function getMarketingID(){
		return $this->marketingid;
	}

	public function setMarketingID($value){
		$this->marketingid = $value;
	}

}


?>
