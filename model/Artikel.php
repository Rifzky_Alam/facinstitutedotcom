<?php
include_once 'Koneksi.php';

class Artikel extends Koneksi{

	function __construct(){
		$this->dbHost = $this->bukaKoneksi();
	}

	public function countAllArtikel(){
		$query="SELECT COUNT(`id`) AS jumlah FROM `artikel`";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();

		$hasil = $statement->fetch(PDO::FETCH_OBJ);
		return $hasil->jumlah;
	}


	public function homeFetchAll(){
		$query="SELECT * FROM `artikel` ORDER BY tanggal DESC LIMIT 0 , 8";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function fetchAll($start){
		$lower = $start * 6;
		$query = "SELECT * FROM artikel ORDER BY tanggal DESC LIMIT $lower , 6";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;

	}

	public function searchTitle($key){
		$key = '%'.$key.'%';
		$query = "SELECT * FROM artikel WHERE `judul` LIKE :key";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':key',$key,PDO::PARAM_STR,500);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function searchTitleAndContent($key){
		$key = '%'.$key.'%';
		$query = "SELECT * FROM artikel WHERE `judul` LIKE :key OR `isi` LIKE :isi";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':key',$key,PDO::PARAM_STR,255);
		$statement->bindParam(':isi',$key,PDO::PARAM_STR,8500);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function fetchAPI(){
		$query = "SELECT id,tanggal,judul,author,image_url FROM artikel ORDER BY tanggal DESC";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function fetchIsiAPI($id){
		$query = "SELECT isi FROM artikel where id='$id'";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function selectById($id){
		$query = "SELECT * FROM artikel WHERE id='$id'";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}


	public function coba(){
		return "okelah";
	}

	public function insertArtikel($objek){
		include_once 'MyXML.php';
		$xml = new MyXML();
		$xml->setDir('/home/facinsti/public_html/');
		$xml->setNamafile('sitemap.xml');

		$id = md5(date('Y-m-d').'-'.$objek->judul);
		$query = "INSERT INTO `artikel` (`id`, `tanggal`, `judul`, `isi`, `tags`,`image_url`,`author`)
		VALUES ('$id', :tanggal, :judul, :isi, :tags,:imageUrl,:author)";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':tanggal',date('Y-m-d'),PDO::PARAM_STR,15);
		$statement->bindParam(':judul',$objek->judul,PDO::PARAM_STR,500);
		$statement->bindParam(':isi',$objek->isi,PDO::PARAM_STR,8000);
		$statement->bindParam(':tags',$objek->tags,PDO::PARAM_STR,255);
		$statement->bindParam(':imageUrl',$objek->imageUrl,PDO::PARAM_STR,150);
		$statement->bindParam(':author',$objek->author,PDO::PARAM_STR,40);

		if ($statement->execute()){
			$xml->SitemapAddArtikel($id);
			return true;
		}else{
			return false;
		}
	}


	public function viewerAddition($id){
		$query="UPDATE artikel SET `viewer`= `viewer`+ 1 WHERE `id`='$id'";
		$statement=$this->dbHost->prepare($query);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}

	}

	public function editArtikel($objek){
		$query = "UPDATE artikel
		SET `judul` = :judul,
		`isi` =:isi,
		`tags`=:tagz
		WHERE `id`=:aidi";
		$statement = $this->dbHost->prepare($query);

		$statement->bindParam(':judul',$objek->judul,PDO::PARAM_STR,500);
		$statement->bindParam(':isi',$objek->isi,PDO::PARAM_STR,8000);
		$statement->bindParam(':tagz',$objek->tags,PDO::PARAM_STR,255);
		$statement->bindParam(':aidi',$objek->id,PDO::PARAM_STR,75);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}

	}

	public function editArtikelWithImage($objek){
		$query = "UPDATE artikel
		SET `judul` = :judul,
		`isi` =:isi,
		`image_url` =:imagez,
		`tags`=:tagz
		WHERE `id`=:aidi";
		$statement = $this->dbHost->prepare($query);

		$statement->bindParam(':judul',$objek->judul,PDO::PARAM_STR,500);
		$statement->bindParam(':isi',$objek->isi,PDO::PARAM_STR,8000);
		$statement->bindParam(':imagez',$objek->gambar,PDO::PARAM_STR,255);
		$statement->bindParam(':tagz',$objek->tags,PDO::PARAM_STR,255);
		$statement->bindParam(':aidi',$objek->id,PDO::PARAM_STR,75);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}

	}

	public function deleteArtikel($id){
		$query = "DELETE FROM artikel WHERE id=:id";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':id',$id,PDO::PARAM_STR,75);
		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
	}

	public function selectJudulAndID(){
		$query="SELECT id,judul FROM artikel ORDER BY viewer DESC LIMIT 0, 7";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

}

?>
