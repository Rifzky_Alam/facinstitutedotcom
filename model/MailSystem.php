<?php 
include_once 'Koneksi.php';
class MailSystem extends Koneksi{
	

	function __construct(){
		$this->dbHost = $this->bukaKoneksi();
	}


	public function inputData($objek){
		$query="INSERT INTO `penawaran` (`id`, `email`, `pengirim`, `waktu_kirim`) 
		VALUES (NULL, :mail, :nama, CURRENT_TIMESTAMP)";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':mail',$objek->email_kontak,PDO::PARAM_STR,75);
		$statement->bindParam(':nama',$objek->petugas,PDO::PARAM_STR,50);

		if ($statement->execute()){
			return true;
		}else{
			return false;
		}

	}

}


?>