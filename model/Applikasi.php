<?php 
include_once 'Koneksi.php';
class App extends Koneksi{
	
	function __construct(){
		$this->dbHost = $this->bukaKoneksi();
	}


	public function cekSession($obj){
		$query="SELECT aktif FROM vb_auth WHERE id =:id AND sandi=:sandi";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':id',$obj->username, PDO::PARAM_STR,35);
		$statement->bindParam(':sandi',$obj->password, PDO::PARAM_STR,35);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function getSession($id){
		$query="SELECT sesi FROM vb_auth WHERE id =:id";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':id',$id, PDO::PARAM_STR,35);
		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return $dataz;
	}

	public function setOnline($id,$ip){
		$query="UPDATE vb_auth SET aktif=1, ip=:ip, ip_forward=:ip2, sesi=:sesi WHERE id =:id";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':id',$id, PDO::PARAM_STR,35);
		$statement->bindParam(':ip',$ip->normal_ip, PDO::PARAM_STR,45);
		$statement->bindParam(':ip2',$ip->forward, PDO::PARAM_STR,45);
		$waktu = md5(date('h:i:sa'));
		$statement->bindParam(':sesi',$waktu, PDO::PARAM_STR,45);
		if ($statement->execute()){
			return true;
		}else{
			return false;
		}
	}

	public function setOffline($id,$ip){
		$query="UPDATE vb_auth SET aktif=0, ip=:ip, ip_forward=:ip2 WHERE id =:id AND sesi=:sesi";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':id',$id, PDO::PARAM_STR,35);
		$statement->bindParam(':ip',$ip->normal_ip, PDO::PARAM_STR,45);
		$statement->bindParam(':ip2',$ip->forward, PDO::PARAM_STR,45);
		$statement->bindParam(':sesi',$ip->sesi, PDO::PARAM_STR,45);
		
		if ($statement->execute()){
			return 1;
		}else{
			return 0;
		}
	}
}


?>