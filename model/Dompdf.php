<?php 
require_once '../dompdf/autoload.inc.php';
use Dompdf\Dompdf;

class Mypdf{
	private $content;
	private $path;
	private $filename;
	
	public function getFilename(){
		return $this->filename;
	}

	public function setFilename($value){
		$this->filename=$value;
	}	

	public function getPath(){
		return $this->path;
	}

	public function setPath($value){
		$this->path=$value;
	}	

	public function getContent(){
		return $this->content;
	}

	public function setContent($value){
		$this->content=$value;
	}

	public function Display($html){
		
		// instantiate and use the dompdf class
		$dompdf = new Dompdf();
		$dompdf->loadHtml($html);

		// (Optional) Setup the paper size and orientation
		$dompdf->setPaper('A4', 'potrait');

		// Render the HTML as PDF
		$dompdf->render();

		// Output the generated PDF to Browser
		$dompdf->stream('my.pdf',array('Attachment'=>0));
	}

	public function Create(){
		// instantiate and use the dompdf class
		$dompdf = new Dompdf();
		$dompdf->loadHtml($this->content);

		// (Optional) Setup the paper size and orientation
		$dompdf->setPaper('A4', 'potrait');

		// Render the HTML as PDF
		$dompdf->render();

		$output = $dompdf->output();
		file_put_contents($this->path.$this->filename.'.pdf', $output);
	}

}

?>