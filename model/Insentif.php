<?php 
include_once 'Koneksi.php';
class Insentif extends Koneksi{
	var $dbHost;
	private $idlaporan;
	private $transid;
	private $custid;
	private $usahaid;
	private $jenistransaksi;
	private $dp;
	private $ppn;
	private $utklokasi;
	private $utkwaktu;
	private $discount;
	private $notes;
	private $status;
	private $tanggal;
	private $agenda;
	private $items;
	private $petugas;
	private $folder;
	private $data;
	

	//constructor
	function __construct(){
		$this->dbHost = $this->bukaKoneksi();
	}

	public function UpdateJumlahAttendance($trans,$tanggal,$jml){
		$query = "UPDATE intensif_summary
		LEFT JOIN laporan_harian 
		ON laporan_harian.id=intensif_summary.is_id_laporan
		SET intensif_summary.is_ttl_attnd=:jml
		WHERE laporan_harian.tanggal=:tanggal 
		AND `intensif_summary`.`is_id_trans`=:trans";
		$statement=$this->dbHost->prepare($query);

		$statement->bindParam(':trans',$trans,PDO::PARAM_STR,35);
		$statement->bindParam(':tanggal',$tanggal,PDO::PARAM_STR,35);
		$statement->bindParam(':jml',$jml,PDO::PARAM_STR,35);


		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function CariJumlahTransaksi($trans,$tanggal){
		$query = "SELECT COUNT(id) AS jumlah FROM `intensif_summary`  
		LEFT JOIN `laporan_harian` ON laporan_harian.id=intensif_summary.is_id_laporan
		WHERE is_id_trans=:trans AND laporan_harian.tanggal=:tanggal";
		$statement=$this->dbHost->prepare($query);

		$statement->bindParam(':trans',$trans,PDO::PARAM_STR,35);
		$statement->bindParam(':tanggal',$tanggal,PDO::PARAM_STR,35);

		$statement->execute();
		$hasil = $statement->fetch(PDO::FETCH_OBJ);
		return $hasil->jumlah;
	}

	public function GetIdLaporanByPeran($idperan,$transaksi,$tanggal){
		$query = "SELECT `intensif_summary`.`is_id_laporan` AS `id` FROM `intensif_summary` 
		INNER JOIN `laporan_harian` 
		ON `laporan_harian`.`id` = `intensif_summary`.`is_id_laporan`
		WHERE `is_id_trans`=:transaksi
		AND `laporan_harian`.`tanggal`=:tanggal
		AND `laporan_harian`.`peran`=:peran";
		$statement=$this->dbHost->prepare($query);

		$statement->bindParam(':transaksi',$transaksi,PDO::PARAM_STR,35);
		$statement->bindParam(':tanggal',$tanggal,PDO::PARAM_STR,35);
		$statement->bindParam(':peran',$idperan,PDO::PARAM_STR,35);

		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function MyroleOnGooglecal($idlaporan){
		$query = "SELECT google_cal_attendees.gca_user_role AS myrole
		FROM `laporan_harian`
		INNER JOIN `absensi` ON `absensi`.`id`=`laporan_harian`.`id_absen`
		INNER JOIN `perusahaan` ON `perusahaan`.`id`=`absensi`.`nama_perusahaan`
		INNER JOIN `new_transaksi` ON `new_transaksi`.`trans_id_usaha`=`perusahaan`.`id`
		INNER JOIN `fac_calendar` 
		ON `fac_calendar`.`acara`=`new_transaksi`.`trans_id`
		AND `fac_calendar`.`tanggal`=`laporan_harian`.`tanggal`
		INNER JOIN `google_cal_data` 
		ON `google_cal_data`.`g_id_cal`=`fac_calendar`.`id`
		INNER JOIN `google_cal_attendees` 
		ON `google_cal_attendees`.`gca_source`=`google_cal_data`.`g_row`
		INNER JOIN `users` 
		ON `users`.`email`=`google_cal_attendees`.`gca_user_email` 
		AND `users`.`username`=`laporan_harian`.`username`
		WHERE `laporan_harian`.`id` =:idlaporan";
		$statement=$this->dbHost->prepare($query);

		$statement->bindParam(':idlaporan',$idlaporan,PDO::PARAM_STR,435);
		$statement->execute();
		$hasil = $statement->fetch(PDO::FETCH_ASSOC);
		return $hasil['myrole'];
	}

	public function TentukanPeranGoogleCalendar($idlaporan){
		$query = "SELECT COUNT(google_cal_attendees.gca_source) AS jumlah
		FROM `laporan_harian`
		INNER JOIN `absensi` ON `absensi`.`id`=`laporan_harian`.`id_absen`
		INNER JOIN `perusahaan` ON `perusahaan`.`id`=`absensi`.`nama_perusahaan`
		INNER JOIN `new_transaksi` ON `new_transaksi`.`trans_id_usaha`=`perusahaan`.`id`
		INNER JOIN `fac_calendar` 
		ON `fac_calendar`.`acara`=`new_transaksi`.`trans_id`
		AND `fac_calendar`.`tanggal`=`laporan_harian`.`tanggal`
		INNER JOIN `google_cal_data` 
		ON `google_cal_data`.`g_id_cal`=`fac_calendar`.`id`
		INNER JOIN `google_cal_attendees` 
		ON `google_cal_attendees`.`gca_source`=`google_cal_data`.`g_row`
		WHERE `laporan_harian`.`id` = '6a256eccabe02ada806defd4a44de8bc'
		AND google_cal_attendees.gca_user_role='2'";
		$statement=$this->dbHost->prepare($query);

		$statement->bindParam(':id',$idlaporan,PDO::PARAM_STR,435);

		$statement->execute();
		$hasil = $statement->fetch(PDO::FETCH_ASSOC);
		return $hasil['jumlah'];
	}

	// @url "administrasi/user/laporan-harian?summary"
	public function HitungPeranBerdasarkanTrans($idperan,$transaksi,$tanggal){
		$query = "SELECT COUNT(`intensif_summary`.`is_id_laporan`) AS `jumlah` FROM `intensif_summary` 
		INNER JOIN `laporan_harian` 
		ON `laporan_harian`.`id` = `intensif_summary`.`is_id_laporan`
		WHERE `is_id_trans`=:transaksi
		AND `laporan_harian`.`tanggal`=:tanggal
		AND `laporan_harian`.`peran`=:peran";
		$statement=$this->dbHost->prepare($query);

		$statement->bindParam(':transaksi',$transaksi,PDO::PARAM_STR,35);
		$statement->bindParam(':tanggal',$tanggal,PDO::PARAM_STR,35);
		$statement->bindParam(':peran',$idperan,PDO::PARAM_STR,35);

		$statement->execute();
		$hasil = $statement->fetch(PDO::FETCH_OBJ);
		return $hasil->jumlah;
	}

	public function GetMySummary($idlaporan){
		$query = "SELECT `laporan_harian`.`username`,`users`.`email` AS `user_email`,`laporan_harian`.`tanggal` AS `tanggal`, `users`.`usr_level`,`status_kerja`,`peran`,`kegiatan`,`jns_hari_kerja`, IFNULL(`acara_harian`.`transaksi`,'') AS `transaksi`,
		`new_transaksi`.`trans_jenis`,IFNULL(`trans_va`,'0') AS `trans_va`,`kec`,`kota`,`provinsi`,
		IFNULL(`ins_acc`.`ija_id`,'0') AS `ija_id`, IFNULL(`ins_peran`.`ip_id`,'0') AS `ip_id`,
		IFNULL(`ins_hari`.`ihk_id`,'0') AS `ihk_id`,IFNULL(`ins_wil`.`itw_wil_kode`,'0') AS `itw_wil_kode`,
		`ins_wil`.`itw_id` AS `itw_id`,
		IFNULL(`intensif_summary`.`is_id_laporan`,'-') AS `summary`,
        `ins_jns_tr`.`ijt_acc` AS `ijt_acc`,
        `ins_jns_tr`.`ijt_hari` AS `ijt_hari`,
        `ins_jns_tr`.`ijt_wilayah` AS `ijt_wilayah`
		FROM `laporan_harian` 
		INNER JOIN `absensi` ON `absensi`.`id`=`laporan_harian`.`id_absen`
		INNER JOIN `users` ON `users`.`username`=`laporan_harian`.`username`
		LEFT JOIN `perusahaan` ON `perusahaan`.`id`=`absensi`.`nama_perusahaan`
		LEFT JOIN `acara_harian` 
		ON `acara_harian`.`tanggal`=`absensi`.`tanggal_absen` AND `absensi`.`nama_perusahaan`=`acara_harian`.`id`
		LEFT JOIN `new_transaksi` ON `acara_harian`.`transaksi` = `new_transaksi`.`trans_id`
        LEFT JOIN (
        	SELECT `ijt_jns_trans`, `ijt_acc`, `ijt_hari`, `ijt_wilayah`, `ijt_status` 
            FROM `intensif_jns_trans` 
            WHERE `ijt_status`='1'
        ) AS `ins_jns_tr`
        ON `ins_jns_tr`.`ijt_jns_trans`=`new_transaksi`.`trans_jenis`
        LEFT JOIN (
        	SELECT `ija_id`,`ija_lvl_trainer`,`ija_jns_acc` FROM `intensif_jenis_acc`
            WHERE `intensif_jenis_acc`.`ija_status`='1'
        ) AS `ins_acc` 
        ON `ins_acc`.`ija_lvl_trainer`= `users`.`usr_level`
        AND `ins_acc`.`ija_jns_acc`=`new_transaksi`.`trans_va`
        LEFT JOIN (
        	SELECT `ip_id`,`ip_peran` FROM `intensif_peran`
            WHERE `ip_status`='1'
        ) AS `ins_peran`
        ON `ins_peran`.`ip_peran`=`laporan_harian`.`peran`
        LEFT JOIN (
        	SELECT `ihk_id`,`ihk_jenis_hari` FROM `intensif_hari_kerja`
            WHERE `intensif_hari_kerja`.`ihk_status`='1'
        ) AS `ins_hari`
        ON `ins_hari`.`ihk_jenis_hari`=`laporan_harian`.`jns_hari_kerja`
        LEFT JOIN `intensif_summary` 
        ON `intensif_summary`.`is_id_laporan`=`laporan_harian`.`id`
        LEFT JOIN (
        	SELECT `itw_id`,`itw_kecamatan`, `itw_kota`, `itw_provinsi`, `itw_wil_kode` 
            FROM `intensif_tbl_wil`
            WHERE `intensif_tbl_wil`.`itw_status`='1'
        ) AS `ins_wil`
        ON `ins_wil`.`itw_provinsi`=`perusahaan`.`provinsi` 
        AND `ins_wil`.`itw_kota`=`perusahaan`.`kota` 
        AND `ins_wil`.`itw_kecamatan`=`perusahaan`.`kec`
        OR `ins_wil`.`itw_provinsi`=`perusahaan`.`provinsi` 
        AND `ins_wil`.`itw_kota`=`perusahaan`.`kota` 
        AND `ins_wil`.`itw_kecamatan`='88888888'
        OR `ins_wil`.`itw_provinsi`=`perusahaan`.`provinsi` 
        AND `ins_wil`.`itw_kota`='88888888'
        AND `ins_wil`.`itw_kecamatan`='88888888'
		WHERE `laporan_harian`.`id`=:idlaporan";
		$statement=$this->dbHost->prepare($query);

		$statement->bindParam(':idlaporan',$idlaporan,PDO::PARAM_STR,35);

		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function NewInsentifSummary($idlaporan){
		$query = "INSERT INTO `intensif_summary`(
    	`is_id_laporan`, `is_id_trans`, `is_lvl_trainer`, `is_harikerja`, `is_jenisacc`, `is_peran`, `is_dawil`
		)
		WITH `sum_raw` AS (SELECT `laporan_harian`.`id` AS `id`,`laporan_harian`.`username`, `users`.`usr_level`,`peran`,`kegiatan`,`jns_hari_kerja`, `acara_harian`.`transaksi` AS `transaksi`,`new_transaksi`.`trans_jenis`,`trans_va`,`kec`,`kota`,`provinsi`,
		IFNULL(`ins_acc`.`ija_id`,0) AS `ija_id`, `ins_peran`.`ip_id` AS `ip_id`,
		`ins_hari`.`ihk_id` AS `ihk_id`,`ins_wil`.`itw_wil_kode` AS `itw_wil_kode`,
		`ins_wil`.`itw_id` AS `itw_id`
				FROM `laporan_harian` 
				INNER JOIN `absensi` ON `absensi`.`id`=`laporan_harian`.`id_absen`
				INNER JOIN `users` ON `users`.`username`=`laporan_harian`.`username`
				LEFT JOIN `perusahaan` ON `perusahaan`.`id`=`absensi`.`nama_perusahaan`
				LEFT JOIN `acara_harian` 
				ON `acara_harian`.`tanggal`=`absensi`.`tanggal_absen` AND `absensi`.`nama_perusahaan`=`acara_harian`.`id`
				LEFT JOIN `new_transaksi` ON `acara_harian`.`transaksi` = `new_transaksi`.`trans_id`
		        LEFT JOIN (
		        	SELECT `ija_id`,`ija_lvl_trainer`,`ija_jns_acc` FROM `intensif_jenis_acc`
		            WHERE `intensif_jenis_acc`.`ija_status`='1'
		        ) AS `ins_acc` 
		        ON `ins_acc`.`ija_lvl_trainer`= `users`.`usr_level`
		        AND `ins_acc`.`ija_jns_acc`=`new_transaksi`.`trans_va`
		        LEFT JOIN (
		        	SELECT `ip_id`,`ip_peran` FROM `intensif_peran`
		            WHERE `ip_status`='1'
		        ) AS `ins_peran`
		        ON `ins_peran`.`ip_peran`=`laporan_harian`.`peran`
		        LEFT JOIN (
		        	SELECT `ihk_id`,`ihk_jenis_hari` FROM `intensif_hari_kerja`
		            WHERE `intensif_hari_kerja`.`ihk_status`='1'
		        ) AS `ins_hari`
		        ON `ins_hari`.`ihk_jenis_hari`=`laporan_harian`.`jns_hari_kerja`
		        LEFT JOIN (
		        	SELECT `itw_id`,`itw_kecamatan`, `itw_kota`, `itw_provinsi`, `itw_wil_kode` 
		            FROM `intensif_tbl_wil`
		            WHERE `intensif_tbl_wil`.`itw_status`='1'
		        ) AS `ins_wil`
		        ON `ins_wil`.`itw_provinsi`=`perusahaan`.`provinsi` 
		        AND `ins_wil`.`itw_kota`=`perusahaan`.`kota` 
		        AND `ins_wil`.`itw_kecamatan`=`perusahaan`.`kec`
		        OR `ins_wil`.`itw_provinsi`=`perusahaan`.`provinsi` 
		        AND `ins_wil`.`itw_kota`=`perusahaan`.`kota` 
		        AND `ins_wil`.`itw_kecamatan`='88888888'
		        OR `ins_wil`.`itw_provinsi`=`perusahaan`.`provinsi` 
		        AND `ins_wil`.`itw_kota`='88888888'
		        AND `ins_wil`.`itw_kecamatan`='88888888'
				WHERE `laporan_harian`.`id`=:idlaporan
		)
		SELECT `sum_raw`.`id`,`sum_raw`.`transaksi`,`sum_raw`.`usr_level`,`sum_raw`.`ihk_id`,`sum_raw`.`ija_id`,`sum_raw`.`ip_id`,`sum_raw`.`itw_wil_kode` FROM `sum_raw`;";
		
		$statement=$this->dbHost->prepare($query);
		
		$statement->bindParam(':idlaporan',$idlaporan,PDO::PARAM_STR,35);
		
		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function GetTotalDawilInLaporan($idwilayah){
		$query = "SELECT COUNT(`trans_id`) AS `jumlah` FROM `intensif_tbl_wil`
		INNER JOIN `perusahaan`
		ON `perusahaan`.`provinsi`=`intensif_tbl_wil`.`itw_provinsi`
		AND (`intensif_tbl_wil`.`itw_kota`='88888888' OR `perusahaan`.`kota`=`intensif_tbl_wil`.`itw_kota`)
		AND (`intensif_tbl_wil`.`itw_kecamatan`='88888888' OR `perusahaan`.`kec`=`intensif_tbl_wil`.`itw_kecamatan`)
		INNER JOIN `new_transaksi` ON `new_transaksi`.`trans_id_usaha`=`perusahaan`.`id`
		INNER JOIN `intensif_summary` ON `intensif_summary`.`is_id_trans`=`new_transaksi`.`trans_id`
		WHERE `intensif_tbl_wil`.`itw_id`=:id";
		$statement=$this->dbHost->prepare($query);
		
		$statement->bindParam(':id',$idwilayah,PDO::PARAM_STR,35);
		$statement->execute();
		$results=$statement->fetch(PDO::FETCH_ASSOC);
		return $results['jumlah'];
	}

	public function TabelDaftarWilayah(){
		$query = "SELECT itw_id,ind_wilayah,
		IFNULL(daftar_prov.prov_nama,itw_provinsi) AS itw_provinsi,
		IFNULL(daftar_kota.kota_nama,itw_kota) AS itw_kota,
		IFNULL(daftar_kecamatan.kec_nama,itw_kecamatan) AS itw_kecamatan,ind_tariff
		FROM `intensif_tbl_wil` 
		INNER JOIN intensif_dawil ON intensif_dawil.ind_id=intensif_tbl_wil.itw_wil_kode
		LEFT JOIN daftar_prov ON daftar_prov.prov_id=intensif_tbl_wil.itw_provinsi
		LEFT JOIN daftar_kota ON daftar_kota.kota_id=intensif_tbl_wil.itw_kota
		LEFT JOIN daftar_kecamatan ON daftar_kecamatan.kec_id=intensif_tbl_wil.itw_kecamatan";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	}

	public function TabelInsentifJenisTransaksi(){
		$query="SELECT `ijt_jns_trans`, `jt_ket`, IF(`ijt_acc`='1','Ya','Tidak') AS `ijt_acc`,
		IF(`ijt_hari`='1','Ya','Tidak') AS `ijt_hari`, IF(`ijt_wilayah`='1','Ya','Tidak') AS `ijt_wilayah`, `ijt_status` 
		FROM `intensif_jns_trans`
		INNER JOIN `new_jenis_transaksi` ON `new_jenis_transaksi`.`jt_id`=`intensif_jns_trans`.`ijt_jns_trans`";
		$statement=$this->dbHost->prepare($query);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	}

	public function FungsiStatusKerjaDanPeran($tgl,$idusaha){
		$query = "SELECT `laporan_harian`.`id` AS `id`,`users`.`nama` AS `nama_staff`,`users`.`username` AS `username`,`absensi`.`nama_perusahaan` AS `namausaha`,
		`acara_harian`.`transaksi` AS `trans`,
		IFNULL(`laporan_harian`.`status_kerja`,'0') AS `statuskerja`,`laporan_harian`.`peran` AS `peran`, `perusahaan` 
		FROM `laporan_harian`
		LEFT JOIN `users` ON `users`.`username`=`laporan_harian`.`username`
		LEFT JOIN `absensi` ON `absensi`.`id`=`laporan_harian`.`id_absen`
		LEFT JOIN `acara_harian` 
		ON `acara_harian`.`tanggal`=`laporan_harian`.`tanggal` AND `absensi`.`nama_perusahaan`=`acara_harian`.`id`
		WHERE `laporan_harian`.`tanggal`=:tgl AND `acara_harian`.`transaksi` IS NOT NULL AND `absensi`.`nama_perusahaan`=:namausaha";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':tgl',$tgl,PDO::PARAM_STR,35);
		$statement->bindParam(':namausaha',$idusaha,PDO::PARAM_STR,35);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	}

	public function FetchDawil(){
		$query = "SELECT * 
		FROM `intensif_dawil`
		INNER JOIN `desc_tbl`
		ON `desc_tbl`.`dt_relate_tbl`='indawil' 
		AND `desc_tbl`.`dt_flag`=`intensif_dawil`.`ind_status`";
		$statement = $this->dbHost->prepare($query);
		// $statement->bindParam(':id',$this->transid,PDO::PARAM_STR,35);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	}

	public function GetInsentifLuarKota($prov,$kota,$kec){
		$query = "SELECT * FROM `intensif_tbl_wil` 
		WHERE 
			`itw_provinsi`=:prov AND `itw_kota`=:kota AND `itw_kecamatan`=:kec
		OR
			`itw_provinsi`=:prov1 AND `itw_kota`=:kota1 AND `itw_kecamatan`='88888888'
		OR
			`itw_provinsi`=:prov2 AND `itw_kota`='88888888' AND `itw_kecamatan`='88888888'";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':prov',$prov,PDO::PARAM_STR,35);
		$statement->bindParam(':prov1',$prov,PDO::PARAM_STR,35);
		$statement->bindParam(':prov2',$prov,PDO::PARAM_STR,35);
		$statement->bindParam(':kota',$kota,PDO::PARAM_STR,35);
		$statement->bindParam(':kota1',$kota,PDO::PARAM_STR,35);
		$statement->bindParam(':kec',$kec,PDO::PARAM_STR,35);

		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);	
	}

	public function FetchPeran(){
		$query = "SELECT * FROM `intensif_peran`
		INNER JOIN `desc_tbl` 
		ON `desc_tbl`.`dt_relate_tbl`='ip' 
		AND `desc_tbl`.`dt_flag`=`intensif_peran`.`ip_status`
		INNER JOIN `daftar_peran_kerja` ON `daftar_peran_kerja`.`pk_id` = `intensif_peran`.`ip_peran`";
		$statement = $this->dbHost->prepare($query);
		// $statement->bindParam(':id',$this->transid,PDO::PARAM_STR,35);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	}

	public function FetchHariKerja(){
		$query = "SELECT * 
		FROM `intensif_hari_kerja`
		INNER JOIN `desc_tbl` 
		ON `desc_tbl`.`dt_relate_tbl`='ihk' 
		AND `desc_tbl`.`dt_flag`=`intensif_hari_kerja`.`ihk_status`
		INNER JOIN `intensif_jns_hr_kerja`
		ON `intensif_jns_hr_kerja`.`int_hk_id`=`intensif_hari_kerja`.`ihk_jenis_hari`";
		$statement = $this->dbHost->prepare($query);
		// $statement->bindParam(':id',$this->transid,PDO::PARAM_STR,35);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	}

	public function CariInsentif() {
		$query = "SELECT `laporan_harian`.`username` AS `username`,`laporan_harian`.`status_kerja` AS `status_kerja`,IFNULL(`intensif_summary`.`is_id_laporan`,'-') AS `idsummary`,IFNULL(`is_id_trans`,'-') AS `transaksi`,
		IFNULL(`ip_tariff`,0) AS `tarif_peran`, IFNULL(`ihk_tariff`,0) AS `tarif_hari_kerja`, 
		IFNULL(`ija_tariff`,0) AS `tarif_accurate`, 
		IFNULL(`intensif_dawil`.`ind_id`,0) AS `id_dawil`, `ind_tariff` AS `tarif_wilayah`, 
		IFNULL(`ijt_acc`,0) AS `ijt_acc`, IFNULL(`ijt_hari`,0) AS `ijt_hari`, 
		IFNULL(`ijt_wilayah`,0) AS `ijt_wilayah`,IFNULL(is_ttl_attnd,1) AS is_ttl_attnd
		FROM `laporan_harian` 
        LEFT JOIN `intensif_summary`
        ON `laporan_harian`.`id`=`intensif_summary`.`is_id_laporan`
		LEFT JOIN `intensif_peran`
		ON `intensif_peran`.`ip_id`=`intensif_summary`.`is_peran`
		LEFT JOIN `intensif_jenis_acc`
		ON `intensif_jenis_acc`.`ija_id`=`intensif_summary`.`is_jenisacc`
		LEFT JOIN `intensif_hari_kerja`
		ON `intensif_hari_kerja`.`ihk_id`=`intensif_summary`.`is_harikerja`
		LEFT JOIN `intensif_dawil`
		ON `intensif_dawil`.`ind_id`=`intensif_summary`.`is_dawil`
		LEFT JOIN `new_transaksi`
		ON `new_transaksi`.`trans_id`=`intensif_summary`.`is_id_trans`
		LEFT JOIN `intensif_jns_trans` 
		ON `intensif_jns_trans`.`ijt_jns_trans`=`new_transaksi`.`trans_jenis` 
		AND `intensif_jns_trans`.`ijt_status`='1'
		WHERE `laporan_harian`.`id`=:idlaporan";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':idlaporan',$this->idlaporan,PDO::PARAM_STR,35);
		$statement->execute();

		return $statement->fetch(PDO::FETCH_OBJ);
	}
	
	public function FetchForJenisHariKerja($jnshari,$tanggal){
	    $query="SELECT ihk_id 
        FROM `intensif_hari_kerja`
        WHERE intensif_hari_kerja.ihk_jenis_hari=:jnshari
        AND (
	        (ihk_status='1' AND '".$tanggal."' >= intensif_hari_kerja.ihk_created_at)
            OR
            (ihk_status > '-1' AND '".$tanggal."' BETWEEN intensif_hari_kerja.ihk_created_at AND intensif_hari_kerja.ihk_valdate)
        )";
        $statement = $this->dbHost->prepare($query);
		$statement->bindParam(':jnshari',$jnshari,PDO::PARAM_STR,35);
		$statement->execute();

		return $statement->fetch(PDO::FETCH_ASSOC);
	}
	

    public function FetchForJenisAccurate($level,$acc,$tanggal){
        $query = "SELECT ija_id 
        FROM `intensif_jenis_acc`
        WHERE intensif_jenis_acc.ija_lvl_trainer=:lvltrainer
        AND intensif_jenis_acc.ija_jns_acc=:jnsacc
        AND (
            (intensif_jenis_acc.ija_status='1' 
             AND '".$tanggal."' >= intensif_jenis_acc.ija_updateat
            )
            OR (
                intensif_jenis_acc.ija_status>'-1' 
                AND '".$tanggal."' BETWEEN intensif_jenis_acc.ija_updateat 
                AND intensif_jenis_acc.ija_valdate
            )
        )";
        $statement = $this->dbHost->prepare($query);
		$statement->bindParam(':lvltrainer',$level,PDO::PARAM_STR,35);
		$statement->bindParam(':jnsacc',$acc,PDO::PARAM_STR,35);
		$statement->execute();

		return $statement->fetch(PDO::FETCH_ASSOC);
    }

	public function FetchForSummary(){
		$query = "SELECT `laporan_harian`.`username`,`acara_harian`.`tanggal` AS `tanggal`, `users`.`usr_level`,`status_kerja`,`peran`,`kegiatan`,`jns_hari_kerja`, `acara_harian`.`transaksi` AS `transaksi`,`new_transaksi`.`trans_jenis` AS `trans_jenis`,`trans_va`,`kec`,`kota`,`provinsi` 
		FROM `laporan_harian` 
		INNER JOIN `absensi` ON `absensi`.`id`=`laporan_harian`.`id_absen`
		INNER JOIN `users` ON `users`.`username`=`laporan_harian`.`username`
		LEFT JOIN `perusahaan` ON `perusahaan`.`id`=`absensi`.`nama_perusahaan`
		LEFT JOIN `acara_harian` 
		ON `acara_harian`.`tanggal`=`absensi`.`tanggal_absen` AND `absensi`.`nama_perusahaan`=`acara_harian`.`id`
		LEFT JOIN `new_transaksi` ON `acara_harian`.`transaksi` = `new_transaksi`.`trans_id`
		WHERE `laporan_harian`.`id`=:idlaporan";
		$statement = $this->dbHost->prepare($query);
		$statement->bindParam(':idlaporan',$this->idlaporan,PDO::PARAM_STR,35);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	}


	public function FetchJenisAccurate(){
		$query ="SELECT `ija_id`, `ija_jns_acc`,`va_nama`, `intr_level`,`ija_lvl_trainer`, `ija_tariff`, `ija_status`,`dt_desc` 
		FROM `intensif_jenis_acc` 
		LEFT JOIN `desc_tbl`
		ON `desc_tbl`.`dt_relate_tbl`='ija' 
		AND `desc_tbl`.`dt_flag`=`intensif_jenis_acc`.`ija_status`
		LEFT JOIN `versi_accurate` 
		ON `versi_accurate`.`va_id`=`intensif_jenis_acc`.`ija_jns_acc`
		LEFT JOIN `intensif_lvl_trainer` 
		ON `intensif_lvl_trainer`.`intr_status`='1' 
		AND `intensif_lvl_trainer`.`intr_id`=`intensif_jenis_acc`.`ija_lvl_trainer`;";
		$statement = $this->dbHost->prepare($query);
		// $statement->bindParam(':id',$this->transid,PDO::PARAM_STR,35);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		return $results;

	}
	

	public function FetchLevelTrainer(){
		$query ="SELECT * 
		FROM `intensif_lvl_trainer`
		INNER JOIN `desc_tbl` 
		ON `desc_tbl`.`dt_relate_tbl`='ilt' 
		AND `desc_tbl`.`dt_flag`=`intensif_lvl_trainer`.`intr_status`;";
		$statement = $this->dbHost->prepare($query);
		// $statement->bindParam(':id',$this->transid,PDO::PARAM_STR,35);
		$statement->execute();

		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	}

	public function getID(){
		return $this->transid;
	}

	public function setID($value){
		$this->transid = $value;
	}

	public function getCustomer(){
		return $this->custid;
	}

	public function setCustomer($value){
		$this->custid = $value;
	}

	public function getDP(){
		return $this->dp;
	}

	public function setDP($value){
		$this->dp = $value;
	}


	public function getPPN(){
		return $this->ppn;
	}

	public function setPPN($value){
		$this->ppn = $value;
	}

	public function getTempat(){
		return $this->utklokasi;
	}

	public function setTempat($value){
		$this->utklokasi = $value;
	}

	public function getWaktu(){
		return $this->utkwaktu;
	}

	public function setWaktu($value){
		$this->utkwaktu = $value;
	}


	public function getJenistransaksi(){
		return $this->jenistransaksi;
	}

	public function setJenistransaksi($value){
		$this->jenistransaksi = $value;
	}


	public function getDiscount(){
		return $this->discount;
	}

	public function setDiscount($value){
		$this->discount = $value;
	}

	public function getNotes(){
		return $this->notes;
	}

	public function setNotes($value){
		$this->notes = $value;
	}

	public function getStatus(){
		return $this->status;
	}

	public function setStatus($value){
		$this->status = $value;
	}

	public function getTanggal(){
		return $this->tanggal;
	}

	public function setTanggal($value){
		$this->tanggal = $value;
	}

	public function getAgenda(){
		return $this->agenda;
	}

	public function setAgenda($value){
		$this->agenda = $value;
	}

	public function getItems(){
		return $this->items;
	}

	public function setItems($value){
		$this->items = $value;
	}

	public function getPetugas(){
		return $this->petugas;
	}

	public function setPetugas($value){
		$this->petugas = $value;
	}

	public function getFolder(){
		return 'attachments/';
	}

	public function getData(){
		return $this->data;
	}

	public function setData($value){
		return $this->data = $value;
	}

    public function getUsahaid(){
        return $this->usahaid;
    }

    public function setUsahaid($usahaid){
        $this->usahaid = $usahaid;
    }

    public function getIdlaporan(){
        return $this->idlaporan;
    }

    public function setIdlaporan($idlaporan){
        $this->idlaporan = $idlaporan;
    }
}

?>