<?php 
include_once 'Koneksi.php';
class Invoice extends Koneksi{
	private $noinvoice;
	private $idtransaksi;
	private $metodebayar;
	private $alamat;
	private $deskripsi;
	private $status;
	private $tanggal;
	private $data;
	private $folder;



	//constructor
	function __construct(){
		$this->dbHost = $this->bukaKoneksi();
	}

	public function getDataPembayaranInvoice($idinv){
	    $query = "SELECT `lseqid`, `ssource`, `ipayamt`, `ipaymeth`, masdet.ket, DATE_FORMAT(`ddate`,'%d/%m/%Y') AS `ddate`, `istatus`, `dupdate_at`, users.nama AS pic 
        FROM `new_inv_payment` 
        LEFT JOIN users ON users.username=new_inv_payment.sstaff
        LEFT JOIN (
            SELECT `dt_flag` AS id, `dt_desc` AS ket FROM desc_tbl WHERE desc_tbl.dt_relate_tbl='new_invoice.inv_py_mtd'
        ) AS masdet ON masdet.id=new_inv_payment.ipaymeth
        WHERE new_inv_payment.ssource=:id AND new_inv_payment.istatus<>'0'";
        $statement=$this->dbHost->prepare($query);

        $statement->bindParam(':id',$idinv,PDO::PARAM_STR,35);
		$statement->execute();
		// return $query;
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}


	public function GetDataBulanLalu($bulan,$tahun){
		if ($bulan=='0') {
			$bulan = '12';
			$tahun = $tahun-1;
		}
		
		$data = array('bulan' => $bulan,'tahun'=> $tahun);

		return $data;
	}

	public function GetIDInvoice(){
		$query = "SELECT COUNT(`inv_no_invoice`) AS jumlah FROM `new_invoice`";

		$statement=$this->dbHost->prepare($query);

		$statement->execute();
		$hasil = $statement->fetch(PDO::FETCH_OBJ);

		return 'FAC/W'.date('y').'/'.date('m').'/'.$this->GetNumberInvoice($hasil->jumlah);
	}

	public function GetNumberInvoice($value){
		$value = intval($value) + 1;
		if ($value<10) {
			return '00000'.$value;
		}elseif ($value<100) {
			return '0000'.$value;
		}elseif ($value<1000) {
			return '000'.$value;
		}elseif ($value<10000) {
			return '00'.$value;
		}elseif ($value<100000) {
			return '0'.$value;
		}elseif ($value<1000000) {
			return $value;
		}
	}

	public function EditData(){
		$query = "UPDATE `new_invoice` SET `inv_metode_bayar`=:metodebayar,`inv_deskripsi`=:deskripsi,`inv_date`=:tgl,`inv_alamat`=:alamat  WHERE `inv_no_invoice`=:noinvoice";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':noinvoice',$this->noinvoice,PDO::PARAM_STR,15);
		@$statement->bindParam(':metodebayar',$this->metodebayar,PDO::PARAM_STR,1);
		@$statement->bindParam(':deskripsi',$this->deskripsi,PDO::PARAM_STR,255);
		@$statement->bindParam(':tgl',$this->tanggal,PDO::PARAM_STR,255);		
		@$statement->bindParam(':alamat',$this->alamat,PDO::PARAM_STR,800);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function ChangeStatus(){
		$query = "UPDATE `new_invoice` 
		SET `inv_status` = :status 
		WHERE `new_invoice`.`inv_no_invoice` = :noinvoice;";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':noinvoice',$this->noinvoice,PDO::PARAM_STR,15);
		@$statement->bindParam(':status',$this->status,PDO::PARAM_STR,1);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function FetchAllMethods(){
		$query = "SELECT `imb_id`, `imb_keterangan` FROM `invoice_metode_bayar`;";
		$statement=$this->dbHost->prepare($query);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function GetByID(){
		$query = "SELECT `inv_no_invoice`,`nama` AS `nama_usaha`,`inv_alamat`, `inv_id_trans`, `inv_metode_bayar`, `inv_deskripsi`, `inv_status`, `inv_date`, `inv_date_created` FROM `new_invoice`,`new_transaksi`,`new_customer`,`perusahaan` 
		WHERE `new_invoice`.`inv_id_trans`=`new_transaksi`.`trans_id`
		AND `new_transaksi`.`trans_cust_id`=`new_customer`.`id_cust`
		AND `new_customer`.`id_perusahaan`=`perusahaan`.`id` 
		AND `new_invoice`.`inv_no_invoice`=:noinvoice;";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':noinvoice',$this->noinvoice,PDO::PARAM_STR,35);
		
		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function CountInvoiceData(){
		$query = "SELECT COUNT(`inv_no_invoice`) AS `jumlah` FROM `new_invoice`";
		$statement=$this->dbHost->prepare($query);

		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_OBJ);
		return $result->jumlah;
	}

	public function GetByTransaction(){
		$query = "SELECT `inv_no_invoice`,`trans_jenis`, `inv_id_trans`, `inv_metode_bayar`, `inv_deskripsi`, `inv_status`, `inv_date_created` FROM `new_invoice`,`new_transaksi` WHERE `inv_id_trans`=:transaksi AND `new_invoice`.`inv_id_trans`=`new_transaksi`.`trans_id`;";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':transaksi',$this->idtransaksi,PDO::PARAM_STR,35);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

	public function CariUntung($maksud,$marketing,$bulan,$tahun){
		$query = "SELECT 
		SUM(harga*itm_qty - ROUND(harga*itm_qty*trans_ppn) - ROUND(harga*itm_qty*trans_discount) - `trans_dp`) AS `jumlah` 
		FROM `new_invoice` 
        INNER JOIN `new_transaksi` 
        ON `new_invoice`.`inv_id_trans`= `new_transaksi`.`trans_id`
        INNER JOIN `new_trans_items` 
        ON `new_trans_items`.`itm_trans_id` = `new_transaksi`.`trans_id`
        INNER JOIN `paket_training`
        ON `paket_training`.`id` = `new_trans_items`.`itm_paket`
        INNER JOIN `new_customer`
        ON `new_customer`.`id_cust`=`new_transaksi`.`trans_cust_id`
        INNER JOIN `new_marketing`
        ON `new_marketing`.`marketing_id`=`new_customer`.`marketing_id`
		WHERE MONTH(`new_invoice`.`inv_date`) = :bulan
		AND YEAR(`new_invoice`.`inv_date`)= :tahun
		AND `new_marketing`.`marketing_id`=:marketing";

		if ($maksud=='omset') {
			$query .= " AND `new_invoice`.`inv_status` > '0';";
		}elseif ($maksud=='net') {
			$query .= " AND `new_invoice`.`inv_status` = '2'";
			$query .= " OR MONTH(`new_invoice`.`inv_date`) = :bulan
		AND YEAR(`new_invoice`.`inv_date`)= :tahun
		AND `new_marketing`.`marketing_id`=:marketing AND `new_invoice`.`inv_status` = '3';";
		}

		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':marketing',$marketing,PDO::PARAM_STR,35);
		@$statement->bindParam(':bulan',$bulan,PDO::PARAM_STR,25);
		@$statement->bindParam(':tahun',$tahun,PDO::PARAM_STR,25);
		$statement->execute();

		$result = $statement->fetch(PDO::FETCH_OBJ);
		return $this->NumberValue($result->jumlah);


	}


	public function DataPenjualan($bulan,$tahun,$user,$tr,$maksud){
		if ($user=='3') {
			$query = "SELECT 
			SUM(harga*itm_qty - ROUND(harga*itm_qty*trans_ppn) - ROUND(harga*itm_qty*trans_discount) - `trans_dp`) AS `jumlah` 
			FROM `new_transaksi`,`new_trans_items`,`paket_training`,`new_invoice` 
			WHERE `new_transaksi`.`trans_id`=`new_trans_items`.`itm_trans_id` 
			AND `new_trans_items`.`itm_paket`=`paket_training`.`id`
			AND `new_invoice`.`inv_id_trans`=`new_transaksi`.`trans_id`
			AND `new_transaksi`.`trans_jenis` = '$tr'
			AND MONTH(`new_invoice`.`inv_date`) = :bulan
			AND YEAR(`new_invoice`.`inv_date`)=:tahun ";
		}

		if ($maksud=='omset') {
			$query .= "AND `new_invoice`.`inv_status` > '0';";
		}elseif ($maksud=='paid') {
			$query .= "AND `new_invoice`.`inv_status`= '3';";
		}

		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':bulan',$bulan,PDO::PARAM_STR,25);
		@$statement->bindParam(':tahun',$tahun,PDO::PARAM_STR,25);
		$statement->execute();

		$result = $statement->fetch(PDO::FETCH_OBJ);
		return $this->NumberValue($result->jumlah);
	}

	public function GetJumlahDanTerbayarByInvDate($menu){
		$query = "SELECT inv_no_invoice, trans_jenis, jumlah, terbayar, inv_status 
		FROM view_data_invoice";
		if ($menu=='lastmonth') {
			if (date('m')=='1') {
				$query.= " WHERE MONTH(view_data_invoice.inv_date) = MONTH(DATE_SUB(CURDATE(), INTERVAL 1 MONTH)) AND YEAR(view_data_invoice.inv_date)=YEAR(DATE_SUB(CURDATE(), INTERVAL 1 YEAR))";
			} else {
				$query.= " WHERE MONTH(view_data_invoice.inv_date) = MONTH(DATE_SUB(CURDATE(), INTERVAL 1 MONTH)) AND YEAR(view_data_invoice.inv_date)=YEAR(CURDATE())";
			}
			
		}else{
			$query .= "	WHERE MONTH(view_data_invoice.inv_date) = MONTH(CURDATE()) AND YEAR(view_data_invoice.inv_date) = YEAR(CURDATE())";
		}
		
		$statement=$this->dbHost->prepare($query);

		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function GetJumlahDanTerbayarByTglBayar($menu){
		$query = "SELECT inv_no_invoice, trans_jenis, jumlah, terbayar, inv_status 
		FROM view_data_invoice";

		if ($menu=='lastmonth') {
			if (date('m')=='1') {
				$query.= " WHERE MONTH(view_data_invoice.inv_date) = MONTH(DATE_SUB(CURDATE(), INTERVAL 1 MONTH)) AND YEAR(view_data_invoice.inv_date)=YEAR(DATE_SUB(CURDATE(), INTERVAL 1 YEAR))";
			}else{
				$query.= " WHERE MONTH(view_data_invoice.inv_date) = MONTH(DATE_SUB(CURDATE(), INTERVAL 1 MONTH)) AND YEAR(view_data_invoice.inv_date)=YEAR(CURDATE())";
			}
			
		}else{
			$query .= "	WHERE MONTH(view_data_invoice.inv_date) = MONTH(CURDATE()) AND YEAR(view_data_invoice.inv_date) = YEAR(CURDATE())";
		}

		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':bulan',$bulan,PDO::PARAM_STR,25);
		@$statement->bindParam(':tahun',$tahun,PDO::PARAM_STR,25);
		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function OmsetBulanan($bulan,$tahun){
		$query = "SELECT 
		SUM(harga*itm_qty - ROUND(harga*itm_qty*trans_ppn) - ROUND(harga*itm_qty*trans_discount) - `trans_dp`) AS `jumlah` 
		FROM `new_transaksi`,`new_trans_items`,`paket_training`,`new_invoice` 
		WHERE `new_transaksi`.`trans_id`=`new_trans_items`.`itm_trans_id` 
		AND `new_trans_items`.`itm_paket`=`paket_training`.`id`
		AND `new_invoice`.`inv_id_trans`=`new_transaksi`.`trans_id`
		AND MONTH(`new_invoice`.`inv_date`) = :bulan
		AND YEAR(`new_invoice`.`inv_date`)=:tahun
		AND `new_invoice`.`inv_status` > '0';";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':bulan',$bulan,PDO::PARAM_STR,25);
		@$statement->bindParam(':tahun',$tahun,PDO::PARAM_STR,25);
		$statement->execute();

		$result = $statement->fetch(PDO::FETCH_OBJ);
		return $this->NumberValue(@$result->jumlah);
	}

	public function OmsetTahunan($tahun){
		$query = "SELECT IFNULL(SUM(`jumlah`),0) AS `jumlah` 
		FROM `view_data_invoice`
		WHERE YEAR(`inv_date`)=:tahun
		AND `inv_status` > '0';";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':tahun',$tahun,PDO::PARAM_STR,25);
		$statement->execute();

		$result = $statement->fetch(PDO::FETCH_OBJ);
		return $this->NumberValue($result->jumlah);	
	}

	public function PaidInvoiceByMonth($bulan,$tahun){
		$query = "SELECT IFNULL(SUM(`jumlah`-`terbayar`),0) AS `jumlah` 
		FROM `view_data_invoice`
		WHERE MONTH(`tgl_bayar`)=:bulan
		AND YEAR(`tgl_bayar`)=:tahun
		AND `inv_status`='3';";
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':bulan',$bulan,PDO::PARAM_STR,25);
		@$statement->bindParam(':tahun',$tahun,PDO::PARAM_STR,25);
		$statement->execute();

		$result = $statement->fetch(PDO::FETCH_OBJ);
		return $this->NumberValue($result->jumlah);
	}

	public function PaidInvoiceByYear($tahun){
		$query = "SELECT IFNULL(SUM(`terbayar`),0) AS `jumlah` 
		FROM `view_data_invoice`
		WHERE YEAR(`inv_date`)=:tahun
		AND `inv_status`>'1';";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':tahun',$tahun,PDO::PARAM_STR,25);
		$statement->execute();

		$result = $statement->fetch(PDO::FETCH_OBJ);
		return $this->NumberValue($result->jumlah);	
	}

	public function SelectForPaymentMenu($idinv){
		$query = "SELECT `inv_no_invoice`, `inv_id_trans`, `inv_metode_bayar`, `inv_alamat`, `inv_deskripsi`, `inv_py_mtd`, `inv_owing`, `inv_status`, `inv_date`, `inv_date_paid`, `inv_date_created`,
		(
			SELECT 
				SUM(`harga`*`itm_qty` - ROUND(`harga`*`itm_qty`*`trans_ppn`) - ROUND(`harga`*`itm_qty`*`trans_discount`)) - `trans_dp` FROM `new_transaksi`,`new_trans_items`,`paket_training` WHERE `new_transaksi`.`trans_id`=`new_trans_items`.`itm_trans_id` AND `new_trans_items`.`itm_paket`=`paket_training`.`id` AND `new_transaksi`.`trans_id` = `inv_id_trans`
		) AS `jumlah`
		FROM `new_invoice` WHERE `inv_no_invoice` = :idinv";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':idinv',$idinv,PDO::PARAM_STR,25);
		$statement->execute();

		return $statement->fetch(PDO::FETCH_ASSOC);
	}

	public function ListTable($offset,$limit){
		$query = "SELECT * FROM `view_data_invoice` LIMIT $offset,$limit;";
		$statement=$this->dbHost->prepare($query);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}

    public function SearchInvoiceTerbayarByDateTerbayar($returntype=''){
        $namausaha = "%".$this->data->nama_usaha."%";
		$namacustomer = "%".$this->data->nama_cust."%";
		
        $query = "SELECT `inv_no_invoice`,`inv_id_trans`,`jt_ket`, `trans_jenis`, `nama_cust`,`perusahaan`.`nama` AS `nama_usaha`,`inv_alamat`,`inv_metode_bayar`,`inv_deskripsi`,`inv_py_mtd`, 
        `sipm_desc`, `inv_status`,`si_desc`,`inv_date_created`, `inv_date`,
        IFNULL((
			SELECT GROUP_CONCAT(DATE_FORMAT(new_inv_payment.ddate,'%d %b %Y') SEPARATOR ' # ') AS tanggal
			FROM `new_inv_payment`
			WHERE new_inv_payment.ssource=`inv_no_invoice` AND new_inv_payment.istatus > '0'
		),'-') AS `tgl_bayar`,`users`.`username` AS `username`, `users`.`nama` AS `nama_petugas`, new_inv_payment.ddate AS tanggal_bayar_bener,
    	new_inv_payment.ipayamt AS `terbayar`, cabangz.nama_cabang AS cabang,new_marketing.marketing_nama AS nama_marketing,
   	IFNULL((
        SELECT
        SUM(`harga`*`itm_qty` - ROUND(`harga`*`itm_qty`*`trans_ppn`) - ROUND(`harga`*`itm_qty`*`trans_discount`)) - `trans_dp`
         FROM `new_transaksi`
         INNER JOIN `new_trans_items`
         ON `new_transaksi`.`trans_id`=`new_trans_items`.`itm_trans_id`
         INNER JOIN `paket_training`
         ON `new_trans_items`.`itm_paket`=`paket_training`.`id`
         WHERE `new_transaksi`.`trans_id` = `inv_id_trans`
       ),'0') AS `jumlah`,
    	(SELECT GROUP_CONCAT(`nama_item` SEPARATOR '##')
     	FROM `paket_training`
     	INNER JOIN `new_trans_items` ON `paket_training`.`id`=`new_trans_items`.`itm_paket`
     	WHERE `new_trans_items`.itm_trans_id=`inv_id_trans`) AS `items`
    	FROM `new_invoice`
        INNER JOIN new_inv_payment ON new_inv_payment.ssource=new_invoice.inv_no_invoice AND new_inv_payment.istatus > '0'
    	INNER JOIN `new_transaksi` ON `new_transaksi`.`trans_id`=`new_invoice`.`inv_id_trans`
    	INNER JOIN `new_jenis_transaksi`
    	ON `new_jenis_transaksi`.`jt_id`=`new_transaksi`.`trans_jenis`
    	INNER JOIN `new_customer` ON `new_customer`.`id_cust`=`new_transaksi`.`trans_cust_id`
    	INNER JOIN `perusahaan` ON `perusahaan`.`id`=`new_transaksi`.`trans_id_usaha`
    	INNER JOIN `status_inv_py_mtd` ON
    	`status_inv_py_mtd`.`sipm_id`=`new_invoice`.`inv_py_mtd`
    	INNER JOIN `status_invoice` ON `status_invoice`.`si_id`=`new_invoice`.`inv_status`
    	INNER JOIN `users` ON `users`.`username`=`new_transaksi`.`trans_petugas`
    	INNER JOIN `new_marketing`
    	ON `new_marketing`.`marketing_id`=`new_customer`.`marketing_id`
    	LEFT JOIN (
    	    SELECT id_cabang AS id,perusahaan.nama AS nama_cabang 
            FROM `cabang`
            INNER JOIN perusahaan ON perusahaan.id=cabang.id_perusahaan
    	) `cabangz` ON cabangz.id=`new_marketing`.`marketing_cabang`  
    	WHERE `nama_cust` LIKE :namacustomer AND perusahaan.nama LIKE :namausaha ";
    	
    	if ($this->data->tipe_transaksi!='') {
			$query.=" AND `trans_jenis`='".$this->data->tipe_transaksi."'";
		}
		
		if ($this->data->tanggal_awal!=''&&$this->data->tanggal_akhir) {
		    $query .= " AND new_inv_payment.ddate BETWEEN '".$this->data->tanggal_awal."' AND '".$this->data->tanggal_akhir."'";
		}    
		
		if ($this->data->status!='') {
			$query .= " AND `inv_status`='".$this->data->status."'";
		}

		if ($this->data->jumlah!='0') {
			$query .= " AND `jumlah` = '".$this->data->jumlah."'";
		}

		if ($this->data->sales!='') {
			$query .= " AND `username`='".$this->data->sales."'";
		}
		
		$query .= ";";
// 		echo $query;
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':namacustomer',$namacustomer,PDO::PARAM_STR,75);
		@$statement->bindParam(':namausaha',$namausaha,PDO::PARAM_STR,75);
		// @$statement->bindParam(':jenistransaksi',$this->data->tipe_transaksi,PDO::PARAM_STR,75);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		if ($returntype=='') {
			$dataz=json_encode($results);
			return json_decode($dataz);	
		}else{
			return $results;
		}
    }

	public function SearchInvoice($returntype=''){
		$namausaha = "%".$this->data->nama_usaha."%";
		$namacustomer = "%".$this->data->nama_cust."%";

		$query = "SELECT * FROM `view_data_invoice` 
		WHERE `nama_cust` LIKE :namacustomer
		AND `nama_usaha` LIKE :namausaha";

		if ($this->data->tipe_transaksi!='') {
			$query.=" AND `trans_jenis`='".$this->data->tipe_transaksi."'";
		}

		if ($this->data->tglbyr=='y') {
			if ($this->data->tanggal_awal!=''&&$this->data->tanggal_akhir) {
        	    $query = "SELECT view_data_invoice.* 
                FROM `new_inv_payment` 
                INNER JOIN view_data_invoice ON view_data_invoice.inv_no_invoice=new_inv_payment.ssource 
        		WHERE `nama_cust` LIKE :namacustomer
        		AND `nama_usaha` LIKE :namausaha";
        			if ($this->data->tipe_transaksi!='') {
        			    $query.=" AND `trans_jenis`='".$this->data->tipe_transaksi."'";
        		    }    
				$query .= " AND new_inv_payment.ddate BETWEEN '".$this->data->tanggal_awal."' AND '".$this->data->tanggal_akhir."'";
			}
		} else {
			if ($this->data->tanggal_awal!=''&&$this->data->tanggal_akhir) {
				$query .= " AND `inv_date` BETWEEN '".$this->data->tanggal_awal."' AND '".$this->data->tanggal_akhir."'";
			}
		}
			

		if ($this->data->status!='') {
			$query .= " AND `inv_status`='".$this->data->status."'";
		}

		if ($this->data->jumlah!='0') {
			$query .= " AND `jumlah` = '".$this->data->jumlah."'";
		}

		if ($this->data->sales!='') {
			$query .= " AND `username`='".$this->data->sales."'";
		}

		$query .= ";";
		// echo $query;
		$statement=$this->dbHost->prepare($query);
		@$statement->bindParam(':namacustomer',$namacustomer,PDO::PARAM_STR,75);
		@$statement->bindParam(':namausaha',$namausaha,PDO::PARAM_STR,75);
		// @$statement->bindParam(':jenistransaksi',$this->data->tipe_transaksi,PDO::PARAM_STR,75);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		if ($returntype=='') {
			$dataz=json_encode($results);
			return json_decode($dataz);	
		}else{
			return $results;
		}
		
	}

	public function PendingInvoice(){

		$query = "SELECT `inv_no_invoice`,`inv_id_trans`, `trans_jenis`, `nama_cust`,`nama` AS `nama_usaha`,`inv_deskripsi`, `inv_status`,`inv_date_created`,`inv_date`,
		(SELECT 
			SUM(harga*itm_qty - ROUND(harga*itm_qty*trans_ppn) - ROUND(harga*itm_qty*trans_discount) - `trans_dp`) FROM `new_transaksi`,new_trans_items,paket_training WHERE new_transaksi.trans_id=new_trans_items.itm_trans_id AND new_trans_items.itm_paket=paket_training.id AND new_transaksi.trans_id = `inv_id_trans`) AS jumlah,
		(SELECT GROUP_CONCAT(nama_item SEPARATOR '##') FROM paket_training,new_trans_items WHERE paket_training.id=new_trans_items.itm_paket AND new_trans_items.itm_trans_id=`inv_id_trans`) AS items
		FROM `new_invoice`,new_customer,perusahaan,new_transaksi 
		WHERE new_invoice.inv_id_trans=new_transaksi.trans_id 
		AND new_transaksi.trans_cust_id = new_customer.id_cust 
		AND new_customer.id_perusahaan = perusahaan.id AND `new_invoice`.`inv_status`='0';";

		$statement=$this->dbHost->prepare($query);

		$statement->execute();
		$results=$statement->fetchAll(PDO::FETCH_ASSOC);
		$dataz=json_encode($results);
		return json_decode($dataz);
	}


	public function InputNewData(){

		$query = "INSERT INTO `new_invoice` (`inv_no_invoice`, `inv_id_trans`, `inv_metode_bayar`, `inv_deskripsi`, `inv_status`, `inv_date_created`,`inv_date`,`inv_alamat`) 
		VALUES (:noinvoice, :idtransaksi, :metodebayar, :deskripsi, '0', CURRENT_TIMESTAMP,:tanggal,:alamat);";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':noinvoice',$this->GetIDInvoice(),PDO::PARAM_STR,35);
		@$statement->bindParam(':idtransaksi',$this->idtransaksi,PDO::PARAM_STR,255);
		@$statement->bindParam(':metodebayar',$this->metodebayar,PDO::PARAM_STR,1);
		@$statement->bindParam(':deskripsi',$this->deskripsi,PDO::PARAM_STR,255);
		@$statement->bindParam(':tanggal',$this->tanggal,PDO::PARAM_STR,255);
		@$statement->bindParam(':alamat',$this->alamat,PDO::PARAM_STR,800);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	// return no_invoice
	public function InputNewDataReturnInvNumber($statusinv){

		$query = "INSERT INTO `new_invoice` (`inv_no_invoice`, `inv_id_trans`, `inv_metode_bayar`, `inv_deskripsi`, `inv_status`, `inv_date_created`,`inv_date`,`inv_alamat`) 
		VALUES (:noinvoice, :idtransaksi, :metodebayar, :deskripsi, '".$statusinv."', CURRENT_TIMESTAMP,:tanggal,:alamat);";
		$statement=$this->dbHost->prepare($query);
		$id = $this->GetIDInvoice();
		@$statement->bindParam(':noinvoice',$id,PDO::PARAM_STR,35);
		@$statement->bindParam(':idtransaksi',$this->idtransaksi,PDO::PARAM_STR,255);
		@$statement->bindParam(':metodebayar',$this->metodebayar,PDO::PARAM_STR,1);
		@$statement->bindParam(':deskripsi',$this->deskripsi,PDO::PARAM_STR,255);
		@$statement->bindParam(':tanggal',$this->tanggal,PDO::PARAM_STR,255);
		@$statement->bindParam(':alamat',$this->alamat,PDO::PARAM_STR,800);

		if ($statement->execute()) {
			return $id;
		}else{
			return '';
		}
	}


	public function InputSurat(){
		$query = "INSERT INTO `new_mail_data` (`emd_source`, `emd_to`, `emd_cc`, `emd_attachments`) 
		VALUES (:noinvoice, :to, :cc, :attachments);";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':noinvoice',$this->noinvoice,PDO::PARAM_STR,45);
		@$statement->bindParam(':to',$this->data->to,PDO::PARAM_STR,125);
		@$statement->bindParam(':cc',$this->data->cc,PDO::PARAM_STR,300);
		@$statement->bindParam(':attachments',$this->data->attachments,PDO::PARAM_STR,500);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}
	}

	public function EditSurat(){
		$query = "UPDATE `new_mail_data` 
		SET `emd_to`=:to,`emd_cc`=:cc,`emd_attachments`=:attachments 
		WHERE `emd_source`=:noinvoice;";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':noinvoice',$this->noinvoice,PDO::PARAM_STR,45);
		@$statement->bindParam(':to',$this->data->to,PDO::PARAM_STR,125);
		@$statement->bindParam(':cc',$this->data->cc,PDO::PARAM_STR,300);
		@$statement->bindParam(':attachments',$this->data->attachments,PDO::PARAM_STR,500);

		if ($statement->execute()) {
			return true;
		}else{
			return false;
		}

	}

	public function CountAttachment(){
		$query = "SELECT COUNT(`emd_source`) AS `jumlah` FROM `new_mail_data` WHERE `emd_source`=:noinvoice";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':noinvoice',$this->noinvoice,PDO::PARAM_STR,35);
		
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_OBJ);
		return $result->jumlah;
	}

	public function FetchDataSurat(){
		$query = "SELECT `emd_to`, `inv_id_trans`,`emd_cc`, `emd_attachments` 
		FROM `new_mail_data` 
		INNER JOIN `new_invoice` 
		ON `new_invoice`.`inv_no_invoice`= `new_mail_data`.`emd_source`
		WHERE `emd_source`= :noinvoice;";
		$statement=$this->dbHost->prepare($query);

		@$statement->bindParam(':noinvoice',$this->noinvoice,PDO::PARAM_STR,35);
		
		$statement->execute();
		return $statement->fetch(PDO::FETCH_OBJ);

	}

	public function StringValue($value){
		if ($value=='') {
			return '';
		} else {
			return $value;
		}	
	}	

	function NumberValue($value){
		if ($value=='') {
			return '0';
		} else {
			return $value;
		}
		
	}

	public function TentukanMetodeBayar($value){
		if ($value=='1') {
			return 'Bayar dimuka';
		}elseif ($value=='2') {
			return 'Termin 1';
		}elseif ($value=='3') {
			return 'Termin 2';
		}
	}

	public function CekLampiran($value){
		if ($value=='') {
			return '';
		} else {
			$result = implode('##', $value);
			return $result;
		}
		
	}

	public function CekDataLampiran($value){
		if ($value=='') {
			return '';
		} else {
			$result = explode("##", $value);
			return $result;
		}
	}


	public function getNo(){
		return $this->noinvoice;
	}

	public function setNo($value){
		return $this->noinvoice = $value;
	}

	public function getIdTransaksi(){
		return $this->idtransaksi;
	}

	public function setIdTransaksi($value){
		return $this->idtransaksi = $value;
	}

	public function getMetodebayar(){
		return $this->metodebayar;
	}

	public function setMetodebayar($value){
		return $this->metodebayar = $value;
	}

	public function getAlamat(){
		return $this->alamat;
	}

	public function setAlamat($value){
		return $this->alamat = $value;
	}


	public function getDeskripsi(){
		return $this->deskripsi;
	}

	public function setDeskripsi($value){
		return $this->deskripsi = $value;
	}

	public function getStatus(){
		return $this->status;
	}

	public function setStatus($value){
		return $this->status = $value;
	}

	public function getTanggal(){
		return $this->tanggal;
	}

	public function setTanggal($value){
		return $this->tanggal = $value;
	}

	public function getData(){
		return $this->data;
	}

	public function setData($value){
		return $this->data = $value;
	}

	public function getFolder(){
		return 'invoices/';
	}


}

?>