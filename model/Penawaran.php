<?php 
date_default_timezone_set("Asia/Jakarta"); 
include_once 'Koneksi.php';
class Penawaran extends Koneksi{
	
	function __construct(){
		$this->dbHost = $this->bukaKoneksi();
	}

	public function getNomorPenawaran(){
		$query="SELECT isi_data FROM other_datas WHERE nama_data='invoice_number'";
		$statement = $this->dbHost->prepare($query);
		$statement->execute();
		return $result = $statement->fetch(PDO::FETCH_OBJ);
	}


	public function editNomorPenawaran($nomor){
		$query = "UPDATE `other_datas` SET `isi_data`=:nomor WHERE `nama_data`='invoice_number'";
		$statement=$this->dbHost->prepare($query);
		$statement->bindParam(':nomor',$nomor, PDO::PARAM_STR,15);

		if ($statement->execute()) {
			return "<script>alert('No Penawaran berhasil di ubah!')</script>";
		}else{
			return "<script>alert('No Penawaran gagal di ubah!')</script>";
		}
	}

}
?>