<?php 
class MyXML{
	private $namafile;
	private $dir;

	public function getDir(){
		return $this->dir;
	}

	public function setDir($value){
		$this->dir = $value;
	}	

	public function getNamafile(){
		return $this->namafile;
	}

	public function setNamafile($value){
		$this->namafile = $value;
	}

	public function SitemapAddArtikel($id){
		$dom = new DOMDocument();
		$dom->load($this->dir.$this->namafile);

		// Apply some modification
		$specificNode = $dom->getElementsByTagName('urlset')->item(0);
		$myurl = $dom->createElement('url');
		$myurl->appendChild(
			$dom->createElement('loc','https://fac-institute.com/artikel/?app=1&amp;id='.$id) //test disini!! as text 	inside the tag
		);

		$urlAttribute = $dom->createAttribute('name');
		$urlAttribute->value=$id;
		$myurl->appendChild($urlAttribute);
		$specificNode->appendChild($myurl);


		$dom->save($this->dir.$this->namafile);
		return true;
	}

	public function SitemapAddTutorial($id){
		$dom = new DOMDocument();
		$dom->load($this->dir.$this->namafile);

		// Apply some modification
		$specificNode = $dom->getElementsByTagName('urlset')->item(0);
		$myurl = $dom->createElement('url');
		$myurl->appendChild(
			$dom->createElement('loc','https://fac-institute.com/tutorial/?app=1&amp;id='.$id) //test disini!! as text 	inside the tag
		);

		$urlAttribute = $dom->createAttribute('idtut');
		$urlAttribute->value=$id;
		$myurl->appendChild($urlAttribute);
		$specificNode->appendChild($myurl);


		$dom->save($this->dir.$this->namafile);
		return true;
	}
}


?>