<?php 

class File{
	private $namafile;
	private $folder;
	private $ukuran;
	private $ekstensi;
	private $temp;

	public function getNamafile(){
	    return $this->namafile;
	}

	public function setNamafile($namafile){
	    $this->namafile = $namafile;
	}

	public function getFolder(){
	    return $this->folder;
	}
	 
	public function setFolder($folder){
	    $this->folder = $folder;
	}
	public function getUkuran(){
	     return $this->ukuran;
	}

	public function setUkuran($ukuran){
	     $this->ukuran = $ukuran;
	}

	public function getEkstensi(){
	    return $this->ekstensi;
	}

	public function setEkstensi($ekstensi){
	    $this->ekstensi = $ekstensi;
	}

	public function getTemp(){
	    return $this->temp;
	}

	public function setTemp($temp){
	    $this->temp = $temp;
	}

	public function uploadFile(){
		if(move_uploaded_file($this->getTemp(), $this->getFolder().$this->getNamafile())){
		    chmod($this->getFolder().$this->getNamafile(),0644);
			return "<script>alert('File berhasil di upload!')</script>";
		}else{
			return "<script>alert('File gagal di upload!')</script>";
		}
	}

	public function uploadExternal($type){
		$defaultFolder = "/home/facinsti/public_html/external/attachments/".$type;
		if(move_uploaded_file($this->getTemp(), $defaultFolder.$this->getNamafile())){
			return "<script>alert('File berhasil di upload!')</script>";
		}else{
			return "<script>alert('File gagal di upload!')</script>";
		}
	}
}



?>