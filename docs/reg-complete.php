<?php 
include_once '../fpdf/fpdf.php';
$pdf = new FPDF();
$pdf->AddPage('l','A4',0);
$pdf->SetAuthor('Rifzky Alam - Software Dev');
$pdf->SetAutoPageBreak(false,1);



$pdf->Image('../images/logo-fac.jpg',25,10,50,30);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(65);
$pdf->Cell(130, 10, 'Kursus Akuntansi First Asian Consulting Institute', 0, 0, "C");

//line
$pdf->Line(30,40,270,40);
$pdf->SetLineWidth(1);
$pdf->Line(30,41,270,41);


//footer line

$pdf->SetLineWidth(1);
$pdf->Line(30,150,270,150);



$pdf->Ln();
$pdf->SetFont('Arial','',12);
$pdf->Cell(65);
$pdf->Cell(130, 5, 'Jl Raya Jatiwaringin No 8 Pangkalan Jati Jatiwaringin Jakarta Timur', 0, 0, "L");
$pdf->Cell(10);
$pdf->SetFont('Arial','B',24);
$pdf->Cell(60, 5, 'Reg. Kursus', 0, 0, "C");
$pdf->Ln();

$pdf->SetFont('Arial','',12);
$pdf->Cell(65);
$pdf->Cell(15, 5, 'Email', 0, 0, "L");
$pdf->Cell(5);
$pdf->Cell(3, 5, ':', 0, 0, "L");
$pdf->Cell(30, 5, 'finance@fac-institute.com,cs.facinstitute@gmail.com', 0, 0, "L");

$pdf->Ln();
$pdf->Cell(65);
$pdf->Cell(15, 5, 'Website', 0, 0, "L");
$pdf->Cell(5);
$pdf->Cell(3, 5, ':', 0, 0, "L");
$pdf->Cell(30, 5, 'http://www.fac-institute.com', 0, 0, "L");
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
if (isset($_GET['nr'])&&!empty($_GET['nr'])) {
	include_once '../model/Kursus.php';
	$kursus = new Kursus();
	$data = $kursus->getDataRegister($_GET['nr']);
	$data = json_decode($data);
	$pdf->SetFont('Arial','',22);
	$pdf->Cell(20);
	$pdf->Cell(240,10,'Terima Kasih Telah Mendaftar',0,0,'C');
	$pdf->SetFont('Arial','',14);
	$pdf->Ln();
	$pdf->Ln();
	$pdf->Ln();
	$pdf->Cell(20);
	$pdf->Cell(35,10,'Nama',0,0,'L');
	$pdf->Cell(5);
	$pdf->Cell(2,10,':',0,0,'L');
	$pdf->Cell(1);
	if (@$data[0]->nama!='') {
		$pdf->Cell(120,10,@$data[0]->nama,0,0,'L');	
	}else{
		$pdf->Cell(120,10,'Data tidak ada',0,0,'L');
	}
	$pdf->Ln();
	$pdf->Ln();
	$pdf->Cell(20);
	$pdf->Cell(35,10,'Kelas Kursus',0,0,'L');
	$pdf->Cell(5);
	$pdf->Cell(2,10,':',0,0,'L');
	$pdf->Cell(1);
	if (@$data[0]->nama_kursus!='') {
		$pdf->Cell(120,10,@$data[0]->nama_kursus,0,0,'L');
	}else{
		$pdf->Cell(120,10,'Data tidak ada',0,0,'L');
	}
	$pdf->Ln();
	$pdf->Ln();

	$pdf->Cell(20);
	$pdf->Cell(35,10,'Tanggal Kursus',0,0,'L');
	$pdf->Cell(5);
	$pdf->Cell(2,10,':',0,0,'L');
	$pdf->Cell(1);
	if (@$data[0]->nama_kursus!='') {
		$pdf->Cell(120,10,@$data[0]->mulai_kursus,0,0,'L');
	}else{
		$pdf->Cell(120,10,'Data tidak ada',0,0,'L');
	}
	$pdf->Ln();
	$pdf->Ln();
	



	$pdf->Cell(20);
	$pdf->Cell(35,10,'Kode',0,0,'L');
	$pdf->Cell(5);
	$pdf->Cell(2,10,':',0,0,'L');
	$pdf->Cell(1);
	if (@$data[0]->id!='') {
		$pdf->Cell(120,10,'#'.@$data[0]->status,0,0,'L');	
	}else{
		$pdf->Cell(120,10,'Data tidak ada',0,0,'L');
	}
	
	$pdf->Ln();
	$pdf->Ln();
	$pdf->Cell(20);
	$pdf->SetFont('Arial','B',14);
	$pdf->Cell(40,10,'Note');

	$pdf->SetFont('Arial','',12);
	$pdf->Ln();
	$pdf->Cell(20);
	$pdf->Cell(40,5,'- Pembayaran melalui Bank BCA KCU Bekasi No. Acc. 0663162851 a/n : Fajar Shodiq.');
	$pdf->Ln();
	$pdf->Cell(20);
	$pdf->Cell(40,5,'- Pembayaran melalui Bank Mandiri KCP Bekasi Ahmad Yani No. Acc. 1670001377901 a/n : Fajar Shodiq.');
	$pdf->Ln();
	$pdf->Cell(20);
	$pdf->Cell(40,5,'- Bukti pembayaran dan bukti registrasi ini harap di bawa minimal 1 jam sebelum kelas dimulai.');
}else{
	$pdf->SetFont('Arial','',22);
	$pdf->Cell(20);
	$pdf->Cell(240,10,'Data Pendaftar Kursus Tidak Ada',0,0,'C');
	$pdf->SetFont('Arial','',14);
}



$pdf->Output();

?>