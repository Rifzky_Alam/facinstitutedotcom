<?php 
session_start();
if (!isset($_SESSION['admin'])) {
	header('Location: https://fac-institute.com');
}
include_once '../model/Mail.php';
include_once '../fpdf/fpdf.php';
include_once '../model/Surat.php';
include_once '../model/Pendaftar.php';




$pendaftar = new Pendaftar();
#$surat = new Surat();
#$mail = new FACMail();

if (isset($_GET['id'])&&isset($_GET['no'])&&!empty($_GET['no'])) {
	$el_data=$pendaftar->fetchDataRequestByID($_GET['id']);
	$dataPerusahaan = json_decode($el_data->nama_perusahaan);
	$dataJadwal = json_decode($el_data->data_jadwal);
	$dataInvoice = json_decode($el_data->temp_invoice);
	$paketTraining = json_decode($pendaftar->getPaketTraining($dataInvoice->paket));

	$objData = $pendaftar->getInvoiceByIDandStatus($_GET['no']);

	if ($objData->metode_bayar=='cash') {
		$myMethodPayment='Bayar di Muka';
	}elseif ($objData->metode_bayar=='term-1') {
		$myMethodPayment='Termin 1';
	}elseif ($objData->metode_bayar=='term-2') {
		$myMethodPayment='Termin 2';
	}else{
		$myMethodPayment=$objData->metode_bayar;
	}

	$datapdf = array(
		'no_invoice' => $objData->no_invoice, 
		'item' => $objData->nama_item, 
		'hargaitem' => $objData->harga, 
		'qty' => $dataInvoice->x, 
		'transport' => $dataInvoice->transport, 
		'jmlhari' => $dataInvoice->jml_hari, 
		'namausaha' => $dataPerusahaan->nama, 
		'alamat' => $dataPerusahaan->alamat, 
		'up' => $el_data->nama_personal, 
		'telepon' => $el_data->telepon, 
		'deskripsi' => $objData->deskripsi, 
		'metode' => $myMethodPayment,
		'dp' => $objData->dp,
		'diskon' => $objData->diskon
	);

	$datapdf = (object) $datapdf;

}elseif (isset($_GET['inv'])&&!empty($_GET['inv'])){
	$datas=$pendaftar->jadwalWithInvoicePreview($_GET['inv']);
	if ($datas->metode_bayar=='cash') {
		$myMethodPayment='Bayar di Muka';
	}elseif ($datas->metode_bayar=='term-1') {
		$myMethodPayment='Termin 1';
	}elseif ($datas->metode_bayar=='term-2') {
		$myMethodPayment='Termin 2';
	}else{
		$myMethodPayment=$objData->metode_bayar;
	}

	$datapdf = array(
		'no_invoice' => $datas->no_invoice, 
		'item' => $datas->nama_item, 
		'hargaitem' => $datas->harga, 
		'qty' => $datas->qty, 
		'transport' => $datas->biaya_trans, 
		'jmlhari' => $datas->qty_trans, 
		'namausaha' => $datas->nama, 
		'alamat' => $datas->alamat, 
		'up' => $datas->nama_personal, 
		'telepon' => $datas->telepon, 
		'deskripsi' => $datas->deskripsi, 
		'metode' => $myMethodPayment,
		'dp' => $datas->dp,
		'diskon' => $datas->diskon,
		'time_sent'=>$datas->sent_date
	);

	$datapdf = (object) $datapdf;
}



$pdf = new FPDF();
$totalHarga = 0;
$tglKirim = explode(' ', $datapdf->time_sent);
$sekarang = explode('-', $tglKirim[0]);

$pdf->AddPage('p','A4',0);
$pdf->SetAuthor('Rifzky Alam - Software Dev');
$pdf->SetAutoPageBreak(false,1);

//image FAC Institute
$pdf->Image('../images/logo-fac.jpg',10,10,50,30);

$pdf->setXY(160,10);
$pdf->SetFont('Arial','B',12);

if (@$datapdf->no_invoice!='') {
	$pdf->Cell(30,5,$datapdf->no_invoice);	
}else{
	$pdf->Cell(30,5,'');
}


$pdf->setXY(160,30);
$pdf->SetFont('Arial','B',24);
$pdf->Cell(30,5,'INVOICE');


$pdf->SetFont('Arial','',13);
$pdf->SetTextColor(255,255,255);
$pdf->SetFillColor(33,150,243);
$pdf->Rect(10,40,190,10,'F');
//pembayaran
$pdf->Rect(10,52,190,62,'D');
//info transaksi
$pdf->Rect(10,118,190,35,'D');
//rincian pembayaran
$pdf->Rect(10,157,190,45,'D');


$pdf->setXY(15,44);
$pdf->Cell(45,3,'DETAIL PEMBAYARAN',0,0,'L');
$pdf->setXY(165,44);
$pdf->SetFont('Arial','',9);
$pdf->Cell(30,3,$sekarang[2].' '. getNamaBulan($sekarang[1]),0,0,'L');


pembayaran($pdf,$totalHarga,$datapdf);


perusahaan($pdf,60,$datapdf);


transaksi($pdf,122,$datapdf);

rincian($pdf,160,$totalHarga,$datapdf);
footer($pdf,280);

$pdf->Output();






function rincian($pdf,$yValue,$totalHarga,$data){
	$yValue = intval($yValue);
	$pdf->SetFont('Arial','B',10);
	$pdf->setXY(15,$yValue);
	$pdf->Cell(30,5,'Rincian Pembayaran',0,0,'L');
	$pdf->setXY(15,$yValue + 5.5);
	$pdf->SetFont('Arial','',10);
	$pdf->Cell(30,5,'Jumlah Pembayaran',0,0,'L');
	$pdf->Cell(25);
	$pdf->Cell(2,5,':',0,0,'L');
	$pdf->Cell(85);
	$pdf->Cell(30,5,number_format($totalHarga).' IDR',0,0,'R');
	$pdf->setXY(15,$yValue + 10.5);
	$pdf->Cell(30,5,'DP',0,0,'L');
	$pdf->Cell(25);
	$pdf->Cell(2,5,':',0,0,'L');
	$pdf->Cell(85);
	$dp = 2500000;
	$pdf->Cell(30,5,'- '.number_format($data->dp).' IDR',0,0,'R');
	$pdf->setXY(15,$yValue + 15.5);

	if (intval($data->diskon)!=0) {
		$pdf->Cell(30,5,'Potongan Harga',0,0,'L');
		$pdf->Cell(25);
		$pdf->Cell(2,5,':',0,0,'L');
		$pdf->Cell(85);
		$diskon = $totalHarga * ($data->diskon/100);
		$pdf->Cell(30,5,'- '.number_format($diskon).' IDR',0,0,'R');
	}
	$pdf->setXY(15,$yValue + 30.5);
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(30,5,'Total Pembayaran',0,0,'L');
	$pdf->Cell(25);
	$pdf->Cell(2,5,':',0,0,'L');
	$pdf->Cell(85);
	if (intval($data->diskon)!=0) {
		$pdf->Cell(30,5,number_format($totalHarga-intval($data->dp)-intval($diskon)).' IDR',0,0,'R');
	}else{
		$pdf->Cell(30,5,number_format($totalHarga-intval($data->dp)).' IDR',0,0,'R');
	}
	
}

function transaksi($pdf,$y,$data){

	$pdf->SetFont('Arial','B',10);
	$pdf->setXY(15,$y);
	$pdf->Cell(30,5,'Deskripsi',0,0,'L');
	$pdf->Cell(5);
	$pdf->Cell(2,5,':',0,0,'L');
	$pdf->Cell(5);
	$pdf->SetFont('Arial','',9);
	$pdf->Cell(30,5,$data->deskripsi,0,0,'L');

	$pdf->SetFont('Arial','B',10);
	$pdf->setXY(15,$y+7);
	$pdf->Cell(30,5,'Metode Pembayaran',0,0,'L');
	$pdf->Cell(5);
	$pdf->Cell(2,5,':',0,0,'L');
	$pdf->Cell(5);
	$pdf->SetFont('Arial','',9);
	$pdf->Cell(30,5,$data->metode,0,0,'L');



	$line2 = $y + 13.5;
	$pdf->setXY(15,$line2);
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(30,5,'Transfer Bank',0,0,'L');
	$pdf->Cell(5);
	$pdf->Cell(2,5,':',0,0,'L');
	$pdf->Cell(5);
	$pdf->SetFont('Arial','',9);
	$pdf->Cell(30,5,'1. Bank BCA KCU Bekasi No Acc. 0663162851 a/n : Fajar Shodiq',0,0,'L');
	$pdf->setXY(57,$line2 + 6.5);
	$pdf->Cell(30,5,'2. Bank Mandiri KCP Bekasi Ahmad Yani No. Acc. 1670001377901 a/n : Fajar Shodiq',0,0,'L');

}

function perusahaan($pdf,$y,$data){

	$pdf->SetFont('Arial','',10);
	$pdf->setXY(105,$y);
	$pdf->Cell(30,5,"Nama Usaha",0,0,'L');
	$pdf->Cell(5);
	$pdf->SetFont('Arial','',8);
	if (strlen($data->namausaha)>40) {
		$y += 4;
		$pdf->MultiCell(58,5,$data->namausaha,0,'L');	
	}elseif (strlen($data->namausaha)>80) {
		$y += 8;
		$pdf->MultiCell(58,5,$data->namausaha,0,'L');	
	}elseif (strlen($data->namausaha)>120) {	
		$y += 12;
		$pdf->MultiCell(58,5,$data->namausaha,0,'L');
	} else {
		$pdf->Cell(30,5,$data->namausaha,0,0,'L');
	}
	
	
	$pdf->Ln();

	$line1 = $y + 7.5;
	$pdf->SetFont('Arial','',10);
	$pdf->setXY(105,$line1); //text inline
	$pdf->Cell(30,5,'Alamat',0,0,'L');
	$pdf->Cell(5);
	$pdf->SetFont('Arial','',8);
	$pdf->setXY(140,$line1); //text inline
	$pdf->MultiCell(58,5,$data->alamat,0,'L');

	if (strlen($data->alamat)>100) {
		$line2 = $y + 34.5;
		// $pdf->Ln();
		// $pdf->Ln();
	} else {
		$line2 = $y + 24.5;
	}
	
	
	$pdf->setXY(105,$line2);
	$pdf->SetFont('Arial','',10);
	$pdf->Cell(30,5,'UP',0,0,'L');
	$pdf->Cell(5);
	$pdf->SetFont('Arial','',9);
	$pdf->setXY(140,$line2); //text inline
	$pdf->Cell(30,5,$data->up,0,0,'L');


	$line3 = $line2 + 5.5;
	$pdf->SetFont('Arial','',10);
	$pdf->setXY(105,$line3); //text inline
	$pdf->Cell(30,5,'Telepon',0,0,'L');
	$pdf->Cell(5);
	$pdf->SetFont('Arial','',9);
	$pdf->setXY(140,$line3); //text inline
	$pdf->Cell(30,5,$data->telepon,0,0,'L');

}
function pembayaran($pdf,&$totalHarga,$data){
	$hargaDasar = 1250000;

	$pdf->SetFillColor(207, 209, 211);
	$pdf->Rect(15,100,75,5,'F');
	$pdf->Rect(15,75,75,5,'F');
	$pdf->SetFont('Arial','B',12);
	$pdf->SetTextColor(0,0,0);
	$pdf->setXY(15,55);
	$pdf->Cell(30,5,'BIAYA PELAYANAN',0,0,'L');
	$pdf->Ln();

	$pdf->SetFont('Arial','',10);
	$pdf->Cell(5);
	$pdf->Cell(30,5,'Nama Paket',0,0,'L');
	$pdf->Cell(5);
	$pdf->Cell(2,5,':',0,0,'L');
	$pdf->Cell(1.5);
	
	if (strlen($data->item)>35){
		$pdf->SetFont('Arial','B',7);
		$pdf->Cell(30,5,$data->item,0,0,'L');
	}else{
		$pdf->SetFont('Arial','B',8);
		$pdf->Cell(30,5,$data->item,0,0,'L');
	}
	
	$pdf->Ln();

	$pdf->SetFont('Arial','',10);
	$pdf->Cell(5);
	$pdf->Cell(30,5,'Training Accurate',0,0,'L');
	$pdf->Cell(5);
	$pdf->Cell(2,5,':',0,0,'L');
	$pdf->Cell(5);
	$pdf->Cell(30,5,number_format($data->hargaitem),0,0,'R');
	$pdf->Ln();
	$pdf->Cell(5);
	$pdf->Cell(30,5,'Qty',0,0,'L');
	$pdf->Cell(5);
	$pdf->Cell(2,5,':',0,0,'L');
	$pdf->Cell(5);
	$jmlHariTraining=5;
	$pdf->Cell(30,5,$data->qty,0,0,'R');
	$pdf->Ln();
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(5);
	$pdf->Cell(30,5,'Sub Total',0,0,'L');
	$pdf->Cell(5);
	$pdf->Cell(2,5,':',0,0,'L');
	$pdf->Cell(5);
	$totalHarga = intval($data->hargaitem) * intval($data->qty);
	$pdf->Cell(30,5,number_format($totalHarga),0,0,'R');

	$pdf->Ln();
	$pdf->Ln();
	$biayaTraining=150000;
	$pdf->SetFont('Arial','B',12);
	$pdf->Cell(5);
	$pdf->Cell(30,5,'AKOMODASI',0,0,'L');
	$pdf->Ln();
	$pdf->SetFont('Arial','',10);
	$pdf->Cell(5);
	$pdf->Cell(30,5,'Biaya Transport',0,0,'L');
	$pdf->Cell(5);
	$pdf->Cell(2,5,':',0,0,'L');
	$pdf->Cell(5);
	$pdf->Cell(30,5,number_format($data->transport),0,0,'R');
	$pdf->Ln();
	$pdf->Cell(5);
	$pdf->Cell(30,5,'Jumlah Hari',0,0,'L');
	$pdf->Cell(5);
	$pdf->Cell(2,5,':',0,0,'L');
	$pdf->Cell(5);
	$pdf->Cell(30,5,number_format($data->jmlhari),0,0,'R');
	$pdf->Ln();
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(5);
	$pdf->Cell(30,5,'Sub Total',0,0,'L');
	$pdf->Cell(5);
	$pdf->Cell(2,5,'',0,0,'L');
	$pdf->Cell(5);
	$totalHarga = $totalHarga + intval($data->transport) * intval($data->jmlhari);
	$pdf->Cell(30,5,number_format(intval($data->transport) * intval($data->jmlhari)),0,0,'R');
}

function footer($pdf, $y){
	$pdf->SetFillColor(33,150,243);
	$pdf->Rect(10,$y,190,10,'F');
	$pdf->SetTextColor(255,255,255);
	$pdf->SetFont('Arial','',10);
	$pdf->setXY(10,$y + 3);
	$pdf->Cell(180,5,'FAC Institute: Jl Jatiwaringin No 8 Pangkalan Jati Jakarta Timur - 13620.',0,0,'C');
	$pdf->setXY(15,$y + 10);
	$pdf->SetTextColor(0,0,0);
	$pdf->SetFont('Arial','',8);
	$pdf->Cell(180,5,'Apabila memerlukan bantuan, silahkan hubungi Customer Service: +6281290083983, E-Mail: training@fac-institute.com',0,0,'C');
	
}

function getNamaBulan($bulan){
		$bulan=intval($bulan);
		if ($bulan==1) {
			return "Januari ".date('Y');
		}elseif ($bulan==2) {
			return "Februari ".date('Y');
		}elseif ($bulan==3) {
			return "Maret ".date('Y');
		}elseif ($bulan==4) {
			return "April ".date('Y');
		}elseif ($bulan==5) {
			return "Mei ".date('Y');
		}elseif ($bulan==6) {
			return "Juni ".date('Y');
		}elseif ($bulan==7) {
			return "Juli ".date('Y');
		}elseif ($bulan==8) {
			return "Agustus ".date('Y');
		}elseif ($bulan==9) {
			return "September ".date('Y');
		}elseif ($bulan==10) {
			return "Oktober ".date('Y');
		}elseif ($bulan==11) {
			return "November ".date('Y');
		}elseif ($bulan==12) {
			return "Desember ".date('Y');
		}elseif ($bulan>12) {
			return "Januari ".intval(date('Y'))+1;
		}else{
			return "Error reading current month!";
		}
}

?>