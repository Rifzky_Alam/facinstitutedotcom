<?php 
include_once '../fpdf/fpdf.php';
$pdf = new FPDF();
$pdf->AddPage('l','A4',0);
$pdf->SetAuthor('Rifzky Alam - Software Dev');
$pdf->SetAutoPageBreak(false,1);
//bill to
$pdf->Rect(42,45,135,8);
//alamat bill to
$pdf->Rect(42,53,135,37);
//invoice date
$pdf->Rect(180,70,50,20);
//invoice no
$pdf->Rect(230,70,50,20);
//items
$pdf->Rect(20,103,50,30);
$pdf->Rect(70,103,70,30);
$pdf->Rect(140,103,30,30);
$pdf->Rect(170,103,20,30);
$pdf->Rect(190,103,40,30);
$pdf->Rect(230,103,50,30);
//terbilang
$pdf->Rect(40,135,120,10);

//sub total
$pdf->Rect(180,135,50,7);
//discount
$pdf->Rect(180,142,50,7);
//description
$pdf->Rect(20,151.5,150,20);
//total invoice
$pdf->Rect(180,153,50,8);
//payment & balance
$pdf->Rect(180,162.5,50,10);


$pdf->Image('../images/logo-fac.jpg',25,10,50,30);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(65);
$pdf->Cell(130, 10, 'LPK First Asian Consulting Institute (FACI)', 0, 0, "C");



$pdf->Ln();
$pdf->SetFont('Arial','',12);
$pdf->Cell(65);
$pdf->Cell(130, 5, 'Jl Raya Jatiwaringin No 8 Pangkalan Jati Jatiwaringin Jakarta Timur', 0, 0, "L");
$pdf->Cell(10);
$pdf->SetFont('Arial','B',24);
$pdf->Cell(60, 5, 'Sales Invoice', 0, 0, "C");
$pdf->Ln();

$pdf->SetFont('Arial','',12);
$pdf->Cell(65);
$pdf->Cell(15, 5, 'Email', 0, 0, "L");
$pdf->Cell(5);
$pdf->Cell(3, 5, ':', 0, 0, "L");
$pdf->Cell(30, 5, 'finance@fac-institute.com,cs.facinstitute@gmail.com', 0, 0, "L");

$pdf->Ln();
$pdf->Cell(65);
$pdf->Cell(15, 5, 'Website', 0, 0, "L");
$pdf->Cell(5);
$pdf->Cell(3, 5, ':', 0, 0, "L");
$pdf->Cell(30, 5, 'http://www.fac-institute.com', 0, 0, "L");

$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->Cell(10);
$pdf->Cell(15, 8, 'Bill To', 0, 0, "L");
$pdf->Cell(2);
$pdf->Cell(3, 8, ':', 0, 0, "L");
$pdf->Cell(5);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(130, 8, 'PT Teknologi Alam Lestari', 0, 0, "L");

$pdf->Ln();
$pdf->SetFont('Arial','',12);
//$pdf->Cell(35);
$pdf->SetXY(45,55);
$pdf->MultiCell(130,5,'Gedung Rasuna Office park 3, Lantai 4 Blok UO no. 11 Kompleks Taman Rasuna Jl. HR. Rasuna Said Jakarta Selatan,12960 ');
$pdf->Cell(35);
$pdf->Cell(125,5,'Up. Ibu Nova 081218532100',0,0,'L');
//$pdf->Cell(15);
$pdf->SetXY(182,72);
$pdf->Cell(15, 5, 'Invoice Date', 0, 0, "L");
$pdf->Cell(35);
$pdf->Cell(15, 5, 'Invoice No.', 0, 1, "L");

$pdf->Ln();
$pdf->Cell(175);
$pdf->Cell(20,5,'12 Sep 2016',0,0,'T');
$pdf->Cell(35);
$pdf->Cell(20,5,'SI/FAC/16/0251',0,1,'T');

$pdf->Ln();

$pdf->SetXY(20,93);
$pdf->Cell(50,10,'Item',1,0,'C');
$pdf->Cell(70,10,'Item Description',1,0,'C');
$pdf->Cell(30,10,'Qty',1,0,'C');
$pdf->Cell(20,10,'Item Unit',1,0,'C');
$pdf->Cell(40,10,'Unit Price',1,0,'C');
$pdf->Cell(50,10,'Amount',1,0,'C');

$pdf->Ln();
$pdf->Cell(10);
$pdf->Cell(50,10,'Impl',0,0,'L');
$pdf->Cell(70,10,'Implementasi Accurate',0,0,'L');
$pdf->Cell(30,10,'1',0,0,'R');
$pdf->Cell(20,10,'Day',0,0,'C');
$pdf->Cell(40,10,'500.000',0,0,'R');
$pdf->Cell(50,10,'500.000',0,0,'R');

$pdf->SetXY(20,135);
$pdf->Cell(10,10,'Say',0,0,'L');
$pdf->Cell(5,10,':',0,0,'L');
$pdf->Cell(10);
$pdf->Cell(135,10,'Lima Ratus Ribu Rupiah',0,0,'L');
$pdf->Cell(25,7,'Sub Total',0,0,'L');
$pdf->Cell(5,7,':',0,0,'L');
$pdf->Cell(25,7,'500.000',0,0,'L');
$pdf->Ln();
$pdf->Cell(170);
$pdf->Cell(25,7,'Discount',0,0,'L');
$pdf->Cell(5,7,':',0,0,'L');
$pdf->Cell(25,7,'0',0,0,'L');
$pdf->Ln();
$pdf->Cell(12);
$pdf->SetFillColor(255,255,255);
$pdf->Cell(25,5,'Description',0,1,'L',true);
$pdf->Cell(11);
$pdf->Cell(25,5,'Training Accurate 3 September 2016',0,0,'L'); //klo lebih dari satu => 0,1,'L' setelahnya pakai SetXY or pakai MultiCell

$pdf->SetXY(180,155);
$pdf->Cell(25,5,'Total Invoice',0,0,'L');
$pdf->Cell(5,5,':',0,0,'L');
$pdf->Cell(25,5,'500.000',0,0,'L');

$pdf->SetXY(180,163);
$pdf->Cell(25,5,'Payment',0,0,'L');
$pdf->Cell(5,5,':',0,0,'L');
$pdf->Cell(25,5,'0',0,0,'L');
$pdf->SetXY(180,168);
$pdf->Cell(25,5,'Balance',0,0,'L');
$pdf->Cell(5,5,':',0,0,'L');
$pdf->Cell(25,5,'500.000',0,0,'L');
$pdf->Ln();

$pdf->Cell(10);
$pdf->Cell(25,5,'Best Regards',0,0,'L');

$pdf->Ln();
$pdf->Cell(105);
$pdf->Cell(25,7,'Please transfer to the following accounts:',0,0,'L');
$pdf->Ln();
$pdf->Cell(105);
$pdf->Cell(25,5,'- Bank BCA KCU Bekasi No. Acc. 0663162851 a/n : Fajar Shodiq',0,0,'L');
$pdf->Ln();

$pdf->Cell(105);
$pdf->Cell(25,5,'- Bank Mandiri KCP Bekasi Ahmad Yani No. Acc. 1670001377901 a/n : Fajar Shodiq',0,0,'L');

$pdf->Ln();
$pdf->Cell(10);
$pdf->SetFont('Arial','BU',12);
$pdf->Cell(25,5,'Fajar Shodiq',0,0,'L');

$pdf->Output();

?>