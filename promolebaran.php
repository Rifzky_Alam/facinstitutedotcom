<?php
$data = array(
        'base_url' => base_url()
    );

    $data = (object) $data;
?>

<?php
function base_url(){
  $currentPath = $_SERVER['PHP_SELF'];$pathInfo = pathinfo($currentPath);$hostName = $_SERVER['HTTP_HOST'];$protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https://'?'https://':'http://';
  return 'https://'.$hostName."/";
}


?>

<!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="<?= $data->base_url ?>images/homepage2019/elements/fav.png">
	<!-- Author Meta -->
	<meta name="author" content="Rifzky Alam, Dino Damara">
	<!-- Meta Description -->
	<meta name="description" content="<?= $data->pagedesc ?>">
	<!-- Meta Keyword -->
	<meta name="keywords" content="<?= $data->pagekeywords ?>">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title></title>

	<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
		<!--
		CSS
		============================================= -->
		<link rel="shortcut icon" href='https://fac-institute.com/_caramel/assets/img/g25992_9_kUd_icon.ico'>
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/linearicons.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/owl.carousel.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/nice-select.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/magnific-popup.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/bootstrap.css">
		<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/main.css">
		<style media="screen">
		.banner-content h1 {
				font-size: 38px;
		}
		.nav-menu a {
				padding: 15px 8px 1px 8px;
				font-size: 16px;
				font-weight: 500;
		}
		.nav-menu ul li a {
				font-size: 14px;
		}

		.menu-active {
			border-bottom: 5px solid #df003a;
		}

		.navbar {
				padding: 0.2rem 1rem;
		}


    @media (max-width: 480px) {
      .mybanner {
        height:auto;
      }
      #myh2 {
        font-size: 22px;
      }
      .myimg{
        display: none;
      }
      #mobile-nav-toggle {
        top: 35px;
      }
    }



		</style>
		<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
	</head>
	<body>



<section style='background: url("https://fac-institute.com/images/homepage2019/promo/01-BG-atas-min.jpg");
background-position: center center;background-repeat: no-repeat;
padding-top:40px;padding-right:0px;padding-bottom:60px;padding-left:0px;
-webkit-background-size:cover;-moz-background-size:cover;
-o-background-size:cover;background-size:cover;
height: auto'>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-6 col-md-12 col-sm-12 mt-40">
				<img class="img-fluid" width="100%" src="https://fac-institute.com/images/homepage2019/promo/01-IMAGE-atas-min.png" alt="">
			</div>
		</div>
	</div>
</section>

<section style='background: url("") ;background-position: center center;background-repeat: no-repeat;
padding-top:60px;padding-right:30px;padding-bottom:20px;padding-left:30px;
-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
  <div class="container-fluid">
    <div class="row justify-content-center align-items-center">
      <div class="col-lg-12 text-center" style="max-width:1200px">
        <h1 style="font-size:40px" class="mb-10">Bisnis Jadi Mudah dengan Accurate Online</h1>
        <p style="font-size:20px;font-weight:300;line-height:32px">
          Accurate Online membantu Anda menjalankan usaha, mencatat transaksi, hingga memantau jalannya bisnis kapan pun dan di mana pun.</p>
          <p><a type="button" class="btn btn-danger" target="_self" href="https://youtu.be/c6rnnqx0usw?autoplay=1"><i class="fa fa-play"></i><span class="fusion-button-text"> Tonton Video</span></a></p>
          <img class="img-fluid mt-20 mb-40" width="500" data-aos="fade-up" src="https://accurate.id/wp-content/uploads/2019/05/Accurate-online-video-image.png" alt="Audit FAC">
      </div>
    </div>

    <div class="row justify-content-center align-items-center" >
      <div class="col-lg-6 text-center" style="max-width:300px">
        <img class="img-fluid mt-20 mb-20" width="100%"  src="https://accurate.id/wp-content/uploads/2019/05/fitur-1-multi-mata-uang-dan-gudang.png" alt="Akuntansi Nyaman FAC-Institute">
        <p style="font-size:16px">
          Multi Mata Uang dan Multi Gudang
        </p>
      </div>
      <div class="col-lg-6 text-center" style="max-width:300px">
        <img class="img-fluid mt-20 mb-20" width="100%"  src="https://accurate.id/wp-content/uploads/2019/05/fitur-2-stok-opname-piutang-pelanggan.png" alt="Akuntansi Nyaman FAC-Institute">
        <p  style="font-size:16px">
          Stok Opname dan Piutang Pelanggan
        </p>
      </div>
    </div>


    <div class="row justify-content-center align-items-center" >
      <div class="col-lg-6 text-center" style="max-width:300px">
        <img class="img-fluid mt-20 mb-20" width="100%"  src="https://accurate.id/wp-content/uploads/2019/05/fitur-3-pajak-dan-smartlink.png" alt="Akuntansi Nyaman FAC-Institute">
        <p style="font-size:16px">
          Hitung Pajak dan SmartLink e-banking
        </p>
      </div>
      <div class="col-lg-6 text-center" style="max-width:300px">
        <img class="img-fluid mt-20 mb-20" width="100%"  src="https://accurate.id/wp-content/uploads/2019/05/fitur-4-backup-otomatis-dan-keamanan-data.png" alt="Akuntansi Nyaman FAC-Institute">
        <p  style="font-size:16px">
          Backup Otomatis dan 100% Data Aman
        </p>
      </div>
    </div>
  </div>
</section>



    <section style='background-color: rgba(255,255,255,0);background-image: url("https://accurate.id/wp-content/uploads/2019/05/02-BG-intro-min-min.png");
    background-position: left bottom;background-repeat:no-repeat;
    padding-top:4%;padding-right:30px;padding-bottom:100px;padding-left:30px;
    -webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>

    	<div class="container-fluid">
    		<div class="row">
    			<div class="col-lg-6">
    				<img class="myimg" src="https://accurate.id/wp-content/uploads/2019/05/NEW-02-IMAGE-ketupat-min-min-min-min.png" width="283" height="500" alt="image ketupat" title="image ketupat"  srcset="https://accurate.id/wp-content/uploads/2019/05/NEW-02-IMAGE-ketupat-min-min-min-min-200x353.png 200w, https://accurate.id/wp-content/uploads/2019/05/NEW-02-IMAGE-ketupat-min-min-min-min.png 283w" sizes="(max-width: 1000px) 100vw, 283px">
    			</div>
         <div class="col d-flex align-items-center justify-content-center ">
           <img class="img-fluid" style="padding-top:120px" width="100%" src="https://accurate.id/wp-content/uploads/2019/05/NEW-Landing-Page-2-Tulisan-Mau-Ketupat-min.png" alt="Entry Data FAC"></div>
    		</div>
    	</div>
    </section>

    <section style='background: url("https://fac-institute.com/images/homepage2019/promo/01-BG-atas-min.jpg") ;
    background-position: left bottom;background-repeat:no-repeat;
    padding-top:4%;padding-right:30px;padding-bottom:80px;padding-left:30px;
    -webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>

      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12 text-center">
              <img class="img-fluid mb-20" width="300" src="https://accurate.id/wp-content/uploads/2019/05/04-IMAGE-promo-harga-min.png" alt="Audit FAC">
          </div>
          <div class="col-lg-12 text-center">
            <img class="img-fluid mt-10 mb-20" width="300" src="https://accurate.id/wp-content/uploads/2019/05/04-IMAGE-promo-package-min.png" alt="Audit FAC">
          </div>
          <div class="col-lg-12 text-center">
            <img class="img-fluid mt-10 mb-20" width="300"src="https://accurate.id/wp-content/uploads/2019/05/04-IMAGE-promo-kode-min.png" alt="Entry Data FAC">
          </div>
          <div class="col-lg-12 text-center">
            <img class="img-fluid mt-10 mb-20" width="500"src="https://accurate.id/wp-content/uploads/2019/05/05-IMAGE-button-1-new.gif" alt="Entry Data FAC">
          </div>
        </div>
      </div>
    </section>

    <section style="background-color: rgba(255,255,255,0);background-image: url(&quot;https://accurate.id/wp-content/uploads/2019/05/08-BG-syarat-min.jpg&quot;);background-position: left top;background-repeat: no-repeat;padding-top:10%;padding-right:5%;padding-bottom:0px;padding-left:5%;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;">
    	<div class="container-fluid">
    		<div class="row">
    			<div class="col-lg-12 mb-40">
            <h2 class="mb-20"><strong>Syarat dan Ketentuan</strong></h2>
<ol style="list-style-type: circle; font-size:18px;line-height: 1.6;">
	<li>Promo ini hanya berlaku untuk database baru (yang belum pernah diaktifkan sama sekali)</li>
	<li>Harap masukkan kode promo KETUPAT sebelum melakukan check out pembayaran</li>
	<li>Periode promo Ketupat berlangsung dari tanggal 23 &ndash; 29 Mei 2019</li>
	<li>Dengan melakukan pembelian promo Ketupat, Anda cukup membayar Rp 1.600.000 (belum termasuk PPN) &ndash; dari harga normal Rp 2.880.000 &ndash; untuk setiap pembelian 1 Accurate Online + add on 2 user</li>
	<li>Setiap promo Gratis add on 2 user berlaku untuk 1 tahun semenjak database diaktivasi</li>
	<li>Khusus paket promo onsite (training) dapat diperoleh dengan mengisi form pendaftaran. Mohon untuk membaca&nbsp;<a href="#" rel="noopener noreferrer" target="_blank">syarat dan ketentuan Onsite</a>&nbsp;terlebih dahulu</li>
	<li>Syarat dan ketentuan dapat berubah sewaktu-waktu tanpa pemberitahuan sebelumnya</li>
</ol>
    		</div>
    	</div>
    </section>


    <section style="background-color: rgba(255,255,255,0);background-image: url(&quot;https://accurate.id/wp-content/uploads/2019/05/NEW-PNG-BG-TIMER-BAWAH-min.png&quot;);background-position: left top;background-repeat: no-repeat;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;">
    	<div class="container-fluid">
    		<div class="row">
    			<div class="col-lg-12 mb-40 mt-40">
            <h3 style="text-align: center;" data-fontsize="30" data-lineheight="45">Jangan Sampai Terlewat!<br></h3>
    		</div>
    	</div>
    </section>


		<script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/jquery-2.2.4.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/bootstrap.min.js"></script>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/easing.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/hoverIntent.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/superfish.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.ajaxchimp.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.magnific-popup.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/owl.carousel.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.sticky.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.nice-select.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/parallax.min.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/mail-script.js"></script>
		<script src="<?= $data->base_url ?>assets/homepage2019/js/main.js"></script>
		<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
		<script>
		AOS.init();
		$('.test1').click(function() {
				var sectionTo = $(this).attr('href');
				$('html, body').animate({
					scrollTop: $(sectionTo).offset().top
				}, 1000);
		});
		</script>
	</body>
</html>
