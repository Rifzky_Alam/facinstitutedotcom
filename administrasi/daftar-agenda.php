<?php 
session_start();
if (!isset($_SESSION['admin'])||($_SESSION['admin']['tipe']!='admin'&&$_SESSION['admin']['tipe']!='marketing')) {
    header('Location:../login/index.php');
}
?>




<!DOCTYPE html>
<html>

<?php 
//echo $tipeAdmin=$_SESSION['admin']['tipe'];
$judul='Daftar Agenda Client';
$page = 'home';
include_once 'header.php';
include_once '../model/Pendaftar.php';
include_once '../model/page.php';

$myPage = new Page();
$pendaftar = new Pendaftar();
?>

<body>



<?php 

include_once 'sidebar.php';

include_once 'top-nav.php';

 ?>


<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3>Daftar Agenda</h3>
	</div>

    <div class="row">
        <div class="col-md-12">
            <form action="" method="GET">
            <div class="form-inline">
                <input type="text" name="search" class="form-control" placeholder="Cari nama perusahaan" style="width:70%">
                <button class="btn btn-primary" style="width:20%">Search</button>
            </div>
            </form>
        </div>
    </div>


    <?php if (isset($_GET['p'])&&!empty($_GET['p'])): ?>
    <?php $datas=json_decode($pendaftar->fetchAllAgendaByPendaftarID($_GET['p'])); ?>

    <?php 
        $data = json_decode($pendaftar->fetchAllAgenda());
    ?>

    <div class="row">
        <div class="col-md-12">
            <a href="schedules?idp=<?php echo $_GET['p'] ?>" class="btn btn-xs btn-default">
                <span class="glyphicon glyphicon-plus"></span>
            </a>            
        </div>
    </div>


    <div class="row" style="overflow:auto;">
        <div class="col-md-12">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Nama Perusahaan</th>
                        <th>Nama Kontak</th>
                        <th>Tanggal</th>
                        <th>Agenda</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>

                    <?php  
                    for ($i=0; $i < count($datas); $i++) { ?>
                    
                    <?php if ($datas[$i]->status=='1'): ?>
                    <tr class="success">
                    <?php elseif($datas[$i]->status=='-1'): ?>
                    <tr class="info">    
                        <?php else: ?>
                    <tr class="warning">            
                    <?php endif ?>    
                    

                        <td><?php echo $datas[$i]->nama; ?></td>
                        <td><?php echo $datas[$i]->nama_personal ?></td>
                        <td><?php echo $datas[$i]->tanggal ?></td>
                        <?php $arr=json_decode($datas[$i]->agenda); ?>
                        <td>
                            <?php 
                            for ($j=0; $j < count($data); $j++){ 
                                for ($k=0; $k < count($arr); $k++){ 
                                    if ($arr[$k]==$data[$j]->id){ 
                                        echo $data[$j]->nama_agenda." # ";
                                    }
                                }
                            }
                             ?>
                        </td>
                        <td>
                            <?php if (@$datas[$i]->id_invoice==''): ?>
                            <a href="preview?an=jadwal&id=<?php echo $datas[$i]->id ?>" target="_blank">Preview</a> ||
                                <?php else: ?>
                            <a href="preview?an=ji&id=<?php echo $datas[$i]->id ?>" target="_blank">Preview</a> ||        
                            <?php endif ?>
                            
                            <a href="<?php echo 'editjadwaltraining?kd='.$datas[$i]->id ?>">Edit</a> ||
                            
                            <?php if (@$datas[$i]->id_invoice==''&&$datas[$i]->status!='-1'): ?>
                                <a href="<?php echo 'invoice-form?idj='.$datas[$i]->id ?>">Invoice</a> ||
                                <a href="<?php echo basename(__FILE__, '.php').'?ac=send&id='.$datas[$i]->id ?>">Kirim</a> ||
                            <?php elseif($datas[$i]->status!='-1'): ?>
                                <a href="<?php echo 'send-invoice?app=edit&kd='.$datas[$i]->id_invoice ?>">Edit Invoice</a> ||
                                <a href="<?php echo getBaseUrl().'docs/invoice-baru?inv='.$datas[$i]->id ?>">PDF</a> ||
                            <?php endif ?>
                            <a href="<?php echo basename(__FILE__, '.php').'?ac=rmv&id='.$datas[$i]->id ?>">Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>            
        </div>
    </div>
        <?php elseif(isset($_GET['ac'])&&$_GET['ac']=='send'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>
            <?php 
            include_once '../model/Surat.php';
            include_once '../model/Mail.php'; 
            // $dataz = $pendaftar->fetchDataAgendaForSendingMail($_GET['id']);
            $surat = new Surat();
            $mail = new FACMail();

            $oke = $pendaftar->getJadwal($_GET['id']);

            $arr = array();
            $content = $surat->KirimJadwal($oke,$_SESSION['admin']['nama']);

            // print_r($oke);
            $lampiran = explode(" # ", $oke->attachment);
            

            foreach ($lampiran as $key) {
                array_push($arr, 'attachments/'.$key);
            }



            $dataEmail = array(
                'to' => $oke->email_kontak,
                'nama'=> '',
                'cc'=> explode(" ", $oke->cc),
                'subject'=> 'Jadwal Training Accurate',
                'content'=>$content,
                'attachments'=> $arr
            );

            $dataEmail = (object) $dataEmail;

             if ($mail->KirimJadwal($dataEmail)) {
                 include_once '../model/Acara.php';
                 $acara = new Acara();

                 echo "<script>alert('Email has been sent!');</script>";
                
                 $objek = array('acara' => $oke->id_pendaftar, 'tanggal' => explode(' # ', $oke->tanggal));
                 if ($oke->status=='0') {
                    $acara->inputSimple((object) $objek);
                    $pendaftar->editStatusAgenda($oke->id);
                 }
                $myPage->addLogForLogin($_SESSION['admin']['username'],'successfully sent project note and material request',$_SERVER['REMOTE_ADDR']);

                 echo "<script>location.replace('".basename(__FILE__, '.php')."?p=".$oke->id_pendaftar."');</script>";
             }else{
                 echo "<script>alert('Email was failed to send!');</script>";
             }

            ?>
            

        <?php elseif(isset($_GET['ac'])&&$_GET['ac']=='rmv'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>

            <?php echo $_SESSION['admin']['tipe'] ?>
        <?php elseif(isset($_GET['search'])&&!empty($_GET['search'])): ?>

            <?php 
                $datas = $pendaftar->fetchAgendaByPerusahaanName($_GET['search']);
                $data = json_decode($pendaftar->fetchAllAgenda());
            ?>
            <div class="row" style="overflow:auto;">
                <div class="col-md-12">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Nama Perusahaan</th>
                                <th>Nama Kontak</th>
                                <th>Tanggal</th>
                                <th>Agenda</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php  
                            for ($i=0; $i < count($datas); $i++) { ?>
                            
                            <?php if ($datas[$i]->status=='1'): ?>
                            <tr class="success">
                            <?php elseif($datas[$i]->status=='-1'): ?>
                            <tr class="info">    
                                <?php else: ?>
                            <tr class="warning">            
                            <?php endif ?>    
                            

                                <td><?php echo $datas[$i]->nama; ?></td>
                                <td><?php echo $datas[$i]->nama_personal ?></td>
                                <td><?php echo $datas[$i]->tanggal ?></td>
                                <?php $arr=json_decode($datas[$i]->agenda); ?>
                                <td>
                                    <?php 
                                    for ($j=0; $j < count($data); $j++){ 
                                        for ($k=0; $k < count($arr); $k++){ 
                                            if ($arr[$k]==$data[$j]->id){ 
                                                echo $data[$j]->nama_agenda." # ";
                                            }
                                        }
                                    }
                                     ?>
                                </td>
                                <td>
                                    <?php if (@$datas[$i]->id_invoice==''): ?>
                                    <a href="preview?an=jadwal&id=<?php echo $datas[$i]->id ?>" target="_blank">Preview</a> ||
                                        <?php else: ?>
                                    <a href="preview?an=ji&id=<?php echo $datas[$i]->id ?>" target="_blank">Preview</a> ||        
                                    <?php endif ?>
                                    
                                    <a href="<?php echo 'editjadwaltraining?kd='.$datas[$i]->id ?>">Edit</a> ||
                                    
                                    <?php if (@$datas[$i]->id_invoice==''&&$datas[$i]->status!='-1'): ?>
                                        <a href="<?php echo 'invoice-form?idj='.$datas[$i]->id ?>">Invoice</a> ||
                                        <a href="<?php echo basename(__FILE__, '.php').'?ac=send&id='.$datas[$i]->id ?>">Kirim</a> ||
                                    <?php elseif($datas[$i]->status!='-1'): ?>
                                        <a href="<?php echo 'send-invoice?app=edit&kd='.$datas[$i]->id_invoice ?>">Edit Invoice</a> ||
                                        <a href="<?php echo getBaseUrl().'docs/invoice-baru?inv='.$datas[$i]->id ?>">PDF</a> ||
                                    <?php endif ?>
                                    <a href="<?php echo basename(__FILE__, '.php').'?ac=rmv&id='.$datas[$i]->id ?>">Delete</a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>            
                </div>
            </div>



        <?php else: ?>


            <?php 

            if (isset($_GET['page'])&&!empty($_GET['page'])) {
                $datas=json_decode($pendaftar->fetchAllAgendas($_GET['page']));
                $data = json_decode($pendaftar->fetchAllAgenda($_GET['page']));
            } else {
                $datas=json_decode($pendaftar->fetchAllAgendas('1'));
                $data = json_decode($pendaftar->fetchAllAgenda('1'));
            }

            ?>


    <div class="row" style="overflow:auto;margin-top:25px;">
        <div class="col-md-12">
            silahkan mencari data terlebih dahulu..
        </div>
    </div>
    <!--
    <div class="row">
        <div class="col-md-12">
        <center>
            <ul class="pagination pagination-sm">
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
            </ul>
        </center>
        </div>
    </div>
    -->
    <?php endif ?>

	
</div>
</body>
</html> 