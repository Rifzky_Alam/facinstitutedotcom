<?php 

session_start();
$url = 'new-attachments';

include_once '../baseurl.php';
include_once 'session/session-class.php';

$session = new Sessionz();
$session->AdminMarketing();

if (isset($_GET['inv'])&&!empty($_GET['inv'])) {
	include_once '../model/Transaksi.php';
	include_once '../model/Invoice.php';
	$transaksi = new Transaksi();
	$invoice = new Invoice();
	$invoice->setNo($_GET['inv']); //set invoice number
	if ($invoice->CountAttachment()!='0') { //check if the transaction has 'data surat' on database or not, else input new
		echo "<script>location.replace('".$url."?inve=".$_GET['inv']."')</script>";	
	}

	if (isset($_POST['in'])) { //check if input data is valid
		if (empty($_POST['in']['email'])) { //user must select the 'email to' data
			echo "<script>alert('Tujuan email harus dipilih!!')</script>";
		}else{ 
			
			$input = array(
				'to' => $_POST['in']['email'],
				'cc' => $_POST['in']['cc'],
				'attachments' => $invoice->CekLampiran(@$_POST['in']['lampiran'])
			);
			$input = (object) $input;
			// print_r($input);
			$invoice->setData($input);
			if ($invoice->InputSurat()) { //check if data enter database
				echo "<script>alert('Berhasil menyimpan data surat!')</script>";
				echo "<script>location.replace('new-invoice?send=".$_GET['inv']."')</script>";
			} else {
				echo "<script>alert('Data surat gagal disimpan, harap menghubungi admin sistem!')</script>";
			}
		}
	}

	$datainv = $invoice->GetByID(); //getting invoice data
	
	$transaksi->setID($datainv->inv_id_trans); //setting transaction id_transaction

	$datas = $transaksi->FetchTransactionForPreview(); //getting transaction data
	
	//check if pdf file is created
	if(!file_exists($invoice->getFolder().'Invoice '.$datainv->inv_id_trans.'.pdf')){ //checking if invoice file is created
		include_once '../model/Mypdf.php'; //calling pdf class for creating the new invoice file .pdf for future attachment
		$pdf = new Mypdf();
		
		if (count($transaksi->FetchItemsForInvoice())=='0') {
			$mylist = array();
			$obj1 = array('nama_item' => 'Tidak ada data yang dipilih','harga'=>'0','qty'=> '0');
			array_push($mylist, (object) $obj1);
		}else{
			$mylist = $transaksi->FetchItemsForInvoice(); 
		}

		$data = array(
			'nama_cust' => $datas->nama_cust,
	        'telepon_cust' => $datas->telp_cust,
    	    'email_cust' =>$datas->email_cust,
			'no_invoice' => $_GET['inv'],
			'nama_usaha' => $datas->nama,
			'alamat' => $datainv->inv_alamat,
			'deskripsi' => $datainv->inv_deskripsi,
			'tanggal' => $datainv->inv_date,
			'metode_bayar' => $invoice->TentukanMetodeBayar($datainv->inv_metode_bayar),
			'items' => $mylist,
			'diskon' => $datas->trans_discount,
			'dp' => $datas->trans_dp 
		);
		$data = (object) $data;
		$pdf->setData($data);
		$pdf->setPath($invoice->getFolder());
		$pdf->setNameFile('Invoice '.$datainv->inv_id_trans.'.pdf');
		//creating file..
		$pdf->Invoice('');
		$berita = 'File invoice sudah dibuat';
	}else{ //sending info if file is not available yet
		$berita = 'File invoice sudah dibuat';
	}

	if (!file_exists(getcwd().'/'.$invoice->getFolder().'Invoice '.$datainv->inv_id_trans.'.pdf')) {
		$berita ="File invoice Belum terbuat, Kontak admin sistem anda!";
	}
	

	$files = array();
	$emails = array();

	if (@$datas->email_cust!='') {
		array_push($emails, $datas->email_cust);
	}
	if (@$datas->email!='') {
		array_push($emails, $datas->email);
	}


	$dirfiles = scandir('attachments/');
	foreach ($dirfiles as $key) {
		if ($key!='.'&&$key!='..') {
			array_push($files, $key);
			// echo $key;
		}
	}

	$data = array(
		'base_url' => getBaseUrl(), //base data
        'url'=> $url,
        'judul' => 'Input Data Surat/FAC Institute',
        'username'=>$_SESSION['admin']['nama'],
        'page' => 'input-surat', // end base data
        'no_invoice' => $_GET['inv'],
		'nama' => $datas->nama_cust,
		'id_transaksi' => $datainv->inv_id_trans,
		'nama_usaha' => $datas->nama,
		'emails' => $emails,
		'files' => $files,
		'berita'=> $berita
	);
	$data['linkin'] = 'new-preview?inv=';
	$data['linkem'] = 'new-preview?einv=';
	$data = (object) $data;
	include_once 'view/invoice/input-attachments.php';
	// print_r($dirfiles);
	// echo Invoice($data);
}elseif (isset($_GET['inve'])&&!empty($_GET['inve'])) { //edit attachment of invoice

	include_once '../model/Transaksi.php';
	include_once '../model/Invoice.php';
	$transaksi = new Transaksi();
	$invoice = new Invoice();
	$invoice->setNo($_GET['inve']);

	if (isset($_POST['ubh'])) {
		if (empty($_POST['ubh']['email'])) {
			echo "<script>alert('Tujuan email harus dipilih!!')</script>";
		}else{
			$input = array(
				'to' => $_POST['ubh']['email'],
				'cc' => $_POST['ubh']['cc'],
				'attachments' => $invoice->CekLampiran(@$_POST['ubh']['lampiran'])
			);
			$input = (object) $input;
			// print_r($input);
			$invoice->setData($input);
			if ($invoice->EditSurat()) {
				echo "<script>alert('Berhasil mengubah data surat!')</script>";
				echo "<script>location.replace('new-invoice?send=".$_GET['inve']."')</script>";
			} else {
				echo "<script>alert('Data surat gagal disimpan, harap menghubungi admin sistem!')</script>";
			}
		}
	}

	
	$datasurat = $invoice->FetchDataSurat();
	$datainv = $invoice->GetByID();
	
	$transaksi->setID($datainv->inv_id_trans);

	$datas = $transaksi->FetchTransactionForPreview();
	
	//check if pdf file is created
	if(!file_exists($invoice->getFolder().'Invoice '.$datainv->inv_id_trans.'.pdf')){
		include_once '../model/Mypdf.php';
		$pdf = new Mypdf();
		
		if (count($transaksi->FetchItemsForInvoice())=='0') {
			$mylist = array();
			$obj1 = array('nama_item' => 'Tidak ada data yang dipilih','harga'=>'0','qty'=> '0');
			array_push($mylist, (object) $obj1);
		}else{
			$mylist = $transaksi->FetchItemsForInvoice(); 
		}

		$data = array(
			'nama_cust' => $datas->nama_cust,
	        'telepon_cust' => $datas->telp_cust,
    	    'email_cust' =>$datas->email_cust,
			'no_invoice' => $_GET['inve'],
			'nama_usaha' => $datas->nama,
			'alamat' => $datainv->inv_alamat,
			'deskripsi' => $datainv->inv_deskripsi,
			'tanggal' => $datainv->inv_date,
			'metode_bayar' => $invoice->TentukanMetodeBayar($datainv->inv_metode_bayar),
			'items' => $mylist,
			'diskon' => $datas->trans_discount,
			'dp' => $datas->trans_dp 
		);
		$data = (object) $data;
		$pdf->setData($data);
		$pdf->setPath($invoice->getFolder());
		$pdf->setNameFile('Invoice '.$datainv->inv_id_trans.'.pdf');
		//creating file..
		$pdf->Invoice('');
		$berita = 'File invoice sudah dibuat';

	}else{
		$berita = 'File invoice sudah dibuat';
	}
	//end pdf

	//second checking if pdf is successfully created..
	if (!file_exists(getcwd().'/'.$invoice->getFolder().'Invoice '.$datainv->inv_id_trans.'.pdf')) {
		$berita ="File invoice Belum terbuat, Kontak admin sistem anda!";
	}
	//end checking

	$files = array();
	$emails = array();

	if (@$datas->email_cust!='') {
		array_push($emails, $datas->email_cust);
	}
	if (@$datas->email!='') {
		array_push($emails, $datas->email);
	}


	$dirfiles = scandir('attachments/');
	$attachments = explode("##", $datasurat->emd_attachments);

	foreach ($dirfiles as $key) {
		if ($key!='.'&&$key!='..') {
			array_push($files, $key);
			// echo $key;
		}
	}
	//data for view
	$data = array(
		'base_url' => getBaseUrl(), //base data
        'url'=> $url,
        'judul' => 'Input Data Surat/FAC Institute',
        'username'=>$_SESSION['admin']['nama'],
        'page' => 'input-surat', // end base data
        'no_invoice' => $_GET['inve'],
		'nama' => $datas->nama_cust,
		'id_transaksi' => $datainv->inv_id_trans,
		'nama_usaha' => $datas->nama,
		'emails' => $emails,
		'email' => $datasurat->emd_to,
		'cc' => $datasurat->emd_cc,
		'files' => $files,
		'attachments' => $attachments,
		'berita'=> $berita
	);
	$data['linkin'] = 'new-preview?inv=';
	$data['linkem'] = 'new-preview?einv=';
	$data = (object) $data;
	include_once 'view/invoice/edit-attachments.php';

}elseif (isset($_GET['quot'])&&!empty($_GET['quot'])) { //penawaran...
	include_once '../model/Quotation.php';
	$quot = new Quotation();
	$quot->setID($_GET['quot']);

	if ($quot->CountAttachment()!='0') { //check if the transaction has 'data surat' on database or not, else input new
		echo "<script>location.replace('".$url."?quote=".$_GET['quot']."')</script>";	
	}

	if (isset($_POST['in'])) { //check if input data is valid
		if (empty($_POST['in']['email'])) { //user must select the 'email to' data
			echo "<script>alert('Tujuan email harus dipilih!!')</script>";
		}else{ 
			$input = array(
				'to' => $_POST['in']['email'],
				'cc' => $_POST['in']['cc'],
				'attachments' => $quot->CekLampiran($_POST['in']['lampiran'])
			);
			$input = (object) $input;
			// print_r($input);
			$quot->setData($input);
			if ($quot->InputSurat()) { //check if data enter database
				echo "<script>alert('Berhasil menyimpan data surat!')</script>";
				echo "<script>location.replace('new-quotation?send=".$_GET['quot']."')</script>";
			} else {
				echo "<script>alert('Data surat gagal disimpan, harap menghubungi admin sistem!')</script>";
			}
		}
	}


	$dataquot = $quot->DisplayPDF();

	if(!file_exists($quot->getFolder().'Penawaran '.$dataquot->qtn_id_trans.'.pdf')){
		include_once '../model/Mypdf.php';
		
		$pdf = new Mypdf();
		
		$mylist = array();
		
		$obj1 = array('nama_item' => 'Tidak ada data yang dipilih','harga'=>'0','itm_qty'=> '0');
		
		if (count($quot->FetchItems())=='0') {
			$mylist = array();
			array_push($mylist, (object) $obj1);
		}else{
			$mylist = $quot->FetchItems(); 
		}


		$data = array(
			'quot_num' => $_GET['quot'],
			'title' => $dataquot->qtn_title,
			'subject' => $dataquot->qtn_subject,
			'jenistr'=>$dataquot->trans_jenis,
			'nama_usaha' => $dataquot->nama_usaha,
			'customer' => $dataquot->nama_cust,
			'email' => $dataquot->email_cust,
			'items' => $mylist,
			'petugas' => $_SESSION['admin']['nama'] 
		);

		if ($dataquot->qtn_notes=='') {
			$data['notes'] = '';
		} else {
			$data['notes'] = $dataquot->qtn_notes;
		}
		
		$data['tanggal'] = $dataquot->qtn_date;

		$data = (object) $data;
		$pdf->setData($data);
		$pdf->setPath($quot->getFolder());
		$pdf->setNameFile('Penawaran '.$dataquot->qtn_id_trans.'.pdf');
		$pdf->Quotation('');
		$berita = 'File Penawaran sudah dibuat';
	}else{
		$berita = 'File Penawaran sudah dibuat';
	}

	if (!file_exists(getcwd().'/'.$quot->getFolder().'Penawaran '.$dataquot->qtn_id_trans.'.pdf')) {
		$berita ="File Penawaran Belum terbuat, Kontak admin sistem anda!";
	}

	$files = array();
	$emails = array();

	if (@$dataquot->email_cust!='') {
		array_push($emails, $dataquot->email_cust);
	}
	if (@$dataquot->email!='') {
		array_push($emails, $dataquot->email);
	}


	$dirfiles = scandir('attachments/');
	foreach ($dirfiles as $key) {
		if ($key!='.'&&$key!='..') {
			array_push($files, $key);
		}
	}

	$data = array(
		'base_url' => getBaseUrl(), //base data
        'url'=> $url,
        'judul' => 'Input Data Surat/FAC Institute',
        'username'=>$_SESSION['admin']['nama'],
        'page' => 'input-surat', // end base data
        'no_invoice' => $_GET['quot'],
		'nama' => $dataquot->nama_cust,
		'id_transaksi' => $dataquot->qtn_id_trans,
		'nama_usaha' => $dataquot->nama_usaha,
		'emails' => $emails,
		'attachments' => '',
		'files' => $files,
		'berita'=> $berita
	);

	$data['linkin'] = 'new-preview?quot=';
	$data['linkem'] = 'new-preview?equot=';
	$data = (object) $data;
	include_once 'view/invoice/input-attachments.php';


}elseif (isset($_GET['quote'])&&!empty($_GET['quote'])) {
	include_once '../model/Quotation.php';
	$quot = new Quotation();
	$quot->setID($_GET['quote']);

	$dataquot = $quot->DisplayPDF();
	$datasurat = $quot->FetchDataSurat();

	//post data edit
	if (isset($_POST['ubh'])) {
		if (empty($_POST['ubh']['email'])) {
			echo "<script>alert('Tujuan email harus dipilih!!')</script>";
		}else{
			$input = array(
				'to' => $_POST['ubh']['email'],
				'cc' => $_POST['ubh']['cc'],
				'attachments' => $quot->CekLampiran(@$_POST['ubh']['lampiran'])
			);
			$input = (object) $input;
			// print_r($input);
			$quot->setData($input);
			if ($quot->EditSurat()) {
				echo "<script>alert('Berhasil mengubah data surat!')</script>";
				echo "<script>location.replace('new-quotation?send=".$_GET['quote']."')</script>";
			} else {
				echo "<script>alert('Data surat gagal disimpan, harap menghubungi admin sistem!')</script>";
			}
		}
	}
	// end post data edit

	if(!file_exists($quot->getFolder().'Penawaran '.$dataquot->qtn_id_trans.'.pdf')){ //creating pdf
		include_once '../model/Mypdf.php';
		
		$pdf = new Mypdf();
		
		$mylist = array();
		
		$obj1 = array('nama_item' => 'Tidak ada data yang dipilih','harga'=>'0','itm_qty'=> '0');
		
		if (count($quot->FetchItems())=='0') {
			$mylist = array();
			array_push($mylist, (object) $obj1);
		}else{
			$mylist = $quot->FetchItems(); 
		}


		$data = array(
			'quot_num' => $_GET['quote'],
			'title' => $dataquot->qtn_title,
			'subject' => $dataquot->qtn_subject,
			'jenistr'=>$dataquot->trans_jenis,
			'nama_usaha' => $dataquot->nama_usaha,
			'customer' => $dataquot->nama_cust,
			'email' => $dataquot->email_cust,
			'items' => $mylist,
			'petugas' => $_SESSION['admin']['nama'] 
		);

		if ($dataquot->qtn_notes=='') {
			$data['notes'] = '';
		} else {
			$data['notes'] = $dataquot->qtn_notes;
		}
		
		$data['tanggal'] = $dataquot->qtn_date;

		$data = (object) $data;
		$pdf->setData($data);
		$pdf->setPath($quot->getFolder());
		$pdf->setNameFile('Penawaran '.$dataquot->qtn_id_trans.'.pdf');
		$pdf->Quotation('');
	}else{
		$berita = 'File Penawaran sudah dibuat';
	} //end creating pdf

	if (!file_exists(getcwd().'/'.$quot->getFolder().'Penawaran '.$dataquot->qtn_id_trans.'.pdf')) {
		$berita ="File Penawaran Belum terbuat, Kontak admin sistem anda!";
	}else{
		$berita = 'File Penawaran sudah dibuat';
	}

	$attachments = explode("##", $datasurat->emd_attachments);
	// $datasurat = $invoice->FetchDataSurat();
	// echo "Editing quotations ...";
	$files = array();
	$emails = array();

	if (@$dataquot->email_cust!='') {
		array_push($emails, $dataquot->email_cust);
	}
	if (@$dataquot->email!='') {
		array_push($emails, $dataquot->email);
	}

	$dirfiles = scandir('attachments/');

	foreach ($dirfiles as $key) {
		if ($key!='.'&&$key!='..') {
			array_push($files, $key);
			// echo $key;
		}
	}
	//data for view
	$data = array(
		'base_url' => getBaseUrl(), //base data
        'url'=> $url,
        'judul' => 'Input Data Surat/FAC Institute',
        'username'=>$_SESSION['admin']['nama'],
        'page' => 'input-surat', // end base data
        'no_invoice' => $_GET['quote'],
		'nama' => $dataquot->nama_cust,
		'id_transaksi' => $dataquot->qtn_id_trans,
		'nama_usaha' => $dataquot->nama_usaha,
		'emails' => $emails,
		'email' => $datasurat->emd_to,
		'cc' => $datasurat->emd_cc,
		'files' => $files,
		'attachments' => $attachments,
		'berita'=> $berita
	);
	$data['linkin'] = 'new-preview?inv=';
	$data['linkem'] = 'new-preview?einv=';
	$data = (object) $data;
	include_once 'view/invoice/edit-attachments.php';
}


?>