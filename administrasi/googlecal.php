<?php 
include_once '/home/facinsti/public_html/administrasi/controller/googlecal.controller.php';

session_start();
if (isset($_SERVER['PATH_INFO'])) {
	@$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
	$key = array_shift($url_segment);
	switch ($key) {
		case 'sendtoapi':
			if (isset($_GET['tr'])&&!empty($_GET['tr'])) {
				GoogleCalendar::AddNewAgenda($_GET['tr']);
			}
		break;
		case 'summarylist':
			if (isset($_GET['tr'])&&!empty($_GET['tr'])) {
				GoogleCalendar::ListSummary($_GET['tr']);
			}
			break;
		case 'deleteparent':
			if (isset($_POST['id'])&&!empty($_POST['id'])) {
				GoogleCalendar::DeleteData($_POST['id']);
			}
			break;
		case 'updateadmin':
			if (isset($_GET['idcal'])&&!empty($_GET['idcal'])) {
				GoogleCalendar::UpdateAdmin($_GET['idcal']);
			}
			break;
		case 'midware':
			if (isset($_GET['id'])) { // id fac_calendar
				GoogleCalendar::MidwareAPI($_GET['id']);
			}
			break;
		case 'verify':
			if (isset($_GET['id'])) {
				GoogleCalendar::Verifikasi($_GET['id']);
			}
			break;
		case 'updatecalstat':
			if (isset($_POST['id'])&&!empty($_POST['id'])) {
				GoogleCalendar::UpdateStatusCalendar($_POST['id'],'63');
			}
			break;
		case 'sendbackupinvitation':
			if (isset($_GET['id'])) {
				GoogleCalendar::SendingBackupInvitation($_GET['id']);	
			}
			break;
		case 'tes':
			if (isset($_GET['id'])) {
				GoogleCalendar::MidwareAPI($_GET['id']);	
			}
			break;
		case 'newattendee':
			if (isset($_GET['id'])) {
				if (isset($_POST['in'])) {
					GoogleCalendar::NewAttendeePOST($_POST['in'],$_GET['id']);
				} else {
					GoogleCalendar::NewAttendee($_GET['id']);	
				}
			}
			break;
		case 'delattendee':
			if (isset($_GET['id'])) {
				GoogleCalendar::Deleteattendee($_GET['id']);
			}
			break;
		case 'upcoming':
			GoogleCalendar::UpcomingeventAPI();
			break;
		case 'reptraining':
			if (isset($_GET['src'])) {
				if (!empty($_GET['src']['tglawal'])&&!empty($_GET['src']['tglakhir'])) {
					GoogleCalendar::RepeatTrainingData($_GET['src']['tglawal'],$_GET['src']['tglakhir']);
				}else{
					GoogleCalendar::RepeatTrainingData();
				}
			}elseif (isset($_GET['det'])&&!empty($_GET['det'])) {
				GoogleCalendar::GetDetailRepTraining($_GET['det']);
			} else {
				GoogleCalendar::RepeatTrainingData();	
			}
			
			break;
		case 'apirepeattrainer':
		    if(isset($_GET['m'])&&!empty($_GET['m']))
			    GoogleCalendar::GetRepeatTrainer($_GET['m']);
			break;
	}
}else{
	
}