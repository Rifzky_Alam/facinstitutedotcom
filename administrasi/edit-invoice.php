<?php 
session_start();

if (!isset($_SESSION['admin'])||($_SESSION['admin']['username']!='Imelia'&&$_SESSION['admin']['username']!='charlez')){
	header('Location:../index.php');
}
$page='dataPegawai'
?>

<?php 

//include_once 'variables.php';

?>

<!DOCTYPE html>

<html>

<head>
	<title>FAC - Edit Invoice Status</title>
<?php include_once 'header.php'; ?>

</head>

<body>
	<?php include_once 'sidebar.php'; ?>
	<?php include_once 'top-nav.php'; ?>

	<?php 
	include_once '../model/Pendaftar.php';
	$pendaftar = new Pendaftar();

	if (isset($_POST['sent'])&&!empty($_POST['sent'])) {
		if ($pendaftar->updateStatusInvoice($_POST['sent'],'1')) {
            echo "<script>alert('Status invoice no. ".$_POST['sent']." berhasil diubah menjadi terkirim!!')</script>";
            echo "<script>location.replace('daftar-invoice?iv=".$_POST['sent']."')</script>";
        } else {
            echo "<script>alert('Status invoice gagal diubah, Kontak admin sistem!');</script>";
        }
		
	}elseif (isset($_POST['notsent'])&&!empty($_POST['notsent'])) {
		if ($pendaftar->updateStatusInvoice($_POST['notsent'],'0')) {
            echo "<script>alert('Status invoice no. ".$_POST['notsent']." berhasil diubah menjadi belum terkirim!!')</script>";
            echo "<script>location.replace('daftar-invoice?iv=".$_POST['notsent']."')</script>";
        } else {
            echo "<script>alert('Status invoice gagal diubah, Kontak admin sistem!');</script>";
        }
	}elseif (isset($_POST['paid'])&&!empty($_POST['paid'])) {
		if ($pendaftar->updateStatusInvoice($_POST['paid'],'2')) {
            echo "<script>alert('Status invoice no. ".$_POST['paid']." berhasil diubah menjadi terbayar!!')</script>";
            echo "<script>location.replace('daftar-invoice?iv=".$_POST['paid']."')</script>";
        } else {
            echo "<script>alert('Status invoice gagal diubah, Kontak admin sistem!');</script>";
        }
	}

	?>
	


	<div class="container" style='margin-bottom:40px;margin-top:40px'>

			<div class='row'>
				<div class='page-header'>					
	            	<h1>Edit Status Invoice</h1>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="form-inline">
						<form action="" method="GET">
							<input type="text" name="iv" class="form-control" placeholder="Masukkan nomor invoice contoh: FAC/W/2017/01/77" style="width:50%">
							<button class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> Search</button>
						</form>
					</div>
				</div>
			</div>
			<br>
		
			<div class='row'>
				<div class='col-md-12'>
					<?php //print_r($datas) ?>

					<table class="table table-hover">
						<thead>
							<tr>
								<th>No Invoice</th>
								<th>Nama Perusahaan</th>
								<th>Nama Kontak</th>
								<th>Nama Paket</th>
								<th>Deskripsi</th>
								<th>Jumlah</th>
								<th>Tanggal Kirim</th>
							</tr>
						</thead>
						<tbody>
							<?php if (isset($_GET['iv'])&&!empty($_GET['iv'])): ?>
								<?php $datas=$pendaftar->getPendingInvoice($_GET['iv']); ?>
							
							<?php foreach ($datas as $key ) { ?>
								<tr>
									<td><?php echo $key->no_invoice ?></td>
									<td><?php echo $key->nama ?></td>
									<td><?php echo $key->nama_personal ?></td>
									<td><?php echo $key->nama_item ?></td>
									<td><?php echo $key->deskripsi ?></td>
									<?php 
                        			$myAmount = intval($key->harga) * intval($key->qty); 
                        			$transport = intval($key->biaya_trans) * intval($key->qty_trans);
                        			?>
                        			<td><?php echo number_format( $myAmount + $transport ) ?></td>
                        			<td><?php echo $key->sent_date ?></td>
								</tr>
							<?php } ?>
							
						</tbody>
					</table><br><br>
					<h4>Ganti Status Invoice</h4>
					<div class="col-md-4">
						<form action="" method="post">
							<input type="text" name="sent" value="<?php echo $_GET['iv'] ?>" style="display:none;">
							<button style="width:100%" class="btn btn-lg btn-warning">Terkirim</button>
						</form>
					</div>
					<div class="col-md-4">
						<form action="" method="post">
							<input type="text" name="notsent" value="<?php echo $_GET['iv'] ?>" style="display:none;">
							<button style="width:100%" class="btn btn-lg btn-danger">Belum Terkirim</button>
						</form>
					</div>
					<div class="col-md-4">
						<form action="" method="post">
							<input type="text" name="paid" value="<?php echo $_GET['iv'] ?>" style="display:none;">
							<button style="width:100%" class="btn btn-lg btn-success">Terbayar</button>
						</form>
					</div>
					<?php endif ?>
				</div>
			</div>

		
		
	</div>

	     

</body>

</html>