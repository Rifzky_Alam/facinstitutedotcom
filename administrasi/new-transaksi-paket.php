<?php 
session_start();
include_once 'session/session-class.php';
include_once '../baseurl.php';
include_once '../model/Transaksi.php';
include_once '../model/Pendaftar.php';
$session = new Sessionz();
$session->AdminMarketing();
$transaksi = new Transaksi();
$page = 'new-transaksi-paket';



if (isset($_GET['tr'])&&!empty($_GET['tr'])) {
	if (isset($_POST['in'])) {
		$transaksi->setID($_GET['tr']);
		$dataInput = array('paket' => $_POST['in']['paket'],'qty'=> $_POST['in']['qty']);
		$transaksi->setItems((object)$dataInput);
		if ($transaksi->InputNewItem()){
			echo "<script>alert('Item transaksi berhasil disimpan!');</script>";
			echo "<script>location.replace('new-transaksi-paket?tr=".$_GET['tr']."');</script>";
		}else{
			echo "<script>alert('Item transaksi gagal disimpan!');</script>";
		}
	}

	$no_transaksi = $_GET['tr'];
	$transaksi->setID($_GET['tr']);
	//data description
	$data = array(
	    'base_url' => getBaseUrl(),
	    'judul' => 'Input Item Transaksi - FAC Institute',
	    'username'=>$_SESSION['admin']['nama'],
	    'page' => $page,
	    'no_transaksi' => $no_transaksi,
	    'paket' => $transaksi->FetchPaketTraining(),
	    'items'=> $transaksi->FetchItems()
	);

	$data = (object) $data;

	include_once 'view/transaksi/tambah-item.php';

}elseif (isset($_GET['del'])&&!empty($_GET['del'])) {
	$transaksi->setItems($_GET['del']);
	$datadelete= $transaksi->FetchItemsByID();

	if (isset($_POST['nomorid'])) {
		$transaksi->setItems($_POST['nomorid']);
		if ($transaksi->DeleteItem()) {
			echo "<script>alert('Item berhasil dihapus!');</script>";
		    echo "<script>location.replace('new-perusahaan-info?tr=$datadelete->itm_trans_id');</script>";
		} else {
			echo "<script>alert('Item gagal dihapus!');</script>";
		}
	}



	
	$data = array(
	    'base_url' => getBaseUrl(),
	    'judul' => 'Hapus Item Transaksi - FAC Institute',
	    'username'=>$_SESSION['admin']['nama'],
	    'page' => $page,
	    'no_transaksi'=>$datadelete->itm_trans_id,
	    'id_item'=>$datadelete->no,
	    'item' => $datadelete->nama_item,
	    'price'=>$datadelete->harga,
	    'qty' => $datadelete->itm_qty
	);

	$data = (object) $data;

	include_once 'view/transaksi/delete-paket.php';
}else{
	echo "<script>alert('Harap memilih data transaksi terlebih dahulu!');</script>";
    echo "<script>location.replace('data-order');</script>";
    
    $no_transaksi = "-";
}


?>