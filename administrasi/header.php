<?php 
function getBaseUrl(){
  $currentPath = $_SERVER['PHP_SELF'];$pathInfo = pathinfo($currentPath);$hostName = $_SERVER['HTTP_HOST'];$protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https://'?'https://':'http://';
  return 'https://'.$hostName."/";
}
?>


<head>
<meta charset='utf-8'>
<meta name="viewport" content="width=device-width, initial-scale=1">


	<?php if (isset($judul)&&!empty($judul)): ?>
			<title><?php echo $judul ?></title>
		<?php else: ?>
			<title>FAC Institute</title>
	<?php endif ?>
  
  <link rel="stylesheet" type="text/css" href=<?php echo "'".getBaseUrl()."css/bootstrap.css'"; ?>>
  <link rel="stylesheet" type="text/css" href=<?php echo "'".getBaseUrl()."css/administrasi-index.css'"; ?>>

  <?php if ($page=='laporan'): ?>
  <link rel="stylesheet" type="text/css" href=<?php echo "'".getBaseUrl()."css/laporan.css'"; ?>>  	
  <link rel="stylesheet" type="text/css" href=<?php echo "'".getBaseUrl()."css/multiselect/css/bootstrap-multiselect.css'"; ?>> 
  <?php endif ?>

  <?php if ($page=='coba'): ?>
  <link rel="stylesheet" type="text/css" href=<?php echo "'".getBaseUrl()."css/agenda.css'"; ?>>  		
  <?php endif ?>

  <?php if ($page=='dataLaporan'): ?>
  <link rel="stylesheet" type="text/css" href=<?php echo "'".getBaseUrl()."css/data-laporan.css'"; ?>> 
  <?php endif ?>

  <?php if ($page=='daftarHadir'||$page=='orderNew'||$page=='editLaporan'||$page=='dataLaporan'||$page=='laporan'||$page=='orderTraining'||$page=='absensi'||$page=='dataOrderTraining'||$page=='calendar'||$page=='laporan-followup'): ?>
  <link rel="stylesheet" type="text/css" href=<?php echo "'".getBaseUrl()."js/jquery-datepicker/jquery-ui.min.css'"; ?>>  
  <?php endif ?>

  <?php if ($page=='absensi'): ?>
  	<link rel="stylesheet" type="text/css" href="<?php echo getBaseUrl().'css/profilecard/style.css' ?>">
  <?php endif ?>


  <?php if ($page=='daftarHadir'): ?>
  	<link rel="stylesheet" type="text/css" href=<?php echo "'".getBaseUrl()."css/multiselect/css/bootstrap-multiselect.css'"; ?>>
  <?php endif ?>

  <?php if ($page=='dataPegawai'||$page=='penawaran'): ?>
  	<link rel="stylesheet" type="text/css" href=<?php echo "'".getBaseUrl()."css/datepicker/css/datepicker.css'"; ?>> 
  	
  <?php endif ?>

  <?php if ($page=='orderTraining'||$page=='orderNew'): ?>
  	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCUn6Q6sg0JVJBlgiz4tL2kF1zmVoC0ScE&libraries=places&sensor=true"></script>
  <?php endif ?>

  <?php if ($page=='orderNew'): ?>
  	<link rel="stylesheet" type="text/css" href=<?php echo "'".getBaseUrl()."css/style-order.css'"; ?>>
 
  <?php endif ?>

  <?php if ($page=='upload'): ?>
  		<link href=<?php echo "'".getBaseUrl()."assets/upload/css/fileinput.css'"; ?> media="all" rel="stylesheet" type="text/css" />
  		
  <?php endif ?>

	<script type="text/javascript" src=<?php echo "'".getBaseUrl()."js/jquery.js'"; ?>></script>
	<script type="text/javascript" src=<?php echo "'".getBaseUrl()."js/bootstrap.min.js'"; ?>></script>

	<?php if ($page=='upload'): ?>
		<script src=<?php echo "'".getBaseUrl()."assets/upload/js/fileinput.js'"; ?> type="text/javascript"></script>
        <script src=<?php echo "'".getBaseUrl()."assets/upload/js/fileinput_locale_fr.js'"; ?> type="text/javascript"></script>
        <script src=<?php echo "'".getBaseUrl()."assets/upload/js/fileinput_locale_es.js'"; ?> type="text/javascript"></script>
	<?php endif ?>

	<?php if ($page=='daftarHadir'): ?>
		<script type="text/javascript" src=<?php echo "'".getBaseUrl()."css/multiselect/js/bootstrap-multiselect.js'" ?>></script>
	<?php endif ?>

	<?php if ($page=='orderNew'): ?>
		<script type="text/javascript" src=<?php echo "'".getBaseUrl()."js/order.js'"; ?>></script>
	<?php endif ?>

	<script>
	function openNav() {
	    document.getElementById("mySidenav").style.width = "250px";
	}

	function closeNav() {
	    document.getElementById("mySidenav").style.width = "0";
	}
	</script>

	<?php 
		if ($page=='absensi'||$page=='daftarHadir') {
	?>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCUn6Q6sg0JVJBlgiz4tL2kF1zmVoC0ScE&libraries=places"></script>
	<?php		
		}
	?>



	<style type="text/css">
		.tengah{
			text-align: center;
		}
		#isi{
			margin-top:0px;
			margin-bottom:40px;
		}
		.geser{
			padding-left: 25px;
			width: 100%;
		}
	</style>

</head>