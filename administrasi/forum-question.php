<?php 
session_start();
if (!isset($_SESSION['admin'])||($_SESSION['admin']['tipe']!='admin'&&$_SESSION['admin']['tipe']!='marketing'&&$_SESSION['admin']['tipe']!='staff')) {
    header('Location:../login/index.php');
}
?>

<!DOCTYPE html>
<html>

<?php 
//echo $tipeAdmin=$_SESSION['admin']['tipe'];
$judul='Forum FAC';
$page = 'home';
include_once 'header.php';
include_once '../model/Forum.php';
include_once '../model/page.php';

$myPage = new Page();
$forum = new Forum();
?>

<body>

<?php 

include_once 'sidebar.php';

include_once 'top-nav.php';

 ?>

<div class="container" id="isi">
	
<?php if (isset($_GET['v'])&&!empty($_GET['v'])): ?>
    <?php $forum->setID($_GET['v']); ?>
    <?php $data=$forum->selectById() ?>

    <?php 
    if (isset($_POST['ij'])) {
        $inputData = array(
            'id_question' => $_GET['v'],
            'jawaban'=> $_POST['ij']['jawaban'],
            'petugas'=> $_SESSION['admin']['username']
        );
        $forum->setAnswer((object)$inputData);
        
        if ($forum->addAnswer()) {
            echo "<script>alert('Terimakasih atas jawaban anda!');</script>";
            echo "<script>location.replace('".$_SERVER['SCRIPT_NAME']."?v=".$_GET['v']."');</script>";
        } else {
            echo "<script>alert('Jawaban belum bisa tersimpan, bila ada masalah harap hubungi amin system!');</script>";
            echo "<script>location.replace('".$_SERVER['SCRIPT_NAME']."?v=".$_GET['v']."');</script>";
        }
        // print_r($forum->getAnswer());
    }

    if (isset($_POST['idAnswer'])){
        $inputData = array(
            'id_question' => $_POST['idAnswer'],
            'petugas' => $_SESSION['admin']['username']
        );
        $forum->setVote((object) $inputData);

        if ($forum->addVote()) {
            echo "<script>location.replace('".$_SERVER['SCRIPT_NAME']."?v=".$_GET['v']."');</script>";
        }

    }    

    ?>

    
    <div class="page-header" id="top-logo">
		<h3><?php echo $data->title ?></h3>
	</div>

    <div class="row">
        <div class="col-md-10">
            <?php echo $data->question; ?>

            <div>
                <?php $mylist = explode(",", $data->tag); ?>
                <ul class="pagination pagination-sm" style="margin-bottom:0px;">
                <?php for ($i=0; $i < count($mylist); $i++) { ?>
                    <li><a href='#'><?php echo trim($mylist[$i]); ?></a></li>
                <?php } ?>          
                </ul>
            </div>
            <div style="text-align:right;">
                <?php echo $data->nama ?> || <span><?php echo $data->time_post ?></span>
            </div>           
        </div>
    </div>

    <?php 
        $forum->setAnswer($_GET['v']);
        $answerData = $forum->getAllAnswer();
    ?>


    <div class="row">
        <div class="col-md-10">
            <div class="page-header">
                <h5><?php echo $forum->getJumlahJawaban(); ?> Jawaban</h5>
            </div>
        </div>
    </div>

    <?php foreach ($answerData as $key){ ?>
    
    <div class="row">
        <div class="col-md-10">
            <section class="col-md-1">
                <?php $forum->setAnswer($key->id); ?>
                <h4 style="text-align: center;"><?php echo $forum->getJumlahVote(); ?></h4>
                <form action="" method="POST">
                    <input type="text" name="idAnswer" value="<?php echo $key->id ?>" style="display:none;">
                    <button class="btn btn-sm btn-primary">vote!</button>
                </form>
                <?php if ($key->user==$_SESSION['admin']['username']): ?>
                    <br>
                    <a href="forum-edit?idq=<?php echo $key->id ?>" id="<?php echo $key->id ?>" class="btn btn-sm btn-warning">Edit</a>
                <?php endif ?>
            </section>
            <section class="col-md-11">
                <?php echo $key->answer; ?>
                <br>
            <div style="text-align:right;">
                <?php echo $key->nama; ?> || <span><?php echo $key->time_replied; ?></span>
            </div>
            </section>        
        </div>
    </div>

    <div class="row">
        <div class="col-md-10">
            <hr>
        </div>
    </div>
    <br>

    <?php } ?>

    <br>
    <div id="forEdit" class="row">
        
    </div>

    <form action="" method="POST">
    <div class="row" style="overflow:auto;">
        <div class="col-md-10">
            <div class="form-group">
                <label>Jawaban anda</label>
                <textarea name="ij[jawaban]" class="form-control" id="editor1"></textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10">
            <button class="btn btn-lg btn-primary">Submit</button>
        </div>
    </div>
    </form>
<?php endif ?>
        

	
</div>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
    <script type="text/javascript">
        CKEDITOR.replace('editor1');
    </script>
    <!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/58be3a2b93cfd3557204996c/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
</body>
</html> 