<?php 
include_once 'controller/adminfile.controller.php';
session_start();
if (isset($_SERVER['PATH_INFO'])) {
    @$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
    $key = array_shift($url_segment);
    switch ($key) {
        case '':
            header('Location: https://fac-institute.com/administrasi/upload-files');
            break;
        case 'uploadnew':
            Adminfile::UploadAttachment();
            break;
        case 'delete':
            if (!isset($url_segment[0])) {
                header('Location: https://fac-institute.com/administrasi/');   
            }
            Adminfile::DeleteAttachment($url_segment[0]);
            break;
        case '/':
            header('Location: https://fac-institute.com/administrasi/upload-files');
            break;
    }
}else{
    Adminfile::ListAdminAttachments();
}



