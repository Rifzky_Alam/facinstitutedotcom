<?php 
session_start();
if (!isset($_SESSION['admin'])||($_SESSION['admin']['tipe']!='admin'&&$_SESSION['admin']['tipe']!='marketing')) {
    header('Location:../login/index.php');
}
?>
<!DOCTYPE html>
<html>


<?php 
$judul='Form Laporan Follow-up :: FAC';
$page = 'laporan-followup';
include_once 'header.php'; 
include_once '../model/Pendaftar.php'; 
$pendaftar = new Pendaftar();

?>

<body>



<?php 

include_once 'sidebar.php';

include_once 'top-nav.php';
include_once '../model/Calendar.php';
include_once '../model/Petugas.php';
$petugas = new ControlPetugas();
$cal = new Calender();

function getValue($value){
    if ($value=='') {
        return '';
    } else {
        return $value;
    }   
}

function getLampiran($value){
    if (empty($value)){
        return '';
    } else {
        return implode(' # ', $value);
    }
}


 ?>


<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3>Laporan Follow-Up Marketing</h3>
	</div>


    <?php if (isset($_GET['ac'])&&$_GET['ac']=='edit'&&isset($_GET['n'])&&!empty($_GET['n'])): ?>

    <?php if (isset($_POST['in'])): ?>
        <div class="row">
            <?php 
                // echo $_POST['in']['tanggal']."<br>";
                // echo $_POST['in']['agenda']."<br>";
                // echo $_POST['in']['status']."<br>";
                // echo $_POST['in']['jumlahhari']."<br>";
                // echo $_POST['in']['deskripsitanggal']."<br>";
                // echo $_SESSION['admin']['username']."<br>";
                $obj = array(
                    'id'=> $_GET['n'],
                    'tanggal' => $_POST['in']['tanggal'],
                    'customer' => $_POST['in']['customer'],
                    'agenda' => $_POST['in']['agenda'],
                    'status' => $_POST['in']['status'],
                    'jumlahhari' => $_POST['in']['jumlahhari'],
                    'deskripsihari' => $_POST['in']['deskripsitanggal'],
                    'petugas' => $_SESSION['admin']['username']
                );
                $objek = (object) $obj;
                if ($petugas->editFollowUp($objek)) {
                    echo "<script>alert('Data berhasil disimpan!');</script>";
                    echo "<script>location.replace('table?ac=lfm');</script>";
                                       
                }else {
                    echo "<script>alert('Data gagal disimpan, kontak administrator!');</script>";
                }

            ?>
        </div>
    <?php endif ?>



    <?php 
    $data = $petugas->fetchLaporanFuByID($_GET['n']);
    ?>

    <form action="" method="post">
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Tanggal</label>
                        <input type="text" name="in[tanggal]" class="form-control" required id="tanggal-lapor" value="<?= $data->tanggal ?>">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Customer</label>
                        <input value="<?= $data->customer ?>" type="text" name="in[customer]" class="form-control" required maxlength="75">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Agenda</label>
                        <input value="<?= $data->agenda ?>" type="text" name="in[agenda]" class="form-control" required>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Status</label>
                        <select name="in[status]" class="form-control" required>
                            <?php if ($data->status=='1'): ?>
                                <option value="1" selected>Tunggu Konfirmasi</option>    
                                <?php else: ?>
                                <option value="1">Tunggu Konfirmasi</option>
                            <?php endif ?>

                            <?php if ($data->status=='2'): ?>
                                <option value="2" selected>Pre-order</option>    
                                <?php else: ?>
                                <option value="2">Pre-order</option>
                            <?php endif ?>

                            <?php if ($data->status=='3'): ?>
                                <option value="3" selected>Finishing</option>    
                                <?php else: ?>
                                <option value="3">Finishing</option>
                            <?php endif ?>
                            
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Jumlah Hari</label>
                        <input value="<?= $data->jumlah_hari ?>" type="text" name="in[jumlahhari]" class="form-control" id="jmlhari" maxlength="3">
                    </div>
                </div>
            </div>
                <div class="row">
                    <div class="col-md-10">
                        <label>Deskripsi jumlah hari (dalam tanggal)</label>
                        <input value="<?= $data->deskripsi_hari ?>" type="text" name="in[deskripsitanggal]" class="form-control" placeholder="contoh 20/03/2017,21/03/2017,22/03/2017">
                    </div>
                </div>
            <br>
            
            <div class="row">
                <div class="col-md-10">
                    <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>
                </div>
            </div>

        </form>        



        <?php else: ?>

    <?php if (isset($_POST['in'])): ?>
        <div class="row">
            <?php 
                // echo $_POST['in']['tanggal']."<br>";
                // echo $_POST['in']['agenda']."<br>";
                // echo $_POST['in']['status']."<br>";
                // echo $_POST['in']['jumlahhari']."<br>";
                // echo $_POST['in']['deskripsitanggal']."<br>";
                // echo $_SESSION['admin']['username']."<br>";
                $obj = array(
                    'tanggal' => $_POST['in']['tanggal'],
                    'customer' => $_POST['in']['customer'],
                    'agenda' => $_POST['in']['agenda'],
                    'status' => $_POST['in']['status'],
                    'jumlahhari' => $_POST['in']['jumlahhari'],
                    'deskripsihari' => $_POST['in']['deskripsitanggal'],
                    'petugas' => $_SESSION['admin']['username']
                );
                $objek = (object) $obj;
                if ($petugas->InputLaporanFolMarketing($objek)) {
                    echo "<script>alert('Data berhasil disimpan!');</script>";
                    echo "<script>location.replace('table?ac=lfm');</script>";                   
                }else {
                    echo "<script>alert('Data gagal disimpan, kontak administrator!');</script>";
                }

            ?>
        </div>
    <?php endif ?>

    <form action="" method="post">
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Tanggal follow-up</label>
                        <input type="text" name="in[tanggal]" class="form-control" required id="tanggal-lapor">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Customer</label>
                        <input type="text" name="in[customer]" class="form-control" required maxlength="75">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Agenda</label>
                        <input type="text" name="in[agenda]" class="form-control" required>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Status</label>
                        <select name="in[status]" class="form-control" required>
                            <option value="">--Pilih Status--</option>
                            <option value="1">Tunggu Konfirmasi</option>
                            <option value="2">Pre-order</option>
                            <option value="3">Finishing</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Jumlah Hari</label>
                        <input type="text" name="in[jumlahhari]" class="form-control" id="jmlhari" maxlength="3">
                    </div>
                </div>
            </div>
                <div class="row">
                    <div class="col-md-10">
                        <label>Deskripsi jumlah hari (dalam tanggal)</label>
                        <input type="text" name="in[deskripsitanggal]" class="form-control" placeholder="contoh 20/03/2017,21/03/2017,22/03/2017">
                    </div>
                </div>
            <br>
            
            <div class="row">
                <div class="col-md-10">
                    <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>
                </div>
            </div>

        </form>


    <?php endif ?>

    
</div>


  <script type="text/javascript" src=<?php echo "'".getBaseUrl()."js/jquery-ui/jquery-ui.js'"; ?>></script>
  <script type="text/javascript" src=<?php echo "'".getBaseUrl()."assets/jqnumber/jquery.number.js'"; ?>></script>  
  <script type="text/javascript">
    $(document).ready(function(){
        $('#jmlhari').number( true, 0 );
        $('#tanggal-lapor').datepicker({
            dateFormat: 'yy-mm-dd'
        });
    });
  </script>   
</body>
</html> 