<?php 
session_start();
if (!isset($_SESSION['admin'])||($_SESSION['admin']['tipe']!='admin'&&$_SESSION['admin']['tipe']!='marketing')) {
    header('Location:../login/index.php');
}
?>




<!DOCTYPE html>
<html>

<?php 
//echo $tipeAdmin=$_SESSION['admin']['tipe'];
$page = 'upload';
include_once 'header.php'; 

?>

<body>



<?php 

include_once 'sidebar.php';

include_once 'top-nav.php';

include_once '../model/page.php';
include_once '../model/File.php';

$page = new Page();
$file = new File();

 ?>
<?php 
if (isset($_POST['upload'])&&!empty($_POST['upload']['folder'])) {
    if (isset($_FILES['cobaz'])) {
    $filetype = pathinfo($_FILES['cobaz']['name'],PATHINFO_EXTENSION);
    $file->setFolder("/home/facinsti/public_html/external/attachments/".$_POST['upload']['folder']);
    $file->setNamafile($_FILES['cobaz']['name']);
    // $file->setFolder('attachments/');
    $file->setUkuran($_FILES['cobaz']['size']);
    $file->setEkstensi($filetype);
    $file->setTemp($_FILES['cobaz']['tmp_name']);

    if (!file_exists($file->getFolder().$file->getNamafile())) {

        if ($filetype=='xls'||$filetype=='pdf'||$filetype=='xlsx') {
            echo $file->uploadExternal($_POST['upload']['folder']);
        } else {
            echo "<script>alert('Hanya File PDF, xls, xlsx yang dapat di upload!')</script>";
        }

    }else{
            echo "<script>alert('Nama file sudah ada!')</script>";
    }

    }

}

?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3>Upload Files Eksternal</h3>
	</div>
    
	<div class="row">
     <form id="my-form" enctype="multipart/form-data" method="post">
         
         <div class="row">
             <div class="col-md-10">
                 <label>Folder</label>
                 <div class="radio">
                    <label><input type="radio" name="upload[folder]" value="off/" required>Offline</label>
                </div>
                <div class="radio">
                    <label><input type="radio" name="upload[folder]" value="on/">Online</label>
                </div>
             </div>
         </div>

         <div class="row">
             <div class="col-md-10">
                <div class="form-group">
                    <label>File yang akan di upload</label>
                    <input id="file-3" type="file" class="file" name="cobaz">
                </div>  
             </div>
         </div>         
         <hr>

     </form>   
    </div>

</div><!--end container-->
</body>
</html> 