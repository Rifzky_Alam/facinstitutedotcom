<?php 
include_once '/home/facinsti/public_html/administrasi/controller/user.controller.php';

session_start();
if (isset($_SERVER['PATH_INFO'])) {
	@$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
	$key = array_shift($url_segment);
	switch ($key) {
		case 'sendnotifdasbor':
			User::sendnotiftouserdashboard();
			break;
		case 'updatemap':
			User::UpdateMap();
			break;
		case 'new':
			if (isset($_POST['in'])) {
				User::NewUserPost($_POST['in']);
			} else {
				User::NewUser();	
			}
			break;
		case 'lists':
				User::ListUsers();
			break;
		case 'detail':
			if(count($url_segment)==1)
		        User::DetailUser($url_segment[0]);
		    break;
// 			break;
// 		case 'lists':
// 			User::tesfungsi();
// 			break;
		case 'validate':
			if (isset($_GET['usr'])&&!empty($_GET['usr'])) {
				User::ValidateUserByAdmin($_GET['usr']);	
			}
			break;
		case 'reimlist':
		    User::ListReimburse();
		    break;
		case 'reimupld':
            // upload bukti reimbursement
		    if(isset($_GET['id'])&&!empty($_GET['id']))
		        User::UploadBuktiReim($_GET['id']);
		    elseif(isset($_GET['rmv'])&&!empty($_GET['rmv']))
		        User::RemoveReimburseImg($_GET['rmv']);
		    break;
		case 'reimapv':
		    // approval reimbursement
		    if(isset($_GET['id'])&&!empty($_GET['id'])){
		    	if (isset($_GET['index'])&&!empty($_GET['index'])) {
		    		User::PersetujuanReimbursement($_GET['id'],$_GET['index']);
		    	}else{
		    		User::PersetujuanReimbursement($_GET['id']);
		    	}
		        
		    }
		    break;
		case 'reimcomp': 
		    // compare reimbursement dengan data yang di submit user
		    
		    break;
		case 'reimbport':
		    //portal submit & go to the next one
		    if(isset($_GET['bln'],$_GET['thn'],$_GET['user'])){
		        if(!empty($_GET['bln'])&&!empty($_GET['thn'])&&!empty($_GET['user'])){
		        	if (isset($_GET['idx'])) {
		        		 User::PortReimburse($_GET['user'],$_GET['bln'],$_GET['thn'],$_GET['idx']);
		        	}else{
		        		User::PortReimburse($_GET['user'],$_GET['bln'],$_GET['thn']);	
		        	}
		            
		        }
		    }
		    break;
		case 'reimcknex':
		    
		    break;
		case 'laporan-harian':
			if (isset($_SESSION['admin']['username'])&&!empty($_SESSION['admin']['username'])) {
				if (isset($_GET['src'])) {
					User::LaporanHarian($_GET['src']['bln'],$_GET['src']['thn']);
				}elseif(isset($_GET['id'])){
					if (isset($_POST['in'])) {
						User::EditLaporanHarianPost($_GET['id'],$_POST['in']);// print_r($_POST['in']);
					}elseif (isset($_POST['editAbsen'])) {
						User::EditNamaPerusahaan($_GET['id'],$_POST['editAbsen']);
					}else{
						User::EditLaporanHarian($_GET['id']);	
					}				
				}elseif (isset($_GET['summary'])&&!empty($_GET['summary'])) {
					User::CreatingSummary($_GET['summary']);
				}elseif (isset($_GET['act'])&&$_GET['act']=='unr') {
					User::DataLaporanBelumLengkap($_SESSION['admin']['username']);
				}elseif (isset($_GET['inst'])&&!empty($_GET['inst'])) {
					User::GetSummary($_GET['inst']);
				}elseif (isset($_GET['detin'])&&!empty($_GET['detin'])) {
					if (isset($_POST['in'])) {
					    // kalkulasi manual trainer
						User::UpdateInsentif($_GET['detin']);	
					} else {
						User::Detailinsentif($_GET['detin'],$_SESSION['admin']['username']);	
					}
				}elseif (isset($_GET['insdet'])&&!empty($_GET['insdet'])) {
					if (isset($_SESSION['admin']['username'])&&$_SESSION['admin']['tipe']=='admin') {
						if (isset($_POST['in'])) {
						    // kalkulasi manual admin
							User::UpdateInsentif($_GET['insdet']);	
						}else{
							User::Detailinsentifadmin($_GET['insdet']);
						}	
						
					}else{
						header('location:https://fac-institute.com/administrasi/accessdenied');
					}
				}else{
					User::LaporanHarian();
				}	
			} else {
				header('location:https://fac-institute.com/administrasi/accessdenied');
			}
			break;
		case 'laporan':
			if (!isset($_SESSION['admin']['username'])&&$_SESSION['admin']['tipe']!='admin') {
				header('location:https://fac-institute.com/administrasi');	
			}elseif (isset($_GET['req'])&&$_GET['req']=='medreim') {
				User::APIBiayaReimbursement($_SESSION['admin']['username']);
			} else {
				User::DataLaporanHarian();
			}
			break;
		case 'api':
		    if(isset($_GET['usrdet'])&&!empty($_GET['usrdet']))
		        User::DetailUserAPI($_GET['usrdet']);
		    elseif(isset($_GET['req'])&&$_GET['req']=='allpic')
		        User::UserPicAPI();
		    break;
		case '':
			header('location:https://fac-institute.com/administrasi');
			break;
// 		default:
// 			header('Location:https://fac-institute.com/error/404/');
// 		break;
	}
}else{
	header('Location:https://fac-institute.com/administrasi');
}