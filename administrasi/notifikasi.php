<?php 
include_once '/home/facinsti/public_html/administrasi/controller/notifikasi.controller.php';

session_start();
if (isset($_SERVER['PATH_INFO'])) {
	@$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
	$key = array_shift($url_segment);
	switch ($key) {
		case 'api':
			if (isset($_GET['req'])&&$_GET['req']=='list') {
				Notifikasi::NotificationAPI(@$_SESSION['admin']['username']);
			}elseif (isset($_GET['req'])&&$_GET['req']=='tes') {
				Notifikasi::tesinputdata();
			}

			// if (isset($url_segment[0])&&$url_segment[0]=='changestatus') {
				// Notifikasi::UpdateStatus();	
			// }
			break;
		case 'markasread':
		    if(isset($_GET['id'])&&!empty($_GET['id']))
		        Notifikasi::MarkAsRead($_GET['id']);
		    break;
		case 'markasreadall':
		    if(isset($_POST['usr'])&&!empty($_POST['usr'])){
		        Notifikasi::MarkAllAsRead($_POST['usr']);
		    }
		    break;
		case 'history':
		    Notifikasi::NotifHistory($_SESSION['admin']['username']);
		    break;
		case 'input-io':
			if (isset($_POST['in'])) {
				Notifikasi::InputNew($_POST['in']);
			}
			break;
		case 'edit-io':
			if (isset($_POST['in'])) {
				Notifikasi::UpdateStatus($_POST['in']);
			}
			break;
		case 'delete-io':
			if (isset($_POST['in'])) {
				Notifikasi::DeleteNotif($_POST['in']);
			}
			break;
		case '':
			header('location:https://fac-institute.com/administrasi');
			break;
	}
}else{
	header('Location:https://fac-institute.com/administrasi');
}