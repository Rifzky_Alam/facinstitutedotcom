<?php 
session_start();

if (!isset($_SESSION['admin'])||!@$_SESSION['admin']['type']=='admin'){
	if (!$_SESSION['admin']['tipe']=='staff') {
		header('Location:../index.php');
	}
}

?>

<?php 

include_once '../../baseurl.php';

?>

<!DOCTYPE html>

<html>

<head>
	<title>Editor - Tutorial</title>
<?php include_once 'header.php'; ?>

</head>

<body>
	<?php include_once '../sidebar.php'; ?>
	<?php include_once '../top-nav.php'; ?>
	<?php include_once '../../model/Artikel.php'; ?>

	<?php 
	$ctrlArtikel = new Artikel();
	if (isset($_GET['page'])&&!empty($_GET['page'])){
		$datas = json_decode($ctrlArtikel->fetchAll($_GET['page']));
	}elseif (isset($_GET['search'])&&!empty($_GET['search'])){
		$datas = $ctrlArtikel->searchTitle($_GET['search']);
	} else {
		$datas = json_decode($ctrlArtikel->fetchAll(0));	
	}
	
	
		
		if (isset($_POST['delete-id'])&&!empty($_POST['delete-id'])) {
			echo $_POST['delete-id'];
			$dataOneRow = $ctrlArtikel->selectById($_POST['delete-id']);
			if(unlink('images/'.$dataOneRow->image_url)){
				if ($ctrlArtikel->deleteArtikel($_POST['delete-id'])) {

					echo "<script>";
					echo "alert('Data Telah Dihapus');";
					echo "location.replace('".$_SERVER['SCRIPT_NAME']."');";
					echo "</script>";
				}else{
					echo "<script>";
					echo "alert('Data Gagal Dihapus');";
					echo "</script>";				
				}
			}

		}
	?>
	


	<div class="container" style='margin-bottom:40px;margin-top:40px'>

	<div class="row">
		<div class="col-md-12">
			<form>
				<div class="form-group">
					<input type="text" name="search" class="form-control" placeholder="Cari judul artikel..">
				</div>
			</form>
		</div>
	</div>
		<table class='table table-bordered'>
			<thead>
				<tr>
					<th style='text-align:center'>Nama Judul</th>
					<th style='text-align:center'>Tags</th>
					<th style='text-align:center'>Tanggal Posting</th>
					<th style='text-align:center'>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				for ($i=0; $i < count($datas) ; $i++) { 
				?>	

					<tr>
						<td><?php echo $datas[$i]->judul; ?></td>
						<td><?php echo $datas[$i]->tags; ?></td>
						<td><?php echo $datas[$i]->tanggal; ?></td>
						<td>
							<div class='form-inline'>
								<a href=<?php echo "'".getBaseUrl()."administrasi/artikel/edit-data.php?id=".$datas[$i]->id."'"; ?> class='btn btn-warning'>Edit</a>
								<a href="https://fac-institute.com/administrasi/new-artikel?del=<?= $datas[$i]->id ?>" class='btn btn-danger'>Delete</a>
							</div>
						</td>
					</tr>

				<?php } ?> 

				
				
			</tbody>
		</table>

		<div class="row">
        	<div class="col-md-12">
        		<center>
            		<ul class="pagination pagination-sm">

		            <?php 
		            	$totalRow = $ctrlArtikel->countAllArtikel();
		                $limit = 6;
		                if (isset($_GET['page'])) {
		                    $page = $_GET['page'];
		                } else {
		                    $page = 0;
		                }
		                
		                
		                $b = intval($page/$limit);
		                $c = ($b + 1) * 5;
		                $batas = intval($totalRow) / 6;

		                if ($page >= $limit) {
		                    if ($page == $limit) {
		                        $prev = $b * $limit - 2;
		                    } else {
		                        $prev = $b * $limit - 1;
		                    }

		                    echo "<li><a href='".basename(__FILE__, '.php')."?page=$prev'>< Prev</a></li>";
		                }

		                for ($i=$b*$limit-1; $i < $c; $i++) { 
		                    $j = $i +1;

		                    if ($i<$batas&&$j!=0) {
		                        echo "<li><a href='".basename(__FILE__, '.php')."?page=$j'>$j</a></li>";    
		                    }
		                }
		                
		                if ($c < $batas) {
		                    $k = $c + 1;
		                    echo "<li><a href='".basename(__FILE__, '.php')."?page=$k'>Next ></a></li>";
		                }
		                
		                // for ($i=1; $i < intval($totalRow) / 30 + 1 ; $i++) { 
		                    // echo "<li><a href='".basename(__FILE__, '.php')."?page=$i'>$i</a></li>";   
		                // }
		            ?>
            		</ul>
        		</center>
        	</div>
    	</div><!--end row pagination-->
</div>

	    <!-- Modal -->
  <div class='modal fade' id='modal-konfirmasi' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pesan Konfirmasi</h3>

        </div>
        <div class='modal-body'>
        <div style='padding:15px' class='row'>
          
          <div class="col-md-12">
            
            		<div class="row" id='message-body'>
            			Anda Yakin Ingin Menghapus Data <span id='judul-delete' style="font-weight: bold;"></span> ?
            		</div>           		


          </div>
        </div>

        </div>
        <div class='modal-footer'>
        	<div class="row">
        		        <form method='post' action=<?php echo "'".$_SERVER['SCRIPT_NAME']."'"; ?>>
        					<input type='text' name='delete-id' id='konfirm-id' style='display:none' />
        		<div class="col-md-6" style="text-align:left">
							<button class='btn btn-danger'>Ya</button>
						</form>
						<button class='btn btn-info' data-dismiss='modal'>Tidak</button>        			
        		</div>

        		<div class="col-md-6">
        			<div class='form-inline'>
						<span>FAC-Institute 2016</span>
        			</div>
        			
        		</div>
        	</div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->  

<script type="text/javascript">
	function coba(aidi,myTitle){
		//alert(id);
		document.getElementById("judul-delete").innerHTML=myTitle;
		document.getElementById('konfirm-id').value=aidi;
	}
</script>

</body>

</html>