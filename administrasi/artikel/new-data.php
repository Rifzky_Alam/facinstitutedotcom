<?php 
session_start();

if (!isset($_SESSION['admin'])||!@$_SESSION['admin']['type']=='admin'){
	if (!$_SESSION['admin']['tipe']=='staff'){
		header('Location:../index.php');
	}
	
}

?>

<?php 

include_once '../../baseurl.php';

//echo $coba = getBaseUrl();

?>

<!DOCTYPE html>

<html>

<head>
	<title>Editor - Tutorial</title>
<?php include_once 'header.php'; ?>

</head>

<body>
	<?php include_once '../sidebar.php'; ?>
	<?php include_once '../top-nav.php'; ?>
	<?php include_once '../../model/Artikel.php'; ?>

	


	<div class="container" style='margin-bottom:40px;margin-top:40px'>

		<form enctype="multipart/form-data" action='' method='post'>

		<div class='row'>
			<div class='col-md-10'>
				<div class='form-group'>
					<label>Judul Tulisan</label>
					<?php if (isset($_POST['judul'])): ?>
					<input type="text" class='form-control' name='judul' placeholder="tulis judul dari tutorial disini" value=<?php echo "'".$_POST['judul']."'"; ?>>	
						<?php else: ?>
					<input type="text" class='form-control' name='judul' placeholder="tulis judul dari tutorial disini">	
					<?php endif ?>
					
				</div>
			</div>
		</div>

		<div class='row'>
			<div class='col-md-6'>
				<div class='form-group'>
					<label>Tags</label>
					<?php if (isset($_POST['tags'])): ?>
					<input class='form-control' name='tags' id='tagz' value=<?php echo "'".$_POST['tags']."'"; ?>/>
						<?php else: ?>
					<input class='form-control' name='tags' id='tagz' />
					<?php endif ?>
					
				</div>
			</div>
		</div>

		<div class='row'>
            <div class="form-group col-sm-8">
            	<label for="">Thumbnail Artikel</label>
            	<input type="file" class="filestyle" data-icon="true" data-classButton="btn btn-default" data-classInput="form-control inline v-middle input-s" id="artikel-imageUpload" name="imageUpload">
            	<span>Masukan Gambar minimal 2.5 MB</span>
            </div>
		</div>

		
			<?php if (isset($_POST['isi'])): ?>
			<textarea id='contoh' name='isi'><?php echo $_POST['isi']; ?></textarea>
				<?php else: ?>
			<textarea id='contoh' name='isi'></textarea>
			<?php endif ?>
			

			<br>

			<button class='btn btn-primary' style="width:100%">Submit</button>

		</form>

	</div>



	<div class='row'>
		<div class='col-md-12'>
		<?php 
			if (isset($_POST['judul'],$_POST['isi'],$_POST['tags'],$_FILES['imageUpload']['name'])){
				$artikel = new Artikel();

				if (preg_match('/.jpg/i', $_FILES['imageUpload']['name'])) {
					$nama_gambar = md5('img-'.date('Y-m-d').$_POST['judul']).'.jpg';
				}elseif (preg_match('/.png/i', $_FILES['imageUpload']['name'])) {
					$nama_gambar = md5('img-'.date('Y-m-d').$_POST['judul']).'.png';
				}elseif (preg_match('/.jpeg/i', $_FILES['imageUpload']['name'])) {
					$nama_gambar = md5('img-'.date('Y-m-d').$_POST['judul']).'.jpeg';
				}

				$inputData = array(
									'judul' => $_POST['judul'],
									'isi' => $_POST['isi'],
									'tags' => $_POST['tags'],
									'imageUrl' => $nama_gambar,
									'tempName' => $_FILES['imageUpload']['tmp_name'],
									'fileSize' => $_FILES['imageUpload']['size'],
									'author' => $_SESSION['admin']['nama']
								  );

				$objek = json_encode($inputData);
				$objek = json_decode($objek);

				if (preg_match('/.jpg/i', $_FILES['imageUpload']['name'])||preg_match('/.png/i', $_FILES['imageUpload']['name'])||preg_match('/.jpeg/i', $_FILES['imageUpload']['name'])) {

					if (move_uploaded_file($objek->tempName, 'images/'.$objek->imageUrl)){
						
						if ($artikel->insertArtikel($objek)){
							echo "<script>";
							echo "alert('Data dan Gambar Berhasil Disimpan');";
							echo "location.replace('".$_SERVER['SCRIPT_NAME']."')";
							echo "</script>";
						}else{
							echo "<script>";
							echo "alert('Data Gagal Disimpan');";
							echo "</script>";
							@unlink('images/'.$objek->imageUrl);						
						}

					}	

				}else{
					echo "<script>";
					echo "alert('Hanya gambar berekstensi *.jpg dan *.png yang diperbolehkan untuk di upload.');";
					echo "</script>";						
				}
				
				//echo $objek->fileSize;echo "<br>";echo $objek->tempName;
			}

		?>
		</div>
	</div>
	<script type="text/javascript" src=<?php echo "'".getBaseUrl()."js/bootstrap-filestyle.min.js'"; ?>></script>
	<script type="text/javascript" src=<?php echo "'".getBaseUrl()."administrasi/cms/ckeditor/ckeditor.js'"; ?>></script>

	<script type="text/javascript">

		CKEDITOR.replace('contoh');

	</script>

</body>

</html>