<?php 
session_start();

if (!isset($_SESSION['admin'])||!@$_SESSION['admin']['type']=='admin'){
	if (!$_SESSION['admin']['tipe']=='staff'){
		header('Location:../index.php');
	}
}

?>

<?php 

include_once '../../baseurl.php';

?>

<!DOCTYPE html>

<html>

<head>
	<title>Editor - Tutorial::Edit</title>
<?php include_once 'header.php'; ?>

</head>

<body>
	<?php include_once '../sidebar.php'; ?>
	<?php include_once '../top-nav.php'; ?>
	<?php include_once '../../model/Artikel.php'; ?>

	<?php 
	$ctrlArtikel = new Artikel();
	$datas = json_decode($ctrlArtikel->selectJudulAndID());


	if (isset($_POST['edit-id'],$_POST['edit-judul'],$_POST['edit-tags'],$_POST['edit-isi'],$_FILES['imageUpload']['name'])){
		if (!empty($_POST['judul'])) {
			echo "<script>";
			echo "alert('Judul Tutorial Harus Di isi');";
			echo "</script>";
		}else{

			if (!empty($_FILES['imageUpload']['name'])) {

				if (preg_match('/.jpg/i', $_FILES['imageUpload']['name'])) {
					$nama_gambar = md5('img-'.time().$_POST['judul']).'.jpg';
				}elseif (preg_match('/.png/i', $_FILES['imageUpload']['name'])) {
					$nama_gambar = md5('img-'.time().$_POST['judul']).'.png';
				}elseif (preg_match('/.jpeg/i', $_FILES['imageUpload']['name'])) {
					$nama_gambar = md5('img-'.time().$_POST['judul']).'.jpeg';
				}


				$dataInput = array(
									'id' => $_POST['edit-id'],
									'judul' => $_POST['edit-judul'], 
									'tags' => $_POST['edit-tags'],
									'isi' => $_POST['edit-isi'],
									'tempName' => $_FILES['imageUpload']['tmp_name'],
									'gambar'=>$nama_gambar
								   );
				$objek = json_encode($dataInput);
				$objek = json_decode($objek);

				if (preg_match('/.jpg/i', $_FILES['imageUpload']['name'])||preg_match('/.png/i', $_FILES['imageUpload']['name'])||preg_match('/.jpeg/i', $_FILES['imageUpload']['name'])) {

					if (move_uploaded_file($objek->tempName, 'images/'.$objek->gambar)){
						
						if ($ctrlArtikel->editArtikelWithImage($objek)){
							@unlink('images/'.$_POST['edit-gambarLama']);
							echo "<script>";
							echo "alert('Data dan Gambar Berhasil Disimpan');";
							echo "location.replace('".$_SERVER['SCRIPT_NAME']."')";
							echo "</script>";
						}else{
							echo "<script>";
							echo "alert('Data Gagal Disimpan');";
							echo "</script>";
							@unlink('images/'.$objek->imageUrl);						
						}

					}	

				}else{
					echo "<script>";
					echo "alert('Hanya gambar berekstensi *.jpg dan *.png yang diperbolehkan untuk di upload.');";
					echo "</script>";						
				}

			}else{
				$dataInput = array(
									'id' => $_POST['edit-id'],
									'judul' => $_POST['edit-judul'], 
									'tags' => $_POST['edit-tags'],
									'isi' => $_POST['edit-isi']
								   );
				$objek = json_encode($dataInput);
				$objek = json_decode($objek);
				
				if ($ctrlArtikel->editArtikel($objek)) {
					echo "<script>";
					echo "alert('Data berhasil diubah!');";
					echo "</script>";									
				}else{
					echo "<script>";
					echo "alert('Data gagal diubah');";
					echo "</script>";						
				}

			}



			/*
			if ($ctrlArtikel->updateTutorial($dataInput)){
				echo "<script>";
				echo "alert('Success: Pengubahan data berhasil! ');";
				echo "</script>";				
			}else{
				echo "<script>";
				echo "alert('Error: Data gagal di ubah! ');";
				echo "</script>";					
			}
			*/
		}
	}

	?>
	




	<div class="container" style='margin-bottom:40px;margin-top:40px'>

		<div class='row'>
			<div class='col-md-10'>
				<form name='judulForm' action=<?php echo "'".$_SERVER['SCRIPT_NAME']."'"; ?> method="GET">
					<select name='id' class='form-control' onchange='judulForm.submit()'>
						<option value=''>--Pilih Judul Tutorial--</option>
						<?php 
							for ($i=0; $i < count($datas); $i++){
								echo "<option value='".$datas[$i]->id."'>".$datas[$i]->judul."</option>";
							}
						?>
					</select>
				</form>
			</div>
		</div>
		<hr>

		<?php if (isset($_GET['id'])): ?>

		<?php 
			$dataOfArticles = $ctrlArtikel->selectById($_GET['id']);
		?>

		<form enctype="multipart/form-data" id="form-edit" action='' method="post">



			<br>
			<div class='row'>
				<div class='col-md-10'>
					<div class='form-group'>
						<label>Judul</label>
						<input class='form-control' name='edit-judul' value=<?php echo "'".$dataOfArticles->judul."'"; ?>>
					</div>
				</div>
			</div>

			<div class='row'>
				<div class='col-md-10'>
					<div class='form-group'>
						<label>Tags</label>
						<input class='form-control' id='my-tags' name='edit-tags' value=<?php echo "'".$dataOfArticles->tags."'"; ?>>
					</div>
				</div>
			</div>

			<div class='row'>
				<div class='col-md-6'>
					<label>Gambar Thumbnail Artikel</label>
					<img src=<?php echo "'images/".$dataOfArticles->image_url."'"; ?> alt='gambar thumbnail' id='gambarku' class='img img-responsive'/>
				</div>
			</div>
			<br>
			<div class='row'>
		            <div class="form-group col-sm-6">
		            	<label for="">Thumbnail Artikel</label>
		            	<input type="file" class="filestyle" data-icon="true" onchange="myPreview(this.id,document.getElementById('preview-gambar').id)" data-classButton="btn btn-default" data-classInput="form-control inline v-middle input-s" id="artikel-imageUpload" name="imageUpload">
		            	<a id='preview-gambar' href="" target='_blank'></a>
		            </div>
			</div>

			<div class='row'>
				<div class='col-md-10'>
					<div class='form-group'>
						<label>Konten/Isi</label>
						<textarea id='isi-tutorial' name='edit-isi'><?php echo $dataOfArticles->isi; ?></textarea>
					</div>
				</div>
			</div>

			<input style='display:none' name='edit-id' value=<?php echo "'".$_GET['id']."'"; ?>>
			<input style='display:none' name='edit-gambarLama' value=<?php echo "'".$dataOfArticles->image_url."'"; ?>>
			<div class="row">
				<div class="col-md-10">
					<button class='btn btn-primary' style="width:100%">Submit</button>
				</div>
			</div>
			
		</form>
		<?php endif ?>


	</div>
	<script type="text/javascript" src=<?php echo "'".getBaseUrl()."js/bootstrap-filestyle.min.js'"; ?>></script>
	<script type="text/javascript" src=<?php echo "'".getBaseUrl()."administrasi/cms/ckeditor/ckeditor.js'"; ?>></script>

	<script type="text/javascript">

		CKEDITOR.replace('isi-tutorial');

	</script>

	<script type="text/javascript">
	    function myPreview(x,y){
	    //console.log(y);
	    var ex = x;
	    var ye = y;
	    var input = document.getElementById(ex);
	    var fReader = new FileReader();
	    fReader.readAsDataURL(input.files[0]);
	      fReader.onloadend=function(event){
	        var myImg = document.getElementById(ye);
	        myImg.innerHTML = 'Preview Gambar Baru';//input.value;
	        myImg.href = event.target.result;

	      }
	    }
	</script>

</body>

</html>
