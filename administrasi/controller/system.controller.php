<?php include_once 'classes/View.php';
class FacSystem{

	//@url (value="/administrasi/bug/new-bug", method=POST)
	public static function SubmitNewBugPost($input){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		
		$r = $db->InsertData('report_bugs',
			[
				'rb_username' => $_SESSION['admin']['username'],
				'rb_report' => $input['bug'],
				'rb_links' => $input['links'],
				'rb_status' => '0'
			]
		);
		
		if ($r) {
		     $db->InsertData('notifikasi',
			    [
				    'ntf_judul' => 'a new bug report has been submitted.',
				    'ntf_desc' => $_SESSION['admin']['nama'].' has submitted a new bug report',
				    'ntf_username' => 'charlez',
				    'ntf_url_target' => $data->base_url.'administrasi/bug/listdata'
			    ]
		    );
			header('location:'.$data->base_url.'administrasi/bug/listdata');
		}else{
			header('location:'.$data->base_url.'administrasi/bug/listdata');
		}
	}

	//@url (value="/administrasi/bug/new-bug", method=GET)
	public static function NewBugForm(){
		$data = new Data();
		
		$data->headertext = "System - Report Bug / Error";
		$data->judul = 'FAC || Bug/Error/Keluhan - System';
		$data->username = $_SESSION['admin']['nama'];

		$data->View('system/vnewbug.bug',$data);
	}

	//@url (value="/administrasi/bug/editbug", method=POST)
	public static function EditBugPost($id,$input){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		$r = $db->UpdateData('report_bugs',
			[
				'rb_report' => $input['bug'],
				'rb_status' => $input['status'],
				'rb_links' => $input['links'],
			],
			array($db->GetCond('rb_id','=',$id))
		);
		if ($r) {
			header('location:'.$data->base_url.'administrasi/bug/listdata');
		}else{
			header('location:'.$data->base_url.'administrasi/bug/listdata');
		}

	}

	//@url (value="/administrasi/bug/editbug", method=GET)
	public static function EditBug($id){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		$data->rowdata = $db->FetchOneRow(['*'],'report_bugs',array($db->GetCond('rb_id','=',$id)));
		$data->statusdata = $db->Fetch('flag_report_bugs');
		
		$data->headertext = "System - Edit Report Bug / Error";
		$data->judul = 'FAC || Edit Bug/Error/Keluhan ';
		$data->username = $_SESSION['admin']['nama'];

		$data->View('system/vedit.bug',$data);
	}

	//@url (value="/administrasi/bug/listdata", method=GET)
	public static function ListOfError(){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$db->setOrder('rb_timestamp','DESC');
		$data->listdata = $db->FetchJoin(['*'],'report_bugs',array($db->getJoin('INNER','flag_report_bugs','rb_status','frb_flag')));
		$data->headertext = "System - Data Error Web FAC Institute";
		$data->judul = 'FAC || Data Bug/Error/Keluhan';
		$data->username = $_SESSION['admin']['nama'];

		$data->View('system/vlistdata.bug',$data);
	}
}