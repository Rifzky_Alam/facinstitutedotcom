<?php include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/classes/View.php';
class CustServCtr{
    // /administrasi/customerservice/api?req=cte, GET
    public static function GetAllExportableCompany(){
        header('Content-Type: application/json');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		
		$idata = $db->FetchCount('id','perusahaan',array($db->GetCond('p_status','=','51')));
		
		$arr = [
		    'status' => ['code'=>'1','description'=>'ok'],
					'totaldata' => $idata
		];
		
		echo json_encode($arr);
    }
    
    // /administrasi/customerservice/exportusaha?id={value}, GET
    public static function EditStatusUsaha($id,$status){
        $data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$idata = $db->FetchOneRow(['*'],'perusahaan',array($db->GetCond('id','=',$id)));
		if($idata['alamat']!=''){
		    $r = $db->UpdateData('perusahaan',['p_status'=>$status],array($db->GetCond('id','=',$id)));
		    if($r)
		        header('location: '.$data->base_url.'administrasi/new-usaha?id='.$id);
		    else
    		    header('location: '.$data->base_url.'administrasi/new-usaha?id='.$id);
		}
        
    }    
    
    // /administrasi/customerservice/editclient?id={value}, GET
    public static function EditClient($id){
        $data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		
		$idata = $db->FetchOneRow(['*'],'new_customer',array($db->GetCond('id_cust','=',$id)));
		print_r($idata);
    }
    
    // /administrasi/customerservice/newcomp, GET
	public static function NewCompanyList(){
		$data = new Data();
		$data->Model('Perusahaan');
		$db = new Perusahaan();

        $data->listdata = $db->FetchDataPerusahaanSupport();

        //print_r($data->listdata);

		$data->subtitle = 'List Perusahaan Support';
		$data->judul = 'List Perusahaan Support';
		$data->page = 'perusahaan';
		$data->username = $_SESSION['admin']['nama'];

		$data->View('customerservice/vtblcompany.cs',$data);
	}

	// /administrasi/customerservice/newclient, POST
	private static function NewClientPost($masukdata){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$r = $db->InsertData(
			'new_customer'
			,
			[
				'id_cust' => md5($masukdata['telepon']),
				'id_perusahaan' => $masukdata['iu'],
				'nama_cust' => $masukdata['namacust'],
				'gender_cust' => $masukdata['gender'],
				'telp_cust' => $masukdata['telepon'],
				'marketing_id' => 'support',
				'status_cust' => '51'
			]
		);

		if ($r) {
			header('location:'.$data->base_url.'administrasi/customerservice/data-usaha?id='.$masukdata['iu']);
		}else{
			header('location:'.$data->base_url.'administrasi/customerservice/data-support');
		}
	}

	// /administrasi/customerservice/newclient, GET
	public static function NewClient($idusaha){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		if (isset($_POST['in'])) {
			self::NewClientPost($_POST['in']);
		}

		$idata = $db->FetchOneRow(['nama'],'perusahaan',array($db->GetCond('id','=',$idusaha)));
		// echo $idata['nama'];
		$data->namausaha = $idata['nama'];
		$data->idusaha = $idusaha;
		$data->subtitle = 'Klien Baru - '.$idata['nama'];
		$data->judul = 'Input Klien Baru';
		$data->page = 'perusahaan';
		$data->username = $_SESSION['admin']['nama'];

		$data->view('customerservice/vnewclient.cs', $data);
	}

    public static function EditSupportDataPost($masukdata){
        $data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		$ur = $db->UpdateData('cs_suppdet',
			[
				'sisstitle' => $masukdata['title'],
				'ssolution' => $masukdata['solution'],
				'iflmedia' => $masukdata['via'],
				'snotes' => $masukdata['notes']
			],
			array($db->GetCond('sticket','=',$masukdata['id']))
		);

		if ($ur) {
			header('location:'.$data->base_url.'administrasi/customerservice/data-support');
		} else {
			header('location:'.$data->base_url.'administrasi/customerservice/data-support');
		}

    }    
    // @url /administrasi/customerservice/data-support?edt={value}
    public static function EditSupportData($idtiket){
        
        if(isset($_POST['in'])){
            // print_r($_POST['in']);
            self::EditSupportDataPost($_POST['in']);
        }
        
        $data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		
		$singlerow = $db->FetchJoinWhere(['*'],
		'cs_suppdet',
		array(
		    $db->getJoin('INNER','new_customer','new_customer.id_cust','cs_suppdet.sfkclient'),
		    $db->getJoin('INNER','perusahaan','perusahaan.id','cs_suppdet.sfkusaha')
		    ),
		array($db->GetCond('sticket','=',$idtiket))
		);
		$data->listmedia = $db->FetchWhere(['dt_flag','dt_desc'],'desc_tbl',array($db->GetCond('dt_relate_tbl','=','cs_suppdet')));
		// print_r($singlerow);
		$data->idusaha = $singlerow[0]['id'];
		$data->idcust = $singlerow[0]['id_cust'];
		$data->nama_usaha = $singlerow[0]['nama'];
        $data->cust = $singlerow[0]['nama_cust'];
        $data->id = $singlerow[0]['sticket'];
        $data->telepon = $singlerow[0]['telp_cust'];
        $data->email = $singlerow[0]['email_cust'];
        $data->topik = $singlerow[0]['sisstitle'];
        $data->solusi = $singlerow[0]['ssolution'];
        $data->media = $singlerow[0]['iflmedia'];
        $data->notes = $singlerow[0]['snotes'];
        
        $data->subtitle = 'Edit Support Client No. '. $singlerow[0]['sticket'];
		$data->judul = 'EDIT SUPPORT CLIENT';
		$data->page = 'perusahaan';
		$data->username = $_SESSION['admin']['nama'];
		$data->view('customerservice/veditsupp.cs', $data);
        
    }
	// @url /administrasi/customerservice/detail?tid={value}
	public static function DataSupportDetail($id){
		$data = new Data();
		$data->Model('Customer');
		$cust = new Customer();
		$cust->SetLocalFormat();
		$detail = $cust->DetailSupportClient($id);
		//print_r($detail);
		
		$data->notiket=$detail['sticket'];
		$data->tanggal = $detail['tanggal'];
		$data->nama_usaha = $detail['nama_usaha'];
		$data->cust = $detail['sname'];
		$data->telepon = $detail['sphone'];
		$data->email = $detail['semail'];
		$data->topik =$detail['sisstitle'];
		$data->solusi =$detail['ssolution'];
		$data->media =$detail['via'];
		$data->pic =$detail['pic'];
		if($detail['snotes']!='-'){
		    $arr = explode('#',$detail['snotes']);
		    $data->notes = "Nama Usaha: ".@$arr[0]."<br> Jenis Usaha: ".@$arr[1]."<br> Versi Accurate: ".@$arr[2];
		}else{
		    $data->notes = '-';
		}
		
		$data->subtitle = 'Detail Support Client';
		$data->judul = 'DETAIL SUPPORT CLIENT';
		$data->page = 'perusahaan';
		$data->username = $_SESSION['admin']['nama'];
		$data->view('customerservice/vdetsupp.cs', $data);
	}
	
	public static function DownloadDataSupport($searchz){
        $data = new Data();
		$data->Model('Perusahaan');
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$usaha = new Perusahaan();
		$data->JustInclude('administrasi/functions/ExcelFunc');
		
		$search = [
			    'namausaha' => $searchz['np'],
			    'klien' => $searchz['client'],
			    'topik' => $searchz['topik'],
			    'fd' => $searchz['fd'],
			    'td' => $searchz['td'],
			    'petugas' => $searchz['username']
		];
		
		$data->listdata = $usaha->FetchDataSupport($search);
		$tambahan = "";
		if($search['fd']!=''&&$search['td']!=''){
		    $tglawal = str_replace('-','',$search['fd']);
		    $tglakhir = str_replace('-','',$search['td']);
		    $tambahan = "_".$tglawal."_".$tglakhir;
		}
		
		$filename = "DataSupport".$tambahan.".xls";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: application/vnd.ms-excel");
        
        $myRow = array();
        foreach($data->listdata as $key) { 
		
        $row = array(
            "No Tiket"=>$key['sticket'],
            "Tanggal"=>$key['tanggal'], 
            "Perusahaan" => $key['nama_usaha'], 
            "Nama Klien"=>$key['sname'],
            "Telepon" =>$key['sphone'],
            "Email" => $key['semail'],
            "Topik Masalah"=>$key['sisstitle'],
            "Solusi"=>$key['ssolution'],
            "Media"=>$key['media'],
            "Petugas"=>$key['spic'],
            "Catatan Lain"=>$key['snotes']
        );
    	    array_push($myRow, $row);
    
    	}
    	$flag = false;
    	foreach($myRow as $row) {
            if(!$flag) {
              // display field/column names as first row
              echo implode("\t", array_keys($row)) . "\n";
              $flag = true;
            }
            array_walk($row,'cleanData');
            echo implode("\t", array_values($row)) . "\n";
        }
        exit;
    }
    
    public static function DownloadDataSupportForMeeting($searchz){
        $data = new Data();
		$data->Model('Perusahaan');
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$usaha = new Perusahaan();
		$data->JustInclude('administrasi/functions/ExcelFunc');
		
		$search = [
			    'namausaha' => $searchz['np'],
			    'klien' => $searchz['client'],
			    'topik' => $searchz['topik'],
			    'fd' => $searchz['fd'],
			    'td' => $searchz['td'],
			    'petugas' => $searchz['username']
		];
		
		$data->listdata = $usaha->DownloadzDataSupportForMeeting($search);
		$tambahan = "";
		if($search['fd']!=''&&$search['td']!=''){
		    $tglawal = str_replace('-','',$search['fd']);
		    $tglakhir = str_replace('-','',$search['td']);
		    $tambahan = "_".$tglawal."_".$tglakhir;
		}
		
		$filename = "Data_meeting_bulanan".$tambahan.".xls";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: application/vnd.ms-excel");
        
        $myRow = array();
        foreach($data->listdata as $key) { 
            $row = array(
                "No Tiket"=>$key['sticket'],
                "Tanggal"=>$key['tanggal'], 
                "Perusahaan" => $key['nama_usaha'], 
                "Nama Klien"=>$key['sname'],
                "Telepon" =>$key['sphone'],
                "Email" => $key['semail'],
                "Topik Masalah"=>$key['sisstitle'],
                "Solusi"=>$key['ssolution'],
                "Media"=>$key['media'],
                "Petugas"=>$key['spic'],
                "Status Customer"=>$key['status_cust'],
                "Last Trainer"=>$key['last_trainer'],
                "Trainers"=>$key['trainers'],
                "Total Training"=>$key['totaltraining']
            );
    	    array_push($myRow, $row);
    
    	}
    	$flag = false;
    	foreach($myRow as $row) {
            if(!$flag) {
              // display field/column names as first row
              echo implode("\t", array_keys($row)) . "\n";
              $flag = true;
            }
            array_walk($row,'cleanData');
            echo implode("\t", array_values($row)) . "\n";
        }
        exit;
    }

    public static function TabelDataSupport($page='0'){
        $data = new Data();
		$data->Model('Perusahaan');
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$usaha = new Perusahaan();
		$data->querystring = '';
		if (isset($_GET['src'])) {
			$search = [
			    'namausaha' => $_GET['src']['np'],
			    'klien' => $_GET['src']['client'],
			    'topik' => $_GET['src']['topik'],
			    'fd' => $_GET['src']['fd'],
			    'td' => $_GET['src']['td'],
			    'petugas' => $_GET['src']['username']
			];
			$data->listdata = $usaha->FetchDataSupport($search);
			$data->querystring = $_SERVER['QUERY_STRING'];
		}elseif($page>'0'){
			// $page = intval($page)-1; 
		    $data->listdata = $usaha->FetchDataSupport('',$page*30,'30');
		}else {
			$data->listdata = $usaha->FetchDataSupport('','0','30');
		}

		$db->setOrder('users.nama','ASC');
		$data->users = $db->FetchColumns(['username,nama'],'users');
		$db->setOrder('','');
		
		$data->totaldata = $db->OnlyFetchCount('sticket','cs_suppdet');
		
		
		
        $data->subtitle = 'Tabel Support Client';
		$data->judul = 'DATA SUPPORT CLIENT';
		$data->page = 'penawaran';
		$data->pagenum = $page;
		$data->username = $_SESSION['admin']['nama'];
		$data->view('customerservice/vtblsupport.cs', $data);
        
    }

	public static function TabelUsaha(){
		$data = new Data();
// 		$data->Model('Querybuilder');
		
		$data->inputbtn = false;
		$data->subtitle = 'Data Perusahaan';
		$data->judul = 'DATA PERUSAHAAN';
		$data->page = 'perusahaan';
		$data->username = $_SESSION['admin']['nama'];
		$data->view('usaha/view-usaha-default', $data);
	}

	public static function SearchNamaUsaha($keyword,$menu=''){
		$data = new Data();
		$data->Model('Perusahaan');
		$usaha = new Perusahaan();
		if ($menu=='') {
			$usaha->setNamausaha($keyword);
			$data->mylists = $usaha->FetchSearchCs();	
		}else{
			$usaha->setId($keyword);
			$data->mylists = $usaha->FetchCustServByID($keyword);
		}
		
// 		print_r($data->mylists);
		$data->subtitle = 'Data Perusahaan';
		$data->judul = 'DATA PERUSAHAAN';
		$data->page = 'perusahaan';
		$data->username = $_SESSION['admin']['nama'];
		$data->view('customerservice/vrsdatausaha.cs', $data);
	}
	
	private static function getIdSupport($numberfromdb){
	    $num = $numberfromdb +1;
	    if($num<10){
	        return "TS-00000".$num;
	    }elseif($num<100){
	        return "TS-0000".$num;
	    }elseif($num<1000){
	        return "TS-000".$num;
	    }elseif($num<10000){
	        return "TS-00".$num;
	    }elseif($num<100000){
	        return "TS-0".$num;
	    }elseif($num<1000000){
	        return "TS-".$num;
	    }
	}
	
	private static function NewSupportDataPost($masukdata){
	    $data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		// print_r($masukdata);
		$idticket = self::getIdSupport($db->OnlyFetchCount('sticket','cs_suppdet'));

		if ($masukdata['iu']=='-') {
			$r = $db->InsertData('cs_suppdet',
			    [
			        'sticket'=>$idticket,
			        'dcurdate'=>Date('Y-m-d'),
			        'sfkusaha'=>'-',
			        'sfkclient' => $masukdata['ic'],
			        'sisstitle'=>$masukdata['title'],
			        'ssolution'=>$masukdata['solution'],
			        'iflmedia'=>$masukdata['via'],
			        'spic'=>$_SESSION['admin']['username'],
			        'istatus'=>'1',
			        'snotes'=> $masukdata['namausaha'].'#'.$masukdata['ju'].'#'.$masukdata['ja']
			    ]
			);
		} else {
			$r = $db->InsertData('cs_suppdet',
			    [
			        'sticket'=>$idticket,
			        'dcurdate'=>Date('Y-m-d'),
			        'sfkusaha'=>$masukdata['iu'],
			        'sfkclient' => $masukdata['ic'],
			        'sisstitle'=>$masukdata['title'],
			        'ssolution'=>$masukdata['solution'],
			        'iflmedia'=>$masukdata['via'],
			        'spic'=>$_SESSION['admin']['username'],
			        'istatus'=>'1',
			        'snotes'=>'-'
			    ]
			);
		}
			
		
		if($r){
		    header('location:'.$data->base_url.'administrasi/customerservice/data-support');
		}else{
		    header('location:'.$data->base_url.'administrasi/customerservice/data-support');
		}
	}

	public static function NewSupportData($id='-',$cust){
	    $data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		
		if(isset($_POST['in'])){
		    self::NewSupportDataPost($_POST['in']);
		}
		
		$datausaha = $db->FetchOneRow(['*'],'perusahaan',array($db->GetCond('id','=',$id)));
		$data->listmedia = $db->FetchWhere(['dt_flag','dt_desc'],'desc_tbl',array($db->GetCond('dt_relate_tbl','=','cs_suppdet')));
		
		$data->namacust = '';
		$idata = $db->FetchOneRow(['nama_cust'],'new_customer',array($db->GetCond('id_cust','=',$cust)));
		$data->namacust = $idata['nama_cust'];
		if ($idata['nama_cust']=='') {
			$idata = $db->FetchOneRow(['sname'],'cs_klien',array($db->GetCond('sid','=',$cust)));
			$data->namacust = $idata['sname'];
		}

		$data->idusaha = $datausaha['id'];
		$data->idcust = $cust;
		$data->subtitle = 'Support '.$datausaha['nama'];
		$data->judul = 'Support '.$datausaha['nama'];
		$data->page = 'perusahaan';
		$data->username = $_SESSION['admin']['nama'];
		$data->view('customerservice/vnewsupport.cs', $data);
	}
	
	private static function NewUsahaPost($masukdata){
		// print_r($masukdata);
		$data = new Data();
		$data->Model('Querybuilder');
		$data->Model('Perusahaan');
		$usaha = new Perusahaan();
		$db = new QueryBuilder();

		$id = md5($masukdata['np'].$masukdata['ketJenisUsaha']);
		$usaha->setId($id);

		$r = $db->InsertData(
			'perusahaan',
			[
				'id' => $id,
				'nama' => $masukdata['np'],
				'email' => $masukdata['email'],
				'telepon' => $masukdata['telepon'],
				'ket_jenis_usaha' => $masukdata['ketJenisUsaha'],
				'p_status' => '51'
			]
		);

		if ($r) {
			if (isset($masukdata['jnsAccurate'])) {
	  			$usaha->InputVersiAccurate($usaha->getId(),$_POST['data']['jnsAccurate']);
	  		}

	  		if (isset($masukdata['jnsUsaha'])) {
	  			$usaha->InputJenisUsaha($usaha->getId(),$_POST['data']['jnsUsaha']);
	  		}
	  		sleep(0.5);

	  		header('location:'.$data->base_url.'administrasi/customerservice/data-usaha?id='.$id);
		}else{
			header('location:'.$data->base_url.'administrasi/customerservice/data-usaha');
		}
			

	}

	public static function NewSupportNonClientData($id='-'){
	    $data = new Data();
		$data->Model('Querybuilder');
		
		$db = new QueryBuilder();
		
		if(isset($_POST['data'])){
		    self::NewUsahaPost($_POST['data']);
		}
		$data->Model('Perusahaan');
		$usaha = new Perusahaan();

		$data->jenisusaha = $usaha->FetchJenisUsaha();
		$data->accurates = $usaha->FetchVersiAccurate();
		
		
		$data->idusaha = '';
		$data->subtitle = 'Support Non Client FAC';
		$data->judul = 'Perusahaan Support Baru';
		$data->page = 'perusahaan';
		$data->username = $_SESSION['admin']['nama'];
		$data->view('customerservice/vnewusaha.cs', $data);
	}


}