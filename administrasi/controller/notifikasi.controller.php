<?php include_once 'classes/View.php';
class Notifikasi{

    public static function NotifHistory($user){
        $data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		
		$db->setOrder('ntf_timestamp','DESC');
		
		$data->listdata = $db->FetchWhere(["ntf_judul","ntf_desc","ntf_url_target","CASE WHEN `ntf_status`='-1' THEN 'Terbaca' ELSE 'Belum dibaca' END AS status","DATE_FORMAT(`ntf_timestamp`,'%d %b %Y') AS tanggal"],'notifikasi',array($db->GetCond('notifikasi.ntf_username','=',$user)));
// 		print_r($data->listdata);
        $data->page = 'upload';
		$data->title = 'Data Pegawai';
		$data->judul = 'Data Pegawai';
		$data->subtitle='Data Pegawai';
		$data->username = $_SESSION['admin']['nama'];
        $data->View('notifikasi/vhistory.notif',$data);
    }

    //@url (value="/administrasi/notifikasi/markasreadall?usr={value}")
    public static function MarkAllAsRead($username){
        header('Content-Type: application/json');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		
        $data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		
		$r= $db->UpdateData('notifikasi',
		    [
		        'ntf_status' => '-1'  
		    ],
		    array(
		        $db->GetCond('ntf_username','=',$username)
		    )
		);
		
		if($r){
    		$arr = [
    			'status' => ['code'=>'1','description'=>'Updated!']
    		];
		}
		else{
		    $arr = [
    			'status' => ['code'=>'0','description'=>'Something has gone wrong!']
    		];
		}
		echo json_encode($arr);
		
    }


    //@url (value="/administrasi/notifikasi/markasread?id={value}")
    public static function MarkAsRead($idnotif){
        $data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$row = $db->FetchOneRow(['*'],'notifikasi',array($db->GetCond('ntf_id','=',$idnotif)));
		$targeturl = $row['ntf_url_target'];
		
		$r= $db->UpdateData('notifikasi',
		    [
		        'ntf_status' => '-1'  
		    ],
		    array(
		        $db->GetCond('ntf_id','=',$idnotif)
		    )
		);
		if($r)
		    header('location:'.$targeturl);
		else
		    header('location:'.$data->base_url.'administrasi/');

    }    
	//@url (value="/administrasi/notifikasi/api?req=list")
	public static function NotificationAPI($username){
		header('Content-Type: application/json');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$r = $db->FetchJoinWhere(['`ntf_id`', '`ntf_judul`', '`ntf_desc`', '`ntf_username`', '`ntf_url_target`', '`ntf_status`', '`ntf_timestamp`'],
			'notifikasi',
			array($db->getJoin('LEFT','users','users.username','notifikasi.ntf_username')),
			array(
			    $db->GetCond('notifikasi.ntf_username','=',$username),
			    $db->GetCond('notifikasi.ntf_status','>','-1')
			)
		);
		// print_r($r);
		
		if (count($r)!='0') {
			$arr = [
			    'status' => ['code'=>'1','description'=>'data is available'],
			    'totaldata' => count($r),
			    'data'=>$r
			];
		}else{
			$arr = [
			    'status' => ['code'=>'0','description'=>'Cannot retrieve any data for now, Sorry :('],
			    'data' => [] 
			];
		}
		echo json_encode($arr);
		
	}

	public static function InputNew($inputs){
		header('Content-Type: application/json');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$str = '{ "judul":"hebat", "desc":"hebat sekali yah", "username":"charlez", "url":"https://ajax.googleapis.com","status":"7" }';
		$hasil = json_decode($inputs);

		if (is_null($hasil)) {
			$arr = [
				'status' => ['code'=>'500','description'=>'Ooops, something wrong with your JSON!']
			];
			echo json_encode($arr);
			return;
		}

		if (property_exists($hasil, 'judul')&&property_exists($hasil, 'desc')&&property_exists($hasil, 'username')&&property_exists($hasil, 'url')&&property_exists($hasil, 'status')) {
			// if json is valid
			// insert new data
			$r = $db->InsertData(
				'notifikasi',
				[
					'ntf_judul' => $hasil->judul,
					'ntf_desc' => $hasil->desc,
					'ntf_username' => $hasil->username,
					'ntf_url_target' => $hasil->url,
					'ntf_status' => $hasil->status
				]
			);

			if ($r) {
				$arr = [
				    'status' => ['code'=>'1','description'=>'Inserted Successfully']
				];
				echo json_encode($arr);
				return;
			}else{
				$arr = [
				    'status' => ['code'=>'0','description'=>'Database Error.']
				];
				echo json_encode($arr);
				return;
			}

		}else{
			$arr = [
				'status' => ['code'=>'50','description'=>'Cannot accept your JSON Format!']
			];
			echo json_encode($arr);
			return;
		}
	}

	public static function DeleteNotif($datainput){
		header('Content-Type: application/json');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$hasil = json_decode($datainput);

		if (is_null($hasil)) {
			$arr = [
			    'status' => ['code'=>'0','description'=>'Error in JSON Format.'],
			    'last_request_str' => $str
			];
			echo json_encode($arr);
			return;
		}else {
			$r = $db->DeleteWhere('notifikasi',array($db->GetCond('ntf_id','=',$hasil->id)));
			if ($r) {
				$arr = [
				    'status' => ['code'=>'1','description'=>'Updated Successfully']
				];		
			}else {
				$arr = [
				    'status' => ['code'=>'0','description'=>'Cannot Update the data, contact administrator!']
				];
			}
			echo json_encode($arr);
		}
	}

	public static function UpdateStatus($datainput){
		header('Content-Type: application/json');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		// $str is supposed to be $_POST['data'] in JSON Format
		$str = '{ "id":"1", "status":3 }';
		$hasil = json_decode($datainput);
		// print_r($hasil);
		if (is_null($hasil)) {
			$arr = [
			    'status' => ['code'=>'0','description'=>'Error in JSON Format.'],
			    'last_request_str' => $str
			];
			echo json_encode($arr);
			return;
		}else {

			$r = $db->UpdateData('notifikasi',['ntf_status' => $hasil->status],array($db->GetCond('ntf_id','=',$hasil->id)));
			if ($r) {
				$arr = [
				    'status' => ['code'=>'1','description'=>'Updated Successfully']
				];		
			}else {
				$arr = [
				    'status' => ['code'=>'0','description'=>'Cannot Update the data, contact administrator!']
				];
			}
			
		}
		echo json_encode($arr);
	}

	public static function tesinputdata(){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		/*
		$db->InsertData('notifikasi',[
			'ntf_judul' => 'CASE #712',
			'ntf_desc' => 'Data Invoice telah diperbaharui',
			'ntf_username' => 'charlez',
			'ntf_url_target' => 'https://fac-institute.com/administrasi/data-invoice',
			'ntf_status' => '0'
		]);
		*/
	}


}