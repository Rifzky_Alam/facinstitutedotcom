<?php include_once 'classes/View.php';
class Workshopctr{

	private static function SyncTransaction($tanggal,$idusaha,$idcust,$item,$paid=''){
		$data = new Data();
		$data->Model('Querybuilder');
		$data->Model('Invoice');
		$data->Model('Workshopmdl');
		$db = new QueryBuilder();
		$workshop = new Workshopmdl();
		$invoice = new Invoice();
		$seq = $db->FetchCount('trans_id','new_transaksi',array($db->GetCond('trans_jenis','=','4')));
		$dataworkshop = $db->FetchOneRow(['alamat'],'perusahaan',array($db->GetCond('id','=',$idusaha)));
		$idtransaksi= "TR-WKS".date('mdY')."0".$seq;
		$descinv = 'Workshop Accurate '.$tanggal; // tanggal harus di perbaiki ke format indonesia, cth: 2 Februari 2020
		// echo "id_trans: ".$idtransaksi."<br>";
		// echo "invoice: ".$idtransaksi."<br>";
		// echo 'descinv: '.$descinv;
		// echo 'alamat inv'.$dataworkshop['alamat'];
		$itdata = $db->FetchOneRow(['harga'],'paket_training',array($db->GetCond('id','=',$item)));
		
		
		
		
		$itr = $db->InsertData('new_transaksi',
			[
				'trans_id' => $idtransaksi,
				'trans_jenis' => '4',
				'trans_id_usaha' => $idusaha,
				'trans_cust_id' => $idcust,
				'trans_lokasi' => $dataworkshop['alamat'],
				'trans_waktu' => '09:00-17:00',
				'trans_petugas' => $_SESSION['admin']['username']
			]
		);

		

		$ifc = $db->InsertData('fac_calendar',['tanggal' => $tanggal,'acara' => $idtransaksi,'cal_status' => '23']);
		$ititm = $db->InsertData('new_trans_items',['itm_trans_id' => $idtransaksi,'itm_paket' => $item]);
		$invoice->setIdtransaksi($idtransaksi);
        $invoice->setMetodebayar('1');
        $invoice->setAlamat('');
        $invoice->setDeskripsi($descinv);
        $invoice->setTanggal(date('Y-m-d'));
        if ($paid!='') {
        	$noinv = $invoice->InputNewDataReturnInvNumber('3');
        } else {
        	$noinv = $invoice->InputNewDataReturnInvNumber('0');	
        }
		

		// $idata = $workshop->GetDataToInsertNewInvPayment($item,$idcust);
		// echo 'data to insert into new_inv_payment: '.$idata['no_invoice']. '   '.$idata['harga'];
		$inipymt=false;
		if ($paid!='') {
			$inipymt = $db->InsertData('new_inv_payment',['ssource' => $noinv,'ipayamt' => $itdata['harga'],'ipaymeth' => '1','ddate' => $tanggal,'sstaff' => 'charlez']);
		}


		if ($itr) {
			echo 'data transaksi berhasil disimpan!'."<br>";
		}

		if ($ititm) {
			echo 'data item transaksi berhasil disimpan!'."<br>";
		}

		if ($ifc) {
			echo 'data kalender transaksi berhasil disimpan!'."<br>";
		}

		if ($noinv!='') {
			echo 'data invoice berhasil disimpan!'."<br>";
		}

		if ($inipymt) {
			echo 'data pembayaran invoice berhasil disimpan';
		}
		// $inpyinv = $db->InsertData('new_inv_payment',['' => '',]);
		
		
        
        
	}

	public static function InputExistingCustomer($idcust,$idusaha){
		$data = new Data();
		$data->Model('Querybuilder');
		$data->Model('Workshopmdl');
		$db = new QueryBuilder();
		$workshop = new Workshopmdl(); 

		if (isset($_POST['in'])) {
			$paid = '';
			if (isset($_POST['in']['paid'])&&!empty($_POST['in']['paid'])) {
				$paid = $_POST['in']['paid'];
			}
			// echo $paid;
			// print_r($_POST['in']);
			self::SyncTransaction(
				$_POST['in']['tanggal'],
				$_POST['in']['idworkshop'],
				$_POST['in']['idcust'],
				$_POST['in']['item'],
				$paid
			);
			
		}

		$datacust = $workshop->GetDataToInsertNewTrans($idcust);
		// print_r($datacust);

		$data->listtanggal = $db->FetchWhere(['tanggal','id'],
			'fac_calendar',
			array($db->GetCond('cal_status','=','231'),$db->GetCond('acara','=',$idusaha))
		);

		$data->listitem = $db->FetchWhere(['id','nama_item','harga'],
			'paket_training',
			array($db->GetCond('nama_item','LIKE','%workshop%'))
		);

		$data->idcust = $idcust;
		$data->idusaha = $idusaha;
		$data->namacust = $datacust['nama_cust'];
		$data->emailcust = $datacust['email_cust'];
		$data->telpcust=$datacust['telp_cust'];
		$data->namamarketing=$datacust['marketing_nama'];
		$data->namausaha=$datacust['nama_usaha'];
		$data->page = 'perusahaan';
		$data->title = 'FAC | Input Peserta Existing';
		$data->judul = 'Input Peserta Existing';
		$data->subtitle='Input Peserta Existing';
		$data->username = $_SESSION['admin']['nama'];
        $data->View('workshop/vinputexcust.workshop',$data);
	}

	public static function NewDate($idusaha){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		if (isset($_POST['in'])) {
			// print_r($_POST['in']);

			$r = $db->InsertData('fac_calendar',['acara'=>$idusaha,'tanggal' => $_POST['in']['tanggal'],'cal_status' => '231']);

			if ($r) {
				echo "<script>alert('Tanggal berhasil disimpan!')</script>";
				echo "<script>location.replace('".$data->base_url."administrasi/workshop/tambah-tanggal?id=".$idusaha."')</script>";
			}else{
				echo "<script>alert('Tanggal gagal disimpan!')</script>";
			}
		}

		$data->listtanggal = $db->FetchWhere(['tanggal','id'],
			'fac_calendar',
			array($db->GetCond('cal_status','=','231'),$db->GetCond('acara','=',$idusaha))
		);

		$data->page = 'new-transaksi-tanggal';
		$data->title = 'FAC | Input Tanggal Workshop';
		$data->judul = 'Input Tanggal Workshop';
		$data->subtitle='Input Tanggal Workshop';
		$data->username = $_SESSION['admin']['nama'];
        $data->View('workshop/vnewdate.workshop',$data);
	}

    public static function SearchPage(){
        $data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		
// 		print_r($data->listdata);
        $data->page = 'perusahaan';
		$data->title = 'FAC | Search Workshop';
		$data->judul = 'Search Workshop';
		$data->subtitle='Data Workshop';
		$data->username = $_SESSION['admin']['nama'];
        $data->View('workshop/vsearchentity.workshop',$data);
    }

    public static function ValidasiPeserta($idworkshop){
    	$data = new Data();
    	$data->Model('Workshopmdl');
		$db = new Workshopmdl();

		if (isset($_POST['in'])) {
			$idcust = $db->getCustomer($_POST['in']['phoneoremail']);
			if ($idcust!='') {
				header('location: '.$data->base_url.'administrasi/workshop/existingcust/'.$idcust.'/?id='.$idworkshop);
			}else{
				header('location: '.$data->base_url.'administrasi/workshop/pesertabaru?id='.$idworkshop);
			}
		}

    	$data->page = 'perusahaan';
		$data->title = 'FAC | Peserta Workshop';
		$data->judul = 'Validasi Peserta';
		$data->subtitle='Validasi Peserta Workshop';
		$data->username = $_SESSION['admin']['nama'];
        $data->View('workshop/vpeserta.workshop',$data);	
    }


    public static function PesertaBaru($idworkshop){
    	$data = new Data();
    	$data->Model('Querybuilder');
		$db = new QueryBuilder();

		if (isset($_POST['in'])) {
			// print_r($_POST['in']);
			$idcust=md5($_POST['in']['email']);
			$icust = $db->InsertData('new_customer',[
				'id_cust' => $idcust,
				'id_perusahaan' => $idworkshop,
				'nama_cust' => $_POST['in']['nama'],
				'gender_cust' => $_POST['in']['gender'],
				'email_cust' => $_POST['in']['email'],
				'telp_cust' => $_POST['in']['telepon'],
				'marketing_id' => $_POST['in']['marketing'],
				'status_cust' => '23'
			]);	

			if ($icust) {
				header('location: '.$data->base_url.'administrasi/workshop/existingcust/'.$idcust.'/?id='.$idworkshop);
			}
		}

		$data->listmarketing=$db->FetchWhere(['marketing_id','marketing_nama'],'new_marketing',array($db->GetCond('ivisib','=','1')));

    	$data->page = 'perusahaan';
		$data->title = 'FAC | Peserta Workshop';
		$data->judul = 'Peserta Baru Workshop';
		$data->subtitle='Peserta Workshop Non Existing Customer';
		$data->username = $_SESSION['admin']['nama'];
        $data->View('workshop/vnewpeserta.workshop',$data);
    }

    public static function ListWorkshop($name){
    	$data = new Data();
		$data->Model('Workshopmdl');
		$db = new Workshopmdl();

		$data->mylists = $db->getWorkshopname($name);

		$data->page = 'perusahaan';
		$data->title = 'FAC | Search Workshop';
		$data->judul = 'Search Workshop';
		$data->subtitle='Data Workshop';
		$data->username = $_SESSION['admin']['nama'];
        $data->View('workshop/vlistsname.workshop',$data);	
    }

}