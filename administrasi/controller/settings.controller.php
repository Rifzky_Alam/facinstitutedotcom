<?php include_once 'classes/View.php';
class Settings{

	public static function SoftDeleteTargetMarketing($id){
		$data = new Data();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$data->Model('Querybuilder');
		$sesi = new Sessionz();
		$db = new QueryBuilder();
		
		$sesi->AdminMarketing();

		$r = $db->UpdateData('marketing_target',
			[
				'marketing_target.istatus' => '0'
			],
			array($db->GetCond('marketing_target.sid','=',$id))
		);
		if ($r) {
			header('location:'.$data->base_url.'administrasi/marketing/data-targetharian');
		} else {
			header('location:'.$data->base_url.'administrasi/marketing/data-targetharian');
		}
	}
	// @url /administrasi/marketing/data-targetharian
	public static function TabelTargetMarketing(){
		$data = new Data();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$data->Model('Querybuilder');
		$sesi = new Sessionz();
		$db = new QueryBuilder();
		
		$sesi->AdminMarketing();

		$data->listdata = $db->FetchWhere(['*'],'marketing_target', array($db->GetCond('istatus','=','1')));

		// print_r($data->listdata);
		$data->judul = 'Tabel Target Marketing';
		$data->username = $_SESSION['admin']['nama'];
		$data->subtitle = 'Tabel Target Marketing';
		$data->page = 'penawaran';
		$data->View('marketing/vtbltarget.marketing',$data);
	}

	private static function ConfigTargetMarketingPost($masukdata){
		$data = new Data();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$data->Model('Querybuilder');
		$sesi = new Sessionz();
		$db = new QueryBuilder();
		
		$sesi->OnlyAdmin();

		$r = $db->InsertData('marketing_target',
			[
				'sid' => md5($masukdata['tanggal']),
				'ddate' => $masukdata['tanggal'],
				'iexprice' => $masukdata['hargaexpert'],
				'iextarget' => $masukdata['targetexpert'],
				'isupexprice' => $masukdata['hargasuperexpert'],
				'isupextarget' => $masukdata['targetsuperexpert'],
				'notes' => $masukdata['notes']
			]
		);

		if ($r) {
			echo "<script>alert('data berhasil disimpan!');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi');</script>";
		} else {
			
		}

	}
	// @url /administrasi/marketing/config-targetharian method=GET
	public static function ConfigTargetMarketing(){
		$data = new Data();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$sesi = new Sessionz();
		$sesi->OnlyAdmin();

		if (isset($_POST['in'])) {
			self::ConfigTargetMarketingPost($_POST['in']);
		}

		$data->judul = 'Konfigurasi Target Marketing';
		$data->username = $_SESSION['admin']['nama'];
		$data->subtitle = 'Konfigurasi Target Marketing';
		$data->page = 'penawaran';

		$data->View('marketing/vconfigtarget.marketing',$data);
	}

	public static function Widget(){
		
	}

	public static function ChangeStaffStatus($username,$status){
		$data = new Data();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$data->Model('Querybuilder');
		$sesi = new Sessionz();
		$db = new QueryBuilder();
		
		$sesi->OnlyAdmin();
		if ($status=='0') {
			$r = $db->UpdateData('users',['status' => 'inactive'],array($db->GetCond('username','=',$username)));
		} elseif ($status=='1') {
			$r = $db->UpdateData('users',['status' => 'aktif'],array($db->GetCond('username','=',$username)));
		}
		
		if ($r) {
			echo "<script>alert('Status berhasil diubah!');</script>";
			echo "<script>location.replace('https://fac-institute.com/administrasi/settings/petugas');</script>";
		}else{
			echo "<script>alert('Status gagal diubah!');</script>";
			echo "<script>location.replace('https://fac-institute.com/administrasi/settings/petugas');</script>";
		}

	}

	public static function Petugas(){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$db->setOrder('nama','ASC');
		$data->listpetugas = $db->FetchJoin(['*'],'users',array($db->getJoin('INNER','intensif_lvl_trainer','intr_id','usr_level')));
		// print_r(json_encode($listpetugas));

		$data->header = 'Data Petugas FAC Institute';
		$data->judul = 'FAC || Data Petugas FAC _institute';
		$data->username = $_SESSION['admin']['nama'];

		$data->View('settings/petugas/vlistadmin.petugas',$data);
		// echo 'petugas!';
	}
}