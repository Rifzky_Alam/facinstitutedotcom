<?php include_once 'classes/View.php';
class Intensif{
	//@url(value="administrasi/insentif/edit-jnskegiatan", method=GET)
	public static function EditJenisKegiatan($id,$status){
		if ($id!=''&&$status!='') {
			$data=new Data();
			$data->Model('Querybuilder');
			$db = new QueryBuilder();

			$r = $db->UpdateData('daftar_keg_laporan',['dkl_insentif' => $status],array($db->GetCond('dkl_id','=',$id)));
			if ($r) {
				header('location: '.$data->base_url.'administrasi/insentif/config-jeniskegiatan');
			} else {
				header('location: '.$data->base_url.'administrasi/insentif/config-jeniskegiatan');
			}	
		}else{
			header('location: '.$data->base_url.'administrasi/insentif/config-jeniskegiatan');
		}

	}

	//@url(value="administrasi/insentif/config-jeniskegiatan", method=GET)
	public static function SettingJenisKegiatan(){
		$data=new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		$data->headertext = "Insentif - Jenis Kegiatan";
		$data->judul = 'FAC || Insentif Jenis Kegiatan';
		$data->username = $_SESSION['admin']['nama'];
		$data->listdata = $db->Fetch('daftar_keg_laporan');

		$data->View('insentif/jeniskegiatan/vjeniskegiatantbl.ins',$data);

	}	

	public static function DeleteJenisTransaksi($id){
		
	}

	//@url(value="administrasi/insentif/edit-jnstrans", method=GET)
	public static function EditStatusJnsTrans($id,$status){
		$data=new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		$r = $db->UpdateData('intensif_jns_trans',
			[
				'ijt_status' => $status
			],
			array($db->GetCond('ijt_jns_trans','=',$id))
		);
		echo 'oke';
		if ($r) {
			header('location: '.$data->base_url.'administrasi/insentif/config-jnstransaksi');
		} else {
			header('location: '.$data->base_url.'administrasi/insentif/config-jnstransaksi');
		}
	}

	// @url(value="administrasi/insentif/new-jnstrans", method=POST)
	public static function NewJenisTransaksiPost($masukdata){
		$data=new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		// print_r($masukdata);
		$r = $db->InsertData('intensif_jns_trans',
			[
				'ijt_jns_trans' => $masukdata['jnstrans'],
				'ijt_acc' => $masukdata['acc'],
				'ijt_hari' => $masukdata['harikerja'],
				'ijt_wilayah' => $masukdata['wilayah']
			]
		);
		if ($r) {
			header('location:'.$data->base_url.'administrasi/insentif/config-jnstransaksi');
		}else{
			echo "<script>alert('Data gagal disimpan');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi/insentif/config-jnstransaksi');</script>";
		}
	}

	// @url(value="administrasi/insentif/new-jnstrans", method=GET)
	public static function NewJenisTransaksi(){
		$data=new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		$data->headertext = "Insentif - Jenis Transaksi Baru";
		$data->judul = 'FAC || Jenis Transaksi Insentif';
		$data->username = $_SESSION['admin']['nama'];
		$data->listdata = $db->Fetch('new_jenis_transaksi');

		$data->View('insentif/jenistransaksi/vnewjnstrans.ins',$data);
	}

	// @url(value="administrasi/insentif/config-jnstransaksi", method=GET)
	public static function TabelJenisTransaksi(){
		$data=new Data();
		$data->Model('Querybuilder');
		$data->Model('Insentif');
		$db = new QueryBuilder();
		$insentif = new Insentif();
		$data->headertext = "Insentif - Jenis Transaksi";
		$data->judul = 'FAC || Daftar Jenis Transaksi';
		$data->username = $_SESSION['admin']['nama'];
		$data->datatrans = count($db->Fetch('new_jenis_transaksi'));
		// echo $data->datatrans;
		$data->listdata = $insentif->TabelInsentifJenisTransaksi();

		$data->View('insentif/jenistransaksi/vtbljenistrans.ins',$data);
	}


	public static function Dasbor(){
		$data=new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		$data->headertext = "Insentif - Dasbor";
		$data->judul = 'FAC || Dasbor Insentif';
		$data->username = $_SESSION['admin']['nama'];
		$data->View('insentif/vdasbor.ins',$data);
	}

	// @url(value="administrasi/insentif/deldawil?id={val}", method=GET)
	public static function DeleteDawil($id){
		$data=new Data();
		$data->Model('Insentif');
		$data->Model('Querybuilder');
		$qb = new QueryBuilder();
		$db = new Insentif();
		$jumlahdata = $db->GetTotalDawilInLaporan($id);
		// echo 'kokgaada';
		// echo $jumlahdata;
		if ($jumlahdata=='0') {
			$r = $qb->DeleteWhere('intensif_tbl_wil',array($qb->GetCond('itw_id','=',$id)));
			if ($r) {
				echo "<script>alert('Data berhasil dihapus.');</script>";
				echo "<script>location.replace('".$data->base_url."administrasi/insentif/config-tbldawil');</script>";
			}else {
				echo "<script>alert('DBERROR: Data gagal dihapus');</script>";
				echo "<script>location.replace('".$data->base_url."administrasi/insentif/config-tbldawil');</script>";
			}
		}else {
			echo "<script>alert('Data tidak bisa di hapus karena sudah masuk ke dalam transaksi dan laporan.');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi/insentif/config-tbldawil');</script>";
		}
	}

	// @url(value="administrasi/insentif/config-tbldawil", method=GET)
	public static function TabelDaftarDawil(){
		$data=new Data();
		$data->Model('Insentif');
		$db = new Insentif();

		$data->listdata = $db->TabelDaftarWilayah();
		// print_r($data->listdata);
		$data->headertext = "Insentif - Daftar Daerah Wilayah";
		$data->judul = 'FAC || Daftar Daerah Wilayah';
		$data->username = $_SESSION['admin']['nama'];
		$data->View('insentif/dawil/vdaftardawil.ins',$data);
	}

	// @url(value="administrasi/insentif/new-daftardawil", method=POST)
	public static function NewDaftarDawilPost($masukdata){
		$data=new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$r = $db->InsertData('intensif_tbl_wil',
				[
					'itw_kecamatan' => $masukdata['sbd'],
					'itw_kota' => $masukdata['kota'],
					'itw_provinsi' => $masukdata['prov'],
					'itw_wil_kode' => $masukdata['dawil']
				]
		);
		if ($r) {
			header('location: '.$data->base_url.'administrasi/insentif/config-tbldawil');
		} else {
			header('location: '.$data->base_url.'administrasi/insentif/config-tbldawil');
		}
	}

	// @url(value="administrasi/insentif/new-daftardawil", method=GET)
	public static function NewDaftarDawil(){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		$data->listdata = $db->FetchWhere(['*'],'intensif_dawil',array($db->GetCond('ind_status','=','1')));
		$data->headertext = "Insentif - Daftar Daerah Wilayah";
		$data->judul = 'FAC || Daftar Daerah Wilayah';
		$data->username = $_SESSION['admin']['nama'];
		$data->View('insentif/dawil/vnewdaftarwil.ins',$data);
	}
	// @url(value="administrasi/insentif/deleteharlibnas", method=GET)
	public static function HapusHarlibnas($id){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		$r = $db->DeleteWhere('harlibnas', array($db->GetCond('hlb_id','=',$id)));
		if ($r) {
			header('location: '.$data->base_url.'administrasi/insentif/config-harlibnas');
		} else {
			header('location: '.$data->base_url.'administrasi/insentif/config-harlibnas');
		}
	}

	// @url(value="administrasi/insentif/config-harlibnas", method=GET)
	public static function TabelHarlibnas(){
		$data=new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		$data->listdata = $db->Fetch('harlibnas');

		$data->headertext = "Insentif - Tabel Hari Libur Nasional";
		$data->judul = 'FAC || Tabel Hari Libur Nasional';
		$data->username = $_SESSION['admin']['nama'];
		$data->View('insentif/harlibnas/vtblharlibnas.ins',$data);
	}

	// @url(value="administrasi/insentif/new-harlibnas", method=POST)
	public static function NewHarlibnasPost($masukdata){
		$data=new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$r = $db->InsertData('harlibnas',
				['hlb_tanggal' => $masukdata['tanggal'],'hlb_keterangan' => $masukdata['ketlibur']]);
		if ($r) {
			header('location: '.$data->base_url.'administrasi/insentif/config-harlibnas');
		} else {
			header('location: '.$data->base_url.'administrasi/insentif/config-harlibnas');
		}
	}

	// @url(value="administrasi/insentif/new-harlibnas", method=GET)
	public static function NewHarlibnas(){
		$data=new Data();
		$data->headertext = "Insentif - Hari Libur Nasional";
		$data->judul = 'FAC || Hari Libur Nasional';
		$data->username = $_SESSION['admin']['nama'];
		$data->datepicker = '1';
		$data->View('insentif/harlibnas/vnewharlibnas.ins',$data);
	}

	//@url(value="administrasi/insentif/edit-statusdawil", method=GET)
	public static function EditStatusDawil($id,$status){
		$data=new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		$r = $db->UpdateData('intensif_dawil',
			[
				'ind_status' => $status
			],
			array($db->GetCond('ind_id','=',$id))
		);
		echo 'oke';
		if ($r) {
			header('location: '.$data->base_url.'administrasi/insentif/config-dawil');
		} else {
			header('location: '.$data->base_url.'administrasi/insentif/config-dawil');
		}
	}

	//@url(value="administrasi/insentif/config-dawil", method=GET)
	public static function TabelDawil(){
		$data = new Data();
		$data->Model('Insentif');
		$db = new Insentif();

		$data->listdata = $db->FetchDawil();
		$data->headertext = "Insentif - Tabel Insentif by Dawil";
		$data->judul = 'FAC || Tabel Insentif By Daerah Wilayah';
		$data->username = $_SESSION['admin']['nama'];
		$data->View('insentif/dawil/vtbldawil.ins',$data);
	}

	//@url(value="administrasi/insentif/new-dawil", method=POST)	
	public static function NewDawilPost($masukdata){
		$data=new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		$jumlah = $db->FetchCount('ind_id','intensif_dawil',
			array($db->GetCond('ind_status','=','1'),$db->GetCond('ind_wilayah','=',$masukdata['wilayah'])));

		if ($jumlah == '0') {
			$r = $db->InsertData('intensif_dawil',
				['ind_wilayah' => $masukdata['wilayah'],'ind_tariff' => $masukdata['tarif']]);
			if ($r) {
				header('location: '.$data->base_url.'administrasi/insentif/config-dawil');
			} else {
				header('location: '.$data->base_url.'administrasi/insentif/config-dawil');
			}
		}else{
			echo "<script>alert('Data Ganda: Harap menon-aktifkan data insentif peran yang lama terlebih dahulu')</script>;";
			echo "<script>location.replace('".$data->base_url."administrasi/insentif/config-peran')</script>;";
		}
	}

	//@url(value="administrasi/insentif/new-dawil", method=GET)
	public static function NewDawil(){
		$data=new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		$data->listperan = $db->Fetch('peran_kerja');
		// print_r($data->listperan);
		$data->headertext = "Insentif - Insentif by Dawil";
		$data->judul = 'FAC || Insentif By Daerah Wilayah';
		$data->username = $_SESSION['admin']['nama'];
		$data->View('insentif/dawil/vnewdawil.ins',$data);

	}

	public static function EditStatusPeran($id,$status){
		$data=new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		$r = $db->UpdateData('intensif_peran',
			[
				'ip_status' => $status
			],
			array($db->GetCond('ip_id','=',$id))
		);
		echo 'oke';
		if ($r) {
			header('location: '.$data->base_url.'administrasi/insentif/config-peran');
		} else {
			header('location: '.$data->base_url.'administrasi/insentif/config-peran');
		}
	}

	//@url(value="administrasi/insentif/config-peran", method=GET)
	public static function PeranTable(){
		$data=new Data();
		$data->Model('Insentif');
		$db = new Insentif();
		$data->listdata = $db->FetchPeran();
		// print_r($data->listdata);
		$data->headertext = "Insentif - Tabel Insentif by peran";
		$data->judul = 'FAC || Tabel Insentif By Peran';
		$data->username = $_SESSION['admin']['nama'];
		$data->View('insentif/peran/vtblperan.ins',$data);
	}

	//@url(value="administrasi/insentif/new-peran", method=POST)
	public static function NewPeranPost($masukdata){
		$data=new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		$jumlah = $db->FetchCount('ip_id','intensif_peran',
			array($db->GetCond('ip_status','=','1'),$db->GetCond('ip_peran','=',$masukdata['peran'])));

		if ($jumlah == '0') {
			$r = $db->InsertData('intensif_peran',
				['ip_peran' => $masukdata['peran'],'ip_tariff' => $masukdata['perseninsentif']/100]);
			if ($r) {
				header('location: '.$data->base_url.'administrasi/insentif/config-peran');
			} else {
				header('location: '.$data->base_url.'administrasi/insentif/config-peran');
			}
		}else{
			echo "<script>alert('Data Ganda: Harap menon-aktifkan data insentif peran yang lama terlebih dahulu')</script>;";
			echo "<script>location.replace('".$data->base_url."administrasi/insentif/config-peran')</script>;";
		}
	}


	//@url(value="administrasi/insentif/new-peran", method=GET)
	public static function NewPeran(){
		$data=new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		$data->listperan = $db->Fetch('daftar_peran_kerja');
		// print_r($data->listperan);
		$data->headertext = "Insentif - Insentif by peran";
		$data->judul = 'FAC || Insentif By Peran';
		$data->username = $_SESSION['admin']['nama'];
		$data->View('insentif/peran/vnewperan.ins',$data);

	}

	//@url(value="administrasi/insentif/edit-statusjnsaccurate")
	public static function EditStatusHariKerja($id,$status){
		$data=new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		$r = $db->UpdateData('intensif_hari_kerja',
			[
				'ihk_status' => $status
			],
			array($db->GetCond('ihk_id','=',$id))
		);
		echo 'oke';
		if ($r) {
			header('location: '.$data->base_url.'administrasi/insentif/config-harikerja');
		} else {
			header('location: '.$data->base_url.'administrasi/insentif/config-harikerja');
		}
	}

	// @url (value="/administrasi/insentif/new-harikerja", method=POST)
	public static function NewHariKerjaPost($masukdata){
		$data=new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		//harus ada verifikasi bahwa hari kerja tidak ada yang valid baru bisa input data baru
		$jumlah = $db->FetchCount('ihk_id','intensif_hari_kerja',
			array($db->GetCond('ihk_status','=','1'),$db->GetCond('ihk_jenis_hari','=',$masukdata['jnshari'])));
		if ($jumlah=='0') {
			$r = $db->InsertData('intensif_hari_kerja',
				['ihk_jenis_hari' => $masukdata['jnshari'],'ihk_tariff' => $masukdata['tarif']]);
			if ($r) {
				header('location: '.$data->base_url.'administrasi/insentif/config-harikerja');
			} else {
				header('location: '.$data->base_url.'administrasi/insentif/config-harikerja');
			}
		}else{
			echo "<script>alert('Data Ganda: Harap menon-aktifkan data jenis hari yang ada terlebih dahulu')</script>;";
			echo "<script>location.replace('".$data->base_url."administrasi/insentif/config-harikerja')</script>;";
		}

	}

	// @url (value="/administrasi/insentif/new-harikerja", method=GET)
	public static function NewHariKerja(){
		$data=new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		$data->listjenishari = $db->Fetch('intensif_jns_hr_kerja');

		$data->headertext = "Insentif - Tarif Jenis Hari Baru";
		$data->judul = 'FAC || Tarif Jenis Hari Baru';
		$data->username = $_SESSION['admin']['nama'];
		$data->View('insentif/jenishari/vnewtarifjnshari.ins',$data);		
	}

	//@url(value="administrasi/insentif/edit-statusjnsaccurate" method=GET)
	public static function EditStatusJnsAccurate($id,$status){
		$data=new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		$r = $db->UpdateData('intensif_jenis_acc',
			[
				'ija_status' => $status
			],
			array($db->GetCond('ija_id','=',$id))
		);
		echo 'oke';
		if ($r) {
			header('location: '.$data->base_url.'administrasi/insentif/config-jenisaccurate');
		} else {
			header('location: '.$data->base_url.'administrasi/insentif/config-jenisaccurate');
		}
	}

	//@url (value="administrasi/insentif/config-harikerja" method=GET)
	public static function DaftarTariffHariKerja(){
		$data = new Data();
		$data->Model('Insentif');
		$db = new Insentif();

		$data->listharikerja = $db->FetchHariKerja();
		
		$data->headertext = "Insentif - Tabel Tarif Jenis Hari";
		$data->judul = 'FAC || Tabel Tarif Jenis Hari';
		$data->username = $_SESSION['admin']['nama'];
		$data->View('insentif/jenishari/vtbljnshari.ins',$data);
	}

	//@url (value = "/administrasi/insentif/config-jenisaccurate",method=GET)
	public static function DataJenisAccurate(){
		$data = new Data();
		$data->Model('Insentif');
		$db = new Insentif();

		$data->listdata = $db->FetchJenisAccurate();

		$data->headertext = "Insentif - Tabel Level Trainer";
		$data->judul = 'FAC || Tabel Level Trainer';
		$data->username = $_SESSION['admin']['nama'];

		$data->View('insentif/jenisaccurate/vtblversiacc.ins',$data);
	}

	// @url (value="administrasi/intensif/edit-jenisaccurate/{id}",method="POST")	
	public static function NewJenisAccuratePost($masukdata){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		$jumlah = $db->FetchCount('ija_id','intensif_jenis_acc',
			array(
				$db->GetCond('ija_status','=','1'),
				$db->GetCond('ija_lvl_trainer','=',$masukdata['leveltrainer']),
				$db->GetCond('ija_jns_acc','=',$masukdata['jenisaccurate'])
			)
		);
		echo $jumlah;
		
		if ($jumlah=='0') {
			$r = $db->InsertData('intensif_jenis_acc',
				[
					'ija_jns_acc' => $masukdata['jenisaccurate'],
					'ija_lvl_trainer' => $masukdata['leveltrainer'],
					'ija_tariff' => $masukdata['insentif'],
				]
			);
			if ($r) {
				header('location: '.$data->base_url.'administrasi/insentif/config-jenisaccurate');
			} else {
				header('location: '.$data->base_url.'administrasi/insentif/config-jenisaccurate');
			}
		} else {
			echo "<script>alert('Data Ganda: Harap menon-aktifkan data jenis accurate-leveltrainer yang ada terlebih dahulu')</script>;";
			echo "<script>location.replace('".$data->base_url."administrasi/insentif/config-jenisaccurate')</script>;";
		}
	}

	// @url (value="administrasi/intensif/edit-jenisaccurate/{id}")
	public static function EditJenisAccurate(){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		$data->listacc = $db->Fetch('versi_accurate');
		// print_r($data->listacc);
		// echo '<br>';
		$data->listleveltrainer = $db->FetchWhere(['*'],'intensif_lvl_trainer',array($db->GetCond('intr_status','=','1')));
		// print_r($data->listleveltrainer);
		$data->headertext = "Insentif - Tabel Level Trainer";
		$data->judul = 'FAC || Tabel Level Trainer';
		$data->username = $_SESSION['admin']['nama'];
		$data->View('insentif/jenisaccurate/vinputjenisacc.ins',$data);
	}

	//@url (value="/administrasi/insentif/config-leveltrainer")
	public static function DataLevelTrainer(){
		$data = new Data();
		$data->Model('Insentif');
		$db = new Insentif();

		$data->listdata = $db->FetchLevelTrainer();

		$data->headertext = "Insentif - Tabel Level Trainer";
		$data->judul = 'FAC || Tabel Level Trainer';
		$data->username = $_SESSION['admin']['nama'];

		$data->View('insentif/leveltrainer/vtbllvltrainer.ins',$data);
	}

	public static function NewLevelTrainerPost($postdata){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		$r = $db->InsertData('intensif_lvl_trainer',['intr_level'=>$postdata['level']]);
		if ($r) {
			header('location: '.$data->base_url.'administrasi/insentif/config-leveltrainer');
		} else {
			header('location: '.$data->base_url.'administrasi/insentif/config-leveltrainer');
		}
	}

	public static function NewLevelTrainer(){
		$data = new Data();


		$data->headertext = "Insentif - Level Trainer Baru";
		$data->judul = 'FAC || Input Level Trainer';
		$data->username = $_SESSION['admin']['nama'];

		$data->View('insentif/leveltrainer/vinputlvltrainer.ins',$data);
	}

	//@url(value="/administrasi/insentif/edit-statusleveltrainer/{id}?st={val}")
	public static function EditLevelTrainer($id,$status){
		$data=new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		$r = $db->UpdateData('intensif_lvl_trainer',
			[
				'intr_status' => $status
			],
			array($db->GetCond('intr_id','=',$id))
		);
		echo 'oke';
		if ($r) {
			header('location: '.$data->base_url.'administrasi/insentif/config-leveltrainer');
		} else {
			header('location: '.$data->base_url.'administrasi/insentif/config-leveltrainer');
		}
	}


	//@url (value="/administrasi/intensif/new-jenisaccurate")
	public static function InsertNewIntensJenisAcc(){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$r = $db->InsertData(
			'intensif_jenis_acc',
			[
				'ija_jns_acc' => 'Standard',
				'ija_lvl_trainer' => '1',
				'ija_tariff' => '50000',
				'ija_status' => '1'
			]
		);
		if ($r) {
			echo 'inserted!';
		} else {
			echo 'rejected!';
		}
	}

	//@url (value="/administrasi/intensif/new-libnas")
	public static function InsertNewLibnas(){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$r = $db->InsertData(
			'intensif_data_libnas',
			[
				'intlb_tanggal' => date('Y-m-d'),
				'intlb_keterangan' => 'Libur coba aja'
			]
		);
		if ($r) {
			echo 'inserted!';
		} else {
			echo 'rejected!';
		}
	}

	//@url (value="/administrasi/intensif/new-wilayah")
	public static function InsertNewWilayah(){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$r = $db->InsertData(
			'intensif_tbl_wil',
			[
				'itw_kecamatan' => '17',
				'itw_kota' => '41',
				'itw_provinsi' => '12',
				'itw_wil_kode' => '1',
				'itw_status' => '1'
			]
		);
		if ($r) {
			echo 'inserted!';
		} else {
			echo 'rejected!';
		}
	}

	//@url (value="/administrasi/intensif/new-wilayah")
	public static function InsertNewHariKerja(){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$r = $db->InsertData(
			'intensif_hari_kerja',
			[
				'ihk_jenis_hari' => '3', // FK from table insentif_jns_hr_kerja
				'ihk_tariff' => '50000',
				'ihk_status' => '1'
			]
		);
		if ($r) {
			echo 'inserted!';
		} else {
			echo 'rejected!';
		}
	}

}