<?php include_once '/home/facinsti/public_html/administrasi/classes/View.php';
class Tutorial{
	public static function DeleteTutorial($id){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$sesi = new Sessionz();
		$sesi->OnlyAdmin();
		$data->subtitle = "Input TO-DO";
		$data->username = $_SESSION['admin']['nama'];
		$victim = $db->FetchOneRow(['judul'],'tutorial',array(
				$db->GetCond('id','=',$id)
			)
		);
		$data->judultut = $victim['judul'];
		if (count($victim)<1) {
			echo "<script>alert('Tidak ada data untuk ID ini.');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi/cms/tutorial-table');</script>";
		}else{
			$dr = $db->DeleteWhere('tutorial',array(
				$db->GetCond('id','=',$id)
			));
			$r = $db->DeleteWhere('tags',array($db->GetCond('fi_t_source','=',$id)));
			if ($dr) {
				$data->msg = 'Berhasil Dihapus';
			}else{
				$data->msg = 'Gagal dihapus';
			}

			$data->view('tutorial/vdelsuccess.tut', $data);	
		}
	}

	public static function DeleteThumbnail($idtuts){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$folder = $db->FetchOneRow(['folder_path'],'folders',array($db->GetCond('folder_relation_data','=','tuts_thumbnails')));
		$datatut = $db->FetchOneRow(['thumbnail'],'tutorial',array($db->GetCond('id','=',$idtuts)));
		// print_r($datatut);
		// print_r($folder);
		$ur = $db->UpdateData('tutorial',['thumbnail'=>'-'],array(
			$db->GetCond('id','=',$idtuts)
		));
		if ($ur) {
			@unlink($data->homedir.$folder['folder_path'].$datatut['thumbnail']);
			header('location:'.$data->base_url.'administrasi/cms/tutorial-table');
		} else {
			echo "<script>alert('thumbnail tidak terhapus!');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi/cms/tutorial-table');</script>";
		}
	}


	public static function HapusTag($tag){ //@url value="/administrasi/cms/tags/removetag/${data}"
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		if (isset($_POST['rmvtag'])) {
			// echo $_POST['rmvtag'].' Ready to remove';
			$r = $db->DeleteWhere('tags',array($db->GetCond('fi_t_idtag','=',$_POST['rmvtag'])));
			$r1 = $db->DeleteWhere('tags_desc',array($db->GetCond('tags_id','=',$_POST['rmvtag'])));
			if ($r&&$r1) {
				echo "<script>alert('Data dihapus!');</script>";
				echo "<script>location.replace('".$data->base_url."administrasi/cms/tags/lists');</script>";
			}else {
				echo "<script>alert('Data gagal dihapus, kontak admin sistem!');</script>";
			}

		}

		$data->subtitle = 'Tutorial Tag';
		$data->tagid = $tag;
		$data->username = $_SESSION['admin']['nama'];
		$data->view('tutorial/vremovetag.tut',$data);	
	}

	public static function AddTags(){
		$data = new Data();
		$data->Model('Querybuilder');
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$sesi = new Sessionz();
		$sesi->Staff();
		$db = new QueryBuilder();
		if (isset($_POST['tagname'])) {
			$chk = $db->FetchCount('tags_txt','tags_desc',array(
				$db->GetCond('tags_txt','=',$_POST['tagname'])
			));
			if ($chk=='0') {
				$r = $db->InsertData('tags_desc',['tags_txt'=>$_POST['tagname']]);
			} else {
				$r = false;
			}
				
			
			if ($r) {
				header('location:https://fac-institute.com/administrasi/cms/tutorial-table');
			}else{
				echo "<script>alert('Data gagal di input, Kemungkinan terjadi duplikasi data atau database error!');</script>";
				echo "<script>location.replace('".$data->base_url."administrasi/cms/tags/addnew');</script>";
			}
		}
		$data->subtitle = 'Tutorial Tag';
		$data->username = $_SESSION['admin']['nama'];
		$data->view('tutorial/vnewtag.tuts',$data);
	}

	public static function SelectTags($idtuts){ // edittag/${data}
		$data = new Data();
		$data->Model('Querybuilder');
		$data->Model('Tutorial');
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$sesi = new Sessionz();
		$db = new QueryBuilder();
		$tuts = new ControlTutorial();
		$sesi->Staff();
		$data->deftag = $db->FetchColumns(['tags_id','tags_txt'],'tags_desc');
		$data->tags = $db->FetchOneRow(['tags'],'tutorial',array($db->GetCond('id','=',$idtuts)));

		if (isset($_POST['tag'])) {
			for ($i = 0; $i < count($_POST['tag']); $i++) {
				$hasil = $db->FetchCount('fi_t_id','tags',array(
					$db->GetCond('fi_t_source','=',$idtuts),
					$db->GetCond('fi_t_idtag','=',$_POST['tag'][$i])
				));
				if ($hasil=='0') {
					$db->InsertData('tags',['fi_t_source' => $idtuts,'fi_t_idtag' => $_POST['tag'][$i]]);
				}
			}
			
			
		}
		$data->newtags = $tuts->GetInstalledTags($idtuts);
		// print_r($data->newtags);
		$data->idtus = $idtuts;
		$data->subtitle = 'Tutorial Tag';
		$data->username = $_SESSION['admin']['nama'];
		$data->view('tutorial/vchoosetag.tuts',$data);
	}

	public static function ClearOldTags($id){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$r = $db->UpdateData('tutorial',['tags'=>''],array($db->GetCond('id','=',$id)));
		if ($r) {
			header('Location:'.$data->base_url.'administrasi/cms/tutorial-table');
		} else {
			echo "<script>alert('Data gagal di hapus!');</script>";
			echo "<script>location.replace('".$data->base_url."'administrasi/cms/tutorial-table);</script>";
		}
	}

	public static function RemoveTag($source){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$r = $db->DeleteWhere('tags',array($db->GetCond('fi_t_id','=',$source)));
		if ($r) {
			header('Location:'.$data->base_url.'administrasi/cms/tutorial-table');
		} else {
			echo "<script>alert('Data gagal di hapus!');</script>";
			echo "<script>location.replace('".$data->base_url."'administrasi/cms/tutorial-table);</script>";
		}
	}

	// @url value="/administrasi/tags/lists"
	public static function GetTagLists(){ // tags table list
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$data->tags = $db->FetchColumns(['tags_id','tags_txt'],'`tags_desc`');
		$data->subtitle = 'Tutorial Tag';
		$data->username = $_SESSION['admin']['nama'];
		$data->view('tutorial/vdisplaytag.tuts',$data);
	}

	public static function GetAjaxTags($tagname){ // API
		$data = new Data();
		$data->Model('Tutorial');
		$tuts = new ControlTutorial();
		$r = $tuts->GetTagName($tagname);
		if (count($r)>'0') {
			$arr = [
			    'status' => ['code'=>'1','description'=>'data is available'],
			    'data'=>$r
			];
		}else{
			$arr = [
			    'status' => ['code'=>'0','description'=>'Database connection error!'],
			    'data' => [] 
			];
		}	
		echo json_encode($arr);
		
	}
	// @url: /api/tutsvc?src
	public static function GetAjaxSrcAPI($key){
		header('Content-Type: application/json');
		$data = new Data();
		$data->Model('Tutorial');
		$data->Model('Querybuilder');

		$tuts = new ControlTutorial();
		$db = new QueryBuilder();

		$r = $tuts->searchAllDataAPI($key);

		$folder = $db->FetchOneRow(['folder_path'],'folders',array($db->GetCond('folder_relation_data','=','tuts_thumbnails')));

		if (count($r)>'0') {
			$arr = [
			    'status' => ['code'=>'1','description'=>'data is available'],
			    'folderthumbnail' => $data->base_url.$folder['folder_path'],
			    'data'=>$r
			];
		}else {
			$arr = [
			    'status' => ['code'=>'0','description'=>'Database connection error!'],
			    'data' => [] 
			];
		}
		echo json_encode($arr);
	}

	// @url: ("/api/tutsvc?tagname")
	public static function GetAjaxTagsAPI($tagname){
		header('Content-Type: application/json; charset=ISO-8859-1');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		// echo $tagname;
		// print_r($_SERVER);
		$data = new Data();
		$data->Model('Querybuilder');
		$data->Model('Tutorial');
		$tuts = new ControlTutorial();
		$db = new QueryBuilder();
		$r = $tuts->GetTagNameAPI($tagname);
		// print_r($r);
		$folder = $db->FetchOneRow(['folder_path'],'folders',array($db->GetCond('folder_relation_data','=','tuts_thumbnails')));

		if (count($r)>'0') {
			$arr = [
			    'status' => ['code'=>'1','description'=>'data is available'],
			    'folderthumbnail' => 'adkafkdsjf',//$data->base_url.$folder['folder_path'],
			    'totaldata' => count($r),
			    'data'=> $r
			];
		}else{
			$arr = [
			    'status' => ['code'=>'0','description'=>'Database connection error!'],
			    'data' => [] 
			];
		}	
		// $res = json_encode($arr);
		// echo $arr;
		// print_r($r);
		// $arr = ['jdfkshfakdshs','bikin pusing aja lo'];
		echo json_encode($arr);
	}

	// @url: ("/api/tutsvc?svc=lists")
	public static function GetAjaxListTagsAPI(){
		header('Content-Type: application/json;charset=ISO-8859-1');
		$data = new Data();
		$data->Model('Tutorial');
		$tuts = new ControlTutorial();
		$r = $tuts->ListTagsAPI();
		
		if (count($r)>'0') {
			$arr = [
			    'status' => ['code'=>'1','description'=>'data is available'],
			    'data'=>$r
			];
		}else{
			$arr = [
			    'status' => ['code'=>'0','description'=>'Database connection error!'],
			    'data' => [] 
			];
		}	
		// print_r($r);
		echo json_encode($arr);
	}

}