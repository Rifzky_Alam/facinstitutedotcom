<?php include_once 'classes/View.php';
class Absen{
	

	public static function SendEmail($idabsen,$idusaha){
		$data = new Data();
		$data->Model('Petugas');
		$data->Model('Querybuilder');
		$data->Model('Mail');
		$data->JustInclude('administrasi/view/absensi/vemailformarketing.absensi');
		$data->JustInclude('administrasi/view/absensi/vemailmaster.absensi');
		$qb = new QueryBuilder();
		$db = new ControlPetugas();
		$iemail = new FACMail();
		$requireddata = $db->MarketingEmailSystem($idusaha,date('Y-m-d'));
		
		//sending email here
		

		if (count($requireddata)!='0'||$requireddata->email!='') {
		    $contentemail = HeadMail();
		    $contentemail .= EmailContent($requireddata);
		    $contentemail .= Footer();
		    @$data->to = $requireddata->email;
		    @$data->subject = 'Informasi Training '. $requireddata->perusahaan;
		    @$data->content = $contentemail;
		    $data->cc = ['fajar@fac-institute.com','training.facinstitute@gmail.com','ceso.facinstitute@gmail.com'];
			if (@$iemail->sendMailWithCC($data)) {
				$qb->UpdateData(
				'absensi',
					['absen_status' => '9'], //status = 9 (sudah di kirim email ke marketing tersebut)
					array($qb->GetCond('id','=',$idabsen))
				);	
			}
		}
		//insert log_data
		$logsuccess = $qb->InsertData('log_data',['username' => $_SESSION['admin']['username'],'log_action' => 'Successfully attended','log_ip' => $_SERVER['REMOTE_ADDR']]);
		//end sending email

		//update absen status after successfully sending email
		/*$qb->UpdateData(
		'absensi',
			['absen_status' => '9'],
			array($qb->GetCond('id','=',$idabsen))
		);*/

		// end update status

		/*$db->FetchJoinWhere(
			['perusahaan', 'nm.marketing_email AS email', 'petugas'],
			'acara_harian AS ah',
			array(
				$db->getJoin('INNER','new_transaksi AS nt','nt.trans_id','ah.transaksi'),
				$db->getJoin('INNER','new_customer AS nc','nc.id_cust','nt.trans_cust_id'),
				$db->getJoin('INNER','new_marketing AS nm','nm.marketing_id','ah.marketing_id'),
			),
			array(
				$db->GetCond('ah.id','=','53f23f13ad89bbe49db2acd2bd58ddfe'),
				$db->GetCond('ah.tanggal','=','2017-10-25')
			)
		);*/
		// echo $contentemail;
		// print_r($listpetugas);

		// echo 'petugas!';
	}
}