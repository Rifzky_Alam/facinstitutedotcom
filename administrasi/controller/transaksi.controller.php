<?php include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/classes/View.php';
class Transaksi{
    // @url /client/penawaran/{value}
    public static function FormPenawaranClient($idtransaksi){
        $data = new Data();
        $data->Model('Quotation');
        $data->Model('Querybuilder');
        $quot = new Quotation();
        $db = new QueryBuilder();
        $data->Func('Fsecurity');
        
        if(isset($_POST['in'])){
            for ($i = 0; $i < count($_POST['in']['id']); $i++) {
				// echo 'ID: '.$_POST['in']['id'][$i].' qty:'.$_POST['in']['qty'][$i]."<br>";
				if($_POST['in']['qty'][$i]=='0'){
				    $r = $db->DeleteWhere('new_trans_items',array($db->GetCond('no','=',$_POST['in']['id'][$i])));
				}else{
				    $r = $db->UpdateData('new_trans_items',['itm_qty'=>$_POST['in']['qty'][$i]],array($db->GetCond('no','=',$_POST['in']['id'][$i])));
				}
			}
			
			if($r){
			    echo "<script>alert('Terimakasih telah mengisi form penawaran online ini. Team marketing kami akan segera mengirimkan invoice ke email yang telah di daftarkan pada transaksi ini.')</script>";
			    echo "<script>location.replace('".$data->base_url."');</script>";
			    
			}
        }
        
        $idtrans = AES256($idtransaksi,'d');
        $datatrans = $quot->GetDataTransaksi($idtrans);
        $data->itemtrans = $quot->GetItemTransaksi($idtrans);
        $data->judul='Form Penawaran '.$datatrans['nama_usaha'];
        $data->subtitle = 'Item Transaksi '.$datatrans['nama_usaha'];
		$data->page = 'penawaran';
        $data->View('quotation/vformquotclient',$data);
    }
    
    
    // @url /administrasi/invoice/paymtd
    public static function NewPayMethod(){
    	$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		if (isset($_POST['in'])) {
			$r = $db->InsertData('desc_tbl',
				[
					'dt_relate_tbl' => 'new_invoice.inv_py_mtd',
					'dt_flag' => $_POST['in']['kode'],
					'dt_desc' => $_POST['in']['desc'],	
				]
			);

			if ($r) {
				echo "<script>alert('data berhasil disimpan!');</script>";

		    } else {
		    	echo "<script>alert('data gagal disimpan!');</script>";
		    }
		}


		$data->listdata = $db->FetchWhere(['dt_flag','dt_desc','ivisibility'],'desc_tbl',array($db->GetCond('dt_relate_tbl','=','new_invoice.inv_py_mtd')));

		$data->judul = 'Metode Pembayaran / Baru';
		$data->username = $_SESSION['admin']['nama'];
		$data->subtitle = 'Metode Pembayaran';
		$data->page = 'penawaran';

		$data->View('invoice/vnewpaymethod.inv',$data);
    }

    // @url /administrasi/invoice/?rmvpay={value}
    public static function RemoveInvPayment($idinv){
        $data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		
		$r = $db->UpdateData('new_inv_payment',['istatus'=>'0'],array($db->GetCond('lseqid','=',$idinv)));
		$idata=$db->FetchOneRow(['ssource'],'new_inv_payment',array($db->GetCond('lseqid','=',$idinv)));
		$r2 = $db->UpdateData('new_invoice',['inv_status'=>'2'],array($db->GetCond('inv_no_invoice','=',$idata['ssource'])));
		header('location: '.$data->base_url.'administrasi/invoice?paidmenutes='.$idata['ssource']);
    }    
    
	// @url /administrasi/data-transaksi/api?getphones={value}
	public static function GetPhonesTrans($idtrans){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		header('Content-Type: application/json');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");

		$idata = $db->FetchJoinWhereSingleRow(['telp_cust','telepon'],'new_transaksi',
			array(
				$db->getJoin('INNER','perusahaan','perusahaan.id','new_transaksi.trans_id_usaha'),
				$db->getJoin('INNER','new_customer','trans_cust_id','id_cust')
			),
			array(
				$db->GetCond('trans_id','=',$idtrans)
			)
		);

		$data->Func('StringFunc');

		$telpcust = BenerinTelepon($idata['telp_cust']);
		$telpusaha = BenerinTelepon($idata['telepon']);
		$arr = [
			'status' => ['code'=>'1','description'=>'data is available'],
			'totaldata' => count($idata),
			'data' => ['telpcust' => $telpcust,'telpusaha' => $telpusaha]
		];
		echo json_encode($arr);
	}

	// @url /administrasi/data-transaksi/api?getemails={value}
	public static function GetEmailsTrans($idtrans){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		header('Content-Type: application/json');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");

		$idata = $db->FetchJoinWhereSingleRow(['email','email_cust'],'new_transaksi',
			array(
				$db->getJoin('INNER','perusahaan','perusahaan.id','new_transaksi.trans_id_usaha'),
				$db->getJoin('INNER','new_customer','trans_cust_id','id_cust')
			),
			array(
				$db->GetCond('trans_id','=',$idtrans)
			)
		);

		$emailusaha = $idata['email'];
		$emailcust = $idata['email_cust'];

		$arr = [
			'status' => ['code'=>'1','description'=>'data is available'],
			'totaldata' => count($idata),
			'data' => ['emailusaha' => $emailusaha,'emailcust' => $emailcust]
		];
		echo json_encode($arr);

	}

	// @url /administrasi/data-transaksi/feedback/{value}
	public static function DaftarTrainerUtama($idtrans){
		$data = new Data();

		$data->Model('Googlecalendarmodel');
		$gcm = new Googlecalendarmodel();
		$data->listdata = $gcm->GetStaffUtama($idtrans);
		$idata = $gcm->DaftarFeedback($idtrans);

		$data->Func('StringFunc');
		$data->Func('Fsecurity');

		$data->nama_usaha = $idata['nama_usaha'];
		$data->namacust = $idata['namacust'];
		$data->idtrans = $idtrans;

		$data->judul = 'Feedback Trainer';
		$data->username = $_SESSION['admin']['nama'];
		$data->subtitle = 'Feedback Trainer';
		$data->page = 'penawaran';

		$data->View('transaksi/vtblfeedbacktrainer.trans',$data);
	}

	public static function MarkAsRead($id){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		if ($id!='') {
			$r = $db->UpdateData('pendaftar',['status_transaksi'=>'4'],array($db->GetCond('id','=',$id)));

			header('location:'.$data->base_url.'administrasi/marketing/data-pendaftar');
		}
	}

	// @url /administrasi/marketing/data-pendaftar
	public static function DataPendaftar(){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$db->setOrder('tanggal_daftar','DESC');
		$data->datadaftar = $db->FetchWhere(['*'],'pendaftar',array($db->GetCond('status_transaksi','=','1')));
		// print_r($data->datadaftar);
		$data->judul = 'DATA ITEMS TRANSAKSI';
		$data->username = $_SESSION['admin']['nama'];
		$data->subtitle = 'Data Items Transaksi';
		$data->page = 'penawaran';
		$data->View('transaksi/vdatadaftar.trans',$data);
		
	}

	//@url /administrasi/data-transaksi/items
	public static function TabelItems($search=''){
		$data = new Data();
		// authentication
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$sesi = new Sessionz();
		$sesi->AdminMarketing();
		
		$data->Model('Marketing');
		$db = new Marketing();

		$data->listdata = $db->FetchAllTransItem();
		$data->judul = 'DATA ITEMS TRANSAKSI';
		$data->username = $_SESSION['admin']['nama'];
		$data->subtitle = 'Data Items Transaksi';
		$data->page = 'penawaran';
		$data->View('transaksi/vtblitems.trans',$data);
	}

	//@url /administrasi/data-transaksi/outstanding
	public static function TabelOutstanding(){
		$data = new Data();
		// authentication
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$sesi = new Sessionz();
		$sesi->AdminMarketing();
		
		$data->Model('Querybuilder');
		$data->Model('Calendar');
		$db = new QueryBuilder();
		$cal = new Calender();
		//print_r($cal->GetOutstandingDayFromTransaction());
		if(isset($_GET['src']['m'],$_GET['src']['y'])){
		    $data->listdata = $cal->GetOutstandingDayFromTransaction($_GET['src']);
		}else{
		    $data->listdata = $cal->GetOutstandingDayFromTransaction();
		}
		
		
		$data->judul = 'FAC || DATA OUTSTANDING TRANSAKSI';
		$data->username = $_SESSION['admin']['nama'];
		$data->subtitle = 'Data Outstanding Transaksi';
		$data->page = 'outstanding table';
		$data->View('transaksi/voutstanding.trans',$data);
	}

	private static function EditItemTransPost($masukdata){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		$r = $db->UpdateData('paket_training',['paket_hari' => $masukdata['hari'],'kategori' => $masukdata['kategori']],array($db->GetCond('id','=',$masukdata['id'])));
		if ($r) {
			echo "<script>alert('Data item berhasil diubah');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi/data-transaksi/items');</script>";
		} else {
			echo "<script>alert('Data item gagal diubah');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi/data-transaksi/items');</script>";
		}
	}

	// @url /administrasi/new-transaksiz?uitm={value}
	public static function EditItemTrans($iditem){
		$data = new Data();
		// authentication
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$sesi = new Sessionz();
		$sesi->AdminMarketing();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		if (isset($_POST['in'])) {
			self::EditItemTransPost($_POST['in']);
		}

		$dataitem = $db->FetchOneRow(['*'],'paket_training',array($db->GetCond('id','=',$iditem)));
		$data->listkategori = $db->FetchWhere(['*'],'desc_tbl',array($db->GetCond('dt_relate_tbl','=','pktitemkat')));
		// print_r($dataitem);
		$data->iditem = $iditem;
		$data->namaitem = $dataitem['nama_item'];
		$data->harga = $dataitem['harga'];
		$data->iditem = $iditem;
		$data->katitem = $dataitem['kategori'];
		$data->totalhari = $dataitem['paket_hari'];
		$data->judul = 'FAC || Update Item';
		$data->username = $_SESSION['admin']['nama'];
		$data->subtitle = 'Update Item Transaksi';
		$data->page = 'detailitem';
		$data->View('transaksi/vupdateitem.trans',$data);
	}

	public static function DetailItem($iditem){
		$data = new Data();
		// authentication
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$sesi = new Sessionz();
		$sesi->AdminMarketing();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$dataitem = $db->FetchOneRow(['*'],'paket_training',array($db->GetCond('id','=',$iditem)));
		// print_r($dataitem);
		$data->namaitem = $dataitem['nama_item'];
		$data->harga = $dataitem['harga'];
		$data->iditem = $iditem;
		$data->totalhari = $dataitem['paket_hari'];
		$data->judul = 'FAC || Detail Item';
		$data->username = $_SESSION['admin']['nama'];
		$data->subtitle = 'Detail Item Transaksi';
		$data->page = 'detailitem';
		$data->View('transaksi/vdetailitem.trans',$data);
	}

	private static function TambahTanggalPost($masukdata){
		$data = new Data();
		$data->Model('Querybuilder');
		$data->Model('Calendar');
		$db = new QueryBuilder();
		$cal = new Calender();

		$jmlpakethari = $cal->GetAvailableDateForTrans($masukdata['idTransaksi']);
		$data->tanggal = $db->FetchWhere(['*'],'fac_calendar',array($db->GetCond('acara','=',$masukdata['idTransaksi'])));
		if (count($data->tanggal)==$jmlpakethari) {
			echo "<script>alert('Transaksi ini hanya dapat menerima ".$jmlpakethari." hari (berdasarkan jumlah hari dari item yang terdaftar untuk transaksi ini), hapus atau tambah item untuk menambah tanggal transaksi ini.');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi/new-transaksiz?newdate=".
					$masukdata['idTransaksi'].
					"');</script>";
		}elseif ($jmlpakethari<count($data->tanggal)) {
			echo "<script>alert('Data tanggal melebihi batas maksimal jumlah hari dari item yang terdaftar untuk transaksi ini.');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi/new-transaksiz?newdate=".
					$masukdata['idTransaksi'].
					"');</script>";
		}elseif ($jmlpakethari>count($data->tanggal)) {
			$tanggaldimaksud = $db->FetchWhere(
				['tanggal'],
				'fac_calendar',
				array(
					$db->GetCond('tanggal','=',$masukdata['tanggalz']),
					$db->GetCond('acara','=',$masukdata['idTransaksi'])
				)
			);
			if (count($tanggaldimaksud)=='0') {
				$r = $db->InsertData('fac_calendar',['tanggal' => $masukdata['tanggalz'],'acara'=>$masukdata['idTransaksi']]);
				if ($r) {
					echo "<script>alert('Data tanggal berhasil disimpan');</script>";
					echo "<script>location.replace('".$data->base_url."administrasi/new-transaksiz?newdate=".
					$masukdata['idTransaksi'].
					"');</script>";
				}
			}else{
				echo "<script>alert('Transaksi sudah memiliki agenda pada tanggal tersebut');</script>";
				echo "<script>location.replace('".$data->base_url."administrasi/new-transaksiz?newdate=".
					$masukdata['idTransaksi'].
					"');</script>";
			}
		}
			

	}

	// @url /administrasi/new-transaksiz?newdate={idtrans}
	public static function TambahTanggal($idtrans){
		$data = new Data();
		// authentication
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$sesi = new Sessionz();
		$sesi->AdminMarketing();
		$data->Model('Querybuilder');
		$data->Model('Calendar');
		$db = new QueryBuilder();
		$cal = new Calender();

		$datatrans = $db->FetchJoinWhere(['*'],
			'new_transaksi',
			array($db->getJoin('LEFT','perusahaan','perusahaan.id','new_transaksi.trans_id_usaha')),
			array($db->GetCond('new_transaksi.trans_id','=',$idtrans))
		);
		
		if (count($datatrans)!='1') {
			echo "<script>alert('Transaksi tidak terdaftar di sistem, silahkan untuk mengecek transaksi yang di maksud.');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi/new-perusahaan-info?tr=".$idtrans."');</script>";
		}else{
			$jmlpakethari = $cal->GetAvailableDateForTrans($idtrans);
			$db->setOrder('fac_calendar.tanggal','ASC');
			$data->tanggal = $db->FetchWhere(['*'],'fac_calendar',array($db->GetCond('acara','=',$idtrans)));
			$db->setOrder('','');
			if ($jmlpakethari<count($data->tanggal)) {
				echo "<script>alert('Tanggal Transaksi tidak boleh lebih besar dari total jumlah hari item yang telah di tentukan, harap mengecek jumlah hari yang tersedia untuk setiap item dari transaksi ini.');</script>";
				echo "<script>location.replace('".$data->base_url."administrasi/new-perusahaan-info?tr=".
				$idtrans.
				"');</script>";
			}

			if (isset($_POST['input'])) {
				self::TambahTanggalPost($_POST['input']);
			}


			// print_r($datatrans);
			$data->no_transaksi = $datatrans[0]['trans_id'];
			$data->perusahaan = $datatrans[0]['nama'];
			$data->agenda = $db->FetchJoinWhere(['nama_agenda'],
				'new_trans_agendas',
				array($db->getJoin('INNER','daftar_agenda','daftar_agenda.id','new_trans_agendas.agd_training')),
				array($db->GetCond('new_trans_agendas.agd_id_trans','=',$idtrans))
			);
			$data->totalhari = $jmlpakethari;
			// print_r($data->agenda);
			$data->judul = 'FAC || Tambah Tanggal Transaksi';
			$data->username = $_SESSION['admin']['nama'];
			$data->page = 'new-transaksi-tanggal';
			$data->View('transaksi/vaddnewdate.trans',$data);
		}
	}

	public static function TotalLaporanAgendaTransBulanan(){
	    $data = new Data();
	    // authentication
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$sesi = new Sessionz();
		$sesi->OnlyAdmin();
		$data->Model('Laporan');
		$data->Model('Querybuilder');
		$lap = new ControlLaporan();
		$db = new QueryBuilder();

		include_once $data->homedir.'administrasi/session/session-class.php';
		$isessions= new Sessionz();
		$isessions->IsSuperUser();
		
		$data->listdata = $lap->LapBulananByAgendaTrans('curmonth');
		$data->listdatalastmonth = $lap->LapBulananByAgendaTrans('lastmonth');
		$data->headertext = 'Laporan Berdasarkan Agenda Transaksi';
        $data->judul = 'FAC || Laporan Transaksi ~ Agenda';
		$data->username = $_SESSION['admin']['nama'];

		// print_r($data->listdatalastmonth);
		// foreach ($data->listdatalastmonth as $value => $key) {
		// 	echo $key['nama'].' '.$key['jumlah'].'<br>';
		// }
		$data->View('laporan/transaksi/vagendabulanan.laporan',$data);
	}    

	public static function TotalLaporanMarketingBulanan(){
		$data = new Data();

		include_once $data->homedir.'administrasi/session/session-class.php';
		$isessions= new Sessionz();
		$isessions->IsSuperUser();

		$data->Model('Marketing');
		$marketing = new Marketing();
		$data->datamarketing = $marketing->CountInvoiceBulanan();
		$data->datamarketingblnlalu = $marketing->CountInvoiceBulanLalu();
		$data->cabangblnini = $marketing->CountInvoiceBulananByCabang();
		$data->cabangblnlalu = $marketing->CountInvoiceBulanLaluByCabang();
		$data->title = "Sales Report";
		$data->subtitle="Laporan Sales";
		@$data->username=$_SESSION['admin']['nama'];
		$data->totalrow = count($data->datamarketing);
		$data->JustInclude('administrasi/view/laporan/functions');
		$data->View('laporan/marketing/vrsalesmontly',$data);
	}

	public static function SendingInvoice($idinv){
		
	}

	public static function ValidatingInvoice($idinv){
		$data = new Data();
		$data->Model('Invoice');
		$invoice = new Invoice();

		if (isset($_POST['delinv'])) {
        if(!file_exists($invoice->getFolder().$_POST['delinv'])){
            echo "<script>alert('File PDF tidak ada, anda dapat membuatnya kembali dengan klik edit data surat.');</script>";
        }else {
            // 
            if (unlink($invoice->getFolder().$_POST['delinv'])) {
                echo "<script>alert('File telah di hapus');</script>"; 
            }else {
                echo "<script>alert('File gagal di hapus, harap hubungi admin sistem kami!');</script>";
            }
        }
    }


    if ($_SESSION['admin']['tipe']=='admin') {
        $invoice->setNo($idinv);
        $datasurat = $invoice->FetchDataSurat();

        if (!isset($datasurat->emd_to)||$datasurat->emd_to==''||empty($datasurat->emd_to)) {
            echo "<script>alert('Data email belum siap, silahkan lengkapi terlebih dahulu.');</script>";
            // echo "<script>location.replace('https://fac-institute.com/administrasi/new-attachments?inv=".$_GET['valid']."');</script>";
        }
        $data->url = $data->base_url.'administrasi/invoice/invoice/validating';
        $data->judul = 'Validation Invoice Report/FAC';
        $data->username = $_SESSION['admin']['nama'];
        $data->page = 'tambah_customer';
        $data->email = $datasurat->emd_to;
        $data->no_invoice = $idinv;
        $data->idtransaksi = $datasurat->inv_id_trans;
        $data->carboncopy = $datasurat->emd_cc;
        $data->attachments = $datasurat->emd_attachments;

        $data->View('invoice/mail-validation',$data);
        // include_once 'view/invoice/mail-validation.php';
    }
	}

	// function for validating transaction (jenis accurate must be set b4 creating invoice)
	public static function CheckingPortal($idtransaksi){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$row = $db->FetchOneRow(['*'],'new_transaksi',array($db->GetCond('trans_id','=',$idtransaksi)));
		// after checking set a session to tell the system that the transaction is ready for invoice
		// when successfully insert new invoice, unset the session
		print_r($row);

	}

	public static function UpdateJenisAccurate($idtransaksi){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();


	}

	public static function TerbayarAPI(){
		// header type JSON
		header('Content-Type: application/json');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		
		$data = new Data();
		$data->Model('Invoice');
		$inv = new Invoice();

		$r = $inv->GetJumlahDanTerbayarByTglBayar('currentmonth');
		$result = $inv->GetJumlahDanTerbayarByTglBayar('lastmonth');
		// echo date('m', strtotime('+1 months'));//date('Y', strtotime('+1 years'));
		
		$totalterbayarblnini = 0;
		$totalterbayarblnlalu = 0;

		$terbayartrainingblnini = 0;
		$terbayaraccsvcblnini = 0;
		$terbayarkursusblnini = 0;
		$terbayarlainnya = 0;

		$terbayartrainingblnlalu = 0;
		$terbayaraccsvcblnlalu = 0;
		$terbayarkursusblnlalu = 0;
		$terbayarlainnyablnlalu = 0;
		
		foreach ($r as $key) {
			if (intval($key['inv_status'])>1) {
				if ($key['trans_jenis']=='1') {
					if (intval($key['terbayar'])==0) {
						$terbayartrainingblnini += intval($key['terbayar']);	
					} else {
						$terbayartrainingblnini += intval($key['terbayar']);	
					}
				}elseif ($key['trans_jenis']=='2') {
					if (intval($key['terbayar'])==0) {
						$terbayaraccsvcblnini += intval($key['terbayar']);
					} else {
						$terbayaraccsvcblnini += intval($key['terbayar']);
					}
				}elseif ($key['trans_jenis']=='3') {
					if (intval($key['terbayar'])==0) {
						$terbayarkursusblnini += intval($key['terbayar']);
					} else {
						$terbayarkursusblnini += intval($key['terbayar']);
					}
				}else{
					if (intval($key['terbayar'])==0) {
						$terbayarlainnya += intval($key['terbayar']);
					} else {
						$terbayarlainnya += intval($key['terbayar']);
					}
				}
				if (intval($key['terbayar'])==0) {
					$totalterbayarblnini += intval($key['terbayar']);
				} else {
					$totalterbayarblnini += intval($key['terbayar']);	
				}
				
			}
		}
		
		foreach ($result as $key) {
			if (intval($key['inv_status'])>1) {
				if ($key['trans_jenis']=='1') {
					if (intval($key['terbayar'])==0) {
						$terbayartrainingblnlalu += intval($key['terbayar']);	
					} else {
						$terbayartrainingblnlalu += intval($key['terbayar']);	
					}
				}elseif ($key['trans_jenis']=='2') {
					if (intval($key['terbayar'])==0) {
						$terbayaraccsvcblnlalu += intval($key['terbayar']);
					} else {
						$terbayaraccsvcblnlalu += intval($key['terbayar']);
					}
				}elseif ($key['trans_jenis']=='3') {
					if (intval($key['terbayar'])==0) {
						$terbayarkursusblnlalu += intval($key['terbayar']);
					} else {
						$terbayarkursusblnlalu += intval($key['terbayar']);
					}
				}else{
					if (intval($key['terbayar'])==0) {
						$terbayarlainnyablnlalu += intval($key['terbayar']);
					} else {
						$terbayarlainnyablnlalu += intval($key['terbayar']);
					}
				}
				if (intval($key['terbayar'])==0) {
					$totalterbayarblnlalu += intval($key['terbayar']);
				} else {
					$totalterbayarblnlalu += intval($key['terbayar']);	
				}
			}
		}
		
		if (count($r)=='0'&&count($result)=='0') {
			$arr = [
					'status' => ['code'=>'0','description'=>'empty result, there must be an error on query or database connection'],
					'terbayarbulanini' => [
						'total' => $totalterbayarblnini,
						'training' => $terbayartrainingblnini,
						'accsvc' => $terbayaraccsvcblnini,
						'kursus' => $terbayarkursusblnini,
						'lainnya' => $terbayarlainnya
					],
					'terbayarbulanlalu' => [
						'total' => $totalterbayarblnlalu,
						'training' => $terbayartrainingblnlalu,
						'accsvc' => $terbayaraccsvcblnlalu,
						'kursus' => $terbayarkursusblnlalu,
						'lainnya' => $terbayarlainnyablnlalu
					]
					
					// ,'data' => $r
			];
		}else{
			$arr = [
					'status' => ['code'=>'1','description'=>'ok'],
					'terbayarbulanini' => [
						'total' => $totalterbayarblnini,
						'training' => $terbayartrainingblnini,
						'accsvc' => $terbayaraccsvcblnini,
						'kursus' => $terbayarkursusblnini,
						'lainnya' => $terbayarlainnya
					],
					'terbayarbulanlalu' => [
						'total' => $totalterbayarblnlalu,
						'training' => $terbayartrainingblnlalu,
						'accsvc' => $terbayaraccsvcblnlalu,
						'kursus' => $terbayarkursusblnlalu,
						'lainnya' => $terbayarlainnyablnlalu
					]
					
					// ,'data' => $r
			];
		}
		echo json_encode($arr);
	}

	public static function omsetAPI(){
		// header type JSON
		header('Content-Type: application/json');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");

		$data = new Data();
		$data->Model('Invoice');
		$inv = new Invoice();

		$r = $inv->GetJumlahDanTerbayarByInvDate('currentmonth');
		$result = $inv->GetJumlahDanTerbayarByInvDate('lastmonth');
		// echo date('m', strtotime('+1 months'));//date('Y', strtotime('+1 years'));
		//omset
		//bulan ini
		$totalomsetbulanini = 0;
		$omsettrainingblnini = 0;
		$omsetaccountingserviceblnini = 0;
		$omsetkursusbulanini = 0;
		$omsetlainnyablnini = 0;
		//bulan lalu
		$totalomsetbulanlalu = 0;
		$omsettrainingblnlalu = 0;
		$omsetaccountingserviceblnlalu = 0;
		$omsetkursusbulanlalu = 0;
		$omsetlainnyablnlalu = 0;

		foreach ($r as $key) {
			if (intval($key['inv_status'])>=0) {
				if ($key['trans_jenis']=='1') {
					$omsettrainingblnini += intval($key['jumlah']);
				}elseif ($key['trans_jenis']=='2') {
					$omsetaccountingserviceblnini += intval($key['jumlah']);
				}elseif ($key['trans_jenis']=='3') {
					$omsetkursusbulanini += intval($key['jumlah']);
				}else{
					$omsetlainnyablnini += intval($key['jumlah']);
				}
				$totalomsetbulanini += intval($key['jumlah']);
			}
		}
		foreach ($result as $key) {
			if (intval($key['inv_status'])>=0) {
				if ($key['trans_jenis']=='1') {
					$omsettrainingblnlalu += intval($key['jumlah']);
				}elseif ($key['trans_jenis']=='2') {
					$omsetaccountingserviceblnlalu += intval($key['jumlah']);
				}elseif ($key['trans_jenis']=='3') {
					$omsetkursusbulanlalu += intval($key['jumlah']);
				}else{
					$omsetlainnyablnlalu += intval($key['jumlah']);
				}
				$totalomsetbulanlalu += intval($key['jumlah']);	
			}
		}


		if (count($r)=='0'&&count($result)=='0') {
			$arr = [
					'status' => ['code'=>'0','description'=>'Empty Result, there must be an error on query or database connection'],
					'omsetbulanini' => [
						'total' => $totalomsetbulanini,
						'training' => $omsettrainingblnini,
						'accsvc' => $omsetaccountingserviceblnini,
						'kursus' => $omsetkursusbulanini,
						'lainnya' => $omsetlainnyablnini
					],
					'omsetbulanlalu' => [
						'total' => $totalomsetbulanlalu,
						'training' => $omsettrainingblnlalu,
						'accsvc' => $omsetaccountingserviceblnlalu,
						'kursus' => $omsetkursusbulanlalu,
						'lainnya' => $omsetlainnyablnlalu
					]
					// 'data' => $result
			];
		}else{
			$arr = [
					'status' => ['code'=>'1','description'=>'ok'],
					'omsetbulanini' => [
						'total' => $totalomsetbulanini,
						'training' => $omsettrainingblnini,
						'accsvc' => $omsetaccountingserviceblnini,
						'kursus' => $omsetkursusbulanini,
						'lainnya' => $omsetlainnyablnini
					],
					'omsetbulanlalu' => [
						'total' => $totalomsetbulanlalu,
						'training' => $omsettrainingblnlalu,
						'accsvc' => $omsetaccountingserviceblnlalu,
						'kursus' => $omsetkursusbulanlalu,
						'lainnya' => $omsetlainnyablnlalu
					]
					// 'data' => $result
			];
		}
		echo json_encode($arr);
	}

	public static function similarDataByToken($token){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		// header type JSON
		header('Content-Type: application/json');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");

		$r = $db->FetchJoinWhere(['*'],'temp_cust',
			array($db->getJoin('INNER','temp_usaha','temp_usaha.tu_token','temp_cust.reg_token')),
			array($db->GetCond('temp_cust.reg_token','=',$token)));

		if (count($r)=='0') {
			$arr = [
				'status' => ['code'=>'0','description'=>'Ooops, something wrong with database or query!']
			];
		}else{
			$arr = [
					'status' => ['code'=>'1','description'=>'ok'],
					'data' => $r
			];
		}
		echo json_encode($arr);
	}

	public static function DataListCustAdmin(){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		// header type JSON
		header('Content-Type: application/json');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");

		$r = $db->FetchJoin(['*'],'temp_cust',array($db->getJoin('LEFT','temp_usaha','temp_usaha.tu_id','temp_cust.id_usaha')));
		if (count($r)=='0') {
			$arr = [
				'status' => ['code'=>'0','description'=>'Ooops, something wrong with database or query!']
			];
		}else{
			$arr = [
					'status' => ['code'=>'1','description'=>'ok'],
					'data' => $r
			];
		}
		echo json_encode($arr);
	}


	public static function OnlineRegTrans($datainput){
		// return "not ready yet";
		$data = new Data();
		$data->Model('Querybuilder');
		$data->Model('Mail');
		$imail = new FACMail();
		$db = new QueryBuilder();
		$data->JustInclude('view/email/vmaster.mail');
		$data->JustInclude('view/email/notifdaftar');




		// header type JSON
		header('Content-Type: application/json');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");

		// $str = '{ "idusaha":"1348udsfkseda01231", "idcust":"sldfj0453234dgs2sdf2", "agenda":"pokoknya training ajalah", "paket":"15 - OL1 - 1.200.000" }';
		$hasil = json_decode($datainput);
		$rawID='OLTR-'.date('dmY');
        $rawID2 = substr($hasil->idcust, 0, 4);
        $id = $rawID.$rawID2;

        //declaration for email to admin
		$data->subject = 'Notifikasi Registrasi Online '.$id;
		$data->to = 'training@fac-institute.com';
		$data->cc = ['fajarfifa@gmail.com','rifzky.mail@gmail.com'];
		$content = HeadMail();
		$content .= EmailContent();
		$content .= Footer();
		$data->content = $content;

        if (is_null($hasil)) {
			$arr = [
				'status' => ['code'=>'500','description'=>'Ooops, something wrong with your JSON!']
			];
			echo json_encode($arr);
			return;
		}elseif (!isset($_SESSION['fac_reg_token'])||empty($_SESSION['fac_reg_token'])) {
			$arr = [
				'status' => ['code'=>'505','description'=>'Service not available. make sure you accessed this service from a proper page!']
			];
			echo json_encode($arr);
			return;
		}elseif (isset($_SESSION['fac_reg_token'])&&!empty($_SESSION['fac_reg_token'])){
			$insd = [
				'trans_id' => $id,
				'trans_id_usaha' => $hasil->idusaha,
			    'trans_cust_id' => $hasil->idcust,
			    'trans_jenis' => '1',
			    'trans_ket_lain' => $hasil->agenda . ' # '.$hasil->paket,
			    'trans_waktu' => '09:00 - 16:00',
			    'trans_petugas' => 'charlez'
			];
			$qr = $db->InsertData('new_transaksi',$insd);
			if ($qr) {
				$imail->sendMailWithCC($data);

				$arr = [
					'status' => ['code'=>'1','description'=>'data inserted!']
				];
			}else{
				$arr = [
					'status' => ['code'=>'0','description'=>'cannot insert your data :(']
				];
			}
				
			echo json_encode($arr);
			return;
		}


	}

	public static function DataCabang(){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$data->JustInclude('administrasi/functions/StringFunc');

		// header type JSON
		header('Content-Type: application/json');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		$r = $db->FetchJoin(
			['id_cabang','nama'],
			'cabang',
			array(
				$db->getJoin('INNER','perusahaan','perusahaan.id','cabang.id_perusahaan')
			));
		if (empty($r)) {
			$arr = [
				'status' => ['code'=>'0','description'=>'Ooops, something wrong with database or query!']
			];
		}else{
			$arr = [
				'status' => ['code'=>'0','description'=>'Ooops, something wrong with database or query!'],
				'data' => $r
			];
		}
		echo json_encode($r);
	}

	public static function hasilinputregtrans(){
		// datatype JSON
		header('Content-Type: application/json');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$r = $db->FetchWhere(['trans_id','trans_jenis','trans_id_usaha','trans_cust_id'],'new_transaksi',array($db->GetCond('trans_id','LIKE','%OLTR%')));
		if (count($r)=='0') {
			$arr = [
					'status' => ['code'=>'','description'=>'data is not available']
			];
		} else {
			$arr = [
					'status' => ['code'=>'1','description'=>'ok'],
					'data' => $r
			];
		}
			
		echo json_encode($arr);
	}

	public static function paketTrainingAPI(){
		// header type JSON
		header('Content-Type: application/json');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");


		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$r = $db->FetchWhere(['id','nama_item','harga','paket_hari'],'paket_training',array($db->GetCond('status_paket','=','3')));
		$arr = [
				'status' => ['code'=>'1','description'=>'ok'],
				'data' => $r
		];
		echo json_encode($arr);
	}

	public static function hasilinputregcust(){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$r = $db->Fetch('temp_cust');
		$arr = [
				'status' => ['code'=>'1','description'=>'ok'],
				'data' => $r
		];
		echo json_encode($arr);
	}

	public static function hasilinputregusaha(){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$r = $db->Fetch('temp_usaha');
		// datatype JSON
		header('Content-Type: application/json');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");

		$arr = [
				'status' => ['code'=>'1','description'=>'ok'],
				'data' => $r
		];
		echo json_encode($arr);
	}

	// @url (value=/daftar-online#step2)
	public static function OnlineRegUsaha($datainput){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$data->JustInclude('administrasi/functions/StringFunc');
		// create ID (primary key)

		// delete data if exists
		// $db->DeleteWhere($tabel,$cond);

		// header type JSON
		header('Content-Type: application/json');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");

		// start processing input

		$str = '{ "namausaha":"PT OKE BISA", "email":"okebisa@usaha.com","idcust":"32asfjsnf32spsdpoftxcvm", "telepon":"081279222250", "alamat":"Jl Jatiwaringin raya no 8 Pangkalan Jati Jakarta Barat","kota":"1","kecamatan":"12","provinsi":"55","jenisusaha":"1#4","ketjenisusaha":"saya dagang somay","versiaccurate":"1#2#5","map":"-6.2087634,106.84559899999999" }';
		$hasil = json_decode($datainput);

		if (is_null($hasil)) {
			$arr = [
				'status' => ['code'=>'500','description'=>'Ooops, something wrong with your JSON!']
			];
			echo json_encode($arr);
			return;
		}elseif (!isset($_SESSION['fac_reg_token'])||empty($_SESSION['fac_reg_token'])) {
			$arr = [
				'status' => ['code'=>'505','description'=>'Service not available. make sure you accessed this service from a proper page!']
			];
			echo json_encode($arr);
			return;
		}elseif (isset($_SESSION['fac_reg_token'])&&!empty($_SESSION['fac_reg_token'])){
			$id = md5($hasil->namausaha.'-'.$hasil->alamat);
			$insd = [
				'tu_id' => $id,
				'tu_token' => @$_SESSION['fac_reg_token'],
			    'tu_nama' => $hasil->namausaha,
			    'tu_email' => $hasil->email,
			    'tu_telepon' => $hasil->telepon,
			    'tu_alamat' => GetVal(@$hasil->alamat),
			    'tu_kecamatan' => $hasil->kecamatan,
			    'tu_kota' => $hasil->kota,
			    'tu_provinsi' => $hasil->provinsi,
			    'tu_ju' => $hasil->jenisusaha,
			    'tu_kju' => $hasil->ketjenisusaha,
			    'tu_va' => $hasil->versiaccurate,
			    'tu_map' => $hasil->map 
			];
			$qr = $db->InsertData('temp_usaha',$insd);
			if ($qr) {
				$arr = [
					'status' => ['code'=>'1','description'=>'data inserted!'],
					'idusaha' => $id,
				];
				// update customer perusahaan_id
				$db->UpdateData('temp_cust',['id_usaha' => $id],array($db->GetCond('reg_token','=',$hasil->idcust)));
			}else{
				$arr = [
					'status' => ['code'=>'0','description'=>'cannot insert your data :(']
				];
			}
				
			echo json_encode($arr);
			return;
		}
	}

	// @url (value=/daftar-online#step1)
	public static function OnlinaRegCust($datainput){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$data->JustInclude('administrasi/functions/StringFunc');
		$str = '{ "nama":"tesnama", "email":"email@email", "telepon":"081279222250","jabatan":"owner","cabang":"0","marketing":"rifzky_alam","jenispengguna":"1" }';
		$hasil = json_decode($datainput);
		// create ID (primary key)

		// delete data if exists
		

		// header type JSON
		header('Content-Type: application/json');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");

		// start input here

		
		

		if (is_null($hasil)) {
			$arr = [
				'status' => ['code'=>'702','description'=>'Ooops, something wrong with your JSON!']
			];
			echo json_encode($arr);
			return;
		}elseif (!isset($_SESSION['fac_reg_token'])||empty($_SESSION['fac_reg_token'])) {
			$arr = [
				'status' => ['code'=>'505','description'=>'Service not available. make sure you accessed this service from a proper page!']
			];
			echo json_encode($arr);
			return;
		}elseif (isset($_SESSION['fac_reg_token'])&&!empty($_SESSION['fac_reg_token'])){
			$insd = [
				'reg_token' => @$_SESSION['fac_reg_token'],
				'tc_id' => md5($hasil->email),
			    'tc_nama' => $hasil->nama,
			    'tc_email' => $hasil->email,
			    'tc_telepon' => $hasil->telepon,
			    'tc_jabatan' => GetVal(@$hasil->jabatan),
			    'tc_marketing' => $hasil->cabang .'#'.$hasil->marketing,
			    'tc_jp' => $hasil->jenispengguna 
			];
			$qr = $db->InsertData('temp_cust',$insd);
			if ($qr) {
				$arr = [
					'status' => ['code'=>'1','description'=>'data inserted!'],
					'data' => ['id_cust' => $insd['tc_id']]
				];
			}else{
				$arr = [
					'status' => ['code'=>'0','description'=>'cannot insert your data :(']
				];
			}
				
			echo json_encode($arr);
			return;
		}

	}


	// @url (value=/administrasi/exceldownload/laporaninvoice)
	public static function DownloadInvoice($src){
		$data = new Data();
		$data->JustInclude('administrasi/functions/DateFunc');
		$data->JustInclude('administrasi/functions/StringFunc');
		$data->Model('Invoice');
		$invoice = new Invoice();

		$filename = "Laporan_Invoice.xls";
		header("Content-Disposition: attachment; filename=\"$filename\"");
  		header("Content-Type: application/vnd.ms-excel");
  		// Original PHP code by Chirp Internet: www.chirp.com.au
  		// Please acknowledge use of this code by including this header.
  		$myRow = array();

  		$search = array(
			'nama_usaha' => $src['np'],
			'nama_cust' =>$src['nk'],
			'tanggal_awal' =>$src['fd'],
			'tanggal_akhir' =>$src['ld'],
			'tipe_transaksi'=>$src['tipe'],
			'jumlah' => $src['total'],
			'status' => $src['st'],
			'sales' => $src['sls']
		);
        $tglbayar = '0';
		if (isset($_GET['src']['tglbayar'])&&$_GET['src']['tglbayar']=='y') {
			$search['tglbyr'] = 'y';
			$tglbayar = '1';
		}else{
			$search['tglbyr'] = 'n';
		}
		//setting invoice variables
		$invoice->setData((object) $search);
		
		if($tglbayar=='1'){
		    $datahasilsearch = $invoice->SearchInvoiceTerbayarByDateTerbayar();
		}else{
		    $datahasilsearch = $invoice->SearchInvoice();
		}    
		

		$no = 1;
		foreach ($datahasilsearch as $key) {
			if ($key->inv_status=='3') {
				$sisa = 0;
			}else{
				$sisa = $key->terbayar;
			}
			
			if($tglbayar=='1'){
			    $tanggalbayar = $key->tanggal_bayar_bener;
			    
			}else{
			    $tanggalbayar = $key->tgl_bayar;
			}
			
			if($key->inv_status=='-1'){
			    $status='invalid';
			}else{
			    $status='valid';
			}

			$row = [
  			    'NO' => $no,
  			    'TANGGAL KIRIM' => GetTanggal($key->inv_date),
  			    'NO INVOICE' => $key->inv_no_invoice,
  			    'PERUSAHAAN' => $key->nama_usaha,
  			    'CUSTOMER' => $key->nama_cust,
  			    'JUMLAH' => $key->jumlah,
  			    'TERBAYAR' => intval($key->terbayar),
  			    'SISA' => intval($key->jumlah)-intval($key->terbayar),
  			    'TANGGAL BAYAR' => $tanggalbayar,
  			    'TIPE TRANSAKSI' => $key->jt_ket,
  			    'MARKETING' => $key->nama_petugas,
  			    'MARKETING SOFTWARE' => $key->cabang,
  			    'MARKETING SOFTWARE STAFF' => $key->nama_marketing,
  			    'STATUS INVOICE' => $status
  			];
  			$no++;
  			array_push($myRow, $row);	
		}

  		$flag = false;
		foreach($myRow as $row) {
			if(!$flag) {
		      // display field/column names as first row
		      echo implode("\t", array_keys($row)) . "\n";
		      $flag = true;
		    }
		    array_walk($row,'cleanData');
		    echo implode("\t", array_values($row)) . "\n";
		}

		exit;

	}

	// @url(value=/administrasi/invoice/tabel-marketing)
	public static function TabelMarketing(){
		$data = new Data();
		$data->JustInclude('administrasi/functions/DateFunc');
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$data->Model('Querybuilder');
		$data->Model('Invoice');
		$sesi = new Sessionz();
		$db = new QueryBuilder();
		$inv = new Invoice();
		$sesi->AdminMarketing();

		$data->page = 'data-invoice';

		$data->datamarketing = $db->FetchWhere(['username','nama'],'users',array($db->GetCond('team','=','3'),$db->GetCond('status','=','aktif')));
		$data->list_jenis_transaksi = $db->FetchColumns(['jt_id','jt_ket'],'new_jenis_transaksi');

		$limit = 30;
		if (isset($_GET['page'])&&!empty($_GET['page'])) {
			$offset = ($_GET['page']-1)*$limit;
			$data->pagenum = $_GET['page'];
			$data->tabeldata = $db->FetchColumns(['inv_no_invoice','nama_cust','inv_id_trans','nama_usaha','inv_deskripsi','sipm_desc','inv_status','si_desc','inv_date','tgl_bayar','items','jumlah','nama_petugas','terbayar'],'view_data_invoice',$offset,$limit);
		}elseif (isset($_GET['src'])&&!empty($_GET['src'])) {
			$search = array(
				'nama_usaha' => $_GET['src']['np'],
				'nama_cust' =>$_GET['src']['nk'],
				'tanggal_awal' =>$_GET['src']['fd'],
				'tanggal_akhir' =>$_GET['src']['ld'],
				'tipe_transaksi'=>$_GET['src']['tipe'],
				'jumlah' => $_GET['src']['total'],
				'status' => $_GET['src']['st'],
				'sales' => $_GET['src']['sls']
			);

			if (isset($_GET['src']['tglbayar'])&&$_GET['src']['tglbayar']=='y') {
				$search['tglbyr'] = 'y';
			}else{
				$search['tglbyr'] = 'n';
			}

			$inv->setData((object) $search);
			$data->tabeldata = $inv->SearchInvoice('array');
			$data->pagenum='0';
		} else {
			$data->tabeldata = $db->FetchColumns(['inv_no_invoice','nama_cust','inv_id_trans','nama_usaha','inv_deskripsi','sipm_desc','inv_status','si_desc','inv_date','tgl_bayar','items','jumlah','nama_petugas','terbayar'],'view_data_invoice','0',$limit);	
			$data->pagenum='0';
		}
		
		
		$data->subtitle = 'Tabel Invoice';
		$data->judul = 'FAC || Data Invoice FAC Institute';
		$data->username = $_SESSION['admin']['nama'];
		$data->url = $data->base_url.'administrasi/invoice/tabel-marketing';
		$data->totaldata = $inv->CountInvoiceData();
		
		$data->view('invoice/vtabelmarketing.inv',$data);
	}

    public static function MenuInvoiceBaru($idinvoice){
        $data = new Data();
		$data->JustInclude('administrasi/functions/DateFunc');
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$data->Model('Querybuilder');
		$data->Model('Invoice');
		$sesi = new Sessionz();
		$db = new QueryBuilder();
		$invoice = new Invoice();
		//session check
		$sesi->AdminMarketing();
		if ($_SESSION['admin']['tipe']=='marketing') {
			$mylink = 'invoice/tabel-marketing';
		} else {
			$mylink = 'data-invoice';
		}
		
		if(isset($_POST['in'])){
		    // print_r($_POST['in']);
		    $r = $db->InsertData('new_inv_payment',[
		    	'ssource' => $idinvoice,
		    	'ipayamt' => $_POST['in']['paidtotal'],
		    	'ipaymeth' => $_POST['in']['paidtype'],
		    	'ddate' => $_POST['in']['paiddate'],
		    	'sstaff' => $_SESSION['admin']['username']
		    ]);


		    if ($r) {

		    	if (isset($_POST['in']['paidmark'])&&$_POST['in']['paidmark']=='paid'||isset($_POST['in']['paidmark2'])&&$_POST['in']['paidmark2']=='paid') {
		    		$db->UpdateData('new_invoice',['inv_status' => '3'],array($db->GetCond('inv_no_invoice','=',$idinvoice)));
		    	}else{
		    	    $db->UpdateData('new_invoice',['inv_status' => '2'],array($db->GetCond('inv_no_invoice','=',$idinvoice)));
		    	}

		    	echo "<script>alert('data berhasil disimpan!');</script>";
		    	echo "<script>location.replace('".$data->base_url."administrasi/".$mylink."');</script>";
		    } else {
		    	echo "<script>alert('data gagal disimpan!');</script>";
		    }
		}
		
		
		$datainv = $db->FetchOneRow(['inv_status','inv_no_invoice','tgl_bayar','jumlah'],'view_data_invoice',array($db->GetCond('inv_no_invoice','=',$idinvoice)));//$invoice->SelectForPaymentMenu($idinvoice);
        // echo "ISINYA APAAN SI? ".$datainv['inv_status'];
        // print_r($datainv);
// 		if (intval($datainv['inv_status'])<1) {
// 			echo "<script>alert('Invoice ini belum terkirim/dibatalkan!');</script>";
// 			echo "<script>location.replace('".$data->base_url."administrasi/".$mylink."');</script>";
// 		}


		$data->daftarbayar = $db->FetchWhere(['dt_flag','dt_desc'],
			'desc_tbl',
			array(
				$db->GetCond('dt_relate_tbl','=','new_invoice.inv_py_mtd'),
				$db->GetCond('ivisibility','=','1')
			)
		);

		$data->page = 'tambah-invoice';
		$data->datepicker='1';
		$data->subtitle = 'Menu Pembayaran Invoice '.$datainv['inv_no_invoice'];
		$data->judul = 'FAC || Data Petugas FAC _institute';
		$data->username = $_SESSION['admin']['nama'];
		$data->islunas = $datainv['inv_status'];
		$data->totaldibayar = '0';
		$data->jumlahbayar = $datainv['jumlah'];	
		$data->listtrans = $invoice->getDataPembayaranInvoice($idinvoice);	
        $data->backlink = $mylink;
		$data->view('invoice/vpaidmenu2.inv',$data);
		
    }
    
    // klo proses migrasi sistem selesai, tolong dihapus (zeke 3 agustus 2019)
	public static function MenuInvoice($idinvoice){
		$data = new Data();
		$data->JustInclude('administrasi/functions/DateFunc');
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$data->Model('Querybuilder');
		$data->Model('Invoice');
		$sesi = new Sessionz();
		$db = new QueryBuilder();
		$invoice = new Invoice();
		//session check
		$sesi->AdminMarketing();
		if ($_SESSION['admin']['tipe']=='marketing') {
			$mylink = 'invoice/tabel-marketing';
		} else {
			$mylink = 'data-invoice';
		}

		if (isset($_POST['in'])) {
			if (isset($_POST['in']['paidmark'])&&$_POST['in']['paidmark']=='paid') {
				$ur = $db->UpdateData('new_invoice',
					[
						'inv_py_mtd' => $_POST['in']['paidtype'],
						'inv_date_paid' => $_POST['in']['paiddate'],
						'inv_owing' => '0',
						'inv_status' => '3'
					],
					array($db->GetCond('inv_no_invoice','=',$idinvoice))
				);

				if ($ur) {
					$db->InsertData('log_data',['username'=>$_SESSION['admin']['username'],'log_action' => $idinvoice.' telah dilunasi (3)','log_ip' => $_SERVER['REMOTE_ADDR']]);
					echo "<script>alert('data berhasil diubah!');</script>";
					echo "<script>location.replace('".$data->base_url."administrasi/".$mylink."');</script>";
				}else{ //else data insertion is failed
					echo "<script>alert('data gagal diubah, hubungi admin sistem!');</script>";
				}

			}else{ // kalo misalnya total dibayar tidak sama dengan nilai biaya transaksi

				if (isset($_POST['in']['paidmark2'])) { // kalo misalnya tanda lunas di ceklis
					$ur = $db->UpdateData('new_invoice',
						[
							'inv_py_mtd' => $_POST['in']['paidtype'],
							'inv_date_paid' => $_POST['in']['paiddate'],
							'inv_owing' => intval($_POST['in']['amount'])-intval($_POST['in']['paidtotal']),
							'inv_status' => '3'
						],
						array($db->GetCond('inv_no_invoice','=',$idinvoice))
					);

					if ($ur) {
						$db->InsertData('log_data',['username'=>$_SESSION['admin']['username'],'log_action' => $idinvoice.' telah dilunasi (3)','log_ip' => $_SERVER['REMOTE_ADDR']]);
						echo "<script>alert('data berhasil diubah!');</script>";
						echo "<script>location.replace('".$data->base_url."administrasi/".$mylink."');</script>";
					}else{ //else data insertion is failed
						echo "<script>alert('data gagal diubah, hubungi admin sistem!');</script>";
					}
				} else { // kalo misalnya masih ada nilai yang harus dibayar dan belom lunas.
					$ur = $db->UpdateData('new_invoice',
						[
							'inv_py_mtd' => $_POST['in']['paidtype'],
							'inv_date_paid' => $_POST['in']['paiddate'],
							'inv_owing' => intval($_POST['in']['amount'])-intval($_POST['in']['paidtotal']),
							'inv_status' => '2'
						],
						array($db->GetCond('inv_no_invoice','=',$idinvoice))
					);

					if ($ur) {
						$db->InsertData('log_data',['username'=>$_SESSION['admin']['username'],'log_action' => $idinvoice.' ditandai terbayar (2)','log_ip' => $_SERVER['REMOTE_ADDR']]);
						echo "<script>alert('data berhasil diubah!');</script>";
						echo "<script>location.replace('".$data->base_url."administrasi/".$mylink."');</script>";
					}else{ //else data insertion is failed
						echo "<script>alert('data gagal diubah, hubungi admin sistem!');</script>";
					}
				}
			}
		}


		$datainv = $invoice->SelectForPaymentMenu($idinvoice);

		if (intval($datainv['inv_status'])<1) {
			echo "<script>alert('Invoice ini belum terkirim/dibatalkan!');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi/".$mylink."');</script>";
		}


		$data->daftarbayar = $db->FetchWhere(['dt_flag','dt_desc'],'desc_tbl',array($db->GetCond('dt_relate_tbl','=','new_invoice.inv_py_mtd')));

		$data->page = 'tambah-invoice';
		$data->datepicker='1';
		$data->subtitle = 'Menu Pembayaran Invoice '.$datainv['inv_no_invoice'];
		$data->judul = 'FAC || Data Petugas FAC _institute';
		$data->username = $_SESSION['admin']['nama'];
		$data->tglbayar = $datainv['inv_date_paid'];
		$data->totaldibayar = $datainv['inv_owing'];
		$data->islunas = $datainv['inv_status'];
		$data->tipepembayaran = $datainv['inv_py_mtd'];
		$data->jumlahbayar = $datainv['jumlah'];		

		$data->view('invoice/vpaidmenu.inv',$data);
	}

	public static function NewPayType($idinv){
		$data = new Data();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$data->Model('Querybuilder');
		$sesi = new Sessionz();
		$db = new QueryBuilder();
	}

	public static function ListQuotation(){
		$data = new Data();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$sesi = new Sessionz();

		$data->subtitle = 'Data Penawaran Transaksi';
		$data->judul = 'FAC || Data Penawaran Transaksi';
		$data->username = $_SESSION['admin']['nama'];

		//session check
		$sesi->OnlyAdmin();
		$data->view('quotation/vnewtable.quot',$data);
	}

	//@url (value="/administrasi/quotation/api/search")	
	public static function SearchQuotationAPI($inputs){
		//session check for quotation $_SESSION['fac_app_id'] = 'facqtn'
		header('Content-Type: application/json');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		
		$data = new Data();
		$data->Model('Quotation');
		$quot = new Quotation();
		if (!empty($inputs)) {
			$inputs = json_decode($inputs);
			// check if JSON is valid
			if (is_null($inputs)) {
				$r = [];
			}else{
				$r = $quot->SearchQuotation($inputs);
			}
			
		}else{
			$r = array();
		}
		
		if (count($r)!='0') {
			$arr = [
			    'status' => ['code'=>'1','description'=>'data is available'],
			    'totaldata' => count($r),
			    'data'=>$r
			];
		}else{
			$arr = [
			    'status' => ['code'=>'0','description'=>'Cannot retrieve the data, check your input or the system may be in maintenance!'],
			    'data' => [] 
			];
		}
		echo json_encode($arr);
	}

	public static function RejectRequestAPI(){
		header('Content-Type: application/json');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		$arr = [
			'status' => ['code'=>'0','description'=>'Authentication required!']
		];
		echo json_encode($arr);
	}

	//@url (value="/administrasi/quotation/api?req=table")
	public static function TabelQuotationAPI(){
		header('Content-Type: application/json');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		$data = new Data();
		$data->Model('Quotation');
		$quot = new Quotation();
		$r = $quot->TableQuotations();
		// print_r($r);
		
		if (count($r)!='0') {
			$arr = [
			    'status' => ['code'=>'1','description'=>'data is available'],
			    'totaldata' => count($r),
			    'data'=>$r
			];
		}else{
			$arr = [
			    'status' => ['code'=>'0','description'=>'Database connection error!'],
			    'data' => [] 
			];
		}
		echo json_encode($arr);
		
	}


}