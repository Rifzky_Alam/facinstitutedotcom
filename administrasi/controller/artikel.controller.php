<?php include_once 'classes/View.php';
class Artikel{
	public static function DeleteArtikel($id){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$sesi = new Sessionz();
		$sesi->OnlyAdmin();
		$data->subtitle = "Input TO-DO";
		$data->username = $_SESSION['admin']['nama'];
		$victim = $db->FetchOneRow(['judul'],'artikel',array(
				$db->GetCond('id','=',$id)
			)
		);
		$data->judulartikel = $victim['judul'];
		if (count($victim)<1) {
			echo "<script>alert('Tidak ada data untuk ID ini.');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi/artikel');</script>";
		}else{
			if (isset($_POST['del'])&&$_POST['del']=='1') {
				$dr = $db->DeleteWhere('artikel',array(
					$db->GetCond('id','=',$id)
				));
				if ($dr) {
					$data->msg = 'Berhasil Dihapus';
				}else{
					$data->msg = 'Gagal dihapus';
				}

				$data->view('artikel/vdelsuccess.artikel', $data);	
			}else{
				$data->view('artikel/vconfirm.artikel', $data);	
			}
				
		}
	}

		
	}
