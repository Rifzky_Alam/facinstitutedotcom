<?php 
// session_start();
include_once 'classes/View.php';
class Adminfile{
    public static function UploadAttachment(){
    	$data = new Data();
    	$data->JustInclude('administrasi/functions/FileFunc');

    	
    	if (isset($_FILES['ifile'])) {
    		if (CheckExtension($_FILES['ifile'],['xls','xlsx','doc','docx','pdf','rar'])) {
    			//$_FILES['ifile']['name'] = date("Ymdhis").$_FILES['ifile']['name'];
    			if (UploadPic($_FILES['ifile'],'attachments/')) {
    				echo "<script>alert('File berhasil diupload');</script>";
    				echo "<script>location.replace('".$data->base_url."administrasi/upload-files');</script>";
    			}else{
    				echo "<script>alert('Terjadi kesalahan ketika upload file, harap hubungi admin sistem!');</script>";
    			}
    		}else {
    			echo "<script>alert('Ekstensi file tidak diterima');</script>";
    		}
    	}
    	

    	$data->page='upload';
    	$data->headertext = 'Input attachments baru';
    	$data->username = $_SESSION['admin']['nama'];
    	$data->View('files/vinput.uploadfiles',$data);	
    }

    public static function DeleteAttachment($file){
    	$data = new Data();
    	$data->JustInclude('administrasi/functions/FileFunc');
    	if (DeleteFile('attachments',$file)) {
    		echo "<script>alert('File berhasil dihapus');</script>";
    		echo "<script>location.replace('".$data->base_url."administrasi/upload-files');</script>";
    	}else{
    		echo "<script>alert('File gagal dihapus');</script>";
    		echo "<script>location.replace('".$data->base_url."administrasi/upload-files');</script>";
    	}
    }

    public static function ListAdminAttachments(){
    	$data = new Data();
    	$data->JustInclude('administrasi/functions/FileFunc');
    	$data->files = ScanDirectory('attachments/');
    	// print_r($files);
    	$data->username = $_SESSION['admin']['nama'];
    	$data->headertext = 'Data File dalam folder attachments';
    	$data->View('files/vlistattachment',$data);
    }

}

?>