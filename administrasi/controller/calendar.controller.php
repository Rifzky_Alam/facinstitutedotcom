<?php include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/classes/View.php';
class Calendarc{


	public static function FixingData($idcal){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$data->JustInclude('administrasi/functions/StringFunc');
		// echo $_SESSION['admin']['username'];
		$r = $db->InsertData('trans_cal_approv',
			[
				'tca_id' => md5($_SESSION['admin']['username'].$idcal),
				'tca_staff' => $_SESSION['admin']['username'],
				'tca_cal_id' => $idcal
			]
		);
		if ($r) {
			echo "<script>alert('Tanggal sudah di verifikasi.');</script>";
		} else {
			echo "<script>alert('Tanggal gagal di verifikasi, anda sudah verifikasi atau ada kesalahan database.');</script>";
		}


		$data->listdata = $db->FetchJoinWhere(['`tca_id`', '`tca_staff`', '`tca_cal_id`','nama'],
			'trans_cal_approv',
			array($db->getJoin('INNER','users','users.username','trans_cal_approv.tca_staff')),
			array($db->GetCond('tca_cal_id','=',$idcal))
		);
		// print_r($data->listdata);

		$data->subtitle = 'Tabel Verifikasi Calendar';
		$data->judul = 'FAC || Data Calendar FAC Institute';
		$data->username = $_SESSION['admin']['nama'];
		$data->view('calendar/view-cal-fixing', $data);
	}
}