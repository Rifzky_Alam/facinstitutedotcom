<?php 
include_once 'classes/View.php';
class Attendance{
    
    public static function index(){
    	$data = new Data();

    	$data->Model('Petugas');
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$sesi = new Sessionz();
		$sesi->Staff();

		$petugas = new ControlPetugas();
		$data->username=$_SESSION['admin']['nama'];

    	$data->title="Laporan Absensi";
		$data->subtitle = "Data Laporan Absensi";	
		$petugas->setUsername($_SESSION['admin']['username']);
		$data->team = $_SESSION['admin']['team'];
		$data->IncludeFileWithData('administrasi/sidebar.php',$data);

		$data->dataabsensi = $petugas->DataBelumAbsenPulang();
		$data->View('absensi/vbelumabsen',$data);
    }

    // @url: /administrasi/attendance/request
    public static function RequestAbsenLists(){
    	$data = new Data();
    	$data->Model('Petugas');
    	$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$sesi = new Sessionz();
    	$petugas = new ControlPetugas();

    	$sesi->OnlyAdmin();
		
    	
    	$data->listrequest = json_decode($petugas->dataRequest(''));

    	$data->judul = 'FAC || Request Absen';
    	$data->subtitle = 'Data Request Absen';
    	$data->username=$_SESSION['admin']['nama'];
    	$data->View('absensi/vrequest.absen',$data);
    }

    public static function Edit($idabsen){
    	$data = new Data();

    	$data->Model('Petugas');
    	$data->Model('Querybuilder');
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$sesi = new Sessionz();
		$sesi->Staff();
		$petugas = new ControlPetugas();
		$db = new QueryBuilder();
    	$jamskrg = date('H');
		$jambuka = 1;
		$jamtutup = 23;
		
		$data->title="Edit Absensi";
		$data->subtitle = "Edit Absensi";	
		$data->username=$_SESSION['admin']['nama'];
		if ($jamskrg <= $jamtutup && $jamskrg >= $jambuka) {
			$dataedit = ['id' => $idabsen];
			if (isset($_POST['in'])) {
			    $idata = $db->FetchWhere(['username'],'users',array($db->GetCond('tipe','=','admin')));
				if (!empty($_POST['in']['jam'])) {
					// $jam = explode(':', $_POST['in']['jam']);
					if (preg_match('/AM/i', $_POST['in']['jam'])) {
						$_POST['in']['jam'] = str_replace(' AM', ':00', $_POST['in']['jam']);
					} elseif (preg_match('/PM/i', $_POST['in']['jam'])) {
						$_POST['in']['jam'] = str_replace(' PM', ':00', $_POST['in']['jam']);
						$jam = explode(':', $_POST['in']['jam']);
						$jam[0] += 12;
						$_POST['in']['jam'] = $jam[0].':'.$jam[1].':'.$jam[2];
					}
			
					$_POST['in']['jam'] = $jam[0].':'.$jam[1].':00';
					$dataedit['jampulang'] = $_POST['in']['jam'];
					$waktu = explode(':',$_POST['in']['jam']);
					$dataedit['alasan'] = $_POST['in']['alasan'];
					// $petugas->setAbsen((object)$dataedit);
					
					// echo $_POST['in']['jam'];
                    if(@$waktu[0]==''||@$waktu[1]==''||@$waktu[2]==''){
    					echo "<script>alert('isi data dengan benar.');</script>";
                        $qreturn = false;
                    }else{
                        $qreturn = $db->UpdateData('absensi',
    						[
    							'absen_pulang'=>$_POST['in']['jam'],
    							'absen_status'=>'-1',
    							'absen_ket'=> $_POST['in']['alasan']
    						],
    						array($db->GetCond('id','=',$idabsen))
    					);
                    }
    					

					if ($qreturn) {
					    
					    foreach ($idata as $key) {
							$db->InsertData('notifikasi',[
								'ntf_judul' => 'Request Absen',
								'ntf_desc' => $_SESSION['admin']['nama'].' request absen.',
								'ntf_username' => $key['username'],
								'ntf_url_target' => $data->base_url.'administrasi/attendance/admin',
								'ntf_status' => '0'
							]);	    	
					    }
					    
						echo "<script>alert('Absensi Berhasil diupdate, harap menunggu admin untuk mengaktifkan laporan anda.');</script>";
						echo "<script>location.replace('".$data->base_url."administrasi/attendance');</script>";
					} else {
						
					}

				}else {
					echo "<script>alert('Jam Pulang tidak boleh kosong!');</script>";
				}
					
			}
				
			$petugas->setAbsen((object)$dataedit);
			
			$dataabsensi = $petugas->SelectAbsenByID();
			$data->nama_petugas=$dataabsensi->nama_user;
			$data->absenmasuk=$dataabsensi->absen_masuk;
			$data->nama_usaha=$dataabsensi->nama_usaha;
			$data->tglabsen = $dataabsensi->tanggal_absen;
			$data->View('absensi/veditabsenstaff',$data);
		} else {

			$data->absen_masuk = "";
			$data->absen_pulang = "";
			$data->is_success = "Pemberitahuan";
			$data->notif = 'Sistem Ini dibuka mulai jam '.$jambuka.' dan ditutup pada jam '.$jamtutup;
			$data->View('absensi/vnotif-absen',$data);
		}
    }

    public static function Cekbuatinputlaporan(){
    	$data = new Data();
    	$data->Model('Laporan');
    	$laporan = new ControlLaporan();
    	$hari = date('l');
    	echo 'Hari: '.$laporan->cariNamaHari($hari)."<br>";
    	echo 'Jenis Hari: '.$laporan->cariHariKerjaNasional($hari)."<br>";
    	$jamdatang = '09:00:00';
    	$jampulang = '18:00:00';
    	echo $laporan->carioverTime($jamdatang,$jampulang);
    }

    public static function ListAdmin(){
    	$data = new Data();
    	$data->Model('Laporan');
    	$data->Model('Querybuilder');
    	$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
    	$data->JustInclude('administrasi/functions/DateFunc');
    	$data->objlaporan = new ControlLaporan();
    	$db = new QueryBuilder();
		$sesi = new Sessionz();
		$sesi->OnlyAdmin();
		$data->username=$_SESSION['admin']['nama'];
    	
    	// print_r(json_encode($db->FetchWhere(['id','username','absen_masuk','absen_pulang','absen_status','absen_ket'],'absensi',array($db->GetCond('absen_status','=','-1')))));
    	$db->setOrder('tanggal_absen','DESC');
    	$data->tablelist = $data->objlaporan->getDataPendingAttendance();
    	// print_r(json_encode($data->tablelist));
    	$data->View('absensi/vlistadmin.editabsen',$data);
    }

    public static function RejectRequest($idabsen){
    	$data = new Data();
    	$data->Model('Laporan');
    	$laporan = new ControlLaporan();
    	$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$r = $db->FetchOneRow(['username','tanggal_absen'],'absensi',array($db->GetCond('id','=',$idabsen)));
    	if ($laporan->RejectRequestEdit($idabsen)) {
	    	
			$db->InsertData('notifikasi',[
				'ntf_judul' => 'Request Absen Ditolak',
				'ntf_desc' => 'Absen tanggal '.$r['tanggal_absen'].' tidak disetujui oleh admin',
				'ntf_username' => $r['username'],
				'ntf_url_target' => 'https://fac-institute.com/administrasi/attendance',
				'ntf_status' => '0'
			]);
			
    		echo "<script>alert('Request telah ditolak');</script>";
	    	echo "<script>location.replace('".$data->base_url."administrasi/attendance/admin');</script>";
    	} else {
    		echo "<script>alert('Laporan gagal ditolak.');</script>";
	    	echo "<script>location.replace('".$data->base_url."administrasi/attendance/admin');</script>";
    	}
    	
    }

    public static function CreateReport($idabsen){
    	$data = new Data();
    	$data->Model('Querybuilder');
    	$data->Model('Laporan');
    	$laporan = new ControlLaporan();
    	$db = new QueryBuilder();
    	$data->JustInclude('administrasi/functions/DateFunc');
    	$qr = $db->FetchOneRow(['id','username','tanggal_absen','absen_masuk','absen_pulang'],'absensi',array($db->GetCond('id','=',$idabsen)));
    	
    	$arrinput = [
    		'id'=>md5('L-'.$qr['username'].$qr['tanggal_absen']),
    		'id_absen' => $qr['id'],
    		'username' => $qr['username'],
    		'tanggal' => $qr['tanggal_absen'],
    		'hari' => $laporan->cariNamaHari(FindDay($qr['tanggal_absen'])),
    		'jns_hari_kerja' => $laporan->cariHariKerjaNasionaldua(FindDay($qr['tanggal_absen']),$qr['tanggal_absen']),
    		'jumlah_overtime' => RoundOvertime(intval($laporan->carioverTime($qr['absen_masuk'],$qr['absen_pulang'])))
    	];
    	$inputresult = $db->InsertData('laporan_harian',$arrinput);

    	if ($inputresult) {
    		// update status absensi
    		$qreturn = $db->UpdateData('absensi',
							[
								'absen_status'=>'3'
							],
							array($db->GetCond('id','=',$idabsen))
						);
	    	if ($qreturn) {
	    		$db->InsertData('notifikasi',[
					'ntf_judul' => 'Laporan Request Di Terima',
					'ntf_desc' => 'Laporan tanggal '.$qr['tanggal_absen'].' sudah dibuat.',
					'ntf_username' => $qr['username'],
					'ntf_url_target' => 'https://fac-institute.com/administrasi/user/laporan-harian',
					'ntf_status' => '0'
				]);
	    		echo "<script>alert('Laporan sudah dibuat.');</script>";
	    		echo "<script>location.replace('".$data->base_url."administrasi/attendance/admin');</script>";
	    	} else {
	    		echo "<script>alert('Laporan sudah dibuat, kesalahan dalam perubahan data absensi');</script>";
	    		echo "<script>location.replace('".$data->base_url."administrasi/attendance/admin');</script>";
	    	}
    	}else{
    		echo "<script>alert('Laporan gagal dibuat, harap kontak admin sistem!');</script>";
	    	echo "<script>location.replace('".$data->base_url."administrasi/attendance/admin');</script>";
    	}
    	
    }

}

?>