<?php include_once 'classes/View.php';
class Todo{
	public static function index(){

	}

	public static function Input(){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$data->JustInclude('administrasi/functions/imageuploadfunc');
		$sesi = new Sessionz();
		$sesi->OnlyAdmin();

			

		if (isset($_POST['in'])) {
			// print_r($_POST['in']);
			$hash = md5($_POST['in']['assigner'].$_POST['in']['assignee'].date('Y-m-d').$_POST['in']['task']);
			$arr = [
				'ft_hash' => $hash,
			    'ft_assigner' => $_POST['in']['assigner'],
				'ft_assignee' => $_POST['in']['assignee'],
				'ft_priority' => $_POST['in']['priority'],
				'ft_assignment' => $_POST['in']['task'],
				'ft_desc' => $_POST['in']['desc'],
				'ft_due' => $_POST['in']['due'],
			];
			$r = $db->InsertData('fac_todolist',$arr);
			if ($r) {
				echo "<script>alert('Data telah disimpan');</script>";
			}else{
				echo "<script>alert('Data telah disimpan');</script>";
			}

			if (isset($_FILES['atch'])&&$_FILES['atch']['size'] != 0) {
				// print_r($_FILES['atch']);
				// echo 'file scanned';
				$fileEx = '.'.pathinfo($_FILES['atch']['name'],PATHINFO_EXTENSION);
				$_FILES['atch']['name'] = $hash.$fileEx;
				$folderatc = 'administrasi/todoattachments/';
				$arr = [
				    'atch_source' => $hash,
				    'atch_folder' => $folderatc,
				    'atch_filename' => $_FILES['atch']['name']
				];


				$uploadresponse = UploadImage(['jpg','png'],$_FILES['atch'],$data->homedir.$folderatc);
				if ($uploadresponse['result']) {
					echo "<script>alert('".$uploadresponse['msg']."');</script>";
					$db->InsertData('attachments',$arr);
				}else {
					echo "<script>alert('".$uploadresponse['msg']."');</script>";
				}
					
			}else { //else file is not uploaded
				
			}



		}

		$data->subtitle = "Input TO-DO";
		$data->username = $_SESSION['admin']['nama'];
		$data->datepicker = 1;
		$data->adminlist = $db->FetchWhere(
			['username','nama'],
			'users',
			array(
				$db->GetCond('tipe','=','admin'),
				$db->GetCond('status','=','aktif')
			)
		);
		$data->stafflist = $db->FetchWhere(
			['username','nama'],
			'users',
			array(
				$db->GetCond('status','=','aktif')
			)
		);
		
		$data->view('todo/vinput.todo', $data);

		/*
		$arr = [
		    'ft_assigner' => 'assigner',
			'ft_assignee' => 'si fulan',
			'ft_priority' => '1',
			'ft_assignment' => 'lorem ipsum...',
			'ft_due' => '2018-02-10 16:47:21',
			'ft_attachment' => 'foto.jpg',
			'ft_link' => 'http://fac-institute.com'
		];
		$r = $db->InsertData('fac_todolist',$arr);
		if ($r) {
			echo 'inserted';
		}else{
			echo 'failed';
		}
		*/
	}

	public static function EditAdmin($id){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		if(isset($_POST['in'])){
		    $arr = [
    		    'ft_assigner' => 'assignerz',
    			'ft_assignee' => 'si fulanz',
    			'ft_priority' => '7',
    			'ft_assignment' => 'isi todolist',
    			'ft_due' => '2018-02-10 21:21:12',
    			'ft_attachment' => 'gambar.jpg',
    			'ft_link' => 'http://topm2.com'
    		];
    		$r = $db->UpdateData('fac_todolist',$arr,array($db->GetCond('ft_id','=',$id)));
    		if ($r) {
    			echo 'updated';
    		}else{
    			echo 'failed';
    		}
		}
	}

	public static function Reject($id){
		$data = new Data();
		$data->Model('Querybuilder');
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$sesi = new Sessionz();
		$sesi->OnlyAdmin();
		$db = new QueryBuilder();
		$r = $db->UpdateData('fac_todolist',['ft_status' => '0'],array($db->GetCond('ft_id','=',$id)));
		if ($r) {
			header('Location: '.$data->base_url.'administrasi/todo/');
		} else {
			
		}
	}

	public static function Remove($id){
		
	}
	//@url /administrasi/todo/editstaff/{value}
	public static function EditStaff($id){
		$data = new Data();
		$data->Model('Querybuilder');
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$sesi = new Sessionz();
		$db = new QueryBuilder();

		if (isset($_POST['selesai'])) {
			$r = $db->UpdateData('fac_todolist',['ft_status' => '2'],array($db->GetCond('ft_id','=',$id)));
			if ($r) {
				echo "<script>location.replace('".$data->base_url."administrasi/');</script>";
			} else {
				echo "<script>alert('Data gagal diupdate!');</script>";
			}
		}

		if (isset($_POST['in'])) {
			$tgl = new DateTime();
			$arr = [
			    'ft_message' => $_POST['in']['msg'],
				'ft_link' => $_POST['in']['link'],
				'ft_updated' => $tgl->format('Y-m-d H:i:s'),
				'ft_status' => '1'
			];

			$r = $db->UpdateData('fac_todolist',$arr,array($db->GetCond('ft_id','=',$id)));
			if ($r) {
				echo "<script>location.replace('".$data->base_url."administrasi/');</script>";
			}else{
				echo "<script>alert('Data gagal diupdate!');</script>";
			}	
		}

		//data todo from admin
		$arr = [
		    'ft_assigner',
		    'ft_assignee',
			'ft_priority',
			'ft_message',
			'ft_desc',
			'ft_status',
			'ft_link',
			'ft_assignment',
			'ft_message',
			'ft_due'
		];
		$r = $db->FetchOneRow($arr,'fac_todolist',array($db->GetCond('ft_id','=',$id)));
		$sesi->OnlyMe($r['ft_assignee']);
		$data->assigner = $r['ft_assigner'];
		$data->priority = $r['ft_priority'];
		$data->judul_tugas = $r['ft_assignment'];
		$data->ket_tugas = $r['ft_message'];
		$data->deskripsi = $r['ft_desc'];
		$data->deadline = $r['ft_due'];
		$data->pesan = $r['ft_message'];
		$data->link = $r['ft_link'];
		$data->status = $r['ft_status'];

		$data->subtitle = "Input TO-DO";
		$data->username = $_SESSION['admin']['nama'];

		
		$data->view('todo/vinputassignee.todo', $data);
	}

	public static function SoftDelete(){
		
	}

	public static function Delete(){
		
	}

	public static function DisplayPetugas(){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		
	}

	public static function ShowDetail($id){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$data->JustInclude('administrasi/functions/StaffFunc');

		$rstaff = $db->FetchColumns(['username','nama'],'users');
		$arr = [
		    'ft_assigner',
			'ft_assignee',
			'ft_priority',
			'ft_desc',
			'ft_message',
			'ft_assignment',
			'ft_due',
			'ft_status',
			'ft_attachment',
			'ft_link',
			'ft_updated',
			'ft_timestamp'
		];
		$r = $db->FetchOneRow($arr,'fac_todolist',array($db->GetCond('ft_id','=',$id)));
		$data->assigner = FindStaff($r['ft_assigner'],$rstaff);
		$data->assignee = FindStaff($r['ft_assignee'],$rstaff);
		$data->priority = $r['ft_priority'];
		$data->desc = $r['ft_desc'];
		$data->feedback = $r['ft_message'];
		$data->judul_tugas = $r['ft_assignment'];
		$data->deadline = $r['ft_due'];
		$data->status = $r['ft_status'];
		$data->lampiran = $r['ft_attachment'];
		$data->link = $r['ft_link'];
		$data->updated = $r['ft_updated'];
		$data->timestamp = $r['ft_timestamp'];
		$data->username = $_SESSION['admin']['nama'];
		$data->judul = "FAC - TO-DO List";
		$data->subtitle = "Detail Tugas ".FindStaff($data->assignee,$rstaff);
		$data->view('todo/vdetail.todo', $data);
	}

	public static function Test(){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		// $r = $db->FetchWhere(['username','nama'],'users',array($db->GetCond('tipe','=','admin'),$db->GetCond('status','=','aktif')));
		$r = $db->FetchWhere(['username','nama'],'users',array($db->GetCond('tipe','!=','admin'),$db->GetCond('status','=','aktif')));
		print_r(json_encode($r));
	}

	public static function ListKerjaan(){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		$arr = [
			'ft_id',
		    'ft_assigner',
			'ft_assignee',
			'fac_todolist_pr.tdpr_desc AS ft_priority',
			'ft_assignment',
			'ft_desc',
			'ft_due',
			'ft_status',
			'ft_attachment',
			'ft_link'
		];
		$data->staffs = $db->FetchColumns(['username','nama'],'users');
		$data->list = $db->FetchJoinWhere($arr,'fac_todolist',array($db->getJoin('INNER','fac_todolist_pr','fac_todolist_pr.tdpr_id','fac_todolist.ft_priority')),array(
			$db->GetCond('ft_assignee','=',$_SESSION['admin']['username']),
			$db->GetCond('fac_todolist.ft_status','<','2'),
			$db->GetCond('ft_status','>','-2')
		));


		$hasil = [
		    'jumlahdata' => count($data->list),
		    'staffs' => $data->staffs,
		    'data' => $data->list 
		];
		return $hasil;
	}

	public static function Showmine(){
		$data = new Data();
		$data->JustInclude('administrasi/functions/StaffFunc');
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		$arr = [
			'ft_id',
		    'ft_assigner',
			'ft_assignee',
			'ft_priority',
			'ft_assignment',
			'ft_due',
			'ft_status',
			'ft_attachment',
			'ft_link'
		];
		$data->staffs = $db->FetchColumns(['username','nama'],'users');
		$data->list = $db->FetchWhere($arr,'fac_todolist',array($db->GetCond('ft_assignee','=',$_SESSION['admin']['username'])));
		$data->subtitle = "Input TO-DO";
		$data->username = $_SESSION['admin']['nama'];
		$data->judul = 'FAC - Todolist';
		// print_r(json_encode($data->list));
		// print_r($_SESSION['admin']);
		$data->view('todo/vdisplayadmin.todo', $data);	
	}


	public static function ChangeStatus($status, $id){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		if (isset($_SESSION['todochangestatus'])&&$_SESSION['todochangestatus']=='allow') {
			$r = $db->UpdateData('fac_todolist',['ft_status' => $status],array($db->GetCond('ft_id','=',$id)));	
		} else {
			$r = false;
		}
		
		if ($r) {
			unset($_SESSION['todochangestatus']);
			echo "<script>location.replace('".$data->base_url."administrasi/todo/showadmin');</script>";
		} else {
			echo "<script>alert('Cannot change anything from outer system!');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi/');</script>";	
		}
	}

	// @url /administrasi/todo/showadmin
	public static function DisplayAdmin(){
		$data = new Data();
		$data->JustInclude('administrasi/functions/StaffFunc');
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		$_SESSION['todochangestatus']='allow';
		
		$data->staffs = $db->FetchColumns(['username','nama'],'users');

		$arr = [
			'ft_id',
		    'ft_assigner',
			'ft_assignee',
			'tdpr_desc',
			'ftdls_desc',
			'ft_priority',
			'ft_assignment',
			'ft_due',
			'ft_status',
			'ft_attachment',
			'ft_link'
		];
		$r = $db->FetchJoinWhere(
			$arr,
			'fac_todolist AS ft', 
			array(
				$db->getJoin('LEFT','fac_todolist_pr AS pr','pr.tdpr_id','ft.ft_priority'),
				$db->getJoin('LEFT','fac_todolist_st AS st','st.ftdls_id','ft.ft_status')
			),
			array(
				$db->GetCond('ft_status','>','-2')
			),'0','30');
			
		// $r = $db->FetchOneRow($arr,'fac_todolist',array($db->GetCond('ft_id','=',$id)));
		// print_r(json_encode($r));
		$data->subtitle = "Input TO-DO";
		$data->username = $_SESSION['admin']['nama'];

		$data->list = $r;
		$data->view('todo/vdisplayadmin.todo', $data);	
	}
}