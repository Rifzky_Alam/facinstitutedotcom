<?php include_once 'classes/View.php';
class GoogleCalendar{
	
	// @url="/administrasi/googlecal/sendbackupinvitation?id={value}"
	public static function SendingBackupInvitation($idgocal){
		$data = new Data();
		if (isset($_SESSION['facgocalsendbackup'])&&$_SESSION['facgocalsendbackup']=='yes') {
			
			$data->Model('Googlecalendarmodel');
			$data->Model('Mail');
			$db = new Googlecalendarmodel();
			$surat = new FACMail();
			$data->JustInclude('administrasi/view/system/email-master.sys');
			$data->JustInclude('administrasi/view/googlecal/vemailbackup.googlecal');
			$dataku = $db->FetchForBackupEmailTrainer($idgocal);

			for ($i = 0; $i < count($dataku); $i++) {
				$idata = $dataku[$i]; 
				
				$data->nama_cust = $idata['trainer'];
				$data->mytitle = $idata['g_summary'];
				$data->emailtitle = 'Backup - Invitation: [ '.$idata['g_summary'].' ] @ [ '.$idata['tanggal_agenda'].' ] ('.$idata['gca_user_email'].')';
				$data->tanggal = $idata['tanggal_agenda'];
				$data->tempattraining = $idata['trans_lokasi'];
				$data->jenisaccurate = $idata['va_nama'];
				$data->jenisusaha = $idata['jenis_usaha'];
				$data->namacust = $idata['nama_cust'];
				$data->telpcust = $idata['telp_cust']; 
				$data->emailcust = $idata['email_cust'];
				$data->marketing = $idata['marketing_nama'];
				$data->petugas = $idata['petugas'];
				$data->tipepengguna = $idata['tipepengguna'];
				$data->lastrainer = $idata['last_trainer'];
				$data->attendeez = $idata['attendees'];
				$data->agendas = $idata['agendas'];
				$data->desc = $idata['g_description'];
				$data->tanggals = $idata['tanggalz'];

				$data->to = $idata['gca_user_email'];
				$data->nama = '';
				$data->subject = 'Backup - Invitation: [ '.$idata['g_summary'].' ] @ [ '.$idata['tanggal_agenda'].' ] ('.$idata['gca_user_email'].')';
				$data->content = BodySample($data);
				
				$surat->sendMail($data);
			}
			unset($_SESSION['facgocalsendbackup']);	
			header('location:'.$data->base_url.'administrasi/googlecal/summarylist?tr='.$idata['trans_id']);
		}else{
			header('location:'.$data->base_url.'administrasi/');
		}

	}

	// @url="/administrasi/googlecal/reptraining?det={value}", method=GET
	public static function GetDetailRepTraining($idusaha){
		$data = new Data();
		$data->Model('Googlecalendarmodel');
		$db = new Googlecalendarmodel();

		$data->listdata = $db->FetchDetailRepeatTraining($idusaha);

		$data->subtitle = "Detail Training Repeat ".@$data->listdata[0]['nama_usaha'];
		$data->judul = 'Detail Training Repeat';
		$data->username = $_SESSION['admin']['nama'];
		$data->roleuser = $_SESSION['admin']['tipe'];
		$data->datepicker = '1';
		// print_r($data->listdata);
		$data->View('laporan/staff/vdetreptraining.laporan',$data);
	}

	// @url="/administrasi/googlecal/reptraining", method=GET	
	public static function RepeatTrainingData($tglawal='',$tanggalakhir=''){
		$data = new Data();
		$data->Model('Googlecalendarmodel');
		$db = new Googlecalendarmodel();
		// echo $tglawal.' '.$tanggalakhir;
		if ($tglawal!=''&&$tanggalakhir!='') {
			$data->listdata=$db->FetchDataRepeatTrainerByTgl($tglawal,$tanggalakhir);
		}else{
			$data->listdata = $db->FetchDataRepeatTrainer('curmonth');
		}

		$data->subtitle = "List Training Repeat";
		$data->judul = 'Training Repeat';
		$data->username = $_SESSION['admin']['nama'];
		$data->roleuser = $_SESSION['admin']['tipe'];
		$data->datepicker = '1';
		// print_r($data->listdata);
		$data->View('laporan/staff/vlistrepeattraining.laporan',$data);

	}
	// @url="/administrasi/googlecal/apirepeattrainer?m={value}", method=GET	
	public static function GetRepeatTrainer($menu='curmonth'){
		header('Content-Type: application/json');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		$data = new Data();
		$data->Model('Googlecalendarmodel');
		$db = new Googlecalendarmodel();
		$idata = $db->FetchDataRepeatTrainer($menu);
		// print_r($idata);
		if (count($idata)=='0') {
			$arr = [
				'status' => ['code'=>'0','description'=>'no data available'],
				'listdata' => $idata
			];
		} else {
			$arr = [
				'status' => ['code'=>'1','description'=>'Data Available'],
				'listdata' => $idata
			];
		}
		echo json_encode($arr);
	}

	// @url="/administrasi/calendar?gcal=table", method=GET
	public static function TableCalendar(){
		$data = new Data();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$data->Model('Querybuilder');
		$data->Model('Googlecalendarmodel');
		$sesi = new Sessionz();
		$db = new Googlecalendarmodel();
		$sesi->AdminMarketing();

		$data->listdata = $db->TableAsPerMonth();
		// print_r($data->listdata);
		$data->headertext = "List Google Calendar";
		$data->judul = 'FAC || Google Calendar';
		$data->username = $_SESSION['admin']['nama'];
		$data->roleuser = $_SESSION['admin']['tipe'];
		// $data->teamuser = $_SESSION['admin']['team'];
		// echo $_SESSION['admin']['tipe'];

		$data->View('googlecal/vtbllistall.googlecal',$data);
	}

	// @url="/administrasi/googlecal/upcoming", method=GET
	public static function UpcomingeventAPI(){
		$data = new Data();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$data->Model('Querybuilder');
		$data->Model('Googlecalendarmodel');
		$sesi = new Sessionz();
		$db = new Googlecalendarmodel();
		$sesi->AdminMarketing();

		header('Content-Type: application/json');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");

		$r = $db->UpcomingEvents(date('Y-m-d'));

		$arr = [
			'status' => ['code'=>'1','description'=>'Page is ok!'],
			'data' => $r 
		];

		echo json_encode($arr);
	}

	// @url="/administrasi/googlecal/delattendee?id={value}", method=GET
	public static function Deleteattendee($idrow){
		$data = new Data();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$data->Model('Querybuilder');
		$sesi = new Sessionz();
		$db = new QueryBuilder();
		$sesi->AdminMarketing();
		$idgcal = $db->FetchOneRow(['gca_source'],'google_cal_attendees',array($db->GetCond('gca_id','=',$idrow)));
		
		if(isset($_POST['in']['idat'])){
		    if ($_POST['in']['token']==$_SESSION['secure']) {
			    $capchaisok=true;
		    }else{
			    $capchaisok=false;
		    }
		    
		    if($capchaisok){
		      //  echo "delete is ready!";
		        $r = $db->DeleteWhere('google_cal_attendees',array($db->GetCond('gca_id','=',$idrow)));
        		$rdua = $db->InsertData(
        			'log_data',[
        				'log_action' => $_SESSION['admin']['nama'].' Deleted data gocal attendee ('.$idgcal['gca_source'].')',
        			'username' => $_SESSION['admin']['username'],
        			'log_ip' => $_SERVER['REMOTE_ADDR']
        			]
        		);
        		if ($r) {
        			header('location:'.$data->base_url.'administrasi/googlecal/newattendee?id='.$idgcal['gca_source']);
        		} else {
        			header('location:'.$data->base_url.'administrasi/googlecal/newattendee?id='.$idgcal['gca_source']);
        		}
		    }else{
		        echo "<script>alert('Token tidak sesuai, harap untuk mengulangi.')</script>";
		    }
        		
		}
		
		
		
		$data->idgocalsrc = $idgcal['gca_source'];
		$data->idat = $idrow;
		$data->headertext = "Google Cal - Verification";
		$data->page = 'penawaran';
		$data->judul = 'FAC || Google Calendar - Delete Attendee';
		$data->username = $_SESSION['admin']['nama'];
		$data->user = $_SESSION['admin']['username'];
		$data->View('googlecal/vtbldeleteattendees.googlecal',$data);
// 		print_r($idgcal);

	}

	// @url="/administrasi/googlecal/newattendee?id={value}", method=POST
	public static function NewAttendeePOST($masukdata,$idgooglecaldata){
		$data = new Data();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$data->Model('Querybuilder');
		$sesi = new Sessionz();
		$db = new QueryBuilder();
		$sesi->AdminMarketing();

		$r = $db->InsertData('google_cal_attendees',[
			'gca_source' => $idgooglecaldata,
			'gca_user_email' => $masukdata['peserta'],
			'gca_user_role' => $masukdata['peran']]
		);
        $rdua = $db->InsertData(
			'log_data',[
				'log_action' => $_SESSION['admin']['nama'].' Inserted '. $masukdata['peserta'] .' in data gocal attendee',
			'username' => $_SESSION['admin']['username'],
			'log_ip' => $_SERVER['REMOTE_ADDR']
			]
		);
		if ($r) {
			echo "<script>alert('Data berhasil disimpan!');</script>";
			echo "<script>location.replace('".
			$data->base_url."administrasi/googlecal/newattendee?id=".$idgooglecaldata.
			"');</script>";
		} else {
			echo "<script>alert('Data gagal disimpan, harap hubungi admin sistem!');</script>";
		}

	}

	// @url="/administrasi/googlecal/newattendee?id={value}", method=GET
	public static function NewAttendee($idgooglecaldata){
		$data = new Data();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$data->Model('Querybuilder');
		$sesi = new Sessionz();
		$db = new QueryBuilder();

		$sesi->AdminMarketing();

		$masterdata = $db->FetchOneRow(['g_status','g_id_cal'],'google_cal_data',array($db->GetCond('g_row','=',$idgooglecaldata)));
		$fackalobj = $db->FetchOneRow(['acara'],'fac_calendar',array($db->GetCond('id','=',$masterdata['g_id_cal'])));
		$data->idtrans = $fackalobj['acara'];
		if ($masterdata['g_status']=='1') {
			echo "<script>alert('Data berhasil disimpan!');</script>";
			echo "<script>location.replace('".
			$data->base_url."administrasi/googlecal/updateadmin?idcal=".$masterdata['g_id_cal'].
			"');</script>";
		}

		$data->listuser = $db->FetchWhere(['email','nama'],'users',array($db->GetCond('users.status','=','aktif')));
		$data->listperan = $db->FetchColumns(['pk_id','pk_peran'],'daftar_peran_kerja');
		$data->listpeserta = $db->FetchJoinWhere(['gca_id','gca_user_email','nama','pk_peran','gca_user_role'],
			'google_cal_attendees',
			array(
				$db->getJoin('LEFT','users','google_cal_attendees.gca_user_email','users.email'),
				$db->getJoin('LEFT','daftar_peran_kerja','google_cal_attendees.gca_user_role','daftar_peran_kerja.pk_id')
			),
			array($db->GetCond('gca_source','=',$idgooglecaldata)));
		$data->idfackal = $masterdata['g_id_cal'];


		// print_r($data->listpeserta);
		$data->headertext = "Google Cal - Verification";
		$data->judul = 'FAC || Google Calendar - Verification & GetToken';
		$data->username = $_SESSION['admin']['nama'];
		$data->user = $_SESSION['admin']['username'];

		$data->View('googlecal/vinputattendee.googlecal',$data);
	}
	
	public static function Verifikasi($idgooglecal){
		$data = new Data();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$data->Model('Querybuilder');
		$sesi = new Sessionz();
		$db = new QueryBuilder();

		$sesi->AdminMarketing();

		$idata = $db->FetchJoinWhereSingleRow(['*'],
			'google_cal_data',
			array(
				$db->getJoin('INNER','acara_harian','acara_harian.cal_id','google_cal_data.g_id_cal'),
				$db->getJoin('INNER','new_transaksi','new_transaksi.trans_id','acara_harian.transaksi'),
				$db->getJoin('INNER','perusahaan','perusahaan.id','acara_harian.id')
			),
			array($db->GetCond('g_row','=',$idgooglecal))
		);
		$singlerow = $db->FetchOneRow(['*'],'google_cal_verf',array($db->GetCond('gcv_src','=',$idgooglecal)));
		$data->myhash = $singlerow['gcv_hash'];
		$waktu = explode(' - ', $idata['trans_waktu']);
		$jsondata = [
			'perusahaan' => $idata['perusahaan'],
			'userlogin' => $_SESSION['admin']['username'],
		    'tanggal' => $idata['tanggal'],
		    'waktumulai' => $waktu[0],
		    'waktuselesai' => $waktu[1],
		    'summary' => $idata['g_summary'],
		    'summarydesc' => str_replace("'", "", $idata['g_description'])
		];

		$data->formidware = json_encode($jsondata);
		// echo $data->formidware;

		if (isset($_POST['in'])) {
			$ifexists = $db->HasRow('gcv_src','google_cal_verf',array($db->GetCond('gcv_src','=',$idgooglecal)));
			if ($ifexists) {
				$r1 = $db->UpdateData('google_cal_verf',[
					'gcv_hash' => $_POST['in']['summary'],
					'gcv_src' => $idgooglecal,
					'gcv_username' => $_SESSION['admin']['username']
				], array($db->GetCond('gcv_src','=',$idgooglecal)));
			} else {
				$r1 = $db->InsertData('google_cal_verf',[
					'gcv_hash' => $_POST['in']['summary'],
					'gcv_src' => $idgooglecal,
					'gcv_username' => $_SESSION['admin']['username']
				]);		
			}
			
			$r2 = $db->UpdateData('fac_calendar',['cal_status' => intval(62)],array($db->GetCond('id','=',$idata['g_id_cal'])));
			if ($r1) {
				header('location:'.$data->base_url.'administrasi/googlecal/summarylist?tr='.$idata['transaksi']);
			}
		}

		$data->headertext = "Google Cal - Verification";
		$data->judul = 'FAC || Google Calendar - Verification & GetToken';
		$data->username = $_SESSION['admin']['nama'];
		$data->user = $_SESSION['admin']['username'];

		$data->View('googlecal/vformgetid.googlecal',$data);

		/*
		$jumlahverifikasi = $db->FetchCount('gcv_hash','google_cal_verf',array($db->GetCond('gcv_src','=',$idgooglecal)));
		// echo $jumlahverifikasi;
		if ($jumlahverifikasi>=0&&$jumlahverifikasi<2) {
			$r1 = $db->InsertData('google_cal_verf',['gcv_hash' => md5($_SESSION['admin']['username'].$idgooglecal),'gcv_src' => $idgooglecal,'gcv_username' => $_SESSION['admin']['username']]);
			if ($r1) {
				$r2 = $db->UpdateData('fac_calendar',['cal_status' => intval($jumlahverifikasi+61)],array($db->GetCond('id','=',$idata['g_id_cal'])));
			}
			header('location:'.$data->base_url.'administrasi/googlecal/summarylist?tr='.$idata['transaksi']);
		}*/
		

	}


	public static function MidwareAPI($idagenda){
		// header type JSON
		

		$data = new Data();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$data->Model('Querybuilder');
		$data->Model('Googlecalendarmodel');
		$sesi = new Sessionz();
		$db = new QueryBuilder();
		$dbtwo = new Googlecalendarmodel();
		
		$sesi->OnlyAdmin();
		$data->headertext = "Google Cal - MidwareAPI";
		$data->judul = 'FAC || Google Calendar - Midware';
		$data->username = $_SESSION['admin']['nama'];

		$idata = $db->FetchJoinWhereSingleRow(['*'],
			'google_cal_data',
			array(
				$db->getJoin('INNER','acara_harian','acara_harian.cal_id','google_cal_data.g_id_cal'),
				$db->getJoin('INNER','new_transaksi','new_transaksi.trans_id','acara_harian.transaksi'),
				$db->getJoin('INNER','perusahaan','perusahaan.id','acara_harian.id')
			),
			array($db->GetCond('g_id_cal','=',$idagenda))
		);
// 		print_r($idata);
		$r = $db->FetchJoinWhere(['gca_user_email','nama'],
			'google_cal_attendees',
			array($db->getJoin('LEFT','users','google_cal_attendees.gca_user_email','users.email')),
			array($db->GetCond('gca_source','=',$idata['g_row']))
		);
		
		if(count($r)=='0'){
		    echo "<script>alert('Data peserta tidak ada, harap untuk mengisi data peserta terlebih dahulu.');</script>";
		    return;
		}
		
		$dataverif = $db->FetchOneRow(['gcv_hash'],'google_cal_verf',array($db->GetCond('gcv_src','=',$idata['g_row'])));
		$datasummary = $dbtwo->Newagendasummary($idata['trans_id']);
		$petugas = $dbtwo->GetLastTrainer($idata['trans_id']);
		$datastatustrainer = $dbtwo->GetUtamaPendampingPeninjau($idata['g_row']);
		$attendeez = array();
		foreach ($r as $key) {
			// $bongkar = json_decode($key['gca_attendees_json']);
			array_push($attendeez,['displayName' => trim($key['nama']),'email'=>trim($key['gca_user_email']),'responseStatus' => 'needsAction']);
			// echo $bongkar->displayName;
		}
		// print_r($attendeez);
		$agenda = array();
		$tgltrans = array();
		if ($datasummary['agenda']!='') {
			$agenda = explode('#', $datasummary['agenda']);
		}
		$agendas ='<ol>';
		foreach ($agenda as $key) {
			$agendas .= '<li>'.$key.'</li>';			
		}
		
		if($idata['g_agendas']!=''){
		    $agendas .= '<li>'.$idata['g_agendas'].'</li>';
		}
		$agendas .= '</ol>';
		
		//print_r($agenda);
		if ($datasummary['tgl_trans']!='') {
			$tgltrans = explode('#', $datasummary['tgl_trans']);
		}
		$tanggaltraining ='<ul>';
		foreach ($tgltrans as $key) {
			$tanggaltraining .= '<li>'.$key.'</li>';			
		}
		$tanggaltraining .= '</ul>';

		// echo '<br/>';
		$datadescgooglecal = 'Jenis Accurate: '.$datasummary['va_nama'].' <br/>'.
		'Jenis Usaha: '.$datasummary['ket_jenis_usaha'].' <br/>'.
		'Agenda Kegiatan: '.' <br/>'.
		$agendas.
		'CP: '.$datasummary['nama_cust'].' <br/>'.
		'('.$datasummary['telp_cust'].') <br/>'.
		'Email: '.$datasummary['email_cust'].' <br/>'.
		'Cust: '.$datasummary['marketing_nama'].' - '.$datasummary['nama_cabang'].' <br/>'.
		'Marketing: '.$datasummary['m_fac'].' <br/>'.
		'Pengguna: '.$datasummary['jns_pgn'].' <br/>'.
		'Last Trainer: '.$petugas['trainer'].' <br/>'.
		'Tanggal Training: '.' <br/>'.
		$tanggaltraining. 
		'*Notes: '.' <br/>'.
		str_replace('##', '<br/>', $idata['g_description']);
		if ($datastatustrainer['staff_utama']!='') {
			$datadescgooglecal .= '<br/>'.'Utama: '.$datastatustrainer['staff_utama']; 
		}
		if ($datastatustrainer['staff_pendamping']!='') {
			$datadescgooglecal .= '<br/>'.'Pendamping: '.$datastatustrainer['staff_pendamping']; 
		}
		if ($datastatustrainer['staff_peninjau']!='') {
			$datadescgooglecal .= '<br/>'.'Peninjau: '.$datastatustrainer['staff_peninjau']; 
		}
		@$waktu = explode(' - ', $idata['trans_waktu']);
		@$data->waktumulai = $waktu[0];
		@$data->waktuselesai = $waktu[1];

		$arr = [
		    'end' => ['dateTime'=>$idata['tanggal'].'T'.$waktu[1],'timeZone'=>'Asia/Jakarta'],
		    'start' => ['dateTime'=>$idata['tanggal'].'T'.$waktu[0],'timeZone'=>'Asia/Jakarta'],
		    'summary' => $idata['g_summary'], // cek ini letaknya bakal dimana
		    'id' => $idagenda,
		    'idgooglecalz' => $dataverif['gcv_hash'],
		    'attendees' => $attendeez,
		    'colorId' => '2',
		    'location' => str_replace('<br>',' -- ',$idata['trans_lokasi']).', map: https://www.google.co.id/maps/place/'.$idata['map'],
		    'description' => $datadescgooglecal
		];
		$data->passingvariable = json_encode($arr);
		 //echo  $idata['g_summary'];
		$data->View('googlecal/vmidware.googlecal',$data);
	}

	public static function UpdateAdmin($idtanggal){
		$data = new Data();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$data->Model('Querybuilder');
		$sesi = new Sessionz();
		$db = new QueryBuilder();
		
		$sesi->OnlyAdmin();
		$idata = $db->FetchJoinWhereSingleRow(['*'],
			'google_cal_data',
			array(
				$db->getJoin('INNER','fac_calendar','fac_calendar.id','google_cal_data.g_id_cal'),
				$db->getJoin('INNER','new_transaksi','new_transaksi.trans_id','fac_calendar.acara')
			),
			array($db->GetCond('g_id_cal','=',$idtanggal))
		);

		if (isset($_POST['in'])) {
			$updateresult = $db->UpdateData('google_cal_data',
				[
					'g_summary' => $_POST['in']['summary'],
					'g_agendas' => $_POST['in']['agenda'],
					'g_description' => $_POST['in']['desc'],
					'g_status' => '2',
					'g_id_cal' => $_POST['in']['tgl']
				],
				array($db->GetCond('g_row','=',$_POST['in']['row']))
			);

			if ($updateresult) {
				echo "<script>alert('Data berhasil disimpan!');</script>";
				echo "<script>location.replace('".$data->base_url."administrasi/googlecal/newattendee?id=".$_POST['in']['row']."');</script>";	
			}
			
		}
		// end post

		
		// print_r($transaksi);
		
		$data->tanggaltransaksi = $db->FetchWhere(['id','tanggal'],'fac_calendar',array($db->GetCond('acara','=',$idata['trans_id'])));
		$data->listofemails = $db->FetchWhere(['nama','email'],'users',array($db->GetCond('status','=','aktif')));
		$data->attendees = $db->FetchWhere(['gca_attendees_json'],'google_cal_attendees',array($db->GetCond('gca_source','=',$idata['g_row'])));
		// print_r($data->attendees);

		@$waktu = explode(' - ', $idata['trans_waktu']);
		@$data->waktumulai = $waktu[0];
		@$data->waktuselesai = $waktu[1];
		$data->transid = $idata['trans_id'];
		$data->currentdate = $idata['id'];
		$data->row = $idata['g_row'];
		$data->summary = $idata['g_summary'];
		$data->description = $idata['g_description'];
		$data->agendacal = $idata['g_agendas'];
		$data->idgcal = $idata['g_row'];
		$data->statusgcal = $idata['g_status'];


		$data->idtrans = $idata['trans_id'];
		$data->headertext = "Google Cal - Update Admin";
		$data->judul = 'FAC || Google Calendar - Admin';
		$data->username = $_SESSION['admin']['nama'];

		$data->View('googlecal/vadmupdate.googlecal',$data);
		
	}

	public static function AddNewAgenda($transaksi){
		$data = new Data();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$data->Model('Querybuilder');
		$sesi = new Sessionz();
		$db = new QueryBuilder();
		
		$sesi->AdminMarketing();

		if (isset($_POST['in'])) {
			$agenda = $_POST['in']['agenda'];
			$desc = str_replace('##', '<br/>', $_POST['in']['desc']);
			$r = $db->InsertData(
				'google_cal_data',
				[
					'g_id_cal' => $_POST['in']['tgl'],
					'g_summary' => $_POST['in']['summary'],
					'g_agendas' => $agenda,
					'g_description' => $desc
				]
			);

			if ($r) {
				header('Location:'.$data->base_url.'administrasi/googlecal/summarylist?tr='.$transaksi);
			} else {
				echo "<script>alert('Data gagal disimpan!);</script>";
			}
		}

		$data->tanggaltransaksi = $db->FetchWhere(['id','tanggal'],'fac_calendar',array($db->GetCond('acara','=',$transaksi)));
		$datatrans = $db->FetchJoinWhereSingleRow(['trans_waktu','nama'],'new_transaksi',array($db->getJoin('LEFT','perusahaan','new_transaksi.trans_id_usaha','perusahaan.id')),array($db->GetCond('trans_id','=',$transaksi)));

		// print_r($datat);
		@$waktu = explode(' - ', $datatrans['trans_waktu']);
		@$data->waktumulai = $waktu[0];
		@$data->waktuselesai = $waktu[1];
		@$data->nama_usaha = $datatrans['nama'];
		
		$data->idtrans = $transaksi;
		$data->headertext = "Tabel Agenda Google Calendar";
		$data->judul = 'FAC || Google Calendar - Table';
		$data->username = $_SESSION['admin']['nama'];

		$data->View('googlecal/vnewagenda.googlecal',$data);

	}

	public static function DeleteData($id){
		// header type JSON
		header('Content-Type: application/json');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");

		$data = new Data();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$data->Model('Googlecalendarmodel');
		$sesi = new Sessionz();
		$db = new Googlecalendarmodel();
		
		$sesi->OnlyAdmin();

		$r = $db->DeleteAll($id);
		if ($r) {
			$arr = [
				'status' => ['code'=>'1','description'=>'Data berhasil dihapus!','redirect' => $data->base_url.'administrasi/data-transaksi']
			];
		} else {
			$arr = [
				'status' => ['code'=>'0','description'=>'Data gagal dihapus!']
			];
		}

		echo json_encode($arr);
	}

	public static function UpdateStatusCalendar($id,$status){
		// header type JSON
		header('Content-Type: application/json');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");

		$data = new Data();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$data->Model('Querybuilder');
		$sesi = new Sessionz();
		$db = new QueryBuilder();
		
		$sesi->OnlyAdmin();

		$idata = $db->FetchJoinWhereSingleRow(['*'],
			'google_cal_data',
			array(
				$db->getJoin('INNER','fac_calendar','fac_calendar.id','google_cal_data.g_id_cal'),
				$db->getJoin('INNER','new_transaksi','new_transaksi.trans_id','fac_calendar.acara')
			),
			array($db->GetCond('g_id_cal','=',$id))
		);


		$r = $db->UpdateData('fac_calendar',['cal_status'=>$status],array($db->GetCond('id','=',$id)));
		if ($r) {
			$_SESSION['facgocalsendbackup']='yes';
			$arr = [
				'status' => ['code'=>'1','description'=>'Data berhasil diubah, status: berhasil terkirim ke google calendar'],
				'redirect' => $data->base_url.'administrasi/googlecal/sendbackupinvitation?id='.$id
			];
		} else {
			$arr = [
				'status' => ['code'=>'0','description'=>'Data gagal diubah!']
			];
		}

		echo json_encode($arr);
	}

	public static function ListSummary($transaksi){
		$data = new Data();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$data->Model('Googlecalendarmodel');
		$sesi = new Sessionz();
		$googlecal = new Googlecalendarmodel();
		
		$sesi->AdminMarketing();
		
		$googlecal->setIdtransaksi($transaksi);
		$data->g_cal_list = $googlecal->DisplayTableSummary();
		// print_r($data->g_cal_list);

		$data->idtrans = $transaksi;

		$data->headertext = "Agenda Google Calendar";
		$data->judul = 'FAC || Google Calendar';
		$data->username = $_SESSION['admin']['nama'];
		$data->roleuser = $_SESSION['admin']['tipe']; 
		$data->teamuser = $_SESSION['admin']['team'];
		// echo $_SESSION['admin']['tipe'];

		$data->View('googlecal/vtblagendatrans.googlecal',$data);

	}

}