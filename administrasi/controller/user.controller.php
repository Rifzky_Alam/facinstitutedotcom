<?php include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/classes/View.php';
class User{
    
    public static function UserPicAPI(){
        header('Content-Type: application/json');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		$data = new Data();
        $data->Model('Querybuilder');
		$db = new QueryBuilder();
        $idata = $db->FetchWhere(['picture'],'users',array($db->GetCond('status','=','aktif')));
        
        if (count($idata)=='0') {
			$arr = [
				'status' => ['code'=>'0','description'=>'no data available'],
				'listdata' => $idata
			];
		} else {
			$arr = [
				'status' => ['code'=>'1','description'=>'Data Available'],
				'listdata' => $idata
			];
		}
		echo json_encode($arr);
    }

    public static function DetailUserAPI($username){
        header('Content-Type: application/json');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		$data = new Data();
        $data->Model('Petugas');
		$petugas = new ControlPetugas();
        $idata = $petugas->GetDetailForAPI($username);
        
        if (count($idata)=='0') {
			$arr = [
				'status' => ['code'=>'0','description'=>'no data available'],
				'listdata' => $idata
			];
		} else {
			$arr = [
				'status' => ['code'=>'1','description'=>'Data Available'],
				'listdata' => $idata
			];
		}
		echo json_encode($arr);
    }

	public static function APIBiayaReimbursement($username){
		header('Content-Type: application/json');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		$data = new Data();
        $data->Model('Petugas');
		$db = new ControlPetugas();

		if (!isset($_SESSION['admin'])||empty($_SESSION['admin']['username'])) {
			header('location:'.$data->base_url.'administrasi');
		}

		$idata = $db->GetReimbursementData($_SESSION['admin']['username']);
		// print_r($idata);
		if (count($idata)=='0') {
			$arr = [
				'status' => ['code'=>'0','description'=>'no data available'],
				'listdata' => $idata
			];
		} else {
			$arr = [
				'status' => ['code'=>'1','description'=>'Data Available'],
				'listdata' => $idata
			];
		}
		echo json_encode($arr);
	}

	public static function UploadImage($masukdata,$username){
		$data = new Data();
        $data->Model('Petugas');
        $data->Model('File');
        $file = new File();
		$petugas = new ControlPetugas();
		$petugas->setUsername($username);
		$filetype = pathinfo($masukdata['name'],PATHINFO_EXTENSION);
    
	    $file->setNamafile($masukdata['name']);
	    $petugas->setPicture($masukdata['name']);
	    $file->setFolder('../images/member/');
	    $file->setUkuran($masukdata['size']);
	    $file->setEkstensi($filetype);
	    $file->setTemp($masukdata['tmp_name']);
	    $okay = false;
	    if (!file_exists($file->getFolder().$file->getNamafile())) {
	        if (!unlink($file->getFolder().$file->getNamafile())) {
              echo "<script>alert('Terjadi kesalahan saat menghapus file yang sudah ada, kami menyarankan untuk rename nama file dan coba upload ulang.')</script>";
            } else {
              $okay = true;
            }
	    }

	    if (!file_exists($file->getFolder().$file->getNamafile())) {
	        if ($filetype=='jpg'||$filetype=='png'||$filetype=='jpeg') {
	            $file->uploadFile();
	            $petugas->editPicture();
	            header('location:'.$data->base_url.'administrasi/user/lists');
	        } else {
	            echo "<script>alert('Hanya File JPG, JPEG, PNG yang dapat di upload!')</script>";
	            echo "<script>location.replace('".$data->base_url."administrasi/user/lists')</script>";
	        }        
	    }else{
	        echo "<script>alert('File Sudah Ada, coba untuk rename nama file.')</script>";
	        echo "<script>location.replace('".$data->base_url."administrasi/user/lists')</script>";
	    }
	}

	public static function ListUsers(){
		$data = new Data();
        $data->Model('Petugas');
		$db = new ControlPetugas();
		// echo $username;
		
		$data->listpetugas = $db->GetListUser();
		$data->page = 'upload';
		$data->title = 'Data Pegawai';
		$data->judul = 'Data Pegawai';
		$data->subtitle='Data Pegawai';
		$data->username = $_SESSION['admin']['nama'];
        $data->View('user/vtabelusr.user',$data);
	}

	public static function DetailUser($username){
		$data = new Data();
        $data->Model('Petugas');
        $data->Model('Querybuilder');
		$qb = new QueryBuilder();
		$db = new ControlPetugas();
		// echo $username;
		$idata = $db->GetUserDetail($username);
// 		print_r($idata);

		if (isset($_FILES['cobaz'])) {
			self::UploadImage($_FILES['cobaz'],$username);
		}
		
		if(isset($_POST['in'])){
		  //  print_r($_POST['in']);
		  $mydata = $db->GetBestTrainerCurdate();
		  //print_r($mydata);
		  if(count($mydata)=='0'){
		      $r =$db->InsertBestTrainer($username);
		  }else{
		      $r = $db->EditBestTrainer($username);
		  }
		  
		  if($r){
		    echo "<script>alert('Data Berhasil disimpan');</script>";
		    echo "<script>location.replace('".$data->base_url."administrasi/user/lists');</script>";
		  }else{
		    echo "<script>alert('Data Gagal disimpan');</script>";
		  }
		    
		    
		}
		
		$data->namalengkap = $idata['nama'];
		$data->usrname = $idata['username'];
		$data->tgllahir = $idata['tanggal_lahir'];
		$data->email=$idata['email'];
		$data->telepon = $idata['telepon'];
		$data->team = $idata['team'];
		$data->level = $idata['leveluser'];
		$data->status = $idata['statususer'];
		$data->picture = $idata['picture'];

		$data->page = 'upload';
		$data->title = 'Detail Pegawai / '.$data->namalengkap;
		$data->judul = 'Detail Pegawai / '.$data->namalengkap;
		$data->subtitle='Detail Pegawai / '.$data->namalengkap;
		$data->username = $_SESSION['admin']['nama'];
        $data->View('user/vdetailusr.user',$data);
	}

    // @url "administrasi//user/reimupld?rmv={value}"
    public static function RemoveReimburseImg($id){
        $data = new Data();
        $data->Model('Querybuilder');
		$db = new QueryBuilder();
		$dataimage = $db->FetchOneRow(['*'],'reim_upload',array($db->GetCond('iseqid','=',$id)));
		print_r($dataimage);
		$r = $db->DeleteWhere('reim_upload',array($db->GetCond('iseqid','=',$id)));
		
		if($r){
		    unlink($data->homedir.'administrasi/reimproof/'.$dataimage['sfilename']);
		    header('location:'.$data->base_url.'administrasi/user/reimupld?id='.$dataimage['sfklaphar']);
		}else{
		    header('location:'.$data->base_url.'administrasi/user/reimupld?id='.$dataimage['sfklaphar']);
		}
    }
    // @url "administrasi/user/reimbport"
    public static function PortReimburse($username,$bulan,$tahun,$i='0'){
        $data = new Data();
		$data->Model('Laporan');
		$db = new ControlLaporan();
        $data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$sesi = new Sessionz();
		
		$sesi->IsSuperUser();
		
        $list = $db->DaftarListReimburse($username,$bulan,$tahun);
        
        if(count($list)>0){
            //echo $list[0]['id'].' '.$list[0]['tanggal'];
            if($i!='0'){
            	if (count($list)==$i) {
            		header('location:'.$data->base_url.'administrasi/user/reimlist?src[bln]='.$bulan.'&src[thn]='.$tahun);
            	}else{
            	    if($list[$i]['id']==''){
            	       header('location:'.$data->base_url.'administrasi/user/reimlist?src[bln]='.$bulan.'&src[thn]='.$tahun); 
            	    }else{
            	        header('location:'.$data->base_url.'administrasi/user/reimapv?id='.$list[$i]['id'].'&index='.intval($i));	
            	    }
            		
            	}
                
                // echo 'masuk sini';
            }else{
                header('location:'.$data->base_url.'administrasi/user/reimapv?id='.$list[0]['id'].'&index=0');
            }
                
        }elseif(count($list)=='0'){
            header('location:'.$data->base_url.'administrasi/user/reimlist?src[bln]='.$bulan.'&src[thn]='.$tahun);
        }
    }
    
    // @url "administrasi/user/reimlist" method=GET
    public static function ListReimburse(){
        $data = new Data();
		$data->Model('Laporan');
		$db = new ControlLaporan();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$sesi = new Sessionz();
		
		$sesi->IsSuperUser();
		
		$data->searchbln = '';
		$data->searchthn = '';
// 		$data->qs = '';
		if(isset($_GET['src'])){
		    $data->searchbln = $_GET['src']['bln'];
		    $data->searchthn = $_GET['src']['thn'];
            // $data->qs = $_SERVER['QUERY_STRING'];
            $data->listdata = $db->DaftarYangAdaReimburse($_GET['src']['bln'],$_GET['src']['thn']);
		}else{
		    $data->listdata = $db->DaftarYangAdaReimburse();
		}
		    
        $data->subtitle='List Reimbursement';
        $data->page='penawaran';
        $data->judul = 'List Reimbursement';
		$data->username = $_SESSION['admin']['nama'];
        $data->View('laporan/staff/vtblreim.laporan',$data);
    }    
    // @url (value="/administrasi/user/reimupld?id={value}", method = GET)
    public static function UploadBuktiReim($id){
        $data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		
		$data->Func('FileFunc');
		if(isset($_POST['tipe'])){
    		if (isset($_FILES['ifile'])) {
        		if (CheckExtension($_FILES['ifile'],['jpg','jpeg','png','gif'])) {
        		    $_FILES['ifile']['name']=date("Ymdhis").$_FILES['ifile']['name'];
        		    
        			if (UploadPic($_FILES['ifile'],$data->homedir.'administrasi/reimproof/')) {
        			    $r = $db->InsertData('reim_upload',['sfklaphar'=>$id,'sfilename'=>$_FILES['ifile']['name'],'itype'=>$_POST['tipe']]);
        				echo "<script>alert('File berhasil diupload');</script>";
        				echo "<script>location.replace('".$data->base_url."administrasi/user/reimupld?id=".$id."');</script>";
        			}else{
        				echo "<script>alert('Terjadi kesalahan ketika upload file, harap hubungi admin sistem!');</script>";
        			}
        		}else {
        			echo "<script>alert('Ekstensi file tidak diterima');</script>";
        		}
        	}
		}
    	$data->listdata = $db->FetchWhere(['*'],'reim_upload',array($db->GetCond('sfklaphar','=',$id)));
    	$db->setOrder('dt_flag','ASC');
        $data->listket = $db->FetchWhere(['dt_flag','dt_desc'],'desc_tbl',array($db->GetCond('dt_relate_tbl','=','reim_upload')));
        
        $data->subtitle='Upload Bukti Reimbursement';
        $data->page='upload';
        $data->judul = 'Upload Bukti Reimbursement';
		$data->username = $_SESSION['admin']['nama'];
		$data->View('files/vuploadreim.files',$data);
    }
    
    // @url (value="/administrasi/user/reimapv?id={value}", method = GET)
    public static function PersetujuanReimbursement($id,$idx='0'){
        $data = new Data();
		$data->Model('Laporan');
		$data->Model('Querybuilder');
		$qb = new QueryBuilder();
		$db = new ControlLaporan();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$sesi = new Sessionz();
		
		$sesi->IsSuperUser();
		
		$db->SetLocalFormat();
        $data->rowdata = $db->DetailLaporanReimbursement($id);
        $tgl = explode('-', $data->rowdata['tgl']);
        // echo $data->rowdata['username']. ' ' .$tgl[1].' '.$tgl[2];
// 		echo 'https://fac-institute.com/administrasi/user/reimbport?bln='.$tgl[1].'&thn='.$tgl[0].'&user='.$data->rowdata['username'];
		if(isset($_POST['in'])){
		    // print_r($_POST['in']);
		    if (isset($_POST['in']['setuju'])&&$_POST['in']['setuju']=='1') {
		    	$ru = $qb->UpdateData('laporan_harian',['status_laporan' => '5'],array($qb->GetCond('id','=',$id)));
		    	if ($ru) {
		    		// look for data of user, month and year
		    		header('location:'.$data->base_url.'https://fac-institute.com/administrasi/user/reimbport?bln='.$tgl[1].'&thn='.$tgl[0].'&user='.$data->rowdata['username']);
		    	}else{
		    		echo "<script>alert('Data gagal disimpan!');</script>";
		    	}
		    } else { // if request is rejected, execute codes below.
		    	$r = $qb->InsertData('reim_denial',[
		    		'sfkid' => $id,
		    		'itrans' => $_POST['in']['transport'],
		    		'ipark' => $_POST['in']['parkir'],
		    		'ilunch' => $_POST['in']['lunch'],
		    		'idnr' => $_POST['in']['dinner'],
		    		'imed' => $_POST['in']['medic'],
		    		'iallow' => $_POST['in']['allow'],
		    		'iothr' => $_POST['in']['other'],
		    		'srsn' => $_POST['in']['reason']
		    	]);

		    	$rupdate = $qb->UpdateData('laporan_harian',
		    		[
		    			'biaya_transport' => $_POST['in']['transport'],
			    		'biaya_parkir' => $_POST['in']['parkir'],
			    		'biaya_lunch' => $_POST['in']['lunch'],
			    		'biaya_dinner' => $_POST['in']['dinner'],
			    		'biaya_kesehatan' => $_POST['in']['medic'],
			    		'biaya_allowance' => $_POST['in']['allow'],
			    		'biaya_lain' => $_POST['in']['other'],
			    		'status_laporan' => '5'
		    		],
		    		array($qb->GetCond('id','=',$id)));
		    	if ($r) {
		    		header('location:'.$data->base_url.'administrasi/user/reimbport?bln='.$tgl[1].'&thn='.$tgl[0].'&user='.$data->rowdata['username']);
		    		// echo 'https://fac-institute.com/administrasi/user/reimbport?bln='.$tgl[1].'&thn='.$tgl[0].'&user='.$data->rowdata['username'];
		    	} else {
		    		echo "<script>alert('Data gagal disimpan!');</script>";
		    	}
		    }
		}
		    
		$data->images = $db->BuktiReimburse($id);
		
        $data->idx = $idx;
        
        $data->subtitle='Laporan Pengajuan Reimbursement';
        $data->page='penawaran';
        $data->judul = 'Laporan Harian Approval';
		$data->username = $_SESSION['admin']['nama'];
		$data->View('user/vreimverif.user',$data);
    }
    
    // @url (value="/administrasi/user/reimcomp?id={value}", method = GET)
    public static function KomparasiReimbursement($id){
        $data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
    }
    
    // @url (value="/administrasi/marketing/followup-delete?id={value}", method = GET)
    public static function DeleteFolUpData($id){
        $data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$r = $db->UpdateData('laporan_followup',['status'=>'0'],array($db->GetCond('num','=',$id)));
		if($r){
		    echo "<script>alert('data berhasil dihapus!');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi/marketing/data-followup');</script>";
		}else{
		    echo "<script>alert('data gagal dihapus!');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi/marketing/data-followup');</script>";
		}
    }
    // @url (value="/administrasi/marketing/followup-edit?id={value}", method = POST)
    private static function EditFolUpPost($masukdata){
        $data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		
		$r = $db->UpdateData('laporan_followup',[
		        'tanggal' => $masukdata['tanggal'],
		        'customer' => $masukdata['customer'],
		        'agenda' => $masukdata['agenda'],
		        'status' => $masukdata['status'],
		        'jumlah_hari' => $masukdata['jmlhari'],
		        'deskripsi_hari' => $masukdata['descjmlhari']
		    ],array($db->GetCond('num','=',$masukdata['id'])));
		//print_r($masukdata);
		
		if($r){
		    echo "<script>alert('data berhasil diubah!');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi/marketing/data-followup');</script>";
		}else{
		    echo "<script>alert('data gagal diubah!');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi/marketing/followup-edit?id=".$masukdata['id']."');</script>";
		}
    }
	// @url (value="/administrasi/marketing/followup-edit?id={value}", method = GET)
	public static function EditFolUp($id){
	    $data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		
		
		if(isset($_POST['in'])){
		    self::EditFolUpPost($_POST['in']);
		}
		
		
		$datafolup = $db->FetchOneRow(['*'],'laporan_followup',array($db->GetCond('num','=',$id)));
		$db->setOrder('dt_flag','ASC');
		$data->listdesc = $db->FetchWhere(['dt_flag','dt_desc'],'desc_tbl',
			array(
				$db->GetCond('dt_relate_tbl','=','lapfolup')
			)
		);
		
		$data->rowno = $datafolup['num'];
		$data->tgl = $datafolup['tanggal'];
		$data->cust = $datafolup['customer'];
		$data->agenda = $datafolup['agenda'];
		$data->status = $datafolup['status'];
		$data->jmlhari = $datafolup['jumlah_hari'];
		$data->desc = $datafolup['deskripsi_hari'];
		
		$data->page='penawaran';
		$data->subtitle = 'Edit Laporan Follow-Up';
        $data->judul = 'Edit Laporan Follow-Up';
		$data->username = $_SESSION['admin']['nama'];
		$data->View('laporan/marketing/vreditfolup.marketing',$data);
	}
	// @url (value="/administrasi/marketing/new-followup", method = POST)
	private static function NewFolUpPost($masukdata){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$r = $db->InsertData('laporan_followup',
			[
				'tanggal'=>$masukdata['tanggal'],
				'customer' => $masukdata['customer'],
				'agenda' => $masukdata['agenda'],
				'status' => $masukdata['status'],
				'jumlah_hari' => $masukdata['jmlhari'],
				'deskripsi_hari' => $masukdata['descjmlhari'],
				'petugas' => $_SESSION['admin']['username']
			]
		);

		if ($r) {
			echo "<script>alert('data berhasil disimpan!');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi/marketing/data-followup');</script>";
		} else {
			echo "<script>alert('data gagal disimpan!');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi/marketing/new-followup');</script>";
		}
		// print_r($masukdata);
	}

	// @url (value="/administrasi/marketing/new-followup", method = GET)
	public static function NewFollowupMarketing(){
		$data = new Data();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$sesi = new Sessionz();
		
		$sesi->AdminMarketing();

		if (isset($_POST['in'])) {
			self::NewFolUpPost($_POST['in']);
		}

		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$db->setOrder('dt_flag','ASC');
		$data->listdesc = $db->FetchWhere(['dt_flag','dt_desc'],'desc_tbl',
			array(
				$db->GetCond('dt_relate_tbl','=','lapfolup')
			)
		);
		$data->page='penawaran';
		$data->subtitle = 'Input Laporan Follow-Up';
        $data->judul = 'Input Laporan Follow-Up';
		$data->username = $_SESSION['admin']['nama'];
		$data->View('laporan/marketing/vrnewfolup.marketing',$data);
	}

	// @url (value="/administrasi/marketing/data-followup", method = GET)
	public static function DataFollowUpMarketing($search=''){
		// edit dan delete belum selesai 07032019
		$data = new Data();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$sesi = new Sessionz();
		
		$sesi->AdminMarketing();
		$data->Model('Transaksi');
		$trans = new Transaksi();
		// echo json_encode($trans->LaporanFollowUpMarketing());
		$data->page='penawaran';
		if(isset($_GET['src'])){
		    $data->listdata = $trans->LaporanFollowUpMarketing($_GET['src']);
		}else{
		    $data->listdata = $trans->LaporanFollowUpMarketing();
		}
		
		$data->subtitle = 'Laporan Follow-Up';
        $data->judul = 'FAC || Laporan Follow-Up';
		$data->username = $_SESSION['admin']['nama'];
		$data->View('laporan/marketing/vrfolup.marketing',$data);


	}
	// @url (value="/administrasi/user/notifdasbor", method = POST)
	
	// @url (value="/administrasi/laporan/staff/bulanan/bywilayah", method = GET)
	public static function GenLaporanByWilayah(){
		$data = new Data();
		$data->Model('Laporan');
		$data->Model('Querybuilder');
		$lap = new ControlLaporan();
		$db = new QueryBuilder();

		$daftarperan = $db->FetchWhere(['ind_id','ind_wilayah'],'intensif_dawil',array($db->GetCond('ind_status','<>','0')));
		$data->listdata = array();
		$data->listdatalastmonth = array();
		foreach ($daftarperan as $key) {
			array_push($data->listdata,[$key['ind_wilayah']=>$lap->GeneralLaporanByWilayah($key['ind_id'],'')]);
			array_push($data->listdatalastmonth,[$key['ind_wilayah']=>$lap->GeneralLaporanByWilayah($key['ind_id'],'lastmonth')]);
		}
		
        $data->headertext = 'Laporan Berdasarkan Wilayah';
        $data->judul = 'FAC || Laporan Staff ~ Wilayah';
		$data->username = $_SESSION['admin']['nama'];
		$data->View('laporan/staff/vlapbywilstaff.laporan',$data);
	}

	// @url (value="/administrasi/laporan/staff/bulanan/byjenisacc", method = GET)
	public static function GenLaporanByJenisAccurate(){
		$data = new Data();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$sesi = new Sessionz();
		
		$sesi->OnlyAdmin();
		$data->Model('Laporan');
		$data->Model('Querybuilder');
		$lap = new ControlLaporan();
		$db = new QueryBuilder();

		$daftarperan = $db->FetchColumns(['va_id','va_nama'],'versi_accurate');
		$data->listdata = array();
		$data->listdatalastmonth = array();
		foreach ($daftarperan as $key) {
			array_push($data->listdata,[$key['va_nama']=>$lap->GenLaporanByJnsAccurate($key['va_id'],'')]);
			array_push($data->listdatalastmonth,[$key['va_nama']=>$lap->GenLaporanByJnsAccurate($key['va_id'],'lastmonth')]);
		}
		
        $data->headertext = 'Laporan Berdasarkan Jenis Accurate';
        $data->judul = 'FAC || Laporan Staff ~ Jenis Accurate';
		$data->username = $_SESSION['admin']['nama'];
		$data->View('laporan/staff/vlapbyjenisaccstaff.laporan',$data);
	}

	// @url (value="/administrasi/laporan/staff/bulanan/byjenisusaha", method = GET)
	public static function GenLaporanByJenisUsaha(){
		$data = new Data();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$sesi = new Sessionz();
		
		$sesi->OnlyAdmin();
		$data->Model('Laporan');
		$data->Model('Querybuilder');
		$lap = new ControlLaporan();
		$db = new QueryBuilder();

		$daftarperan = $db->FetchColumns(['id','nama_jenis_usaha'],'daftar_jenis_usaha');
		$data->listdata = array();
		$data->listdatalastmonth = array();
		foreach ($daftarperan as $key) {
			array_push($data->listdata,[$key['nama_jenis_usaha']=>$lap->GeneralReportByJnsUsaha($key['id'],'')]);
			array_push($data->listdatalastmonth,[$key['nama_jenis_usaha']=>$lap->GeneralReportByJnsUsaha($key['id'],'lastmonth')]);
		}
		
        $data->headertext = 'Laporan Berdasarkan Jenis Usaha';
        $data->judul = 'FAC || Laporan Staff ~ Jenis Usaha';
		$data->username = $_SESSION['admin']['nama'];
		$data->View('laporan/staff/vlapbyjenisusahastaff.laporan',$data);
	}

	// @url (value="/administrasi/laporan/staff/bulanan/bytransport", method = GET)
	public static function GenLaporanByTrans(){
	    $data = new Data();
	    $data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$sesi = new Sessionz();
		
		$sesi->OnlyAdmin();
		$data->Model('Laporan');
		$data->Model('Querybuilder');
		$lap = new ControlLaporan();
		$db = new QueryBuilder();

		$daftarperan = $db->FetchColumns(['lt_id','lt_ket'],'daftar_alat_trans');
		$data->listdata = array();
		$data->listdatalastmonth = array();
		foreach ($daftarperan as $key) {
			array_push($data->listdata,[$key['lt_ket']=>$lap->GeneralReportByTrans($key['lt_id'],'')]);
			array_push($data->listdatalastmonth,[$key['lt_ket']=>$lap->GeneralReportByTrans($key['lt_id'],'lastmonth')]);
		}
		
        $data->headertext = 'Laporan Berdasarkan Jenis Transportasi';
        $data->judul = 'FAC || Laporan Staff ~ Jenis Trans';
		$data->username = $_SESSION['admin']['nama'];
		$data->View('laporan/staff/vlapbytransstaff.laporan',$data);
	}
	
	// @url (value="/administrasi/laporan/staff/bulanan/bystatuskerja", method = GET)
	public static function GenLaporanByStatus(){
	    $data = new Data();
	    $data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$sesi = new Sessionz();
		
		$sesi->OnlyAdmin();
		$data->Model('Laporan');
		$data->Model('Querybuilder');
		$lap = new ControlLaporan();
		$db = new QueryBuilder();

		$daftarperan = $db->FetchColumns(['dsk_id','dsk_desc'],'daftar_status_kerja');
		$data->listdata = array();
		$data->listdatalastmonth = array();
		foreach ($daftarperan as $key) {
			array_push($data->listdata,[$key['dsk_desc']=>$lap->GeneralReportByStatusKerja($key['dsk_id'],'')]);
			array_push($data->listdatalastmonth,[$key['dsk_desc']=>$lap->GeneralReportByStatusKerja($key['dsk_id'],'lastmonth')]);
		}
		
        $data->headertext = 'Laporan Berdasarkan Status Kerja';
        $data->judul = 'FAC || Laporan Staff ~ Status Kerja';
		$data->username = $_SESSION['admin']['nama'];
		$data->View('laporan/staff/vlapbystatuskerjastaff.laporan',$data);
	}
	
	
	// @url (value="/administrasi/laporan/staff/bulanan/byperan", method = GET)
	public static function LaporanByPeran(){
		$data = new Data();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$sesi = new Sessionz();
		
		$sesi->OnlyAdmin();
		$data->Model('Laporan');
		$data->Model('Querybuilder');
		$lap = new ControlLaporan();
		$db = new QueryBuilder();

		$daftarperan = $db->FetchColumns(['pk_id','pk_peran'],'daftar_peran_kerja');
		$data->listdata = array();
		$data->listdatalastmonth = array();
		foreach ($daftarperan as $key) {
			array_push($data->listdata,[$key['pk_peran']=>$lap->GeneralReportByPeran($key['pk_id'],'')]);
			array_push($data->listdatalastmonth,[$key['pk_peran']=>$lap->GeneralReportByPeran($key['pk_id'],'lastmonth')]);
		}
		
        $data->headertext = 'Laporan Berdasarkan Peran';
        $data->judul = 'FAC || Laporan Staff ~ Peran Kerja';
		$data->username = $_SESSION['admin']['nama'];
		$data->View('laporan/staff/vlapbyperanstaff.laporan',$data);
	}

	// @url (value="/administrasi/laporan/staff/bulanan", method = GET)
    public static function MenuGeneralReport(){
        $data = new Data();
        $data->headertext = 'Menu Laporan Trainer';
        $data->judul = 'FAC || Laporan Staff ~ Jenis Accurate';
		$data->username = $_SESSION['admin']['nama'];
		$data->View('laporan/staff/vlaproutebulanan.laporan',$data);
    }    
    
	public static function DiagramAlatTrans($idtransport){
		$data = new Data();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$sesi = new Sessionz();
		
		$sesi->OnlyAdmin();
		$data->Model('Petugas');
		$data->Model('Querybuilder');
		$data->JustInclude('administrasi/functions/ChartFunc');
		$petugas = new ControlPetugas();
		$db = new QueryBuilder();
		$data->listdata = $petugas->LaporanAlatTransCurrentMonth($idtransport);
		$data->jenis = $db->FetchOneRow(['lt_ket'],'daftar_alat_trans',array($db->GetCond('lt_id','=',$idtransport)));
		// print_r($data->jenis);
		$data->subtitle = "Data Laporan Berdasarkan Alat Transportasi ".ucfirst($data->jenis['lt_ket']);
		$data->judul = 'FAC || Laporan Staff ~ Jenis Accurate';
		$data->username = $_SESSION['admin']['nama'];
		$data->View('laporan/staff/vpiediagramstaff.laporan',$data);
	}

	// @url (value="/administrasi/laporan/staff/chart/laporanstatuskerja?id={value}", method = GET)
	public static function DiagramStatusKerja($idstat){
		$data = new Data();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$sesi = new Sessionz();
		
		$sesi->OnlyAdmin();
		$data->Model('Petugas');
		$data->Model('Querybuilder');
		$data->JustInclude('administrasi/functions/ChartFunc');
		$petugas = new ControlPetugas();
		$db = new QueryBuilder();
		$data->listdata = $petugas->LaporanStatusKerjaCurrentMonth($idstat);
		$data->jenis = $db->FetchOneRow(['dsk_desc'],'daftar_status_kerja',array($db->GetCond('dsk_id','=',$idstat)));
		// print_r($data->jenis);
		$data->subtitle = "Data Laporan Berdasarkan Status Kerja ".ucfirst($data->jenis['dsk_desc']);
		$data->judul = 'FAC || Laporan Staff ~ Jenis Accurate';
		$data->username = $_SESSION['admin']['nama'];
		$data->View('laporan/staff/vpiediagramstaff.laporan',$data);
	}

	// @url (value="/administrasi/laporan/staff/chart/jenisaccpercentage?id={value}", method = GET)
	public static function DiagramPeran($idperan){
		$data = new Data();
		$data->Model('Petugas');
		$data->Model('Querybuilder');
		$data->JustInclude('administrasi/functions/ChartFunc');
		$petugas = new ControlPetugas();
		$db = new QueryBuilder();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$sesi = new Sessionz();
		
		$sesi->OnlyAdmin();
		$data->listdata = $petugas->LaporanPeranbyCurrentMonth($idperan);
		$data->jenis = $db->FetchOneRow(['pk_peran'],'daftar_peran_kerja',array($db->GetCond('pk_id','=',$idperan)));
		$data->subtitle = "Data Laporan Berdasarkan Peran ".$data->jenis['pk_peran'];
		$data->judul = 'FAC || Laporan Staff ~ Jenis Accurate';
		$data->username = $_SESSION['admin']['nama'];
		$data->View('laporan/staff/vpiediagramstaff.laporan',$data);
	}

	// @url (value="/administrasi/laporan/staff/chart/jenisaccpercentage?id={value}", method = GET)
	public static function DiagramJenisAccurate($id){
		$data = new Data();
		$data->Model('Perusahaan');
		$data->Model('Querybuilder');
		$data->JustInclude('administrasi/functions/ChartFunc');
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$sesi = new Sessionz();
		
		$sesi->OnlyAdmin();
		$usaha = new Perusahaan();
		$db = new QueryBuilder();
		$data->listdata = $usaha->GetDiagramLaporanByJenisAcc($id);
		$data->laman='jenisacc';
		$data->idacc = $id;
		$data->jenis = $db->FetchOneRow(['va_nama'],'versi_accurate',array($db->GetCond('va_id','=',$id)));
		$data->subtitle = "Data Laporan Berdasarkan Jenis Accurate ".$data->jenis['va_nama'];
		$data->judul = 'FAC || Laporan Staff ~ Jenis Accurate';
		$data->username = $_SESSION['admin']['nama'];
		$data->View('laporan/staff/vpiediagramstaff.laporan',$data);
	}

	// @url (value="/administrasi/user/notifdasbor", method = GET)
	public static function sendnotiftouserdashboard(){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$sesi = new Sessionz();
		
		$sesi->OnlyAdmin();
		$data->listusers = $db->FetchWhere(['username','nama'],'users',array($db->GetCond('status','=','aktif')));
		// $data->page = 'laporan';
		$data->headertext = "Send Notification";
		$data->judul = 'FAC || Notif Dasbor';
		$data->username = $_SESSION['admin']['nama'];
		$data->datepicker = '1';
		$data->page='penawaran';
		$data->View('user/vnotiftrainer.user',$data);
	}

	public static function tesfungsi(){
		$data = new Data();
		$data->JustInclude('administrasi/functions/insentiffunc');
		$insacc = '100000';
		$peran = '0.75';
		$jenistrans ='1';

		echo number_format(KalkulasiInsentifAcc($insacc,$peran,$jenistrans));
		echo '<br>';
		$inshari = '50000';
		echo number_format(KalkulasiInsentifHaker($inshari,$jenistrans));
		echo '<br>';
		$jumlahpeserta = '3';
		$insluarkota = '180000';
		echo number_format(KalkulasiInsentifLuarKota($insluarkota,$jenistrans,$jumlahpeserta));
		echo '<br>';
		$total = number_format(KalkulasiInsentifAcc($insacc,$peran,$jenistrans) + KalkulasiInsentifHaker($inshari,$jenistrans) + KalkulasiInsentifLuarKota($insluarkota,$jenistrans,$jumlahpeserta));
		echo $total;
	}

	// @url (value="/administrasi/user/laporan", method = GET)
	public static function DataLaporanHarian(){
		$data = new Data();
		$data->Model('Laporan');
		$data->Model('Querybuilder');
		$data->JustInclude('administrasi/functions/insentiffunc');
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$sesi = new Sessionz();
		$db = new ControlLaporan();
		$dao = new QueryBuilder();
		
		$sesi->OnlyAdmin();
		$db->SetLocalFormat();
		if (isset($_GET['src'])) {
			$data->listdata = $db->NewDataHarian($_GET['src']['username'],$_GET['src']['tglawal'],$_GET['src']['tglakhir']);
			$data->enabledownload = true;
			$data->searchuser = $_GET['src']['username'];
			$data->searchfromdate =$_GET['src']['tglawal'];
			$data->searchuntildate =$_GET['src']['tglakhir'];
		}else{
			$data->listdata = $db->NewDataHarian();
			$data->enabledownload = false;
			$data->searchuser = '';
			$data->searchfromdate ='';
			$data->searchuntildate ='';
		}
		$dao->setOrder('nama','ASC');
		$data->users = $dao->FetchColumns(['username','nama'],'users');
		// print_r($data->listdata);
		$data->page = 'laporan';
		$data->headertext = "Data Laporan Harian";
		$data->judul = 'FAC || Data Laporan Harian';
		$data->username = $_SESSION['admin']['nama'];
		$data->datepicker = '1';
		$data->page='penawaran';
		$data->View('laporan/admin/vlaporanstaff.lap',$data);
	}

	// @url (value="/administrasi/user/laporan-harian?det={value}", method = POST)
	public static function UpdateInsentif($idlaporan){
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		$data = new Data();
		$data->Model('Querybuilder');
		$data->Model('Insentif');
		$db = new QueryBuilder();
		$insentif = new Insentif();
		$mydata = $insentif->GetMySummary($idlaporan);
		
		$role = $insentif->TentukanPeranGoogleCalendar($idlaporan);
		$idiputama = $db->FetchOneRow(['ip_id'],'intensif_peran',
			array(
				$db->GetCond('ip_status','=','1'),
				$db->GetCond('ip_peran','=','1')
			));
		$jmlrecord = $insentif->CariJumlahTransaksi($mydata->transaksi,$mydata->tanggal);
		if ($jmlrecord=='0') {
			$jmlrecord = '1';
		}
// 		print_r($mydata);
// 		echo $mydata->transaksi. "<br";
// 		echo $mydata->summary. "<br";

		if (@$mydata->transaksi!=''&&$mydata->summary!='-') {
			// update insentif jenis accurate, hari kerja
// 			echo "masuk ke @$mydata->transaksi!=''&&$mydata->summary!='-'";
            // echo $mydata->usr_level." ".$mydata->trans_va." ".$mydata->tanggal;
			$insacc = $insentif->FetchForJenisAccurate($mydata->usr_level,$mydata->trans_va,$mydata->tanggal);
// 			print_r($insacc);
		    $inshaker = $insentif->FetchForJenisHariKerja($mydata->jns_hari_kerja,$mydata->tanggal);
		    $mydata->ija_id = $insacc['ija_id'];
		    $mydata->ihk_id = $inshaker['ihk_id'];
			
			//if utama has no 'pendamping' then update peran utama by 99 (100%) insentif by peran 
			$jml = $insentif->HitungPeranBerdasarkanTrans('2',$mydata->transaksi,$mydata->tanggal);
			@$idlaputama = $insentif->GetIdLaporanByPeran('1',$mydata->transaksi,$mydata->tanggal); // buat update yang utama
			
			if ($mydata->status_kerja=='1') {
				$insentifperan = '99';
			}elseif ($role=='0'&&$mydata->ip_id=='1') {
				$insentifperan = '99';
			}else{
				$insentifperan = $mydata->ip_id;
			}

			$r = $db->UpdateData('intensif_summary',
				[
					'is_id_trans' => $mydata->transaksi,
					'is_lvl_trainer' => $mydata->usr_level,
					'is_harikerja' => $mydata->ihk_id,
					'is_jenisacc' => $mydata->ija_id,
					'is_peran' => $insentifperan,
					'is_dawil' => $mydata->itw_wil_kode,
					'is_tr_allow' => $mydata->ijt_acc,
					'is_sml_allow' => $mydata->ijt_hari,
					'is_wil_allow' => $mydata->ijt_wilayah
				],
					array($db->GetCond('is_id_laporan','=',$idlaporan))
			);
			$insentif->UpdateJumlahAttendance($mydata->transaksi,$mydata->tanggal,$jmlrecord);
			//updating peran utama (another user whose peran is 1 [utama]) by 99
			if ($jml=='0'&&@$idlaputama->id!='') {
				$db->UpdateData('intensif_summary',['is_peran' => '99'],array($db->GetCond('is_id_laporan','=',@$idlaputama->id)));
			}elseif ($jml>0&&@$idlaputama->id!='') {
				$db->UpdateData('intensif_summary',['is_peran' => $idiputama['ip_id']],
					array($db->GetCond('is_id_laporan','=',@$idlaputama->id))
				);
			}
		}elseif ($mydata->transaksi==''&&$mydata->summary!='-') {
// 			echo 'masuk ke tidak ada trans dan tidak ada summary!';
			$r = $db->UpdateData('intensif_summary',
				[
					'is_id_trans' => '',
					'is_lvl_trainer' => $mydata->usr_level,
					'is_harikerja' => '0',
					'is_jenisacc' => '0',
					'is_peran' => '0',
					'is_dawil' => '0'
				],
					array($db->GetCond('is_id_laporan','=',$idlaporan))
			);	
		} else {
			// update insentif jenis accurate, hari kerja
// 			echo "masuk insert ngga?";
			$insacc = $insentif->FetchForJenisAccurate($mydata->usr_level,$mydata->trans_va,$mydata->tanggal);
// 			print_r($insacc);
		    $inshaker = $insentif->FetchForJenisHariKerja($mydata->jns_hari_kerja,$mydata->tanggal);
		    print_r($inshaker);
		    $mydata->ija_id = $insacc['ija_id'];
		    $mydata->ihk_id = $inshaker['ihk_id'];
			
			// tambahkan dari google calendar disini.
            
			// end google calendar
			$jml = $insentif->HitungPeranBerdasarkanTrans('2',$mydata->transaksi,$mydata->tanggal);
			@$idlaputama = $insentif->GetIdLaporanByPeran('1',$mydata->transaksi,$mydata->tanggal);

			if ($mydata->status_kerja=='1') {
				$insentifperan = '99';
			}else{
				$insentifperan = $mydata->ip_id;
			}
			// echo $idlaputama->id. ' ' . $jml;
// 			echo "idlap: ".$idlaporan.", trans: ".$mydata->transaksi.", usrlvl: ".$mydata->usr_level.", ihk_id: ".$mydata->ihk_id.", ijaid: ".
// 			$mydata->ija_id.", insperan: ".$insentifperan.", insdawil: ".$mydata->itw_wil_kode.", trallow: ".$mydata->ijt_acc.", smlall: ".
// 			$mydata->ijt_hari.", wilall:".$mydata->ijt_wilayah;
			$r = $db->InsertData('intensif_summary',
						[
							'is_id_laporan' => $idlaporan,
							'is_id_trans' => $mydata->transaksi,
							'is_lvl_trainer' => $mydata->usr_level,
							'is_harikerja' => $mydata->ihk_id,
							'is_jenisacc' => $mydata->ija_id,
							'is_peran' => $insentifperan,
							'is_dawil' => $mydata->itw_wil_kode,
							'is_tr_allow' => $mydata->ijt_acc,
							'is_sml_allow' => $mydata->ijt_hari,
							'is_wil_allow' => $mydata->ijt_wilayah
						]
					);
			$insentif->UpdateJumlahAttendance($mydata->transaksi,$mydata->tanggal,$jmlrecord);
			//updating peran utama by 99
			if ($jml=='0'&&@$idlaputama->id!='') {
				$db->UpdateData('intensif_summary',['is_peran' => '99'],array($db->GetCond('is_id_laporan','=',@$idlaputama->id)));
			}elseif ($jml>0&&@$idlaputama->id!='') {
				$db->UpdateData('intensif_summary',['is_peran' => $idiputama['ip_id']],
					array($db->GetCond('is_id_laporan','=',@$idlaputama->id))
				);
			}
		}
		
		if ($r) {
			echo "<script>alert('Kalkulasi insentif selesai.');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi/user/laporan-harian');</script>";
		}else {
			echo "<script>alert('Kalkulasi insentif gagal.');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi/user/laporan-harian');</script>";
		}
	}

	// @url (value="/administrasi/user/laporan-harian?insdet={value}", method = GET)
	public static function Detailinsentifadmin($idlaporan){
		$data = new Data();
		$data->JustInclude('administrasi/functions/insentiffunc');
		$data->Model('Querybuilder');
		$data->Model('Insentif');
		$db = new QueryBuilder();
		$insentif = new Insentif();
		$insentif->setIdlaporan($idlaporan);

		$summary = $insentif->CariInsentif();
		$detail = $insentif->GetMySummary($idlaporan);

		if ($summary->status_kerja=='') {
			echo "<script>alert('Harap laporkan dahulu kegiatan pada tanggal ini.');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi/user/laporan-harian');</script>";
		}
		
		if ($summary->status_kerja=='1') {
			$persentaseluarkota = '1';
		}else{
			$persentaseluarkota = $summary->is_ttl_attnd;
		}

		$data->userlevel = $detail->usr_level;
		$formulaacc = "Tarif Accurate x Peran x Jenis Transaksi (ya/tdk) <br> ".number_format($summary->tarif_accurate)." x ".$summary->tarif_peran * 100 ."% x $summary->ijt_acc <br> ";
		$data->insentifacc = $formulaacc . number_format(KalkulasiInsentifAcc($summary->tarif_accurate,$summary->tarif_peran,$summary->ijt_acc));
		$formulahari = "Insentif SML x Jenis Transaksi (ya/tdk) <br> ".number_format($summary->tarif_hari_kerja)." x $summary->ijt_hari <br> ";
		$data->insentifharikerja = $formulahari.number_format(KalkulasiInsentifHaker($summary->tarif_hari_kerja,$summary->ijt_hari));
		$formulawilayah = "Insentif Luar Kota x Jenis Transaksi (ya/tdk) : jumlahpeserta <br> ".number_format($summary->tarif_wilayah)." x $summary->ijt_hari : $persentaseluarkota <br> ";
		$data->insentifwilayah = $formulawilayah.number_format(KalkulasiInsentifLuarKota($summary->tarif_wilayah,$summary->ijt_wilayah,$persentaseluarkota));
		$data->ringkasanlaporan = $summary->idsummary;
		$data->transs = $summary->transaksi;
		$data->transacara = $detail->transaksi;
		$data->accurate = $detail->trans_va;
		$data->prov = $detail->provinsi;
		$data->page = 'laporan';
		$data->headertext = "Summary Insentif ".$detail->tanggal;
		$data->judul = 'FAC || Laporan Harian';
		$data->username = $_SESSION['admin']['nama'];
		$data->View('laporan/staff/vdetailins.laporan',$data);
	}

	// @url (value="/administrasi/user/laporan-harian?detin={value}", method = GET)
	public static function Detailinsentif($idlaporan,$username){
		$data = new Data();
		$data->JustInclude('administrasi/functions/insentiffunc');
		$data->Model('Querybuilder');
		$data->Model('Insentif');
		$db = new QueryBuilder();
		$insentif = new Insentif();
		$insentif->setIdlaporan($idlaporan);

		$summary = $insentif->CariInsentif();
		$detail = $insentif->GetMySummary($idlaporan);
		if ($summary->username!=$username) {
			echo "<script>location.replace('".$data->base_url."administrasi/accessdenied');</script>";
		}

		if ($summary->status_kerja=='') {
			echo "<script>alert('Harap laporkan dahulu kegiatan anda pada tanggal ini.');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi/user/laporan-harian');</script>";
		}
		
		if ($summary->status_kerja=='1') {
			$persentaseluarkota = '1';
		}else{
			$persentaseluarkota = $summary->is_ttl_attnd;
		}

		
		$data->userlevel = $detail->usr_level;
		$data->insentifacc = number_format(KalkulasiInsentifAcc($summary->tarif_accurate,$summary->tarif_peran,$summary->ijt_acc));
		$data->insentifharikerja = number_format(KalkulasiInsentifHaker($summary->tarif_hari_kerja,$summary->ijt_hari));
		$data->insentifwilayah = number_format(KalkulasiInsentifLuarKota($summary->tarif_wilayah,$summary->ijt_wilayah,$persentaseluarkota));
		$data->ringkasanlaporan = $summary->idsummary;
		$data->transs = $summary->transaksi;
		$data->transacara = $detail->transaksi;
		$data->accurate = $detail->trans_va;
		$data->prov = $detail->provinsi;
		
		$data->page = 'laporan';
		$data->headertext = "Summary Insentif";
		$data->judul = 'FAC || Laporan Harian';
		$data->username = $_SESSION['admin']['nama'];
		$data->View('laporan/staff/vdetailins.laporan',$data);
	}

	public static function DataLaporanBelumLengkap($username){
		$data = new Data();
		$data->Model('Petugas');
		$petugas = new ControlPetugas();
		
		$petugas->setUsername($username);
		
		$data->judul = 'Administrasi/FAC Institute';
		$data->page='home';
		$data->lists = $petugas->DataBelumDilaporkan();
		$data->petugas = $_SESSION['admin']['username'];
		$data->judul = 'FAC || Laporan Harian';
		$data->username = $_SESSION['admin']['nama'];

		$data->View('laporan/view-data-blmlapor',$data);
	}

	public static function JsonDataAbsenBelomLengkap($username){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$query = "SELECT * 
		FROM `absensi` 
		WHERE `username`='".$username."' 
		AND `absensi`.`absen_pulang` IS NULL 
		OR `username`='".$username."' 
		AND `absensi`.`absen_pulang`='';";
		$r = $db->ExecuteSQLQuery($query);
		// print_r($r);
		$jumlah = count($r);
		header('Content-Type: application/json');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");

		$arr = [
			    'status' => ['code'=>'1','description'=>'Page is ok!'],
			    'data' => ['databelumabsen' => $jumlah] 
			];
		echo json_encode($arr);
	}

	// @url (value="/administrasi/user/laporan-harian?summary={value}", method = POST)
	public static function CreatingSummary($idlaporan){
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		$data = new Data();
		$data->Model('Querybuilder');
		$data->Model('Insentif');
		$db = new QueryBuilder();
		$insentif = new Insentif();
		$data->JustInclude('administrasi/functions/StringFunc');
		$insentif->setIdlaporan($idlaporan);
		$role = $insentif->TentukanPeranGoogleCalendar($idlaporan);
		$datalaporan = $insentif->FetchForSummary();
		$countdata = $db->FetchCount('is_id_laporan','intensif_summary',array($db->GetCond('is_id_laporan','=',$idlaporan)));
		$kegiatanterlarang = false;
		// ambil user_level, jenis_hari, peran, jenis accurate, data wilayah untuk langsung dimasukkan ke dalam insentif summary
		//querysample #007

		$jmlrecord = $insentif->CariJumlahTransaksi($datalaporan[0]['transaksi'],$datalaporan[0]['tanggal']);
// 		if ($jmlrecord=='0') {
// 			$jmlrecord = '1';
// 		}

		//ambil data jenis kegiatan
		$inskegiatan = $db->FetchWhere(['dkl_id'],'daftar_keg_laporan',
			array(
				$db->GetCond('dkl_insentif','=','0')
			)
		);
		//cek apakah kegiatan boleh atau ngga
		$exkegiatan = explode(' # ', $datalaporan[0]['kegiatan']);
		foreach ($exkegiatan as $key) {
			foreach ($inskegiatan as $value) {
				if ($value['dkl_id']==$key) {
					$kegiatanterlarang = true;
				}
			}
		}

		// ambil data jenistrans dan jenis accurate untuk mencari insentif di tabel konfigurasi
		if ($datalaporan[0]['transaksi']!='') { // only when data transaksi available
			$insjenistrans = $db->FetchOneRow(['ijt_jns_trans','ijt_acc','ijt_hari','ijt_wilayah'],
				'intensif_jns_trans',
				array(
					$db->GetCond('ijt_status','=','1'),
					$db->GetCond('ijt_jns_trans','=',$datalaporan[0]['trans_jenis'])
				)
			);
			// ambil data konfig insentif jenis accurate
			$insjenacc = $insentif->FetchForJenisAccurate($datalaporan[0]['usr_level'],$datalaporan[0]['trans_va'],$datalaporan[0]['tanggal']);
            // print_r($insjenacc);
			if ($insjenacc['ija_id']=='') {
				$insjenacc['ija_id']='0';
			}

			// ambil data konfig peran
			if ($datalaporan[0]['status_kerja']=='1') {
				$insperan = '99';
			}elseif ($role=='0'&&$datalaporan[0]['peran']=='1') {
				$insperan = '99';
			}else{
				$insperan = $db->FetchOneRow(['ip_id'],'intensif_peran',
					array(
						$db->GetCond('ip_peran','=',$datalaporan[0]['peran']),
						$db->GetCond('ip_status','=','1')
					)
				);
				$insperan=$insperan['ip_id'];
			}
				
			

			

			// ambil data konfig jenisharikerja
			$insharker = $insentif->FetchForJenisHariKerja($datalaporan[0]['jns_hari_kerja'],$datalaporan[0]['tanggal']);
			// ambil data konfig dawil 
			$insdawil = $insentif->GetInsentifLuarKota($datalaporan[0]['provinsi'],
				$datalaporan[0]['kota'],$datalaporan[0]['kec']);

			if ($countdata>0) { // update last saved
				if ($kegiatanterlarang) { // kalo kegiatan tidak dibolehkan
					$r = $db->UpdateData('intensif_summary',
						[
							'is_id_laporan' => $idlaporan,
							'is_id_trans' => '',
							'is_lvl_trainer' => $datalaporan[0]['usr_level'],
							'is_harikerja' => '0',
							'is_jenisacc' => '0',
							'is_peran' => '0',
							'is_dawil' => '0'
						],
						array($db->GetCond('is_id_laporan','=',$idlaporan))
					);
				} else {
					$r = $db->UpdateData('intensif_summary',
						[
							'is_id_laporan' => $idlaporan,
							'is_id_trans' => $datalaporan[0]['transaksi'],
							'is_lvl_trainer' => $datalaporan[0]['usr_level'],
							'is_harikerja' => $insharker['ihk_id'],
							'is_jenisacc' => $insjenacc['ija_id'],
							'is_peran' => $insperan,
							'is_dawil' => GetIntVal($insdawil->itw_wil_kode),
							'is_tr_allow' => $insjenistrans['ijt_acc'],
							'is_sml_allow' => $insjenistrans['ijt_hari'],
							'is_wil_allow' => $insjenistrans['ijt_wilayah']
						],
						array($db->GetCond('is_id_laporan','=',$idlaporan))
					);
					$insentif->UpdateJumlahAttendance($datalaporan[0]['transaksi'],$datalaporan[0]['tanggal'],$jmlrecord);
				}
			} else { // insert new
				if ($kegiatanterlarang) { // kalo kegiatan tidak dibolehkan
					$r = $db->InsertData('intensif_summary',
						[
							'is_id_laporan' => $idlaporan,
							'is_id_trans' => '',
							'is_lvl_trainer' => $datalaporan[0]['usr_level'],
							'is_harikerja' => '0',
							'is_jenisacc' => '0',
							'is_peran' => '0',
							'is_dawil' => '0'
						]
					);
				} else {
				    $jmlrecord += 1;
					//Ade Error disini
					// echo "id laporan: ".$idlaporan."<br>";
					// echo "transaksi: ".$datalaporan[0]['transaksi']."<br>";
					// echo "user level: ".$datalaporan[0]['usr_level']."<br>";
					// echo "hari_kerja_id: ".$insharker['ihk_id']."<br>";
					// echo "insjenisacc: ".$insjenacc['ija_id']."<br>";
					// echo "insperan: ".$insperan."<br>";
					// echo "inskodewil: ".GetIntVal($insdawil->itw_wil_kode) ."<br>";
					$r = $db->InsertData('intensif_summary',
						[
							'is_id_laporan' => $idlaporan,
							'is_id_trans' => $datalaporan[0]['transaksi'],
							'is_lvl_trainer' => $datalaporan[0]['usr_level'],
							'is_harikerja' => $insharker['ihk_id'],
							'is_jenisacc' => $insjenacc['ija_id'],
							'is_peran' => $insperan,
							'is_dawil' => GetIntVal($insdawil->itw_wil_kode),
							'is_tr_allow' => $insjenistrans['ijt_acc'],
							'is_sml_allow' => $insjenistrans['ijt_hari'],
							'is_wil_allow' => $insjenistrans['ijt_wilayah']
						]
					);
					$insentif->UpdateJumlahAttendance($datalaporan[0]['transaksi'],$datalaporan[0]['tanggal'],$jmlrecord);
				}	
			}
				
			if ($r) {
				echo "<script>alert('Kalkulasi insentif selesai.');</script>";
				echo "<script>window.close();</script>";
			} else {
				echo "<script>alert('Terjadi kesalahan dalam perhitungan insentif.');</script>";
				echo "<script>window.close();</script>";
			}	
		}else { // -> transaksi.equal("")
			if ($countdata>0) {
				$r = $db->UpdateData('intensif_summary',
						[
							'is_id_laporan' => $idlaporan,
							'is_id_trans' => '',
							'is_lvl_trainer' => $datalaporan[0]['usr_level'],
							'is_harikerja' => '0',
							'is_jenisacc' => '0',
							'is_peran' => '0',
							'is_dawil' => '0'
						],
						array($db->GetCond('is_id_laporan','=',$idlaporan))
					);
			} else {
				$r = $db->InsertData('intensif_summary',
					[
						'is_id_laporan' => $idlaporan,
						'is_id_trans' => '',
						'is_lvl_trainer' => $datalaporan[0]['usr_level'],
						'is_harikerja' => '0',
						'is_jenisacc' => '0',
						'is_peran' => '0',
						'is_dawil' => '0'
					]
				);
			}
			echo "<script>window.close();</script>";
		
		}
	}

	// @url (value="/administrasi/user/laporan-harian?inst={value}", method = POST)
	public static function GetSummary($idlaporan){
		$data = new Data();
		$data->Model('Querybuilder');
		$data->Model('Insentif');
		$db = new QueryBuilder();
		$insentif = new Insentif();
		$insentif->setIdlaporan($idlaporan);

		$summary = $insentif->CariInsentif();
		
		if ($summary->status_kerja=='1') {
			$summary->tarif_peran = '1';
			$persentaseluarkota = '1';
		}else{
			$persentaseluarkota = '0.5';
		}

		$data->insentifacc = number_format($summary->tarif_accurate * $summary->tarif_peran*$summary->ijt_acc). " ($summary->tarif_accurate x $summary->tarif_peran x $summary->ijt_acc)";
		$data->insentifharikerja = number_format($summary->tarif_hari_kerja*$summary->ijt_hari). " ( $summary->tarif_hari_kerja x $summary->ijt_hari)";
		$data->insentifwilayah = number_format($summary->tarif_wilayah*$summary->ijt_wilayah * $persentaseluarkota). " ( $summary->tarif_wilayah x $summary->ijt_wilayah x $persentaseluarkota )";
		$data->page = 'laporan';
		$data->headertext = "Summary Insentif";
		$data->judul = 'FAC || Laporan Harian';
		$data->username = $_SESSION['admin']['nama'];
		$data->View('laporan/insentif/vsummary.ins',$data);
	}

	// @url (value="/administrasi/user/laporan-harian?id={value}", method = POST)
	public static function EditNamaPerusahaan($id,$masukdata){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$data->mainrow = $db->FetchJoinWhereSingleRow(['*'],'laporan_harian',array(
			$db->getJoin('LEFT','absensi','laporan_harian.id_absen','absensi.id'),
			$db->getJoin('LEFT','perusahaan','perusahaan.id','absensi.nama_perusahaan')
			),array(
				$db->GetCond('laporan_harian.id','=',$id),
				$db->GetCond('laporan_harian.username','=',$_SESSION['admin']['username'])
			)
		);

		if ($data->mainrow['status_kerja']=='1') {
			$r=false;
			echo "<script>alert('Harap mengubah status kerja menjadi tidak sendiri. Submit ulang laporan anda terlebih dahulu dengan memilih status kerja berbeda, kemudian klik tombol hijau muda di paling bawah halaman.');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi/user/laporan-harian?id=".$id."');</script>";
		}elseif ($data->mainrow['peran']=='1'||$data->mainrow['peran']=='2') {
			$r = false;
			echo "<script>alert('Harap mengubah peran kerja menjadi peninjau. Submit ulang laporan anda terlebih dahulu dengan memilih peran kerja peninjau, kemudian klik tombol hijau muda di paling bawah halaman.');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi/user/laporan-harian?id=".$id."');</script>";
		}else{
			$r=$db->UpdateData('absensi',
				['nama_perusahaan' => $masukdata['namaPerusahaan']],
				array($db->GetCond('absensi.id','=',$masukdata['idAbsen']))
			);
			$rr=$db->UpdateData('laporan_harian',
				['status_laporan' => '2'],
				array($db->GetCond('laporan_harian.id','=',$id))
			);
		}

			

		if ($r) {
			echo "<script>alert('Nama perusahaan berhasil diubah!');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi/user/laporan-harian?id=".$id."');</script>";
		} else {
			echo "<script>alert('Nama perusahaan gagal diubah, Kontak admin dengan mengisi keluhan.');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi/user/laporan-harian?id=".$id."');</script>";
		}
	}

	// @url (value="/administrasi/user/laporan-harian?id={value}", method = POST)	
	public static function EditLaporanHarianPost($id,$masukdata){
		// print_r($masukdata);
		date_default_timezone_set("Asia/Jakarta"); 
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$data->irow = $db->FetchOneRow(['*'],'laporan_harian',array($db->GetCond('id','=',$id)));
		if (isset($masukdata['rekan'])) {
			$realrekan = implode(' # ', $masukdata['rekan']);
		} else {
			$realrekan = '';
		}
		
		if ($data->irow['status_laporan']=='1'||$data->irow['status_laporan']=='3') {
			$r = $db->UpdateData('laporan_harian',
				[
					'peserta_kegiatan' => $masukdata['peserta'],
					'kegiatan' => implode(' # ', $masukdata['kegiatan']),
					'ket_kegiatan' => $masukdata['ketkegiatan'],
					'status_kerja' => $masukdata['statuskerja'],
					'peran' => $masukdata['peran'],
					'rekan' => $realrekan,
					'jns_transportasi' => $masukdata['jnstrans'],
					'biaya_transport' => $masukdata['biaya_transport'],
					'ket_transport' => $masukdata['ket_biaya_trans'],
					'biaya_parkir' => $masukdata['biaya_parkir'],
					'biaya_lunch' => $masukdata['biaya_makan_siang'],
					'biaya_dinner' => $masukdata['biaya_makan_malam'],
					'biaya_kesehatan' => $masukdata['biaya_kesehatan'],
					'biaya_allowance' => $masukdata['biaya_allowance'],
					'biaya_lain' => $masukdata['biayalain'],
					'ket_biaya_lain' => $masukdata['ket_biaya_lain'],
					'waktu_lapor' => date("Y-m-d H:i:s", time())
				],
				array($db->GetCond('id','=',$id))
			);
		} else {
			$r = $db->UpdateData('laporan_harian',
				[
					'peserta_kegiatan' => $masukdata['peserta'],
					'kegiatan' => implode(' # ', $masukdata['kegiatan']),
					'ket_kegiatan' => $masukdata['ketkegiatan'],
					'status_kerja' => $masukdata['statuskerja'],
					'peran' => $masukdata['peran'],
					'rekan' => $realrekan,
					'jns_transportasi' => $masukdata['jnstrans'],
					'biaya_transport' => $masukdata['biaya_transport'],
					'ket_transport' => $masukdata['ket_biaya_trans'],
					'biaya_parkir' => $masukdata['biaya_parkir'],
					'biaya_lunch' => $masukdata['biaya_makan_siang'],
					'biaya_dinner' => $masukdata['biaya_makan_malam'],
					'biaya_kesehatan' => $masukdata['biaya_kesehatan'],
					'biaya_allowance' => $masukdata['biaya_allowance'],
					'biaya_lain' => $masukdata['biayalain'],
					'ket_biaya_lain' => $masukdata['ket_biaya_lain'],
					'status_laporan' => '3'
				],
				array($db->GetCond('id','=',$id))
			);
		}
			

		if ($r) {
			$logsuccess = $db->InsertData('log_data',['username' => $_SESSION['admin']['username'],'log_action' => 'Successfully updated daily report','log_ip' => $_SERVER['REMOTE_ADDR']]);
			echo "<script>alert('Data Berhasil Disimpan!');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi/user/laporan-harian?summary=".$id."');</script>";
		} else {
			echo "<script>alert('Data Gagal Disimpan!');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi/user/laporan-harian?id=".$id."');</script>";
		}
	}

	// @url (value="/administrasi/user/laporan-harian?id={value}", method = GET)	
	public static function EditLaporanHarian($id){
		$data = new Data();
		$data->Model('Querybuilder');
		$data->Model('Petugas');
		$data->Model('Insentif');
		$db = new QueryBuilder();
		$petugas = new ControlPetugas();
		$insentif = new Insentif();

		$data->JustInclude('administrasi/functions/DateFunc');
		
		//maintenance code
		// if ($_SESSION['admin']['username']!='charlez') {
			// echo "<script>alert('Sedang dalam maintenance');</script>";
			// echo "<script>location.replace('".$data->base_url."administrasi/user/laporan-harian');</script>";
		// }
		$myrolegocal = $insentif->MyroleOnGooglecal($id); 

		$data->mainrow = $db->FetchJoinWhereSingleRow(['*'],'laporan_harian',array(
			$db->getJoin('LEFT','absensi','laporan_harian.id_absen','absensi.id'),
			$db->getJoin('LEFT','perusahaan','perusahaan.id','absensi.nama_perusahaan')
			),array(
				$db->GetCond('laporan_harian.id','=',$id),
				$db->GetCond('laporan_harian.username','=',$_SESSION['admin']['username'])
			)
		);

		if ($data->mainrow['status_laporan']=='5') {
			echo "<script>alert('Laporan sudah disetujui admin, akses edit ditolak!');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi/user/laporan-harian');</script>";
		}

		$data->insentifdata = $insentif->FungsiStatusKerjaDanPeran($data->mainrow['tanggal'],$data->mainrow['nama_perusahaan']);
		// print_r($data->insentifdata);


		$data->statuskerjasendiri='';
		$data->peranutama = '';
		$data->peranpendamping = '';
		$data->theusername = '';
		$data->idutama = '-';
		$data->insper = '-'; // insentif peran kalau status sendiri == 99 or bila kondisi tidak ada pendamping (hanya peninjau)
		foreach ($data->insentifdata as $key) {
			if ($key['statuskerja']=='1') {
				$data->statuskerjasendiri=$key['nama_staff'];
				$data->theusername = $key['username'];
			}
			if ($key['peran']=='1') {
				$data->idutama = $key['id'];
				$data->peranutama = $key['nama_staff'];
			}elseif ($key['peran']=='2') {
				$data->tidakbolehsendiri = '1';
				$data->peranpendamping= $key['nama_staff'];
			}elseif ($key['peran']=='3') {
				$data->tidakbolehsendiri = '1';
				$data->peninjau = $key['nama_staff'];
			}
		}

		// peran ditentukan google calendar
		$data->perangocal = '';
		$data->gocal = '0';
		if ($myrolegocal!='') {
			$data->gocal = '1';	
			$data->perangocal = $myrolegocal;
		}

		// validate status kerja
		if ($data->statuskerjasendiri!=''&&$data->theusername!=$_SESSION['admin']['username']) {
			echo "<script>alert('".$data->statuskerjasendiri." telah menandai laporan atas perusahaan ini tidak memiliki pendamping.');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi/user/laporan-harian');</script>";
		}

		if (count($data->mainrow)=='0'||$data->mainrow=='') {
			echo "<script>alert('Laporan Tidak Terdaftar!');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi/user/laporan-harian');</script>";
		}
		// print_r($data->mainrow);
		$db->setOrder('dsk_id','ASC');
		$data->statuskerjaa = $db->Fetch('daftar_status_kerja');
		$db->setOrder('lt_id','ASC');
		$data->transportasi = $db->Fetch('daftar_alat_trans');
		$db->setOrder('dkl_id','ASC');
		$data->kegiatan = $db->Fetch('daftar_keg_laporan');
		$db->setOrder('nama','ASC');
		$data->daftarpegawai = $db->FetchWhere(['username','nama'],'users',array($db->GetCond('status','=','aktif')));
		$db->setOrder('pk_id','ASC');
		$data->perankerjalist = $db->Fetch('daftar_peran_kerja');
		$data->listperusahaan = json_decode($petugas->getDataPendaftarToday($data->mainrow['tanggal']));

		$data->page = 'laporan';
		$data->headertext = "Laporan Harian ~ <span style='color:red;'>".GetTanggal($data->mainrow['tanggal'])."</span>";
		$data->judul = 'FAC || Laporan Harian';
		$data->username = $_SESSION['admin']['nama'];
		$data->View('laporan/staff/vdailyreportedit.laporan',$data);
	}

	// @url (value="/administrasi/user/laporan-harian", method = GET)
	public static function LaporanHarian($bulan='',$tahun=''){
		$data = new Data();
		$data->JustInclude('administrasi/functions/insentiffunc');
		$data->Model('Laporan');
		$db = new ControlLaporan();


		$db->setUsername($_SESSION['admin']['username']);
		if ($bulan==''&&$tahun=='') {
			$data->listdata = $db->GetLaporanPerBulan(date('m'),date('Y'));	
		} else {
			$data->listdata = $db->GetLaporanPerBulan($bulan,$tahun);
		}
		
		// print_r($data->listdata);

		$data->headertext = "Data Laporan Harian";
		$data->judul = 'FAC || Laporan Harian';
		$data->username = $_SESSION['admin']['nama'];
		$data->datepicker = '1';

		$data->View('laporan/staff/vdailyreport.laporan',$data);
	}

	// @url (value="/administrasi/user/new", method = POST)
	public static function NewUserPost($masukdata){
		$data = new Data();
		$data->Model('Mail');
		$data->Model('Querybuilder');
		$data->Model('Insentif');
		$db = new QueryBuilder();
		$mymail = new FACMail();
		
		$data->JustInclude('administrasi/view/user/email.user');
		$newpass = substr(md5($masukdata['username']), 0,6);
		$data->usernameuser = $masukdata['username'];
		$hasil = $db->InsertData('users',
			[
				'nama' => $masukdata['nama'],
				'username' => $masukdata['username'],
				'password' => md5($newpass),
				'email' => $masukdata['email'],
				'lahir' => $masukdata['tgllahir'],
				'telepon' => $masukdata['telepon'],
				'tipe' => $masukdata['tipe'],
				'team' => $masukdata['team'],
				'status' => 'waiting'
			]
		);
		if ($hasil) {
			//data for email
			$data->to = 'fajarfifa@gmail.com';
			$data->subject = 'Notification of New FAC institute Web User';
			$data->cc = ['rifzky.mail@gmail.com','cs.facinstitute@gmail.com'];
			$data->content = EmailForAdmin($data);

			$r = $mymail->sendMailWithCC($data);
			echo "<script>location.replace('".$data->base_url."administrasi/settings/petugas');</script>";
		} else {
			echo "<script>alert('Data gagal disimpan!');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi/user/new');</script>";
		}
			

	}

	// @url (value="/administrasi/user/validate", method = GET)
	public static function ValidateUserByAdmin($username){
		// pending data user
		$data = new Data();
		$data->Model('Querybuilder');
		$data->Model('Mail');

		$mymail = new FACMail();
		$db = new QueryBuilder();

		$r = $db->FetchOneRow(['*'],'users',array($db->GetCond('username','=',$username)));
		print_r($r);
		echo $r['status'];
		if ($r['status']=='aktif') {
			echo "<script>alert('Status user telah aktif');</script>";
			echo "<script>location.replace('".$data->base_url."administrasi');</script>";
		}elseif($r['status']=='waiting') {
			$data->JustInclude('administrasi/view/user/email.user');
			$data->usernameuser = $r['username'];
			$data->passworduser = substr(md5($r['username']), 0,6);
			$data->namauser = $r['nama'];
			$data->token = md5($r['username'].'-'.date('Y-m-d'));

			//data for email
			$data->to = $r['email'];
			$data->subject = 'Verifikasi Pengguna Web fac-institute.com';

			$data->content = EmailForUser($data);

			// sending email ..
			$rr = $mymail->sendMail($data);
			echo "<script>location.replace('".$data->base_url."administrasi');</script>";
		}
// 		else {
// 			echo "<script>alert('Status user tidak aktif');</script>";
// 			echo "<script>location.replace('".$data->base_url."administrasi');</script>";
// 		}
		
	}

	// @url (value="/administrasi/user/new", method = GET)
	public static function NewUser(){
		$data = new Data();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$data->Model('Querybuilder');
		$sesi = new Sessionz();
		$db = new QueryBuilder();

		$sesi->OnlyAdmin();
		$data->listteam = $db->Fetch('users_ket_team');
		$data->listtipe = $db->Fetch('users_tipe');
		$data->page = 'dataPegawai';
		$data->subtitle = 'User Baru';
		$data->judul = 'FAC || New User';
		$data->username = $_SESSION['admin']['nama'];

		$data->View('user/vnewuser.user',$data);
	}

	public static function UpdateMap(){
		$data = new Data();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$data->Model('Querybuilder');
		$sesi = new Sessionz();
		$db = new QueryBuilder();

		$sesi->Staff();

		if (isset($_POST['in'])) {
			// print_r($_POST['in']);
			$r = $db->UpdateData('users',['addr_map' => $_POST['in']['map']],array($db->GetCond('username','=',$_SESSION['admin']['username'])));
			if ($r) {
				echo "<script>alert('Terimakasih telah melengkapi data anda di sistem kami :)');</script>";
            echo "<script>location.replace('https://fac-institute.com/administrasi/');</script>";
			} else {
				echo "<script>alert('Terjadi kesalahan, silahkan untuk melengkapi lain waktu :(');</script>";
			}
		}


		$data->page = 'tambah-invoice';
		$data->datepicker='1';
		$data->subtitle = 'Input Data Lokasi User';
		$data->judul = 'FAC || Data Petugas FAC _institute';
		$data->username = $_SESSION['admin']['nama'];

		$data->view('user/vupdatemap.user', $data);
	}

	public static function ChangeUserLevelStatus($username){
		$data = new Data();
		$data->IncludeFileWithData('administrasi/session/session-class.php',$data);
		$data->Model('Querybuilder');
		$sesi = new Sessionz();
		$db = new QueryBuilder();

		if (isset($_POST['in'])) {
			// print_r($_POST['in']);
			$r = $db->UpdateData('users',['usr_level'=>$_POST['in']['level']],array($db->GetCond('username','=',$username)));
			if ($r) {
				header('location:'.$data->base_url.'administrasi/settings/petugas');
			}else{
				echo "<script>alert('Data gagal di ubah!')</script>";
			}
		}

		$data->leveluser = $db->Fetch('intensif_lvl_trainer');
		$data->userdata = $db->FetchOneRow(['username','nama','usr_level'],'users',array($db->GetCond('username','=',$username)));

		$data->subtitle = 'Input Data Level User';
		$data->judul = 'FAC || Data Petugas FAC - institute';
		$data->username = $_SESSION['admin']['nama'];
		$data->View('user/vcgeusrlvl.user',$data);
	}

}