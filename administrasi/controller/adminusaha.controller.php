<?php include_once 'classes/View.php';
class Usaha{
	
	public static function coba(){
		$array = [
		    'id' => 'tes',
		    'coba' => 'bisa'		    
		];
		echo json_encode($array);
	}

	public static function ImportData($idusaha){
		$data = new Data();
		$data->Model('Querybuilder');
		$data->Model('Perusahaan');
		$data->JustInclude('administrasi/functions/StringFunc');
		$qb = new QueryBuilder();
		$usaha = new Perusahaan();

		if (isset($_POST['data'])) {
			print_r($_POST['data']);
		}



		$datausaha = [
		    'namausaha' => 'PT Bahagia Sejahtera',
		    'emailusaha' => 'rifzky.apple@gmail.com',
		    'alamatusaha' => 'Jl Flores no 271 Perum Pondok Aren Ciluar Bogor Utara',
		    'kotausaha' => 'Bogor',
		    'teleponusaha' => '',
		    'ketjenisusaha' => 'Jual Beli Furniture' 
		];

		$datausaha = json_encode($datausaha);
		$datausaha = json_decode($datausaha);

		$data->judul = 'Import Data Registrasi';
		$data->username = $_SESSION['admin']['nama'];
		$data->subtitle = 'Import Data Registrasi Client';
		$data->page = 'perusahaan';

		$arr = ['tu_id','tu_nama','tu_email','tu_telepon','tu_alamat','tu_kota','tu_provinsi','tu_ju','tu_kju','tu_va','tu_va','tu_map','tu_exported'];
    	$r = $qb->FetchOneRow($arr,'temp_usaha',array($qb->GetCond('tu_id','=',$idusaha)));

		//required data
		$data->jenisusaha = $usaha->FetchJenisUsaha();
		$data->accurates = $usaha->FetchVersiAccurate();
		$data->namausaha = GetVal(@$r['tu_nama']);
		$data->emailusaha = GetVal(@$r['tu_email']);
		$data->teleponusaha = GetVal(@$r['tu_telepon']);
		$data->ketjenisusaha = GetVal(@$datausaha->ketjenisusaha);
		$data->alamatusaha = GetVal(@$datausaha->alamatusaha);
		$data->jnsusaha = GetVal(@$r['tu_ju']);
		$data->va = GetVal(@$r['tu_va']);
		$data->map = GetVal(@$r['tu_map']);

		$data->kotausaha = GetVal(@$r['tu_kota']);
		$data->View('usaha/vexport-usaha', $data);

	}
}