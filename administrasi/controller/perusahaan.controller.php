<?php include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/classes/View.php';
class Usaha{
	// @url = "/administrasi/laporan/staff/chart/jenisusahapercentage" method="GET"
	public static function DiagramJenisUsaha($id){
		$data = new Data();
		$data->Model('Perusahaan');
		$data->Model('Querybuilder');
		$data->JustInclude('administrasi/functions/ChartFunc');
		$usaha = new Perusahaan();
		$db = new QueryBuilder();
		$data->listdata = $usaha->GetDiagramLaporanByJenisUsaha($id);
		$data->idjenisusaha = $id;
		$data->laman = 'jenisusaha';
		$data->jenisusaha = $db->FetchOneRow(['jenis_usaha'],'jenis_usaha',array($db->GetCond('id','=',$id)));
		$data->subtitle = "Data Laporan Berdasarkan Jenis Usaha ".$data->jenisusaha['jenis_usaha'];
		$data->judul = 'FAC || Laporan Staff ~ Jenis Usaha';
		$data->username = $_SESSION['admin']['nama'];
		$data->View('laporan/staff/vpiediagramstaff.laporan',$data);
	}

	public static function NewProv($masukdata){
		
	}

	public static function NewPerusahaanPost($masukdata){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();
		$db->InsertData('daftar_prov',['prov_id' => $masukdata['provinsi'],'prov_nama' => $masukdata['namaprov']]);
		$db->InsertData('daftar_kota',['kota_id' => $masukdata['kota'],'kota_nama' => $masukdata['namakota']]);
		$db->InsertData('daftar_kecamatan',['kec_id' => $masukdata['sbd'],'kec_nama' => $masukdata['namakec']]);
		// print_r($masukdata);
	}

	public static function NewPerusahaan(){
		$data = new Data();
		$data->Model('Querybuilder');
		$db = new QueryBuilder();

		$data->headertext = "Insentif - Daftar Daerah Wilayah";
		$data->judul = 'FAC || Daftar Daerah Wilayah';
		$data->username = $_SESSION['admin']['nama'];
		$data->View('usaha/vinputtest',$data);
	}

	public static function CariProvinsi($id=''){
		header('Content-Type: application/json');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		$myurl = "https://pro.rajaongkir.com/api/province/";

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $myurl,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "key: 1b0b4fcff1d4f1e01c68c9e7fccbeea5"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  echo $response;
		}
	}

	public static function ListKota($idprov){
		header('Content-Type: application/json');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		$myurl = "https://pro.rajaongkir.com/api/city?province=".$idprov;

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $myurl,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "key: 1b0b4fcff1d4f1e01c68c9e7fccbeea5"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  echo $response;
		}
	}

	public static function CariSubdistrict($idkota){
		header('Content-Type: application/json');
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		
		$myurl = "https://pro.rajaongkir.com/api/subdistrict?city=".$idkota;
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $myurl,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "key: 1b0b4fcff1d4f1e01c68c9e7fccbeea5"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  echo $response;
		}
	}
}
/*
if (isset($_GET['q'])) {
	if ($_GET['q']=='province') {
		$myurl = "https://pro.rajaongkir.com/api/province/";
	}elseif ($_GET['q']=='city') {
		$myurl = "https://pro.rajaongkir.com/api/city?province=5";
	}elseif ($_GET['q']=='subd') {
		$myurl = "https://pro.rajaongkir.com/api/subdistrict?city=79";
	}
}

if (isset($_GET['prov'])&&!empty($_GET['prov'])) {
	$myurl = "https://pro.rajaongkir.com/api/city?province=".$_GET['prov'];
}

if (isset($_GET['kota'])&&!empty($_GET['kota'])) {
	$myurl = "https://pro.rajaongkir.com/api/city?province=".$_GET['kota'];
}

if (isset($_GET['sbd'])&&!empty($_GET['sbd'])) {
	$myurl = "https://pro.rajaongkir.com/api/subdistrict?city=".$_GET['sbd'];
}
*/


