<?php 
session_start();
if (!isset($_SESSION['admin'])||($_SESSION['admin']['tipe']!='admin'&&$_SESSION['admin']['tipe']!='staff'&&$_SESSION['admin']['tipe']!='marketing')) {
    header('Location:../login/index.php');
}
?>

<!DOCTYPE html>
<html>

<?php 
//echo $tipeAdmin=$_SESSION['admin']['tipe'];
$judul='Daftar Forum FAC Institute';
$page = 'home';
include_once 'header.php';
include_once '../model/Forum.php';

$forum = new Forum();
?>

<body>

<?php 

include_once 'sidebar.php';

include_once 'top-nav.php';

 ?>


<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3>FAC Forums</h3>
	</div>

    <div class="row">
        <div class="col-md-12">
            <section class="col-md-6">
                <a href="forum-add" class="btn btn-xs btn-default">
                    <span class="glyphicon glyphicon-plus"></span>
                </a>
            </section>
            <section class="col-md-6" style="text-align:right;">
                <a data-toggle="modal" href="#modal-cari" class="btn btn-primary">Search <span class="glyphicon glyphicon-search"></span></a>               
            </section>
        </div>
    </div>
    <div class="row" style="overflow:auto;">
        <div class="col-md-12">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Judul</th>
                        <th>Petugas</th>
                        <th>Kategori</th>
                        <th>Views</th>
                    </tr>
                </thead>
                <?php 
                    if (isset($_GET['s'])) {
                        $forum->setJudul($_GET['s']['j']);
                        $forum->getTag($_GET['s']['t']);
                        $forum->getKategori($_GET['s']['k']);
                        $forum->getPetugas($_GET['s']['p']);
                        $datas=$forum->searchForum();
                    } else {
                        $datas=$forum->fetchAllData();    
                    }
                    
                ?>
                <tbody>
                <?php foreach ($datas as $key) { ?>
                    <tr>
                        <td><a href="forum-question?v=<?php echo $key->id ?>"><?php echo $key->title ?></a></td>
                        <td><?php echo $key->nama ?></td>
                        <td><?php echo $key->categories ?></td>
                        <td><?php echo $key->views ?></td>
                    </tr>
                <?php } ?>

                </tbody>
            </table>            
        </div>
    </div>

</div>

<!-- Modal -->
  <div class='modal fade' id='modal-cari' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pencarian Data</h3>

        </div>
       <div class='modal-body' style='overflow-y:scroll'>
        <div style='padding:15px; height: 300px;' class='row'>
          
          <div class="col-md-12">
        
                <form action="" method="GET">

                    <div class="row">
                        <div class="form-group">
                          <label>Judul</label>
                          <input type="text" name="s[j]" class="form-control">
                        </div>
                        <div class="form-group">
                          <label>Tags</label>
                          <input type="text" name="s[t]" class="form-control">
                        </div>
                        <div class="form-group">
                          <label>Kategori</label>
                          <input type="text" name="s[k]" class="form-control">
                        </div>
                        <div class="form-group">
                          <label>Petugas</label>
                          <input type="text" name="s[p]" class="form-control">
                        </div>
                        
                    </div>

                    <div class="row">
                        <button class="btn btn-success" style='width:100%'>Cari</button>
                    </div>

                </form>
          </div>
        </div>
       </div>


        <div class='modal-footer'>

            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>FAC-Institute 2016</span>
                </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->  

<!--Start of Tawk.to Script-->
<script type="text/javascript">
  var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
  (function(){
  var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
  s1.async=true;
  s1.src='https://embed.tawk.to/58be3a2b93cfd3557204996c/default';
  s1.charset='UTF-8';
  s1.setAttribute('crossorigin','*');
  s0.parentNode.insertBefore(s1,s0);
  })();
</script>
<!--End of Tawk.to Script-->
</body>
</html> 