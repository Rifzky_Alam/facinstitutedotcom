<?php 
session_start();
include_once '../baseurl.php';
include_once 'session/session-class.php';
include_once '../model/Transaksi.php';
$session = new Sessionz();
$session->Staff();
$transaksi = new Transaksi();
if (isset($_GET['tr'])&&!empty($_GET['tr'])) {
	$transaksi->SetID($_GET['tr']);

	$datatrans = $transaksi->FetchForDetail();
	$data = array(
	'base_url' => getBaseUrl(), //base data
        'url'=> basename(__FILE__, '.php'),
        'judul' => 'Detail Transaksi/FAC Institute',
        'username'=>$_SESSION['admin']['nama'],
        'idtrans' =>$_GET['tr'],
        'subtitle' => 'Detail Transaksi',
        'page' => 'detail-transaksi', // end base data
        'id_usaha' => $datatrans->id,
        'nama_usaha' => $datatrans->nama_usaha,
        'nama_cust' => $datatrans->nama_cust,
        'email_usaha'=> $datatrans->email,
        'email_cust'=> $datatrans->email_cust,
        'telepon_usaha' => $datatrans->telepon,
        'telp_cust' => $datatrans->telp_cust,
        'alamat'=> $datatrans->alamat,
        'alamat_training'=> $datatrans->trans_lokasi,
        'map'=> $datatrans->map,
        'jabatan' => $datatrans->jabatan_cust,
        'jenis_usaha' => $datatrans->jenis_usaha,
        'ket_jenis_usaha'=> $datatrans->ket_jenis_usaha,
        'tanggal_pesan' => $datatrans->trans_date,
        'lama_training' => $datatrans->jumlah_hari,
        'jenis_pengguna'=> JenisPengguna($datatrans->jenis_pengguna_cust),
        'versi_accurate'=> $datatrans->versi_acc,
        'agenda_training' => $datatrans->nama_agenda,
        'nama_marketing' => $datatrans->marketing_nama
	);

        if (!isset($_SESSION['admin'])||($_SESSION['admin']['tipe']!='admin'&&$_SESSION['admin']['tipe']!='marketing')) {
                $data['isadmin'] = 0;
        } else {
                $data['isadmin'] = 1;                
        }
        


	$data = (object) $data;
	include_once 'view/transaksi/view-detail-transaksi.php';

}




function JenisPengguna($value){
	if ($value=='1') {
		return 'Lama';
	} else {
		return 'Baru';
	}
	
}
?>