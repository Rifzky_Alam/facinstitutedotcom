<?php 
session_start();
if (!isset($_SESSION['admin'])||empty($_SESSION['admin']['nama'])) {
    //echo "oke";
    header('Location:index.php');
}

include_once '../model/Petugas.php';
$control = new ControlPetugas();

?>
<!DOCTYPE html>
<html>


<?php 
$judul='Daftar Hadir Absensi - FAC Instite';
$page='daftarHadir';
include_once 'header.php'; 
?>

<body>
<?php 
//echo $_SESSION['admin']['tipe'];
?>


<?php 

include_once 'sidebar.php';
include_once 'top-nav.php';

 ?>

<?php 
if ($_SESSION['admin']['tipe']=='admin'){
    $dataSelect = $control->fetchUsernameAndName(); 
}else{
    $dataSelect = $control->fetchUsernameAndNameByTeam($_SESSION['admin']['team']); 
}

if (isset($_GET['key'],$_GET['loc'])) { 
$dataPetugas=$control->selectByID($_GET['key']);
?>

<script>
var myCenter=new google.maps.LatLng(<?php echo $_GET['loc']; ?>);

function initialize()
{
var mapProp = {
  center:myCenter,
  zoom:15,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };

var map=new google.maps.Map(document.getElementById("map-canvas"),mapProp);

var marker=new google.maps.Marker({
  position:myCenter,
  });

marker.setMap(map);
}

google.maps.event.addDomListener(window, 'load', initialize);
</script>

<div class="container" style="padding-top:40px;padding-bottom:40px;">
    <div class="page-header" id="top-logo">
        <h3>Detail Absen</h3>
    </div>
    
    <div class="row">
        <div class="col-md-6">
            <table class="table table-bordered">
                <thead><tr><th colspan="2"><?php echo $dataPetugas->nama; ?></th></tr></thead>
                <tbody>
                    
                    <tr>
                        <td>Email:</td>
                        <td>
                            <a href="mailto:<?= $dataPetugas->email ?>">
                                <?php echo $dataPetugas->email; ?>
                            </a>
                        </td>
                        </tr>
                    <tr>
                        <td>Telepon:</td>
                        <td>
                            <a href="tel://<?= $dataPetugas->telepon ?>">
                                <?php echo $dataPetugas->telepon; ?>
                            </a>
                        </td>
                    </tr>
                    <tr><td>Links to share:</td><td><a target="_blank" href=<?php echo "'https://www.google.co.id/maps/place/". $_GET['loc']."'"; ?>>Lokasi ku</a></td></tr>
                    
                </tbody>
            </table>
        </div>
        <div class="col-md-6">
            <label>Lokasi Absen</label>
            <div id="map-canvas" style="width:100%;height:400px;border:1px solid">Map is here</div>
        </div>
    </div>



<?php }else{ 

if ($_SESSION['admin']['tipe']=='admin'){
    $dataAbsen = $control->daftarHadir('%'.date('Y-m-d').'%','%%');
}else{
    $dataAbsen = $control->daftarHadirByTeam('%'.date('Y-m-d').'%','%%',$_SESSION['admin']['team']);
}

?>

<?php 
    if ($_SESSION['admin']['tipe']=='admin' || $_SESSION['admin']['tipe']=='HoAC'){
        
        if (isset($_GET['tanggalAwal'],$_GET['tanggalAkhir'],$_GET['nama'],$_GET['srcNamaPerusahaan'])) {

            if ($_GET['tanggalAwal']=='') {
                $_GET['tanggalAwal']=date('Y-m-d');
            }

            if ($_GET['tanggalAkhir']=='') {
                $_GET['tanggalAkhir']=date('Y-m-d');
            }
            /*
            $query = "SELECT nama,absen_masuk,nama_perusahaan,lokasi_absen_masuk,absen_pulang,lokasi_absen_pulang,tanggal_absen FROM users,absensi 
            WHERE absensi.username=users.username tanggal_absen BETWEEN :tanggalAwal AND :tanggalAkhir AND nama_perusahaan LIKE :namaPerusahaan";

            for ($i=0; $i < count($_GET['nama']) ; $i++) { 
                if ($i>0) {
                    $query .= " OR absensi.username='".$_GET['nama'][$i]."' AND tanggal_absen BETWEEN :tanggalAwal AND :tanggalAkhir AND nama_perusahaan LIKE :namaPerusahaan AND absensi.username=users.username";
                }else{
                    $query .= " AND absensi.username='".$_GET['nama'][0]."'";
                }
            }
            echo $query;
            #*/$dataAbsen = $control->daftarHadirByParameterAdmin($_GET['tanggalAwal'],$_GET['tanggalAkhir'],$_GET['nama'],$_GET['srcNamaPerusahaan']);
            
        }else if(isset($_GET['tanggalAwal'],$_GET['tanggalAkhir'],$_GET['srcNamaPerusahaan'])){
            $dataAbsen = $control->daftarHadirWithoutUsername($_GET['tanggalAwal'],$_GET['tanggalAkhir'],$_GET['srcNamaPerusahaan']);
        }
    }else{

        if (isset($_GET['tanggalAwal'],$_GET['tanggalAkhir'],$_GET['nama'])) {

            if ($_GET['tanggalAwal']=='') {
                $_GET['tanggalAwal']=date('Y-m-d');
            }

            if ($_GET['tanggalAkhir']=='') {
                $_GET['tanggalAkhir']=date('Y-m-d');
            }

            $dataAbsen = $control->daftarHadirByParameter($_GET['tanggalAwal'],$_GET['tanggalAkhir'],$_GET['nama']);
            echo "oke";
        }
    }

?>

<div class="container" id="isi">
    
    <div class="row">
        <div class="col-md-6 col-sm-6">
            <h1>Daftar Hadir</h1>
        </div>

        <div class="col-md-6 col-sm-6" style="padding-top:30px;text-align:right;">
            <button class="btn btn-primary" style="text-align:right" id="cari-data" href="#modal-cari" data-toggle="modal" >Cari Data <span class="glyphicon glyphicon-search"></span></button>
        </div>
    </div>
    
    
    <div class="row" style="overflow:auto">
        <div class="col-md-12">

            <table class="table table-bordered">
                <thead><tr><th style="width:100px" class="tengah">Tanggal</th><th class="tengah">Nama</th><th class="tengah">Jam Masuk</th><th class="tengah">Lokasi Masuk</th><th class="tengah">Perusahaan</th><th class="tengah">Jam Keluar</th><th class="tengah">Lokasi Keluar</th></tr></thead>
                <tbody>
                    
                    <?php 
                    for ($i=0; $i < count($dataAbsen); $i++) { 
                        echo "<tr>";
                        $myDate = explode('-',$dataAbsen[$i]->getTanggal());
                        echo "<td>".$myDate[2]."-".$myDate[1]."-".$myDate[0]."</td>";
                        echo "<td>".$dataAbsen[$i]->getNama()."</td>";
                        echo "<td>".$dataAbsen[$i]->getJamMasuk()."</td>";
                        echo "<td><a href='".basename(__FILE__, '.php')."?key=".$dataAbsen[$i]->getUsername()."&loc=".$dataAbsen[$i]->getLokasiMasuk()."'>".$dataAbsen[$i]->getLokasiMasuk()."</a></td>";
                        echo "<td>".$dataAbsen[$i]->getNamaPerusahaan()."</td>";
                        echo "<td>".$dataAbsen[$i]->getJamPulang()."</td>";
                        echo "<td><a href='".basename(__FILE__, '.php')."?key=".$dataAbsen[$i]->getUsername()."&loc=".$dataAbsen[$i]->getLokasiPulang()."'>".$dataAbsen[$i]->getLokasiPulang()."</a></td>";            
                        echo "</tr>";
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>

	
</div>
<?php } ?>
<!-- Modal -->
  <div class='modal fade' id='modal-cari' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pencarian Data</h3>

        </div>
        <div class='modal-body'>
        <div style='padding-left:15px;padding-right:15px;padding-top:0px; height: 300px;' class='row'>
          
          <div class="col-md-12">
            
              
                <form action=<?php echo "'".basename(__FILE__, '.php')."'"; ?> method="GET">
                    <div class="row">
                        <?php if ($_SESSION['admin']['tipe']=='admin'): ?>
                        <div class="form-group">
                            <select id='userz' class='form-control' name='nama[]' multiple='multiple' >
                                <?php 
                                for ($i=0; $i < count($dataSelect) ; $i++) {
                                    echo "<option value='".$dataSelect[$i]->username."'>".$dataSelect[$i]->nama."</option>";
                                }
                                 ?>
                            </select>
                        </div>
                            <?php else: ?>
                        <div class="form-group">
                            <select class='form-control' name='nama'>
                                <?php 
                                for ($i=0; $i < count($dataSelect) ; $i++) {
                                    echo "<option value='".$dataSelect[$i]->username."'>".$dataSelect[$i]->nama."</option>";
                                }
                                 ?>
                            </select>
                        </div>
                        <?php endif ?>
                        
                    </div>


                    <div class="row">
                        <div class="form-group">
                            <label for="dari-tanggal">Mulai Tanggal</label>
                            <input type="text" name="tanggalAwal" id="tanggal-awal" class="form-control" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label for="dari-tanggal">Sampai Tanggal</label>
                            <input type="text" name="tanggalAkhir" id="tanggal-akhir" class="form-control" />
                        </div>
                    </div>

                    <?php if ($_SESSION['admin']['tipe']=='admin' || $_SESSION['admin']['tipe']=='HoAC'): ?>
                        
                    <div class='row'>
                        <div class='form-group'>
                            <label>Nama Perusahaan</label>
                            <input type="text" name="srcNamaPerusahaan" id="nama-perusahaan" class="form-control" />
                        </div>
                    </div>

                    <?php endif ?>

                  

                    <div class="row">
                        <button class="btn btn-success">Cari</button>
                    </div>

                </form>
          </div>
             
          

        </div>
          

        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>FAC-Institute 2016</span>
                </div>
                <div class="col-md-6">
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->  
<script type="text/javascript" src=<?php echo "'".getBaseUrl()."js/jquery-datepicker/jquery-ui.js'"; ?>></script>
<script type="text/javascript" src=<?php echo "'".getBaseUrl()."js/daftar-absen.js'"; ?>></script>
<script type="text/javascript">
$('#userz').multiselect({
        buttonWidth: '400px',
        includeSelectAllOption: true
    });
</script>

</body>
</html> 