<?php
session_start();
if ($_SESSION['admin']['tipe']!='admin'&&$_SESSION['admin']['username']!=$_GET['data']['username']) {
  header('location: ../login/register/validasi.php');
}

function removeLastString($value){
    if (substr($value,strlen($value)-1)=='-') {
      return substr($value, 0,strlen($value)-1);
    }else{
      return $value;
    }
}


if (isset($_GET['data'])) {
	include_once '../model/Laporan.php'; 
	include_once '../model/Petugas.php';
  include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/functions/insentiffunc.php';
	$petugas = new ControlPetugas();
	$ctrl_laporan = new ControlLaporan();
  $dkl = $ctrl_laporan->FetchKeteranganKegiatan();
  $datas = json_decode($ctrl_laporan->downloadLaporan($_GET['data']['username'],$_GET['data']['tanggalAwal'],$_GET['data']['tanggalAkhir']));

  $bulan = explode('-', $_GET['data']['tanggalAwal']);
  $filename = "Laporan Kerja_" .$bulan[1]."_".date('Y') ."_".$datas[0]->nama.".xls";
  header("Content-Disposition: attachment; filename=\"$filename\"");
  header("Content-Type: application/vnd.ms-excel");
  // Original PHP code by Chirp Internet: www.chirp.com.au
  // Please acknowledge use of this code by including this header.

	$dataSelect = $petugas->fetchUsernameAndName();

	$dataPegawai = array();
		
    for ($i=0; $i < count($dataSelect) ; $i++) { 
        array_push($dataPegawai,array($dataSelect[$i]->username,$dataSelect[$i]->nama));
    }
    //data downloaded from view
	
	$myRow = array();

	for ($i=0; $i < count($datas); $i++) { 
		$myRekan = explode(" # ",$datas[$i]->rekan);
		$dataRekan = array();
		for ($j=0; $j < count($myRekan) ; $j++) { 
			for ($k=0; $k < count($dataPegawai); $k++) { 
				if ($dataPegawai[$k][0]==$myRekan[$j]) {
					array_push($dataRekan, $dataPegawai[$k][1]);
				}
			}
		}
		$realRekan = implode(" # ", $dataRekan);

    $row = array(
      "No"=>$i+1,
      "Timestamp"=>$datas[$i]->waktu_lapor, 
    	"Nama" => $datas[$i]->nama, 
    	"Jenis Hari Kerja"=>ucwords($datas[$i]->jns_hari_kerja),
    	"Hari" =>ucfirst($datas[$i]->hari),
      "Tanggal" => $datas[$i]->tanggal,
      "Jam Masuk"=>$datas[$i]->absen_masuk,
    	"Jam Pulang"=>$datas[$i]->absen_pulang,
    	"Overtime"=>KalkulasiOvertime($datas[$i]->absen_masuk,$datas[$i]->absen_pulang),
    	"Klien"=>$datas[$i]->nama_perusahaan,
      "Versi Accurate" => $datas[$i]->va,
      "Jenis Usaha"=>ucwords(strtolower($datas[$i]->jenis_usaha)),
      "Keterangan Jenis Usaha"=>ucfirst($datas[$i]->keterangan_jenis_usaha),
      "Alamat Klien"=>$datas[$i]->alamat,
    	"Kota"=>ucwords(strtolower($datas[$i]->kota)),
    	"Provinsi"=>ucwords(strtolower($datas[$i]->provinsi)),
      "Kegiatan"=>$ctrl_laporan->CekKeterangan($datas[$i]->kegiatan,$dkl),
    	"Keterangan Kegiatan"=>$datas[$i]->ket_kegiatan,
    	"Peserta Kegiatan"=>$datas[$i]->peserta_kegiatan,
    	"Status Kerja"=>ucwords(strtolower($datas[$i]->status_kerja)),
    	"Peran"=>ucwords(strtolower($datas[$i]->peran)),
    	"Rekan"=>$realRekan,
    	"Jenis Transportasi"=>ucwords(strtolower(@$datas[$i]->jns_transportasi)),
    	"Biaya Transport"=>$datas[$i]->biaya_transport,
    	"Keterangan Transport"=>@$datas[$i]->ket_transport,
    	"Biaya Parkir"=>$datas[$i]->biaya_parkir,
    	"Biaya Makan Siang"=>$datas[$i]->biaya_lunch,
    	"Biaya Makan Malam"=>$datas[$i]->biaya_dinner,
      "Biaya Allowance"=>$datas[$i]->biaya_allowance,
    	"Biaya Kesehatan"=>$datas[$i]->biaya_kesehatan,
    	"Biaya Lain"=>$datas[$i]->biaya_lain,
    	"Keterangan Biaya Lain"=>@$datas[$i]->ket_biaya_lain,
      "Insentif Training"=> KalkulasiInsentifAcc(@$datas[$i]->tarif_accurate,@$datas[$i]->tarif_peran,@$datas[$i]->ijt_acc),
      "Insentif Libur"=> KalkulasiInsentifHaker(@$datas[$i]->tarif_hari_kerja,@$datas[$i]->ijt_hari),
      "Insentif Luar Kota"=> KalkulasiInsentifLuarKota(@$datas[$i]->tarif_wilayah,@$datas[$i]->ijt_wilayah,@$datas[$i]->is_ttl_attnd)
    	);
	array_push($myRow, $row);

	}

  function cleanData(&$str){
    $str = preg_replace("/\t/", "\\t", $str);
    $str = preg_replace("/\r?\n/", "\\n", $str);
    if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
  }

  // file name for download

  $flag = false;
  foreach($myRow as $row) {
    if(!$flag) {
      // display field/column names as first row
      echo implode("\t", array_keys($row)) . "\n";
      $flag = true;
    }
    array_walk($row, __NAMESPACE__ . '\cleanData');
    echo implode("\t", array_values($row)) . "\n";
  }

  exit;
//end experiment here
	





}



?>