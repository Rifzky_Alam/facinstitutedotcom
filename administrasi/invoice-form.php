<?php 
session_start();
if (!isset($_SESSION['admin'])||($_SESSION['admin']['tipe']!='admin'&&$_SESSION['admin']['tipe']!='marketing')) {
    header('Location:../login/index.php');
}
?>


<!DOCTYPE html>
<html>


    <?php 
    $judul = 'Persiapan Data Invoice';
    $page = 'penawaran';
    include_once 'header.php'; 

    ?>
<body>

<?php 

include_once 'sidebar.php';

include_once 'top-nav.php';
include_once '../model/Pendaftar.php'; 
include_once '../model/page.php';
    $myPage = new Page();
    $pendaftar = new Pendaftar();

if (isset($_GET['idj'])&&!empty($_GET['idj'])) {
    $dataz=$pendaftar->getJadwal($_GET['idj']);
    $datazz=$pendaftar->getJadwal2($dataz->id_pendaftar);
 ?>

<div class="container" id="isi">
    <div class="page-header" id="top-logo">
        <h4>Form Data Invoice <strong><?php echo $dataz->nama ?></strong></h4>
    </div>

<?php 

if (isset($_POST['in'])){
    $datazz=$pendaftar->getJadwal2($dataz->id_pendaftar);

    $input = array(
        'id' => $_POST['in']['idpendaftar'],
        'idJadwal'=> $_GET['idj'],
        'deskripsi' => $_POST['in']['deskripsi'],
        'metode' => $_POST['in']['metode'],
        'dp' => $_POST['in']['dp'],
        'diskon' => $_POST['in']['diskon']
    );

    $objek = (object) $input;

    
        if ($pendaftar->addInvoice($objek)) {
            include_once '../model/Mail.php';
            $mail = new FACMail();
            echo "<script>alert('Invoice Berhasil disimpan');</script>";
            $myMailData = array(
                'to' => 'finance@fac-institute.com',
                'nama' => '',
                'cc'=>array('ceso.facinstitute@gmail.com'),
                'subject' => 'FAC - Invoice Confirmation Request of '.$dataz->nama, 
                'content'=> 'Data Invoice berhasil disimpan dan menunggu konfirmasi dari admin.'
            );
            $myMailData = (object) $myMailData;
            $mail->sendMailWithCC($myMailData);
            echo "<script>location.replace('daftar-agenda?p=".$objek->id."');</script>";
        }else{
            echo "<script>alert('Data gagal disimpan, harap hubungi admin system!');</script>";
        }
        


    /*echo "<div class='row'>";
    echo "id: ".$objek->id."<br>";
    echo "paket: ".$objek->paket."<br>";
    echo "qty: ".$objek->qty."<br>";
    echo "jumlah hari: ".$objek->jml_hari."<br>";
    echo "deskripsi: ".$objek->deskripsi."<br>";
    echo "metode: ".$objek->metode."<br>";
    echo "dp: ".$objek->dp."<br>";
    echo "diskon: ".$objek->diskon."<br>";
    echo "</div>";*/   
}


?>
        

    <div class="row">
        <div class="col-md-10">
            <table style="width:100%">
            <tbody>
                <tr>
                    <td style="width:15%">Paket</td><td style="width:5px">:</td><td style="padding-left:5px"><?php echo $datazz->nama_item ?></td>
                </tr>
                <tr>
                    <td >Qty</td><td>:</td><td style="padding-left:5px"><?php echo $datazz->qty ?></td>
                </tr>
                <tr>
                    <td>Lama Hari</td><td>:</td><td style="padding-left:5px"><?php echo $datazz->qty * $datazz->paket_hari ?></td>
                </tr>
            </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <?php 
            if ($dataz->id_invoice!='') {
                echo "<script>alert('Invoice untuk jadwal ini sudah ada!');</script>";
                echo "<script>location.replace('daftar-agenda?p=".$dataz->id_pendaftar."');</script>";    
            }
        ?>
    </div>

    <br>
    <form action="" method="post">

    <div class="row" style="display:none">
        <input type="text" name="in[idpendaftar]" value="<?php echo $dataz->id_pendaftar ?>">
    </div>


    <div class="row">
        <div class="col-md-10 form-group">
            <label>Deskripsi Invoice</label>
            <input type="text" name="in[deskripsi]" class="form-control">
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-10 form-group">
            <label>Metode Pembayaran</label>
            <select class="form-control" name="in[metode]">
                <option value="cash">Bayar di Muka</option>
                <option value="term-1">Termin 1</option>
                <option value="term-2">Termin 2</option>
            </select>
        </div>
    </div>

    <div class="row">
        <div class="col-md-10 form-group">
            <label>DP</label>
            <input type="number" name="in[dp]" class="form-control" placeholder="(hanya angka)" value="0">
        </div>
    </div>

    <div class="row">
        <div class="col-md-10 form-group">
            <label>Diskon</label>
            <input type="number" name="in[diskon]" class="form-control" placeholder="Tentukan dalam % (hanya angka)" value="0">
        </div>
    </div>

    <div class="row">
        <div class="col-md-10">
            <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>
        </div>
    </div>
    </form>

    <?php } ?>
</div>
</body>
</html>