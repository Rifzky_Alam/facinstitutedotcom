<?php 
session_start();
if (!isset($_SESSION['admin'])||($_SESSION['admin']['tipe']!='admin'&&$_SESSION['admin']['tipe']!='marketing')) {
    header('Location:../login/index.php');
}
?>
<!DOCTYPE html>
<html>


<?php 
$judul='Penawaran (custom) - FAC Institute';
$page = 'calendar';
include_once 'header.php'; 
include_once '../model/Pendaftar.php'; 
$pendaftar = new Pendaftar();

?>

<body>



<?php 

include_once 'sidebar.php';

include_once 'top-nav.php';
include_once '../model/Calendar.php';
include_once '../model/Pendaftar.php';
$ctrlTransaksi = new Pendaftar();
$cal = new Calender();

function getValue($value){
    if ($value=='') {
        return '';
    } else {
        return $value;
    }   
}

function getLampiran($value){
    if (empty($value)){
        return '';
    } else {
        return implode(' # ', $value);
    }
}

if (isset($_POST['ip'])) {
    $jumlah = intval($pendaftar->getJumlahCustQuot()) + 1;
    $aidi = 'WS/FAC/C-'. $jumlah;//md5($_POST['ip']['np'].$_POST['ip']['paket'].date('Y-m-d'));

    if (isset($_POST['ip']['lampiran'])){
        $lampir = getLampiran($_POST['ip']['lampiran']);    
    }else {
        $lampir='';
    }
    

    $objek = array(
        'id'=> $aidi,
        'nama_usaha' => getValue($_POST['ip']['np']),
        'nama_personal' => getValue($_POST['ip']['namakontak']),
        'subjek' => getValue($_POST['ip']['subject']),
        'q_title' => getValue($_POST['ip']['quotTitle']),
        'paket' => getValue($_POST['ip']['paket']),
        'biaya_transport' => getValue($_POST['ip']['biayatrans']),
        'myqty'=>getValue($_POST['ip']['myqty']),
        'qtytrans'=>getValue($_POST['ip']['qtytrans']),
        'notes' => getValue($_POST['ip']['notes']),
        'email' => getValue($_POST['ip']['email']),
        'cc' => getValue($_POST['ip']['emailCC']),
        'petugas' => $_SESSION['admin']['username'],
        'lampiran' => $lampir,
        'ip'=> $_SERVER['REMOTE_ADDR']
    );

    $objek = (object) $objek;
    if ($pendaftar->quotationInput($objek)) {
        echo "<script>alert('Data berhasil disimpan!');</script>";
        echo "<script>location.replace('daftar-quotation');</script>";
    } else {
        echo "<script>alert('Data gagal disimpan, Harap kontak administrator!');</script>";
    }
    
}
 ?>


<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3>Penawaran (Custom) FAC-Institute</h3>
	</div>

    <?php if (isset($_GET['ac'])&&$_GET['ac']=='edit'&&!empty($_GET['id'])): ?>
        <?php $dataEdit=$pendaftar->getQuotationByID($_GET['id']); ?>

        <div class="row">
            <?php 
                if (isset($_POST['ep'])) {
                    $lampirz=getLampiran(@$_POST['ep']['lampiran']);
                    $editObj = array(
                        'id' => $_POST['ep']['id'],
                        'nama_usaha' => $_POST['ep']['np'],
                        'nama_personal' => $_POST['ep']['namakontak'],
                        'subjek' => $_POST['ep']['subject'],
                        'paket' => $_POST['ep']['paket'],
                        'myqty'=>$_POST['ep']['myqty'],
                        'biaya_transport' => $_POST['ep']['biayatrans'],
                        'qtytrans'=> $_POST['ep']['qtytrans'],
                        'notes' => $_POST['ep']['notes'],
                        'title' => $_POST['ep']['quotTitle'],
                        'email' => $_POST['ep']['email'],
                        'cc' => $_POST['ep']['emailCC'],
                        'lampiran' => $lampirz,
                        'petugas' => $_SESSION['admin']['username']
                    );

                    // echo "ID: ".$_POST['ep']['id']."<br>";
                    // echo "Nama Usaha: ".$_POST['ep']['np']."<br>";
                    // echo "Nama Kontak: ".$_POST['ep']['namakontak']."<br>";
                    // echo "Subjek: ".$_POST['ep']['subject']."<br>";
                    // echo "Title: ".$_POST['ep']['quotTitle']."<br>";
                    // echo "Paket: ".$_POST['ep']['paket']."<br>";
                    // echo "Biaya Transportasi: ".$_POST['ep']['biayatrans']."<br>";
                    // echo "Email: ".$_POST['ep']['email']."<br>";
                    // echo "CC: ".$_POST['ep']['emailCC']."<br>";
                    // echo "Lampiran: ".."<br>";   
                
                    $editObj = (object) $editObj;                
                    if ($pendaftar->editQuotation($editObj)) {
                        echo "<script>alert('Data berhasil disimpan!');</script>";
                        echo "<script>location.replace('daftar-quotation');</script>";
                    } else {
                        echo "<script>alert('Data gagal disimpan!');</script>";
                    }
                }
            ?>
        </div>


        <form action="" method="post">
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Nama Perusahaan</label>
                        <?php if (isset($_POST['ep'])): ?>
                        <input type="text" name="ep[np]" class="form-control" required maxlength="120" value="<?php echo $_POST['ep']['np'] ?>">
                            <?php else: ?>
                        <input type="text" name="ep[np]" class="form-control" required maxlength="120" value="<?php echo $dataEdit->nama_usaha ?>">
                        <?php endif ?>
                        <input type="text" name="ep[id]" value="<?php echo $dataEdit->id ?>" style="display:none;">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Nama Personal</label>
                        <?php if (isset($_POST['ep'])): ?>
                        <input type="text" name="ep[namakontak]" class="form-control" required maxlength="40" value="<?php echo $_POST['ep']['namakontak'] ?>">
                            <?php else: ?>
                        <input type="text" name="ep[namakontak]" class="form-control" required maxlength="40" value="<?php echo $dataEdit->nama_personal ?>">
                        <?php endif ?>
                        
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Subject</label>
                        <?php if (isset($_POST['ep'])): ?>
                            <input type="text" name="ep[subject]" class="form-control" required maxlength="75" value="<?php echo $_POST['ep']['subject'] ?>">
                            <?php else: ?>
                            <input type="text" name="ep[subject]" class="form-control" required maxlength="75" value="<?php echo $dataEdit->subjek ?>">
                        <?php endif ?>
                        
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Judul Penawaran (PDF)</label>
                        <?php if (isset($_POST['ep'])): ?>
                            <input type="text" name="ep[quotTitle]" class="form-control" required maxlength="100" value="<?php echo $_POST['ep']['quotTitle'] ?>">
                            <?php else: ?>
                                <input type="text" name="ep[quotTitle]" class="form-control" required maxlength="100" value="Best Quotation Jasa Training ACCURATE Accounting Software" value="<?php echo $dataEdit->quotation_title ?>">
                        <?php endif ?>
                        
                    </div>
                </div>
            </div>

            <?php $dataPaket = json_decode($pendaftar->fetchPaketTraining()); ?>
                <div class="row">
                    <div class="col-md-10">
                        <label><a href="input-paket">Nama Paket</a></label>
                        <select class="form-control" name="ep[paket]" id="nitem">
                        <?php  
                            for ($i=0; $i < count($dataPaket); $i++) { 
                                if ($dataPaket[$i]->id==$dataEdit->produk_id) {
                                    echo "<option value='".$dataPaket[$i]->id."' selected>".$dataPaket[$i]->nama_item." -- ".$dataPaket[$i]->price_for." -- ".number_format($dataPaket[$i]->harga)."</value>";
                                } else {
                                    echo "<option value='".$dataPaket[$i]->id."'>".$dataPaket[$i]->nama_item." -- ".$dataPaket[$i]->price_for." -- ".number_format($dataPaket[$i]->harga)."</value>";
                                }
                            }
                        ?>
                        </select>
                    </div>
                </div>
            <br>

            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Qty</label>
                        <input class="form-control" id="jmlhari" name="ep[myqty]" maxlength="3" value="<?php echo $dataEdit->jumlah_hari ?>">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Biaya transport</label>
                        <?php if (isset($_POST['ep'])): ?>
        <input type="text" id="biayaTransport" name="ep[biayatrans]" class="form-control" required maxlength="9" value="<?php echo $_POST['ep']['biayatrans'] ?>">
                            <?php else: ?>
        <input type="text" id="biayaTransport" name="ep[biayatrans]" class="form-control" required maxlength="9" value="<?php echo $dataEdit->biaya_transport ?>">
                        <?php endif ?>
                        
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Jumlah hari (transport)</label>
                        <input class="form-control" id="jmlhari" name="ep[qtytrans]" maxlength="3" value="<?php echo $dataEdit->transport_qty ?>">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Notes</label>
                        <?php if (isset($_POST['ep'])): ?>
        <input type="text" id="notes" name="ep[notes]" class="form-control" value="<?php echo $_POST['ep']['notes'] ?>">
                            <?php else: ?>
        <input type="text" id="notes" name="ep[notes]" placeholder="pisahkan dengan ##" class="form-control" value="<?php echo $dataEdit->q_notes ?>">
                        <?php endif ?>
                        
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Email</label>
                        <?php if (isset($_POST['ep'])): ?>
                        <input type="email" name="ep[email]" class="form-control" required maxlength="125" value="<?php echo $_POST['ep']['email'] ?>">
                            <?php else: ?>
                        <input type="email" name="ep[email]" class="form-control" required maxlength="125" value="<?php echo $dataEdit->email ?>">
                        <?php endif ?>
                        
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Email CC</label>
                        <?php if (isset($_POST['ep'])): ?>
                        <input type="text" name="ep[emailCC]" class="form-control" maxlength="400" value="<?php echo $_POST['ep']['emailCC'] ?>">
                            <?php else: ?>
                        <input type="text" name="ep[emailCC]" class="form-control" maxlength="400" value="<?php echo @$dataEdit->email_cc ?>">
                        <?php endif ?>
                        
                    </div>
                </div>
            </div>

            <?php $myFile = explode(' # ', $dataEdit->attachments) ?>

            <h5><strong>Attachments</strong></h5>
            <div class="row">
                <div class="col-sm-8">
                <?php 
                $files = scandir('attachments/');
                for ($i=0; $i < count($files); $i++) { 
                    if ($files[$i]!='.'&&$files[$i]!='..') {
                         
                        if (in_array('../attachments/'.$files[$i], $myFile)) {
                            echo "<div class='checkbox'><label><input type='checkbox' name='ep[lampiran][]' value='../attachments/".$files[$i]."' checked>".
                                            $files[$i].
                                            "</label></div>";
                                }else{
                                    echo "<div class='checkbox'><label><input type='checkbox' name='ep[lampiran][]' value='../attachments/".$files[$i]."'>".
                                            $files[$i].
                                            "</label></div>";
                                }
                                    

                             
                    }
                                
                }
                ?>
                </div>
            </div>
            <br>

            
            <div class="row">
                <div class="col-md-10">
                    <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>
                </div>
            </div>

        </form>

        <?php else: ?>

        <form action="" method="post">
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Nama Perusahaan</label>
                        <?php if (isset($_POST['ip'])): ?>
                        <input type="text" name="ip[np]" class="form-control" required maxlength="120" value="<?php echo $_POST['ip']['np'] ?>">
                            <?php else: ?>
                        <input type="text" name="ip[np]" class="form-control" required maxlength="120">
                        <?php endif ?>
                        
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Nama Personal</label>
                        <?php if (isset($_POST['ip'])): ?>
                        <input type="text" name="ip[namakontak]" class="form-control" required maxlength="40" value="<?php echo $_POST['ip']['namakontak'] ?>">
                            <?php else: ?>
                        <input type="text" name="ip[namakontak]" class="form-control" required maxlength="40">
                        <?php endif ?>
                        
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Subject</label>
                        <?php if (isset($_POST['ip'])): ?>
                            <input type="text" name="ip[subject]" class="form-control" required maxlength="75" value="<?php echo $_POST['ip']['subject'] ?>">
                            <?php else: ?>
                            <input type="text" name="ip[subject]" class="form-control" required maxlength="75">
                        <?php endif ?>
                        
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Judul Penawaran (PDF)</label>
                        <?php if (isset($_POST['ip'])): ?>
                            <input type="text" name="ip[quotTitle]" class="form-control" required maxlength="100" value="<?php echo $_POST['ip']['quotTitle'] ?>">
                            <?php else: ?>
                                <input type="text" name="ip[quotTitle]" class="form-control" required maxlength="100" value="Best Quotation Jasa Training ACCURATE Accounting Software">
                        <?php endif ?>
                        
                    </div>
                </div>
            </div>

            <?php $dataPaket = json_decode($pendaftar->fetchPaketTraining()); ?>
                <div class="row">
                    <div class="col-md-10">
                        <label><a href="input-paket">Nama Paket</a></label>
                        <select class="form-control" name="ip[paket]" id="nitem">
                        <?php  
                            for ($i=0; $i < count($dataPaket); $i++) { 
                                echo "<option value='".$dataPaket[$i]->id."'>".$dataPaket[$i]->nama_item." -- ".$dataPaket[$i]->price_for." -- ".number_format($dataPaket[$i]->harga)."</value>";    
                            }
                        ?>
                        </select>
                    </div>
                </div>
            <br>

            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Qty</label>
                        <?php if (isset($_POST['ip']['myqty'])): ?>
                                <input class="form-control" id="jmlhari" name="ip[myqty]" maxlength="3" value="<?php echo $_POST['ip']['myqty'] ?>">
                            <?php else: ?> 
                                <input class="form-control" id="jmlhari" name="ip[myqty]" maxlength="3" value="1">   
                        <?php endif ?>
                        
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Biaya transport</label>
                        <?php if (isset($_POST['ip'])): ?>
        <input type="text" id="biayaTransport" name="ip[biayatrans]" class="form-control" value="0" required maxlength="9" value="<?php echo $_POST['ip']['biayatrans'] ?>">
                            <?php else: ?>
        <input type="text" id="biayaTransport" name="ip[biayatrans]" class="form-control" value="0" required maxlength="9">
                        <?php endif ?>
                        
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Jumlah hari (transport)</label>
                        <?php if (isset($_POST['ip']['qtytrans'])): ?>
                                <input class="form-control" id="jmlharitrans" name="ip[qtytrans]" maxlength="3" value="<?php echo $_POST['ip']['qtytrans'] ?>">
                            <?php else: ?>
                                <input class="form-control" id="jmlharitrans" name="ip[qtytrans]" maxlength="3" value="1">
                        <?php endif ?>
                        
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Notes</label>
                        <?php if (isset($_POST['ip'])): ?>
        <input type="text" id="notes" name="ip[notes]" class="form-control" placeholder="pisahkan dengan ##" value="<?php echo $_POST['ip']['notes'] ?>">
                            <?php else: ?>
        <input type="text" id="notes" name="ip[notes]" placeholder="pisahkan dengan##" class="form-control">
                        <?php endif ?>
                        
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Email</label>
                        <?php if (isset($_POST['ip'])): ?>
                        <input type="email" name="ip[email]" class="form-control" required maxlength="125" value="<?php echo $_POST['ip']['email'] ?>">
                            <?php else: ?>
                        <input type="email" name="ip[email]" class="form-control" required maxlength="125">
                        <?php endif ?>
                        
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Email CC</label>
                        <?php if (isset($_POST['ip'])): ?>
                        <input type="text" name="ip[emailCC]" class="form-control" maxlength="400" value="<?php echo $_POST['ip']['emailCC'] ?>">
                            <?php else: ?>
                        <input type="text" name="ip[emailCC]" class="form-control" maxlength="400">
                        <?php endif ?>
                        
                    </div>
                </div>
            </div>


            <h5><strong>Attachments</strong></h5>
            <div class="row">
                <div class="col-sm-8">
                <?php 
                $files = scandir('attachments/');
                for ($i=0; $i < count($files); $i++) { 
                    if ($files[$i]!='.'&&$files[$i]!='..') {
                                
                    echo "<div class='checkbox'><label><input type='checkbox' name='ip[lampiran][]' value='../attachments/".$files[$i]."'>".
                                    $files[$i].
                                    "</label></div>";              
                            }
                        }
                ?>
                </div>
            </div>
            <br>

            
            <div class="row">
                <div class="col-md-10">
                    <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>
                </div>
            </div>

        </form>

    <?php endif ?>

</div>



  <script type="text/javascript" src=<?php echo "'".getBaseUrl()."js/jquery-ui/jquery-ui.js'"; ?>></script>
  <script type="text/javascript" src=<?php echo "'".getBaseUrl()."assets/jqnumber/jquery.number.js'"; ?>></script>

  <script type="text/javascript">
    $(document).ready(function(){
        $('#biayaTransport').number( true, 0 );
    });
    $('#nitem').change(function(){
        $.ajax({
              type: "GET",
              url: "reverse",
              data: {
                'npkt':$('#nitem').val()
                },
              cache: false,
              success: function(data){
                 $('#jmlhari').val($(data).filter('#paketHari').html());
              }
            }); //end ajax      
});
  </script>   
</body>
</html> 