<?php
session_start();
if (!isset($_SESSION['admin'])){
    //echo $_SESSION['admin']['tipe'];
    header('Location:index.php');
}

// include_once '../model/page.php';
// include_once '../model/Petugas.php';
// $ctrl_page = new Page();
// $ctrlPetugas = new ControlPetugas();
// $usernames = $ctrlPetugas->fetchUsernameAndName();


?>
<!DOCTYPE html>
<html>


<?php
if (isset($_GET['ac'])&&$_GET['ac']=='map') {
    $judul='Tabel Map Absensi';
}elseif(isset($_GET['ac'])&&$_GET['ac']=='srclm'){
    $judul='Data Laporan Trainer';
}elseif(isset($_GET['ac'])&&$_GET['ac']=='nppi'){
    $judul='Data Paket training';
}elseif(isset($_GET['ac'])&&$_GET['ac']=='lfm'){
    $judul='Data Laporan Follow-Up';
}elseif (isset($_GET['ac'])&&$_GET['ac']=='sotd'){
    $judul="Data Subscriber";
}else{
    $judul='Request Absen';
}

$page='dataOrderTraining';
include_once 'header.php';

?>

<body>
<?php
//echo $_SESSION['admin']['tipe'];
?>


<?php
include_once 'sidebar.php';
include_once 'top-nav.php';
include_once 'component/table.php';
?>

<?php




?>



<div class="container" id="isi">

<?php
    //post dari request absen
    if (isset($_GET['v'])&&isset($_POST['perusahaan'])) {
        if (!empty($_GET['v'])&&!empty($_POST['perusahaan'])){
            include_once '../model/Petugas.php';
            $petugas = new ControlPetugas();

            $myObj = array(
                'namaPerusahaan' => $_POST['perusahaan'],
                'idAbsen'=>$_GET['v']
            );
            $myObj = (object) $myObj;
            echo $petugas->updateNamaPerusahaan($myObj);
            if ($_SESSION['admin']['tipe']=='marketing'){
                echo "<script>location.replace('index');</script>";
            }else{
                echo "<script>location.replace('".basename(__FILE__, '.php')."?ac=request');</script>";
            }
        }
    } // end post dari request absen
?>



    <div class="row">
        <div class="col-md-6 col-sm-6">
            <?php if (isset($_GET['ac'])&&$_GET['ac']=='dtrequest'): ?>
                <h2>Data Request Absensi</h2>
            <?php elseif(isset($_GET['ac'])&&$_GET['ac']=='request'): ?>
                <h2>Data Request Absensi <?php echo $_SESSION['admin']['nama'] ?></h2>
            <?php elseif(isset($_GET['ac'])&&$_GET['ac']=='cgatt'&&isset($_GET['tanggal'])): ?>
                <h2>Daftar agenda/perusahaan tanggal <?php echo $_GET['tanggal'] ?></h2>
            <?php elseif(isset($_GET['ac'])&&$_GET['ac']=='map'): ?>
                <h2>Data Map Absensi</h2>
            <?php elseif(isset($_GET['ac'])&&$_GET['ac']=='srclm'): ?>
                <h2>Data Laporan Harian</h2>
            <?php elseif(isset($_GET['ac'])&&$_GET['ac']=='lfm'): ?>
                <h2>Data FollowUp Marketing</h2>
            <?php elseif (isset($_GET['ac'])&&$_GET['ac']=='nppi'): ?>
                <h2>Data Paket Training</h2>
            <?php elseif (isset($_GET['ac'])&&$_GET['ac']=='sotd'): ?>
                <h2>Data subscriber</h2>
            <?php endif ?>
        </div>

        <div class="col-md-6 col-sm-6" style="padding-top:30px;text-align:right;">
            <?php if (isset($_GET['ac'])&&$_GET['ac']=='lfm'): ?>
                <a class="btn btn-success" style="text-align:right" href="laporan-followup" >Tambah <span class="glyphicon glyphicon-plus"></span></a>
            <?php endif ?>
            <button class="btn btn-primary" style="text-align:right" id="cari-data" href="#modal-cari" data-toggle="modal" >Cari Data <span class="glyphicon glyphicon-search"></span></button>
        </div>
    </div>
    <br>
    <?php if (isset($_GET['ac'])&&$_GET['ac']=='cgatt'&&isset($_GET['tanggal'])&&isset($_GET['v'])&&!empty($_GET['v'])): ?>
        <?php
            include_once '../model/Petugas.php';
            $petugas = new ControlPetugas();
            $absen = $petugas->getRequestPerusahaan($_GET['v']);
        ?>
        <div class="row">
            <div class="col-md-12">
                <table>
                    <tbody>
                        <tr>
                            <td><strong>Nama Request</strong></td>
                            <td style="padding-left:10px">:</td>
                            <td style="padding-left:10px"><b><?php echo $absen->nama_perusahaan ?></b></td>
                        </tr>
                        <tr>
                            <td><strong>Nama Staff</strong></td>
                            <td style="padding-left:10px">:</td>
                            <td style="padding-left:10px"><b><?php echo $absen->nama ?></b></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    <?php endif ?>

    <div class="row" style="overflow:auto">
        <div class="col-md-12">
            <?php if (isset($_GET['ac'])&&$_GET['ac']=='dtrequest'&&($_SESSION['admin']['tipe']=='admin'||$_SESSION['admin']['tipe']=='marketing')): ?>
                <?php
                    include_once '../model/Petugas.php';
                    $ctrPetugas = new ControlPetugas();
                    $myDataz = json_decode($ctrPetugas->dataRequest('forEdit'));
                ?>
                <?php tabel($_GET['ac'],$myDataz) ?>

            <?php elseif(isset($_GET['ac'])&&$_GET['ac']=='request'): ?>
                <?php
                    include_once '../model/Petugas.php';
                    $ctrPetugas = new ControlPetugas();
                    $myDataz = json_decode($ctrPetugas->getDataRequestByUsername($_SESSION['admin']['username']));
                    tabel($_GET['ac'],$myDataz);
                ?>

            <?php elseif(isset($_GET['ac'])&&$_GET['ac']=='cgatt'&&isset($_GET['tanggal'])&&isset($_GET['v'])&&!empty($_GET['v'])): ?>
                <?php

                    $datas = json_decode($petugas->getDataPendaftarToday($_GET['tanggal']));
                    tabel($_GET['ac'],$datas);
                ?>
            <?php elseif(isset($_GET['ac'])&&$_GET['ac']=='map'&&isset($_GET['np'])&&!empty($_GET['np'])): ?>
                <?php
                    if (!isset($_SESSION['admin'])||($_SESSION['admin']['tipe']!='admin'&&$_SESSION['admin']['tipe']!='staff')) {
                        echo "<script>alert('Anda tidak memiliki akses untuk halaman ini!');</script>";
                        echo "<script>location.replace('index');</script>";
                    }
                    include_once '../model/Petugas.php';
                    $petugas = new ControlPetugas();
                    $datas = $petugas->getMapData($_GET['np']);
                    tabel($_GET['ac'],$datas);
                ?>
            <?php elseif(isset($_GET['ac'])&&$_GET['ac']=='srclm'): ?>
                <?php
                    if (!isset($_SESSION['admin'])||($_SESSION['admin']['tipe']!='admin'&&$_SESSION['admin']['tipe']!='marketing')) {
                        echo "<script>alert('Anda tidak memiliki akses untuk halaman ini!');</script>";
                        echo "<script>location.replace('index');</script>";
                    }
                    if (isset($_GET['v'])) {
                        $mySrc = array(
                            'nama' => $_GET['v']['sn'],
                            'usaha' => $_GET['v']['np'],
                            'kk' => $_GET['v']['kk']
                        );
                        $mySrc = (object) $mySrc;
                        include_once '../model/Laporan.php';
                        $laporan = new ControlLaporan();
                        $datas = $laporan->searchLaporanForMarketing($mySrc);
                        tabel($_GET['ac'],$datas);
                    }else{
                        echo "Silahkan cari data terlebih dahulu.";
                    }

                ?>
            <?php elseif(isset($_GET['ac'])&&$_GET['ac']=='lfm'): ?>
                <?php
                if (!isset($_SESSION['admin'])||($_SESSION['admin']['tipe']!='admin'&&$_SESSION['admin']['tipe']!='marketing')) {
                        echo "<script>alert('Anda tidak memiliki akses untuk halaman ini!');</script>";
                        echo "<script>location.replace('index');</script>";
                }
                include_once '../model/Petugas.php';
                $laporan = new ControlPetugas();
                $datas = $laporan->fetchAllFollowUp();
                tabel($_GET['ac'],$datas);

                ?>

            <?php elseif (isset($_GET['ac'])&&$_GET['ac']=='nppi'): ?>
              <?php
              if (!isset($_SESSION['admin'])||($_SESSION['admin']['tipe']!='admin'&&$_SESSION['admin']['tipe']!='marketing')) {
                      echo "<script>alert('Anda tidak memiliki akses untuk halaman ini!');</script>";
                      echo "<script>location.replace('index');</script>";
              }
              // echo "oke";
              include_once '../model/Pendaftar.php';
              $laporan = new Pendaftar();
              $datas = $laporan->fetchDataPaketTraining();
              tabel($_GET['ac'],$datas);
              ?>
            <?php elseif (isset($_GET['ac'])&&$_GET['ac']=='sotd'): ?>
            <?php
              if (!isset($_SESSION['admin'])||($_SESSION['admin']['tipe']!='admin'&&$_SESSION['admin']['tipe']!='marketing')) {
                      echo "<script>alert('Anda tidak memiliki akses untuk halaman ini!');</script>";
                      echo "<script>location.replace('index');</script>";
              }
              // echo "oke";
              include_once '../model/Pendaftar.php';
              $laporan = new Pendaftar();
              $datas = $laporan->getSubcribers(date('Y-m-d'));
              tabel($_GET['ac'],$datas);
            ?>

            <?php endif ?>
        </div>
    </div>


</div>

<?php
if (isset($_GET['ac'])&&$_GET['ac']=='map') {
    include_once 'component/modal.php';
    modal($_GET['ac']);
}elseif (isset($_GET['ac'])&&$_GET['ac']=='srclm') {
    include_once 'component/modal.php';
    modal($_GET['ac']);
}elseif(isset($_GET['ac'])&&$_GET['ac']=='lfm'){
    include_once 'component/modal.php';
    modal($_GET['ac']);
}

?>


<script type="text/javascript" src=<?php echo "'".getBaseUrl()."js/jquery-datepicker/jquery-ui.js'"; ?>></script>
<script type="text/javascript">
    $(document).ready(function(){

        //alert('okeh');

      $('#tanggal-awal').datepicker({
        dateFormat: 'yy-mm-dd'
      });



      $('#tanggal-akhir').datepicker({
        dateFormat: 'yy-mm-dd'
      });

    });

</script>
</body>
</html>
