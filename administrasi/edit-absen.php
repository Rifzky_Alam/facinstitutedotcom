<?php 
session_start();
if (!isset($_SESSION['admin']['username'])&&$_SESSION['admin']['tipe']!='admin') {
    header('location: https://fac-institute.com/login/');
}
?>
<!DOCTYPE html>
<html>



<?php 
$judul='Edit Absen - FAC Institute';
$page = 'editLaporan';
include_once 'header.php'; 
?>


<body>



<?php 
include_once 'sidebar.php';
include_once 'top-nav.php';
include_once '../model/Pendaftar.php';
include_once '../model/Petugas.php';
include_once '../model/Laporan.php';

$control = new Pendaftar();
$controlPetugas = new ControlPetugas();
$controlLaporan = new ControlLaporan();

$petugases = $controlPetugas->fetchUsernameAndName();

$dataComboBox = $control->dataCombobox();


?>


<div class="container" style="margin-top:40px;margin-bottom:40px;">


<?php if (isset($_GET['app'])&&$_GET['app']=='add'): ?>
    
    <div class="row" style="margin-bottom:10px">
        <div class="form-group col-md-4">
            <label>Pilih Nama Pegawai</label>
            <select id="namaPegawai" name="inputNamaPegawai" class="form-control">
                <option value="" selected>-- Pilih Pegawai --</option>
                <?php 
                for ($i=0; $i < count($petugases) ; $i++) { 
                    echo "<option value='".$petugases[$i]->getUsername()."'>";
                    echo $petugases[$i]->getNama();
                    echo "</option>"; 
                }
                ?>
            </select>
        </div>
    </div>

    <div class='row'>
        <div class='form-group col-md-4'>
            <label>Pilih Tanggal Absen</label>
            <input id="tanggal-lapor" name="tanggalInput" class="form-control" placeholder="pilih tanggal" />
        </div>
    </div>
    
    <div class="row" style="margin-top:15px">
        <div class="col-md-12">
         <label>Nama Client</label>
         <select name="deskripsi[client]" class='form-control' id='cbNamaClient'>
            <option value=''>-- Pilih nama client --</option>
            <option value='f4e745e1015af1c15ebcb70b77f1426c'>FAC-Institute</option>
            <option value='d5eb8c090d191587bb8a4b875bae4fed'>FAC-Remote</option>
         </select>
            <br>
            <input name="deskripsi[request]" placeholder="Isi Nama Perusahaan, contoh: PT Asuransi Jasa Raharja" id='autoCompleteTXT' class="form-control" />               
        </div>
    </div>
    <br>
    <div class='row'>
        <div class='form-group col-md-10'>
            <label>Absen Masuk</label>
            <input class='form-control' type='text' id='inputAbsenMasuk' placeholder='Format: 09:00:00'>
        </div>
    </div>

    <div class='row'>
        <div class='form-group col-md-10'>
            <label>Absen Keluar</label>
            <input class='form-control' type='text' id='inputAbsenKeluar' placeholder='Format: 17:00:00'>
        </div>
    </div>

    <div class='row'>
        <div class="col-md-4">
            <button class="btn btn-primary" id='inputSubmit'>Input Absen</button>
        </div>
    </div>

    <?php else: ?>

    <form action=<?php echo "'".basename(__FILE__, '.php')."'"; ?> method="POST">
    <div class="row" style="margin-bottom:10px">
        
        <div class="col-md-4">
            <select id="namaPegawai" name="pegawai" class="form-control">
                <option value="" selected>-- Pilih Pegawai --</option>
                <?php 
                for ($i=0; $i < count($petugases) ; $i++) { 
                    echo "<option value='".$petugases[$i]->getUsername()."'>";
                    echo $petugases[$i]->getNama();
                    echo "</option>"; 
                }
                ?>
            </select>
        </div>

        <div class="col-md-4">
            <input id="tanggal-lapor" name="tanggal-lapor" class="form-control" placeholder="pilih tanggal" />
        </div>

        <div class="col-md-4">
            <button class="btn btn-primary">Cari Data</button>
        </div>

    </form>

    </div>

    <hr>

    <?php 

        if (isset($_POST['pegawai'],$_POST['tanggal-lapor'])){
            $dataAbsen =  $controlPetugas->selectEditAbsen($_POST['pegawai'],$_POST['tanggal-lapor']);
            $adaLaporan=$controlLaporan->jumlahData('username',$_POST['pegawai'],$_POST['tanggal-lapor']);
            //echo $adaLaporan->jumlah;
    ?>

<?php 
function getNamaPetugas($value){
    if ($value==''||empty($value)) {
        return "<a href='".basename(__FILE__, '.php')."?app=add'>Data pegawai tidak ada, klik disini untuk input manual</a>";
    }else{
        return $value;
    }
}


?>

    <!--edit page-->
    <div class="row">
        
        <div class="row">
            <div class="col-md-12">
                <label>Nama Pegawai :</label>
                <span><?php echo getNamaPetugas(@$dataAbsen->nama);?></span>
                <input value=<?php echo "'".$dataAbsen->id."'" ?> style='display:none' id="aidiAbsen">
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <label>Edit Untuk Tanggal :</label>
                <span id="editTanggal"><?php echo @$_POST['tanggal-lapor'];?></span>
            </div>
        </div>      
            <?php $comboBoxData = json_decode($controlPetugas->getDataPendaftarToday($_POST['tanggal-lapor'])); ?>
            <div class="row" style="margin-top:15px">
               <div class="col-md-12">
                <div class='form-group'>
                    <label>Nama Klien</label>
                <select name="deskripsi[client]" class='form-control' id='editNamaKlien'>
                    <option value=<?php echo "'".$dataAbsen->nama_perusahaan."'"; ?>><?php echo $dataAbsen->perusahaannya; ?></option>
                    <option value='f4e745e1015af1c15ebcb70b77f1426c'>FAC-Institute</option>
                    <option value='d5eb8c090d191587bb8a4b875bae4fed'>FAC-Remote</option>
                    <?php for ($i=0; $i < count($comboBoxData) ; $i++) { 
                    ?>
                    <option value=<?php echo $comboBoxData[$i]->id; ?>><?php echo $comboBoxData[$i]->perusahaan; ?></option>

                    <?php } ?>
                </select>
                </div>
              </div>
            </div>



        <input id="editUsername" style="display:none" value=<?php echo "'".@$dataAbsen->username."'";?>/>


        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label for="jam-masuk">Jam Masuk</label>
                    <input class="form-control" id="editJamMasuk" value=<?php echo "'".$dataAbsen->absen_masuk."'";?> />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label for="jam-pulang">Jam Pulang</label>
                    <input class="form-control" id="editJamPulang" value=<?php echo "'".$dataAbsen->absen_pulang."'";?> />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label for="jam-pulang">Status laporan </label>

                    <?php if (intval($adaLaporan->jumlah)==1): ?>
                        <div class='radio'>
                            <label>
                            <input type='radio' name='statuslaporan' value='aktif' id='laporan-active' checked/>
                                Sistem sudah membuat laporan
                            </label>
                        </div>
                        <div class='radio'>
                            <label>
                            <input type='radio' value='inactive' name='statuslaporan' id='laporan-inactive' disabled="disabled" />
                                Buatkan laporan untuk tanggal ini.
                            </label>
                        </div>
                    <?php else: ?>
                        <div class='radio'>
                            <label>
                                <input type='radio' name='statuslaporan' value='aktif' id='laporan-active'/>Jangan dulu buatkan laporan
                            </label>
                        </div>
                        <div class='radio'>
                            <label>
                                <input type='radio' name='statuslaporan' value='inactive' id='laporan-inactive' checked />Buatkan laporan untuk tanggal ini.
                            </label>
                        </div>                    
                    <?php endif ?>
                    
                    
                </div>
            </div>
        </div>

        <div class="row" style="margin-top:10px">
            <div class="col-md-10">
                <div class="form-group">
                  <button class="btn btn-primary" style="width:100%" id="btnEditSubmit" name="submit">Submit Data</button>
                </div>
            </div>
        </div>

    </div>

    <!--end edit page-->


    <?php

        }else{

    ?>


    <div class="row">
       <div class="col-md-10">
        <p>Silahkan pilih Tanggal dan petugas terlebih dahulu</p>
        <a href=<?php echo "'".basename(__FILE__, '.php')."?app=add'"; ?>>Lupa Absen Pada Tanggal Tertentu?, Klik Disini</a>
       </div>
    </div>

<?php } ?>

<?php endif ?>


</div>

    <script type="text/javascript" src=<?php echo "'".getBaseUrl()."js/jquery.js'"; ?>></script>
    <script type="text/javascript" src=<?php echo "'".getBaseUrl()."js/jquery-ui/jquery-ui.js'"; ?>></script>
    
    <?php if (isset($_GET['app'])&&$_GET['app']=='add'): ?>
        <?php 
            include_once '../model/Pendaftar.php'; 
            $ctrlPendaftar = new Pendaftar();
            $dataAutoComplete = json_decode($ctrlPendaftar->autoCompleteNamaPerusahaan());
        ?>

        <script type="text/javascript">

               var availableTags = [
                <?php for ($i=0; $i < count($dataAutoComplete) ; $i++) { 
                    if ($i<count($dataAutoComplete)-1) {
                        echo "'".$dataAutoComplete[$i]->nama_perusahaan."',";
                    } else{
                        echo "'".$dataAutoComplete[$i]->nama_perusahaan."'";
                    };
                    
                } ?>
                ];
                $( "#autoCompleteTXT" ).autocomplete({
                  source: availableTags
                });


                $('#inputSubmit').click(function(){
                    var inputTanggal = $('#tanggal-lapor').val();
                    var username = $('#namaPegawai').val();
                    var namaPerusahaan = $('#cbNamaClient').val();
                    if (namaPerusahaan=='request') {
                        namaPerusahaan=$('#autoCompleteTXT').val();
                    };
                    var inputJamMasuk = $('#inputAbsenMasuk').val();
                    var inputJamPulang = $('#inputAbsenKeluar').val();
                    var lokasiMasuk = <?php echo "'Edited By: ". $_SESSION['admin']['nama'] ."';"; ?>
                    var lokasiPulang = <?php echo "'Edited By: ". $_SESSION['admin']['nama'] ."';"; ?>
                    var editor = <?php echo "'". $_SESSION['admin']['nama'] ."';"; ?>
                    if (username=='') {
                        alert('Harap isi nama pegawai terlebih dahulu');
                        return;
                    }else if(namaPerusahaan==''){
                        alert('Harap isi Nama Perusahaan terlebih dahulu');
                        return;
                    }else if(inputJamMasuk==''){    
                        alert('Jam masuk tidak boleh kosong');
                        return;
                    }else if(namaPerusahaan==''){
                        alert('Jam keluar tidak boleh kosong');
                        return;
                    };
                 var jsonDatas = {
                    'username':username,
                    'tanggal':inputTanggal,
                    'absenMasuk':inputJamMasuk,
                    'absenPulang':inputJamPulang,
                    'namaPerusahaan':namaPerusahaan,
                    'lokasiMasuk': lokasiMasuk,
                    'lokasiPulang': lokasiPulang,
                    'editor':editor
                }
                    //alert(inputTanggal);
                    //alert(username);
                    //alert(namaPerusahaan);
                    //alert(inputJamMasuk);
                    //alert(inputJamPulang);

                    $.ajax({
                      type: "POST",
                      url: "reverse",
                      data: {'inputAbsen':jsonDatas},
                      cache: false,
                      success: function(data){
                         alert(data);
                         window.location.replace(<?php echo "'".basename(__FILE__, '.php')."'"; ?>);
                      }
                    }); //end ajax

                });


        </script>

    <?php endif ?>

    <script type="text/javascript">
        

        

        $(document).ready(function(){

          $('#tanggal-lapor').datepicker({
            dateFormat: 'yy-mm-dd'
          });

          $('#tanggal-lapor').focusout(function(){
            // alert($('#tanggal-lapor').val());
            setTimeout(getPerusahaan,1000);
          });


          function getPerusahaan() {
            $.ajax({
              type: "GET",
              url: "reverse",
              data: {
                'tescb':$('#tanggal-lapor').val()
                },
              cache: false,
              success: function(data){
                $('#cbNamaClient').empty();
                $('#cbNamaClient').append(data);
                 //alert(data);
              }
            }); //end ajax
          }

            $('#btnEditSubmit').click(function(){
                
                var tanggal = $('#editTanggal').html();
                var username = $('#editUsername').val();
                var namaPerusahaan = $('#editNamaKlien').val();
                if (namaPerusahaan=='request') {
                    namaPerusahaan=$('#autoCompleteTXT').val();
                };
                var idAbsenz= $('#aidiAbsen').val();

                var editJamMasuk = $('#editJamMasuk').val();
                var editJamPulang = $('#editJamPulang').val();
                var editStatusLaporan = $('input[name=statuslaporan]:checked').val();
                if (username=='') {
                        alert('Harap isi nama pegawai terlebih dahulu');
                        return;
                    }else if(namaPerusahaan==''){
                        alert('Harap isi Nama Perusahaan terlebih dahulu');
                        return;
                    }else if(editJamMasuk==''){    
                        alert('Jam masuk tidak boleh kosong');
                        return;
                    }else if(editJamPulang==''){
                        alert('Jam keluar tidak boleh kosong');
                        return;
                    };


                var jsonDatas = {
                    'id':idAbsenz,
                    'username':username,
                    'tanggal':tanggal,
                    'absenMasuk':editJamMasuk,
                    'absenPulang':editJamPulang,
                    'namaPerusahaan':namaPerusahaan,
                    'statusLaporan':editStatusLaporan,
                }

                //var dataString = JSON.stringify(jsonData);
                $("#btnEditSubmit").attr("disabled", true);
                $("#btnEditSubmit").html('Harap Tunggu ...');
                
                $.ajax({
                  type: "POST",
                  url: "reverse",
                  data: {'updateAbsen':jsonDatas},
                  cache: false,
                  success: function(data){
                     alert(data);
                     window.location.replace(<?php echo "'".basename(__FILE__, '.php')."'"; ?>);
                  }
                }); //end ajax
                
            });

        $('#autoCompleteTXT').css('display','none');
        $('#cbNamaClient').change(function(){
            var myValue = $(this).val();
            if (myValue=='request'){
                $('#autoCompleteTXT').css('display','');            
            }else{
                $('#autoCompleteTXT').css('display','none');            
            };
        });

        });

    </script>
</body>
</html> 


