<?php 
session_start();
if (!isset($_SESSION['admin'])) {
    header('Location:../login/index.php');
}
?>




<!DOCTYPE html>
<html>

<?php 
$judul='Form Jadwal dan Agenda Training';
$page = 'dataPegawai';
include_once 'header.php'; 

?>

<body>

<?php 

include_once 'sidebar.php';
include_once 'top-nav.php';

 ?>


<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3>Agenda dan Jadwal Training Accurate</h3>
	</div>
	<?php 
		
		include_once '../model/Pendaftar.php'; 
        include_once '../model/page.php';
        $myPage = new Page();
		$pendaftar = new Pendaftar();
		
        //echo $myRecord->password;

        if (isset($_GET['idp'])&&!empty($_GET['idp'])) {
            $by = array('cond' => 'jadwal','id'=> $_GET['idp']);
            $by = (object) $by;
            $datas = json_decode($pendaftar->fetchDataTraining((object) $by));
        

        	if (isset($_POST['in'])) {
                if ($_POST['in']['attachments']==''||empty($_POST['in']['attachments'])) {
                    $att='';
                }else{
                    $att=implode(" # ", $_POST['in']['attachments']);
                }
                
                $inputData = array(
                    'id' => md5(date('Y-m-d').$_GET['idp']),
                    'idpendaftar'=> $_GET['idp'],
                    'tanggal'=> implode(" # ", $_POST['in']['tanggalPelaksanaan']),
                    'waktu'=> $_POST['in']['waktu'],
                    'tempat'=> $_POST['in']['tempat'],
                    'alamat'=> $_POST['in']['alamat'],
                    'cc'=> $_POST['in']['cc'],
                    'attachments'=> $att
                );
                $inputData = (object) $inputData;

                if ($pendaftar->addJadwalbaru($inputData)) {
                     echo "<script>alert('Data berhasil disimpan!');</script>";
                     echo "<script>location.replace('".basename(__FILE__, '.php')."?step=2&kd=".$inputData->id."');</script>";
                }else{
                    echo "<script>alert('Data gagal disimpan!');</script>";
                }



                /*
        		echo "<div class='row'>";
        		echo "ID: ".$inputData->id."<br>";
                echo "ID Pendaftar: ".$inputData->idpendaftar."<br>";
                echo "Tanggal: ".$inputData->tanggal."<br>";
        		echo "Waktu: ".$inputData->waktu."<br>";
        		echo "Tempat: ".$inputData->tempat."<br>";
        		echo "Alamat: ".$inputData->alamat."<br>";
        		echo "CC: ".$inputData->cc."<br>";
        		echo "Attachments: ".$inputData->attachments."<br>";
        		echo "</div>";*/
                
        	}
	?>
    <form id='my-form' action='' method='post'>

    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <th class="tengah">Nama Perusahaan</th>
                        <th class="tengah">Nama Kontak</th>
                        <th class="tengah">Tanggal Transaksi</th>
                        <th class="tengah">Jenis Usaha</th>
                        <th class="tengah">Nama Paket</th>
                        <th class="tengah">Qty</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tengah"><?php echo $datas[0]->nama_perusahaan ?></td>
                        <td class="tengah"><?php echo $datas[0]->nama_kontak ?></td>
                        <td class="tengah"><?php echo $datas[0]->tanggal_daftar ?></td>
                        <td class="tengah"><?php echo $datas[0]->usaha ?></td>
                        <td class="tengah"><?php echo $datas[0]->nama_item ?></td>
                        <td class="tengah"><?php echo $datas[0]->qty ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row" id='jajal'>
        <div class="col-md-10">
            <label for="tgl-pelaksanaan">Tanggal</label>
            <div class="form-inline" >
                <input type="text" id="tgl-pelaksanaan" name='in[tanggalPelaksanaan][]' class="form-control tglPelaksanaan" placeholder="klik disini untuk memilih tanggal"/>   
                <span class='btn btn-sm btn-primary' id='tambahTombol'><i class="glyphicon glyphicon-plus"></i></span>
            </div>
        </div>
    </div>
    <br>

    <div class='row'>
        <div class='form-group col-sm-8'>
            <label>Waktu</label>
            <input type="text" name="in[waktu]" class="form-control" value="09:00-16:00" >
        </div>
    </div>

    <div class='row'>
        <div class='form-group col-sm-8'>
            <label>Tempat</label>
            <input type='text' class='form-control' name='in[tempat]'>
        </div>
    </div>

    <div class='row'>
        <div class='form-group col-sm-8'>
            <label>Alamat</label>
            <textarea class="form-control" name="in[alamat]"></textarea>
        </div>
    </div>

    <div class='row'>
        <div class='form-group col-sm-8'>
            <label>CC Email</label>
            <input type='text' class='form-control' id='txt-passBaru' name='in[cc]' placeholder="Pisahkan dengan spasi">
        </div>
    </div>

    <h5>Attachments</h5>
    <div class="row">
        <div class="col-sm-8">
        <?php 
        $files = scandir('attachments/');
        for ($i=0; $i < count($files); $i++) { 
            if ($files[$i]!='.'&&$files[$i]!='..') {
                        
            echo "<div class='checkbox'><label><input type='checkbox' name='in[attachments][]' value='".$files[$i]."'>".
                            $files[$i].
                            "</label></div>";
                        
                    }
                }
        ?>


        </div>
    </div>


    <div class='row'>
        <div class='form-inline col-sm-8'>
            <button class='btn btn-lg btn-primary' style='width:100%'>Selanjutnya</button>
        </div>
    </div>

    </form>

    <?php } ?>

    <?php if (isset($_GET['step'])&&$_GET['step']=='2'&&isset($_GET['kd'])&&!empty($_GET['kd'])): ?>
        <?php $dataz = $pendaftar->fetchDataAgendaByID($_GET['kd']); ?>
     <div class="row" id="tes">
        <div class="form-group col-md-10">
            <label><a href=<?php echo "'".basename(__FILE__, '.php')."?step=3&kd=".$_GET['kd']."'"; ?> target="_blank" >Nama Agenda</a></label>
            <input type="text" name="agenda" class="form-control" id="contoh" />
        </div>
     </div>

     <div class="row">
     <?php 
     if (isset($_POST['in'])) {
        $inz = array(
            'agenda' => json_encode($_POST['in']['agenda']),
            'id' =>  $_GET['kd']
        );
        $inz = (object) $inz;
        if ($pendaftar->editAgenda($inz)) {
            echo "<script>alert('Data berhasil disimpan!');</script>";
            echo "<script>location.replace('daftar-agenda?p=".$_POST['in']['idp']."');</script>";
        }else{
            echo "<script>alert('Data gagal disimpan!');</script>";
        }
           
     }

     ?>
     </div>


     <form action="" method="post">
     <div class="row" id="siap">
        <input type="text" name="in[idp]" value=<?php echo "'".$dataz->id_pendaftar."'"; ?> style="display:none;">
        <ul id="ok">
            
        </ul>
     </div>
     <hr>
    

     <div class="row">
        <div class="form-group col-md-10" id='bisalah'>
            
        </div>
     </div>


     <div class="row">
        <div class="col-md-10">
            <button class="btn btn-primary">Submit</button>
        </div>
     </div>
     </form>

    <?php elseif(isset($_GET['step'])&&$_GET['step']=='3'&&isset($_GET['kd'])&&!empty($_GET['kd'])): ?>
        <?php $desc=$pendaftar->fetchDeskripsiAgenda(); ?>
        <?php 
        if (isset($_POST['ag'])) {
            if ($pendaftar->addDaftarAgenda($_POST['ag']['namaagenda'],$_POST['ag']['kategori'])){
                echo "<script>alert('Data berhasil disimpan!');</script>";
                echo "<script>location.replace('daftar-agenda');</script>";
            }else{
                echo "<script>alert('Data gagal disimpan!');</script>";
            }
        }

        ?>

        <form action="" method="post">
            <div class="row">
                <div class="form-group col-md-10">
                    <label>Nama Agenda</label>
                    <textarea class="form-control" name="ag[namaagenda]"></textarea>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-10">
                    <label>Kategori Agenda</label>
                    <select class="form-control" name="ag[kategori]">
                        <option value="" selected>--Pilih Kategori--</option>
                        <?php 
                            for ($i=0; $i < count($desc); $i++) { 
                                echo "<option value='".$desc[$i]->dak_id."'>".$desc[$i]->nama."</option>";
                            }
                         ?>
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>
                </div>
            </div>
        </form>

    <?php endif ?>



</div>
<script type="text/javascript" src=<?php echo "'".getBaseUrl()."css/datepicker/js/bootstrap-datepicker.js'"; ?>></script>
<?php if (isset($_GET['step'])&&$_GET['step']=='2'&&isset($_GET['kd'])&&!empty($_GET['kd'])): ?>
    <script type="text/javascript">
        $(document).ready(function(){
            //var availableTags = ['Rifzky Alam','Avril Lavigne'];
            //$('#contoh').autocomplete({
                //source: availableTags
            //});
        });

        $('#contoh').keyup(function(){
            if ($('#contoh').val()==''){
                $('.mydata').remove();
            }else{
            $.ajax({
              type: "GET",
              url: "reverse",
              data: {
                'kda':$('#contoh').val()
                },
              cache: false,
              success: function(data){
                $('.mydata').remove();
                 $('#bisalah').append(data);
                 //alert(data);
              }
            }); //end ajax  
            }
            
        });

        function coba(haha) {//haha.value
            $('#ok').append("<li class='ilang' id='li" + haha.id + "'>" + $('#y' + haha.id).text() + " <input type='text' name='in[agenda][]' value='" + haha.value + "' style='display:none;'> <a id='i" + haha.id + "' onclick='removeList(this)' href='#'><span class='glyphicon glyphicon-remove' style='padding-left:10px;'></span></a></li>");
            $('#z' + haha.id).remove();
        }

        function removeList(nama) {
            $('#l' + nama.id).remove();
            //alert('oke');
        }

    </script>

    <?php else: ?>
<script type="text/javascript">
    $(document).ready(function(){
        var apakah = 0;
        $('#cobaa').datepicker({
           format: "yyyy-mm-dd"
        });
    $('#fr-accurateLainnya').hide();


    $('#tambahTombol').click(function(){
    
            var cobayah = $('.tglPelaksanaan').length;
            var iseng = "tambahan" + cobayah;
            if ($('#tambahan').length){

        if (cobayah==2) {
            $('#tambahan').after("<div class='row' id='"+iseng+"'><div class='col-md-10'><div class='form-inline'><input name='in[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"'+iseng+'"'+")'></span></div></div></div>");
        }else{
            var okeh = cobayah-1;
            var iseng = "tambahan" + okeh;
            $("#"+iseng).after("<div class='row' id='tambahan"+cobayah+"'><div class='col-md-10'><div class='form-inline'><input name='in[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"tambahan'+cobayah+'"'+")'></span></div></div></div>");
        };
            //nilai +=1;
         }else{
            $('#jajal').after("<div class='row' id='tambahan'><div class='col-md-10'><div class='form-inline'><input name='in[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"'+"tambahan"+'"'+")'></span></div></div></div>");
        };

        $('.tglPelaksanaan').datepicker({
            format: 'yyyy-mm-dd'
        });
        
    });

        $('.tglPelaksanaan').datepicker({
            format: 'yyyy-mm-dd'
        });

    $('#try').click(function(){
        var cobaz = $("#example-getting-started option:selected").map(function () {return this.value;}).get().join(" # ");
        alert(cobaz);
    });

    })

    function removeElement(id) {
        $('#'+id).remove();
    }
</script>
<?php endif ?>
</body>
</html> 