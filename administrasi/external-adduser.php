<?php 
session_start();

if (!isset($_SESSION['admin'])&&!$_SESSION['admin']['type']=='admin'){
	header('Location:../index.php');
}
$page='dataPegawai'
?>

<?php 

//include_once 'variables.php';

?>

<!DOCTYPE html>

<html>

<head>
	<title>FAC - External User/Add</title>
<?php include_once 'header.php'; ?>

</head>

<body>
	<?php include_once 'sidebar.php'; ?>
	<?php include_once 'top-nav.php'; ?>

	<?php 
	include_once '../model/external.php';
	include_once '../model/Querybuilder.php';
	$db = new QueryBuilder();
	$ex = new External();

	$datacabang = $db->FetchJoin(
		['id_cabang','nama'],
		'cabang AS c',
		array(
			$db->getJoin('INNER','perusahaan AS p','c.id_perusahaan','p.id'))
	);

	// print_r($db->$datacabang);

	if (isset($_POST['in'])) {
		$ex->setUsername($ex->getValue($_POST['in']['username']));
		$ex->setPassword($ex->getValue($_POST['in']['password']));
		$ex->setNama($ex->getValue($_POST['in']['namalengkap']));
		$ex->setEmail($ex->getValue($_POST['in']['email']));
		$ex->setTelepon($ex->getValue($_POST['in']['telepon']));
		$ex->setPerusahaan($_POST['in']['cabang']);
		
		if ($ex->inputPetugas()) {
			echo "<script>alert('data berhasil disimpan!');</script>";
		} else {
			echo "<script>alert('data gagal disimpan!');</script>";
		}
		

	}

	?>
	


	<div class="container" style='margin-bottom:40px;margin-top:40px'>

			<div class='row'>
				<div class='page-header'>					
	            	<h1>Input External User</h1>
				</div>
			</div>
		
			<div class='row'>
				<div class='col-md-12'>
					<form action="" method="post">
					<div class="form-group">
						<label>Nama Lengkap</label>
						<input type="text" name="in[namalengkap]" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Email</label>
						<input type="email" name="in[email]" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Telepon</label>
						<input type="text" name="in[telepon]" class="form-control">
					</div>
					<div class="form-group">
						<label>Username</label>
						<input type="text" name="in[username]" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Password</label>
						<input type="password" name="in[password]" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Cabang</label>
						<select class="form-control" name="in[cabang]" required>
							<option>--Pilih Cabang--</option>
							<?php foreach ($datacabang as $key): ?>
								<option value="<?= $key['id_cabang'] ?>"><?= $key['nama'] ?></option>
							<?php endforeach ?>
						</select>
					</div>
					<button class="btn btn-lg btn-primary" style="width:100%">Submit</button>
					</form>	
				</div>
			</div>

			<script type="text/javascript">
				$('#')
			</script>


		
	</div>

	     
<script type="text/javascript" src=<?php echo "'".getBaseUrl()."css/datepicker/js/bootstrap-datepicker.js'"; ?>></script>
<script type="text/javascript">
	function coba(aidi,myTitle){
		//alert(id);
		document.getElementById("judul-delete").innerHTML=myTitle;
		document.getElementById('konfirm-id').value=aidi;
	}

	$('#tglLahir').datepicker({
		   format: "yyyy-mm-dd"
	});
</script>

</body>

</html>