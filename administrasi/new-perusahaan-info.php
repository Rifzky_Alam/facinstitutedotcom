<?php 
session_start();
include_once 'session/session-class.php';
include_once '../baseurl.php';
include_once '../model/Perusahaan.php';
include_once '../model/Customer.php';
include_once '../model/Transaksi.php';
$session = new Sessionz();
$session->AdminMarketing();

$transaksi = new Transaksi();
$customer = new Customer();
$perusahaan = new Perusahaan();

//declaration
$datatransaksi = array();

$fileinv = '';
if (isset($_GET['p'])&&!empty($_GET['p'])) {
        $usaha = $_GET['p'];
        $datausaha = $perusahaan->selectByID($_GET['p']);

        if ($datausaha->p_status!='1') {
            echo "<script>alert('Data perusahaan non transaksi tidak memiliki akses di halaman ini, silahkan untuk ekspor data terlebih dahulu.');</script>";
            echo "<script>location.replace('".getBaseUrl()."administrasi/new-usaha?id=".$_GET['p']."');</script>";
        }

        $customer->setPerusahaan($usaha);
        $datacustomer = $customer->FetchAll('0','30'); //data customer by perusahaan
        $idtr = '-';
        $itm = array();
        $tgl = array();
        $pelanggan = '-';
        $agendas = array();
        $invoices = array();
        $penawaran = array();
    }elseif(isset($_GET['c'])&&!empty($_GET['c'])){
        $transaksi->setCustomer($_GET['c']);
        $datatransaksi = $transaksi->FetchTransactionDataByIdCustomer();
        $customer->setID($_GET['c']);
        $dbcustomer = $customer->FetchSelectedID();
        // print_r($dbcustomer);
        @$usaha = $dbcustomer->id_perusahaan;
        $datacustomer = $customer->FetchSelectedID2(); //data customer by perusahaan
        $idtr = '-';
        $pelanggan = $_GET['c'];
        $tgl = array();
        $itm = array();
        $agendas = array();
        $invoices = array();
        $penawaran = array();
    }elseif(isset($_GET['tr'])&&!empty($_GET['tr'])){
        include_once '../model/Invoice.php';
        $invoice = new Invoice();
        $invoice->setIdTransaksi($_GET['tr']);
        $transaksi->setID($_GET['tr']);
        $idtr = $_GET['tr'];
        $trdata = $transaksi->FetchTransactionDataById();
        $transaksi->setCustomer($trdata->trans_cust_id);
        $datatransaksi = $transaksi->FetchTransactionDataByIdTransaction();
        $customer->setID($trdata->trans_cust_id);
        $dbcustomer = $customer->FetchSelectedID();
        @$usaha = $trdata->trans_id_usaha;
        $datacustomer = $customer->FetchSelectedID2(); //data customer by perusahaan
        $tgl = $transaksi->FetchTanggalByTransaction();
        $itm = $transaksi->FetchItems();
        $agendas = $transaksi->FetchAgendasByTransaction();
        $pelanggan = $trdata->trans_cust_id;
        $invoices = $invoice->GetByTransaction();
        $penawaran = $transaksi->FetchQuotation();

        if(file_exists('invoices/'.'Invoice '.$_GET['tr'].'.pdf')){
            $fileinv = 'Invoice ' . $_GET['tr'] . '.pdf';
        }

}elseif (isset($_GET['delinv'])&&!empty($_GET['delinv'])) {
    $str = str_replace('.pdf', '', $_GET['delinv']);
    $str = str_replace('Invoice ', '', $str);
    if(file_exists('invoices/'.$_GET['delinv'])){
        if (unlink('invoices/'.$_GET['delinv'])) {
            echo "<script>alert('File berhasil dihapus!');</script>";
            echo "<script>location.replace('new-perusahaan-info?tr=".$str."');</script>";
        }
    }else{
        echo "<script>alert('Tidak ada file yang dimaksud!');</script>";
        echo "<script>location.replace('new-perusahaan-info?tr=".$str."');</script>";
    }
} else{
    echo "<script>location.replace('perusahaan');</script>";
}


//data description
$data = array(
    'base_url' => getBaseUrl(),
    'judul' => 'Info Transaksi Perusahaan/FAC Institute',
    'username'=>$_SESSION['admin']['nama'],
    'page' => 'transaksi',
    'id_usaha' => $usaha,
    'nama_usaha'=> $perusahaan->selectNamaUsahaByID($usaha),
    'id_transaksi' => $idtr,
    'tanggal' => $tgl,
    'items' => $itm,
    'agenda' => $agendas,
    'pelanggan' => $pelanggan,
    'invoices' => $invoices,
    'penawaran'=>$penawaran
);



$data = (object) $data;

include_once 'view/transaksi/view-perusahaan.php';

?> 