<?php 
session_start();

include_once '../baseurl.php';
include_once 'session/session-class.php';
include_once '../model/Invoice.php';
include_once '../model/Transaksi.php';
include_once '../model/Querybuilder.php';

$transaksi = new Transaksi();
$session = new Sessionz();
$session->OnlyAdmin();
$invoice = new Invoice();
$qb = new QueryBuilder();

$datamarketing = $qb->FetchWhere(['username','nama'],'users',array($qb->GetCond('team','=','3'),$qb->GetCond('status','=','aktif')));

$databulanlalu = $invoice->GetDataBulanLalu(date('m')-1,date('Y'));
if (isset($_GET['src'])) {
	// print_r($_GET['src']);
	
	$search = array(
		'nama_usaha' => $_GET['src']['np'],
		'nama_cust' =>$_GET['src']['nk'],
		'tanggal_awal' =>$_GET['src']['fd'],
		'tanggal_akhir' =>$_GET['src']['ld'],
		'tipe_transaksi'=>$_GET['src']['tipe'],
		'jumlah' => $_GET['src']['total'],
		'status' => $_GET['src']['st'],
		'sales' => $_GET['src']['sls']
	);
    $tglbayar = '0';
	if (isset($_GET['src']['tglbayar'])&&$_GET['src']['tglbayar']=='y') {
		$search['tglbyr'] = 'y';
		$tglbayar = '1';
	}else{
		$search['tglbyr'] = 'n';
	}


	$invoice->setData((object) $search);

	$data = array(
		'base_url' => getBaseUrl(), //base data
        'url'=> basename(__FILE__, '.php'),
        'judul' => 'Cari Data Invoice/FAC Institute',
        'username'=>$_SESSION['admin']['nama'],
        'subtitle' => 'Data Invoice',
        'page' => 'data-invoice', // end base data
        'totaldata' => $invoice->CountInvoiceData(),
        'is_report'=>1,
        'pagenum'=>0,
        'list_jenis_transaksi'=>$transaksi->FetchJenisTransaksi(),
       
        'omset_last_year'=> $invoice->OmsetTahunan(date('Y')-1),
        'omset_this_year'=> $invoice->OmsetTahunan(date('Y')),
        
        'paid_last_year'=> $invoice->PaidInvoiceByYear(date('Y')-1),
        'paid_this_year'=> $invoice->PaidInvoiceByYear(date('Y'))
	);
	if($tglbayar=='1'){
	    $data['list'] = $invoice->SearchInvoiceTerbayarByDateTerbayar();
	}else{
	    $data['list'] = $invoice->SearchInvoice();
	}
	
	$data['datamarketing'] = $datamarketing;
	$data['dataget']=$_SERVER['QUERY_STRING'];
	$data = (object) $data;
	include_once 'view/invoice/view-data-invoice.php';

}elseif(isset($_GET['cncl'])&&!empty($_GET['cncl'])) {
	$invoice->setStatus('-1');
	$invoice->setNo($_GET['cncl']);

	if ($invoice->ChangeStatus()) {
		echo "<script>alert('Status Invoice: -telah di batalkan-')</script>;";
		echo "<script>location.replace('".basename(__FILE__,'.php')."')</script>;";
	} else {
		echo "<script>alert('Error merubah status invoice!')</script>;";
		echo "<script>location.replace('".basename(__FILE__,'.php')."')</script>;";
	}

}elseif (isset($_GET['paid'])&&!empty($_GET['paid'])) {
	$invoice->setStatus('2');
	$invoice->setNo($_GET['paid']);

	if ($invoice->ChangeStatus()) {
		echo "<script>alert('Status Invoice: -telah terbayar-')</script>;";
		echo "<script>location.replace('".getBaseUrl()."administrasi/invoice?paidmenu=".$_GET['paid']."')</script>;";
	} else {
		echo "<script>alert('Error merubah status invoice!')</script>;";
		echo "<script>location.replace('".basename(__FILE__,'.php')."')</script>;";
	}	
}elseif (isset($_GET['nts'])&&!empty($_GET['nts'])) {
	$invoice->setStatus('0');
	$invoice->setNo($_GET['nts']);

	if ($invoice->ChangeStatus()) {
		echo "<script>alert('Status Invoice: -belum terkirim-')</script>;";
		echo "<script>location.replace('".basename(__FILE__,'.php')."')</script>;";
	} else {
		echo "<script>alert('Error merubah status invoice!')</script>;";
		echo "<script>location.replace('".basename(__FILE__,'.php')."')</script>;";
	}
}elseif (isset($_GET['snt'])&&!empty($_GET['snt'])) {
	$invoice->setStatus('1');
	$invoice->setNo($_GET['snt']);

	if ($invoice->ChangeStatus()) {
		echo "<script>alert('Status Invoice : -telah terkirim-')</script>;";
		echo "<script>location.replace('".basename(__FILE__,'.php')."')</script>;";
	} else {
		echo "<script>alert('Error merubah status invoice!')</script>;";
		echo "<script>location.replace('".basename(__FILE__,'.php')."')</script>;";
	}
}elseif (isset($_GET['page'])&&!empty($_GET['page'])) {
	$limit = 30;
	$offset = ($_GET['page']-1)*$limit;

	$data = array(
		'base_url' => getBaseUrl(), //base data
        'url'=> basename(__FILE__, '.php'),
        'judul' => 'Data Invoice/FAC Institute',
        'username'=>$_SESSION['admin']['nama'],
        'subtitle' => 'Data Invoice',
        'page' => 'data-invoice', // end base data
        'totaldata' => $invoice->CountInvoiceData(),
        'pagenum'=>$_GET['page'],
        'is_report'=>0,
        'list_jenis_transaksi'=>$transaksi->FetchJenisTransaksi(),
        'omset_last_year'=> $invoice->OmsetTahunan(date('Y')-1),
        'omset_this_year'=> $invoice->OmsetTahunan(date('Y')),
        'paid_last_year'=> $invoice->PaidInvoiceByYear(date('Y')-1),
        'paid_this_year'=> $invoice->PaidInvoiceByYear(date('Y')),
        'list' => $invoice->ListTable($offset,$limit)
	);
	$data['datamarketing'] = $datamarketing;
	$data = (object) $data;
	include_once 'view/invoice/view-data-invoice.php';
} else {
	$offset = 0;
	$limit = 30;

	$data = array(
		'base_url' => getBaseUrl(), //base data
        'url'=> basename(__FILE__, '.php'),
        'judul' => 'Data Invoice/FAC Institute',
        'username'=>$_SESSION['admin']['nama'],
        'subtitle' => 'Data Invoice',
        'page' => 'data-invoice', // end base data
        'totaldata' => $invoice->CountInvoiceData(),
        'pagenum'=>$offset,
        'is_report'=>0,
        'list_jenis_transaksi'=>$transaksi->FetchJenisTransaksi(),
        'omset_last_year'=> $invoice->OmsetTahunan(date('Y')-1),
        'omset_this_year'=> $invoice->OmsetTahunan(date('Y')),
        'paid_last_year'=> $invoice->PaidInvoiceByYear(date('Y')-1),
        'paid_this_year'=> $invoice->PaidInvoiceByYear(date('Y')),
        'list' => $invoice->ListTable($offset,$limit)
	);
	$data['datamarketing'] = $datamarketing;
	$data = (object) $data;
	include_once 'view/invoice/view-data-invoice.php';
}


?>