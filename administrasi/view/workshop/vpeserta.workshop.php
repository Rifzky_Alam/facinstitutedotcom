<!DOCTYPE html>
<html>
<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>

<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
  <div class="page-header" id="top-logo">
    <h3><?php echo $data->subtitle ?></h3>
  </div>
  
  <div class="row">
    <div class="col-md-10">
      <form action="" method="post">
      <div class="form-group">
        <label>No. Telp / Email</label>
        <input type="text" name="in[phoneoremail]" class="form-control" placeholder="No Telepon atau email peserta" required>
      </div>
      <button class="btn -btn-lg btn-primary">Submit</button>
      </form>
    </div>
  </div>
</div>
     
</body>
</html>

