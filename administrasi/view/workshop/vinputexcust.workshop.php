<!DOCTYPE html>
<html>
<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>

<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
  <div class="page-header" id="top-logo">
    <h3><?php echo $data->subtitle ?></h3>
  </div>
  
  <div class="row">

    <div class="col-md-6">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th colspan="2" style="text-align:center;"><?= $data->namacust ?></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Perusahaan</td>
            <td><?= $data->namausaha ?></td>
          </tr>
          <tr>
            <td>No Telepon</td>
            <td><?= $data->telpcust ?></td>
          </tr>
          <tr>
            <td>Email</td>
            <td><?= $data->emailcust ?></td>
          </tr>
          <tr>
            <td>Marketing</td>
            <td><?= $data->namamarketing ?></td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="col-md-6">
      <form action="" method="post">
        <div class="form-group">
          <label>Tanggal Workshop <a target="_blank" href="<?= $data->base_url.'administrasi/workshop/tambah-tanggal?id='.$data->idusaha ?>" title="Klik untuk tambah tanggal"><span class="glyphicon glyphicon-plus"></span></a></label>
          <select name="in[tanggal]" class="form-control" required>
            <option value="">-- Pilih Tanggal --</option>
            <?php foreach ($data->listtanggal as $key): ?>
              <option value="<?= $key['tanggal'] ?>"><?= $key['tanggal'] ?></option>
            <?php endforeach ?>
          </select>
        </div>

        <div class="form-group">
          <label>Item Workshop</label>
          <select name="in[item]" class="form-control" required >
            <option value="">-- pilih item workshop --</option>
            <?php foreach ($data->listitem as $key): ?>
              <option value="<?= $key['id'] ?>"><?= $key['nama_item'].' - '.number_format($key['harga']).' IDR' ?></option>
            <?php endforeach ?>
          </select>
        </div>
        <div class="checkbox">
          <label>
            <input type="checkbox" name="in[paid]" value="3">
            Sudah membayar
          </label>
        </div>
        <input type="hidden" name="in[idcust]" value="<?= $data->idcust ?>" class="form-control" required>
        <input type="hidden" name="in[idworkshop]" value="<?= $data->idusaha ?>" class="form-control" required>
        <button class="btn -btn-lg btn-primary">Submit</button>
      </form>
    </div>
  </div>
</div>
     
</body>
</html>

