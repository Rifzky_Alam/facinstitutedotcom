<!DOCTYPE html>
<html>
<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>

<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
  <div class="page-header" id="top-logo">
    <h3><?php echo $data->subtitle ?></h3>
  </div>
  
  <form action="" method="get">
  <div class="row">
    <div class="col-md-12">
      <input type="text" name="namausaha" class="form-control" placeholder="Search Data by Name ...">
    </div>
  </div>
  </form>

  <br><br>

  <?php DataUsaha($data) ?>

</div>
     
</body>
</html>






<?php function DataUsaha($data){ ?>
    <?php if (count($data->mylists)=='0'): ?>
        <div class="row">
            <div class="col-md-12">
                <div class="jumbotron">
                    <h3>Data yang anda cari tidak ada</h3>
                    <p>Pastikan mengetik nama workshop dengan benar, anda dapat mencari nama workshop dengan pecahan kata dari nama, contoh: ketik 'business' dari workshop Accurate Business Center Mal Metropolitan. <br>
                    bila masih menemukan kendala harap hubungi admin sistem kami <a href="tel:\\081279222250">(Rifzky Alam)</a>
                    </p>
                </div>
            </div>
        </div>
        <?php else: ?>

        <?php foreach ($data->mylists as $key) { ?>
        <div class='row'>
            <div class="col-md-12">
                <div class='panel panel-default'>
                    <div class='panel-heading'>
                        
                        <h3 class='panel-title'><?php echo $key['nama'] ?></h3>
                        
                    </div>
                    <div class='panel-body' style='overflow-x:auto'>
                        <section>
                            <h4><?= $key['ket_jenis_usaha'] ?></h4>
                            <p><?= $key['alamat'] ?></p>
                        </section>
                            
                        <a href="<?= $data->base_url ?>administrasi/workshop/peserta?id=<?= $key['id'] ?>" class='btn btn-primary'>Daftarkan Peserta</a>
                        
                    </div>
                </div>
            </div>
</div>        

        <?php } ?>

    <?php endif ?>

<?php } ?>
