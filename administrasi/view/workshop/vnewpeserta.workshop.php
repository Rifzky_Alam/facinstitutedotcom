<!DOCTYPE html>
<html>
<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>

<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
  <div class="page-header" id="top-logo">
    <h3><?php echo $data->subtitle ?></h3>
  </div>
  
  <div class="row">
    <div class="col-md-10">
      <form action="" method="post">
      <div class="form-group">
        <label>Nama Peserta</label>
        <input type="text" name="in[nama]" class="form-control" placeholder="Nama peserta" required>
      </div>

      <div class="form-group">
        <label>Gender</label>
        <select name="in[gender]" class="form-control" required>
          <option value="">-- Pilih Gender --</option>
          <option value="1">Pria</option>
          <option value="2">Wanita</option>
        </select>
      </div>

      <div class="form-group">
        <label>Telepon Peserta</label>
        <input type="text" name="in[telepon]" class="form-control" placeholder="Telepon peserta" required>
      </div>

      <div class="form-group">
        <label>Email Peserta</label>
        <input type="text" name="in[email]" class="form-control" placeholder="Email peserta" required>
      </div>

      <div class="form-group">
        <label>Marketing</label>
        <select name="in[marketing]" required class="form-control">
          <option value="">-- pilih marketing --</option>
          <?php foreach ($data->listmarketing as $key): ?>
            <option value="<?= $key['marketing_id'] ?>"><?= $key['marketing_nama'] ?></option>
          <?php endforeach ?>
        </select>
      </div>

      <button class="btn -btn-lg btn-primary">Submit</button>
      </form>
    </div>
  </div>
</div>
     
</body>
</html>

