<!DOCTYPE html>
<html>
<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>

<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
  <div class="page-header" id="top-logo">
    <h3><?php echo $data->subtitle ?></h3>
  </div>
  
  <div class="row">

    <div class="col-md-6">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th colspan="2" style="text-align:center;">Tanggal Workshop</th>
          </tr>
        </thead>
        <tbody>
          <?php if (count($data->listtanggal)=='0'): ?>
            <tr>
              <td colspan="2" class="tengah">Belum ada tanggal tersedia.</td>
            </tr>
          <?php else: ?>
            <?php foreach ($data->listtanggal as $key): ?>
                <tr>
                  <td><?= $key['tanggal'] ?></td>
                  <td class="tengah"><a href="#" title="Hide">Hide</a></td>
                </tr>
              <?php endforeach ?>  
          <?php endif ?>
          
        </tbody>
      </table>
    </div>
    <div class="col-md-6">
      <form action="" method="post">
        <div class="form-group">
          <label>Tanggal Workshop</label>
          <input type="text" id="idate" name="in[tanggal]" class="form-control" placeholder="Tanggal Workshop">
        </div>
        <button class="btn -btn-lg btn-primary">Submit</button>
      </form>
    </div>
  </div>
</div>
  <script type="text/javascript" src="<?= $data->base_url ?>js/jquery-ui/jquery-ui.js"></script>
    <script>
      $(document).ready(function(){
        $('#idate').datepicker({
            dateFormat: 'yy-mm-dd'
        });
      });
      </script>
</body>
</html>

