<!DOCTYPE html>
<html>


<?php include_once HomeDirectory().'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once HomeDirectory().'administrasi/view/main-component/sidebar.php';
  include_once HomeDirectory().'administrasi/view/main-component/top-nav.php';
 ?>


<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3>Dashboard</h3>
	</div>
	
    <!--
	<div class="row">
		<div class="col-sm-12 col-md-12" style="text-align:right"><a class="" id="cari-data" href="#modal-cari" data-toggle="modal">Cari Data?</a></div>
	</div>
    -->

    <div class='row'>
        <div class='col-md-6'>
            <h3>Notifikasi Untuk <?= $data->petugas ?></h3>
            
                                                                                            
                <div class="alert alert-danger">
                    <strong>Omset Bulan lalu:</strong> <span id="omsetblnlalu">Loading ..</span>                
                </div>
                
                <div class="alert alert-warning">
                    <strong>Omset Bulan ini:</strong> <span id="omsetblnini">Loading ..</span>
                </div>

                <div class="alert alert-success">
                    <strong>Invoice terbayar bulan lalu:</strong> <span id="terbayarblnlalu">Loading ..</span>            
                </div>

                <div class="alert alert-info">
                    <strong>Invoice terbayar bulan ini</strong> <span id="terbayarblnini">Loading ..</span> 
                </div>

                <div class="alert alert-danger">
                    <a href="attendance" title="Klik untuk melihat data yang belum terlapor"><strong>Data Absen Belum Lengkap:</strong> <span id="absenblomlengkap">Loading ..</span></a> 
                </div>
                
                <div class="alert alert-warning">
                    <a href="<?= $data->base_url.'administrasi/customerservice/newcomplists' ?>" title="Klik untuk melihat perusahaan yang belum terekspor"><strong>Data Perusahaan Support:</strong> <span id="usahasupport">Loading ..</span></a> 
                </div>

                <div class="alert alert-info">
                    <a href="table?ac=lfm">
                        <strong>Data Follow-Up Bulan Ini:</strong> 0                    
                    </a>
                </div>

                        
          </div>
        

        <div class='col-md-6'>

            <div class="row">
                <div class="col-md-12">
                    <div class="jumbotron">
                        <h3>Pengumuman</h3>
                        <p>
                            In shaa Allah minggu depan ada sistem baru perencanaan kegiatan (To-do List), diharapkan untuk semua staff fac-institute agar selalu mengerjakan sesuai perencanaan dan tepat waktu -- (Time is like a sword, sharp and may kill) <br>
                            Regards,
                            <br>
                            <br>
                            Rifzky Alam
                        </p>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Kegiatan Hari Ini:</div>
                <div class="panel-body">
                    <table class='table table-bordered'>
                        <thead>
                            <tr>
                                <th>Nama Perusahaan</th><th>Lokasi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php DataAgendaHariIni($data->agendaharian) ?>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="panel panel-primary">
                <div class="panel-heading">TODO-List:</div>
                <div class="panel-body">
                    <table class='table table-bordered'>
                        <thead>
                            <tr>
                                <th>Judul Tugas</th><th>Deadline</th><th>Prioritas</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($data->todolistdata['jumlahdata']=='0'): ?>
                                <tr>
                                    <td colspan="3" style="text-align:center;">Tidak ada to-do list untuk anda saat ini</td>
                                </tr>
                                <?php else: ?>
                                    <?php foreach ($data->todolistdata['data'] as $key): ?>
                                        <tr>
                                            <td>
                                                <a href="<?= $data->base_url.'administrasi/todo/editstaff/'.$key['ft_id'] ?>" title="Edit">
                                                    <?= $key['ft_assignment'] ?>
                                                </a>
                                            </td>
                                            <td><?= $key['ft_due'] ?></td>
                                            <td><?= $key['ft_priority'] ?></td>
                                        </tr>
                                    <?php endforeach ?>
                            <?php endif ?>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="panel panel-danger">
                <div class="panel-heading">Data yang ingin di follow-up:</div>
                <div class="panel-body">
                    <table class='table table-bordered'>
                        <thead>
                            <tr>
                                <th>Nama</th><th>Telepon</th><th>Email</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php DataSubscribers($data->subscribers) ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    
    
        <div class='row'>
            <div class='col-md-12' style='overflow-x:auto'>
                <center><h2>Data Request Transaksi & Absensi <br>Tanggal: 14-11-2017</h2></center>
                <p><strong>Total : </strong> 5</p>
                <table class='table table-hover'>
                        <thead>
                            <tr>
                                <th>Tanggal Request</th>
                                <th>Dari Petugas</th>
                                <th>Nama Perusahaan</th>
                                <th class="tengah">Tandai</th>  
                            </tr>
                        </thead>
                        <tbody>
                            <?php Datarequests($data->datarequest) ?>
                        </tbody>
                </table>
            </div>        

        </div>

    </div>
	    
</div>

<script>
    Number.prototype.format = function(n, x) {
    var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
  };

function UsahaExport() {
    $.ajax({
            type: "POST",
            url: "https://fac-institute.com/administrasi/customerservice/api?req=cte",
            data: {
              'id':''
            },
            cache: false,
            success: function(data){
            //   console.log(data.totaldata);
                $('#usahasupport').html(data.totaldata);
            }
        }); //end ajax
  }

  // function LaporanReimburse() {
  //       $.ajax({
  //           type: "GET",
  //           url: "https://fac-institute.com/administrasi/user/laporan",
  //           data: {
  //             'req':'medreim'
  //           },
  //           cache: false,
  //           success: function(data){
  //             // alert(data);
  //             // var totaltrainingblnlalu = data.omsetbulanlalu.total;
  //             var medreimskrg = parseInt(data.listdata.med_reim);
  //             $('#healthreq').html("Rp " + medreimskrg.format());
  //             // console.log(data);
  //           }
  //       }); //end ajax
  //   }


  function ShowLaporanOmset() {
    $.ajax({
            type: "GET",
            url: "https://fac-institute.com/administrasi/invoice/datas",
            data: {
              'req':'omsetbulanan'
            },
            cache: false,
            success: function(data){
              // alert(data);
              var totaltrainingblnlalu = data.omsetbulanlalu.training;
              var totaltrainingskrg = data.omsetbulanini.training;
              $('#omsetblnini').html("Rp " + totaltrainingskrg.format());
              $('#omsetblnlalu').html("Rp " + totaltrainingblnlalu.format());
              // console.log(data);
            }
        }); //end ajax
  }


  function ShowLaporanTerbayar() {
    $.ajax({
            type: "GET",
            url: "https://fac-institute.com/administrasi/invoice/datas",
            data: {
              'req':'terbayarbulanan'
            },
            cache: false,
            success: function(data){
              // alert(data);
              var totaltrainingbulanlalu = data.terbayarbulanlalu.training;
              var totalsekarang = data.terbayarbulanini.training;
              $('#terbayarblnlalu').html("Rp " + totaltrainingbulanlalu.format());
              $('#terbayarblnini').html("Rp " + totalsekarang.format());
              // console.log(data);
            }
        }); //end ajax
  }

  function DataAbsenBelumLengkap() {
    $.ajax({
            type: "GET",
            url: "https://fac-institute.com/administrasi/absensi/databelomabsenjson",
            data: {
              '':''
            },
            cache: false,
            success: function(data){
              // alert(data);
              // console.log(data);
              $('#absenblomlengkap').html(data.data.databelumabsen);
            }
        }); //end ajax
  }

    $(document).ready(function(){
        ShowLaporanOmset();
        ShowLaporanTerbayar();
        DataAbsenBelumLengkap();
        UsahaExport();
    });
</script>

</body>
</html> 

<?php function Datarequests($value){ ?>
    <?php if (count($value)=='0'): ?>

    <?php else: ?>

        <?php foreach ($value as $key): ?>
            <tr>
                <td><?php echo $key->tanggal_absen ?></td>
                <td><?php echo $key->nama ?></td>
                <td><?php echo $key->nama_perusahaan ?></td>
                <td class="tengah"><a href="table?ac=cgatt&tanggal=<?php echo $key->tanggal_absen ?>&v=<?php echo $key->id ?>"><span class="btn btn-sm btn-info"><i class="glyphicon glyphicon-ok"></i></span></a></td>
            </tr>
        <?php endforeach ?>
    <?php endif ?>
<? } ?>
