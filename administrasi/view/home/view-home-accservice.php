<!DOCTYPE html>
<html>


<?php include_once HomeDirectory().'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  AccountingService($data->base_url,$_SESSION['admin']['username']);
  include_once HomeDirectory().'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3>Dashboard</h3>
	</div>
	
    <!--
	<div class="row">
		<div class="col-sm-12 col-md-12" style="text-align:right"><a class="" id="cari-data" href="#modal-cari" data-toggle="modal">Cari Data?</a></div>
	</div>
    -->

    <div class="row">
        <div class="col-md-12">
            <div class="jumbotron">
                <h3>Pengumuman</h3>
                <p>
                    <?php if ($data->pengumuman=='-'): ?>
                        Tidak ada pengumuman.
                        <?php else: ?>
                        <?= $data->pengumuman ?>
                    <?php endif ?>
                </p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Pemberi Tugas</th>
                        <th>Judul Tugas</th>
                        <th>Deskripsi</th>
                        <th>Prioritas</th>
                        <th>Deadline</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($data->todolistdata['data'])=='0'): ?>
                            <tr>
                                <td colspan="5" style="text-align:center;"><b>Tidak Ada Data Tugas Untuk Anda</b></td>
                            </tr>
                        <?php else: ?>
                        <?php foreach ($data->todolistdata['data'] as $key): ?>
                            <tr>
                                <td><?= FindStaff($key['ft_assigner'],$data->todolistdata['staffs'])  ?></td>
                                <a href="<?= $data->base_url.'administrasi/todo/editstaff/'.$key['ft_id'] ?>" title="Edit">
                                    <?= $key['ft_assignment'] ?>
                                </a>
                                <td><?= $key['ft_desc'] ?></td>
                                <td><?= $key['ft_priority'] ?></td>
                                <td><?= $key['ft_due'] ?></td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                        
                </tbody>
            </table>
        </div>
    </div>

    <div class='row'>
        <div class='col-md-6'>
            <h3>Notifikasi Untuk <?= $data->petugas ?></h3>
            
            <div class="alert alert-danger">
                <a href="<?= $data->base_url.'administrasi/user/laporan-harian?act=unr' ?>" title="Klik untuk melihat data yang belum terlapor">
                    <strong>Laporan belum terlapor:</strong> <?= $data->unreported ?>
                </a>
            </div>

            <div class="alert alert-danger">
                <a href="attendance" title="Klik untuk melihat data yang belum terlapor">
                    <strong>Absensi belum lengkap</strong> <?= $data->unreportedatt ?>
                </a>
            </div>

            <div class="alert alert-warning">
                <strong>Request Absen:</strong> <?= $data->my_request ?>
                <a href="table?ac=request"><span class="glyphicon glyphicon-log-in"></span></a>
            </div>

            <div class="alert alert-info">
                <strong>Total Permintaan Biaya Kesehatan:</strong> <span id="healthreq">Loading ..</span>
            </div>
            
            <!--<div class="alert alert-info">-->
            <!--    <strong>Total Omset Bulan Ini:</strong> <span id="omsetz">Sistem Belum Tersedia</span>-->
            <!--</div>-->
                        
          </div>
        

        <div class='col-md-6'>
            <div class="panel panel-default">
                <div class="panel-heading">Kegiatan Hari Ini:</div>
                <div class="panel-body">
                    <table class='table table-bordered'>
                        <thead>
                            <tr>
                                <th>Nama Perusahaan</th><th>Lokasi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php DataAgendaHariIni($data->agendaharian) ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


    </div>
	    
</div>
<script>
Number.prototype.format = function(n, x) {
    var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
  };

    function ShowLaporanReimburse() {
    $.ajax({
            type: "GET",
            url: "https://fac-institute.com/administrasi/user/laporan",
            data: {
              'req':'medreim'
            },
            cache: false,
            success: function(data){
              
              var totaltrainingskrg = parseInt(data.listdata.med_reim);
              $('#healthreq').html("Rp " + totaltrainingskrg.format());
              
            }
        }); //end ajax
  }

    $(document).ready(function(){
        ShowLaporanReimburse()
    });
</script>
</body>
</html> 

