<!DOCTYPE html>
<html>


<?php include_once HomeDirectory().'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once HomeDirectory().'administrasi/view/main-component/sidebar.php';
  include_once HomeDirectory().'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
    <div class="page-header" id="top-logo">
        <h3>Dashboard</h3>
    </div>

    <div class='row'>
        <div class='col-md-6'>
            <h3>Notifikasi Untuk <?= $data->petugas ?></h3>
                <div class="alert alert-danger">
                    <a href="<?= $data->base_url.'administrasi/user/laporan-harian?act=unr' ?>" title="Klik untuk melihat data yang belum terlapor">
                        <strong>Laporan belum terlapor:</strong> <?= $data->unreported ?>
                    </a>
                </div>

                <div class="alert alert-danger">
                    <a href="attendance" title="Klik untuk melihat data yang belum terlapor">
                        <strong>Absensi belum lengkap</strong> <?= $data->unreportedatt ?>
                    </a>
                </div>

                <div class="alert alert-warning">
                    <a href="attendance/admin">
                        <strong>Laporan Staff Tertunda:</strong> <?= $data->pendingabsen ?> 
                        <span class="glyphicon glyphicon-log-in"></span>
                    </a>
                </div>

                <div class="alert alert-info">
                    <a href="table?ac=sotd">
                        <strong>Data subscriber</strong> <?= $data->count_subsribers ?>
                    </a>
                </div>

                <div class="alert alert-info">
                    <a href="table?ac=lfm">
                        <strong>Data Follow-Up Bulan Ini:</strong> <?= $data->fu_data ?>
                    </a>
                </div>

                <div class="alert alert-warning">
                    <a href="table?ac=request">
                        <strong>Request Absen:</strong> <?= $data->my_request ?>
                        <span class="glyphicon glyphicon-log-in"></span>
                    </a>
                </div>
                
                <div class="alert alert-info">
                    <strong>Total Permintaan Biaya Kesehatan:</strong> <span id="healthreq">Loading ..</span>
                </div>
          </div>
        

        <div class='col-md-6'>
            <div class="panel panel-default">
                <div class="panel-heading">Kegiatan Hari Ini:</div>
                <div class="panel-body">
                    <table class='table table-bordered'>
                        <thead>
                            <tr>
                                <th>Nama Perusahaan</th><th>Lokasi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php DataAgendaHariIni($data->agendaharian) ?>                            
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- end panel -->
            <div class="panel panel-warning">
                <div class="panel-heading">Upcoming Training/Event</div>
                <div class="panel-body">
                    <table class='table table-bordered'>
                        <thead>
                            <tr>
                                <th>Nama Perusahaan</th>
                                <th>Tanggal</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody id="upcomingevent">
                            <tr id="loadingevent">
                                <td colspan="3" style="text-align: center;">Loading ..</td>
                            </tr>                 
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- endpanel -->
            <div class="panel panel-primary">
                <div class="panel-heading">TODO-List:</div>
                <div class="panel-body">
                    <table class='table table-bordered'>
                        <thead>
                            <tr>
                                <th>Judul Tugas</th><th>Deadline</th><th>Prioritas</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($data->todolistdata['jumlahdata']=='0'): ?>
                                <tr>
                                    <td colspan="3" style="text-align:center;">Tidak ada to-do list untuk anda saat ini</td>
                                </tr>
                                <?php else: ?>
                                    <?php foreach ($data->todolistdata['data'] as $key): ?>
                                        <tr>
                                            <td>
                                                <a href="<?= $data->base_url.'administrasi/todo/editstaff/'.$key['ft_id'] ?>" title="Edit">
                                                    <?= $key['ft_assignment'] ?>
                                                </a>
                                            </td>
                                            <td><?= $key['ft_due'] ?></td>
                                            <td><?= $key['ft_priority'] ?></td>
                                        </tr>
                                    <?php endforeach ?>
                            <?php endif ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- end panel -->
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12" style="overflow: auto;">
            <center>
                <h4>Invoice Belum terkirim</h4>
            </center>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>No Invoice</th>
                        <th>Nama Perusahaan</th>
                        <th>Customer</th>
                        <th>Paket</th>
                        <th>Jumlah</th>
                        <th>Tgl Kirim</th>
                        <th>Aksi</th>
                        <th>Kirim</th>
                    </tr>
                </thead>
                <tbody>
                    <?php DataPendingInvoice($data->base_url,$data->unsent_inv) ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <center>
                <h4>Omset</h4>
            </center>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="col-md-3 column">
                <div class="panel panel-danger">
                    <div class="panel-heading libreStatsPanelHeading">
                        <div class="panel-title">
                            <span>Bulan Lalu</span>
                            <!--<a class="fa fa-caret-down pull-right libreStatsPanelHeadingIcon" href="#uniqueVisitor"-->
                            <!--   data-toggle="collapse"></a>-->
                        </div>
                    </div>
                    <div class="panel-body libreStatsPanelBody collapse in" id="uniqueVisitor">
                        <div class="row clearfix libreStatsPanelRow">
                            <div class="col-md-8 column libreStatsPanelValueColumn">
                                <p class="libreStatsCount" id="omsetblnlalu">Loading ..</p>
                                <small>Rupiah</small>
                            </div>
                            <div class="col-md-4 column">
                                <!--<span class="pull-right fa fa-user fa-5x libreStatsIcon"></span>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 column">
                <div class="panel panel-success">
                    <div class="panel-heading libreStatsPanelHeading">
                        <div class="panel-title">
                            <span>Bulan Ini</span>
                            <!--<a class="fa fa-caret-down pull-right libreStatsPanelHeadingIcon" href="#activity"-->
                            <!--   data-toggle="collapse"></a>-->
                        </div>
                    </div>
                    <div class="panel-body libreStatsPanelBody collapse in" id="activity">
                        <div class="row clearfix libreStatsPanelRow">
                            <div class="col-md-8 column libreStatsPanelValueColumn">
                                <p class="libreStatsCount" id="omsetblnini">Loading ..</p>
                                <small>Rupiah</small>
                            </div>
                            <div class="col-md-4 column">
                                <!--<span class="pull-right fa fa-comments-o fa-5x libreStatsIcon"></span>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 column">
                <div class="panel panel-warning">
                    <div class="panel-heading libreStatsPanelHeading">
                        <div class="panel-title">
                            <span>Tahun Lalu</span>
                            <!--<a class="fa fa-caret-down pull-right libreStatsPanelHeadingIcon" href="#user"-->
                            <!--   data-toggle="collapse"></a>-->
                        </div>
                    </div>
                    <div class="panel-body libreStatsPanelBody collapse in" id="user">
                        <div class="row clearfix libreStatsPanelRow">
                            <div class="col-md-8 column libreStatsPanelValueColumn">
                                <p class="libreStatsCount"><?= number_format($data->omset_tahun_lalu) ?></p>
                                <small>Rupiah</small>
                            </div>
                            <div class="col-md-4 column">
                                <!--<span class="pull-right fa fa-users fa-5x libreStatsIcon"></span>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 column">
                <div class="panel panel-primary">
                    <div class="panel-heading libreStatsPanelHeading">
                        <div class="panel-title">
                            <span>Tahun Ini</span>
                            <!--<a class="fa fa-caret-down pull-right libreStatsPanelHeadingIcon" href="#sales"-->
                            <!--   data-toggle="collapse"></a>-->
                        </div>
                    </div>
                    <div class="panel-body libreStatsPanelBody collapse in" id="sales">
                        <div class="row clearfix libreStatsPanelRow">
                            <div class="col-md-8 column libreStatsPanelValueColumn">
                                <p class="libreStatsCount"><?= number_format($data->omset_tahun_ini) ?></p>
                                <small>Rupiah</small>
                            </div>
                            <div class="col-md-4 column">
                                <!--<span class="pull-right fa fa-dollar fa-5x libreStatsIcon"></span>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <center>
                <h4>Invoice Terbayar</h4>
            </center>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="col-md-3 column">
                <div class="panel panel-warning">
                    <div class="panel-heading libreStatsPanelHeading">
                        <div class="panel-title">
                            <span>Bulan Lalu</span>
                            <!--<a class="fa fa-caret-down pull-right libreStatsPanelHeadingIcon" href="#uniqueVisitor"-->
                            <!--   data-toggle="collapse"></a>-->
                        </div>
                    </div>
                    <div class="panel-body libreStatsPanelBody collapse in" id="uniqueVisitor">
                        <div class="row clearfix libreStatsPanelRow">
                            <div class="col-md-8 column libreStatsPanelValueColumn">
                                <p class="libreStatsCount" id="terbayarblnlalu">Loading ..</p>
                                <small>Rupiah</small>
                            </div>
                            <div class="col-md-4 column">
                                <!--<span class="pull-right fa fa-user fa-5x libreStatsIcon"></span>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 column">
                <div class="panel panel-info">
                    <div class="panel-heading libreStatsPanelHeading">
                        <div class="panel-title">
                            <span>Bulan Ini</span>
                            <!--<a class="fa fa-caret-down pull-right libreStatsPanelHeadingIcon" href="#activity"-->
                            <!--   data-toggle="collapse"></a>-->
                        </div>
                    </div>
                    <div class="panel-body libreStatsPanelBody collapse in" id="activity">
                        <div class="row clearfix libreStatsPanelRow">
                            <div class="col-md-8 column libreStatsPanelValueColumn">
                                <p class="libreStatsCount" id="terbayarblnini">Loading ..</p>
                                <small>Rupiah</small>
                            </div>
                            <div class="col-md-4 column">
                                <!--<span class="pull-right fa fa-comments-o fa-5x libreStatsIcon"></span>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 column">
                <div class="panel panel-danger">
                    <div class="panel-heading libreStatsPanelHeading">
                        <div class="panel-title">
                            <span>Tahun Lalu</span>
                            <!--<a class="fa fa-caret-down pull-right libreStatsPanelHeadingIcon" href="#user"-->
                            <!--   data-toggle="collapse"></a>-->
                        </div>
                    </div>
                    <div class="panel-body libreStatsPanelBody collapse in" id="user">
                        <div class="row clearfix libreStatsPanelRow">
                            <div class="col-md-8 column libreStatsPanelValueColumn">
                                <p class="libreStatsCount"><?= number_format($data->inv_last_year) ?></p>
                                <small>Rupiah</small>
                            </div>
                            <div class="col-md-4 column">
                                <!--<span class="pull-right fa fa-users fa-5x libreStatsIcon"></span>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 column">
                <div class="panel panel-info">
                    <div class="panel-heading libreStatsPanelHeading">
                        <div class="panel-title">
                            <span>Tahun Ini</span>
                            <!--<a class="fa fa-caret-down pull-right libreStatsPanelHeadingIcon" href="#sales"-->
                            <!--   data-toggle="collapse"></a>-->
                        </div>
                    </div>
                    <div class="panel-body libreStatsPanelBody collapse in" id="sales">
                        <div class="row clearfix libreStatsPanelRow">
                            <div class="col-md-8 column libreStatsPanelValueColumn">
                                <p class="libreStatsCount"><?= number_format($data->inv_this_year) ?></p>
                                <small>Rupiah</small>
                            </div>
                            <div class="col-md-4 column">
                                <!--<span class="pull-right fa fa-dollar fa-5x libreStatsIcon"></span>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    

    

        <div class='row'>
            <div class='col-md-6' style='overflow-x:auto'>
                <center>
                    <a href="<?= $data->base_url.'administrasi/googlecal/reptraining' ?>" title="Detail Halaman">
                        <h2>Data Trainer Repeat Bln Lalu</h2>
                    </a>
                </center>
                <table class='table table-bordered'>
                        <thead>
                            <tr>
                                <th>Nama Trainer</th>
                                <th>Perusahaan</th>
                                <th>Total Hari Training</th>    
                            </tr>
                        </thead>
                        <tbody id="repeattbodylastmonth">
                            <tr id="loadingrepeatlastmonth">
                                <!-- <td colspan="3" style="text-align: center;">Loading ..</td> -->
                            </tr> 
                        </tbody>
                </table>
            </div>
            <div class='col-md-6' style='overflow-x:auto'>
                <center>
                    <a href="<?= $data->base_url.'administrasi/googlecal/reptraining' ?>" title="Detail Halaman">
                        <h2>Data Trainer Repeat Bln Ini</h2>
                    </a>
                </center>
                <table class='table table-bordered'>
                        <thead>
                            <tr>
                                <th>Nama Trainer</th>
                                <th>Perusahaan</th>
                                <th>Total Hari Training</th>    
                            </tr>
                        </thead>
                        <tbody id="repeattbody">
                            <tr id="loadingrepeat">
                                <!-- <td colspan="3" style="text-align: center;">Loading ..</td> -->
                            </tr> 
                        </tbody>
                </table>
            </div>
        </div>
    </div>  

</div>
<script type="text/javascript">
    Number.prototype.format = function(n, x) {
        var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
        return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
    };

    function ShowLaporanOmset() {
        $.ajax({
            type: "GET",
            url: "https://fac-institute.com/administrasi/invoice/datas",
            data: {
              'req':'omsetbulanan'
            },
            cache: false,
            success: function(data){
              // alert(data);
              var totaltrainingblnlalu = data.omsetbulanlalu.total;
              var totaltrainingskrg = data.omsetbulanini.total;
              $('#omsetblnini').html("Rp " + totaltrainingskrg.format());
              $('#omsetblnlalu').html("Rp " + totaltrainingblnlalu.format());
              // console.log(data);
            }
        }); //end ajax
    }

    function LaporanReimburse() {
        $.ajax({
            type: "GET",
            url: "https://fac-institute.com/administrasi/user/laporan",
            data: {
              'req':'medreim'
            },
            cache: false,
            success: function(data){
              // alert(data);
              // var totaltrainingblnlalu = data.omsetbulanlalu.total;
              var medreimskrg = parseInt(data.listdata.med_reim);
              $('#healthreq').html("Rp " + medreimskrg.format());
              // console.log(data);
            }
        }); //end ajax
    }

    function ShowLaporanRepeat(menu,idtable) {
        $.ajax({
            type: "GET",
            url: "<?= $data->base_url ?>administrasi/googlecal/apirepeattrainer",
            data: {
              'm':menu
            },
            cache: false,
            success: function(data){
              // console.log(data);
              if (data.status.code=='1') {
                // console.log(data);
                var objs = data.listdata;
                for (x in objs) {
                    document.getElementById(idtable).innerHTML += "<tr><td>"+objs[x].nama+"</td><td>"+objs[x].nama_usaha+"</td><td>"+objs[x].total_hari_training+"</td></tr>";
                }
                
              } else {
                // console.log(data);
                alert('data is empty!');
              }
            }
        }); //end ajax
    }


  function ShowLaporanTerbayar() {
    $.ajax({
            type: "GET",
            url: "https://fac-institute.com/administrasi/invoice/datas",
            data: {
              'req':'terbayarbulanan'
            },
            cache: false,
            success: function(data){
              // alert(data);
              var totaltrainingbulanlalu = data.terbayarbulanlalu.total;
              var totalsekarang = data.terbayarbulanini.total;
              $('#terbayarblnlalu').html("Rp " + totaltrainingbulanlalu.format());
              $('#terbayarblnini').html("Rp " + totalsekarang.format());
              // console.log(data);
            }
        }); //end ajax
  }

  function ShowUpcomingEvent() {
    $.ajax({
            type: "GET",
            url: "https://fac-institute.com/administrasi/googlecal/upcoming",
            data: {
              '':''
            },
            cache: false,
            success: function(data){
              var objs = data.data;
              $('#loadingevent').remove();
              for (x in objs) {
                if (objs[x].cal_status=='63') {
                    document.getElementById("upcomingevent").innerHTML += "<tr class='info'><td title='click for detail'><a href='<?= $data->base_url.'administrasi/new-detail?tr=' ?>"+objs[x].trans_id+"'>"+objs[x].nama+"</a></td><td>"+objs[x].tanggal+"</td><td><a href='<?= $data->base_url.'administrasi/googlecal/summarylist?tr=' ?>"+objs[x].trans_id+"'>jump to gocal</a></td></tr>";
                }else if(objs[x].g_status=='2'){
                    document.getElementById("upcomingevent").innerHTML += "<tr class='warning'><td title='click for detail'><a href='<?= $data->base_url.'administrasi/new-detail?tr=' ?>"+objs[x].trans_id+"'>"+objs[x].nama+"</a></td><td>"+objs[x].tanggal+"</td><td><a href='<?= $data->base_url.'administrasi/googlecal/summarylist?tr=' ?>"+objs[x].trans_id+"'>jump to gocal</a></td></tr>";
                } else {
                    document.getElementById("upcomingevent").innerHTML += "<tr><td title='click for detail'><a href='<?= $data->base_url.'administrasi/new-detail?tr=' ?>"+objs[x].trans_id+"'>"+objs[x].nama+"</a></td><td>"+objs[x].tanggal+"</td><td><a href='<?= $data->base_url.'administrasi/googlecal/summarylist?tr=' ?>"+objs[x].trans_id+"'>create event gocal</a></td></tr>";
                }
                    
              }
              // console.log(objs);
            }
        }); //end ajax
  }

    $(document).ready(function(){
        ShowUpcomingEvent();
        LaporanReimburse();
        setTimeout(function(){ 
            ShowLaporanRepeat('curmonth','repeattbody');
            ShowLaporanRepeat('lastmonth','repeattbodylastmonth');
            ShowLaporanOmset();
            ShowLaporanTerbayar();
        }, 500);
    });
    </script>
</body>
</html> 

<?php function DataPendingInvoice($baseurl,$datainv){ ?>
    <?php if (count($datainv)=='0'): ?>
        <tr>
            <td colspan="8" class="tengah">Invoice Pending Tidak Tersedia</td>
        </tr>
    <?php else: ?>

            <?php foreach ($datainv as $key): ?>
                <tr class="danger">
                    <td><?php echo @$key->inv_no_invoice ?></td>
                    <td><?php echo @$key->nama_usaha ?></td>
                    <td><?php echo @$key->nama_cust ?></td>
                    <td><?php echo @$key->items ?></td>
                    <td><?php echo @number_format($key->jumlah) ?></td>
                    <td><?php echo @$key->inv_date ?></td>
                    <td><a href="new-perusahaan-info?tr=<?php echo $key->inv_id_trans ?>">Edit</a> || <a href="new-preview?einv=<?php echo $key->inv_no_invoice ?>" target="_blank">Preview</a> || <a href="<?php echo 'new-preview?inv='.$key->inv_no_invoice ?>" target="_blank">PDF</a></td>
                    <td><a href="<?php echo 'new-invoice?valid='.$key->inv_no_invoice ?>" class="btn btn-success">Send!</a></td>
                </tr>
            <?php endforeach ?>
    <?php endif ?>
<? } ?>
