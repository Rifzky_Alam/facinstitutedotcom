<!DOCTYPE html>
<html>


<?php include_once HomeDirectory().'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  Freelance($data->base_url,$_SESSION['admin']['username']);
  include_once HomeDirectory().'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3>Dashboard</h3>
	</div>
	
    <!--
	<div class="row">
		<div class="col-sm-12 col-md-12" style="text-align:right"><a class="" id="cari-data" href="#modal-cari" data-toggle="modal">Cari Data?</a></div>
	</div>
    -->

    <div class='row'>
        <div class='col-md-6'>
            <h3>Notifikasi Untuk <?= $data->petugas ?></h3>
            
            <div class="alert alert-danger">
                <a href="daily-report?act=unr" title="Klik untuk melihat data yang belum terlapor">
                    <strong>Laporan belum terlapor:</strong> <?= $data->unreported ?>
                </a>
            </div>

                <div class="alert alert-warning">
                    <strong>Request Absen:</strong> <?= $data->my_request ?>                   
                    <a href="table?ac=request"><span class="glyphicon glyphicon-log-in"></span>
                    </a>
                </div>

                        
          </div>
        

        <div class='col-md-6'>
            <div class="panel panel-default">
                <div class="panel-heading">Kegiatan Hari Ini:</div>
                <div class="panel-body">
                    <table class='table table-bordered'>
                        <thead>
                            <tr>
                                <th>Nama Perusahaan</th><th>Lokasi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php DataAgendaHariIni($data->agendaharian); ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    </div>
	    
</div>
</body>
</html> 

