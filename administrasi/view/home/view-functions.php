<?php function DataAgendaHariIni($value){ ?>
    <?php if (count($value)=='0'): ?>
        <tr>
            <td colspan="2" class="tengah">Tidak ada agenda untuk hari ini</td>
        </tr>
    <?php else: ?>

            <?php foreach ($value as $key): ?>
                <tr>
                    <td><?= $key->perusahaan ?></td>
                    <td><a href='new-detail.php?tr=<?= $key->transaksi ?>'><?= $key->alamat ?></a></td>
                </tr>
            <?php endforeach ?>
    <?php endif ?>
<? } ?>

<?php function DataViewer($value){ ?>
   <?php if (count($value)=='0'): ?>
        <td colspan="7">Tidak Ada kunjungan ke laman beranda website.</td>
        <?php else: ?>
            <?php foreach ($value as $key): ?>
                <tr>
                    <td><?= $key->time_stamp ?></td>
                    <td><?= $key->city ?></td>
                    <td><?= $key->region ?></td>
                    <td><?= $key->country ?></td>
                    <td><?= $key->browser ?></td>
                    <td><?= $key->os ?></td>
                    <td><?= $key->ip ?></td>
                </tr>
            <?php endforeach ?>
    <?php endif ?> 
<? } ?>

<?php function DataSubscribers($value){ ?>
    <?php if (count($value)=='0'): ?>
        <tr>
            <td colspan="3" class="tengah">Tidak ada Subscribers</td>
        </tr>
    <?php else: ?>

            <?php foreach ($value as $key): ?>
                <tr>
                    <td><?= $key->name ?></td>
                    <td><a href='tel://'><?= $key->phone ?></a></td>
                    <td><a href='mailto://'><?= $key->email ?></a></td>
                </tr>
            <?php endforeach ?>
    <?php endif ?>
<? } ?>
