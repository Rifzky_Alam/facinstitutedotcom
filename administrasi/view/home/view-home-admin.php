<!DOCTYPE html>
<html>


<?php include_once HomeDirectory().'administrasi/view/main-component/header.php'; ?>



<body>

<?php
  include_once HomeDirectory().'administrasi/view/main-component/sidebar.php';
  include_once HomeDirectory().'administrasi/view/main-component/top-nav.php';
 ?>


<div class="container" id="isi">
    <div class="page-header" id="top-logo">
        <h3>Dashboard</h3>
    </div>

    <!--
    <div class="row">
        <div class="col-sm-12 col-md-12" style="text-align:right"><a class="" id="cari-data" href="#modal-cari" data-toggle="modal">Cari Data?</a></div>
    </div>
    -->

    <div class='row'>
        <div class='col-md-6'>
            <h3>Notifikasi Untuk <?= $data->petugas ?></h3>
                <div class="alert alert-danger">
                    <a href="<?= $data->base_url.'administrasi/user/laporan-harian?act=unr' ?>" title="Klik untuk melihat data yang belum terlapor">
                        <strong>Laporan belum terlapor:</strong> <?= $data->unreported ?>
                    </a>
                </div>

                <div class="alert alert-info">
                    <a href="table?ac=sotd">
                        <strong>Data subscriber</strong> <?= $data->count_subsribers ?>
                    </a>
                </div>

                <div class="alert alert-warning">
                    <a href="attendance/attendance/admin">
                        <strong>Laporan Staff Tertunda:</strong> <?= $data->pendingabsen ?>
                        <span class="glyphicon glyphicon-log-in"></span>
                    </a>
                </div>

                <div class="alert alert-info">
                    <a href="table?ac=lfm">
                        <strong>Data Follow-Up Bulan Ini:</strong> <?= $data->fu_data ?>
                    </a>
                </div>

                <div class="alert alert-warning">
                    <a href="table?ac=request">
                        <strong>Request Absen:</strong> <?= $data->my_request ?>
                        <span class="glyphicon glyphicon-log-in"></span>
                    </a>
                </div>

                <div class="alert alert-danger">
                    <a href="attendance" title="Klik untuk melihat data yang belum terlapor">
                        <strong>Absensi belum lengkap</strong> <?= $data->unreportedatt ?>
                    </a>
                </div>
                
                <div class="alert alert-info">
                    <strong>Total Permintaan Biaya Kesehatan:</strong> <span id="healthreq">Sistem Belum Tersedia</span>
                </div>

                <div class="panel panel-danger">
                    <div class="panel-heading">Data berlangganan website:</div>
                    <div class="panel-body">
                        <table class='table table-bordered'>
                            <thead>
                                <tr>
                                    <th>Nama</th><th>Telepon</th><th>Email</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php DataSubscribers($data->subscribers) ?>
                            </tbody>
                        </table>
                    </div>
                </div>

          </div>


        <div class='col-md-6'>
            <div class="panel panel-default">
                <div class="panel-heading">Kegiatan Hari Ini:</div>
                <div class="panel-body">
                    <table class='table table-bordered'>
                        <thead>
                            <tr>
                                <th>Nama Perusahaan</th><th>Lokasi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php DataAgendaHariIni($data->agendaharian) ?>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="panel panel-warning">
                <div class="panel-heading">Upcoming Training/Event</div>
                <div class="panel-body">
                    <table class='table table-bordered'>
                        <thead>
                            <tr>
                                <th>Nama Perusahaan</th>
                                <th>Tanggal</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody id="upcomingevent">
                            <tr id="loadingevent">
                                <td colspan="3" style="text-align: center;">Loading ..</td>
                            </tr>                 
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="panel panel-primary">
                <div class="panel-heading">TODO-List:</div>
                <div class="panel-body">
                    <table class='table table-bordered'>
                        <thead>
                            <tr>
                                <th>Judul Tugas</th><th>Deadline</th><th>Prioritas</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($data->todolistdata['jumlahdata']=='0'): ?>
                                <tr>
                                    <td colspan="3" style="text-align:center;">Tidak ada to-do list untuk anda saat ini</td>
                                </tr>
                                <?php else: ?>
                                    <?php foreach ($data->todolistdata['data'] as $key): ?>
                                        <tr>
                                            <td>
                                                <a href="<?= $data->base_url.'administrasi/todo/editstaff/'.$key['ft_id'] ?>" title="Edit">
                                                    <?= $key['ft_assignment'] ?>
                                                </a>
                                            </td>
                                            <td><?= $key['ft_due'] ?></td>
                                            <td><?= $key['ft_priority'] ?></td>
                                        </tr>
                                    <?php endforeach ?>
                            <?php endif ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

        <div class='row'>
            <div class='col-md-12' style='overflow-x:auto'>
                <center><h2>Data Pengunjung Website (Landing Page) <br>Tanggal: <?= date('Y-m-d') ?></h2></center>
                <p><strong>Total viewer: </strong> <?= $data->count_viewer ?></p>
                <table class='table table-bordered'>
                        <thead>
                            <tr>
                                <th>Waktu Kunjungan</th>
                                <th>Kota</th>
                                <th>Provinsi</th>
                                <th>Negara</th>
                                <th>Browser</th>
                                <th>Platform</th>
                                <th>IP Address</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php DataViewer($data->viewers) ?>
                        </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>

$(document).ready(function(){
    ShowUpcomingEvent();
    LaporanReimburse();
});
function ShowUpcomingEvent() {
    $.ajax({
            type: "GET",
            url: "https://fac-institute.com/administrasi/googlecal/upcoming",
            data: {
              '':''
            },
            cache: false,
            success: function(data){
              var objs = data.data;
              $('#loadingevent').remove();
              for (x in objs) {
                if (objs[x].cal_status=='63') {
                    document.getElementById("upcomingevent").innerHTML += "<tr class='info'><td title='click for detail'><a href='<?= $data->base_url.'administrasi/new-detail?tr=' ?>"+objs[x].trans_id+"'>"+objs[x].nama+"</a></td><td>"+objs[x].tanggal+"</td><td><a href='<?= $data->base_url.'administrasi/googlecal/summarylist?tr=' ?>"+objs[x].trans_id+"'>jump to gocal</a></td></tr>";
                } else {
                    document.getElementById("upcomingevent").innerHTML += "<tr><td title='click for detail'><a href='<?= $data->base_url.'administrasi/new-detail?tr=' ?>"+objs[x].trans_id+"'>"+objs[x].nama+"</a></td><td>"+objs[x].tanggal+"</td><td><a href='<?= $data->base_url.'administrasi/googlecal/summarylist?tr=' ?>"+objs[x].trans_id+"'>jump to gocal</a></td></tr>"
                }
                    
              }
              // console.log(objs);
            }
        }); //end ajax
  }

  function LaporanReimburse() {
        $.ajax({
            type: "GET",
            url: "https://fac-institute.com/administrasi/user/laporan",
            data: {
              'req':'medreim'
            },
            cache: false,
            success: function(data){
              // alert(data);
              // var totaltrainingblnlalu = data.omsetbulanlalu.total;
              var medreimskrg = parseInt(data.listdata.med_reim);
              $('#healthreq').html("Rp " + medreimskrg.format());
              // console.log(data);
            }
        }); //end ajax
    }
</script>

</body>
</html>
