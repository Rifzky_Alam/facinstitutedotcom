<!DOCTYPE html>
<html>
<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>
<body>
<?php 
include_once $data->homedir.'administrasi/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
?>
<div class="container" id="isi">
	 
	 <div class="page-header">
	 	<h2><?= $data->subtitle ?></h2>
	 </div>
	 
	 <div class="row">
	 	<div class="col-md-12">
	 		<table class="table table-bordered">
	 			<tbody>
	 				<tr>
	 					<td>Pemberi Tugas</td>
	 					<td><?= $data->assigner ?></td>
	 				</tr>
	 				<tr>
	 					<td>Ditujukan</td>
	 					<td><?= $data->assignee ?></td>
	 				</tr>
	 				<tr>
	 					<td>Prioritas</td>
	 					<td><?= $data->priority ?></td>
	 				</tr>
	 				<tr>
	 					<td>Deadline</td>
	 					<td><?= $data->deadline ?></td>
	 				</tr>
	 				<tr>
	 					<td>Judul Tugas</td>
	 					<td><?= $data->judul_tugas ?></td>
	 				</tr>
	 				<tr>
	 					<td>Deskripsi</td>
	 					<td><?= $data->desc ?></td>
	 				</tr>
	 				<tr>
	 					<td>Umpan Balik</td>
	 					<td><?= $data->feedback ?></td>
	 				</tr>
	 				<tr>
	 					<td>Waktu Direspon</td>
	 					<td><?= $data->updated ?></td>
	 				</tr>
	 				<tr>
	 					<td>Status</td>
	 					<td><?= $data->status ?></td>
	 				</tr>
	 				<tr>
	 					<td>Lampiran</td>
	 					<td><?= $data->lampiran ?></td>
	 				</tr>
	 				<tr>
	 					<td>Link</td>
	 					<td><?= $data->link ?></td>
	 				</tr>
	 				
	 			</tbody>
	 		</table>
	 	</div>
	 </div>

</div> <!--end container-->
<script type="text/javascript" src="<?= $data->base_url ?>js/jquery.js"></script>
<script type="text/javascript" src='https://fac-institute.com/js/jquery-ui/jquery-ui.js'></script>
</body>
</html>