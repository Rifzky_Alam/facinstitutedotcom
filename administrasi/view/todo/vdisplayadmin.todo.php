<!DOCTYPE html>
<html>
<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>
<body>
<?php 
include_once $data->homedir.'administrasi/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
?>
<div class="container" id="isi">
	 
	 <div class="page-header">
	 	<h2><?= $data->subtitle ?></h2>
	 </div>
	 <div class="row">
	 	<div class="col-md-12">
	 		<a href="<?= $data->base_url.'administrasi/todo/new' ?>" title="New Data" class="btn btn-primary">New Data</a>
	 	</div>
	 </div>
	 <br>
	 <form action="" method="post" accept-charset="utf-8">
	 <div class="row">
	      <div class="col-md-12">
	      	<table class="table table-bordered">
	      		<thead>
	      			<tr>
	      				<th>Assigner</th>
	      				<th>Assignee</th>
	      				<th>Judul</th>
	      				<th>Prioritas</th>
	      				<th>Status</th>
	      				<th>Aksi</th>
	      			</tr>
	      		</thead>
	      		<tbody>
	      			<?php foreach ($data->list as $key): ?>
	      				<?php if ($key['ft_status']=='0'): ?>
	      					<tr>
	      				<?php elseif($key['ft_status']=='1'): ?>
	      					<tr class="warning">
	      				<?php elseif($key['ft_status']=='2'): ?>
	      				<?php // tr danger for overdue assignment ?>
	      				    <tr class="info">
	      				<?php elseif($key['ft_status']=='3'): ?>
	      					<tr class="success">
	      				<?php endif ?>
	      					<td><?= FindStaff($key['ft_assigner'],$data->staffs) ?></td>
	      					<td><?= FindStaff($key['ft_assignee'],$data->staffs) ?></td>
	      					<td><?= $key['ft_assignment'] ?></td>
	      					<td><?= $key['tdpr_desc'] ?></td>
	      					<td><?= $key['ftdls_desc'] ?></td>
	      					<td>
	      						<a href="<?= $data->base_url.'administrasi/todo/detail/'.$key['ft_id'] ?>" title="Detail">Detail</a>
	      						||
	      						<a href="<?= $data->base_url.'administrasi/todo/changestat/'.$key['ft_id'] ?>/0" title="Tolak">Reject</a>
	      						||
	      						<a href="<?= $data->base_url.'administrasi/todo/changestat/'.$key['ft_id'] ?>/3" title="Setujui dan tandai selesai">Approve & mark as done</a>
	      						||
	      						<a href="<?= $data->base_url.'administrasi/todo/changestat/'.$key['ft_id'] ?>/-2" title="Delete">Remove</a>
	      					</td>
	      				</tr>
	      			<?php endforeach ?>
	      		</tbody>
	      	</table>
	      </div>
	  </div>
	  </form>

</div> <!--end container-->
<script type="text/javascript" src="<?= $data->base_url ?>js/jquery.js"></script>
<script type="text/javascript" src='https://fac-institute.com/js/jquery-ui/jquery-ui.js'></script>
</body>
</html>