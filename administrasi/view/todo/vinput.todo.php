<!DOCTYPE html>
<html>
<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>
<body>
<?php 
include_once $data->homedir.'administrasi/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
?>
<div class="container" id="isi">
	 
	 <div class="page-header">
	 	<h2><?= $data->subtitle ?></h2>
	 </div>
	 
	 <form action="" method="post" accept-charset="utf-8" enctype="multipart/form-data">
	 <div class="row">
	      <div class="col-md-12">
	      	
	      	<div class="form-group">
	      		<label>Nama Staff</label>
	      		<select name="in[assigner]" class="form-control" required>
	      			<option disabled selected value="">-- Pilih Nama Staff --</option>
	      			<?php foreach ($data->adminlist as $key): ?>
	      				<option value="<?= $key['username'] ?>"><?= $key['nama'] ?></option>
	      			<?php endforeach ?>
	      		</select>
	      	</div>
	      	
	      	<div class="form-group">
	      		<label>Untuk Petugas (Assignee)</label>
	      		<select name="in[assignee]" class="form-control" required>
	      			<option disabled selected value="">-- Pilih Yang Ditugaskan --</option>
	      			<?php foreach ($data->stafflist as $key): ?>
	      				<option value="<?= $key['username'] ?>"><?= $key['nama'] ?></option>
	      			<?php endforeach ?>
	      		</select>
	      	</div>

	      	<div class="form-group">
	      		<label>Prioritas</label>
	      		<select name="in[priority]" class="form-control" required>
	      			<option disabled selected value="">-- Pilih Prioritas Tugas --</option>
	      			<option value="1">Rendah</option>
	      			<option value="2">Sedang</option>
	      			<option value="3">Tinggi</option>
	      			<option value="4">Darurat (Urgent)</option>
	      		</select>
	      	</div>
	      	
	      	<div class="form-group">
	      		<label>Judul Tugas</label>
	      		<input type="text" name="in[task]" class="form-control" required/>
	      	</div>

	      	<div class="form-group">
	      		<label>Keterangan Tugas</label>
	      		<textarea name="in[desc]" class="form-control"></textarea>
	      	</div>
	      	
	      	<div class="form-group">
	      		<label>Deadline (Due Date)</label>
	      		<input id="deadline" type="text" name="in[due]" class="form-control" required>
	      	</div>

	      	<div class="form-group">
	      		<label for="attachment">Attachments</label>
	      		<input type="file" name="atch" placeholder>
	      	</div>
	      	
	      	<button style="width:100%" class="btn btn-lg btn-primary">Submit</button>
	      </div>
	  </div>
	  </form>

</div> <!--end container-->
<script type="text/javascript" src="<?= $data->base_url ?>js/jquery.js"></script>
<script type="text/javascript" src='https://fac-institute.com/js/jquery-ui/jquery-ui.js'></script>
<script>
	$(document).ready(function(){
		$('#deadline').datepicker({
            dateFormat: 'yy-mm-dd'
          });
	});
</script>
</body>
</html>