<!DOCTYPE html>
<html>
<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>
<body>
<?php 
include_once $data->homedir.'administrasi/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
?>
<div class="container" id="isi">
	 
	 <div class="page-header">
	 	<h2><?= $data->subtitle ?></h2>
	 </div>
	 
	 <div class="row">
	      <div class="col-md-12">
	      	
	      	<div class="col-md-6">
	      		<table class="table table-bordered">
	      			<tbody>
	      				<tr>
	      					<td>Pembuat Tugas</td>
	      					<td><?= $data->assigner ?></td>
	      				</tr>
	      				<tr>
	      					<td>Judul Tugas</td>
	      					<td><?= $data->judul_tugas ?></td>
	      				</tr>
	      				<tr>
	      					<td>Deskripsi</td>
	      					<td><?= $data->deskripsi ?></td>
	      				</tr>
	      				<tr>
	      					<td>Prioritas</td>
	      					<td><?= $data->priority ?></td>
	      				</tr>
	      				<tr>
	      					<td>Deadline</td>
	      					<td><?= $data->deadline ?></td>
	      				</tr>
	      			</tbody>
	      		</table>
	      		<?php if ($data->status=='1'): ?>
	      			<form action="" method="post" accept-charset="utf-8">
	      				<input type="hidden" name="selesai" value="y">	
	      				<button class="btn btn-lg btn-info">Tandai Selesai</button>
	      			</form>	
	      		<?php endif ?>
	      		
	      	</div>
	      	<form action="" method="post" accept-charset="utf-8">
	      	<div class="col-md-6">
		      	<div class="form-group">
		      		<label>Pesan Balik</label>
		      		<textarea name="in[msg]" class="form-control" required><?= $data->pesan ?></textarea>
		      	</div>
		      	
		      	<div class="form-group">
		      		<label>Link</label>
		      		<input type="text" name="in[link]" class="form-control" value="<?= $data->link ?>" placeholder="Masukkan link hasil tugas (optional)">
		      	</div>
		      	
		      	<button style="width:100%" class="btn btn-lg btn-primary">Submit</button>
		      </div>
		    </div>
		    </form>
	  </div>
	  

</div> <!--end container-->
<script type="text/javascript" src="<?= $data->base_url ?>js/jquery.js"></script>
<script type="text/javascript" src='https://fac-institute.com/js/jquery-ui/jquery-ui.js'></script>
</body>
</html>