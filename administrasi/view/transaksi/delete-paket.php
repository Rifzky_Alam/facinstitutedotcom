<!DOCTYPE html>
<html>


<?php include_once 'view/main-component/header.php'; ?>

<body>



<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
 ?>


<div class="container" id="isi" style="padding-bottom:40px;">
	<div class="page-header" id="top-logo">
		<h3>Anda yakin akan menghapus data ini ?</h3>
	</div>


        <div class='row'>
            <div class='col-md-8'>
                <table class='table table-bordered'>
                    <thead>
                        <tr>
                            <th class='tengah'>Nama Transaksi</th>
                            <th class="tengah">Nama Item</th>
                            <th class="tengah">Harga</th>
                            <th class="tengah">Qty</th>
                            <th class="tengah"><b>Total</b></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?php echo $data->no_transaksi ?></td>
                            <td><?php echo $data->item; ?></td>
                            <td class="tengah">Rp  <?php echo number_format($data->price); ?></td>
                            <td class="tengah"><?php echo $data->qty; ?></td>
                            <td class="tengah">Rp  <?php echo number_format($data->price * $data->qty) ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <form action='' method='post'>
            <div class='row' id='jajal'>
                <input value=<?php echo "'".$data->id_item."'"; ?> name='nomorid' style='display:none' />
            </div>


            <br>
            <a href="<?php echo 'new-perusahaan-info?tr='.$data->no_transaksi ?>" class="btn btn-lg btn-warning">Tidak</a>
            <button class='btn btn-lg btn-danger'>Iya</button>
        </form>
</div>

<script type="text/javascript" src=<?php echo "'".getBaseUrl()."js/jquery-ui/jquery-ui.js'"; ?>></script>
</body>
</html> 


