<?php 
session_start();
include_once 'session/session-class.php';
include_once '../baseurl.php';
include_once '../model/Transaksi.php';
$session = new Sessionz();
$session->AdminMarketing();
$transaksi = new Transaksi();

if (isset($_POST['input'])) {
	$tgl = $_POST['input']['tanggalz'];
	for ($i=0; $i < count($tgl) ; $i++) { 
		$transaksi->setTanggal($tgl[$i]);
		$transaksi->setID($_POST['input']['idTransaksi']);
	
		if ($transaksi->AddTanggal()) {
			echo "<script>alert('Tanggal transaksi berhasil disimpan!');</script>";
		} else {
			echo "<script>alert('Tanggal transaksi gagal disimpan!');</script>";
		}	
	}

}


if (isset($_GET['tr'])&&!empty($_GET['tr'])) {
	$no_transaksi = $_GET['tr'];
	$transaksi->setID($_GET['tr']);

	//data description
	$data = array(
	    'base_url' => getBaseUrl(),
	    'judul' => 'Kalender Transaksi - FAC Institute',
	    'username'=>$_SESSION['admin']['nama'],
	    'page' => 'new-transaksi-tanggal',
	    'no_transaksi' => $no_transaksi,
	    'agenda' => $transaksi->FetchAgendaForInvoice(),
	    'tanggal' => $transaksi->FetchTanggalByTransaction()
	);


	$data = (object) $data;
	print_r($data->agenda);
	include_once 'view/transaksi/tanggal.php';

}elseif(isset($_GET['del'])&&!empty($_GET['del'])){
	$transaksi->setTanggal($_GET['del']);
	$singleData = $transaksi->FetchByIdTanggal();
	if (isset($_POST['del'])) {
		$transaksi->setTanggal($_POST['del']['idTanggal']);
		if ($transaksi->deleteTanggal()) {
			echo "<script>alert('Tanggal transaksi berhasil dihapus!');</script>";
			echo "<script>location.replace('new-transaksi-tanggal?tr=".$singleData->acara."');</script>";
		}else{
			echo "<script>alert('Tanggal transaksi gagal dihapus!');</script>";
		}
	}

	$data = array(
	    'base_url' => getBaseUrl(),
	    'judul' => 'Kalender Transaksi - FAC Institute',
	    'username'=>$_SESSION['admin']['nama'],
	    'page' => 'new-transaksi-tanggal',
	    'no_transaksi' => $singleData->acara,
	    'tanggal' => $singleData->tanggal,
	    'id_tanggal' => $_GET['del']
	);
	$data = (object) $data;
	include_once 'view/transaksi/delete-tanggal.php';

}else{
	echo "<script>alert('Harap memilih data transaksi terlebih dahulu!');</script>";
    echo "<script>location.replace('data-order');</script>";
    
    $no_transaksi = "-";
}
?>