<!DOCTYPE html>
<html>


<?php include_once 'view/main-component/header.php'; ?>

<body>



<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3>Input Transaksi</h3>
	</div>
	<div class="row">
     <form action=""  method="post">

        <div class="row">
            <div class="col-md-10">
                <h4>Perusahaan: </h4>
                <?php if ($data->nama_usaha==''): ?>
                    <script type="text/javascript">alert('Nama perusahaan tidak ada, silahkan memilih nama perusahaan kembali!');</script>
                    <script type="text/javascript">location.replace('perusahaan');</script>
                    <?php else: ?>
                        <h3><?php echo $data->nama_usaha ?></h3>
                <?php endif ?>
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-md-10">
                <h4>Customer: </h4>
                <?php if ($data->nama_cust==''): ?>
                    <script type="text/javascript">alert('Nama customer tidak ada, silahkan memilih customer terlebih dahulu!');</script>
                    <script type="text/javascript">location.replace("new-perusahaan-info?c=<?php echo $data->nama_cust ?>");</script>
                    <?php else: ?>
                        <h3><?php echo $data->nama_cust  ?></h3>
                <?php endif ?>
            </div>
        </div>
        <br>

        <div class="row">
             <div class="col-md-10">
                 <div class="form-group">
                     <label>Jenis transaksi</label>
                     <select name="in[jtr]" class="form-control" required="">
                         <?php foreach ($data->dtjt as $key): ?>
                             <option value="<?= $key->jt_id ?>"><?= $key->jt_ket ?></option>
                         <?php endforeach ?>
                     </select>
                 </div>
             </div>
        </div>

        <div class="row">
             <div class="col-md-10">
                 <div class="form-group">
                     <label>Jenis Accurate</label>
                     <select name="in[jac]" class="form-control">
                        <option value="0">-- Pilih Jenis Accurate --</option>
                         <?php foreach ($data->listaccurate as $key): ?>
                             <option value="<?= $key['va_id'] ?>"><?= $key['va_nama'] ?></option>
                         <?php endforeach ?>
                     </select>
                 </div>
             </div>
        </div>

        <div class="row">
             <div class="col-md-10">
                 <div class="form-group">
                     <label>Tempat</label>
                     <textarea class="form-control" placeholder="Format: (Nama Perusahaan) Alamat" required name="in[tempat]"><?php echo $data->nama_usaha.'##'.$data->alamat_usaha ?></textarea>
                     <input type="hidden" name="in[idusaha]" value="<?= $data->idusaha ?>">
                 </div>
             </div>
         </div>

         <div class="row">
             <div class="col-md-10">
                 <div class="form-group">
                     <label>Waktu</label>
                     <input type="text" class="form-control" name="in[waktu]" placeholder="Format contoh: 08:00-16:00" value="09:00 - 16:00"/>
                 </div>
             </div>
         </div>

         <div class="row">
             <div class="col-md-10">
                 <div class="form-group">
                     <label>DP</label>
                     <input type="text" class="form-control" name="in[depe]" placeholder="Angka tidak ada titik dan koma" value="0"/>
                 </div>
             </div>
         </div>

         <div class="row">
             <div class="col-md-10">
                 <div class="form-group">
                     <label>PPN (%)</label>
                     <input type="text" class="form-control" name="in[ppn]" placeholder="range dari 1-100" value="0"/>
                 </div>
             </div>
         </div>

         <div class="row">
             <div class="col-md-10">
                <div class="form-group">
                    <label>Discount (%)</label>
                     <input type="text" class="form-control" name="in[discount]" placeholder="range dari 1-100" value="0" />
                </div>  
             </div>
         </div>

         <div class="row">
             <div class="col-md-10">
                <div class="form-group">
                    <label>Notes</label>
                     <input type="text" class="form-control" name="in[notes]" placeholder="pisahkan dengan ##, contoh: notes1##notes2" />
                </div>  
             </div>
         </div>

        <div class="row">
             <div class="col-md-10">
                <div class="form-group">
                    <label>Keterangan</label>
                     <textarea name="in[keterangan]" class="form-control" placeholder="Sebagai Syarat Dan Ketentuan  Entry Data"></textarea>
                </div>  
             </div>
         </div>         

         
         <div class="row">
             <div class="col-md-10">
                <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>        
             </div>
         </div>
         
     </form>   
    </div>

</div><!--end container-->
</body>
</html> 