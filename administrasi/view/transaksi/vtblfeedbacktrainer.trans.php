<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
    <div class="page-header" id="top-logo">
        <h3><?= $data->subtitle ?></h3>
    </div>

    <br>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="tengah">Nama Perusahaan</th>
                        <th class="tengah">Nama Customer</th>
                        <th class="tengah">Nama Trainer</th>
                        <th class="tengah">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($data->listdata)=='0'): ?>
                        <tr>
                            <td colspan="4" style="text-align:center;">Tidak Ada data di dalam database kami</td>
                        </tr>
                    <?php else: ?>
                        <?php foreach ($data->listdata as $key): ?>
                            <tr>
                                <td class="tengah"><?= $data->nama_usaha ?></td>
                                <td class="tengah"><?= $data->namacust ?></td>
                                <td class="tengah"><?= $key['nama'] ?></td>
                                <td class="tengah">
                                    <a href="#" title="Email Feedback">Kirim Via Email</a>
                                    ||
                                    <a href="#" title="Whatsapp Feedback" onclick="<?= 'LinkWhatsapp('.EditPhoneNumberForDesktop($key['telp_cust']).",'".AES256($key['email_user'],'e')."')" ?>">Kirim Via Whatsapp</a>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                    
                </tbody>
            </table>  
        </div> 
    </div>

</div><!--end container-->


<!-- Modal -->
  <div class='modal fade' id='modal-telp' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pencarian Data</h3>

        </div>
        <div class='modal-body'>
        <div style='padding-left:15px;padding-right:15px;padding-top:0px; height: 300px;overflow-y:auto;' class='row'>
          


          <div class="col-md-12">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="tengah">
                            No Telp
                        </th>
                        <th class="tengah">
                            Aksi
                        </th>
                    </tr>
                </thead>
                <tbody id="bodytelp">
                    
                </tbody>
            </table>
          </div>
             
          

        </div>
          

        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>FAC-Institute 2019</span>
                </div>
                <div class="col-md-6">
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->
  <script>
      function CallModalTelp(id) {
        $.ajax({
          type: "GET",
          url: "https://fac-institute.com/administrasi/data-transaksi/api",
          data: {
            'getphones':id
          },
          cache: false,
          success: function(data){
            console.log(data);
            $('#bodytelp').html("<tr><td>"+data.data.telpcust+"</td><td><a href='#'>Kirim</a></td></tr><tr><td>"+data.data.telpusaha+"</td><td><a href='#'>Kirim</a></td></tr>");
            // alert(data.data.telpcust);
            $('#modal-telp').modal('show');
          }
        }); //end ajax
      }


      function removeElement(id) {
        $('#'+id).remove();
    }

    function LinkWhatsapp(nomornya,emailtrainer){
        
      if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        var win = window.open("https://wa.me/"+nomornya+"?text=Perkenalkan%20kami%20dari%20FAC%20Institute.%0ATerimakasih%20telah%20menggunakan%20Jasa%20Training%20Accurate%20di%20FAC%20Institute.%0ADemi%20peningkatan%20kualitas%20pelayanan%2C%20mohon%20luangkan%20waktu%20untuk%20mengisi%20survey%20tentang%20trainer%20accurate.%20Hanya%201%20menit%20untuk%20menyelesaikan%20survey%20ini.%20%20%0ATerima%20kasih%20atas%20partisipasi%20Anda.%0ASilahkan%20isi%20survei%20tersebut%20pada%20link%20di%20bawah%20ini%20dibawah%20ini%20%3A%0Ahttps%3A%2F%2Ffac-institute.com%2Ftraining%2Ffeedback%2F<?= AES256($data->idtrans,'e') ?>%2F"+emailtrainer, "_blank");
            win.focus();
         //   $('#whatsappz').prop('href', );
      }else{
          var win = window.open("https://web.whatsapp.com/send?phone=+"+nomornya+"&text=Perkenalkan%20kami%20dari%20FAC%20Institute.%0ATerimakasih%20telah%20menggunakan%20Jasa%20Training%20Accurate%20di%20FAC%20Institute.%0ADemi%20peningkatan%20kualitas%20pelayanan%2C%20mohon%20luangkan%20waktu%20untuk%20mengisi%20survey%20tentang%20trainer%20accurate.%20Hanya%201%20menit%20untuk%20menyelesaikan%20survey%20ini.%20%20%0ATerima%20kasih%20atas%20partisipasi%20Anda.%0ASilahkan%20isi%20survei%20tersebut%20pada%20link%20di%20bawah%20ini%20dibawah%20ini%20%3A%0Ahttps%3A%2F%2Ffac-institute.com%2Ftraining%2Ffeedback%2F<?= AES256($data->idtrans,'e') ?>%2F"+emailtrainer, "_blank");
            win.focus();
    //    $('#whatsappz').prop('href', );
        // alert('Web detected!');
      }
    }
  </script>

</body>
</html> 