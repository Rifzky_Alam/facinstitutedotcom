<!DOCTYPE html>
<html>


<?php include_once 'view/main-component/header.php'; ?>

<body>
<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
?>
<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3>Input Customer</h3>
	</div>
	<div class="row">
     <form action=""  method="post">

        <div class="row">
            <div class="col-md-10">
                <h4>Perusahaan: </h4>
                <h3><?php echo $data->nama_usaha ?></h3>
                <?php if ($data->nama_usaha==''): ?>
                    <script type="text/javascript">alert('Nama perusahaan tidak ada, silahkan memilih nama perusahaan kembali!');</script>
                    <script type="text/javascript">location.replace('perusahaan');</script>
                    <?php else: ?>

                <?php endif ?>
            </div>
        </div>
        <br><br>

        <div class="row">
             <div class="col-md-10">
                <div class="form-group">
                    <label><a href="new-marketing?action=new" target="_blank">Marketing</a></label>
                    <select name="in[mi]" class="form-control">
                        <?php foreach ($data->petugas as $key ) { ?>
                            <?php if ($key->marketing_id=='fac-website'): ?>
                                <option selected value="<?php echo $key->marketing_id ?>">
                                    <?php echo $key->marketing_nama ?>
                                    </option>
                                <?php else: ?>
                                <option value="<?php echo $key->marketing_id ?>"><?php echo $key->marketing_nama ?></option>
                            <?php endif ?>
                            
                        <?php } ?>
                    </select>
                </div>  
             </div>
         </div>

         <div class="row">
             <div class="col-md-10">
                 <div class="form-group">
                     <label>Nama Customer</label>
                     <input type="text" class="form-control" name="in[nama]" required/>
                 </div>
             </div>
         </div>
         
         <div class="row">
             <div class="col-md-10">
                 <div class="form-group">
                     <label>Jenis Kelamin</label>
                     <select name="in[gender]" class="form-control" required>
                         <option value="">--Pilih Jenis Kelamin--</option>
                         <option value="1">Pria</option>
                         <option value="2">Wanita</option>
                     </select>
                 </div>
             </div>
         </div>

         <div class="row">
             <div class="col-md-10">
                <div class="form-group">
                    <label>Email Customer</label>
                     <input type="email" class="form-control" name="in[email]"/>
                </div>  
             </div>
         </div>

         <div class="row">
             <div class="col-md-10">
                <div class="form-group">
                    <label>Telepon</label>
                     <input type="text" class="form-control" name="in[telepon]"/>
                </div>  
             </div>
         </div>

         <div class="row">
             <div class="col-md-10">
                <div class="form-group">
                    <label>Jabatan</label>
                     <input type="text" class="form-control" name="in[jabatan]"/>
                </div>  
             </div>
         </div>

         <div class="row">
             <div class="col-md-10">
                <label>Jenis Pengguna</label>
                 <select name="in[jp]" class="form-control">
                     <option value="1">Lama</option>
                     <option value="2">Baru</option>
                 </select>
             </div>
         </div>
         <br>
         
         <div class="row">
             <div class="col-md-10">
                <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>        
             </div>
         </div>
         
     </form>   
    </div>

</div><!--end container-->
</body>
</html> 