<!DOCTYPE html>
<html>


<?php include_once 'view/main-component/header.php'; ?>

<body>



<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
 ?>


<div class="container" id="isi" style="padding-bottom:40px;">
	<div class="page-header" id="top-logo">
		<h3>Transaksi - <?php echo $data->no_transaksi ?></h3>
	</div>


        <div class='row' style="margin-bottom:30px;">
            <div class='col-md-8'>
                <table class='table table-bordered'>
                    <thead>
                        <tr>
                            <th class="tengah">Paket</th>
                            <th class="tengah">Qty</th>
                            <th class="tengah">Harga</th>
                            <th class="">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php TabelItem($data->items) ?>
                    </tbody>
                </table>
            </div>
        </div>

        <form action='' method='post'>
            <div class='row' id='jajal'>
                <div class="col-md-10">
                    <div class="form-group">
                        <label><a href="input-paket" target="_blank">Nama Paket</a></label>
                        <select class="form-control" name="in[paket]">
                            <?php foreach ($data->paket as $key) { ?>
                                <option value="<?php echo $key->id ?>"><?php echo $key->nama_item. '  --  Rp '.number_format($key->harga) ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Qty</label>
                        <input type="number" name="in[qty]" value="1" class="form-control">
                    </div>                    
                    <button class="btn btn-lg btn-primary">Masukkan Data</button>
                    <a class="btn btn-lg btn-warning" href="new-perusahaan-info?tr=<?php echo $data->no_transaksi ?>">Back to transaction</a>
                </div>
            </div>

        </form>
</div>

<script type="text/javascript" src=<?php echo "'".getBaseUrl()."js/jquery-ui/jquery-ui.js'"; ?>></script>
</body>
</html> 
<?php function TabelItem($data){ ?>
    <?php if (count($data)=='0'): ?>
        <tr>
            <td colspan="2" class="tengah">Tidak ada data item/paket pada transaksi ini.</td>
        </tr>
    <?php else: ?>
        <?php foreach ($data as $key ) { ?>
            <tr>
                <td class="tengah"><?php echo $key->nama_item ?></td>
                <td class="tengah"><?php echo $key->itm_qty ?></td>
                <td class="tengah"><?php echo 'Rp '.number_format($key->harga) ?></td>
                <td class="tengah">
                    <a href="new-transaksi-paket?del=<?php echo $key->no ?>" class="btn btn-danger">
                        Delete
                    </a>
                </td>
            </tr>
        <?php } ?>
    <?php endif ?>
    <?php } ?>

