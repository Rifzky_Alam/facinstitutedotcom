<?php function tabelCustomer($data){ ?>
	<?php if (count($data)=='0'): ?>
			<table class="table table-bordered" style="margin-top:15px;">
				<thead>
					<tr>
						<th>Nama Customer</th>
						<th>Telepon</th>
						<th>Email</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td colspan="4">
							Tidak ada data customer untuk perusahaan ini. 
						</td>
					</tr>
				</tbody>
			</table>
		<?php else: ?>
			<table class="table table-bordered" style="margin-top:15px;">
				<thead>
					<tr>
						<th>Nama Customer</th>
						<th>Telepon</th>
						<th>Email</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($data as $key) { ?>
					
					    <?php if($key->status_cust=='51'): ?>
					    <tr class="warning">
					        <td><?php echo $key->nama ?> (Support)</td>
					    <?php else: ?>
					    <tr>
					        <td><?php echo $key->nama ?></td>
					    <?php endif ?>
						
						<td><?php echo $key->telepon ?></td>
		        		<td><?php echo $key->email ?></td>
		        		<td>
		        			<a href="new-transaksiz?c=<?php echo $key->id ?>" class="btn btn-success">
		        				Transaksi >>
		        			</a>
		        			<a href="<?php echo 'new-perusahaan-info?c='.$key->id ?>" class="btn btn-info">
		        				Detail >>
		        			</a>
		        			<a href="new-customer?edt=<?php echo $key->id ?>" class="btn btn-warning">Edit</a>
		        		</td>
            		</tr>	
					<?php } ?>
					
				</tbody>
    		</table>
	<?php endif ?>
<?php } ?>

<?php function tabelTransaksi($data){ ?>

	<?php if (count($data)=='0'): ?>
		<table class="table table-bordered">
			<thead>
				<tr>
                	<th style="text-align:center;">ID Transaksi</th>
                	<th style="text-align:center;">Tanggal</th>
	            	<th style="text-align:center;">Petugas</th>
	            	<th style="text-align:center;">Aksi</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="4">
						Tidak ada data transaksi untuk customer ini, silahkan untuk menambah data terlebih dahulu.
					</td>
				</tr>
			</tbody>
		</table>

		<?php else: ?>
			<table class="table table-bordered">
				<thead>
					<tr>
                		<th>ID Transaksi</th>
                		<th>Tanggal</th>
	            		<th>Petugas</th>
	            		<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($data as $key) { ?>
					<tr>
             			<td><?php echo $key->trans_id ?></td>
	            		<td><?php echo $key->trans_date ?></td>
	            		<td><?php echo $key->nama ?></td>
	            		<td>
	            			<a href="new-transaksiz?edt=<?php echo $key->trans_id ?>">Edit</a> ||
							<a href="new-quotation?tr=<?php echo $key->trans_id ?>">Penawaran</a> ||
		            		<a href="new-invoice?tr=<?php echo $key->trans_id ?>">Invoice</a> ||
							<a href="new-perusahaan-info?tr=<?php echo $key->trans_id ?>">Detail</a>		            		                               
			    		</td>
					</tr>	
					<?php } ?>
				</tbody>
			</table>
	<?php endif ?>
	
<?php } ?>

<?php function tabelAgenda($data){ ?>
	<?php if (count($data)=='0'): ?>
		<table class="table table-bordered">
				<thead>
		            <tr>
		        	    <th style="text-align:center;">No</th>
		                <th style="text-align:center;">Agenda</th>
		    	        <th style="text-align:center;">Aksi</th>
			        </tr>
				</thead>
		        <tbody>
		        	<tr>
		        		<td colspan="3">
		        			Tidak ada data agenda pada transaksi ini, silahkan untuk klik detail transaksi terlebih dahulu atau tambah data.
		        		</td>
		        	</tr>
		        </tbody>
		</table>
		<?php else: ?>
			<table class="table table-bordered">
				<thead>
		            <tr>
		                <th>Agenda</th>
		    	        <th>Aksi</th>
			        </tr>
				</thead>
		            <tbody>
		            	<?php foreach ($data as $key) { ?>
		            		<tr>
				                <td><?php echo $key->nama_agenda ?></td>
								<td>
									<a href="new-transaksi-agenda?del=<?php echo $key->agd_id ?>" class="btn btn-danger" style="width:100%">
										Delete
									</a>
								</td>
							</tr>
		            	<?php } ?>
					</tbody>
			</table>
	<?php endif ?>
	
<?php } ?>

<?php function tabelItem($data,$base_url='https://fac-institute.com/'){ ?>
	<?php if (count($data)=='0'): ?>
		<table class="table table-bordered">
			<thead>
		            <tr>
		                <th style="text-align:center;">No</th>
		                <th style="text-align:center;">Nama Paket</th>
		                <th style="text-align:center;">Biaya</th>
		                <th style="text-align:center;">Qty</th>
		                <th style="text-align:center;">Aksi</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td colspan="5">
							Tidak ada data agenda pada transaksi ini, silahkan untuk klik detail transaksi terlebih dahulu atau tambah data.
						</td>
					</tr>
				</tbody>
		</table>
		<?php else: ?>
			<table class="table table-bordered">
				<thead>
		            <tr>
		                <th>Nama Paket</th>
		                <th>Biaya</th>
		                <th>Qty</th>
		                <th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					foreach ($data as $key) { ?>
					<tr>
		                <td>
		                	<a href="<?= $base_url.'administrasi/new-transaksiz?detitem='.$key->aidi ?>" title="Klik Untuk Detail">
		                		<?php echo $key->nama_item ?>
		                	</a>
		                </td>
		                <td>Rp <?php echo number_format($key->harga) ?></td>
		                <td><?php echo $key->itm_qty ?></td>
		                <td><a href="new-transaksi-paket?del=<?php echo $key->no ?>" class="btn btn-danger" style="width:100%">Delete</a></td>
		            </tr>
					<?php } ?>

							            
				</tbody>
			</table>
	<?php endif ?>
	
<?php } ?>

<?php function tabelTanggal($data){ ?>
	<?php if (count($data)=='0'): ?>
		<table class="table table-bordered">
			<thead>
			    <tr>
		           	<th style="text-align:center;">Tanggal</th>
					<th style="text-align:center;">Aksi</th>
				</tr>
		    </thead>
		    <tbody>
		    	<tr>
		    		<td colspan="2">
		    			Tidak ada data agenda pada transaksi ini, silahkan untuk klik detail transaksi terlebih dahulu atau tambah data.
		    		</td>
		    	</tr>
		    </tbody>
		</table>
		<?php else: ?>
			<table class="table table-bordered">
				<thead>
			        <tr>
		                <th>Tanggal</th>
		                <th>Aksi</th>
		            </tr>
		        </thead>
		        <tbody>
		        	<?php foreach ($data as $key) { ?>
		        	<tr>
		                <td><?php echo $key->tanggal ?></td>
		                <td>
		                	<a href="new-transaksi-tanggal?del=<?php echo $key->id ?>" class="btn btn-danger" style="width:100%">
		                		Delete
		                	</a>
		                </td>
		            </tr>	
		        	<?php } ?>
		        </tbody>
			</table>
	<?php endif ?>
<?php } ?>

<?php function tabelInvoice($data){ ?>
	<?php if (count($data)=='0'): ?>

		<?php else: ?>
		        <br><br>
                <h4>Invoice</h4>

                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th style="vertical-align: middle;">No Invoice</th>
                            <th style="vertical-align: middle;">Deskripsi</th>
                            <th style="vertical-align: middle;">M. Bayar</th>
                            <th style="vertical-align: middle;">Tanggal</th>
                            <th style="vertical-align: middle;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>

                    	<?php foreach ($data as $key) { ?>
						<tr>
                            <td>
                            	
                            		<?php echo $key->inv_no_invoice ?>
                            			
                            	
                            </td>
                            <td><?php echo $key->inv_deskripsi ?></td>
                            <td><?php echo $key->inv_metode_bayar ?></td>
                            <td><?php echo TanggalInfoFromTimestamp($key->inv_date_created) ?></td>
                            <td>
                            	<a href="new-attachments?inv=<?php echo $key->inv_no_invoice ?>">Kirim</a> ||
                            	<a href="new-invoice?edt=<?php echo $key->inv_no_invoice ?>">Edit</a> ||
                            	<a href="new-preview?inv=<?php echo $key->inv_no_invoice ?>" id="in" target="_blank">PDF</a> ||


                            	<?php echo GetPreviewLink($key->trans_jenis,$key->inv_no_invoice) ?>
                            </td>
                        </tr>

                    	<?php } ?>
					</tbody>
                </table>			
	<?php endif ?>
<?php } ?>


<?php function tabelQuotation($data){ ?>
	<?php if (count($data)=='0'): ?>

		<?php else: ?>
		        <br><br>
                <h4>Penawaran</h4>

                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th class="tengah">No Penawaran</th>
                            <th class="tengah">Tanggal</th>
                            <th class="tengah">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>

                    	<?php foreach ($data as $key) { ?>
						<tr>
                            <td>
                           		<?php echo $key->qtn_id ?>	
                            </td>
                            <td><?php echo @$key->qtn_date ?></td>
                            <td>
                            	<a target="_blank" href="new-quotation?edt=<?php echo $key->qtn_id ?>">Edit</a> ||
                            	<a target="_blank" href="new-attachments?quot=<?php echo $key->qtn_id ?>">Kirim</a> ||
                            	<a target="_blank" href="new-preview?quot=<?php echo $key->qtn_id ?>">PDF</a> ||
                            	<a target="_blank" href="new-preview?equot=<?php echo $key->qtn_id ?>">Prev-Email</a>
                            </td>
                        </tr>

                    	<?php } ?>
					</tbody>
                </table>			
	<?php endif ?>
<?php } ?>



<?php 

function GetPreviewLink($value,$id){
	if ($value=='1') {
		return "<a href='new-preview?einv=$id'>Prev-Email</a>";
	}elseif ($value=='2') {
		return "<a href='new-preview?asinv=$id'>Prev-Email</a>";
	}elseif ($value=='3') {
		return "<a href='new-preview?kinv=$id'>Prev-Email</a>";
	}else{
		return "<a href='#'>Preview not available</a>";
	}
	
}

 function TanggalInfoFromTimestamp($value){
		$value = explode(' ', $value);
		$tgl= explode('-', $value[0]);
		return $tgl[2].' '.IndonesianMonth($tgl[1]).' '.$tgl[0];
}

function IndonesianMonth($value){
	if ($value=='1') {
		return ' Januari ';
	}elseif ($value=='2') {
		return ' Februari ';
	}elseif ($value=='3') {
		return ' Maret ';
	}elseif ($value=='4') {
		return ' April ';
	}elseif ($value=='5') {
		return ' Mei ';
	}elseif ($value=='6') {
		return ' Juni ';
	}elseif ($value=='7') {
		return ' Juli ';
	}elseif ($value=='8') {
		return ' Agustus ';
	}elseif ($value=='9') {
		return ' September ';
	}elseif ($value=='10') {
		return ' Oktober ';
	}elseif ($value=='11') {
		return ' November ';
	}elseif ($value=='12') {
		return ' Desember ';
	}
}
?>