<?php 
function EmailContent($data){
    return '
    <table class="m_-5588023840110578919m_3185550004040353724featured-story m_-5588023840110578919m_3185550004040353724featured-story--top" cellspacing="0" cellpadding="0">
    <tbody><tr>
        <td style="padding-bottom:20px">
            <table cellspacing="0" cellpadding="0">
                <tbody><tr>
                    <td class="m_-5588023840110578919m_3185550004040353724featured-story__inner" style="background:#fff">
                        <table cellspacing="0" cellpadding="0">
                            <tbody><tr>
                                <td class="m_-5588023840110578919m_3185550004040353724featured-story__content-inner" style="padding:32px 30px 45px">
                                    <table cellspacing="0" cellpadding="0">
                                        <tbody><tr>
<td class="m_-5588023840110578919m_3185550004040353724featured-story__heading m_-5588023840110578919m_3185550004040353724featured-story--top__heading" style="background:#fff" width="640" align="left">
                                                <table cellspacing="0" cellpadding="0">
                                                    <tbody><tr>
                                                        <td style="font-family:Geneva,Tahoma,Verdana,sans-serif;font-size:22px;color:#464646" width="600" align="left">
                                                            <a href="#" style="text-decoration:none;color:#464646">Notification</a>
                                                        </td>
                                                    </tr>
                                                </tbody></table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="m_-5588023840110578919m_3185550004040353724featured-story__copy" style="background:#fff" width="640" align="left">
                                                <table cellspacing="0" cellpadding="0">
                                                    <tbody><tr>
                                                        <td style="font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;font-size:16px;line-height:22px;color:#555555;padding-top:16px" align="left">
                                                        Dear '.$data->nama_cust.', <br>
                                                        Kami dari FAC Institute sebagai divisi HR (Human Resource) Department. <br>Terimakasih telah menggunakan Jasa Training Accurate di FAC Institute. Demi peningkatan kualitas pelayanan, mohon luangkan waktu untuk mengisi survey tentang trainer accurate. Hanya 1-3 menit untuk menyelesaikan survey ini.<br>Terima kasih atas partisipasi Anda. <br><br><b>FAC WEB SYSTEM</b>
                                                        </td>
                                                    </tr>
                                                </tbody></table>
                                            </td>
                                        </tr>
                                    </tbody></table>
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>
    ';
}