<!DOCTYPE html>
<html>


<?php include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/view/main-component/header.php'; ?>

<body>



<?php 
include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/view/main-component/sidebar.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->subtitle ?></h3>
	</div>

    <div class="row" >
        <div class="col-md-12" style="text-align:right;">
            <a class="btn btn-info" data-toggle="modal" href="#modal-cari">Cari Data?</a>
        </div>
    </div>


    <div class="row" style="overflow:auto;">
        <div class="col-md-12">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="tengah">Perusahaan</th>
                        <th class="tengah">Jumlah Hari Training</th>
                        <th class="tengah">Maksimal Hari</th>
                        <th class="tengah">Outstanding</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(count($data->listdata)=='0'): ?>
                        <tr>
                            <td colspan="4" style="text-align:center;">Tidak ada data outstanding dalam database kami.</td>
                        </tr>
                    <?php else: ?>
                        <?php $total = 0; ?>
                        <?php foreach($data->listdata as $key): ?>
                        
                        <tr>
                            <td>
                                <?= $key['nama_usaha'] ?>   
                            </td>
                            <td style="text-align:center;">
                                <?= $key['total_hari'] ?>   
                            </td>
                            <td style="text-align:center;">
                                <?= $key['expected_day'] ?>   
                            </td>
                            <td style="text-align:center;">
                                <?= $key['outday'] ?>
                                <?php $total += $key['outday'] ?>
                            </td>
                        </tr>
                        
                        <?php endforeach ?>
                        <tr>
                            <td colspan="3"><b>Total</b></td>
                            <td style="text-align:center;"><b><?= $total ?></b></td>
                        </tr>
                    <?php endif ?>
                </tbody>
            </table>
        </div>
    </div>
	<?php //Pagination($data->url,30,$data->pagenum,$data->totaldata,5) ?>

</div><!--end container-->


<!-- Modal -->
  <div class='modal fade' id='modal-cari' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pencarian Data</h3>

        </div>
        <div class='modal-body'>
        <div style='padding-left:15px;padding-right:15px;padding-top:0px; height: 300px;overflow-y:auto;' class='row'>
          


          <div class="col-md-12">
            <form action="" method="get">
            <div class="row">
                <div class="form-group">
                    <label>Bulan</label>
                    <input type="text" class="form-control" name="src[m]" placeholder="ketik bulan dari 1-12">
                </div>
                <div class="form-group">
                    <label>Tahun</label>
                    <input type="text" class="form-control" name="src[y]" placeholder="ketik tahun, contoh: 2019">
                </div>
            </div>

            <div class="row">
                <button class="btn btn-lg btn-success" style="width:100%">Cari</button>
            </div> 
            </form>
          </div>
             
          

        </div>
          

        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>FAC-Institute 2019</span>
                </div>
                <div class="col-md-6">
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->
<script type="text/javascript" src='https://fac-institute.com/js/jquery-ui/jquery-ui.js'></script>
    <script type="text/javascript">
    $(document).ready(function(){
        $('#tglawal').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('#tglakhir').datepicker({
            dateFormat: 'yy-mm-dd'
        });
    });
    </script>
</body>
</html>


<?php function Rows($data){ ?>
    <?php if (count($data)=='0'): ?>
        <tr>
            <td colspan="6">Tidak ada data yang masuk ke dalam sistem kami.</td>
        </tr>
    <?php else: ?>
        <?php foreach ($data as $key) { ?>
            <tr>
                <td style="vertical-align: middle;"><?php echo $key->nama_cust ?></td>
                <td style="vertical-align: middle;"><a target="_blank" href="new-detail?tr=<?php echo $key->trans_id ?>"><?php echo $key->nama_usaha ?></a></td>
                <td style="vertical-align: middle;"><?php echo $key->jt_ket ?></td>
                <td style="vertical-align: middle;">
                    <?php $items = explode('##', $key->items) ?>
                    <?php if ($key->items!=''): ?>
                    <ul style="padding-left:15px">
                    <?php foreach ($items as $keyz) {
                        echo "<li>".$keyz."</li>";
                    }  ?>
                    </ul>
                    <?php else: ?>
                        <span style="color:red">Not Available</span>
                    <?php endif ?>
                    
                </td>
                <td style="vertical-align: middle;">
                    <?php $agendas = explode('##', $key->agendas) ?>
                    <?php if ($key->agendas!=''): ?>
                    <ul style="padding-left:15px">
                    <?php foreach ($agendas as $keyz) {
                        echo "<li>".$keyz."</li>";
                    }  ?>
                    </ul>
                    <?php else: ?>
                        <span style="color:red">Not Available</span>
                    <?php endif ?>
                </td>
                <td style="vertical-align: middle;"><?php echo $key->jumlah_hari ?></td>
                <td style="vertical-align: middle;">
                    <?php $tanggal = explode('##', $key->tgl) ?>
                    <?php if ($key->tgl!=''): ?>
                    <ul style="padding-left:15px">
                    <?php foreach ($tanggal as $keyz) {
                        echo "<li>".$keyz."</li>";
                    }  ?>
                    </ul>
                    <?php else: ?>
                    <span style="color:red">Not Available</span>
                    <?php endif ?>
                    
                </td>
                <td style="vertical-align: middle;">
                    <a href="new-perusahaan-info?tr=<?php echo $key->trans_id ?>">view</a>
                    ||
                    <a href="new-transaksiz?edt=<?php echo $key->trans_id ?>">edit</a>
                    ||
                    <a href="new-transaksiz?newdate=<?php echo $key->trans_id ?>">more date</a>
                    ||
                    <a href="new-transaksi-agenda?tr=<?php echo $key->trans_id ?>">more agenda</a>
                    ||
                    <a href="new-transaksi-paket?tr=<?php echo $key->trans_id ?>">more items</a>
                    ||
                    <a href="googlecal/summarylist?tr=<?= $key->trans_id ?>">Send to Google Calendar</a>
                </td>
            </tr>
        <?php } ?>
    <?php endif ?>
<?php } ?>

<?php function Pagination($baseurl,$limit,$page,$totaldata,$limitlist){ ?>
    <?php if ($page!='0'): ?>
        <div class="row">
            <div class="col-md-12">
                <center>
        <?php if (intval($page)*$limit<$totaldata): ?>
            Showing data <? echo (intval($page)*$limit)-$limit . ' - ' . intval($page)*$limit . ' of '.$totaldata;?>    
            <?php else: ?>
            Showing data <? echo intval($page). ' - ' . $totaldata . ' rows of '.$totaldata;?>
            <?php endif ?>
                </center>
            </div>
        </div>
    <?php else: ?>
        <div class="row">
            <div class="col-md-12">
                <center>
                    <?php if ($totaldata<=$limit): ?>
                    Showing data 0 - <?php echo $totaldata ?> rows of <?php echo $totaldata; ?>
                        <?php else: ?>
                    Showing data 0 - <?php echo $limit ?> rows of <?php echo $totaldata; ?>
                    <?php endif ?>
                    
                </center>
            </div>
        </div>
    <?php endif ?>


    <div class="row">
        <div class="col-md-12">
        <center>
            <ul class="pagination pagination-sm">

            <?php 

                // $limitlist = 5;
                if (isset($_GET['page'])) {
                    $page = $_GET['page'];
                } else {
                    $page = 0;
                }
                
                
                $b = intval($page/$limitlist);
                $c = ($b + 1) * $limitlist;
                $batas = intval($totaldata) / $limit;

                if ($page >= $limitlist) {
                    if ($page == $limitlist) {
                        $prev = $b * $limitlist - 2;
                    } else {
                        $prev = $b * $limitlist -1;
                    }

                    echo "<li><a href='".$baseurl."?page=$prev'>< Prev</a></li>";
                }

                for ($i=$b*$limit-1; $i < $c; $i++) { 
                    $j = $i +1;

                    if ($i<$batas&&$j!=0) {
                        echo "<li><a href='".$baseurl."?page=$j'>$j</a></li>";    
                    }
                }
                
                if ($c < $batas) {
                    $k = $c + 1;
                    echo "<li><a href='".$baseurl."?page=$k'>Next ></a></li>";
                }
                
                // for ($i=1; $i < intval($totalRow) / 30 + 1 ; $i++) { 
                    // echo "<li><a href='".basename(__FILE__, '.php')."?page=$i'>$i</a></li>";   
                // }
            ?>
            </ul>
        </center>
        </div>
    </div>

<? } //end pagination ?>
