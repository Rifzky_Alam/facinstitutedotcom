<!DOCTYPE html>
<html>
<?php include_once 'view/main-component/header.php'; ?>
<body>
<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
include_once 'view/transaksi/tabel-info-perusahaan.php';
 ?>


<?php 
    if (empty($data->id_usaha)) {
        //bisa diganti dengan harus memilih data customer atau data perusahaan -not done yet
        echo "<script>alert('Harap memilih nama perusahaan terlebih dahulu!');</script>";
        // echo "<script>location.replace('perusahaan');</script>";
    }
?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">

		<h3><a href="new-usaha?id=<?php echo $data->id_usaha ?>"><?php echo $data->nama_usaha ?></a></h3>
	</div>
    
	<div class="row" style="overflow:auto;">
        <div class="col-md-12">
            <h4>List Customer</h4>
            <a href="<?php echo 'new-customer?p='.$usaha ?>" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span></a>
            <?php tabelCustomer($datacustomer) ?>
        </div>   
    </div>
    <br>

    <div class="row" style="overflow:auto;">
        <div class="col-md-12">
            <div class="col-md-6" style="padding-left:0px;">
                <h4>Transaksi</h4>
                <?php  ?>
                <?php tabelTransaksi($datatransaksi) ?>
                <?php if ($data->pelanggan!='-'): ?>
                    <a href="new-transaksiz?c=<?php echo $data->pelanggan ?>" class="btn btn-lg btn-warning" style="width:100%">Add more transaksi</a>
                <?php else: ?>
                    <button class="btn btn-lg btn-warning" style="width:100%" disabled>Pilih customer terlebih dahulu.</button>
                <?php endif ?>

                <?php tabelInvoice($data->invoices) ?>
                <?php tabelQuotation($data->penawaran) ?>

                <br>
                <?php if ($fileinv==''): ?>
                    <?php else: ?>
                        <h5><b>File</b></h5>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Nama File</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?= $fileinv ?></td>
                                    <td style="text-align:center;">
                                        <a href="<?= getBaseUrl().'administrasi/invoices/'.$fileinv ?>" title="">Lihat</a>
                                        ||
                                        <a href="<?= getBaseUrl().'administrasi/new-perusahaan-info?delinv='.$fileinv ?>" title="">
                                            <span class="glyphicon glyphicon-remove"></span>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                <?php endif ?>
            </div>

            <!--DESCRIPTION-->
            <div class="col-md-6">
                <h4>Description</h4>
                
                <h5><b>Agenda</b></h5>
                <?php tabelAgenda($data->agenda) ?>
                <?php if ($data->id_transaksi!='-'): ?>
                <a href="<?php echo 'new-transaksi-agenda?tr='.$data->id_transaksi ?>" class="btn btn-lg btn-primary" style="width:100%">Add more agenda</a>    
                    <?php else: ?>
                        <button class="btn btn-lg btn-primary" style="width:100%" disabled>Pilih transaksi terlebih dahulu.</button>
                <?php endif ?>
                
                <br><br>
                
                <h5><a href="<?= $data->base_url.'administrasi/data-transaksi/items' ?>" title="Lihat Daftar Item Transaksi"><b>Item transaksi</b></a></h5>
                <?php tabelItem($data->items) ?>
                <?php if ($data->id_transaksi!='-'): ?>
                <a href="new-transaksi-paket?tr=<?php echo $data->id_transaksi ?>" class="btn btn-lg btn-success" style="width:100%">
                    Add more item
                </a>
                <?php else: ?>
                    <button class="btn btn-lg btn-success" style="width:100%" disabled>Pilih transaksi terlebih dahulu.</button>
                <?php endif ?>
                
                <br><br>

                <h5>
                    <b>Tanggal</b> || <a href="<?= $data->base_url.'administrasi/googlecal/summarylist?tr='.$data->id_transaksi ?>" title="Google Calendar Data">Google Cal</a>
                </h5>
                <?php tabelTanggal($data->tanggal) ?>
                <?php if ($data->id_transaksi!='-'): ?>
                    <a href="new-transaksiz?newdate=<?php echo $data->id_transaksi ?>" class="btn btn-lg btn-info" style="width:100%">Add more date</a>
                    <?php else: ?>
                    <button class="btn btn-lg btn-info" style="width:100%" disabled>Pilih transaksi terlebih dahulu.</button>
                <?php endif ?>

            </div>
        </div>
    </div>

</div><!--end container-->
</body>
</html>