<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>


<div class="container" id="isi" style="padding-bottom:40px;">
	<div class="page-header" id="top-logo">
		<h3><?= $data->subtitle ?></h3>
	</div>

    <?php foreach ($data->datadaftar as $key): ?>
        <?php $datausaha = json_decode($key['nama_perusahaan']); $datajadwal = json_decode($key['data_jadwal']); ?>
        <div class='row'>
            <div class='col-md-12'>
                <div class='panel panel-primary'>
                    <div class='panel-heading'>
                        <h3 class='panel-title'>
                            <?= @$datausaha->nama ?> -- <?= $key['nama_personal'] ?>   
                            <button class='btn btn-info glyphicon glyphicon-chevron-down' data-toggle='collapse' data-target='#<?= $key['id'] ?>' style='margin-left:5px'></button>
                        </h3>
                    </div>
                    <div class='panel-body collapse' id="<?= $key['id'] ?>" style='overflow-x:auto'>
                        <table class='table table-bordered'>
                            <thead>
                                <tr>
                                    <th>Nama Pendaftar</th>
                                    <th>Alamat</th>
                                    <th>Jenis Usaha</th>
                                    <th>Jabatan Pendaftar</th>
                                    <th>Email</th>
                                    <th>Pengguna</th>
                                    <th>Versi Accurate</th>
                                    <th>Agenda</th>
                                    <th>Tanggal Pelaksanaan</th>
                                    <th>Waktu</th>
                                    <th>Tempat Beli Accurate</th>
                                    <th>Sales Accurate</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?= $key['nama_personal'] ?></td>
                                    <td><?= $datausaha->alamat ?></td>
                                    <td><?= $datausaha->usaha ?></td>
                                    <td><?= $key['jabatan'] ?></td>
                                    <td><?= $key['email_kontak'] ?></td>
                                    <td><?= $key['jenis_pengguna'] ?></td>
                                    <td><?= $key['versi_accurate'] ?></td>
                                    <td><?= $key['agenda_training'] ?></td>
                                    <td><?= $datajadwal->tanggal ?></td>
                                    <td><?= $datajadwal->waktu ?></td>
                                    <td><?= $key['tempat_beli_accurate'] ?></td>
                                    <td><?= $key['salesman_accurate'] ?></td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                        <div class='form-inline'>
                            <a href="<?= $data->base_url ?>administrasi/marketing/pendaftar?mar=<?= $key['id'] ?>" class='btn btn-warning'>Tandai Terbaca</a>
                            <a href="tel://<?= $key['telepon'] ?>" class='btn btn-default' ><?= $key['telepon'] ?></a>
                        </div>  
                        
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach ?>

        
        

</div>

<script type="text/javascript" src=<?php echo "'".$data->base_url."js/jquery-ui/jquery-ui.js'"; ?>></script>
  <script type="text/javascript">
    $(document).ready(function(){
        
        $('#tanggal-awal').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('#tanggal-akhir').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        
    });

    

    function removeElement(id) {
        $('#'+id).remove();
    }

  </script>   
</body>
</html> 