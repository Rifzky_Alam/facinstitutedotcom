<!DOCTYPE html>
<html>


<?php include_once 'view/main-component/header.php'; ?>

<body>



<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
 ?>


<div class="container" id="isi" style="padding-bottom:40px;">
	<div class="page-header" id="top-logo">
		<h3>Tambah Tanggal Transaksi <?php echo $data->no_transaksi ?></h3>
	</div>


        <div class='row'>
            <div class='col-md-8'>
                <table class='table table-bordered'>
                    <thead>
                        <tr>
                            <th class='tengah' colspan="2"><?= $data->perusahaan ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>No Transaksi</td>
                            <td style="text-align: center;"><?php echo $data->no_transaksi; ?></td>
                        </tr>
                        <tr>
                            <td>Agenda</td>
                            <td style="vertical-align:middle;">
                                <?php DeskripsiAgenda($data->agenda) ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Maksimal Total Tanggal</td>
                            <td style="text-align:center;">
                                <?= $data->totalhari ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Tanggal Training</td>
                            <td>
                                <?php DeskripsiTraining($data->tanggal) ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <form action='' method='post'>
            <div class='row' id='jajal'>
                <div class='col-md-8 form-group'>
                    <label for="tglbaru">Tanggal Transaksi</label>
                    <input id="tglbaru" class='form-control tanggalPelaksanaan' name='input[tanggalz]' placeholder="Tambah tanggal.."  />
                </div>
                <input value="<?= $data->no_transaksi ?>" name='input[idTransaksi]' style='display:none' />
            </div>


            <br>
            <button class='btn btn-lg btn-success'>Submit</button>
            <a href="new-perusahaan-info?tr=<?php echo $data->no_transaksi ?>" class="btn btn-lg btn-warning">Back to Transaction</a>
        </form>
</div>

<script type="text/javascript" src=<?php echo "'".$data->base_url."js/jquery-ui/jquery-ui.js'"; ?>></script>
  <script type="text/javascript">
    $(document).ready(function(){
        $('#editTgl').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('#tanggal-awal').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('#tanggal-akhir').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('.tanggalPelaksanaan').datepicker({
            dateFormat: 'yy-mm-dd'
        });
    });

    

    function removeElement(id) {
        $('#'+id).remove();
    }

  </script>   
</body>
</html> 

<?php function DeskripsiAgenda($data){ ?>
    <?php if (count($data)=='0'): ?>
        -
        <?php else: ?>
            <ol>
            <?php foreach ($data as $key) { ?>
                <li><?php echo $key['nama_agenda'] ?></li>
            <?php } ?>
            </ol>
    <?php endif ?>

<?php } //end function ?>

<?php function DeskripsiTraining($data,$base_url='https://fac-institute.com/'){ ?>
    <?php if (count($data)=='0'): ?>
        -
        <?php else: ?>
            <ol>
            <?php foreach ($data as $key) { ?>
                <li>
                    <?php echo $key['tanggal'] ?> 
                    <a href="<?php echo $base_url.'administrasi/new-transaksi-tanggal'.'?del='.$key['id'] ?>">
                        <span class="glyphicon glyphicon-remove" title="Hapus data"></span>
                    </a>
                </li>
            <?php } ?>
            </ol>
    <?php endif ?>

<?php } //end function ?>


