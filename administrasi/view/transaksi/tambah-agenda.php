



<!DOCTYPE html>
<html>
<?php include_once 'view/main-component/header.php'; ?>
<body>

<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
 ?>
    
<div class="container" id="isi" style="padding-bottom:40px;">
	<div class="page-header" id="top-logo">
		<h3>Ubah Jadwal</h3>
	</div>
     <div class="row" id="tes">
        <div class="form-group col-md-10">
            <label><a href="https://fac-institute.com/administrasi/schedules?step=3&kd=hc" target="_blank" >Nama Agenda</a></label>
            <input type="text" name="agenda" class="form-control" id="contoh" />
        </div>
     </div>

     <div class="row">
          </div>


     <form action="" method="post">
     <div class="row" id="siap">
        <input type="text" name="in[idtr]" value="<?php echo $data->id_transaksi ?>" style="display:none;">
        <ul id="ok">  
          <?php DataJadwal($data->agendas) ?>
        </ul>
     </div>
     <hr>
    
     <div class="row">
       <div class="col-md-6">
          <h3>List Agenda:</h3>
         <div class="form-group col-md-12" id='bisalah'></div>
       </div>
       <div class="col-md-6">
         <button class="btn btn-lg btn-primary" style="width:100%;margin-top:25px;">Submit</button>
         <a href="new-perusahaan-info?tr=<?php echo $data->id_transaksi ?>" class="btn btn-lg btn-warning" style="width:100%" >Back to Transaction</a>
       </div>
     </div>
     
     </form>

    
</div>

    <script type="text/javascript" src='https://fac-institute.com/css/multiselect/js/bootstrap-multiselect.js'></script>
    <script type="text/javascript" src='https://fac-institute.com/css/datepicker/js/bootstrap-datepicker.js'></script>
    
            <script type="text/javascript">
        $(document).ready(function(){
            //var availableTags = ['Rifzky Alam','Avril Lavigne'];
            //$('#contoh').autocomplete({
                //source: availableTags
            //});
        });

        $('#contoh').keyup(function(){
            if ($('#contoh').val()==''){
                $('.mydata').remove();
            }else{
            $.ajax({
              type: "GET",
              url: "<?php echo $data->url ?>",
              data: {
                'ajx':$('#contoh').val()
                },
              cache: false,
              success: function(data){
                $('.mydata').remove();
                 $('#bisalah').append(data);
                 //alert(data);
              }
            }); //end ajax  
            }
            
        });

        function coba(haha) {//haha.value
            $('#ok').append("<li class='ilang' id='li" + haha.id + "'>" + $('#y' + haha.id).text() + " <input type='text' name='in[agenda][]' value='" + haha.value + "' style='display:none;'> <a id='i" + haha.id + "' onclick='removeList(this)' href='#'><span class='glyphicon glyphicon-remove' style='padding-left:10px;'></span></a></li>");
            $('#z' + haha.id).remove();
        }

        function removeList(nama) {
            $('#l' + nama.id).remove();
            //alert('oke');
        }

    </script>
            
</body>
</html> 

<?php 
function DataJadwal($data){
  if (count($data)=='0') {
      echo "";
    } else { ?>
      
    <?php foreach ($data as $key) { ?>
      <li><?php echo $key->nama_agenda ?> <a href="<?php echo 'new-transaksi-agenda?del='.$key->agd_id ?>"><span class="glyphicon glyphicon-remove"></span></a></li>
    <?php } ?>




<?php      
    }
}
?>
