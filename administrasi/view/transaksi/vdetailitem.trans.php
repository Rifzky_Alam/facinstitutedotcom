<!DOCTYPE html>
<html>


<?php include_once 'view/main-component/header.php'; ?>

<body>



<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
 ?>


<div class="container" id="isi" style="padding-bottom:40px;">
	<div class="page-header" id="top-logo">
		<h3><?= $data->subtitle ?></h3>
	</div>


        <div class='row'>
            <div class='col-md-10'>
                <table class='table table-bordered'>
                    <thead>
                        <tr>
                            <th class='tengah' colspan="2"><?= $data->namaitem ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Nama Item</td>
                            <td><?php echo $data->namaitem; ?></td>
                        </tr>
                        <tr>
                            <td>Harga</td>
                            <td style="vertical-align:middle;">
                                <?= $data->harga ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Maksimal hari untuk implementasi</td>
                            <td>
                                <?= $data->totalhari ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <br>
                <a class="btn btn-lg btn-warning" href="<?= $data->base_url.'administrasi/new-transaksiz?uitm='.$data->iditem ?>" title="Edit Paket Hari">
                    Edit Paket Hari
                </a>
            </div>
        </div>
</div>

<script type="text/javascript" src=<?php echo "'".$data->base_url."js/jquery-ui/jquery-ui.js'"; ?>></script>
  <script type="text/javascript">
    $(document).ready(function(){
        $('#editTgl').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('#tanggal-awal').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('#tanggal-akhir').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('.tanggalPelaksanaan').datepicker({
            dateFormat: 'yy-mm-dd'
        });
    });

    

    function removeElement(id) {
        $('#'+id).remove();
    }

  </script>   
</body>
</html> 

<?php function DeskripsiAgenda($data){ ?>
    <?php if (count($data)=='0'): ?>
        -
        <?php else: ?>
            <ol>
            <?php foreach ($data as $key) { ?>
                <li><?php echo $key['nama_agenda'] ?></li>
            <?php } ?>
            </ol>
    <?php endif ?>

<?php } //end function ?>

<?php function DeskripsiTraining($data,$base_url='https://fac-institute.com/'){ ?>
    <?php if (count($data)=='0'): ?>
        -
        <?php else: ?>
            <ol>
            <?php foreach ($data as $key) { ?>
                <li>
                    <?php echo $key['tanggal'] ?> 
                    <a href="<?php echo $base_url.'administrasi/new-transaksi-tanggal'.'?del='.$key['id'] ?>">
                        <span class="glyphicon glyphicon-remove" title="Hapus data"></span>
                    </a>
                </li>
            <?php } ?>
            </ol>
    <?php endif ?>

<?php } //end function ?>


