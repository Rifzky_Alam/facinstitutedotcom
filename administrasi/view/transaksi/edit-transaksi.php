<!DOCTYPE html>
<html>


<?php include_once 'view/main-component/header.php'; ?>

<body>



<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3>Edit Transaksi</h3>
	</div>
	<div class="row">
     <form action=""  method="post">

        <div class="row">
            <div class="col-md-10">
                <h4>Perusahaan: </h4>
                <?php if ($data->nama_usaha==''): ?>
                    <script type="text/javascript">alert('Nama perusahaan tidak ada, silahkan memilih nama perusahaan kembali!');</script>
                    <script type="text/javascript">location.replace('perusahaan');</script>
                    <?php else: ?>
                        <h3>
                            <a href="<?= $data->base_url.'administrasi/new-usaha?id='.$data->idusaha ?>" title="">
                                <?php echo $data->nama_usaha ?>        
                            </a>
                        </h3>
                <?php endif ?>
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-md-10">
                <h4>Customer: </h4>
                <?php if ($data->nama_cust==''): ?>
                    <script type="text/javascript">alert('Nama customer tidak ada, silahkan memilih customer terlebih dahulu!');</script>
                    <script type="text/javascript">location.replace("new-perusahaan-info?c=<?php echo $data->nama_cust ?>");</script>
                    <?php else: ?>
                        <h3><?php echo $data->nama_cust  ?></h3>
                <?php endif ?>
            </div>
        </div>
        <br>
        <div class="row">
             <div class="col-md-10">
                 <div class="form-group">
                     <label>Jenis transaksi</label>
                     <select name="ubh[jtr]" class="form-control" required>
                        <?php foreach ($data->dtjt as $key): ?>
                            <?php if ($key->jt_id==$data->jtr): ?>
                                <option value="<?= $key->jt_id ?>" selected="selected"><?= $key->jt_ket ?></option>
                            <?php else: ?>
                                <option value="<?= $key->jt_id ?>"><?= $key->jt_ket ?></option>
                            <?php endif ?>
                        <?php endforeach ?>                         
                     </select>
                 </div>
             </div>
         </div>

        <div class="row">
             <div class="col-md-10">
                 <div class="form-group">
                     <label>Jenis Accurate</label>
                     <select name="ubh[jac]" class="form-control">
                        <option value="0">-- Pilih Jenis Accurate --</option>
                        <?php if (count($data->listaccurate)=='0'): ?>
                            
                        <?php else: ?>
                            <?php foreach ($data->listaccurate as $key): ?>
                                <?php if ($data->versi_acc==$key['va_id']): ?>
                                    <option value="<?= $key['va_id'] ?>" selected="selected"><?= $key['va_nama'] ?></option>
                                <?php else: ?>
                                    <option value="<?= $key['va_id'] ?>"><?= $key['va_nama'] ?></option>
                                <?php endif ?>
                             <?php endforeach ?>
                        <?php endif ?>
                     </select>
                 </div>
             </div>
        </div>

         <div class="row">
             <div class="col-md-10">
                 <div class="form-group">
                     <label>Tempat</label>
                     <input type="text" value="<?php echo $data->tempat ?>" class="form-control" name="ubh[tempat]" placeholder="Tempat dimana jasa/layanan di implementasikan"/>
                 </div>
             </div>
         </div>

         <div class="row">
             <div class="col-md-10">
                 <div class="form-group">
                     <label>Waktu</label>
                     <input type="text" class="form-control" name="ubh[waktu]" placeholder="Format contoh: 08:00-16:00" value="<?php echo $data->waktu ?>"/>
                 </div>
             </div>
         </div>

         <div class="row">
             <div class="col-md-10">
                 <div class="form-group">
                     <label>PPN (%)</label>
                     <input type="text" class="form-control" name="ubh[ppn]" placeholder="range dari 1-100" value="<?php echo $data->ppn ?>"/>
                 </div>
             </div>
         </div>

         <div class="row">
             <div class="col-md-10">
                <div class="form-group">
                    <label>Discount (%)</label>
                     <input type="text" class="form-control" name="ubh[discount]" placeholder="range dari 1-100" value="<?php echo $data->discount ?>" />
                </div>  
             </div>
         </div>

         <div class="row">
             <div class="col-md-10">
                <div class="form-group">
                    <label>DP</label>
                     <input value="<?php echo $data->dp ?>" type="text" class="form-control" name="ubh[depe]" placeholder="angka tanpa titik/koma" />
                </div>  
             </div>
         </div>

         <div class="row">
             <div class="col-md-10">
                <div class="form-group">
                    <label>Notes</label>
                     <input value="<?php echo $data->notes ?>" type="text" class="form-control" name="ubh[notes]" placeholder="pisahkan dengan ##, contoh: notes1##notes2" />
                </div>  
             </div>
         </div>

         
         <div class="row">
             <div class="col-md-10">
                <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>        
             </div>
         </div>
         
     </form>   
    </div>

</div><!--end container-->
</body>
</html> 