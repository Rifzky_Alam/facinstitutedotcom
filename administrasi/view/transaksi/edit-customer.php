<!DOCTYPE html>
<html>


<?php include_once 'view/main-component/header.php'; ?>

<body>
<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
?>
<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3>Edit Customer</h3>
	</div>
	<div class="row">
     <form action=""  method="post">

        <div class="row">
            <div class="col-md-10">
                <h4>Perusahaan: </h4>
                <h3><?php echo $data->nama_usaha ?></h3>
                <?php if ($data->nama_usaha==''): ?>
                    <script type="text/javascript">alert('Nama perusahaan tidak ada, silahkan memilih nama perusahaan kembali!');</script>
                    <script type="text/javascript">location.replace('perusahaan');</script>
                    <?php else: ?>

                <?php endif ?>
            </div>
        </div>
        <br><br>
         <div class="row">
             <div class="col-md-10">
                 <div class="form-group">
                     <label>Nama Customer</label>
                     <input type="text" class="form-control" name="ubh[nama]" value="<?php echo $data->nama_cust ?>" required />
                 </div>
             </div>
         </div>
         
         <div class="row">
             <div class="col-md-10">
                 <div class="form-group">
                     <label>Jenis Kelamin</label>
                     <select name=ubh[gender] class="form-control" required>
                         
                         <?php if ($data->gender_cust=='1'): ?>
                         <option value="">--Pilih Jenis Kelamin--</option>
                         <option value="1" selected="selected">Pria</option>
                         <option value="2">Wanita</option>
                         <?php elseif($data->gender_cust=='2'): ?>
                         <option value="">--Pilih Jenis Kelamin--</option>
                         <option value="1">Pria</option>
                         <option value="2" selected="selected">Wanita</option>
                         <?php else: ?>
                         <option value="" selected="selected">--Pilih Jenis Kelamin--</option>
                         <option value="1">Pria</option>
                         <option value="2">Wanita</option>
                         <?php endif ?>
                     </select>
                 </div>
             </div>
         </div>

         <div class="row">
             <div class="col-md-10">
                <div class="form-group">
                    <label>Email Customer</label>
                     <input type="email" class="form-control" name="ubh[email]" value="<?php echo $data->email_cust ?>" />
                </div>  
             </div>
         </div>

         <div class="row">
             <div class="col-md-10">
                <div class="form-group">
                    <label>Telepon</label>
                     <input type="text" class="form-control" name="ubh[telepon]" value="<?php echo $data->telepon ?>" />
                </div>  
             </div>
         </div>

         <div class="row">
             <div class="col-md-10">
                <div class="form-group">
                    <label>Jabatan</label>
                     <input type="text" class="form-control" name="ubh[jabatan]" value="<?php echo $data->jabatan ?>" />
                </div>  
             </div>
         </div>

         <div class="row">
             <div class="col-md-10">
                <label>Jenis Pengguna</label>
                 <select name="ubh[jp]" class="form-control">
                    <?php if ($data->jenispengguna=='1'): ?>
                        <option value="1" selected>Lama</option>
                        <option value="2">Baru</option>
                        <?php else: ?>
                        <option value="1">Lama</option>
                        <option value="2" selected>Baru</option>
                    <?php endif ?>
                     
                 </select>
             </div>
         </div>
         <br>
         <div class="row">
             <div class="col-md-10">
                <div class="form-group">
                    <label>Marketing</label>
                    <select name="ubh[mi]" class="form-control">
                        <?php foreach ($data->petugas as $key ) { ?>
                            <?php if ($data->marketing==$key->marketing_id): ?>
                            <option value="<?php echo $key->marketing_id ?>" selected><?php echo $key->marketing_nama ?></option>    
                                <?php else: ?>
                            <option value="<?php echo $key->marketing_id ?>"><?php echo $key->marketing_nama ?></option>
                            <?php endif ?>
                            
                        <?php } ?>
                    </select>
                </div>  
             </div>
         </div>
         <div class="row">
             <div class="col-md-10">
                <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>        
             </div>
         </div>
         
     </form>   
    </div>

</div><!--end container-->
</body>
</html> 