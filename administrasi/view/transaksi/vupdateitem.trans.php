<!DOCTYPE html>
<html>


<?php include_once 'view/main-component/header.php'; ?>

<body>



<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
 ?>


<div class="container" id="isi" style="padding-bottom:40px;">
	<div class="page-header" id="top-logo">
		<h3><?= $data->subtitle ?></h3>
	</div>


        <div class='row'>
            <div class='col-md-10'>
                <table class='table table-bordered'>
                    <thead>
                        <tr>
                            <th class='tengah' colspan="2"><?= $data->namaitem ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Nama Item</td>
                            <td><?php echo $data->namaitem; ?></td>
                        </tr>
                        <tr>
                            <td>Harga</td>
                            <td style="vertical-align:middle;">
                                <?= $data->harga ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Maksimal hari untuk implementasi</td>
                            <td>
                                <?= $data->totalhari ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
            </div>
        </div>
        <form action='' method='post'>
            <div class='row' id='jajal'>
                <div class='col-md-8 form-group'>
                    <label for="jmlbaru">Jumlah Hari Maksimal</label>
                    <input id="jmlbaru" class='form-control tanggalPelaksanaan' name='in[hari]' placeholder="jumlah hari pelaksanaan untuk item ini"  />
                    <input type="hidden" name="in[id]" value="<?= $data->iditem ?>">
                </div>
                <div class='col-md-8 form-group'>
                    <label for="kategori">Kategori</label>
                    <select name="in[kategori]" class="form-control">
                        <option required="">--Pilih Kategori--</option>
                        <?php foreach ($data->listkategori as $key): ?>
                            <?php if ($key['dt_flag']==$data->katitem): ?>
                                <option value="<?= $key['dt_flag'] ?>" selected="selected"><?= $key['dt_desc'] ?></option>
                            <?php else: ?>
                                <option value="<?= $key['dt_flag'] ?>"><?= $key['dt_desc'] ?></option>
                            <?php endif ?>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>
            <br>
            <button class='btn btn-lg btn-success'>Submit</button>
        </form>
</div>

<script type="text/javascript" src=<?php echo "'".$data->base_url."js/jquery-ui/jquery-ui.js'"; ?>></script>   
</body>
</html> 



