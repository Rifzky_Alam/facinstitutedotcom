<!DOCTYPE html>
<html>


<?php include_once 'view/main-component/header.php'; ?>

<body>



<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
 ?>


<div class="container" id="isi" style="padding-bottom:40px;">
	<div class="page-header" id="top-logo">
		<h3>Kalender Transaksi <?php echo $data->no_transaksi ?></h3>
	</div>


        <div class='row'>
            <div class='col-md-8'>
                <table class='table table-bordered'>
                    <thead>
                        <tr>
                            <th class='tengah' colspan="2">PT Accurate Business Center</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>No Transaksi</td>
                            <td><?php echo $data->no_transaksi; ?></td>
                        </tr>
                        <tr>
                            <td>Agenda</td>
                            <td>
                                <?php DeskripsiAgenda($data->agenda) ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Tanggal Training</td>
                            <td>
                                <?php DeskripsiTraining($data->tanggal) ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <form action='' method='post'>
            <div class='row' id='jajal'>
                <div class='col-md-8 form-inline'>
                    <input class='form-control tanggalPelaksanaan' name='input[tanggalz][]' placeholder="Tambah tanggal.."  />
                    <span id='tambahTombol' class='btn btn-primary glyphicon-plus'></span>
                </div>
                <input value=<?php echo "'".$data->no_transaksi."'"; ?> name='input[idTransaksi]' style='display:none' />
            </div>


            <br>
            <button class='btn btn-lg btn-success'>Submit</button>
            <a href="new-perusahaan-info?tr=<?php echo $data->no_transaksi ?>" class="btn btn-lg btn-warning">Back to Transaction</a>
        </form>
</div>

<script type="text/javascript" src=<?php echo "'".$data->base_url."js/jquery-ui/jquery-ui.js'"; ?>></script>
  <script type="text/javascript">
    $(document).ready(function(){
        $('#editTgl').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('#tanggal-awal').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('#tanggal-akhir').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('.tanggalPelaksanaan').datepicker({
            dateFormat: 'yy-mm-dd'
        });
    });

    $('#tambahTombol').click(function(){
        var cobayah = $('.tanggalPelaksanaan').length;
        var iseng = "tambahan" + cobayah;
        if ($('#tambahan').length){
            if (cobayah==2) {
                $('#tambahan').after("<div class='row' id='"+iseng+"'><div class='col-md-10'><div class='form-inline'><input name='input[tanggalz][]' class='form-control tanggalPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"'+iseng+'"'+")'></span></div></div></div>");
            }else{
                var okeh = cobayah-1;
                var iseng = "tambahan" + okeh;
                $("#"+iseng).after("<div class='row' id='tambahan"+cobayah+"'><div class='col-md-10'><div class='form-inline'><input name='input[tanggalz][]' class='form-control tanggalPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"tambahan'+cobayah+'"'+")'></span></div></div></div>");
            };
                
             } else{
            $('#jajal').after("<div class='row' id='tambahan'><div class='col-md-10'><div class='form-inline'><input name='input[tanggalz][]' class='form-control tanggalPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"'+"tambahan"+'"'+")'></span></div></div></div>");
        };

        $('.tanggalPelaksanaan').datepicker({
            dateFormat: 'yy-mm-dd'
        });//alert('coba yah');
    });

    function removeElement(id) {
        $('#'+id).remove();
    }

  </script>   
</body>
</html> 

<?php function DeskripsiAgenda($data){ ?>
    <?php if (count($data)=='0'): ?>
        -
        <?php else: ?>
            <ol>
            <?php foreach ($data as $key) { ?>
                <li><?php echo $key->nama_agenda ?></li>
            <?php } ?>
            </ol>
    <?php endif ?>

<?php } //end function ?>

<?php function DeskripsiTraining($data){ ?>
    <?php if (count($data)=='0'): ?>
        -
        <?php else: ?>
            <ol>
            <?php foreach ($data as $key) { ?>
                <li>
                    <?php echo $key->tanggal ?> 
                    <a href="<?php echo 'new-transaksi-tanggal'.'?del='.$key->id ?>">
                        <span class="glyphicon glyphicon-remove" title="Hapus data"></span>
                    </a>
                </li>
            <?php } ?>
            </ol>
    <?php endif ?>

<?php } //end function ?>


