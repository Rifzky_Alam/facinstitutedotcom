<!DOCTYPE html>
<html>
<?php include_once 'view/main-component/header.php'; ?>

<body>

<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->subtitle ?></h3>
	</div>
	
     

	
	<div class="row">
		<div class="col-md-6">
			<table class="table table-bordered">
				<thead>
					<tr><th>Atribut</th><th>Keterangan</th></tr>
				</thead>

				<tbody>
                    <tr>
                      <td>Nama perusahaan</td> 
                      <?php NamaUsaha($data->isadmin,$data->id_usaha,$data->nama_usaha) ?>
                    </tr>
                    <tr>
                      <td>Nama Personal Kontak</td>
                      <td><?php echo $data->nama_cust ?></td>
                    </tr>
                    <tr>
                      <td>Email Perusahaan</td>
                      <td><?php echo $data->email_usaha ?></td>
                    </tr>
                    <tr>
                      <td>Email Kontak</td>
                      <td><?php echo $data->email_cust ?></td>
                    </tr>
                    <tr>
                      <td>Telepon Perusahaan</td> 
                      <td><?php echo $data->telepon_usaha ?></td>
                    </tr>
                    <tr>
                      <td>Telepon Kontak</td> 
                      <td title="Klik untuk menelpon..."><a href="tel:\\<?php echo str_replace('-','',$data->telp_cust) ?>"><?php echo $data->telp_cust ?></a></td>
                    </tr>
                    <tr>
                      <td>Alamat perusahaan</td>
                      <td><?php echo $data->alamat ?></td>
                    </tr>
                    <tr>
                      <td>Alamat Training</td> 
                      <td><?php echo $data->alamat_training ?></td>
                    </tr>
                    <tr>
                      <td>Alamat Map</td> 
                      <td><a target='_blank' href='https://www.google.co.id/maps/place/<?php echo $data->map ?>'>Lokasi</a></td></tr>
                    <tr>
                      <td>Jabatan Pemesan</td> 
                      <td><?php echo $data->jabatan ?></td>
                    </tr>
                    <tr>
                      <td>Jenis Usaha</td>
                      <td><?php echo $data->jenis_usaha ?></td>
                    </tr>
                    <tr>
                      <td>Ket Jenis Usaha</td> 
                      <td><?php echo $data->ket_jenis_usaha ?></td>
                    </tr>

                    <tr>
                      <td>tanggal Pesan</td> 
                      <td><?php echo $data->tanggal_pesan ?></td>
                    </tr>
                    <tr>
                      <td>Lama Training</td> 
                      <td><?php echo $data->lama_training ?></td>
                    </tr>
                    <tr>
                      <td>Jenis Pengguna</td> 
                      <td><?php echo $data->jenis_pengguna ?></td>
                    </tr>
                    <tr>
                      <td>Versi Accurate</td>
                      <td><?php echo $data->versi_accurate ?></td>
                    </tr>
                    <tr>
                      <td>Agenda Training</td> 
                      <td><?php echo $data->agenda_training ?></td>
                    </tr>
                    <tr>
                      <td>Salesman</td> 
                      <td><?php echo $data->nama_marketing ?></td>
                    </tr>
        </tbody>
			</table>
		</div>
        <div class="col-md-6">
            <iframe width="100%" height="450" frameborder="1" style="border:1" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyB9cGqJ4nUrq-Lje2Q1B1Hx1_X3-RofwsY&q=<?php echo ChangeChar($data->map) ?>" allowfullscreen>
            </iframe>
            <?php LinkAssistLocation($data->isadmin,$data->base_url,securitycode($data->idtrans,'e')) ?>
        </div>
	</div>

</div>

     
</body>
</html> 

<?php function LinkAssistLocation($isadmin,$baseurl,$idtrans){
  if ($isadmin) {
    echo "<td title='Klik untuk membagikan lokasi training ke customer'><a target='_blank' href='".$baseurl."member/lokasi-training?tr=".$idtrans."'>Share map to customer</a></td>";
  } else {

  }
  
} ?>

<?php function NamaUsaha($isadmin,$idusaha,$data){
  if ($isadmin) {
    echo "<td title='Klik untuk detail perusahaan'><a target='_blank' href='new-usaha?id=".$idusaha."'>".$data."</a></td>";
  } else {
    echo "<td>".$data."</td>";
  }
  
} ?>

<?php 
function ChangeChar($string){
$string=str_replace('<','&lt;',$string);
$string=str_replace('>','&gt;',$string);
$string=str_replace('"','&quot;',$string);
$string=str_replace("'",'&#39;',$string);
$string=str_replace("&",'%26',$string);
return $string;


}
 ?>
