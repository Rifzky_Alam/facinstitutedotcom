<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>


<div class="container" id="isi" style="padding-bottom:40px;">
	<div class="page-header" id="top-logo">
		<h3><?= $data->subtitle ?></h3>
	</div>


        <div class='row'>
            <div class='col-md-12'>
                <table class='table table-bordered'>
                    <thead>
                        <tr>
                            <th class='tengah'>Nama Item</th>
                            <th class='tengah'>Kategori</th>
                            <th class='tengah'>Harga</th>
                            <th class='tengah'>Paket Hari</th>
                            <th class='tengah'>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php if (count($data->listdata)=='0'): ?>
                        <tr>
                            <td colspan="8" class="tengah">
                                Tidak ada data dalam database kami.
                            </td>
                        </tr>    
                    <?php else: ?>
                        <?php foreach ($data->listdata as $key): ?>
                            <tr>
                                <td><?= $key['nama_item'] ?></td>
                                <td><?= $key['kategori'] ?></td>
                                <td><?= $key['harga'] ?></td>
                                <td><?= $key['paket_hari'] ?></td>
                                <td>
                                    <a href="<?= $data->base_url.'administrasi/new-transaksiz?uitm='.$key['id'] ?>" title="Edit" class="btn btn-danger">Edit</a>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                    </tbody>
                </table>
                
            </div>
        </div>
</div>

<script type="text/javascript" src=<?php echo "'".$data->base_url."js/jquery-ui/jquery-ui.js'"; ?>></script>
  <script type="text/javascript">
    $(document).ready(function(){
        
        $('#tanggal-awal').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('#tanggal-akhir').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        
    });

    

    function removeElement(id) {
        $('#'+id).remove();
    }

  </script>   
</body>
</html> 



