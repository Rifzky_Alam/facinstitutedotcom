<?php

function BodySample($data){
    return EmailHeader($data).'
                                        <tr>
                                            <td class="m_-5588023840110578919m_3185550004040353724featured-story__copy" style="background:#fff" width="640" align="left">
                                                <table cellspacing="0" cellpadding="0">
                                                    <tbody><tr>
                                                        <td style="font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;font-size:16px;line-height:22px;color:#555555;padding-top:16px" align="left">
                                                        Dear '.$data->nama_cust.', <br><br>
                                                        Perkenalkan kami dari FAC Institute.
                                                        Terimakasih telah menggunakan Jasa Training Accurate di FAC Institute.
                                                        Demi peningkatan kualitas pelayanan, mohon luangkan waktu untuk mengisi survey tentang trainer accurate. Hanya 1 menit untuk menyelesaikan survey ini.  
                                                        Terima kasih atas partisipasi Anda.
                                                        Silahkan isi survei tersebut pada link di bawah ini :
                                                        <br><br>
                                                        <a href="'.$data->base_url.'training/feedback/WGJ5c1BDcVZDM2NqVGRlRXEzS0trZz09" style="font-size:20px;font-weight:bold;color:#ffffff;text-decoration:none;border-radius:3px;background-color:#ec7979;border-top:12px solid #ec7979;border-bottom:12px solid #ec7979;border-right:20px solid #ec7979;border-left:20px solid #ec7979;display:inline-block" target="_blank" data-saferedirecturl="https://www.google.com/url?q='.$data->base_url.'training/feedback/?tr%3DWGJ5c1BDcVZDM2NqVGRlRXEzS0trZz09&amp;source=gmail&amp;ust=1570334693544000&amp;usg=AFQjCNHCkOAoWDCVA2QTKw6cCepnYN_Mug">
                                                            ISI SURVEI SEKARANG
                                                        </a>
                                                         <br><br><br>

                                                        Terimakasih, <br><br>
                                                        <b>FAC System.</b>
                                                        </td>
                                                    </tr>
                                                </tbody></table>
                                            </td>
                                        </tr>
                                    </tbody></table>
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>'.EmailFooter($data);
}