<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?= $data->subtitle ?></h3>
	</div>
	<div class="row">
     <form id="my-form" enctype="multipart/form-data" method="post">
         <div class="row">
             <div class="col-md-10">
                <div class="form-group">
                    <label>Jenis Reimburse</label>
                    <select class="form-control" name="tipe">
                        <?php foreach($data->listket as $key): ?>
                        <option value="<?= $key['dt_flag'] ?>"><?= $key['dt_desc'] ?></option>
                        <?php endforeach ?>
                    </select>
                </div>  
             </div>
         </div>
         
         <div class="row">
             <div class="col-md-10">
                <div class="form-group">
                    <label>File yang akan di upload</label>
                    <input id="file-3" type="file" class="file" name="ifile">
                </div>  
             </div>
         </div>         
         <hr>

     </form>   
    </div>
    
    <div class="row">
        <?php if(count($data->listdata)!='0'): ?>
            <?php foreach($data->listdata as $key): ?>
                <div class="col-md-4">
                    <div class="thumbnail">
                        <a href="#">
                            <img src="<?= $data->base_url.'administrasi/reimproof/'.$key['sfilename'] ?>" class="img-responsive" alt="<?= 'Reimburesement '. $key['sfklaphar'] ?>" width="304" height="236"> 
                        </a>
                        <div class="caption" style="text-align:center;">
                            <a href="<?= $data->base_url. 'administrasi/user/reimupld?rmv='.$key['iseqid'] ?>" class="btn btn-danger">Delete</a>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        <?php endif ?>
    </div>

</div><!--end container-->
</body>
</html> 