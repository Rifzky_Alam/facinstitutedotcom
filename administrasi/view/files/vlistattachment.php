<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?= $data->headertext ?></h3>
	</div>
	
    
	<div class="row">
		<div class="col-sm-12 col-md-12">
            <a id="cari-data" href="<?= $data->base_url.'administrasi/upload-files/uploadnew' ?>">
                Input File Baru
            </a>
        </div>
	</div>
    

    <div class='row'>
        <div class='col-md-12'>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Nama File</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($data->files)=='0'): ?>
                        <tr>
                            <td colspan="5" class="tengah">Tidak ada file dalam folder attachments</td>
                        </tr>    
                        <?php else: ?>
                        <?php foreach ($data->files as $key): ?>
                            <tr>
                                <td><?= $key ?></td>
                                <td>
                                    <a href="<?= $data->base_url.'administrasi/upload-files/delete/'.$key ?>">Hapus</a>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                </tbody>
            </table>
        </div>

    </div>
	    
</div>
</body>
</html> 



