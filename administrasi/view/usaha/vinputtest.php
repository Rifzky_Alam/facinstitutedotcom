<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
  <div class="page-header" id="top-logo">
    <h3><?= $data->headertext ?></h3>
  </div>

    

  <div class="row">
        <div class="col-md-12">
            <form action="" method="post" accept-charset="utf-8">
                <div class="form-group">
                    <label>Provinsi</label>
                    <input id="namaprov" type="hidden" name="in[namaprov]" value="">
                    <select id="prv" name="in[prov]" class="form-control"></select>
                </div>
                <div class="form-group">
                    <label>Kota</label>
                    <input id="namakota" type="hidden" name="in[namakota]" value="">
                    <select id="cty" name="in[kota]" class="form-control"></select>
                </div>
                <div class="form-group">
                  <label>Kecamatan</label>
                  <input id="namakec" type="hidden" name="in[namakec]" value="">
                  <select id="sbd" name="in[sbd]" class="form-control"></select>
                </div>
                <div class="form-group">
                  <label>Daerah Wilayah</label>
                  <select class="form-control" name="in[dawil]">
                    <?php foreach ($data->listdata as $key): ?>
                      <option value="<?= $key['ind_id'] ?>"><?= $key['ind_wilayah'] ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
                

                <button class="btn btn-lg btn-primary">Submit</button>
            </form>
        </div> 
    </div>

</div><!--end container-->
<script type="text/javascript" src='https://fac-institute.com/js/jquery-ui/jquery-ui.js'></script>
<script>
    $(document).ready(function(){
        LoadProv('9');
    });

    $('#prv').change(function() {
      document.getElementById("namaprov").value = $("#prv option:selected").text();
      LoadCity();
    });

    $('#cty').change(function() {
      document.getElementById("namakota").value = $("#cty option:selected").text();
      LoadSubdistrict();
    });

    $('#sbd').change(function() {
      document.getElementById("namakec").value = $("#sbd option:selected").text();
      LoadService($('#dv').val());
    });

    function LoadProv(id){
      $('.optprv').remove();
      $.ajax({
            type: "GET",
            url: "https://fac-institute.com/administrasi/wilayah/provinsi/",
            data: {
              'q':'province'
            },
            cache: false,
            success: function(data){
              // console.log(data);
              
              var myjson = data;
              var result = myjson.rajaongkir.results
              for (x in result) {
                if (result[x].province_id==id) {
                  document.getElementById("prv").innerHTML += "<option class='optprv' selected value='" + result[x].province_id + "'>" + result[x].province + '</option>';
                } else {
                  document.getElementById("prv").innerHTML += "<option class='optprv' value='" + result[x].province_id + "'>" + result[x].province + '</option>';
                }
              }
              document.getElementById("namaprov").value = $("#prv option:selected").text();
              LoadCity();
            }
          }); //end ajax
    }

    function LoadCity() {
      $('.optcty').remove();
      $.ajax({
            type: "GET",
            url: "https://fac-institute.com/administrasi/wilayah/kota/" + $('#prv').val(),
            data: {
              '':''
            },
            cache: false,
            success: function(data){
              var myjson = data;
              var result = myjson.rajaongkir.results
              for (x in result) {
                if (result[x].city_id=='79') {
                  document.getElementById("cty").innerHTML += "<option class='optcty' selected value='" + result[x].city_id + "'>" + result[x].type + " "+ result[x].city_name + '</option>';
                } else {
                  document.getElementById("cty").innerHTML += "<option class='optcty' value='" + result[x].city_id + "'>" + result[x].type + " "+ result[x].city_name + '</option>';
                }
              }
              document.getElementById("namakota").value = $("#cty option:selected").text();
              LoadSubdistrict();
            }
          }); //end ajax
    }

    function LoadSubdistrict(){
      $('.optsbd').remove();
      $.ajax({
            type: "GET",
            url: "https://fac-institute.com/administrasi/wilayah/subdist/" + $('#cty').val(),
            data: {
              '': ''
            },
            cache: false,
            success: function(data){
              var myjson = data;
              var result = myjson.rajaongkir.results
              for (x in result) {
                if (result[x].subdistrict_id=='1066') {
                  document.getElementById("sbd").innerHTML += "<option class='optsbd' selected value='" + result[x].subdistrict_id + "'>" + result[x].subdistrict_name + '</option>';
                } else {
                  document.getElementById("sbd").innerHTML += "<option class='optsbd' value='" + result[x].subdistrict_id + "'>" + result[x].subdistrict_name + '</option>';
                }
              }
              document.getElementById("namakec").value = $("#sbd option:selected").text();
            }
          }); //end ajax
    }
</script>
</body>
</html>