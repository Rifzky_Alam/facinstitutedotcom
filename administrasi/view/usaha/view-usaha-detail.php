<!DOCTYPE html>
<html>
<?php include_once 'view/main-component/header.php'; ?>

<body>

<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?= $data->subtitle ?> <a href="<?= $data->url.'?edt='.$data->id_perusahaan ?>" title=""><i class="glyphicon glyphicon-edit"></i></a></h3>
	</div>
	
     

	
	<div class="row" style="overflow:auto;">
		<div class="col-md-6">
			<table class="table table-bordered">
				<thead>
					<tr><th>Atribut</th><th>Keterangan</th></tr>
				</thead>

				<tbody>
                    <tr>
                      <td>Nama perusahaan</td> 
                      <td><?php echo $data->nama_usaha ?></td>
                    </tr>
                    
                    <tr>
                      <td>Email Perusahaan</td>
                      <td><?php echo $data->email_usaha ?></td>
                    </tr>
                    <tr>
                      <td>Telepon Perusahaan</td> 
                      <td><?php echo $data->telepon_usaha ?></td>
                    </tr>
                    <tr>
                      <td>Alamat Lengkap</td>
                      <?php if ($data->alamat==''): ?>
                        <td style="color:red;">Harap lengkapi data</td>
                      <?php else: ?>
                        <td><?php echo $data->alamat ?></td>  
                      <?php endif ?>
                    </tr>
                    <tr>
                      <td>Provinsi</td>
                      <?php if ($data->provinsi==''): ?>
                        <td style="color:red;">Harap lengkapi data</td>
                      <?php else: ?>
                        <td><?php echo $data->provinsi ?></td>
                      <?php endif ?>
                    </tr>
                    <tr>
                      <td>Kota</td>
                      <?php if ($data->kota==''): ?>
                        <td style="color:red;">Harap lengkapi data</td>
                      <?php else: ?>
                        <td><?php echo $data->kota ?></td>
                      <?php endif ?>
                    </tr>
                    <tr>
                      <td>Kecamatan</td>
                      <?php if ($data->kec==''): ?>
                        <td style="color:red;">Harap lengkapi data</td>
                      <?php else: ?>
                        <td><?php echo $data->kec ?></td>  
                      <?php endif ?>
                      
                    </tr>
                    <tr>
                      <td>Alamat Map</td> 
                      <td><a target='_blank' href='https://www.google.co.id/maps/place/<?php echo $data->map ?>'>Lokasi</a></td></tr>
                    <tr>
                      <td>Ket Jenis Usaha</td> 
                      <td><?php echo $data->ket_jenis_usaha ?></td>
                    </tr>
        </tbody>
			</table>
      <br><br>
      <?php if($data->tipeusaha=='1'): ?>
      <a href="new-customer?p=<?php echo $data->id_perusahaan ?>" class="btn btn-lg btn-info">Input Customer >></a>
      <br><br>
      <a href="new-perusahaan-info?p=<?php echo $data->id_perusahaan ?>" class="btn btn-lg btn-success">Detail Transaksi >></a>
      <?php elseif($data->tipeusaha=='51'): ?>
        <?php if($data->alamat==''||$data->provinsi==''): ?>
            <button class="btn btn-lg btn-danger" disabled>Harap Lengkapi Data Terlebih Dahulu</button>
        <?php else: ?>
            <a href="<?= $data->base_url.'administrasi/customerservice/exportusaha?id='.$data->id_perusahaan ?>" class="btn btn-lg btn-primary">Export As Transaction Data</a>
        <?php endif ?>
      <?php endif ?>
		</div>
        <div class="col-md-6">
            
            <div class="row">  
              <div class="col-md-12">
                <h3>Jenis Usaha</h3>
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th class="tengah">Jenis Usaha</th>
                      <th class="tengah">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php DataJenisUsaha($data->jenis_usaha) ?>
                  </tbody>
                </table>
                <a href="new-usaha?iju=<?php echo $data->id_perusahaan ?>" class="btn btn-lg btn-danger" style="width:100%">Add More..</a>
              </div>
            </div>

            <br><br>

            <div class="row">  
              <div class="col-md-12">
                <h3>Versi Accurate</h3>
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th class="tengah">Versi Accurate</th>
                      <th class="tengah">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php DataVersiAccurate($data->versi_accurate) ?>
                  </tbody>
                </table>
                <a href="new-usaha?iva=<?php echo $data->id_perusahaan ?>" class="btn btn-lg btn-warning" style="width:100%">Add More..</a>
              </div>
            </div>  

        </div>
	</div>

</div>

     
</body>
</html> 
<?php function DataVersiAccurate($data){ ?>
    <?php if (count($data)=='0'): ?>
        <tr>
          <td class="tengah">Tidak ada data..</td>
        </tr>
      <?php else: ?>
    <?php foreach ($data as $key ) { ?>  
    <tr>
      <td class="tengah"><?php echo $key->va_nama ?></td>
      <td class="tengah"><a href="new-usaha?dva=<?php echo $key->uva_id ?>" class="btn btn-danger">Delete</a></td>
   </tr> 
   <? } ?>
    <?php endif ?>
<?php } ?>  


<?php function DataJenisUsaha($data){ ?>
    <?php if (count($data)=='0'): ?>
        <tr>
          <td class="tengah">Tidak ada data..</td>
        </tr>
      <?php else: ?>
    <?php foreach ($data as $key ) { ?>  
      <tr>
      <td class="tengah"><?php echo $key->jenis_usaha ?></td>
      <td class="tengah"><a href="new-usaha?dju=<?php echo $key->uju_num ?>" class="btn btn-danger">Delete</a></td>
    </tr>
    <? } ?>
    <?php endif ?>
    
<?php } ?>