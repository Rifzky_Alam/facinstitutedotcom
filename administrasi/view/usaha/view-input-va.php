<!DOCTYPE html>
<html>
<?php include_once 'view/main-component/header.php'; ?>

<body>

<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
  <div class="page-header" id="top-logo">
    <h3><?php echo $data->subtitle ?></h3>
  </div>
  
  <div class="row">
    <div class="col-md-12">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th class="tengah">Jenis Usaha</th>
            <th class="tengah">Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php DataVersiAccurate($data->listaccurate) ?>
        </tbody>
      </table>
    </div>
  </div>

  <form action="" method="post">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group">
        <label>Versi Accurate</label>
        <select name="in[va]" class="form-control">
            <?php VersiAccurate($data->accurates) ?>
        </select>
      </div> 
      <button class="btn btn-lg btn-primary" style="width:50%;">Submit</button>
      <a href="new-usaha?id=<?php echo $data->id_perusahaan ?>" class="btn btn-lg btn-warning" style="width:40%">Back</a>    
    </div>
  </div>
  </form>

</div>
     
</body>
</html>

<?php function VersiAccurate($data){ ?>
    <?php foreach ($data as $key ) { ?>  
      <label><option value="<?php echo $key->va_id ?>"><?php echo $key->va_nama ?></option>
    <? } ?>
<?php } ?> 


<?php function DataVersiAccurate($data){ ?>
    <?php if (count($data)=='0'): ?>
        <tr>
          <td class="tengah">Tidak ada data..</td>
        </tr>
      <?php else: ?>
    <?php foreach ($data as $key ) { ?>  
    <tr>
      <td><?php echo $key->va_nama ?></td>
      <td class="tengah"><a href="new-usaha?dva=<?php echo $key->uva_id ?>" class="btn btn-danger">Delete</a></td>
    </tr>
    <? } ?>
    <?php endif ?>
<?php } ?>  