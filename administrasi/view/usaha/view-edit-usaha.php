<!DOCTYPE html>
<html>
<?php include_once 'view/main-component/header.php'; ?>

<body>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCUn6Q6sg0JVJBlgiz4tL2kF1zmVoC0ScE&libraries=places&sensor=true"></script>
<?php include_once 'map-script.php'; ?>
<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
 ?>


<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->subtitle ?></h3>
	</div>

    <form action="" method='post'>

        <div class='row'>
            <div class='form-group'>
                <div class='col-md-8'>
                    <label>Nama Perusahaan</label>
                    <input class='form-control' name='data[np]' value="<?php echo $data->nama_usaha ?>" required maxlength="150" />
                </div>
            </div>
        </div>
        <br>
        <div class='row'>
            <div class='form-group'>
                <div class='col-md-10'>
                    <label>E-mail</label>
                    <input class='form-control' name='data[email]' value="<?php echo $data->email ?>" />        
                </div>
            </div>
        </div>
        <br>
        <div class='row'>
            <div class='form-group'>
                <div class='col-md-10'>
                    <label>Alamat</label>
                    <textarea class='form-control' name='data[alamat]' required maxlength="500"><?php echo $data->alamat ?></textarea>                    
                </div>
            </div>
        </div>
        <br>
        <div class='row'>
            <div class='form-group'>
                <div class='col-md-10'>
                    <label>Telepon</label>
                    <input class='form-control' name='data[telepon]' value="<?php echo $data->telepon ?>" />
                </div>
            </div>
        </div>
        <br>
        <div class='row'>
            <div class='col-md-10'>
                <div class='form-group'>
                    <label>Provinsi</label>
                    <input id="namaprov" type="hidden" name="data[namaprov]" value="">
                    <select id="prv" name="data[provinsi]" class="form-control"></select>
                </div>
            </div>
        </div>
        <br>
        <div class='row'>
            <div class='form-group'>
                <div class='col-md-10'>
                    <label>Kota</label>
                    <input id="namakota" type="hidden" name="data[namakota]" value="">
                    <select id="cty" name="data[kota]" class="form-control"></select>
                </div>
            </div>
        </div>
        <br>
        <div class='row'>
            <div class='form-group'>
                <div class='col-md-10'>
                    <label>Kecamatan</label>
                    <input id="namakec" type="hidden" name="data[namakec]" value="">
                    <select id="sbd" name="data[sbd]" class="form-control"></select>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                <h4>Jenis Usaha</h4>
                    <a href="new-usaha?id=<?php echo $data->id_perusahaan ?>" target="_blank">Edit?, klik disini!</a>                                               
                </div>
            </div>
        </div>
        <br>
        <div class='row'>
            <div class='col-md-10'>
                <div class='form-group'>
                    <label>Keterangan Jenis Usaha</label>
                    <textarea id="ketJenisUsaha" name="data[ketJenisUsaha]" class="form-control"><?php echo $data->ketjenisusaha ?></textarea>
                </div>
            </div>
        </div>

        <div class='row'>
            <div class='col-md-10'>
                <div class='form-group'>
                    <h4>Versi Accurate</h4>
                    <a href="new-usaha?id=<?php echo $data->id_perusahaan ?>" target="_blank">Edit?, klik disini!</a>
                </div>
            </div>
        </div>


        <br>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label for="alamat">Lokasi Map (Map otomatis terupdate, harap input ulang..)</label>
                    <input type="text" name="data[map]" id="alamatz" class="form-control" placeholder="Cari Nama Tempat atau Tandai Peta di Bawah ini">                    
                </div>
            </div>
        </div>
        <br>
        <div class="row" style="margin-bottom:20px">
            <div class="col-md-10">
                <div id="map-canvas" style="width:100%;height:400px;border:1px solid"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10">
                <button class='btn btn-lg btn-primary' style="width:100%">Submit</button>        
            </div>
        </div>
    </form>

</div>
<script type="text/javascript">
    $(document).ready(function(){
        LoadProv(<?= $data->provinsi ?>);
        $("form").bind("keypress", function (e) {
            if (e.keyCode == 13) {
                $("#btnSearch").attr('value');
                //add more buttons here
                return false;
            }
        });
    });
    $('#prv').change(function() {
      document.getElementById("namaprov").value = $("#prv option:selected").text();
      LoadCity();
    });

    $('#cty').change(function() {
      document.getElementById("namakota").value = $("#cty option:selected").text();
      LoadSubdistrict();
    });

    $('#sbd').change(function() {
      document.getElementById("namakec").value = $("#sbd option:selected").text();
      LoadService($('#dv').val());
    });

    function LoadProv(id){
      $('.optprv').remove();
      $.ajax({
            type: "GET",
            url: "https://fac-institute.com/administrasi/wilayah/provinsi/",
            data: {
              'q':'province'
            },
            cache: false,
            success: function(data){
              // console.log(data);
              
              var myjson = data;
              var result = myjson.rajaongkir.results
              for (x in result) {
                if (result[x].province_id==id) {
                  document.getElementById("prv").innerHTML += "<option class='optprv' selected value='" + result[x].province_id + "'>" + result[x].province + '</option>';
                } else {
                  document.getElementById("prv").innerHTML += "<option class='optprv' value='" + result[x].province_id + "'>" + result[x].province + '</option>';
                }
              }
              document.getElementById("namaprov").value = $("#prv option:selected").text();
              LoadCity();
            }
          }); //end ajax
    }

    function LoadCity() {
      $('.optcty').remove();
      $.ajax({
            type: "GET",
            url: "https://fac-institute.com/administrasi/wilayah/kota/" + $('#prv').val(),
            data: {
              '':''
            },
            cache: false,
            success: function(data){
              var myjson = data;
              var result = myjson.rajaongkir.results
              for (x in result) {
                if (result[x].city_id==<?= $data->kota ?>) {
                  document.getElementById("cty").innerHTML += "<option class='optcty' selected value='" + result[x].city_id + "'>" + result[x].type + " "+ result[x].city_name + '</option>';
                } else {
                  document.getElementById("cty").innerHTML += "<option class='optcty' value='" + result[x].city_id + "'>" + result[x].type + " "+ result[x].city_name + '</option>';
                }
              }
              document.getElementById("namakota").value = $("#cty option:selected").text();
              LoadSubdistrict();
            }
          }); //end ajax
    }

    function LoadSubdistrict(){
      $('.optsbd').remove();
      $.ajax({
            type: "GET",
            url: "https://fac-institute.com/administrasi/wilayah/subdist/" + $('#cty').val(),
            data: {
              '': ''
            },
            cache: false,
            success: function(data){
              var myjson = data;
              var result = myjson.rajaongkir.results
              for (x in result) {
                if (result[x].subdistrict_id==<?= $data->kecamatan ?>) {
                  document.getElementById("sbd").innerHTML += "<option class='optsbd' selected value='" + result[x].subdistrict_id + "'>" + result[x].subdistrict_name + '</option>';
                } else {
                  document.getElementById("sbd").innerHTML += "<option class='optsbd' value='" + result[x].subdistrict_id + "'>" + result[x].subdistrict_name + '</option>';
                }
              }
              document.getElementById("namakec").value = $("#sbd option:selected").text();
            }
          }); //end ajax
    }
</script>
</body>
</html> 