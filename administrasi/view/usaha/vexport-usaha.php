<!DOCTYPE html>
<html>
<?php include_once 'view/main-component/header.php'; ?>

<body>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCUn6Q6sg0JVJBlgiz4tL2kF1zmVoC0ScE&libraries=places&sensor=true"></script>
<?php include_once $data->homedir.'administrasi/map-script.php'; ?>
<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>


<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->subtitle ?></h3>
	</div>

    <form action="" method='post'>

        <div class='row'>
            <div class='form-group'>
                <div class='col-md-8'>
                    <label>Nama Perusahaan</label>
                    <input class='form-control' name='data[np]' required maxlength="150" value="<?= $data->namausaha ?>" />
                </div>
            </div>
        </div>
        <br>
        <div class='row'>
            <div class='form-group'>
                <div class='col-md-10'>
                    <label>E-mail</label>
                    <input class='form-control' name='data[email]' value="<?= $data->emailusaha ?>" />        
                </div>
            </div>
        </div>
        <br>
        <div class='row'>
            <div class='form-group'>
                <div class='col-md-10'>
                    <label>Alamat</label>
                    <textarea class='form-control' name='data[alamat]' required maxlength="500"><?= $data->alamatusaha ?></textarea>                    
                </div>
            </div>
        </div>
        <br>
        <div class='row'>
            <div class='form-group'>
                <div class='col-md-10'>
                    <label>Kota</label>
                    <input class='form-control' name='data[kota]' value="<?= $data->kotausaha ?>" />
                </div>
            </div>
        </div>
        <br>
        <div class='row'>
            <div class='form-group'>
                <div class='col-md-10'>
                    <label>Telepon</label>
                    <input class='form-control' name='data[telepon]' value="<?= $data->teleponusaha ?>" />
                </div>
            </div>
        </div>
        <br>
        <div class='row'>
            <div class='col-md-10'>
                <div class='form-group'>
                    <label>Provinsi</label>
                    <select id='provinsi' name="data[provinsi]" class='form-control'>
                        <option value=''>--Pilih Provinsi--</option>
                        <option value='aceh'>Nanggroe Aceh Darussalam</option>
                        <option value='sumatera utara'>Sumatera Utara</option>
                        <option value='riau'>Riau</option>
                        <option value='kepulauan riau'>Kepulauan Riau</option>
                        <option value='sumatera barat'>Sumatera Barat</option>
                        <option value='jambi'>Jambi</option>
                        <option value='bengkulu'>Bengkulu</option>
                        <option value='bangka belitung'>Bangka Belitung</option>
                        <option value='sumatera selatan'>Sumatera Selatan</option>
                        <option value='lampung'>Lampung</option>

                        <option value='banten'>Banten</option>
                        <option value='jawa barat'>Jawa Barat</option>
                        <option value='jakarta' selected>Jakarta</option>
                        <option value='jawa tengah'>Jawa tengah</option>
                        <option value='jogjakarta'>Jogjakarta</option>
                        <option value='jawa timur'>Jawa Timur</option>

                        <option value='bali'>Bali</option>
                        <option value='ntb'>Nusa tenggara Barat</option>
                                                                
                        <option value='kalbar'>Kalimantan Barat</option>
                        <option value='kaltim'>Kalimantan Timur</option>
                        <option value='kalteng'>Kalimantan Tengah</option>
                        <option value='kalsel'>Kalimantan Selatan</option>
                        <option value='kalut'>Kalimantan Utara</option>

                        <option value='sulbar'>Sulawesi Barat</option>
                        <option value='sulsel'>Sulawesi Selatan</option>
                        <option value='sulteng'>Sulawesi Tengah</option>
                        <option value='sultra'>Sulawesi Tenggara</option>
                        <option value='gorontalo'>Gorontalo</option>
                        <option value='sulut'>Sulawesi Utara</option>

                        <option value='maluku utara'>Maluku Utara</option>
                        <option value='maluku'>Maluku</option>

                        <option value='papua barat'>Papua Barat</option>
                        <option value='papua'>Papua</option>
                    </select>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                <h4>Jenis Usaha</h4>
                    <?php //JenisUsaha($data->jenisusaha) ?>
                    <?php foreach ($data->jenisusaha as $key ) { ?>
                        <div class="checkbox">
                            <label><input name="data[jnsUsaha][]" value="<?php echo $key->id ?>" type="checkbox"><?php echo $key->jenis_usaha ?></label>
                        </div>
                    <? } ?>                                            
                </div>
            </div>
        </div>
        <br>
        <div class='row'>
            <div class='col-md-10'>
                <div class='form-group'>
                    <label>Keterangan Jenis Usaha</label>
                    <textarea id="ketJenisUsaha" name="data[ketJenisUsaha]" class="form-control"><?= $data->ketjenisusaha ?></textarea>
                </div>
            </div>
        </div>

        <div class='row'>
            <div class='col-md-10'>
                <div class='form-group'>
                    <h4>Versi Accurate</h4>
                    <?php //VersiAccurate($data->accurates) ?>
                    <?php foreach ($data->accurates as $key ) { ?>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="data[jnsAccurate][]" value="<?php echo $key->va_id ?>">
                                <?php echo $key->va_nama ?>
                            </label>
                        </div>
                    <? } ?>
                </div>
            </div>
        </div>


        <br>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label>Link Map sudah benar?</label>
                    <br>
                    <span><a href="#" title="">https://www.google.co.id/maps/place/somewhere</a></span>
                    <div class="radio">
                        <label><input type="radio" id="ryes" name="data[linkmap]" value="y" checked>Ya</label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" id="rno" name="data[linkmap]" value="n">Tidak</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" zeke-group="map">
            <div class="col-md-10">
                <div class="form-group">
                    <label for="alamat">Lokasi Map</label>
                    <input type="text" name="data[map]" id="alamatz" class="form-control" placeholder="Cari Nama Tempat atau Tandai Peta di Bawah ini">                    
                </div>
            </div>
        </div>

        <br>
        <div class="row" style="margin-bottom:20px" zeke-group="map">
            <div class="col-md-10">
                <div id="map-canvas" style="width:100%;height:400px;border:1px solid"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <button class='btn btn-lg btn-primary' style="width:100%">Submit</button>        
            </div>
        </div>
    </form>

</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("form").bind("keypress", function (e) {
        if (e.keyCode == 13) {
            $("#btnSearch").attr('value');
            //add more buttons here
            return false;
        }
        });
        $("div[zeke-group='map']").fadeOut();//css('display','none');
        // console.log($("div[zeke-group='map']"));
    });
    $("#ryes").click(function(){
        $("div[zeke-group='map']").fadeOut();//css('display','none');
    });

    $("#rno").click(function(){
        $("div[zeke-group='map']").fadeIn();
    });
</script>
</body>
</html> 

<?php function JenisUsaha($data){ ?>
    <?php foreach ($data as $key ) { ?>
        <div class="checkbox">
            <label><input name="data[jnsUsaha][]" value="<?php echo $key->id ?>" type="checkbox"><?php echo $key->jenis_usaha ?></label>
        </div>
    <? } ?>
<?php } ?>

<?php function VersiAccurate($data){ ?>
    <?php foreach ($data as $key ) { ?>
    <div class="checkbox">
        <label>
            <input type="checkbox" name="data[jnsAccurate][]" value="<?php echo $key->va_id ?>">
            <?php echo $key->va_nama ?>
        </label>
    </div>
    <? } ?>
<?php } ?>
