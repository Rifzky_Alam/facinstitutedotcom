<!DOCTYPE html>
<html>
<?php include_once 'view/main-component/header.php'; ?>

<body>

<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
  <div class="page-header" id="top-logo">
    <h3><?php echo $data->subtitle ?></h3>
  </div>
  
  <div class="row">
    <div class="col-md-12">
      <h3>Anda yakin akan menghapus data ini?</h3>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th class="tengah">Jenis Usaha</th>
          </tr>
        </thead>
        <tbody>
          <?php DataJenisUsaha($data->listjenisusaha) ?>
        </tbody>
      </table>
    </div>
  </div>

  <form action="" method="post">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group" style="display:none;">
        <label>Jenis Usaha</label>
        <input type="text" name="in[ju]" value="<?php echo $data->ju_id ?>">
      </div> 
      <button class="btn btn-lg btn-primary" style="width:50%;">Ya, Hapus Data</button>
      <a href="new-usaha?id=<?php echo $data->id_perusahaan ?>" class="btn btn-lg btn-warning" style="width:40%">Kembali</a>    
    </div>
  </div>
  </form>

</div>
     
</body>
</html>


<?php function DataJenisUsaha($data){ ?>
    <?php if (count($data)=='0'): ?>
        <tr>
          <td class="tengah">Tidak ada data..</td>
        </tr>
      <?php else: ?>
    <?php foreach ($data as $key ) { ?>  
      <td class="tengah"><?php echo $key->jenis_usaha ?></td>
    <? } ?>
    <?php endif ?>
<?php } ?>  