<!DOCTYPE html>
<html>
<?php include_once 'view/main-component/header.php'; ?>

<body>

<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
  <div class="page-header" id="top-logo">
    <h3><?php echo $data->subtitle ?></h3>
  </div>
  
  <form action="" method="get">
  <div class="row">
    <div class="col-md-12">
      <input type="text" name="namausaha" class="form-control" placeholder="Search Data by Name ...">
    </div>
  </div>
  </form>

  <br><br>

  <?php DataUsaha($data->mylists) ?>

</div>
     
</body>
</html>






<?php function DataUsaha($data){ ?>
    <?php if (count($data)=='0'): ?>
        <div class="row">
            <div class="col-md-12">
                <div class="jumbotron">
                    <h3>Data yang anda cari tidak ada</h3>
                    <p>Pastikan mengetik nama perusahaan dengan benar, anda dapat mencari nama perusahaan dengan pecahan kata dari nama, contoh: ketik 'business' dari perusahaan Accurate Business Center Mal Metropolitan. <br>
                    bila masih menemukan kendala harap hubungi admin sistem kami <a href="tel:\\081279222250">(Rifzky Alam)</a>
                    </p>
                </div>
            </div>
        </div>
        <?php else: ?>

        <?php foreach ($data as $key) { ?>
        <div class='row'>
            <div class="col-md-12">
                <div class='panel panel-default'>
                    <div class='panel-heading'>
                        <?php if ($key->status=='1'): ?>
                            <h3 class='panel-title'><?php echo $key->nama ?> (Transaksi)</h3>
                        <?php elseif($key->status=='51'): ?>
                            <h3 class='panel-title'><?php echo $key->nama ?> (Support)</h3>
                        <?php endif ?>
                    </div>
                    <div class='panel-body' style='overflow-x:auto'>
                        <section>
                            <h4><?= $key->ket_jenis_usaha ?></h4>
                            <p><?= $key->alamat ?></p>
                        </section>

                            
                        <button class='btn btn-primary' data-toggle='collapse' data-target="#<?php echo $key->id ?>">Klik Untuk detail</button>
                        <br>
                        <div class='collapse' id='<?php echo $key->id ?>' style='margin-top:15px'>
                            <label>Alamat:</label>
                            <p>
                                <?php echo $key->alamat ?>                    
                            </p>
                            <table class='table'>
                                <thead>
                                    <tr>
                                        <th>Email</th>
                                        <th>Telepon</th>
                                        <th>Kota</th>
                                        <th>Provinsi</th>
                                        <th>Map</th>                                
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?php echo $key->email ?></td>
                                        <td>
                                            <a href='tel://<?php echo $key->telepon ?>' class="keterangan"><?php echo $key->telepon ?></a>
                                        </td>
                                        <td><?php echo $key->kota ?></td>
                                        <td><?php echo $key->provinsi ?></td>
                                        <td>
                                            <a href="https://www.google.co.id/maps/place/<?php echo $key->map ?>">
                                                <?php echo $key->map ?>        
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class='form-inline'>
                                <?php if ($key->status=='1'): ?>
                                    <a href="new-usaha?edt=<?php echo $key->id ?>" class='btn btn-warning'>Edit</a>
                                    <a href="new-usaha?id=<?php echo $key->id ?>" class="btn btn-default" >Detail</a>
                                    <a href="new-usaha?iptc=<?php echo $key->id ?>" class='btn btn-success'>Input Cabang</a>
                                    <a href="new-perusahaan-info?p=<?php echo $key->id ?>" class="btn btn-info">Transaksi >></a>
                                <?php elseif($key->status=='51'): ?>
                                    <a href="new-usaha?id=<?php echo $key->id ?>" class="btn btn-danger" >Detail</a>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>        

        <?php } ?>

    <?php endif ?>

<?php } ?>
