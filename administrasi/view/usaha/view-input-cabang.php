<!DOCTYPE html>
<html>
<?php include_once 'view/main-component/header.php'; ?>

<body>

<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
  <div class="page-header" id="top-logo">
    <h3><?php echo $data->subtitle ?></h3>
  </div>
  
  <form action="" method="post">
  <div class="row">
    <div class="col-md-12">

        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Nama Perusahaan</th>
                    <th>Alamat</th>
                    <th>Kota</th>
                    <th>Provinsi</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?php echo $data->nama_usaha ?></td>
                    <td><?php echo $data->alamat ?></td>
                    <td><?php echo $data->kota ?></td>
                    <td><?php echo $data->provinsi ?></td>
                </tr>
            </tbody>
        </table>

        <div class="form-group" style="display:none;">
            <input type="text" name="in[idusaha]" class="form-control" value="<?php echo $data->id_perusahaan ?>">
        </div>
      <button class="btn btn-lg btn-primary">Ya, Input sebagai cabang marketing FAC Institute</button>
      <a href="new-usaha" class="btn btn-lg btn-warning">Kembali ke Perusahaan</a>
    </div>
  </div>
  </form>

  <br><br>

  

</div>
     
</body>
</html>
