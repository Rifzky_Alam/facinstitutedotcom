<!DOCTYPE html>
<html>
<?php include_once 'view/main-component/header.php'; ?>

<body>

<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
  <div class="page-header" id="top-logo">
    <h3><?php echo $data->subtitle ?></h3>
  </div>
  
  <form action="" method="get">
  <div class="row">
    <div class="col-md-12">
      <input type="text" name="namausaha" class="form-control" placeholder="Search Data by Name ...">
    </div>
  </div>
  </form>

  <br><br>

  <?php DataUsaha($data) ?>

</div>
     
</body>
</html>






<?php function DataUsaha($data){ ?>

    <?php if (count($data->mylists)=='0'): ?>
        <div class="row">
            <div class="col-md-12">
                <div class="jumbotron">
                    <h3>Data yang anda cari tidak ada</h3>
                    <p>Pastikan mengetik nama perusahaan dengan benar, anda dapat mencari nama perusahaan dengan pecahan kata dari nama, contoh: ketik 'business' dari perusahaan Accurate Business Center Mal Metropolitan. <br>
                    Bila perusahaan tersebut tidak terdaftar silahkan klik link berikut untuk melanjutkan <a href="<?= $data->base_url.'administrasi/customerservice/newsupport' ?>" class="btn btn-success">Lanjutkan >></a>
                    </p>
                </div>
            </div>
        </div>
        <?php else: ?>

        <?php foreach ($data->mylists as $key) { ?>
        <div class='row'>
            <div class="col-md-12">
                <div class='panel panel-default'>
                    <div class='panel-heading'>
                        <h3 class='panel-title'><?php echo $key->nama. '   ('.$key->status.')' ?></h3>
                    </div>
                    <div class='panel-body' style='overflow-x:auto'>
                        <section>
                            <h4><?= $key->ket_jenis_usaha ?></h4>
                            <p><?= $key->alamat ?></p>
                        </section>

                        <button class='btn btn-primary' data-toggle='collapse' data-target="#<?php echo $key->id ?>">Klik Untuk detail</button>
                        <br>
                        <div class='collapse' id='<?php echo $key->id ?>' style='margin-top:15px'>
                            <table class='table'>
                                <thead>
                                    <tr>
                                        <th>Email</th>
                                        <th>Telepon</th>
                                        <th>Kota</th>
                                        <th>Provinsi</th>
                                        <th>Map</th>                                
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?php echo $key->email ?></td>
                                        <td>
                                            <a href='tel://<?php echo $key->telepon ?>' class="keterangan"><?php echo $key->telepon ?></a>
                                        </td>
                                        <td><?php echo $key->kota ?></td>
                                        <td><?php echo $key->provinsi ?></td>
                                        <td>
                                            <a href="https://www.google.co.id/maps/place/<?php echo $key->map ?>"><?php echo $key->map ?></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Nama Cust</th>
                                        <th class="tengah">Aksi</th>
                                    </tr>
                                </thead>
                                <?php $listcust = explode(' # ', $key->customers) ?>
                                <tbody>
                                    <?php if ($key->customers==''): ?>
                                    <tr>
                                        <td colspan="2" style="text-align:center;"><a href="<?= $data->base_url.'administrasi/customerservice/newclient?id='.$key->id ?>" title="Klien Baru">New Client</a></td>
                                    </tr>
                                    <?php else: ?>
                                        <?php foreach ($listcust as $value): ?>
                                            <?php $datacus = explode('|*|', $value) ?>
                                            <?php //print_r($datacus) ?>
                                            <tr>
                                                <td><?= $datacus[1] ?></td>
                                                <td style="text-align:center;">
                                                    <a href="<?= $data->base_url.'administrasi/customerservice/newsupport?id='.$key->id.'&k='.$datacus[0] ?>" title="Klien Baru" class='btn btn-warning'>
                                                        New Data Support
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php endforeach ?>
                                        <tr>
                                            <td colspan="2" class="tengah">
                                                <a href="<?= $data->base_url.'administrasi/customerservice/newclient?id='.$key->id ?>" class="btn btn-danger" title="new data">
                                                        Tambah
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endif ?>
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>
            </div>
</div>        

        <?php } ?>

    <?php endif ?>

<?php } ?>
