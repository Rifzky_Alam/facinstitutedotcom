<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>


<div class="container" id="isi" style="padding-bottom:40px;">
	<div class="page-header" id="top-logo">
		<h3><?= $data->subtitle ?></h3>
	</div>


        <div class='row' style="overflow:auto;">
            <div class='col-md-12'>
                <a href="<?= $data->base_url.'administrasi/customerservice/data-usaha' ?>" class="btn btn-primary" title="New Data">New Data</a>
                <a href="#modal-cariz" data-toggle="modal" title="Search" class="btn btn-info" title="Search">Search</a>
                <?php if($data->querystring!=''): ?>
                    <a href="<?= $data->base_url.'administrasi/customerservice/download?'.$data->querystring ?>" title="Download" class="btn btn-success">Download</a>    
                <?php endif ?>
                <br><br>
                <table class='table table-bordered'>
                    <thead>
                        <tr>
                            <th class='tengah' style="vertical-align:middle;">No Tiket</th>
                            <th class='tengah' style="vertical-align:middle;">Tanggal</th>
                            <th class='tengah' style="vertical-align:middle;">Perusahaan</th>
                            <th class='tengah' style="vertical-align:middle;">Client</th>
                            <th class='tengah' style="vertical-align:middle;">Topik</th>
                            <th class='tengah' style="vertical-align:middle;">Solusi</th>
                            <th class='tengah' style="vertical-align:middle;">PIC/Petugas</th>
                            <th class='tengah' style="vertical-align:middle;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php if (count($data->listdata)=='0'): ?>
                        <tr>
                            <td colspan="8" class="tengah">
                                Tidak ada data dalam database kami.
                            </td>
                        </tr>    
                    <?php else: ?>
                        <?php foreach ($data->listdata as $key): ?>
                            <tr>
                                <td style="vertical-align:middle;"><?= $key['sticket'] ?></td>
                                <td style="vertical-align:middle;"><?= $key['tanggal'] ?></td>
                                <td style="vertical-align:middle;"><?= $key['nama_usaha'] ?></td>
                                <td style="vertical-align:middle;"><?= $key['sname'] ?></td>
                                <td style="vertical-align:middle;"><?= $key['sisstitle'] ?></td>
                                <td style="vertical-align:middle;"><?= $key['ssolution'] ?></td>
                                <td style="vertical-align:middle;"><?= $key['spic'] ?></td>
                                <td style="vertical-align:middle;">
                                    <a href="<?= $data->base_url.'administrasi/customerservice/data-support?edt='.$key['sticket'] ?>" title="Ubah" class="btn btn-danger">
                                        Edit
                                    </a>
                                    <a href="<?= $data->base_url.'administrasi/customerservice/data-support?det='.$key['sticket'] ?>" title="Detail" class="btn btn-warning">Detail</a>
                                    <?php if($key['sphone']!=''||$key['sphone']!='-'): ?>
                                    <span onclick="<?= 'LinkWhatsapp('.EditPhoneNumberForDesktop($key['sphone']).')' ?>" title="send thanks via whatapp" class="btn btn-success">Send Thanks (WA)</span>
                                    <?php endif ?>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                    </tbody>
                </table>
                
            </div>
        </div>
        <?php Pagination($data,30,$data->pagenum,$data->totaldata,5) ?>
</div>

<!-- Modal -->
  <div class='modal fade' id='modal-cariz' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pencarian Data</h3>

        </div>
        <div class='modal-body'>
        <div style='padding:15px; height: 300px;overflow: auto;' class='row'>
          
          <div class="col-md-12">
            <form action="" method="GET">
                    <div class="row">
                        <div class="form-group">
                            <label for="namausaha">Nama Perusahaan</label>
                            <input type="text" id="namausaha" name="src[np]" class="form-control" placeholder="Nama Perusahaan">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="client">Nama Client</label>
                            <input type="text" name="src[client]" id="client" class="form-control" placeholder="Nama Klien, bisa dengan keyword sebagian nama" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label for="topik">Topik Masalah</label>
                            <input type="text" name="src[topik]" id="topik" class="form-control" placeholder="topik masalah" />
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="form-group">
                            <label for="fromdate">Dari Tanggal</label>
                            <input type="text" id="fromdate" name="src[fd]" class="form-control" placeholder="Dari Tanggal" />
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="form-group">
                            <label for="todate">Sampai Tanggal</label>
                            <input type="text" id="todate" name="src[td]" class="form-control" placeholder="Sampai Tanggal" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label for="dari-tanggal">PIC/Petugas Support</label>
                            <select name="src[username]" class="form-control" id="usernamez">
                                <option value="">-- Pilih User --</option>
                                <?php foreach ($data->users as $key): ?>
                                    <option value="<?= $key['username'] ?>"><?= $key['nama'] ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>


                    <div class="row">
                        <button class="btn btn-primary" id='btn-downloadLaporan'>Cari</button>
                    </div>
            </form>

          </div>
             
          

        </div>
          

        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>FAC-Institute 2016</span>
                </div>
                <div class="col-md-6">
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->

<script type="text/javascript" src=<?php echo "'".$data->base_url."js/jquery-ui/jquery-ui.js'"; ?>></script>
  <script type="text/javascript">
    $(document).ready(function(){
        $('#todate').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('#fromdate').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('#tanggal-akhir').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('.tanggalPelaksanaan').datepicker({
            dateFormat: 'yy-mm-dd'
        });
    });

    

    function removeElement(id) {
        $('#'+id).remove();
    }

    function LinkWhatsapp(nomornya){
        
  	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
 		var win = window.open("https://wa.me/"+nomornya+"?text=Terima%20kasih%20telah%20menghubungi%20FAC%20Support%20Division.%20Silahkan%20kunjungi%20website%20kami%20di%20https%3A%2F%2Ffac-institute.com%2Ftutorial%2F%20dan%20dapatkan%20tutorial%20praktis%20penggunaan%20accurate", "_blank");
        win.focus();
 		 //		$('#whatsappz').prop('href', );
	}else{
	    var win = window.open("https://web.whatsapp.com/send?phone=+"+nomornya+"&text=Terima%20kasih%20telah%20menghubungi%20FAC%20Support%20Division.%20Silahkan%20kunjungi%20website%20kami%20di%20https%3A%2F%2Ffac-institute.com%2Ftutorial%2F%20dan%20dapatkan%20tutorial%20praktis%20penggunaan%20accurate", "_blank");
        win.focus();
// 		$('#whatsappz').prop('href', );
		// alert('Web detected!');
	}
  }

  </script>   
</body>
</html> 
<?php
function EditPhoneNumberForDesktop($nomor){
    $hasil = '';
    if($nomor!=''){
        $nomor = str_replace('-', '', $nomor);
        $hasil = '62'.substr($nomor,1,strlen($nomor));
    }
    return $hasil;
}

?>

<?php function Pagination($data,$limit,$page,$totaldata,$limitlist){ ?>
    <?php if ($page!='0'): ?>
        <div class="row">
            <div class="col-md-12">
                <center>
        <?php if (intval($page)*$limit<$totaldata): ?>
            Showing data <? echo (intval($page)*$limit)-$limit . ' - ' . intval($page)*$limit . ' of '.$totaldata;?>    
            <?php else: ?>
            Showing data <? echo (intval($page)*$limit)-$limit . ' - ' . $totaldata . ' rows of '.$totaldata;?>
        <?php endif ?>
                </center>
            </div>
        </div>
    <?php else: ?>
        <div class="row">
            <div class="col-md-12">
                <center>
                    <?php if ($totaldata<=$limit): ?>
                    Showing data 0 - <?php echo $totaldata ?> rows of <?php echo $totaldata; ?>
                        <?php else: ?>
                    Showing data 0 - <?php echo $limit ?> rows of <?php echo $totaldata; ?>
                    <?php endif ?>
                    
                </center>
            </div>
        </div>
    <?php endif ?>


    <div class="row">
        <div class="col-md-12">
        <center>
            <ul class="pagination pagination-sm">

            <?php 

                $page = $data->pagenum;
                
                if ($page=='0') {
                    $blok = 0;
                } else {
                    $blok = intval($page/$limitlist);    
                }

                if( $data->totaldata > 30){
                    if ($blok>0) {
                        $j = $j=intval($blok)*intval($limitlist)-1;
                        echo "<li><a href='".
                        $data->base_url.
                        "administrasi/customerservice/data-support?page=" .
                        $j.
                         "'>< Prev</a></li>";
                    }
    
    
                    for($i=intval($blok*$limitlist);$i<intval($blok*$limitlist)+5;$i++){
                        // $jk = $i + $blok * $limitlist + 1;
                        if ($i*$limit<$totaldata) {
                            $kk=$i+1;
                            echo "<li><a href='".$data->base_url."administrasi/customerservice/data-support?page=$kk'>$kk</a></li>"; 
                        }
                    }
    
                    if ($blok < $totaldata/$limitlist&&$i*$limit<$totaldata) {
                        $j=intval($blok)*intval($limitlist)+6;
                        echo "<li><a href='".
                        $data->base_url.
                        "administrasi/customerservice/data-support?page=".
                         $j.
                        "'>Next ></a></li>";
                    }
                }
                    
            ?>
            </ul>
        </center>
        </div>
    </div>

<? } //end pagination ?>

