<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>


<div class="container" id="isi" style="padding-bottom:40px;">
	<div class="page-header" id="top-logo">
		<h3><?= $data->subtitle ?></h3>
	</div>


        <div class='row'>
            <div class='col-md-12'>
                <form action="" method="post" accept-charset="utf-8">
                <div class="form-group">
                    <label>Nama Perusahaan <a href="#"><span class="glyphicon glyphicon-edit"></span></a></label>
                    <input id="namacust" type="text" class="form-control" placeholder="Nama Perusahaan yang berkomunikasi dengan anda" value="<?= $data->nama_usaha ?>" disabled>
                </div>
                <div class="form-group">
                    <label>Nama Customer <a href="<?= $data->base_url.'administrasi/customerservice/editclient?id='.$data->idcust ?>"><span class="glyphicon glyphicon-edit"></span></a></label>
                    <input id="namacust" type="text" class="form-control" placeholder="Nama client yang berkomunikasi dengan anda" value="<?= $data->cust ?>" disabled>
                    <input type="hidden" name="in[id]" value="<?= $data->id ?>" />
                </div>
                <div class="form-group">
                    <label>Topik Masalah</label>
                    <textarea name="in[title]" class="form-control" placeholder="Judul masalah yang sedah dibahas"><?= $data->topik ?></textarea>
                </div>
                <div class="form-group">
                    <label>Solusi</label>
                    <textarea name="in[solution]" class="form-control" placeholder="Solusi untuk masalah yang sedang di bahas"><?= $data->solusi ?></textarea>
                </div>
                <div class="form-group">
                    <label>Media/Via</label>
                    <select name="in[via]" class="form-control" required>
                        <?php foreach($data->listmedia as $key): ?>
                        <?php if($data->media == $key['dt_flag']): ?>
                            <option value="<?= $key['dt_flag'] ?>" selected="selected"><?= $key['dt_desc'] ?></option>
                        <?php else: ?>
                            <option value="<?= $key['dt_flag'] ?>"><?= $key['dt_desc'] ?></option>
                        <?php endif ?>
                        
                        <?php endforeach ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Notes</label>
                    <textarea name="in[notes]" class="form-control" placeholder="Catatan Lain"><?= $data->notes ?></textarea>
                </div>
                <button class="btn btn-lg btn-primary">Submit</button>
                </form>
            </div>
        </div>
</div>
   
</body>
</html> 



