<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>


<div class="container" id="isi" style="padding-bottom:40px;">
	<div class="page-header" id="top-logo">
		<h3><?= $data->subtitle ?></h3>
	</div>


        <div class='row'>
            <div class='col-md-12'>
                <form action="" method="post" accept-charset="utf-8">
                <div class="form-group">
                    <label>Nama Customer</label>
                    <input name="in[namacust]" type="text" class="form-control" required placeholder="Nama client yang berkomunikasi dengan anda">
                    <input type="hidden" name="in[iu]" value="<?= $data->idusaha ?>" />
                </div>
                <div class="form-group">
                    <label>Gender</label>
                    <select required name="in[gender]" class="form-control">
                        <option value="">Pilih Jenis Kelamin</option>
                        <option value="1">Pria</option>
                        <option value="2">Wanita</option>
                    </select>
                </div>                
                <div class="form-group">
                    <label>Telepon</label>
                    <input type="text" name="in[telepon]" class="form-control" required placeholder="hanya angka">
                </div>
                <div class="form-group">
                    <label>Email (jika ada)</label>
                    <input type="email" name="in[email]" class="form-control" placeholder="Email aktif, bila ada">
                </div>
                <button class="btn btn-lg btn-primary">Submit</button>
                </form>
            </div>
        </div>
</div>
   
</body>
</html> 



