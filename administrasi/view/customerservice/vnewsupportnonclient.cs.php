<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>


<div class="container" id="isi" style="padding-bottom:40px;">
	<div class="page-header" id="top-logo">
		<h3><?= $data->subtitle ?></h3>
	</div>


        <div class='row'>
            <div class='col-md-12'>
                <form action="" method="post" accept-charset="utf-8">
                <div class="form-group">
                    <label>Nama Perusahaan</label>
                    <input id="namacust" type="text" name="in[namausaha]" class="form-control" placeholder="Nama Perusahaan, cth: PT Sukses Sejahtera" required>
                    <input type="hidden" name="in[iu]" value="-">
                </div>
                <div class="form-group">
                    <label>Jenis Usaha</label>
                    <input id="namacust" type="text" name="in[ju]" class="form-control" placeholder="Jenis usaha perusahaan client yang sedang di support">
                </div>
                <div class="form-group">
                    <label>Jenis Accurate</label>
                    <input id="namacust" type="text" name="in[ja]" class="form-control" placeholder="Jenis Accurate yang sedang di support">
                </div>
                <div class="form-group">
                    <label>Nama Customer</label>
                    <input id="namacust" type="text" name="in[nc]" class="form-control" placeholder="Nama client yang berkomunikasi dengan anda" required>
                </div>
                <div class="form-group">
                    <label>Telepon</label>
                    <input type="text" name="in[telepon]" class="form-control" placeholder="Telepon Customer">
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" name="in[email]" placeholder="Email client (bila ada)" class="form-control">
                </div>
                <div class="form-group">
                    <label>Topik Masalah</label>
                    <textarea name="in[title]" class="form-control" placeholder="Judul masalah yang sedah dibahas"></textarea>
                </div>
                <div class="form-group">
                    <label>Solusi</label>
                    <textarea name="in[solution]" class="form-control" placeholder="Solusi untuk masalah yang sedang di bahas"></textarea>
                </div>
                <div class="form-group">
                    <label>Media/Via</label>
                    <select name="in[via]" class="form-control" required>
                        <option value="">--Pilih Media Komunikasi Dengan Client--</option>
                        <?php foreach($data->listmedia as $key): ?>
                        <option value="<?= $key['dt_flag'] ?>"><?= $key['dt_desc'] ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                <button class="btn btn-lg btn-primary">Submit</button>
                </form>
            </div>
        </div>
</div>
   
</body>
</html> 



