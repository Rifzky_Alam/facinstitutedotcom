<!DOCTYPE html>
<html>
<?php include_once 'view/main-component/header.php'; ?>

<body>
<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
 ?>


<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->subtitle ?></h3>
	</div>

    <form action="" method='post'>

        <div class='row'>
            <div class='form-group'>
                <div class='col-md-8'>
                    <label>Nama Perusahaan</label>
                    <input class='form-control' name='data[np]' required maxlength="150" />
                </div>
            </div>
        </div>
        <br>
        <div class='row'>
            <div class='form-group'>
                <div class='col-md-10'>
                    <label>E-mail (Jika Ada)</label>
                    <input class='form-control' name='data[email]' />        
                </div>
            </div>
        </div>
        <br>
        <div class='row'>
            <div class='form-group'>
                <div class='col-md-10'>
                    <label>Telepon Perusahaan (Jika ada)</label>
                    <input class='form-control' name='data[telepon]' placeholder="Hanya angka" />
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                <h4>Jenis Usaha</h4>
                    <?php JenisUsaha($data->jenisusaha) ?>                                                
                </div>
            </div>
        </div>
        <br>
        <div class='row'>
            <div class='col-md-10'>
                <div class='form-group'>
                    <label>Keterangan Jenis Usaha</label>
                    <textarea id="ketJenisUsaha" name="data[ketJenisUsaha]" class="form-control" required></textarea>
                </div>
            </div>
        </div>

        <div class='row'>
            <div class='col-md-10'>
                <div class='form-group'>
                    <h4>Versi Accurate</h4>
                    <?php VersiAccurate($data->accurates) ?>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-10">
                <button class='btn btn-lg btn-primary' style="width:100%">Submit</button>        
            </div>
        </div>
    </form>

</div>
<script type="text/javascript">
    $(document).ready(function(){
        LoadProv('9');
        $("form").bind("keypress", function (e) {
            if (e.keyCode == 13) {
                $("#btnSearch").attr('value');
                //add more buttons here
                return false;
            }
        });
    });

    $('#prv').change(function() {
      document.getElementById("namaprov").value = $("#prv option:selected").text();
      LoadCity();
    });

    $('#cty').change(function() {
      document.getElementById("namakota").value = $("#cty option:selected").text();
      LoadSubdistrict();
    });

    $('#sbd').change(function() {
      document.getElementById("namakec").value = $("#sbd option:selected").text();
      LoadService($('#dv').val());
    });

    function LoadProv(id){
      $('.optprv').remove();
      $.ajax({
            type: "GET",
            url: "https://fac-institute.com/administrasi/wilayah/provinsi/",
            data: {
              'q':'province'
            },
            cache: false,
            success: function(data){
              // console.log(data);
              
              var myjson = data;
              var result = myjson.rajaongkir.results
              for (x in result) {
                if (result[x].province_id==id) {
                  document.getElementById("prv").innerHTML += "<option class='optprv' selected value='" + result[x].province_id + "'>" + result[x].province + '</option>';
                } else {
                  document.getElementById("prv").innerHTML += "<option class='optprv' value='" + result[x].province_id + "'>" + result[x].province + '</option>';
                }
              }
              document.getElementById("namaprov").value = $("#prv option:selected").text();
              LoadCity();
            }
          }); //end ajax
    }

    function LoadCity() {
      $('.optcty').remove();
      $.ajax({
            type: "GET",
            url: "https://fac-institute.com/administrasi/wilayah/kota/" + $('#prv').val(),
            data: {
              '':''
            },
            cache: false,
            success: function(data){
              var myjson = data;
              var result = myjson.rajaongkir.results
              for (x in result) {
                if (result[x].city_id=='79') {
                  document.getElementById("cty").innerHTML += "<option class='optcty' selected value='" + result[x].city_id + "'>" + result[x].type + " "+ result[x].city_name + '</option>';
                } else {
                  document.getElementById("cty").innerHTML += "<option class='optcty' value='" + result[x].city_id + "'>" + result[x].type + " "+ result[x].city_name + '</option>';
                }
              }
              document.getElementById("namakota").value = $("#cty option:selected").text();
              LoadSubdistrict();
            }
          }); //end ajax
    }

    function LoadSubdistrict(){
      $('.optsbd').remove();
      $.ajax({
            type: "GET",
            url: "https://fac-institute.com/administrasi/wilayah/subdist/" + $('#cty').val(),
            data: {
              '': ''
            },
            cache: false,
            success: function(data){
              var myjson = data;
              var result = myjson.rajaongkir.results
              for (x in result) {
                if (result[x].subdistrict_id=='1066') {
                  document.getElementById("sbd").innerHTML += "<option class='optsbd' selected value='" + result[x].subdistrict_id + "'>" + result[x].subdistrict_name + '</option>';
                } else {
                  document.getElementById("sbd").innerHTML += "<option class='optsbd' value='" + result[x].subdistrict_id + "'>" + result[x].subdistrict_name + '</option>';
                }
              }
              document.getElementById("namakec").value = $("#sbd option:selected").text();
            }
          }); //end ajax
    }
</script>
</body>
</html> 

<?php function JenisUsaha($data){ ?>
    <?php foreach ($data as $key ) { ?>
        <div class="checkbox">
            <label><input name="data[jnsUsaha][]" value="<?php echo $key->id ?>" type="checkbox"><?php echo $key->jenis_usaha ?></label>
        </div>
    <? } ?>
<?php } ?>

<?php function VersiAccurate($data){ ?>
    <?php foreach ($data as $key ) { ?>
    <div class="checkbox">
        <label>
            <input type="checkbox" name="data[jnsAccurate][]" value="<?php echo $key->va_id ?>">
            <?php echo $key->va_nama ?>
        </label>
    </div>
    <? } ?>
<?php } ?>
