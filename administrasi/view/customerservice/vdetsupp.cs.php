<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>


<div class="container" id="isi" style="padding-bottom:40px;">
	<div class="page-header" id="top-logo">
		<h3><?= $data->subtitle ?></h3>
	</div>


        <div class='row'>
            <div class='col-md-12'>
                <table class='table table-bordered'>
                    
                    <tbody>
                    <tr>
                        <td colspan="2" style="text-align:center;">
                            <b>Detail Support <?= $data->notiket ?></b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Tanggal
                        </td>
                        <td><?= $data->tanggal ?></td>
                    </tr>
                    <tr>
                        <td>
                            Nama Usaha
                        </td>
                        <td><?= $data->nama_usaha ?></td>
                    </tr>
                    <tr>
                        <td>
                            Nama Client
                        </td>
                        <td><?= $data->cust ?></td>
                    </tr>
                    <tr>
                        <td>
                            Telepon
                        </td>
                        <td><?= $data->telepon ?></td>
                    </tr>
                    <tr>
                        <td>
                            Email
                        </td>
                        <td><?= $data->email ?></td>
                    </tr>
                    <tr>
                        <td>
                            Topik Masalah
                        </td>
                        <td><?= $data->topik ?></td>
                    </tr>
                    <tr>
                        <td>
                            Solusi
                        </td>
                        <td><?= $data->solusi ?></td>
                    </tr>
                    <tr>
                        <td>
                            Media
                        </td>
                        <td><?= $data->media ?></td>
                    </tr>
                    <tr>
                        <td>
                            Catatan Lain
                        </td>
                        <td><?= $data->notes ?></td>
                    </tr>
                    <tr>
                        <td>
                            Penerima Support
                        </td>
                        <td><?= $data->pic ?></td>
                    </tr>
                    </tbody>
                </table>
                
            </div>
        </div>
</div>



<script type="text/javascript" src=<?php echo "'".$data->base_url."js/jquery-ui/jquery-ui.js'"; ?>></script>
  <script type="text/javascript">
    $(document).ready(function(){
        $('#editTgl').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('#tanggal-awal').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('#tanggal-akhir').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('.tanggalPelaksanaan').datepicker({
            dateFormat: 'yy-mm-dd'
        });
    });

    

    function removeElement(id) {
        $('#'+id).remove();
    }

  </script>   
</body>
</html> 



