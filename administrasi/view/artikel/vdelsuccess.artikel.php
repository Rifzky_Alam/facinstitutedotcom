<!DOCTYPE html>
<html>
<?php include_once 'view/main-component/header.php'; ?>

<body>

<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->subtitle ?></h3>
	</div>
	
     

	
	<div class="row">
		<div class="col-md-12">
      <div class="jumbotron">
        <h2>Data Berhasil dihapus</h2>
        <p>
          Data tutorial dengan judul <?= @$data->judulartikel ?> <?= @$data->msg ?>, silahkan <a href="<?= $data->base_url.'administrasi/artikel/' ?>" title="Ke tabel tutorial" class="btn btn-lg btn-primary">klik disini</a> untuk kembali ke halaman daftar tutorial.
        </p>
      </div>

		</div>
	</div>

</div>

     
</body>
</html> 

<?php function LinkAssistLocation($isadmin,$baseurl,$idtrans){
  if ($isadmin) {
    echo "<td title='Klik untuk membagikan lokasi training ke customer'><a target='_blank' href='".$baseurl."member/lokasi-training?tr=".$idtrans."'>Share map to customer</a></td>";
  } else {

  }
  
} ?>

<?php function NamaUsaha($isadmin,$idusaha,$data){
  if ($isadmin) {
    echo "<td title='Klik untuk detail perusahaan'><a target='_blank' href='new-usaha?id=".$idusaha."'>".$data."</a></td>";
  } else {
    echo "<td>".$data."</td>";
  }
  
} ?>

<?php 
function ChangeChar($string){
$string=str_replace('<','&lt;',$string);
$string=str_replace('>','&gt;',$string);
$string=str_replace('"','&quot;',$string);
$string=str_replace("'",'&#39;',$string);
$string=str_replace("&",'%26',$string);
return $string;


}
 ?>
