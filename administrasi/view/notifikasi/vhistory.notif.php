<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?= $data->subtitle ?></h3>
	</div>
	
    <!--
	<div class="row">
		<div class="col-sm-12 col-md-12" style="text-align:right"><a class="" id="cari-data" href="#modal-cari" data-toggle="modal">Cari Data?</a></div>
	</div>
    -->

    <div class='row'>
        <div class='col-md-12'>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Notifikasi</th>
                        <th>Deskripsi</th>
                        <th>Link</th>
                        <th>Status</th>
                        <th>Tanggal</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($data->listdata)=='0'): ?>
                        <tr>
                            <td colspan="5" class="tengah">Tidak Ada Data Dalam Database Kami</td>
                        </tr>    
                        <?php else: ?>
                        <?php foreach ($data->listdata as $key): ?>
                            
                            
                                <td><?= $key['ntf_judul'] ?></td>
                                <td><?= $key['ntf_desc'] ?></td>
                                <td><a href="<?= $key['ntf_url_target'] ?>" target="_blank">Link</a></td>
                                <td><?= $key['status'] ?></td>
                                <td><?= $key['tanggal'] ?></td>
                            </tr>

                        <?php endforeach ?>
                    <?php endif ?>
                </tbody>
            </table>
        </div>

    </div>
	    
</div>
</body>
</html> 



