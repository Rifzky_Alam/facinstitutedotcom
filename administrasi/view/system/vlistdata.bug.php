<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?= $data->headertext ?></h3>
	</div>
	<div class="row" style="overflow:auto;">
        <div class="col-md-12">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Error</th>
                        <th>Link</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($data->listdata)=='0'): ?>
                        <tr>
                            <td colspan="4" style="text-align:center;">Tidak Ada data di dalam database kami</td>
                        </tr>
                    <?php else: ?>
                        <?php foreach ($data->listdata as $key): ?>
                            <?php if ($key['rb_status']=='0'): ?>
                            <tr class="danger">
                            <?php elseif ($key['rb_status']=='1'): ?>
                            <tr class="warning">
                            <?php elseif ($key['rb_status']=='2'): ?>
                            <tr class="success">
                            <?php endif ?>
                            
                                <td><?= $key['rb_report'] ?></td>
                                <td><a href="<?= $key['rb_links'] ?>" target="_blank"><?= $key['rb_links'] ?></a></td>
                                <td><?= $key['frb_desc'] ?></td>
                                <td><a href="<?= $data->base_url.'administrasi/bug/editbug/'.$key['rb_id'] ?>" title="Edit">Edit</a> || <a href="#" title="Remove">Remove</a></td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                    
                </tbody>
            </table>  
        </div> 
    </div>

</div><!--end container-->
</body>
</html> 