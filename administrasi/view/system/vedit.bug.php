<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?= $data->headertext ?></h3>
	</div>
	<div class="row">
     <form id="my-form" action="" enctype="multipart/form-data" method="post">

         <div class="row">
             <div class="col-md-10">
                <div class="form-group">
                    <label>Keluhan/Error</label>
                    <textarea name="in[bug]" class="form-control"><?= $data->rowdata['rb_report'] ?></textarea>
                </div>

                <div class="form-group">
                    <label>Link Terjadi Error</label>
                    <input type="text" name="in[links]" class="form-control" value="<?= $data->rowdata['rb_links'] ?>" placeholder="masukkan keluhan system">
                </div>

                <div class="form-group">
                    <label>Status</label>
                    <select name="in[status]" class="form-control">
                        <?php foreach ($data->statusdata as $key): ?>
                            <?php if (@$data->rowdata['rb_status']==$key['frb_flag']): ?>
                                <option value="<?= $key['frb_flag'] ?>" selected="selected"><?= $key['frb_desc'] ?></option>
                            <?php else: ?>
                                <option value="<?= $key['frb_flag'] ?>"><?= $key['frb_desc'] ?></option>
                            <?php endif ?>
                        <?php endforeach ?>
                    </select>
                </div>

                <button class="btn btn-lg btn-success">Submit</button>

             </div>
         </div>         
         <hr>

     </form>   
    </div>

</div><!--end container-->
</body>
</html> 