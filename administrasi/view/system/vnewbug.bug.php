<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?= $data->headertext ?></h3>
	</div>
	<div class="row">
     <form id="my-form" enctype="multipart/form-data" method="post">

         <div class="row">
             <div class="col-md-10">
                <div class="form-group">
                    <label>Keluhan/Error</label>
                    <textarea name="in[bug]" class="form-control" placeholder="Pesan error atau deskripsi error"></textarea>
                    
                </div>

                <div class="form-group">
                    <label>Link Terjadi Error</label>
                    <input type="text" name="in[links]" class="form-control" placeholder="masukkan link munculnya error">
                </div>

                <button class="btn btn-lg btn-success">Submit</button>

             </div>
         </div>         
         <hr>

     </form>   
    </div>

</div><!--end container-->
</body>
</html> 