<?php

function EmailHeader($data){
    return '<table id="m_-5588023840110578919m_3185550004040353724backgroundTable" style="background:#e1e1e1" width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
            <td class="m_-5588023840110578919m_3185550004040353724body" style="background:#e1e1e1" width="100%" valign="top" align="center">
                <table cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td width="640">
                            
                        </td>
                    </tr>
                    <tr>
                        <td class="m_-5588023840110578919m_3185550004040353724main" style="padding:0 10px" width="640" align="center">
                            <table cellspacing="0" cellpadding="0">
    <tbody><tr>
        <td width="640" align="left">
            <table width="100%" cellspacing="0" cellpadding="0">
                <tbody><tr>
                    <td class="m_-5588023840110578919m_3185550004040353724header m_-5588023840110578919m_3185550004040353724header--left" style="padding:20px 10px" align="left">
                        <a href="https://fac-institute.com" target="_blank">
                            <img class="m_-5588023840110578919m_3185550004040353724header__logo CToWUd" src="https://fac-institute.com/images/facnew.png" alt="FAC-Institute" style="display:block;border:0" width="120" height="110"></a>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>
                            <table class="m_-5588023840110578919m_3185550004040353724featured-story m_-5588023840110578919m_3185550004040353724featured-story--top" cellspacing="0" cellpadding="0">
    <tbody><tr>
        <td style="padding-bottom:20px">
            <table cellspacing="0" cellpadding="0">
                <tbody><tr>
                    <td class="m_-5588023840110578919m_3185550004040353724featured-story__inner" style="background:#fff">
                        <table cellspacing="0" cellpadding="0">
                            <tbody><tr>
                                <td class="m_-5588023840110578919m_3185550004040353724featured-story__content-inner" style="padding:32px 30px 45px">
                                    <table cellspacing="0" cellpadding="0">
                                        <tbody><tr>
<td class="m_-5588023840110578919m_3185550004040353724featured-story__heading m_-5588023840110578919m_3185550004040353724featured-story--top__heading" style="background:#fff" width="640" align="left">
                                                <table cellspacing="0" cellpadding="0">
                                                    <tbody><tr>
                                                        <td style="font-family:Geneva,Tahoma,Verdana,sans-serif;font-size:22px;color:#464646" width="600" align="left">
                                                            <a href="https://fac-institute.com/administrasi/absensi" style="text-decoration:none;color:#464646">'.$data->emailtitle.'</a>
                                                        </td>
                                                    </tr>
                                                </tbody></table>
                                            </td>
                                        </tr>';
}

function EmailFooter($data){
    return '<table cellspacing="0" cellpadding="0">
<tbody><tr>
                        <td class="m_-5588023840110578919m_3185550004040353724footer" style="padding-top:10px" width="640" align="center">
                            <table cellspacing="0" cellpadding="0">
                                <tbody><tr>
                                    <td style="font-family:Geneva,Tahoma,Verdana,sans-serif;font-size:14px;line-height:18px;color:#738597;padding:0 20px 30px" align="center">
                                                        
We sent this message to you because you are associated with FAC Institute Website System.<br><br>

The system designed and developed by <a href="mailto:rifzky.mail@gmail.com?subject=feedback">Rifzky Alam</a>

                        </td>
                                </tr>
</tbody></table>
                        
                        </td>
                    </tr>
                    <tr>
                        <td class="m_-5588023840110578919m_3185550004040353724footer" style="padding-top:10px" width="640" align="center">
                            <table cellspacing="0" cellpadding="0">
                                <tbody><tr>
                                    <td style="font-family:Geneva,Tahoma,Verdana,sans-serif;font-size:14px;line-height:18px;color:#738597;padding:0 20px 40px" align="center">
                                        <a href="https://fac-institute.com" target="_blank"><img src="https://fac-institute.com/images/facnew.png" alt="FAC-Institute" style="display:block;border:0" class="CToWUd" width="50" height="50"></a>
                        

                                        <br>FAC INSTITUTE<br><a href="https://maps.google.com/?q=FAC-Institute" target="_blank">'.$data->companyaddr.'</a> 
                                        <br>
                                        Konsultan Akuntansi dan Lembaga Pendidikan Komputerisasi Akuntansi
                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody></table>

    
    <img src="https://ci3.googleusercontent.com/proxy/hKZyLEBTjnS-PycQdIixFD9IvcAGaZ8cU1JqGAVx73I4iKNB9FKc2JNpS2pb4xsS-oadFhmbDC2laUKuBBl28tniF5rJVTfGkXSKMzSitVdrZtka0GUum0LAe3FhnTfb0RwHr2NEN7KZkqgWzQqTX7MuBL3yLlbOIB4yjvY4NU9xJVabZxzrta8p1t_ITNxJ84m_U6CwVulJAnDms1pvNshsqsTgdJO2s8rLbTUpF6IckPl184AsNuw=s0-d-e1-ft#https://click.e.mozilla.org/open.aspx?ffcb10-fe89107470670d787d-fdf716707367057b70127771-fe9915707361037e75-ff6e157075-fe3417757664017b761472-ff2a11767d66&amp;d=40053" class="CToWUd" width="1" height="1">

</td></tr></tbody></table>';
}

// function BodySample($data){
//     return EmailHeader($data).'
//                                         <tr>
//                                             <td class="m_-5588023840110578919m_3185550004040353724featured-story__copy" style="background:#fff" width="640" align="left">
//                                                 <table cellspacing="0" cellpadding="0">
//                                                     <tbody><tr>
//                                                         <td style="font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;font-size:16px;line-height:22px;color:#555555;padding-top:16px" align="left">
//                                                         Dear '.$data->nama_cust.', <br><br>
//                                                         Terimakasih atas partisipasinya untuk mengisi form penawaran yang telah tersedia di website kami, kami akan segera mengirim invoice terkait item/produk yang telah disepakati. <br><br><br>

//                                                         Terimakasih, <br><br>
//                                                         <b>FAC System.</b>
//                                                         </td>
//                                                     </tr>
//                                                 </tbody></table>
//                                             </td>
//                                         </tr>
//                                     </tbody></table>
//                                 </td>
//                             </tr>
//                         </tbody></table>
//                     </td>
//                 </tr>
//             </tbody></table>
//         </td>
//     </tr>
// </tbody></table>'.EmailFooter($data);
// }