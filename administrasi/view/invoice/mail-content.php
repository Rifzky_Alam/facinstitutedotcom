<?php 

function Notif($data){
    return '

    <table id="m_-5588023840110578919m_3185550004040353724backgroundTable" style="background:#e1e1e1" width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
            <td class="m_-5588023840110578919m_3185550004040353724body" style="background:#e1e1e1" width="100%" valign="top" align="center">
                <table cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td width="640">
                            
                        </td>
                    </tr>
                    <tr>
                        <td class="m_-5588023840110578919m_3185550004040353724main" style="padding:0 10px" width="640" align="center">
                            <table cellspacing="0" cellpadding="0">
    <tbody><tr>
        <td width="640" align="left">
            <table width="100%" cellspacing="0" cellpadding="0">
                <tbody><tr>
                    <td class="m_-5588023840110578919m_3185550004040353724header m_-5588023840110578919m_3185550004040353724header--left" style="padding:20px 10px" align="left">
                        <a href="https://fac-institute.com" target="_blank">
                            <img class="m_-5588023840110578919m_3185550004040353724header__logo CToWUd" src="https://fac-institute.com/images/facnew.png" alt="FAC-Institute" style="display:block;border:0" width="120" height="110"></a>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>
                            <table class="m_-5588023840110578919m_3185550004040353724featured-story m_-5588023840110578919m_3185550004040353724featured-story--top" cellspacing="0" cellpadding="0">
    <tbody><tr>
        <td style="padding-bottom:20px">
            <table cellspacing="0" cellpadding="0">
                <tbody><tr>
                    <td class="m_-5588023840110578919m_3185550004040353724featured-story__inner" style="background:#fff">
                        <table cellspacing="0" cellpadding="0">
                            <tbody><tr>
                                <td class="m_-5588023840110578919m_3185550004040353724featured-story__content-inner" style="padding:32px 30px 45px">
                                    <table cellspacing="0" cellpadding="0">
                                        <tbody><tr>
<td class="m_-5588023840110578919m_3185550004040353724featured-story__heading m_-5588023840110578919m_3185550004040353724featured-story--top__heading" style="background:#fff" width="640" align="left">
                                                <table cellspacing="0" cellpadding="0">
                                                    <tbody><tr>
                                                        <td style="font-family:Geneva,Tahoma,Verdana,sans-serif;font-size:22px;color:#464646" width="600" align="left">
                                                            <a href="https://fac-institute.com/administrasi/absensi" style="text-decoration:none;color:#464646">Notification</a>
                                                        </td>
                                                    </tr>
                                                </tbody></table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="m_-5588023840110578919m_3185550004040353724featured-story__copy" style="background:#fff" width="640" align="left">
                                                <table cellspacing="0" cellpadding="0">
                                                    <tbody><tr>
                                                        <td style="font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;font-size:16px;line-height:22px;color:#555555;padding-top:16px" align="left">
                                                        System detected that '.$data->petugas.' has successfully created an invoice for '.$data->nama_usaha.', you can check it by <a href="https://fac-institute.com/administrasi/" target="_blank">visiting this page.</a>
                                                        </td>
                                                    </tr>
                                                </tbody></table>
                                            </td>
                                        </tr>
                                    </tbody></table>
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>
                            <table cellspacing="0" cellpadding="0">
<tbody><tr>
                        <td class="m_-5588023840110578919m_3185550004040353724footer" style="padding-top:10px" width="640" align="center">
                            <table cellspacing="0" cellpadding="0">
                                <tbody><tr>
                                    <td style="font-family:Geneva,Tahoma,Verdana,sans-serif;font-size:14px;line-height:18px;color:#738597;padding:0 20px 30px" align="center">
                                                        
We sent this message to you because you are associated with FAC Institute Website System.<br><br>

The system designed and developed by <a href="mailto:rifzky.mail@gmail.com?subject=feedback">Rifzky Alam</a>

                        </td>
                                </tr>
</tbody></table>
                        
                        </td>
                    </tr>
                    <tr>
                        <td class="m_-5588023840110578919m_3185550004040353724footer" style="padding-top:10px" width="640" align="center">
                            <table cellspacing="0" cellpadding="0">
                                <tbody><tr>
                                    <td style="font-family:Geneva,Tahoma,Verdana,sans-serif;font-size:14px;line-height:18px;color:#738597;padding:0 20px 40px" align="center">
                                        <a href="https://fac-institute.com" target="_blank"><img src="https://fac-institute.com/images/facnew.png" alt="FAC-Institute" style="display:block;border:0" class="CToWUd" width="50" height="50"></a>
                        

                                        <br>FAC INSTITUTE<br><a href="https://maps.google.com/?q=FAC-Institute" target="_blank">Jl. Pahlawan Revolusi 10GG No.3, RT.9/RW.7, Pd. Bambu, Kec. Duren Sawit, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta Indonesia 13430</a> 
                                        <br>
                                        Konsultan Akuntansi dan Lembaga Pendidikan Komputerisasi Akuntansi
                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody></table>

    
    <img src="https://ci3.googleusercontent.com/proxy/hKZyLEBTjnS-PycQdIixFD9IvcAGaZ8cU1JqGAVx73I4iKNB9FKc2JNpS2pb4xsS-oadFhmbDC2laUKuBBl28tniF5rJVTfGkXSKMzSitVdrZtka0GUum0LAe3FhnTfb0RwHr2NEN7KZkqgWzQqTX7MuBL3yLlbOIB4yjvY4NU9xJVabZxzrta8p1t_ITNxJ84m_U6CwVulJAnDms1pvNshsqsTgdJO2s8rLbTUpF6IckPl184AsNuw=s0-d-e1-ft#https://click.e.mozilla.org/open.aspx?ffcb10-fe89107470670d787d-fdf716707367057b70127771-fe9915707361037e75-ff6e157075-fe3417757664017b761472-ff2a11767d66&amp;d=40053" class="CToWUd" width="1" height="1">

</td></tr></tbody></table>
    ';
}


function Invoice($data){


		return '<table style="margin:0;padding:0" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr style="background-color: #0066ff;">
                            <td style="color:white;margin:0 auto;font-size:22px" align="center" valign="middle" height="60"><strong>FAC-Institute</strong></td>
                        </tr>
                    </tbody>
                </table>            
            </td>
        </tr>
        <tr>
            <table  align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="padding-top:10px;">
            <tbody>
            <tr>
            <td style="width:50%"><center><img src="http://fac-institute.com/images/logoaccurate5.png" style="width:50%;height:60px"></center></td>
            <td style="width:50%"><center><img src="http://fac-institute.com/images/facnew.png" style="width:35%;height:70px"></center></td>
            </tr>
            </tbody>
            </table>
        </tr>
        <tr>
            <td valign="middle" height="60"><h4>Dear '.$data->nama_cust.',</h4></td>
        </tr>
        <tr>
            <td>
                <p>
                    Kami dari FAC Institute sebagai divisi onsite training Accurate. Terimakasih telah memilih Accurate sebagai program akuntansi di <strong>'.$data->nama_usaha.'</strong>.
                     Terimakasih pula atas order menggunakan jasa FAC Institute dalam mengimplementasikan Accurate.<br> 
                    Berikut ini kami kirimkan jadwal dan invoice training Accurate : 
                </p>
                <br>
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr>
                            <td style="width:50%;">
                            <label><strong>WAKTU & TEMPAT PELAKSANAAN</strong></label>
                            <table style="padding-top:10px">
                            <tbody>
                            <tr>
                                <td style="width:150px;vertical-align:top;">Tanggal</td>
                                <td style="vertical-align:top;">:</td>
                                <td>
                                    <ul type="none" style="padding-left:10px;margin-top:0px;">
                                        '.getDateOfTraining($data->tanggal).'
                                    </ul>
                                </td>
                            </tr>
                            <tr><td>Waktu</td><td>:</td><td>'.$data->waktu.'</td></tr>
                            <tr><td>Lokasi Training</td><td>:</td><td>'.$data->tempat.'</td></tr>
                            </tbody>
                            </table>
                            </td>
                            <td style="text-align:center;vertical-align:top;">
                            <label><strong>AGENDA KEGIATAN</strong></label>
                            <table style="padding-top:10px">
                            <tbody>
                            <tr>
                                <td style="text-align: left;">
                                    <ol>
                                        '.getAgenda($data->agenda).'
                                    </ol>
                                </td>
                            </tr>
                            
                            </tbody>
                            </table>
                            </td>
                        </tr>
                        
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td><br>
            <table valign="top" align="center" border="0" style="padding-bottom:30;" bgcolor="#f9ffee" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                    <tr>
                        <td>
                            <table valign="top" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tbody>
                                    <tr><td><center><h3><strong>BIAYA</strong></h3></center></td></tr>
                                    <tr>
                                    <td style="margin-bottom:10px">
                                    <table border="1" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Keterangan</th>
                                        <th>Biaya</th>
                                        <th>Qty</th>
                                        <th>Jumlah</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    '.items($data->items).'
                                    </tbody>
                                    </table>
                                    </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>

            </table>
                
            </td>
        </tr>
        <tr>
            <td>
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                '.Notes($data->notes).'                
                <tr>
                <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr style="background-color: #660568;">
                            <td style="color:white;margin:0 auto" align="center" valign="middle" height="40">PEMBAYARAN</td>
                        </tr>
                    </tbody>
                </table>
                <p>Harga diatas adalah tarif nett yang diterima oleh kami (tidak dipotong pph)<br>
				Kami bukan PKP(Pengusaha Kena Pajak), tidak ada faktur pajak. Juga tidak ada NPWP untuk bukti potong PPh 23.
				</p>
                '.syaratBayar($data->metode_bayar).'
                <ul type="square">
                    <li><strong>Bank BCA KCU Bekasi No Acc. 0663162851 a/n : Fajar Shodiq</strong></li>
                    <li><strong>Bank Mandiri KCP Bekasi Ahmad Yani No. Acc. 1670001377901 a/n : Fajar Shodiq</strong></li>
                </ul>
                </td>
                </tr>
                </tbody>
                </table> 
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr style="background-color: #660000;">
                            <td style="color:white;margin:0 auto" align="center" valign="middle" height="40">SYARAT DAN KETENTUAN</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                '.syaratDanKetentuan('').'
            </td>
        </tr>
        <tr>
            <td>
                <p>Demikianlah semoga menjadi maklum adanya. Terimakasih atas perhatian dan kerjasamanya.</p>
                <p>
                Jika ada hal-hal yang ingin ditanyakan, jangan sungkan untuk senantiasa menghubungi kami. Terimakasih, <br><br>Salam <br><br>
                </p>
                <p><strong>'.$data->petugas.'</strong> <br> <strong>Officer of Marketing Division</strong></p>
            </td>
        </tr>
        <tr>
            <td style="padding:0 0 0 0" align="center" bgcolor="#0066ff">
          <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:#0066ff">
            <tbody>
              <tr>
                <td>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr height="30">
                        <td align="center" width="100%" height="16"></td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr>
                        <td align="center" valign="top">
                          <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                              <tr>
                                <td align="left" width="20"></td>
                                <td align="left" valign="top">
                                  <table align="center" border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                      <tr height="15">
                                        <td align="left" height="15">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td style="line-height:18px" align="center" valign="top">
                                          <span style="font-family:'."'"."Open Sans"."'".',sans-serif;color:white;font-size:12px;text-align:center">Email ini dikirim oleh: <strong><a href="http://www.fac-institute.com" style="color:white">FAC-Institute</a></strong><br>
                                          Jl. Pahlawan Revolusi 10GG No.3, RT.9/RW.7, Pd. Bambu, Kec. Duren Sawit, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta 13430<br>
                                          Telp. <a href="tel://+6281290083983" style="color:white">+6281290083983</a></span><br>
                                          <br>
                                            <a href="http://www.fac-institute.com" style="font-family:'."'"."Open Sans"."'".',sans-serif;font-size:22px;color:white">www.fac-institute.com</a><br><br>
                                            <p style="margin:0 0 1em 0;font-family:'."'"."Open Sans"."'".',sans-serif;font-size:16px;line-height:26px;color:white;font-weight:300;text-align:center">Konsultan Akuntansi dan Lembaga Pendidikan Komputerisasi Akuntansi</p>
                                        </td>
                                      </tr>
                                      <tr height="15">
                                        <td align="left" height="15">&nbsp;</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                                <td align="left" width="20"></td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr height="30">
                        <td align="left" width="100%" height="16"></td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
        </tr>
    </tbody>
</table>';
	}

function InvoiceAccountingService($data){
		return '<table style="margin:0;padding:0" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr style="background-color: #0066ff;">
                            <td style="color:white;margin:0 auto;font-size:22px" align="center" valign="middle" height="60"><strong>FAC-Institute</strong></td>
                        </tr>
                    </tbody>
                </table>            
            </td>
        </tr>
        <tr>
            <table  align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tbody>
            <tr>
            <td style="width:50%"><center><img src="http://fac-institute.com/images/logoaccurate5.png" style="width:50%;height:60px"></center></td>
            <td style="width:50%"><center><img src="http://fac-institute.com/images/logo-fac.jpg" style="width:50%;height:100px"></center></td>
            </tr>
            </tbody>
            </table>
        </tr>
        <tr>
            <td valign="middle" height="60"><h4>Dear '.$data->nama_cust.',</h4></td>
        </tr>
        <tr>
            <td>
                <p>
                    Kami dari FAC Institute sebagai divisi Accounting Service. Terimakasih telah memilih FAC Institute sebagai penyedia jasa accounting service di <strong>'.$data->nama_usaha.'</strong>. Kami selalu berusaha untuk memberikan pelayanan terbaik di setiap jasa yang kami berikan.<br> 
                    Berikut ini kami kirimkan jadwal dan invoice Accounting Service : 
                </p>
                <br>
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr>
                            <td style="width:50%;">
                            <label><strong>WAKTU & TEMPAT PELAKSANAAN</strong></label>
                            <table style="padding-top:10px">
                            <tbody>
                            <tr>
                                <td style="width:150px;vertical-align:top;">Tanggal</td>
                                <td style="vertical-align:top;">:</td>
                                <td>
                                    <ul type="none" style="padding-left:10px;margin-top:0px;">
                                        '.getDateOfTraining($data->tanggal).'
                                    </ul>
                                </td>
                            </tr>
                            <tr><td>Waktu</td><td>:</td><td>'.$data->waktu.'</td></tr>
                            <tr><td>Tempat</td><td>:</td><td>'.$data->tempat.'</td></tr>
                            <tr><td>Alamat</td><td>:</td><td>'.$data->alamat.'</td></tr>
                            </tbody>
                            </table>
                            </td>
                            <td style="text-align:center;vertical-align:top;">
                            <label><strong>AGENDA KEGIATAN</strong></label>
                            <table style="padding-top:10px">
                            <tbody>
                            <tr>
                                <td style="text-align: left;">
                                    <ol>
                                        '.getAgenda($data->agenda).'
                                    </ol>
                                </td>
                            </tr>
                            
                            </tbody>
                            </table>
                            </td>
                        </tr>
                        
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td><br>
            <table valign="top" align="center" border="0" style="padding-bottom:30;" bgcolor="#f9ffee" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                    <tr>
                        <td>
                            <table valign="top" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tbody>
                                    <tr><td><center><h3><strong>BIAYA</strong></h3></center></td></tr>
                                    <tr>
                                    <td style="margin-bottom:10px">
                                    <table border="1" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Keterangan</th>
                                        <th>Biaya</th>
                                        <th>Qty</th>
                                        <th>Jumlah</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    '.items($data->items).'
                                    </tbody>
                                    </table>
                                    </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>

            </table>
                
            </td>
        </tr>
        <tr>
            <td>
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                <tr><td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr style="background-color: #660568;">
                            <td style="color:white;margin:0 auto" align="center" valign="middle" height="40">PEMBAYARAN</td>
                        </tr>
                    </tbody>
                </table>
                <p>Harga diatas adalah tarif nett yang diterima oleh kami (tidak dipotong pph)<br>
				Kami bukan PKP(Pengusaha Kena Pajak), tidak ada faktur pajak. Juga tidak ada NPWP untuk bukti potong PPh 23.
				</p>
                '.syaratBayar($data->metode_bayar).'
                <ul type="square">
                    <li><strong>Bank BCA KCU Bekasi No Acc. 0663162851 a/n : Fajar Shodiq</strong></li>
                    <li><strong>Bank Mandiri KCP Bekasi Ahmad Yani No. Acc. 1670001377901 a/n : Fajar Shodiq</strong></li>
                </ul></td>
                </tr>
                </tbody>
                </table> 
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr style="background-color: #660000;">
                            <td style="color:white;margin:0 auto" align="center" valign="middle" height="40">SYARAT DAN KETENTUAN</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                '.syaratDanKetentuan($data->syarat).'
            </td>
        </tr>
        <tr>
            <td>
                <p>Demikianlah semoga menjadi maklum adanya. Terimakasih atas perhatian dan kerjasamanya.</p>
                <p>
                Jika ada hal-hal yang ingin ditanyakan, jangan sungkan untuk senantiasa menghubungi kami. Terimakasih, <br><br>Salam <br><br>
                </p>
                <p><strong>'.$data->petugas.'</strong> <br> <strong>Officer of Marketing Division</strong></p>
            </td>
        </tr>
        <tr>
            <td style="padding:0 0 0 0" align="center" bgcolor="#0066ff">
          <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:#0066ff">
            <tbody>
              <tr>
                <td>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr height="30">
                        <td align="center" width="100%" height="16"></td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr>
                        <td align="center" valign="top">
                          <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                              <tr>
                                <td align="left" width="20"></td>
                                <td align="left" valign="top">
                                  <table align="center" border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                      <tr height="15">
                                        <td align="left" height="15">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td style="line-height:18px" align="center" valign="top">
                                          <span style="font-family:'."'"."Open Sans"."'".',sans-serif;color:white;font-size:12px;text-align:center">Email ini dikirim oleh: <strong><a href="http://www.fac-institute.com" style="color:white">FAC-Institute</a></strong><br>
                                          Jl. Pahlawan Revolusi 10GG No.3, RT.9/RW.7, Pd. Bambu, Kec. Duren Sawit, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta 13430<br>
                                          Telp. <a href="tel://+6281290083983" style="color:white">+6281290083983</a></span><br>
                                          <br>
                                            <a href="http://www.fac-institute.com" style="font-family:'."'"."Open Sans"."'".',sans-serif;font-size:22px;color:white">www.fac-institute.com</a><br><br>
                                            <p style="margin:0 0 1em 0;font-family:'."'"."Open Sans"."'".',sans-serif;font-size:16px;line-height:26px;color:white;font-weight:300;text-align:center">Konsultan Akuntansi dan Lembaga Pendidikan Komputerisasi Akuntansi</p>
                                        </td>
                                      </tr>
                                      <tr height="15">
                                        <td align="left" height="15">&nbsp;</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                                <td align="left" width="20"></td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr height="30">
                        <td align="left" width="100%" height="16"></td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
        </tr>
    </tbody>
</table>';
	}


function Jadwal($data){
        
        return '<table style="margin:0;padding:0" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr style="background-color: #0066ff;">
                            <td style="color:white;margin:0 auto;font-size:22px" align="center" valign="middle" height="60"><strong>FAC-Institute</strong></td>
                        </tr>
                    </tbody>
                </table>            
            </td>
        </tr>
        <tr>
            <table  align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tbody>
            <tr>
            <td style="width:50%"><center><img src="http://fac-institute.com/images/logoaccurate5.png" style="width:50%;height:60px"></center></td>
            <td style="width:50%"><center><img src="http://fac-institute.com/images/logo-fac.jpg" style="width:50%;height:100px"></center></td>
            </tr>
            </tbody>
            </table>
        </tr>
        <tr>
            <td valign="middle" height="60"><h4>Dear '.$data->nama_cust.',</h4></td>
        </tr>
        <tr>
            <td>
                <p>
                    Kami dari FAC Institute sebagai divisi onsite training Accurate.<br> Terimakasih telah memilih Accurate sebagai program akuntansi di <strong>'.$data->nama_usaha.'</strong>.
                     Terimakasih pula atas order menggunakan jasa FAC Institute dalam mengimplementasikan Accurate.<br> 
                    Berikut ini kami kirimkan jadwal training Accurate : 
                </p>
                <br>
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr>
                            <td style="width:50%">
                            <label><strong>WAKTU & TEMPAT PELAKSANAAN</strong></label>
                            <table style="padding-top:10px">
                            <tbody>
                            <tr>
                                <td style="width:150px;vertical-align:top;">Tanggal</td>
                                <td style="vertical-align:top;">:</td>
                                <td>
                                    <ul type="none" style="padding-left:10px;margin-top:0px;">
                                        '.getDateOfTraining($data->tanggal).'
                                    </ul>
                                </td>
                            </tr>
                            <tr><td>Waktu</td><td>:</td><td>'.$data->waktu.'</td></tr>
                            <tr><td>Tempat</td><td>:</td><td>'.$data->tempat.'</td></tr>
                            <tr><td>Alamat</td><td>:</td><td>'.$data->alamat.'</td></tr>
                            </tbody>
                            </table>
                            </td>
                            <td style="text-align:center;vertical-align:top;">
                            <label><strong>AGENDA KEGIATAN</strong></label>
                            <table style="padding-top:10px">
                            <tbody>
                            <tr>
                                <td style="text-align: left;">
                                    <ol>
                                        '.getAgenda($data->agenda).'
                                    </ol>
                                </td>
                            </tr>
                            
                            </tbody>
                            </table>
                            </td>
                        </tr>
                        
                    </tbody>
                </table>
            </td>
        </tr>
        <br>
       
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr style="background-color: #660000;">
                            <td style="color:white;margin:0 auto" align="center" valign="middle" height="40">SYARAT DAN KETENTUAN</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                '.syaratDanKetentuan('').'
            </td>
        </tr>
        <tr>
            <td>
                <p>Demikianlah semoga menjadi maklum adanya. Terimakasih atas perhatian dan kerjasamanya.</p>
                <p>
                Jika ada hal-hal yang ingin ditanyakan, jangan sungkan untuk senantiasa menghubungi kami. <br>Terimakasih, <br><br>Salam <br><br>
                </p>
                <p><strong>'.$data->petugas.'</strong> <br> <strong>Officer of Marketing Division</strong></p>
            </td>
        </tr>
        <tr>
            <td style="padding:0 0 0 0" align="center" bgcolor="#0066ff">
          <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:#0066ff">
            <tbody>
              <tr>
                <td>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr height="30">
                        <td align="center" width="100%" height="16"></td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr>
                        <td align="center" valign="top">
                          <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                              <tr>
                                <td align="left" width="20"></td>
                                <td align="left" valign="top">
                                  <table align="center" border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                      <tr height="15">
                                        <td align="left" height="15">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td style="line-height:18px" align="center" valign="top">
                                          <span style="font-family:'."'"."Open Sans"."'".',sans-serif;color:white;font-size:12px;text-align:center">Email ini dikirim oleh: <strong><a href="http://www.fac-institute.com" style="color:white">FAC-Institute</a></strong><br>
                                          Jl. Pahlawan Revolusi 10GG No.3, RT.9/RW.7, Pd. Bambu, Kec. Duren Sawit, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta Indonesia 13430<br>
                                          Telp. <a href="tel://+6281290083983" style="color:white">+6281290083983</a></span><br>
                                          <br>
                                            <a href="http://www.fac-institute.com" style="font-family:'."'"."Open Sans"."'".',sans-serif;font-size:22px;color:white">www.fac-institute.com</a><br><br>
                                            <p style="margin:0 0 1em 0;font-family:'."'"."Open Sans"."'".',sans-serif;font-size:16px;line-height:26px;color:white;font-weight:300;text-align:center">Konsultan Akuntansi dan Lembaga Pendidikan Komputerisasi Akuntansi</p>
                                        </td>
                                      </tr>
                                      <tr height="15">
                                        <td align="left" height="15">&nbsp;</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                                <td align="left" width="20"></td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr height="30">
                        <td align="left" width="100%" height="16"></td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
        </tr>
    </tbody>
</table>';
    }

    function Kursus($data){

        return '<table style="margin:0;padding:0" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr style="background-color: #0066ff;">
                            <td style="color:white;margin:0 auto;font-size:22px" align="center" valign="middle" height="60"><strong>FAC-Institute</strong></td>
                        </tr>
                    </tbody>
                </table>            
            </td>
        </tr>
        <tr>
            <table  align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tbody>
            <tr>
            <td style="width:50%"><center><img src="http://fac-institute.com/images/logoaccurate5.png" style="width:50%;height:60px"></center></td>
            <td style="width:50%"><center><img src="http://fac-institute.com/images/logo-fac.jpg" style="width:50%;height:100px"></center></td>
            </tr>
            </tbody>
            </table>
        </tr>
        <tr>
            <td valign="middle" height="60"><h4>Kepada Yth,</h4></td>
        </tr>
        <tr>
            <td valign="middle" height="60"><h4>'.$data->nama_cust.'</h4></td>
        </tr>
        <tr>
            <td>
                <p>
                    Degan Hormat, <br>
                    Terimakasih, Anda telah mendaftar di kelas kursus <strong>FAC Institute & Bakat Cendekia</strong>.
                     Kami merupakan partner dari pihak Accurate Accounting Software, sebagai Accurate Authorized Training Center (AATC) yang secara resmi menyelenggarakan Pelatihan Accurate.<br> 
                </p>
                <br>
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr>
                            <td>
                            <label><strong>WAKTU & TEMPAT PELAKSANAAN</strong></label>
                            <table style="padding-top:10px">
                            <tbody>
                            <tr><td>Tanggal</td><td>:</td>
                            	<td>
                            		<ul>
                            			'.getDateOfTraining($data->tanggal).'
                            		</ul>
                            	</td>
                            </tr>
                            <tr><td>Waktu</td><td>:</td><td>09:00 - 17:00</td></tr>
                            <tr><td>Tempat</td><td>:</td><td>Kantor Fac-Institute</td></tr>                           
                            </tbody>
                            </table>
                            </td>
                        </tr>
                        
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td><br>
            <table valign="top" align="center" border="0" style="padding-bottom:30;" bgcolor="#f9ffee" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                    <td>
                        <table valign="top" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                                <tr>
                                <td style="margin-bottom:10px">
                                <table valign="top" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                	<tbody>
                                    	<tr><td><center><h3><strong>BIAYA</strong></h3></center></td></tr>
                                    	<tr>
                                    		<td style="margin-bottom:10px">
                                    	<table border="1" width="100%">
                                    		<thead>
                                    			<tr>
                                        			<th>Keterangan</th>
                                        			<th>Biaya</th>
                                        			<th>Qty</th>
                                        			<th>Jumlah</th>
                                    			</tr>
                                    		</thead>
                                    <tbody>
                                    '.items($data->items).'
                            </tbody>
                        </table>
                    </td>
                                    
            	</tbody>
            </table>

                                </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tbody>

            </table>
                
            </td>
        </tr>
        <tr><br></tr>
        <tr>
            <td>
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                '.Notes($data->notes).'
                <tr><td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr style="background-color: #660568;">
                            <td style="color:white;margin:0 auto" align="center" valign="middle" height="40">PEMBAYARAN</td>
                        </tr>
                    </tbody>
                </table>
                <p>Harga diatas adalah tarif nett yang diterima oleh kami (tidak dipotong pph)<br>
				Kami bukan PKP(Pengusaha Kena Pajak), tidak ada faktur pajak. Juga tidak ada NPWP untuk bukti potong PPh 23.
				</p>
                <ul type="square">
                    <li><strong>Bank BCA KCU Bekasi No Acc. 0663162851 a/n : Fajar Shodiq</strong></li>
                    <li><strong>Bank Mandiri KCP Bekasi Ahmad Yani No. Acc. 1670001377901 a/n : Fajar Shodiq</strong></li>
                </ul></td>
                </tr>
                </tbody>
                </table> 
            </td>
        </tr>
        
        <tr>
            <td>
                <p>
                Jika ada hal-hal yang ingin ditanyakan, jangan sungkan untuk senantiasa menghubungi kami. <br>Terimakasih, <br><br>Salam <br><br>
                </p>
                <p><strong>'.$data->petugas.'</strong> <br> <strong>Marketing Division Staff of FAC Institute</strong></p>
            </td>
        </tr>
        <tr>
            <td style="padding:0 0 0 0" align="center" bgcolor="#0066ff">
          <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:#0066ff">
            <tbody>
              <tr>
                <td>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr height="30">
                        <td align="center" width="100%" height="16"></td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr>
                        <td align="center" valign="top">
                          <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                              <tr>
                                <td align="left" width="20"></td>
                                <td align="left" valign="top">
                                  <table align="center" border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                      <tr height="15">
                                        <td align="left" height="15">&nbsp;</td>
                                      </tr>
                                      <tr>
                                      <td style="line-height:18px" align="center" valign="top">
                                          <span style="font-family:'."'"."Open Sans"."'".',sans-serif;color:white;font-size:12px;text-align:center">Email ini dikirim oleh: <strong><a href="http://www.fac-institute.com" style="color:white">FAC-Institute</a></strong><br>
                                          Jl. Pahlawan Revolusi 10GG No.3, RT.9/RW.7, Pd. Bambu, Kec. Duren Sawit, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta  13430<br>
                                          Telp. <a href="tel://+6281290083983" style="color:white">+6281290083983</a></span><br>
                                          <br>
                                            <a href="http://www.fac-institute.com" style="font-family:'."'"."Open Sans"."'".',sans-serif;font-size:22px;color:white">www.fac-institute.com</a><br><br>
                                            <p style="margin:0 0 1em 0;font-family:'."'"."Open Sans"."'".',sans-serif;font-size:16px;line-height:26px;color:white;font-weight:300;text-align:center">Konsultan Akuntansi dan Lembaga Pendidikan Komputerisasi Akuntansi</p>
                                        </td>
                                      </tr>
                                      <tr height="15">
                                        <td align="left" height="15">&nbsp;</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                                <td align="left" width="20"></td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr height="30">
                        <td align="left" width="100%" height="16"></td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
        </tr>
    </tbody>
</table>';
    }

    function getDateOfTraining($value){
        $hasil = "";
        if (count($value)==0||$value==''||empty($value)||($value[0]==''&&count($value)<2)) {
        	$hasil = "(Di sesuaikan dengan kesepakatan bersama)";
        } else {
            
        	for ($i=0; $i < count($value) ; $i++) { 
            	if ($value[$i]!='') {
                	$tgl = explode('-', $value[$i]);
                	$hasil .= "<li>".$tgl[2].getNamaBulan($tgl[1]).$tgl[0]."</li>";  
                    // $hasil .= "<li>".$value[$i]."</li>";              
            	}
        	}
            // $hasil = $value;
            // echo count($value);
            // print_r($value);
        }
        return $hasil;
    }

    function getAgenda($data){
        
        $hasil = "";
        if (count($data)=='0'||$data=='') {
        	$hasil = "-";
        } else {
        	for ($i=0; $i < count($data); $i++){ 
				$hasil .= "<li>".$data[$i]."</li>";
        	}	
        }
        
        
        
        return $hasil;
    }

    function Notes($value){
    	if (count($value)=='0'||$value==''||$value=='-'||($value[0]==''&&count($value)<2)) {
    		return '';
    	} else {
    		$hasil = "<br><tr><td><h4 style='margin-bottom:0px;'>Catatan:</h4><ul>";
    		foreach ($value as $key) {
    			$hasil .= "<li>".$key."</li>";
    		}

    		$hasil .= "</ul></td></tr>";
    		return $hasil;
    	}
    	
    }

    function syaratDanKetentuan($value){
    	if (count($value)=='0'||$value==''||$value=='-'||($value[0]==''&&count($value)<2)) {
    		return '
                <ol>
                    <li>Training dan implementasi bersifat time oriented, bukan result oriented.</li>
                    <li>Training dan implementasi berfokus pada kegiatan mentransfer <strong>How To UseACCURATE</strong>.</li>
                    <li>Training dan implementasi terdiri dari 1 implementator dengan waktu kerja maksimal 7 jam (termasuk 1 jam istirahat), yaitu jam 09.00 s/d 16.00 atau jam 10.00 s/d 17.00.</li>
                    <li>Jika lembur, mohon di print dan diisi form lembur terlebih dahulu yang telah kami lampirkan.</li>
                    <li>Menyediakan makan siang bagi implementator (Jabodetabek)</li>
                    <li>Untuk implementasi di luar Jabodetabek, customer mempersiapkan akomodasi perjalanan (tiba dan kepulangan), makan dan penginapan selama waktu implementasi.</li>
                    <li>Kelebihan jam training akan dikenakan tarif overtime Rp 75.000.- per jam</li>
                    <li>Pembatalan atau perubahan jadwal implementasi oleh Customer harus disampaikan ke FAC paling lambat 2 hari (terhitung hari kerja senin-jumat diluar hari libur nasional) sebelum waktu pelaksanaan implementasi, apabila dilakukan 1 (satu) hari sebelumnya atau di hari pelaksanaan maka dikenakan denda Rp 250.000/hari. Selanjutnya waktu pelaksanaan akan direschedule kembali.</li>
                    <li>Jika anda pengguna awal atau pengguna lama tapi ingin membuat database baru, pada email ini kami lampirkan format excel untuk saldo awal meliputi daftar akun, daftar pelanggan, daftar pemasok, daftar barang dan daftar fix asset. Harap disiapkan sebelum hari training.</li>
                    <li>Training di hari sabtu, minggu dan hari libur nasional dikenakan biaya Rp 250.000.-</li>
                    <li>Untuk implementasi di Jabodetabek akomodasi perjalanan ditagihkan sesuai jarak dan area implementasi.</li>
                </ol>
                ';
    	} else {
    		$hasil = '<ol>';
    		foreach ($value as $key) {
    			$hasil .= '<li>'.$key.'</li>';
    		}
    		$hasil .= '</ol>';
    		return $hasil;
    	}
    }

    function getNamaBulan($bulan){
        $bulan=intval($bulan);
        if ($bulan==1) {
            return " Januari ";
        }elseif ($bulan==2) {
            return " Februari ";
        }elseif ($bulan==3) {
            return " Maret ";
        }elseif ($bulan==4) {
            return " April ";
        }elseif ($bulan==5) {
            return " Mei ";
        }elseif ($bulan==6) {
            return " Juni ";
        }elseif ($bulan==7) {
            return " Juli ";
        }elseif ($bulan==8) {
            return " Agustus ";
        }elseif ($bulan==9) {
            return " September ";
        }elseif ($bulan==10) {
            return " Oktober ";
        }elseif ($bulan==11) {
            return " November ";
        }elseif ($bulan==12) {
            return " Desember ";
        }else{
            return $bulan;
        }

    }

    function items($data){
    	$hasil = "";
    	$totalamount = 0;
    	if (count($data)=='0') {
    		$hasil .= '<tr>
							<td> - </td>
	                        <td style="text-align:right"> 0 IDR</td>
	                        <td style="text-align:center"> 0 </td>
	                        <td style="text-align:right"> 0 IDR</td>
	                       </tr>';
    	} else {
    		foreach ($data as $key ) {
	    		$totalprice = intval($key->harga)*intval($key->itm_qty);
	    		$totalamount += $totalprice;
	    		$hasil .= '<tr>
							<td>'.$key->nama_item.'</td>
	                        <td style="text-align:right">'.number_format($key->harga).' IDR</td>
	                        <td style="text-align:center">'.$key->itm_qty.'</td>
	                        <td style="text-align:right">'.number_format($totalprice).' IDR</td>
	                       </tr>';
	    	}

	    	$hasil .= '<tr>
						<td colspan="3"><strong>Total</strong></td>
						<td style="text-align:right"><strong> '.number_format($totalamount).' IDR</strong></td>
						</tr>';
    	}
    	
	    	
		return $hasil;
    }


    function syaratBayar($value){

        if ($value=='1') {
            return'
            <p>Pembayaran dimuka dilakukan sebelum training dilaksanakan dan pembayaran di bayarkan ke No Rekening di bawah ini.</p>
            ';
        }elseif($value=='2'){
            return'
            <p>Pembayaran dimuka 50% yaitu sebelum implementasi dilaksanakan dan pembayaran di bayarkan ke No Rekening di bawah ini.</p>
            ';
        }elseif($value=='3') {
        	return '
        	<p>Pembayaran dimuka 50% yaitu sebelum implementasi dilaksanakan dan pembayaran di bayarkan ke No Rekening di bawah ini.</p>
        	';
        }else {
            return'
            <p>Pembayaran dimuka dilakukan sebelum training dilaksanakan dan pembayaran di bayarkan ke No Rekening di bawah ini.</p>
            ';
        }
    }

?>