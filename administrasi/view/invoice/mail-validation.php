<!DOCTYPE html>
<html>


<?php include_once 'view/main-component/header.php'; ?>

<body>



<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3>Validasi Invoice</h3>
	</div>


    <div class="row">
        <div class="col-md-10">
            <div class="jumbotron">
                <h3>Harap Cek Kembali</h3>
                <p>
                    File Invoice siap dikirimkan harap mengecek data email yang akan dikirim di bawah ini dengan teliti.<br>
                    bila <a target="_blank" href="<?= $data->base_url.'administrasi/invoices/Invoice '.$data->idtransaksi.'.pdf' ?>" title="klik untuk melihat file pdf">file pdf</a> terdapat kesalahan harap di hapus dengan klik hapus pdf di bawah ini dan kembali edit surat untuk membuatnya kembali.
                </p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>No Invoice</th>
                        <th>Dikirim Ke</th>
                        <th>Carbon Copy</th>
                        <th>Attachments</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?php echo $data->no_invoice ?></td>
                        <td><?php echo $data->email ?></td>
                        <td>
                            <?php echo $data->carboncopy ?>
                        </td>
                        <td>
                            <?php echo $data->attachments ?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <form action="" method="post">
        <div class="row">
            <div class="col-md-10">
                <a href="new-invoice?send=<?php echo $data->no_invoice ?>" class="btn btn-lg btn-primary">Kirim >></a>
                <a href="new-attachments?inve=<?php echo $data->no_invoice ?>" class="btn btn-lg btn-info">Edit Data Surat</a>
                <input type="text" style="display:none;" name="delinv" value="<?= 'Invoice '.$data->idtransaksi.'.pdf' ?>">
                <button class="btn btn-lg btn-danger">Hapus PDF</button>
            </div>
        </div>
    </form>
	<div class="row">
        <div class="col-md-10">
            
        </div>
    </div>

</div><!--end container-->
</body>
</html>
<?php function CC($data){ ?>
    <?php if (count($data)=='0'): ?>
        <?php else: ?>
            <ul>
                <?php foreach ($data as $key ) { ?>
                    <li><?php echo $key ?></li>
                <? } ?>
            </ul>    
    <?php endif ?>
    
<? } ?>

