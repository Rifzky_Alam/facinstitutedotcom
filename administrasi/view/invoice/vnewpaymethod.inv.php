<!DOCTYPE html>
<html>
<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>

<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->subtitle ?></h3>
	</div>

  <div class="row">
    <div class="col-md-10">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Kode</th>
            <th>Deskripsi</th>
            <th>Hide</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($data->listdata as $key): ?>
            <?php if ($key['ivisibility']=='0'): ?>
              <tr class="danger">
            <?php else: ?>
              <tr>  
            <?php endif ?>
            
              <td class="tengah"><?= $key['dt_flag'] ?></td>
              <td class="tengah"><?= $key['dt_desc'] ?></td>
              <td class="tengah"><a href="#" title="Hide">Hide</a></td>
            </tr>
          <?php endforeach ?>
        </tbody>
      </table>

      <hr>
      <form action="" method="post" accept-charset="utf-8">
      <div class="form-group">
        <label>Kode</label>
        <input type="text" name="in[kode]" class="form-control" maxlength="3" placeholder="Masukkan Kode">
      </div>
      <div class="form-group">
        <label>Deskripsi</label>
        <input type="text" name="in[desc]" class="form-control" placeholder="Deskripsi">
      </div>
      <button class="btn btn-lg btn-primary">Submit</button>  
      </form>
    </div>
  </div>

</div>
<script type="text/javascript" src="<?= $data->base_url ?>js/jquery-ui/jquery-ui.js"></script>
<script>
  $(document).ready(function(){
    $('#tgl').datepicker({
      dateFormat: 'yy-mm-dd'
    });
  });

  $('#samadenganinvoice').click(function(){
    var cek = $('#samadenganinvoice').is(":checked");
    if (cek) {
      // alert('iya, jumlah sama dengan invoice.');
      $('#totaltelahterbayar').fadeOut(1000);
    }else{
      // alert('tidak, saya punya jumlah yang lain.');
      $('#totaltelahterbayar').fadeIn(1000);
    }
  });
</script>
     
</body>
</html> 