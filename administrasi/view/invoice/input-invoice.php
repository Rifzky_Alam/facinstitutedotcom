<!DOCTYPE html>
<html>


<?php include_once 'view/main-component/header.php'; ?>

<body>
<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
?>
<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3>Input Invoice <?php echo $data->id_transaksi ?></h3>
	</div>
	<div class="row">
     <form action=""  method="post">

        <div class="row">
            <div class="col-md-10">
                <h4>Perusahaan: </h4>
                <h3><?php echo $data->nama_usaha ?></h3>
                <?php if ($data->nama_usaha==''): ?>
                    <script type="text/javascript">alert('Nama perusahaan tidak ada, silahkan memilih nama perusahaan kembali!');</script>
                    <!--<script type="text/javascript">location.replace('perusahaan');</script>-->
                    <?php else: ?>

                <?php endif ?>
            </div>
        </div>
        <br><br>
         <div class="row">
             <div class="col-md-10">
                 <div class="form-group">
                     <label>Metode Bayar</label>
                     <select name="in[metodebayar]" class="form-control">
                         <option value="1">Bayar di muka</option>
                         <option value="2">Termin-1</option>
                         <option value="3">Termin-2</option>
                         <option value="4">Termin-3</option>
                         <option value="5">Termin-4</option>
                     </select>
                 </div>
             </div>
         </div>

         <div class="row">
             <div class="col-md-10">
                <div class="form-group">
                    <label>Tanggal</label>
                     <input type="text" class="form-control" name="in[tanggal]" id="tglinv" value="<?php echo date('Y-m-d') ?>" />
                </div>  
             </div>
         </div>

         <div class="row">
             <div class="col-md-10">
                <div class="form-group">
                    <label>Alamat Pengiriman Invoice</label>
                    <textarea class="form-control" name="in[alamatinv]"><?php echo $data->alamatinv ?></textarea>
                </div>  
             </div>
         </div>


         <div class="row">
             <div class="col-md-10">
                <div class="form-group">
                    <label>Deskripsi</label>
                     <input type="text" class="form-control" name="in[deskripsi]"/>
                </div>  
             </div>
         </div>

         <div class="row">
             <div class="col-md-10">
                <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>        
             </div>
         </div>
         
     </form>   
    </div>

</div><!--end container-->
<script type="text/javascript" src=<?php echo "'".$data->base_url."js/jquery-ui/jquery-ui.js'"; ?>></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#tglinv').datepicker({
            dateFormat: 'yy-mm-dd'
        });
    });

</script>

</body>
</html> 