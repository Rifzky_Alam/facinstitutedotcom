<!DOCTYPE html>
<html>
<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>

<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->subtitle ?></h3>
	</div>

  <div class="row" style="text-align:right;">
    <div class="col-md-12">
      <a data-toggle="modal" href="#modal-cari" class="btn btn-danger">Cari Data?</a>
    </div>
  </div><br>

  <div class="row">
    <div class="col-md-12" style="overflow:auto;">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>No Invoice</th>
            <th>Nama Customer</th>
            <th>Nama Perusahaan</th>
            <th>Items</th>
            <th>Deskripsi Invoice</th>
            <th>Jumlah</th>
            <th>Terbayar</th>
            <th>Sisa</th>
            <th>Status</th>
            <th>Tanggal Inv</th>
            <th>Tgl Bayar</th>
            <th>Petugas</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php if (count($data->tabeldata)=='0'): ?>
            <tr>
              <td colspan="11" style="text-align:center;">Tidak ada data tersedia</td>
            </tr>
          <?php else: ?>
            <?php 
              $totalomset = 0;
              $totalterbayar = 0;
            ?>
            <?php foreach ($data->tabeldata as $key): ?>
              <?php if ($key['inv_status']=='0'): ?>
                <tr class="danger">
              <?php elseif($key['inv_status']=='1'): ?>
                <tr class="warning">
              <?php elseif($key['inv_status']=='2'): ?>
                <tr class="success">
              <?php elseif($key['inv_status']=='-1'): ?>
                <tr style='background-color:purple;'>
              <?php elseif($key['inv_status']=='3'): ?>
                <tr class="info">
              <?php else: ?>
                <tr>
              <?php endif ?>
              
                <td><?= $key['inv_no_invoice'] ?></td>
                <td><a href="<?= $data->base_url.'administrasi/new-detail?tr='.$key['inv_id_trans'] ?>" title="Detail Transaksi"><?= $key['nama_cust'] ?></a></td>
                <td><?= $key['nama_usaha'] ?></td>
                <td><?= $key['items'] ?></td>
                <td><?= $key['inv_deskripsi'] ?></td>
                <td><?= number_format($key['jumlah']) ?></td>
                <td><?= number_format($key['terbayar']) ?></td>
                <?php if ($key['inv_status']=='3'): ?>
                  <td>0</td>
                <?php else: ?>  
                  <td>
                    <?= number_format($key['jumlah']-$key['terbayar']) ?>    
                  </td>
                <?php endif ?>

                <?php //hitung omset dan terbayar ?>
                <?php 
                //omset
                if (intval($key['inv_status'])>=0) {
                  $totalomset += $key['jumlah'];
                }

                //terbayar
                if (intval($key['inv_status'])>1) {
                  if (intval($key['terbayar'])==0) {
                    $totalterbayar += intval($key['jumlah']);
                  } else {
                    $totalterbayar += intval($key['jumlah']-$key['terbayar']);
                  }
                }
                ?>
                
                <td><?= $key['si_desc'] ?></td>
                <td><?= GetTanggal($key['inv_date']) ?></td>
                <td><?= GetTanggal($key['tgl_bayar']) ?></td>
                <td><?= $key['nama_petugas'] ?></td>
                <td>
                  <?php if ($key['inv_status']=='3'||$key['inv_status']=='2'||$key['inv_status']=='1'): ?>
                  <a href="<?= $data->base_url ?>administrasi/invoice?paidmenu=<?= $key['inv_no_invoice'] ?>" title="Tandai Terbayar">
                  Tandai Terbayar    
                  </a>  
                  <?php endif ?>
                </td>
              </tr>
            <?php endforeach ?>
            <tr>
              <td colspan="2"><b>Omset</b></td>
              <td colspan="4"><?= number_format($totalomset) ?> IDR</td>
              <td colspan="2"><b>Terbayar</b></td>
              <td colspan="5"><?= number_format($totalterbayar) ?> IDR</td>
            </tr>
          <?php endif ?>
        </tbody>
      </table>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <?php Pagination($data->url,30,$data->pagenum,$data->totaldata,5) ?>
    </div>
  </div>

</div>


<!-- Modal -->
  <div class='modal fade' id='modal-cari' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pencarian Data</h3>

        </div>
        <div class='modal-body'>
        <div style='padding-left:15px;padding-right:15px;padding-top:0px; height: 300px;overflow-y:auto;' class='row'>
          


          <div class="col-md-12">
            <form action="" method="get">
            <div class="row">
                <div class="form-group">
                    <label>Nama Perusahaan</label>
                    <input type="text" class="form-control" name="src[np]">
                </div>
                <div class="form-group">
                    <label>Nama Personal Kontak</label>
                    <input type="text" class="form-control" name="src[nk]">
                </div>

                <div class="form-group">
                    <label>Tipe Transaksi</label>
                    <select class="form-control" name="src[tipe]">
                        <option value="" selected>-- Pilih Jenis Transaksi --</option>
                        <?php foreach ($data->list_jenis_transaksi as $key): ?>
                          <option value="<?= $key['jt_id'] ?>"><?= $key['jt_ket'] ?></option>
                        <?php endforeach ?>
                    </select>
                </div>

                <div class="form-group">
                    <label>Total Biaya</label>
                    <input type="text" class="form-control" name="src[total]" value="0">
                </div>
                <div style="">
                    <input type="checkbox" name="src[tglbayar]" value="y"> Tanggal Bayar    
                </div>
                <div class="form-group">
                    <label>Tanggal Awal</label>
                    <input type="text" id="tglawal" class="form-control" name="src[fd]">
                </div>


                <div class="form-group">
                    <label>Tanggal Akhir</label>
                    <input type="text" id="tglakhir" class="form-control" name="src[ld]">
                </div>
                <div class="form-group">
                    <label>Status</label>
                    <select name="src[st]" class="form-control">
                        <option value="" selected>--pilih status invoice--</option>
                        <option value="1">Belum Terbayar</option>
                        <option value="2">Outstanding</option>
                        <option value="3">Terbayar</option>
                        <option value="0">Belum terkirim</option>
                        <option value="-1">Dibatalkan</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Marketing</label>
                    <select name="src[sls]" class="form-control">
                        <option value="">-- Pilih Marketing --</option>
                        <?php if (count($data->datamarketing)=='0'): ?>
                        
                        <?php else: ?>
                        <?php foreach ($data->datamarketing as $key): ?>
                            <option value="<?= $key['username'] ?>"><?= $key['nama'] ?></option>
                        <?php endforeach ?>
                        <?php endif ?>
                    </select>
                </div>

            </div>

            <div class="row">
                <button class="btn btn-lg btn-success" style="width:100%">Cari</button>
            </div> 
            </form>
          </div>
             
          

        </div>
          

        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>FAC-Institute 2016</span>
                </div>
                <div class="col-md-6">
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->



<script type="text/javascript" src="<?= $data->base_url ?>js/jquery-ui/jquery-ui.js"></script>
<script>
  $(document).ready(function(){
    $('#tgl').datepicker({
      dateFormat: 'yy-mm-dd'
    });
    $('#tglawal').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('#tglakhir').datepicker({
            dateFormat: 'yy-mm-dd'
        });
  });

  $('#samadenganinvoice').click(function(){
    var cek = $('#samadenganinvoice').is(":checked");
    if (cek) {
      // alert('iya, jumlah sama dengan invoice.');
      $('#totaltelahterbayar').fadeOut(1000);
    }else{
      // alert('tidak, saya punya jumlah yang lain.');
      $('#totaltelahterbayar').fadeIn(1000);
    }
  });
</script>
     
</body>
</html> 

<?php function Pagination($baseurl,$limit,$page,$totaldata,$limitlist){ ?>
    <?php if ($page!='0'): ?>
        <div class="row">
            <div class="col-md-12">
                <center>
        <?php if (intval($page)*$limit<$totaldata): ?>
            Showing data <? echo (intval($page)*$limit)-$limit . ' - ' . intval($page)*$limit . ' of '.$totaldata;?>    
            <?php else: ?>
            Showing data <? echo (intval($page)*$limit)-$limit . ' - ' . $totaldata . ' rows of '.$totaldata;?>
        <?php endif ?>
                </center>
            </div>
        </div>
    <?php else: ?>
        <div class="row">
            <div class="col-md-12">
                <center>
                    <?php if ($totaldata<=$limit): ?>
                    Showing data 0 - <?php echo $totaldata ?> rows of <?php echo $totaldata; ?>
                        <?php else: ?>
                    Showing data 0 - <?php echo $limit ?> rows of <?php echo $totaldata; ?>
                    <?php endif ?>
                    
                </center>
            </div>
        </div>
    <?php endif ?>

    <div class="row">
        <div class="col-md-12">
        <center>
            <ul class="pagination pagination-sm">

            <?php 

                // $limitlist = 5;
                if (isset($_GET['page'])) {
                    $page = $_GET['page'];
                } else {
                    $page = 0;
                }
                
                if ($page=='0') {
                    $blok = 0;
                } else {
                    $blok = intval($page/$limitlist);    
                }
                
                if ($blok>0) {
                    $j = $j=intval($blok)*intval($limitlist)-1;
                    echo "<li><a href='".
                    $baseurl.
                    "?page=" .
                    $j.
                     "'>< Prev</a></li>";
                }


                for($i=intval($blok*$limitlist);$i<intval($blok*$limitlist)+5;$i++){
                    // $jk = $i + $blok * $limitlist + 1;
                    if ($i*$limit<$totaldata) {
                        $kk=$i+1;
                        echo "<li><a href='".$baseurl."?page=$kk'>$kk</a></li>";//echo $i + ($blok*$limit)+1;    
                    }
                }

                if ($blok < $totaldata/$limitlist&&$i*$limit<$totaldata) {
                    $j=intval($blok)*intval($limitlist)+6;
                    echo "<li><a href='".
                    $baseurl.
                    "?page=".
                     $j.
                    "'>Next ></a></li>";
                }
            ?>
            </ul>
        </center>
        </div>
    </div>

<? } //end pagination ?>
