<!DOCTYPE html>
<html>


<?php include_once 'view/main-component/header.php'; ?>

<body>



<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3>Input Attachments</h3>
	</div>


    <div class="row">
        <div class="col-md-10">
            <div class="jumbotron">
                <h3><?php echo $data->judul_berita ?></h3>
                <p>
                    <?php echo $data->content_berita ?>
                </p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-10">
            <a href="new-perusahaan-info?tr=<?php echo $data->id_transaksi ?>" class="btn btn-lg btn-primary">Kembali ke Transaksi >></a>
            <a href="data-transaksi" class="btn btn-lg btn-info">Kembali ke tabel Transaksi</a>
        </div>
    </div>

	<div class="row">
        <div class="col-md-10">
            
        </div>
    </div>

</div><!--end container-->
</body>
</html>