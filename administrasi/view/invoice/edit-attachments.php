<!DOCTYPE html>
<html>


<?php include_once 'view/main-component/header.php'; ?>

<body>



<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3>Input Attachments</h3>
	</div>


    <div class="row">
        <div class="col-md-10">
            <div class="jumbotron">
                <h3>File invoice sudah dibuat</h3>
                <p>
                    Silahkan untuk memilih email, attachment dan masukkan carbon copy email yang akan anda kirimkan. selalu pastikan anda sudah melihat <a href="new-preview?einv=<?php echo $data->no_invoice ?>" target="_blank">format email</a> dan <a href="new-preview?inv=<?php echo $data->no_invoice ?>" target="_blank">pdf</a> yang akan dikirim terlebih dahulu.
                </p>
            </div>
        </div>
    </div>
	<div class="row">
     <form action=""  method="post">

            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Email</label>
                        <?php Emails($data->emails,$data->email) ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Email CC</label>
                        <input type="text" name="ubh[cc]" class="form-control" value="<?php echo $data->cc ?>">
                    </div>
                </div>
            </div>

            <h5><strong>Attachments</strong></h5>
            <div class="row">
                <div class="col-sm-8">
                <?php Attachments($data->files,$data->attachments) ?>
                </div>
            </div>

         
         <div class="row">
             <div class="col-md-10">
                <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>        
             </div>
         </div>
         
     </form>   
    </div>

</div><!--end container-->
</body>
</html>


<?php function Attachments($data,$attch){ ?>
    <?php if (count($attch)=='0'): ?>
        <div>
            <p>No Attachment can be detected by system, please change directory</p>
        </div>
    <?php else: ?>
        <?php foreach ($data as $key) { ?>
            <?php if (in_array($key, $attch)): ?>
                <div class='checkbox'>
                    <label>
                        <input checked type='checkbox' name='ubh[lampiran][]' value="<?php echo $key ?>">
                        <?php echo $key ?>
                    </label>
                </div>
                <?php else: ?>
                <div class='checkbox'>
                    <label>
                        <input type='checkbox' name='ubh[lampiran][]' value="<?php echo $key ?>">
                        <?php echo $key ?>
                    </label>
                </div>
            <?php endif ?>
            
        <?php } ?>
    <?php endif ?>
<?php } ?> 

<?php function Emails($arr,$data){ ?>
    <?php foreach ($arr as $key){ ?>
        <?php if ($key==$data): ?>
            <div class="radio">
                <label>
                    <input checked type="radio" name="ubh[email]" value="<?php echo $key ?>">
                    <?php echo $key ?>                                
                </label>
            </div>
            <?php else: ?>
            <div class="radio">
                <label>
                    <input type="radio" name="ubh[email]" value="<?php echo $key ?>">
                    <?php echo $key ?>                                
                </label>
            </div>
        <?php endif ?>
    <?php } ?>
    
<?php } ?> 