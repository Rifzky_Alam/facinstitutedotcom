<!DOCTYPE html>
<html>
<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>

<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->subtitle ?></h3>
	</div>

  <div class="row">
    <div class="col-md-8">
      <form action="" method="POST" accept-charset="utf-8">
      <div class="form-group">
        <label>Tanggal Bayar</label>
        <input id="tgl" type="text" name="in[paiddate]" class="form-control" placeholder="Tanggal Invoice di bayarkan" required>
        
        
      </div>
      <div class="form-group">
        <label>Total yang dibayar</label>
        <br>
        <label>
          <?php if ($data->totaldibayar=='0'): ?>
          <input type="checkbox" name="in[paidmark]" id="samadenganinvoice" value="paid" checked> Jumlah sama dengan invoice</label>
        <div id="totaltelahterbayar" class="collapse">
          <?php else: ?>
          <input type="checkbox" name="in[paidmark]" id="samadenganinvoice" value="paid"> Jumlah sama dengan invoice</label>
        <div id="totaltelahterbayar" class="collapse in">
          <?php endif ?>
          <input type="hidden" name="in[amount]" value="<?= $data->jumlahbayar ?>">
          <input type="number" name="in[paidtotal]" value="<?= intval($data->jumlahbayar) ?>" style="width:75%" class="form-control" placeholder="Total yang telah terbayar">
          <?php if ($data->islunas=='3'): ?>
            <label><input id="lunas" type="checkbox" name="in[paidmark2]" value="paid" checked> Tandai Lunas</label>
            <?php else: ?>
            <label><input id="lunas" type="checkbox" name="in[paidmark2]" value="paid"> Tandai Lunas</label>  
          <?php endif ?>
          
        </div>
      </div>
      <div class="form-group">
        <label><a href="<?= $data->base_url.'administrasi/invoice/paymtd' ?>" target="_blank" title="Tambah baru">Tipe Pembayaran</a></label>
        <select name="in[paidtype]" class="form-control" required>
          <option value="">-- Pilih Tipe Pembayaran --</option>
          <?php foreach ($data->daftarbayar as $value): ?>
            <?php if ($value['dt_flag']==$data->tipepembayaran): ?>
            <option selected value="<?= $value['dt_flag'] ?>"><?= $value['dt_desc'] ?></option>
            <?php else: ?>
            <option value="<?= $value['dt_flag'] ?>"><?= $value['dt_desc'] ?></option>  
            <?php endif ?>
            
          <?php endforeach ?>
        </select>
      </div>
      <?php if ($data->islunas=='3'): ?>
        <button class="btn btn-lg btn-primary" disabled>Submit</button>
      <?php else: ?>
        <button class="btn btn-lg btn-primary">Submit</button>
      <?php endif ?>
      
      <a href="<?= $data->base_url.'administrasi/'.$data->backlink ?>" class="btn btn-lg btn-warning"><< Kembali</a>
      </form>
    </div>

    <div class="col-md-4">
      <center>
        <h3><b>Rincian bayar</b></h3>
      </center>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Tanggal Bayar</th>
            <th>Metode</th>
            <th>Jumlah</th>
            <th>PIC</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          <?php if (count($data->listtrans)=='0'): ?>
            <tr>
              <td class="tengah" colspan="5">
                Belum ada data
              </td>
            </tr>
          <?php else: ?>
              <?php $total = 0; ?>
              <?php foreach ($data->listtrans as $key): ?>
                <tr>
                  <td><?= $key['ddate'] ?></td>
                  <td><?= $key['ket'] ?></td>
                  <?php $total += intval($key['ipayamt']) ?>
                  <td><?= number_format($key['ipayamt']) ?></td>
                  <td><?= $key['pic'] ?></td>
                  <td class="tengah"><a href="<?= $data->base_url.'administrasi/invoice?rmvpay='.$key['lseqid'] ?>" title="Hapus">Hapus</a></td>
                </tr>
              <?php endforeach ?>
              <tr>
                <td colspan="3"><b>Total</b></td>
                <td colspan="2" style="text-align:right;"><b><?= number_format($total).' IDR' ?></b></td>
              </tr>
          <?php endif ?>
            
        </tbody>
      </table>

      <?php if ($data->islunas=='3'): ?>
      <p>Status Invoice: <strong>Lunas</strong></p>
      <?php endif ?>
    </div>
  </div>

</div>
<script type="text/javascript" src="<?= $data->base_url ?>js/jquery-ui/jquery-ui.js"></script>
<script>
  $(document).ready(function(){
    $('#tgl').datepicker({
      dateFormat: 'yy-mm-dd'
    });
  });

  $('#samadenganinvoice').click(function(){
    var cek = $('#samadenganinvoice').is(":checked");
    if (cek) {
      // alert('iya, jumlah sama dengan invoice.');
      $('#totaltelahterbayar').fadeOut(1000);
    }else{
      // alert('tidak, saya punya jumlah yang lain.');
      $('#totaltelahterbayar').fadeIn(1000);
    }
  });
</script>
     
</body>
</html> 