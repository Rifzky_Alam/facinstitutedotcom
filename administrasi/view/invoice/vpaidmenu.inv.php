<!DOCTYPE html>
<html>
<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>

<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->subtitle ?></h3>
	</div>

  <div class="row">
    <div class="col-md-10">
      <form action="" method="POST" accept-charset="utf-8">
      <div class="form-group">
        <label>Tanggal Bayar</label>
        <?php if ($data->tglbayar=='0000-00-00'): ?>
          <input id="tgl" type="text" name="in[paiddate]" class="form-control" placeholder="Tanggal Invoice di bayarkan" required>
        <?php else: ?>
          <input id="tgl" value="<?= $data->tglbayar ?>" type="text" name="in[paiddate]" class="form-control" placeholder="Tanggal Invoice di bayarkan" required>
        <?php endif ?>
        
      </div>
      <div class="form-group">
        <label>Total yang dibayar</label>
        <br>
        <label>
          <?php if ($data->totaldibayar=='0'): ?>
          <input type="checkbox" name="in[paidmark]" id="samadenganinvoice" value="paid" checked> Jumlah sama dengan invoice</label>
        <div id="totaltelahterbayar" class="collapse">
          <?php else: ?>
          <input type="checkbox" name="in[paidmark]" id="samadenganinvoice" value="paid"> Jumlah sama dengan invoice</label>
        <div id="totaltelahterbayar" class="collapse in">
          <?php endif ?>
          <input type="hidden" name="in[amount]" value="<?= $data->jumlahbayar ?>">
          <input type="number" name="in[paidtotal]" value="<?= intval($data->jumlahbayar)-intval($data->totaldibayar) ?>" style="width:75%" class="form-control" placeholder="Total yang telah terbayar">
          <?php if ($data->islunas=='3'): ?>
            <label><input id="lunas" type="checkbox" name="in[paidmark2]" value="paid" checked> Tandai Lunas</label>
            <?php else: ?>
            <label><input id="lunas" type="checkbox" name="in[paidmark2]" value="paid"> Tandai Lunas</label>  
          <?php endif ?>
          
        </div>
      </div>
      <div class="form-group">
        <label><a href="#" title="Tambah baru">Tipe Pembayaran</a></label>
        <select name="in[paidtype]" class="form-control" required>
          <option value="">-- Pilih Tipe Pembayaran --</option>
          <?php foreach ($data->daftarbayar as $value): ?>
            <?php if ($value['dt_flag']==$data->tipepembayaran): ?>
            <option selected value="<?= $value['dt_flag'] ?>"><?= $value['dt_desc'] ?></option>
            <?php else: ?>
            <option value="<?= $value['dt_flag'] ?>"><?= $value['dt_desc'] ?></option>  
            <?php endif ?>
            
          <?php endforeach ?>
        </select>
      </div>
      <button class="btn btn-lg btn-primary">Submit</button>
      </form>
    </div>
  </div>

</div>
<script type="text/javascript" src="<?= $data->base_url ?>js/jquery-ui/jquery-ui.js"></script>
<script>
  $(document).ready(function(){
    $('#tgl').datepicker({
      dateFormat: 'yy-mm-dd'
    });
  });

  $('#samadenganinvoice').click(function(){
    var cek = $('#samadenganinvoice').is(":checked");
    if (cek) {
      // alert('iya, jumlah sama dengan invoice.');
      $('#totaltelahterbayar').fadeOut(1000);
    }else{
      // alert('tidak, saya punya jumlah yang lain.');
      $('#totaltelahterbayar').fadeIn(1000);
    }
  });
</script>
     
</body>
</html> 