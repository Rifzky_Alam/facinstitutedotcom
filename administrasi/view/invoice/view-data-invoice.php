<!DOCTYPE html>
<html>


<?php include_once 'view/main-component/header.php'; ?>

<body>



<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
include_once 'functions/DateFunc.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><a href="<?php echo $data->url ?>"><?php echo $data->subtitle ?></a></h3>
	</div>
    
    <div class="row" style="text-align:right;">
        <div class="col-md-12">
            <a data-toggle="modal" href="#modal-cari" class="btn btn-danger">Cari Data?</a>
        </div>
    </div><br>

    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-info">
                <div class="panel-heading">Omset Bulan Lalu</div>
                <div class="panel-body" id="omsetblnlalu">Loading ..</div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-success">
                <div class="panel-heading">Omset Bulan Ini</div>
                <div class="panel-body" id="omsetblnini">Loading ..</div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-warning">
                <div class="panel-heading">Omset Tahun Lalu</div>
                <div class="panel-body"><?php echo number_format($data->omset_last_year) ?> IDR</div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-primary">
                <div class="panel-heading">Omset Tahun Ini</div>
                <div class="panel-body"><?php echo number_format($data->omset_this_year) ?> IDR</div>
            </div>
        </div>    
    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-info">
                <div class="panel-heading">Terbayar Bulan Lalu</div>
                <div class="panel-body" id="terbayarblnlalu">Loading ..</div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-success">
                <div class="panel-heading">Terbayar Bulan Ini</div>
                <div class="panel-body" id="terbayarblnini">Loading ..</div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-warning">
                <div class="panel-heading">Terbayar Tahun Lalu</div>
                <div class="panel-body"><?php echo number_format($data->paid_last_year) ?> IDR</div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-primary">
                <div class="panel-heading">Terbayar Tahun Ini</div>
                <div class="panel-body"><?php echo number_format($data->paid_this_year) ?> IDR</div>
            </div>
        </div>    
    </div>

    <div class="row">
        <div class="col-md-12"  style="overflow: auto;">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th class="tengah">No Invoice</th>
                        <th class="tengah">Perusahaan</th>
                        <th class="tengah">Customer</th>
                        <th class="tengah">Items</th>
                        <th class="tengah">Deskripsi</th>
                        <th class="tengah">Jumlah</th>
                        <th class="tengah">Terbayar</th>
                        <th class="tengah">Sisa</th>
                        <th class="tengah">Tanggal Bayar</th>
                        <th class="tengah">Tanggal Kirim</th>
                        <th class="tengah">Petugas</th>
                        <th class="tengah">Aksi</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php Rows($data->list,$data->is_report) ?>
                </tbody>
            </table>
            <?php if (isset($data->dataget)): ?>
                <a href="https://fac-institute.com/administrasi/exceldownload/laporaninvoice?<?= $data->dataget ?>" title="Download">Download</a>
                
            <?php endif ?>
        </div>
    </div>

    <?php Pagination($data->url,30,$data->pagenum,$data->totaldata,5) ?>
	

</div><!--end container-->

<!-- Modal -->
  <div class='modal fade' id='modal-cari' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pencarian Data</h3>

        </div>
        <div class='modal-body'>
        <div style='padding-left:15px;padding-right:15px;padding-top:0px; height: 300px;overflow-y:auto;' class='row'>
          


          <div class="col-md-12">
            <form action="" method="get">
            <div class="row">
                <div class="form-group">
                    <label>Nama Perusahaan</label>
                    <input type="text" class="form-control" name="src[np]">
                </div>
                <div class="form-group">
                    <label>Nama Personal Kontak</label>
                    <input type="text" class="form-control" name="src[nk]">
                </div>

                <div class="form-group">
                    <label>Tipe Transaksi</label>
                    <select class="form-control" name="src[tipe]">
                        <option value="" selected>-- Pilih Jenis Transaksi --</option>
                        <?php JenisTransaksi($data->list_jenis_transaksi) ?>
                    </select>
                </div>

                <div class="form-group">
                    <label>Total Biaya</label>
                    <input type="text" class="form-control" name="src[total]" value="0">
                </div>
                <div style="">
                    <input type="checkbox" name="src[tglbayar]" value="y"> Tanggal Bayar    
                </div>
                <div class="form-group">
                    <label>Tanggal Awal</label>
                    <input type="text" id="tglawal" class="form-control" name="src[fd]">
                </div>


                <div class="form-group">
                    <label>Tanggal Akhir</label>
                    <input type="text" id="tglakhir" class="form-control" name="src[ld]">
                </div>
                <div class="form-group">
                    <label>Status</label>
                    <select name="src[st]" class="form-control">
                        <option value="" selected>--pilih status invoice--</option>
                        <option value="1">Belum Terbayar</option>
                        <option value="2">Outstanding</option>
                        <option value="3">Terbayar</option>
                        <option value="0">Belum terkirim</option>
                        <option value="-1">Dibatalkan</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Marketing</label>
                    <select name="src[sls]" class="form-control">
                        <option value="">-- Pilih Marketing --</option>
                        <?php if (count($data->datamarketing)=='0'): ?>
                        
                        <?php else: ?>
                        <?php foreach ($data->datamarketing as $key): ?>
                            <option value="<?= $key['username'] ?>"><?= $key['nama'] ?></option>
                        <?php endforeach ?>
                        <?php endif ?>
                    </select>
                </div>

            </div>

            <div class="row">
                <button class="btn btn-lg btn-success" style="width:100%">Cari</button>
            </div> 
            </form>
          </div>
             
          

        </div>
          

        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>FAC-Institute 2016</span>
                </div>
                <div class="col-md-6">
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->


<script type="text/javascript" src='https://fac-institute.com/js/jquery-ui/jquery-ui.js'></script>
    <script type="text/javascript">
    Number.prototype.format = function(n, x) {
        var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
        return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
    };

    function ShowLaporanOmset() {
    $.ajax({
            type: "GET",
            url: "https://fac-institute.com/administrasi/invoice/datas",
            data: {
              'req':'omsetbulanan'
            },
            cache: false,
            success: function(data){
              // alert(data);
              var totaltrainingblnlalu = data.omsetbulanlalu.total;
              var totaltrainingskrg = data.omsetbulanini.total;
              $('#omsetblnini').html("Rp " + totaltrainingskrg.format());
              $('#omsetblnlalu').html("Rp " + totaltrainingblnlalu.format());
              // console.log(data);
            }
        }); //end ajax
  }


  function ShowLaporanTerbayar() {
    $.ajax({
            type: "GET",
            url: "https://fac-institute.com/administrasi/invoice/datas",
            data: {
              'req':'terbayarbulanan'
            },
            cache: false,
            success: function(data){
              // alert(data);
              var totaltrainingbulanlalu = data.terbayarbulanlalu.total;
              var totalsekarang = data.terbayarbulanini.total;
              $('#terbayarblnlalu').html("Rp " + totaltrainingbulanlalu.format());
              $('#terbayarblnini').html("Rp " + totalsekarang.format());
              // console.log(data);
            }
        }); //end ajax
  }

    $(document).ready(function(){
        $('#tglawal').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('#tglakhir').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        ShowLaporanOmset();
        ShowLaporanTerbayar();
    });
    </script>
</body>
</html>


<?php function Rows($data,$isreport){ ?>
    <?php if (count($data)=='0'): ?>
        <tr>
            <td colspan="9">Tidak ada data yang masuk ke dalam sistem kami atau sistem sedang dalam perbaikan.</td>
        </tr>
    <?php else: ?>
        <?php $total=0; ?>
        <?php $totalterbayar=0; ?>
        <?php foreach ($data as $key) { ?>
            <?php GetRowColor($key->inv_status) ?>
                <td style="vertical-align: middle;"><?php echo $key->inv_no_invoice ?></td>
                <td style="vertical-align: middle;"><?php echo $key->nama_usaha ?></td>
                <td style="vertical-align: middle;">
                    <a href="new-detail?tr=<?php echo $key->inv_id_trans ?>">
                        <?php echo $key->nama_cust ?>
                    </a>
                </td>
                <td style="vertical-align: middle;">
                    <?php $items = explode('##', $key->items) ?>
                    <?php if ($key->items!=''): ?>
                    <ul style="padding-left:15px">
                    <?php foreach ($items as $keyz) {
                        echo "<li>".$keyz."</li>";
                    }  ?>
                    </ul>
                    <?php else: ?>
                        <span style="color:red">Not Available</span>
                    <?php endif ?>
                    
                </td>
                <td style="vertical-align: middle;"><?php echo $key->inv_deskripsi ?></td>
                <td style="vertical-align: middle;"><?php echo number_format($key->jumlah) ?></td>
                <?php if ($key->inv_status=='3'): ?>
                    <td style="vertical-align: middle;"><?= number_format($key->terbayar) ?></td>
                <?php elseif ($key->inv_status=='2'): ?>
                    <td style="vertical-align: middle;"><?= number_format($key->terbayar) ?></td>
                <?php else: ?>
                    <td style="vertical-align: middle;">0</td>
                <?php endif ?>
                <?php if (intval($key->inv_status)>=0): ?>
                    <?php $total +=$key->jumlah; ?>
                <?php endif ?>    
                <?php if ($key->inv_status=='2'): ?>
                    <?php $totalterbayar += $key->terbayar ?>
                <?php endif ?>
                <?php if ($key->inv_status!='3'): ?>

                    <?php //kalo misalnya nilai selisih adalah 0 ?>
                    
                    <td style="vertical-align: middle;"><?= number_format($key->jumlah-$key->terbayar) ?></td>        
                    
                <?php else: ?>
                    <?php $totalterbayar += $key->terbayar ?>
                    <td style="vertical-align: middle;">0</td>    
                <?php endif ?>
                
                <td style="vertical-align: middle;"><?php echo $key->tgl_bayar ?></td>
                <td style="vertical-align: middle;"><?php echo GetTanggal($key->inv_date) ?></td>
                <td style="vertical-align: middle;"><?php echo $key->nama_petugas ?></td>
                <td style="vertical-align: middle;">
                    <a target="_blank" href="new-perusahaan-info?tr=<?php echo $key->inv_id_trans ?>">Edit Transaksi</a>
                    ||
                    <a target="_blank" href="new-invoice?edt=<?php echo $key->inv_no_invoice ?>">Edit Invoice</a>
                    ||
                    <a target="_blank" href="new-preview?<?php echo GetPreviewLink($key->trans_jenis).$key->inv_no_invoice ?>">Prev</a>
                    ||
                    <a target="_blank" href="new-preview?inv=<?php echo $key->inv_no_invoice ?>">PDF</a>
                    ||
                    <a target="_blank" href="new-invoice?valid=<?php echo $key->inv_no_invoice ?>">Kirim</a>
                </td>
                <td style="vertical-align: middle;">
                    <?php GetStatusAction('data-invoice',$key->inv_status,$key->inv_no_invoice) ?>
                </td>
            </tr>
        <?php } ?>
        <?php TotalAmount($total,$totalterbayar,$isreport) ?>
    <?php endif ?>
<?php } ?>


<?php function TotalAmount($value,$value2,$isreport){
    if ($isreport) {
        echo "<tr>";
        echo "<td colspan='4'><b>Total Omset</b></td><td>".number_format($value)." IDR</td>";
        echo "<td colspan='3'><b>Total Terbayar</b></td><td>".number_format($value2)." IDR</td>";
        echo "</tr>";
    } else {
        
    }
    
} ?>

<?php function GetStatusAction($link,$value,$noinv){ ?>
    <?php if ($value=='-1'): ?>
        <a href="<?php echo $link.'?nts='.$noinv ?>">Mark as not sent</a>
    <?php elseif($value=='0'): ?>
        <a href="<?php echo $link.'?snt='.$noinv ?>">Mark as sent</a>
        ||
        <a href="<?php echo $link.'?cncl='.$noinv ?>">Mark as Canceled</a>
    <?php elseif($value=='1'): ?>
        <a href="<?php echo $link.'?paid='.$noinv ?>">Mark as outstanding</a>
        ||
        <a href="<?php echo $link.'?cncl='.$noinv ?>">Mark as Canceled</a>
    <?php elseif($value=='2'||$value=='3'): ?>
        <a href="<?php echo $link.'?snt='.$noinv ?>"><span class="glyphicon glyphicon-remove"></span></a>
        ||
        <a href="<?php echo 'https://fac-institute.com/administrasi/invoice?paidmenu='.$noinv ?>">
            Menu
        </a>
        ||
        <a href="<?php echo $link.'?cncl='.$noinv ?>">Mark as Canceled</a>
    <?php endif ?>
<? } ?>

<?php function Pagination($baseurl,$limit,$page,$totaldata,$limitlist){ ?>
    <?php if ($page!='0'): ?>
        <div class="row">
            <div class="col-md-12">
                <center>
        <?php if (intval($page)*$limit<$totaldata): ?>
            Showing data <? echo (intval($page)*$limit)-$limit . ' - ' . intval($page)*$limit . ' of '.$totaldata;?>    
            <?php else: ?>
            Showing data <? echo (intval($page)*$limit)-$limit . ' - ' . $totaldata . ' rows of '.$totaldata;?>
        <?php endif ?>
                </center>
            </div>
        </div>
    <?php else: ?>
        <div class="row">
            <div class="col-md-12">
                <center>
                    <?php if ($totaldata<=$limit): ?>
                    Showing data 0 - <?php echo $totaldata ?> rows of <?php echo $totaldata; ?>
                        <?php else: ?>
                    Showing data 0 - <?php echo $limit ?> rows of <?php echo $totaldata; ?>
                    <?php endif ?>
                    
                </center>
            </div>
        </div>
    <?php endif ?>


    <div class="row">
        <div class="col-md-12">
        <center>
            <ul class="pagination pagination-sm">

            <?php 

                // $limitlist = 5;
                if (isset($_GET['page'])) {
                    $page = $_GET['page'];
                } else {
                    $page = 0;
                }
                
                if ($page=='0') {
                    $blok = 0;
                } else {
                    $blok = intval($page/$limitlist);    
                }

                // echo 'blok: '.$blok;
                // echo 'page: '.$page;
                // echo 'limitlist: '.$limitlist;
                
                if ($blok>0) {
                    $j = $j=intval($blok)*intval($limitlist)-1;
                    echo "<li><a href='".
                    $baseurl.
                    "?page=" .
                    $j.
                     "'>< Prev</a></li>";
                }


                for($i=intval($blok*$limitlist);$i<intval($blok*$limitlist)+5;$i++){
                    // $jk = $i + $blok * $limitlist + 1;
                    if ($i*$limit<$totaldata) {
                        $kk=$i+1;
                        echo "<li><a href='".$baseurl."?page=$kk'>$kk</a></li>";//echo $i + ($blok*$limit)+1;    
                    }
                }

                if ($blok < $totaldata/$limitlist&&$i*$limit<$totaldata) {
                    $j=intval($blok)*intval($limitlist)+6;
                    echo "<li><a href='".
                    $baseurl.
                    "?page=".
                     $j.
                    "'>Next ></a></li>";
                }

                /*
                $b = intval($page/$limitlist);
                $c = ($b + 1) * $limitlist;
                $batas = intval($totaldata) / $limit;

                if ($page >= $limitlist) {
                    if ($page == $limitlist) {
                        $prev = $b * $limitlist - 2;
                    } else {
                        $prev = $b * $limitlist -1;
                    }

                    echo "<li><a href='".$baseurl."?page=$prev'>< Prev</a></li>";
                }

                for ($i=$b*$limit-1; $i < $c; $i++) { 
                    $j = $i +1;

                    if ($i<$batas&&$j!=0) {
                        echo "<li><a href='".$baseurl."?page=$j'>$j</a></li>";    
                    }
                }
                
                if ($c < $batas) {
                    $k = $c + 1;
                    echo "<li><a href='".$baseurl."?page=$k'>Next ></a></li>";
                }
                */
                // for ($i=1; $i < intval($totalRow) / 30 + 1 ; $i++) { 
                    // echo "<li><a href='".basename(__FILE__, '.php')."?page=$i'>$i</a></li>";   
                // }
            ?>
            </ul>
        </center>
        </div>
    </div>

<? } //end pagination ?>


<?php 
function GetPreviewLink($value){
    if ($value=='1') {
        return 'einv=';
    }elseif ($value=='2') {
        return 'asinv=';
    }elseif ($value=='3') {
        return 'kinv=';
    }else{
        return 'einv=';
    }
}

function JenisTransaksi($data){
    foreach ($data as $key) {
        echo "<option value='$key->jt_id'>$key->jt_ket</option>";
    }
}

function GetRowColor($value){
    if ($value=='0') {
        echo "<tr class='danger'>";
    }elseif ($value=='1') {
        echo "<tr class='warning'>";
    }elseif ($value=='2') {
        echo "<tr class='success'>";
    }elseif ($value=='-1') {
        echo "<tr style='background-color:purple;'>";
    }elseif ($value=='3') {
        echo "<tr class='info'>";
    }else{
        echo "<tr>";
    }
}



?>


