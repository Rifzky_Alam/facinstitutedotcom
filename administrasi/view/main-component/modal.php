<?php function ModalHead($obj){ ?>
<!-- Modal -->
  <div class='modal fade' <?= 'id="'.$obj->getIdmodal().'"' ?> role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title"><?= $obj->getHeadtitle() ?></h3>
        </div>  

        <div class='modal-body' style='overflow-y:scroll'>
        <div style='padding:15px; height: 300px;' class='row'>
<? } ?>
        
<?php function ModalFooter($obj){ ?>
        </div>
       </div>
        <div class='modal-footer'>

            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span><?= $obj->getFooter() ?></span>
                </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->

<? } ?>