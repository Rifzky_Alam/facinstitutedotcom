<?php function Admin($url,$user){ ?>
<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><font style="font-size:14px"><< Back  </font></a>
  <a href='https://fac-institute.com/administrasi'>Beranda</a>

          <a href="javascript:;" data-toggle="collapse" data-target="#team">Data <span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
      
  
    <ul id='team' class='collapse' style='list-style-type:none'>
      <li><a href='https://fac-institute.com/administrasi/daftar-hadir' style='padding-left:0px;color:chartreuse;font-size:14px'>Daftar Absensi Harian</a></li>
        <li>
          <a href="<?= $url ?>administrasi/customerservice/data-support" style='padding-left:0px;color:chartreuse;font-size:14px'>Data Support Client</a>
        </li>
      <li><a href='https://fac-institute.com/administrasi/data-laporan' style='padding-left:0px;color:chartreuse;font-size:14px'>Data Laporan Harian</a></li>
      
              <li>
          <a href='https://fac-institute.com/administrasi/calendar' style='padding-left:0px;color:chartreuse;font-size:14px'>
          Kalender
          </a>
        </li>
        <li>
          <a href='https://fac-institute.com/administrasi/new-usaha' style='padding-left:0px;color:chartreuse;font-size:14px'>Perusahaan
          </a>
        </li>
        <li>
          <a href='https://fac-institute.com/administrasi/forum-table' style='padding-left:0px;color:chartreuse;font-size:14px'>Forum
          </a>
        </li>
            
          </ul>

    

    
  
    <a href="javascript:;" data-toggle="collapse" data-target="#edit-kegiatan">Ubah Data <span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
    <ul id='edit-kegiatan' class='collapse' style='list-style-type:none'>
      <li><a href='https://fac-institute.com/administrasi/edit-absen' style='padding-left:0px;color:aqua;font-size:14px'>Absensi Harian</a></li>
      <li><a href='https://fac-institute.com/administrasi/edit-laporan' style='padding-left:0px;color:aqua;font-size:14px'>Data Laporan Harian</a></li>
    </ul>

    <a href="javascript:;" data-toggle="collapse" data-target="#marketingz">Marketing <span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
    <ul id='marketingz' class='collapse' style='list-style-type:none'>
      <li>
        <a href='https://fac-institute.com/administrasi/data-transaksi' style='padding-left:0px;color:aqua;font-size:14px'>
          Data Transaksi
        </a>
      </li>
      <li>
        <a href='https://fac-institute.com/administrasi/data-customer' style='padding-left:0px;color:aqua;font-size:14px'>
          Data Customer
        </a>
      </li>
      <li>
        <a href='https://fac-institute.com/administrasi/data-training' style='padding-left:0px;color:chartreuse;font-size:14px'>Data Order Training</a>
      </li>
      <li>
      <li><a href='https://fac-institute.com/administrasi/order-new' style='padding-left:0px;color:aqua;font-size:14px'>Input Order Training</a></li>
      <li>
        <a href='https://fac-institute.com/administrasi/kirim-penawaran' style='padding-left:0px;color:aqua;font-size:14px'>
        Penawaran
        </a>
      </li>
      <li>
        <a href='https://fac-institute.com/administrasi/daftar-quotation' style='padding-left:0px;color:aqua;font-size:14px'>
        Data Penawaran Custom
        </a>
      </li>
      <li>
      <a href='https://fac-institute.com/administrasi/daftar-agenda' style='padding-left:0px;color:aqua;font-size:14px'>
      Daftar Jadwal
      </a>
      </li>
      <li>
      <a href='https://fac-institute.com/administrasi/daftar-invoice' style='padding-left:0px;color:aqua;font-size:14px'>
      Invoice
      </a>
      </li>
      <li>
        <a href='https://fac-institute.com/administrasi/data-invoice' style='padding-left:0px;color:aqua;font-size:14px'>
          Data Invoice (New)
        </a>
      </li>
      <li>
        <a href='https://fac-institute.com/administrasi/data-penawaran' style='padding-left:0px;color:aqua;font-size:14px'>
          Data Penawaran (New)
        </a>
      </li>
      <li>
        <a href='https://fac-institute.com/administrasi/table?ac=lfm' style='padding-left:0px;color:aqua;font-size:14px'>
          Laporan follow-up
        </a>
      </li>
      <li>
      <a href="https://fac-institute.com/administrasi/data-external-perusahaan" style='padding-left:0px;color:aqua;font-size:14px'>
        Data External Perusahaan
      </a>
    </li>
    <li>
      <a href="https://fac-institute.com/administrasi/data-external-customer" style='padding-left:0px;color:aqua;font-size:14px'>
        Data External Customer
      </a>
    </li>
    <li>
      <a href="https://fac-institute.com/administrasi/data-external-quotation" style='padding-left:0px;color:aqua;font-size:14px'>
        Data External Penawaran
      </a>
    </li>
    <li>
      <a href="https://fac-institute.com/administrasi/external-upload-files" style='padding-left:0px;color:aqua;font-size:14px'>
        External - Upload Files
      </a>
    </li>

    </ul>

    <a href="javascript:;" data-toggle="collapse" data-target="#input-tulisan">Tutorial <span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
    <ul id='input-tulisan' class='collapse' style='list-style-type:none'>
      <li><a href='https://fac-institute.com/administrasi/cms/tutorial-table' style='padding-left:0px;color:aqua;font-size:14px'>Data Tutorial</a></li>
      <li><a href='https://fac-institute.com/administrasi/cms/' style='padding-left:0px;color:aqua;font-size:14px'>Input Tutorial</a></li>
      <li><a href='https://fac-institute.com/administrasi/cms/tutorial-edit' style='padding-left:0px;color:aqua;font-size:14px'>Edit Tutorial</a></li>
    </ul>

     <a href="javascript:;" data-toggle="collapse" data-target="#input-artikel">Artikel <span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
    <ul id='input-artikel' class='collapse' style='list-style-type:none'>
      <li><a href='https://fac-institute.com/administrasi/artikel/' style='padding-left:0px;color:aqua;font-size:14px'>Data Artikel</a></li>
      <li><a href='https://fac-institute.com/administrasi/artikel/new-data' style='padding-left:0px;color:aqua;font-size:14px'>Input Artikel</a></li>
      <li><a href='https://fac-institute.com/administrasi/artikel/edit-data' style='padding-left:0px;color:aqua;font-size:14px'>Edit Artikel</a></li>
    </ul>
    <a href="javascript:;" data-toggle="collapse" data-target="#adminz">Admin <span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
    <ul id='adminz' class='collapse' style='list-style-type:none'>
      <li><a href='https://fac-institute.com/administrasi/new-users' style='padding-left:0px;color:aqua;font-size:14px'>Pegawai Baru</a></li>
      <li><a href='https://fac-institute.com/administrasi/order-request' style='padding-left:0px;color:aqua;font-size:14px'>Request Order</a></li>
      <li><a href='https://fac-institute.com/administrasi/agents' style='padding-left:0px;color:aqua;font-size:14px'>Agen Training</a></li>
      <li><a href='https://fac-institute.com/administrasi/external-adduser' style='padding-left:0px;color:aqua;font-size:14px'>Input user eksternal</a></li>
    </ul>

      <a href="javascript:;" data-toggle="collapse" data-target="#kursus">Kursus <span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
    <ul id='kursus' class='collapse' style='list-style-type:none'>
      <li>
        <a href='https://fac-institute.com/administrasi/jadwal-kursus' style='padding-left:0px;color:aqua;font-size:14px'>
          Jadwal Kursus
        </a>
      </li>
      <li>
        <a href='https://fac-institute.com/administrasi/siswa-kursus' style='padding-left:0px;color:aqua;font-size:14px'>
          Data Siswa Kursus
        </a>
      </li>
    </ul>

          
</div>
<?php } ?>

<?php function Marketing($url,$user){ ?>
<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><font style="font-size:14px"><< Back  </font></a>
  <a href='https://fac-institute.com/administrasi'>Beranda</a>

          <a href="javascript:;" data-toggle="collapse" data-target="#team">Team <span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
    
  
    <ul id='team' class='collapse' style='list-style-type:none'>
      <li><a href='https://fac-institute.com/administrasi/daftar-hadir' style='padding-left:0px;color:chartreuse;font-size:14px'>Daftar Absensi Harian</a></li>
      <li><a href='https://fac-institute.com/administrasi/data-laporan' style='padding-left:0px;color:chartreuse;font-size:14px'>Data Laporan Harian</a></li>
      
            <li>
        <a href='https://fac-institute.com/administrasi/forum-table' style='padding-left:0px;color:chartreuse;font-size:14px'>
          Forum
        </a>
      </li>
            
      </ul>
      <a href="javascript:;" data-toggle="collapse" data-target="#edit-kegiatan-marketing">Order<span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
    <ul id='edit-kegiatan-marketing' class='collapse' style='list-style-type:none'>
      <li>
        <a href='https://fac-institute.com/administrasi/data-transaksi' style='padding-left:0px;color:aqua;font-size:14px'>
          Data Transaksi
        </a>
      </li>
      <li>
        <a href='https://fac-institute.com/administrasi/data-customer' style='padding-left:0px;color:aqua;font-size:14px'>
          Data Customer
        </a>
      </li>
      <li>
        <a href='https://fac-institute.com/administrasi/order-new' style='padding-left:0px;color:aqua;font-size:14px'>
          Input Order
        </a>
      </li>
      <li>
        <a href='https://fac-institute.com/administrasi/laporan-followup' style='padding-left:0px;color:aqua;font-size:14px'>
          Laporan follow-up
        </a>
      </li>
      <li>
        <a href='https://fac-institute.com/administrasi/data-training' style='padding-left:0px;color:aqua;font-size:14px'>
          Data Order
        </a>
      </li>
      <li>
        <a href='https://fac-institute.com/administrasi/calendar' style='padding-left:0px;color:aqua;font-size:14px'>
          Kalender
        </a>
      </li>
      <li>
        <a href='https://fac-institute.com/administrasi/new-usaha' style='padding-left:0px;color:aqua;font-size:14px'>
          List Perusahaan
        </a>
      </li>
    </ul>
    
    <a href="javascript:;" data-toggle="collapse" data-target="#data-marketing">Data<span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
    <ul id='data-marketing' class='collapse' style='list-style-type:none'>
    <li>
      <a href="https://fac-institute.com/administrasi/daftar-quotation" style='padding-left:0px;color:aqua;font-size:14px'>
        Data Quotation Custom
      </a>
    </li>
    <li>
      <a href="https://fac-institute.com/administrasi/daftar-agenda" style='padding-left:0px;color:aqua;font-size:14px'>
        Data Agenda
      </a>
    </li>
    <li>
      <a href="https://fac-institute.com/administrasi/daftar-invoice" style='padding-left:0px;color:aqua;font-size:14px'>
        Data Invoice
      </a>
    </li>
    <li>
      <a href="https://fac-institute.com/administrasi/data-invoice" style='padding-left:0px;color:aqua;font-size:14px'>
        Data Invoice (New)
      </a>
    </li>
    <li>
        <a href='https://fac-institute.com/administrasi/table?ac=srclm' style='padding-left:0px;color:aqua;font-size:14px'>
          Laporan Training
        </a>
    </li>
    </ul>
    <a href="javascript:;" data-toggle="collapse" data-target="#marketingz">Marketing <span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
    <ul id='marketingz' class='collapse' style='list-style-type:none'>
      <li>
      <a href='https://fac-institute.com/administrasi/kirim-penawaran' style='padding-left:0px;color:aqua;font-size:14px'>
      Penawaran
      </a></li>
      <li>
      <a href='https://fac-institute.com/administrasi/daftar-request' style='padding-left:0px;color:aqua;font-size:14px'>
      Invoice
      </a>
      </li>

    <li>
      <a href="https://fac-institute.com/administrasi/data-external-perusahaan" style='padding-left:0px;color:aqua;font-size:14px'>
        Data External Perusahaan
      </a>
    </li>
    <li>
      <a href="https://fac-institute.com/administrasi/data-external-customer" style='padding-left:0px;color:aqua;font-size:14px'>
        Data External Customer
      </a>
    </li>
    <li>
      <a href="https://fac-institute.com/administrasi/data-external-quotation" style='padding-left:0px;color:aqua;font-size:14px'>
        Data External Penawaran
      </a>
    </li>
    <li>
      <a href="https://fac-institute.com/administrasi/external-upload-files" style='padding-left:0px;color:aqua;font-size:14px'>
        External - Upload Files
      </a>
    </li>
    </ul>
</div>
<?php } ?>

<?php function SuperAdmin($url,$user){ ?>
  
<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><font style="font-size:14px"><< Back  </font></a>
  <a href='https://fac-institute.com/administrasi'>Beranda</a>

          <a href="javascript:;" data-toggle="collapse" data-target="#team">Data <span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
      
  
    <ul id='team' class='collapse' style='list-style-type:none'>
      <li><a href='https://fac-institute.com/administrasi/daftar-hadir' style='padding-left:0px;color:chartreuse;font-size:14px'>Daftar Absensi Harian</a></li>
      <li><a href='https://fac-institute.com/administrasi/data-laporan' style='padding-left:0px;color:chartreuse;font-size:14px'>Data Laporan Harian</a></li>
      
              <li>
          <a href='https://fac-institute.com/administrasi/calendar' style='padding-left:0px;color:chartreuse;font-size:14px'>
          Kalender
          </a>
        </li>
        <li>
          <a href='https://fac-institute.com/administrasi/new-usaha' style='padding-left:0px;color:chartreuse;font-size:14px'>Perusahaan
          </a>
        </li>
        <li>
          <a href='https://fac-institute.com/administrasi/forum-table' style='padding-left:0px;color:chartreuse;font-size:14px'>Forum
          </a>
        </li>
            
            <li><a href='https://fac-institute.com/administrasi/log-data' style='padding-left:0px;color:chartreuse;font-size:14px'>Web User Log Datas</a></li>
      
          </ul>
    <a href="javascript:;" data-toggle="collapse" data-target="#edit-kegiatan">Ubah Data <span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
    <ul id='edit-kegiatan' class='collapse' style='list-style-type:none'>
      <li><a href='https://fac-institute.com/administrasi/edit-absen' style='padding-left:0px;color:aqua;font-size:14px'>Absensi Harian</a></li>
      <li><a href='https://fac-institute.com/administrasi/edit-laporan' style='padding-left:0px;color:aqua;font-size:14px'>Data Laporan Harian</a></li>
    </ul>

    <a href="javascript:;" data-toggle="collapse" data-target="#marketingz">Marketing <span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
    <ul id='marketingz' class='collapse' style='list-style-type:none'>
      <li>
        <a href='https://fac-institute.com/administrasi/data-transaksi' style='padding-left:0px;color:aqua;font-size:14px'>
          Data Transaksi
        </a>
      </li>
      <li>
        <a href='https://fac-institute.com/administrasi/data-customer' style='padding-left:0px;color:aqua;font-size:14px'>
          Data Customer
        </a>
      </li>
      <li>
        <a href='https://fac-institute.com/administrasi/data-training' style='padding-left:0px;color:chartreuse;font-size:14px'>Data Order Training</a>
      </li>
      <li>
      <li><a href='https://fac-institute.com/administrasi/order-new' style='padding-left:0px;color:aqua;font-size:14px'>Input Order Training</a></li>
      <li>
        <a href='https://fac-institute.com/administrasi/kirim-penawaran' style='padding-left:0px;color:aqua;font-size:14px'>
        Penawaran
        </a>
      </li>
      <li>
        <a href='https://fac-institute.com/administrasi/daftar-quotation' style='padding-left:0px;color:aqua;font-size:14px'>
        Data Penawaran Custom
        </a>
      </li>
      <li>
      <a href='https://fac-institute.com/administrasi/daftar-agenda' style='padding-left:0px;color:aqua;font-size:14px'>
      Daftar Jadwal
      </a>
      </li>
      <li>
      <a href='https://fac-institute.com/administrasi/daftar-invoice' style='padding-left:0px;color:aqua;font-size:14px'>
      Invoice
      </a>
      </li>
      <li>
        <a href='https://fac-institute.com/administrasi/data-invoice' style='padding-left:0px;color:aqua;font-size:14px'>
          Data Invoice (New)
        </a>
      </li>
      <li>
        <a href='https://fac-institute.com/administrasi/data-penawaran' style='padding-left:0px;color:aqua;font-size:14px'>
          Data Penawaran (New)
        </a>
      </li>
      <li>
        <a href='https://fac-institute.com/administrasi/table?ac=lfm' style='padding-left:0px;color:aqua;font-size:14px'>
          Laporan follow-up
        </a>
      </li>
      <li>
      <a href="https://fac-institute.com/administrasi/data-external-perusahaan" style='padding-left:0px;color:aqua;font-size:14px'>
        Data External Perusahaan
      </a>
    </li>
    <li>
      <a href="https://fac-institute.com/administrasi/data-external-customer" style='padding-left:0px;color:aqua;font-size:14px'>
        Data External Customer
      </a>
    </li>
    <li>
      <a href="https://fac-institute.com/administrasi/data-external-quotation" style='padding-left:0px;color:aqua;font-size:14px'>
        Data External Penawaran
      </a>
    </li>
    <li>
      <a href="https://fac-institute.com/administrasi/external-upload-files" style='padding-left:0px;color:aqua;font-size:14px'>
        External - Upload Files
      </a>
    </li>

    </ul>

    <a href="javascript:;" data-toggle="collapse" data-target="#input-tulisan">Tutorial <span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
    <ul id='input-tulisan' class='collapse' style='list-style-type:none'>
      <li><a href='https://fac-institute.com/administrasi/cms/tutorial-table' style='padding-left:0px;color:aqua;font-size:14px'>Data Tutorial</a></li>
      <li><a href='https://fac-institute.com/administrasi/cms/' style='padding-left:0px;color:aqua;font-size:14px'>Input Tutorial</a></li>
      <li><a href='https://fac-institute.com/administrasi/cms/tutorial-edit' style='padding-left:0px;color:aqua;font-size:14px'>Edit Tutorial</a></li>
    </ul>

     <a href="javascript:;" data-toggle="collapse" data-target="#input-artikel">Artikel <span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
    <ul id='input-artikel' class='collapse' style='list-style-type:none'>
      <li><a href='https://fac-institute.com/administrasi/artikel/' style='padding-left:0px;color:aqua;font-size:14px'>Data Artikel</a></li>
      <li><a href='https://fac-institute.com/administrasi/artikel/new-data' style='padding-left:0px;color:aqua;font-size:14px'>Input Artikel</a></li>
      <li><a href='https://fac-institute.com/administrasi/artikel/edit-data' style='padding-left:0px;color:aqua;font-size:14px'>Edit Artikel</a></li>
    </ul>
    <a href="javascript:;" data-toggle="collapse" data-target="#adminz">Admin <span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
    <ul id='adminz' class='collapse' style='list-style-type:none'>
      <li><a href='https://fac-institute.com/administrasi/new-users' style='padding-left:0px;color:aqua;font-size:14px'>Pegawai Baru</a></li>
      <li><a href='https://fac-institute.com/administrasi/order-request' style='padding-left:0px;color:aqua;font-size:14px'>Request Order</a></li>
      <li><a href='https://fac-institute.com/administrasi/agents' style='padding-left:0px;color:aqua;font-size:14px'>Agen Training</a></li>
      <li><a href='https://fac-institute.com/administrasi/external-adduser' style='padding-left:0px;color:aqua;font-size:14px'>Input user eksternal</a></li>
    </ul>

      <a href="javascript:;" data-toggle="collapse" data-target="#kursus">Kursus <span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
    <ul id='kursus' class='collapse' style='list-style-type:none'>
      <li>
        <a href='https://fac-institute.com/administrasi/jadwal-kursus' style='padding-left:0px;color:aqua;font-size:14px'>
          Jadwal Kursus
        </a>
      </li>
      <li>
        <a href='https://fac-institute.com/administrasi/siswa-kursus' style='padding-left:0px;color:aqua;font-size:14px'>
          Data Siswa Kursus
        </a>
      </li>
    </ul>   
</div>
<? } ?>



<?php function AccountingService($url,$user){ ?>
  <div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><font style="font-size:14px"><< Back  </font></a>
  <a href="https://fac-institute.com/administrasi">Beranda</a>

          <a href="javascript:;" data-toggle="collapse" data-target="#team">Team <span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
    
  
    <ul id="team" class="collapse" style="list-style-type:none">
      <li><a href="https://fac-institute.com/administrasi/daftar-hadir" style="padding-left:0px;color:chartreuse;font-size:14px">Daftar Absensi Harian</a></li>
      <li><a href="https://fac-institute.com/administrasi/data-laporan" style="padding-left:0px;color:chartreuse;font-size:14px">Data Laporan Harian</a></li>
      <li>
          <a href="<?= $url ?>administrasi/customerservice/data-support" style='padding-left:0px;color:chartreuse;font-size:14px'>Data Support Client</a>
        </li>
        
      <li>
        <a href="https://fac-institute.com/administrasi/table?ac=map" style="padding-left:0px;color:chartreuse;font-size:14px">
          Map
        </a>
      </li>
      <li>
        <a href="https://fac-institute.com/administrasi/forum-table" style="padding-left:0px;color:chartreuse;font-size:14px">
          Forum
        </a>
      </li>
            
    </ul>

       <a href="javascript:;" data-toggle="collapse" data-target="#input-artikel">Artikel <span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
    <ul id="input-artikel" class="collapse" style="list-style-type:none">
      <li><a href="https://fac-institute.com/administrasi/artikel/" style="padding-left:0px;color:aqua;font-size:14px">Data Artikel</a></li>
      <li><a href="https://fac-institute.com/administrasi/artikel/new-data" style="padding-left:0px;color:aqua;font-size:14px">Input Artikel</a></li>
      <li><a href="https://fac-institute.com/administrasi/artikel/edit-data" style="padding-left:0px;color:aqua;font-size:14px">Edit Artikel</a></li>
    </ul>
    <a href="javascript:;" data-toggle="collapse" data-target="#input-tulisan">Tutorial <span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
    <ul id="input-tulisan" class="collapse" style="list-style-type:none">
      <li><a href="https://fac-institute.com/administrasi/cms/tutorial-table" style="padding-left:0px;color:aqua;font-size:14px">Data Tutorial</a></li>
      <li><a href="https://fac-institute.com/administrasi/cms/" style="padding-left:0px;color:aqua;font-size:14px">Input Tutorial</a></li>
      <li><a href="https://fac-institute.com/administrasi/cms/tutorial-edit" style="padding-left:0px;color:aqua;font-size:14px">Edit Tutorial</a></li>
    </ul>    
</div>
<? } ?>

<?php function Freelance($url,$user){ ?>
    
<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><font style="font-size:14px"><< Back  </font></a>
  <a href="<?= $url ?>administrasi">Beranda</a>

        <a href="javascript:;" data-toggle="collapse" data-target="#team">Team 
            <span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span>
        </a>
    
    <ul id="team" class="collapse" style="list-style-type:none">
      <li>
        <a href="<?= $url ?>administrasi/daftar-hadir" style="padding-left:0px;color:chartreuse;font-size:14px">
            Daftar Absensi Harian
        </a>
      </li>
      <li>
        <a href="<?= $url ?>administrasi/data-laporan" style="padding-left:0px;color:chartreuse;font-size:14px">
            Data Laporan Harian
        </a>
      </li>        
    </ul>
    
</div>
<? } ?>

<?php function Trainer($url,$user){ ?>

<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><font style="font-size:14px"><< Back  </font></a>
  <a href="https://fac-institute.com/administrasi">Beranda</a>

          <a href="javascript:;" data-toggle="collapse" data-target="#team">Team <span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
    
  
    <ul id="team" class="collapse" style="list-style-type:none">
        <li>
            <a href="<?= $url ?>administrasi/daftar-hadir" style="padding-left:0px;color:chartreuse;font-size:14px">Daftar Absensi Harian
            </a>
        </li>
        <li>
            <a href="<?= $url ?>administrasi/data-laporan" style="padding-left:0px;color:chartreuse;font-size:14px">Data Laporan Harian
            </a>
        </li>
        <li>
          <a href="<?= $url ?>administrasi/customerservice/data-support" style='padding-left:0px;color:chartreuse;font-size:14px'>Data Support Client</a>
        </li>
        <li>
            <a href="<?= $url ?>administrasi/table?ac=map" style="padding-left:0px;color:chartreuse;font-size:14px">
              Map
            </a>
        </li>
        <li>
            <a href="<?= $url ?>administrasi/forum-table" style="padding-left:0px;color:chartreuse;font-size:14px">
              Forum
            </a>
        </li>        
    </ul>

       <a href="javascript:;" data-toggle="collapse" data-target="#input-artikel">Artikel <span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
    <ul id="input-artikel" class="collapse" style="list-style-type:none">
      <li><a href="<?= $url ?>administrasi/artikel/" style="padding-left:0px;color:aqua;font-size:14px">Data Artikel</a></li>
      <li><a href="<?= $url ?>administrasi/artikel/new-data" style="padding-left:0px;color:aqua;font-size:14px">Input Artikel</a></li>
      <li><a href="<?= $url ?>administrasi/artikel/edit-data" style="padding-left:0px;color:aqua;font-size:14px">Edit Artikel</a></li>
    </ul>
    <a href="javascript:;" data-toggle="collapse" data-target="#input-tulisan">Tutorial <span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
    <ul id="input-tulisan" class="collapse" style="list-style-type:none">
      <li><a href="<?= $url ?>administrasi/cms/tutorial-table" style="padding-left:0px;color:aqua;font-size:14px">Data Tutorial</a></li>
      <li><a href="<?= $url ?>administrasi/cms/" style="padding-left:0px;color:aqua;font-size:14px">Input Tutorial</a></li>
      <li><a href="<?= $url ?>administrasi/cms/tutorial-edit" style="padding-left:0px;color:aqua;font-size:14px">Edit Tutorial</a></li>
    </ul>    
</div>
<? } ?>
