<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>


<style>
@media screen and (max-width: 600px) {
  .dropdown-notifications > .dropdown-container, .dropdown-notifications > .dropdown-menu {
      width: 300px;
      /* max-width: 400px; */
  }

  .right-tabs .nav {
    float: right;
    border-bottom: 0px;
}
.right-tabs .nav li { float: left }
.right-tabs .tab-content {
    float: left;
    border-top: 1px solid #ddd;
    margin-top: -1px;
}
}

.loader-ellips {
  font-size: 10px;
  position: relative;
  width: 4em;
  height: 1em;
  margin: 10px auto;
}

.loader-ellips__dot {
  display: block;
  width: 1em;
  height: 1em;
  border-radius: 0.5em;
  background: #555;
  position: absolute;
  animation-duration: 0.5s;
  animation-timing-function: ease;
  animation-iteration-count: infinite;
}

.loader-ellips__dot:nth-child(1),
.loader-ellips__dot:nth-child(2) {
  left: 0;
}
.loader-ellips__dot:nth-child(3) { left: 1.5em; }
.loader-ellips__dot:nth-child(4) { left: 3em; }

@keyframes reveal {
  from { transform: scale(0.001); }
  to { transform: scale(1); }
}

@keyframes slide {
  to { transform: translateX(1.5em) }
}

.loader-ellips__dot:nth-child(1) {
  animation-name: reveal;
}

.loader-ellips__dot:nth-child(2),
.loader-ellips__dot:nth-child(3) {
  animation-name: slide;
}

.loader-ellips__dot:nth-child(4) {
  animation-name: reveal;
  animation-direction: reverse;
}

/* loader-wheel
------------------------- */

.loader-wheel {
  font-size: 64px; /* change size here */
  position: relative;
  height: 1em;
  width: 1em;
  padding-left: 0.45em;
  overflow: hidden;
  margin: 0 auto;
  animation: loader-wheel-rotate 0.5s steps(12) infinite;
}

.loader-wheel i {
  display: block;
  position: absolute;
  height: 0.3em;
  width: 0.1em;
  border-radius: 0.05em;
  background: #333; /* change color here */
  opacity: 0.8;
  transform: rotate(-30deg);
  transform-origin: center 0.5em;
}

@keyframes loader-wheel-rotate {
  from { transform: rotate(0deg); }
  to { transform: rotate(360deg); }
}

.page-load-status {
  padding-top: 20px;
  border-top: 1px solid #DDD;
  text-align: center;
  color: #777;
}

.notification {
      border-width: 0 0 2px 0;
}

/* search */
.modal {
 text-align: center;
 padding: 0!important;
}

.modal:before {
 content: '';
 display: inline-block;
 height: 100%;
 vertical-align: middle;
 margin-right: -4px;
}

.modal-dialog {
 display: inline-block;
 text-align: left;
 vertical-align: middle;
}

.modal-xl {
  width: 90%;
  margin: auto;
}

.modal-m {
  width: 30%;
  margin: auto;
}
</style>

<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" onclick="location.reload();" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
      </div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="col-md-12">
      <div class="navbar-header">
          <span class="navbar-brand" style="font-size:20px;cursor:pointer;color:white" onclick="openNav()"><span class="glyphicon glyphicon-th-list"></span> Menu</span>
      </div>
      <ul class="nav navbar-nav pull-right nav-tabs">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"><span class="glyphicon glyphicon-user"></span> <?php echo $data->username; ?></a>
          <ul class="dropdown-menu">
            <li><a href=<?php echo "'".$data->base_url."administrasi/profile-user'"; ?> >Profilku</a></li>
            <li><a href=<?php echo "'".$data->base_url."administrasi/absensi'"; ?> >Absen</a></li>
            <li><a href=<?php echo "'".$data->base_url."administrasi/user/laporan-harian'"; ?>>Laporan</a></li>
            <li><a href=<?php echo "'".$data->base_url."administrasi/bug/new-bug'"; ?>>Keluhan</a></li>
            <li><a href=<?php echo "'".$data->base_url."administrasi/ubah-password'"; ?>>Ubah Password</a></li>
            <li><a href=<?php echo "'".$data->base_url."administrasi/logout.php'"; ?>>Log Out</a></li>
          </ul>
        </li>

        <li class="dropdown dropdown-notifications ">
          <a href="#notifications-panel" class="dropdown-toggle" data-toggle="dropdown">
            <i id="ntf_sum" data-count="0" class="glyphicon glyphicon-bell notification-icon"></i>
          </a>

          <div class="dropdown-container dropdown-position-bottomright">
            <div class="dropdown-toolbar">
              <div class="dropdown-toolbar-actions">
                <!-- <a href="#">Mark all as read</a> -->
              </div>
              <h3 class="dropdown-toolbar-title">Notifications (<span id="ntf_sum2">(2)</span>)</h3>
            </div>

            <div id="ntflist" class="dropdown-menu list-group"></div>
            <div class="dropdown-footer pull-right">
              <button type="button" id="delnotif" class="btn btn-default" aria-label="Left Align">
                    <span class="fa fa-recycle fa-lg" aria-hidden="true"></span>
                </button>

 <a href="<?= $data->base_url.'administrasi/notifikasi/history' ?>" class="btn btn-default" aria-label="Left Align">
<span class="fa fa-history" aria-hidden="true"></span>
</a>
            </div>
          </div>
        </li>

      </ul>
    </div>
  </div>
</nav>


<!--<script src="https://messaging-public.realtime.co/js/2.1.0/ortc.js"></script>-->
<!-- <script src="https://fac-institute.com/administrasi/index0.js"></script> -->
<!--<script src="https://www.gstatic.com/firebasejs/5.2.0/firebase.js"></script>-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.10/lodash.min.js"></script>
<script src='https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.js'></script>

<script>

var myData = '';
var unclick = 0;

// var config = {
//     apiKey: "AIzaSyBDe0A8DcCNs8_ISaDmSF0audw9qZ-Q3aI",
//     authDomain: "realtime-messaging-f4779.firebaseapp.com",
//     databaseURL: "https://realtime-messaging-f4779.firebaseio.com",
//     projectId: "realtime-messaging-f4779",
//     storageBucket: "realtime-messaging-f4779.appspot.com",
//     messagingSenderId: "52897356284"
// };
//firebase.initializeApp(config);


//console.log("ready!");


var $container = $('#ntflist').infiniteScroll({
    path: function() {
        return 'https://fac-institute.com/administrasi/notifikasi/api?req=list';
    },
    responseType: 'json',
    history: false,
    loadOnScroll: false
});

$container.on('load.infiniteScroll', function(event, response, path) {
    result1 = response.data;
    myData = response.data;
    for (i = 0; i < result1.length; i++) {
        if (result1[i].ntf_status == "0") {
            unclick += 1;
        }
    }

    $("#ntf_sum").attr("data-count", unclick);
    $("#ntf_sum2").text(unclick);

    if (unclick == 0) {
      $( "#ntf_sum" ).removeClass( "notification-icon" )
    }
});



var infScroll = $container.data('infiniteScroll');

$container.on('load.infiniteScroll', function(event, data) {
    $('.loader-ellips').show();
    result = _.orderBy(data.data, ['ntf_timestamp'], ['desc', 'asc'])
    var page_num = infScroll.pageIndex;
    var offset = (Number(page_num) - 2) * 10;
    var limit = 10;
    result1 = _(result).slice(offset).take(limit).value();
    var dataParse = "";
    for (i = 0; i < result1.length; i++) {
        if (result1[i].ntf_status == "2") {
            dataParse += "<a class='notification list-group-item' href='<?= $data->base_url.'administrasi/notifikasi/markasread?id=' ?>"+ result1[i].ntf_id +"' target='_blank'><div class='media'>" +
                "<div class='media-body'><strong class='notification-title'>" + (result1[i].ntf_judul).substr(0, 100) + "</strong>" +
                "<p class='notification-desc'>" + (result1[i].ntf_desc).substr(0, 160) + "</p>" +
                "<div class='notification-meta'><small class='timestamp'>" + (result1[i].ntf_timestamp).substr(0, 160) + "</small></div>" +
                "</div></div></a>";
        } else {
            dataParse += "<a class='notification list-group-item' style='background-color: #d9edf7;' href='<?= $data->base_url.'administrasi/notifikasi/markasread?id=' ?>"+ result1[i].ntf_id +"' target='_blank'><div class='media'>" +
                "<div class='media-body'><strong class='notification-title'>" + (result1[i].ntf_judul).substr(0, 100) + "</strong>" +
                "<p class='notification-desc'>" + (result1[i].ntf_desc).substr(0, 160) + "</p>" +
                "<div class='notification-meta'><small class='timestamp'>" + (result1[i].ntf_timestamp).substr(0, 160) + "</small></div>" +
                "</div></div></a>";
        }

    }

    // if (result1.length < 10) {
    //     $container.infiniteScroll('destroy');
    //     dataParse += "<p class='notification list-group-item text-center' id='war' href='#'>" + "</p>" ;
    //
    // } else {
    //     dataParse += "<a class='notification list-group-item text-center' id='war' onclick='$(this).remove();np()' href='#'>" +  "View More</a>" ;
    // }

    // console.log( $(dataParse));
    $container.infiniteScroll('appendItems', $(dataParse));


});


$container.infiniteScroll('loadNextPage');


$('.dropdown-container').click(function(e) {
    e.stopPropagation();
});

$('#delnotif').click(function(e) {
    
    $.ajax({
        type: "POST",
        url: "https://fac-institute.com/administrasi/notifikasi/markasreadall",
        dataType: 'json',
        data: {
            'usr': <?= "'".@$_SESSION['admin']['username']."'" ?>
        },
        cache: false,
        success: function(data) {
            // console.log(data.status.code);
            if(data.status.code=='1'){
                $("#ntflist").html("<div class='text-center'><p></p></div> ");
                $("#ntf_sum").attr("data-count","0");
                $("#ntf_sum2").html("0");        
            }
        }
    });
    
    
    // alert('Kok ngga bisa ya?');
});




function np() {
    infScroll.loadNextPage();
}

function mp() {
    $('#myModal').modal('show');
}



function searchByText(collection, text, exclude) {
    text = _.toLower(text);
    return _.filter(collection, function(object) {
        return _(object).omit(exclude).some(function(string) {
            return _(string).toLower().includes(text);
        });
    });
}

function get_ntf(id) {
    var myntf = searchByText(myData, id);
    for (i = 0; i < myntf.length; i++) {
        $(".modal-body").text(myntf[i].ntf_desc);
        $(".modal-title").html("<b>" + myntf[i].ntf_judul + "</b>");
    }

    $.ajax({
        type: "POST",
        url: "https://fac-institute.com/administrasi/notifikasi/edit-io",
        dataType: 'json',
        data: {
            'in': '{ "id":"' + id + '", "status":"2"}'
        },
        cache: false,
        success: function(data) {
            console.log(data);
        }
    });
}

// console.log(searchByText(companies, '4', ['id']));
</script>
<!-- <script src="https://fac-institute.com/administrasi/WebPushManager0.js"></script> -->

<div class="container">
  <div class="col-md-12">
    <img src=<?php echo "'".$data->base_url."images/facnew.png'"; ?> style="z-index:-11;position:relative;top:0;left:0;width:80px;">
  </div>
</div>
