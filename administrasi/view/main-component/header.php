<head>
<meta charset='utf-8'>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="<?= $data->base_url.'images/facnew.png' ?>">
	<?php if (isset($data->judul)&&!empty($data->judul)): ?>
			<title><?php echo $data->judul ?></title>
		<?php else: ?>
			<title>FAC Institute</title>
	<?php endif ?>

	<link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>
  <link rel="stylesheet" type="text/css" href=<?php echo "'".$data->base_url."css/administrasi-index.css'"; ?>>

  <?php if ($data->page=='laporan'): ?>
  <link rel="stylesheet" type="text/css" href=<?php echo "'".$data->base_url."css/laporan.css'"; ?>>
  <link rel="stylesheet" type="text/css" href=<?php echo "'".$data->base_url."css/multiselect/css/bootstrap-multiselect.css'"; ?>>
  <?php endif ?>

  <?php if ($data->page=='coba'): ?>
  <link rel="stylesheet" type="text/css" href=<?php echo "'".$data->base_url."css/agenda.css'"; ?>>
  <?php endif ?>

  <?php if ($data->page=='dataLaporan'): ?>
  <link rel="stylesheet" type="text/css" href=<?php echo "'".$data->base_url."css/data-laporan.css'"; ?>>
  <?php endif ?>

  <?php DatePicker(@$data->datepicker,$data->base_url) ?>
  <?php MultiSelect(@$data->multiselect,$data) ?>

  <?php if ($data->page=='absensi'): ?>
  	<link rel="stylesheet" type="text/css" href="<?php echo $data->base_url.'css/profilecard/style.css' ?>">
  <?php endif ?>


  <?php if ($data->page=='daftarHadir'): ?>
  	<link rel="stylesheet" type="text/css" href=<?php echo "'".$data->base_url."css/multiselect/css/bootstrap-multiselect.css'"; ?>>
  <?php endif ?>

  <?php if ($data->page=='tambah-invoice'||$data->page=='penawaran'||$data->page=='new-transaksi-tanggal'||$data->page=='data-invoice'||$data->page=='data-transaksi'): ?>
  	<link rel="stylesheet" type="text/css" href=<?php echo "'".$data->base_url."js/jquery-datepicker/jquery-ui.min.css'"; ?>>
  <?php endif ?>

  <?php if ($data->page=='orderTraining'||$data->page=='orderNew'): ?>
  	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCUn6Q6sg0JVJBlgiz4tL2kF1zmVoC0ScE&libraries=places&sensor=true"></script>
  <?php endif ?>

  <?php if ($data->page=='orderNew'): ?>
  	<link rel="stylesheet" type="text/css" href=<?php echo "'".$data->base_url."css/style-order.css'"; ?>>
  <?php endif ?>

  <?php if (@$data->boxsquare='1'): ?>
  	<link rel="stylesheet" type="text/css" href="<?= $data->base_url ?>assets/bootstrap/squarebox/style.css">
  <?php endif ?>

  <?php if ($data->page=='upload'): ?>
  		<link href=<?php echo "'".$data->base_url."assets/upload/css/fileinput.css'"; ?> media="all" rel="stylesheet" type="text/css" />
  <?php endif ?>


	<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'></script>
	<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>


	<?php if ($data->page=='upload'): ?>
		<script src=<?php echo "'".$data->base_url."assets/upload/js/fileinput.js'"; ?> type="text/javascript"></script>
        <script src=<?php echo "'".$data->base_url."assets/upload/js/fileinput_locale_fr.js'"; ?> type="text/javascript"></script>
        <script src=<?php echo "'".$data->base_url."assets/upload/js/fileinput_locale_es.js'"; ?> type="text/javascript"></script>
	<?php endif ?>

	<?php if ($data->page=='daftarHadir'): ?>
		<script type="text/javascript" src=<?php echo "'".$data->base_url."css/multiselect/js/bootstrap-multiselect.js'" ?>></script>
	<?php endif ?>

	<?php if ($data->page=='dataPegawai'): ?>
		<link rel="stylesheet" type="text/css" href="<?= $data->base_url.'css/datepicker/css/datepicker.css' ?>"> 
	<?php endif ?>

	<script>
	function openNav() {
	    document.getElementById("mySidenav").style.width = "250px";
	}

	function closeNav() {
	    document.getElementById("mySidenav").style.width = "0";
	}
	</script>
	<?php
		if ($data->page=='absensi'||$data->page=='daftarHadir') {
	?>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCUn6Q6sg0JVJBlgiz4tL2kF1zmVoC0ScE&libraries=places"></script>
	<?php
		}
	?>

	<link rel="stylesheet" href="<?= $data->base_url.'assets/bootstrap/skywalk-docs.min.css' ?>">
	<link rel="stylesheet" href="<?= $data->base_url.'assets/bootstrap/bootstrap-notifications.css' ?>">
	<style type="text/css">
		.tengah{
			text-align: center;
		}

		.ketengah{
			vertical-align: middle;
		}

		#isi{
			margin-top:0px;
			margin-bottom:40px;
		}
		.geser{
			padding-left: 25px;
			width: 100%;
		}
	</style>
</head>
<?php
function MultiSelect($value,$data){ ?>
	<?php if ($value=='1'): ?>
    	<link rel="stylesheet" type="text/css" href="<?= $data->base_url ?>css/multiselect/css/bootstrap-multiselect.css">
	<?php endif ?>
<?php } ?>

<?php
function DatePicker($value,$baseurl){ ?>
	<?php if ($value=='1'): ?>
		<link rel="stylesheet" type="text/css" href=<?php echo "'".$baseurl."js/jquery-datepicker/jquery-ui.min.css'"; ?>>
	<?php endif ?>
<?php } ?>
