<!DOCTYPE html>
<html>


<?php include_once 'view/main-component/header.php'; ?>

<body>



<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->subtitle ?></h3>
	</div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th class="tengah">No Quot</th>
                        <th class="tengah">Transaksi</th>
                        <th class="tengah">Nama Perusahaan</th>
                        <th class="tengah">Nama Customer</th>
                        <th class="tengah">Tanggal</th>
                        <th class="tengah">Items</th>
                        <th class="tengah">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php Rows($data->list) ?>
                </tbody>
            </table>
        </div>
    </div>
	<div class="row">

    </div>

</div><!--end container-->
</body>
</html>


<?php function Rows($data){ ?>
    <?php if (count($data)=='0'): ?>
        <tr>
            <td colspan="7">Tidak ada data yang masuk ke dalam sistem kami atau sistem sedang dalam perbaikan.</td>
        </tr>
    <?php else: ?>
        <?php foreach ($data as $key) { ?>
            <?php GetRowColor($key->qtn_status) ?>
                <td style="vertical-align: middle;"><?php echo $key->qtn_id ?></td>
                <td style="vertical-align: middle;"><?php echo $key->qtn_id_trans ?></td>
                <td style="vertical-align: middle;"><?php echo $key->nama_usaha ?></td>
                <td style="vertical-align: middle;"><?php echo $key->nama_cust ?></td>
                <td style="vertical-align: middle;"><?php echo $key->qtn_date ?></td>
                <td style="vertical-align: middle;">
                    <?php $items = explode(' # ', $key->items) ?>
                    <?php if ($key->items!=''): ?>
                    <ul style="padding-left:15px">
                    <?php foreach ($items as $keyz) {
                        echo "<li>".$keyz."</li>";
                    }  ?>
                    </ul>
                    <?php else: ?>
                        <span style="color:red">Not Available</span>
                    <?php endif ?>
                    
                </td>
                <td style="vertical-align: middle;">
                    <a href="new-perusahaan-info?tr=<?php echo $key->qtn_id_trans ?>">Edit Transaksi</a>
                    ||
                    <a href="new-quotation?edt=<?php echo $key->qtn_id ?>">Edit Invoice</a>
                    ||
                    <a href="new-preview?equot=<?php echo $key->qtn_id ?>">Prev</a>
                    ||
                    <a href="new-preview?quot=<?php echo $key->qtn_id ?>">PDF</a>
                    ||
                    <a href="new-quotation?send=<?php echo $key->qtn_id ?>">Kirim</a>
                </td>
            </tr>
        <?php } ?>
    <?php endif ?>
<?php } ?>

<?php 
function GetPreviewLink($value){
    if ($value=='1') {
        return 'einv=';
    }elseif ($value=='2') {
        return 'asinv=';
    }elseif ($value=='3') {
        return 'kinv=';
    }else{
        return 'einv=';
    }
}

function GetRowColor($value){
    if ($value=='0') {
        echo "<tr class='danger'>";
    }elseif ($value=='1') {
        echo "<tr class='success'>";
    }else{
        echo "<tr>";
    }
}
?>


