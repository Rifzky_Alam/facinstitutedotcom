<!DOCTYPE html>
<html>
<!-- <?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?> -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.10/lodash.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet"/>
<link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>
<link rel="stylesheet" type="text/css" href=<?php echo "'".$data->base_url."css/administrasi-index.css'"; ?>>
<link href=<?php echo "'".$data->base_url."/administrasi/view/quotation/dist/easy-autocomplete.min.css'"?> rel="stylesheet">
<link href=<?php echo "'".$data->base_url."/administrasi/view/quotation/dist/easy-autocomplete.themes.min.css'"?> rel="stylesheet">
<style>
body{
  font-family: 'Quicksand', sans-serif;
}
.btn {
    /* padding:8px;
    background:#ffffff; */
    margin-right :4px;
    margin-bottom:4px;
}
/* .icon-btn {
    padding: 1px 15px 3px 2px;
    border-radius:50px;
    font-size: 10px;
} */
.btn-circle {
  width: 30px;
  height: 30px;
  text-align: center;
  padding: 6px 0;
  font-size: 12px;
  line-height: 1.428571429;
  border-radius: 15px;
}


/* loader-ellips
------------------------- */

.loader-ellips {
  font-size: 20px;
  position: relative;
  width: 4em;
  height: 1em;
  margin: 10px auto;
}

.loader-ellips__dot {
  display: block;
  width: 1em;
  height: 1em;
  border-radius: 0.5em;
  background: #555;
  position: absolute;
  animation-duration: 0.5s;
  animation-timing-function: ease;
  animation-iteration-count: infinite;
}

.loader-ellips__dot:nth-child(1),
.loader-ellips__dot:nth-child(2) {
  left: 0;
}
.loader-ellips__dot:nth-child(3) { left: 1.5em; }
.loader-ellips__dot:nth-child(4) { left: 3em; }

@keyframes reveal {
  from { transform: scale(0.001); }
  to { transform: scale(1); }
}

@keyframes slide {
  to { transform: translateX(1.5em) }
}

.loader-ellips__dot:nth-child(1) {
  animation-name: reveal;
}

.loader-ellips__dot:nth-child(2),
.loader-ellips__dot:nth-child(3) {
  animation-name: slide;
}

.loader-ellips__dot:nth-child(4) {
  animation-name: reveal;
  animation-direction: reverse;
}

/* loader-wheel
------------------------- */

.loader-wheel {
  font-size: 64px; /* change size here */
  position: relative;
  height: 1em;
  width: 1em;
  padding-left: 0.45em;
  overflow: hidden;
  margin: 0 auto;
  animation: loader-wheel-rotate 0.5s steps(12) infinite;
}

.loader-wheel i {
  display: block;
  position: absolute;
  height: 0.3em;
  width: 0.1em;
  border-radius: 0.05em;
  background: #333; /* change color here */
  opacity: 0.8;
  transform: rotate(-30deg);
  transform-origin: center 0.5em;
}

@keyframes loader-wheel-rotate {
  from { transform: rotate(0deg); }
  to { transform: rotate(360deg); }
}

/* search */
.modal {
 text-align: center;
 padding: 0!important;
}

.modal:before {
 content: '';
 display: inline-block;
 height: 100%;
 vertical-align: middle;
 margin-right: -4px;
}

.modal-dialog {
 display: inline-block;
 text-align: left;
 vertical-align: middle;
}

.modal-xl {
  width: 90%;
  margin: auto;
}


</style>
<body onload="">
<?php
// include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
// include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>
 <div class="container" id="isi">
 	<div class="page-header" id="top-logo">
 		<h3><?php echo $data->subtitle ?></h3>
 	</div>
 	<div class="modal fade" id="myModal" role="dialog">
 		<div class="modal-dialog modal-xl">
 			<div class="modal-content">
 				<div class="modal-header">
 					<button type="button" class="close" data-dismiss="modal">&times;</button>
 					<h4 class="modal-title">Hasil Pencarian</h4>
 				</div>
 				<div class="modal-body">
 					<table class="table table-bordered">
 						<thead>
 							<tr>
 								<th>No Penawaran</th>
 								<th>Nama Customer</th>
 								<th>Nama Usaha</th>
 								<th>Items</th>
 								<th>Tanggal</th>
 								<th>Aksi</th>
 								<th>Kirim</th>
 							</tr>
 						</thead>
 						<tbody id="tResult">
 						</tbody>
 					</table>
 				</div>
 			</div>
 			<div class="modal-footer">
 				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
 			</div>
 		</div>
 	</div>
 	<div class="panel-group">
 		<div class="panel panel-default">
 			<div class="panel-heading">
 				<h4 class="panel-title">
 					<a data-toggle="collapse" href="#collapse1">Pencarian...</a>
 				</h4>
 			</div>
 			<div id="collapse1" class="panel-collapse collapse">
 				<div class="panel-body">
 					<form class="form-horizontal" role="form">
 						<div class="form-group">
 							<div class="col-md-12">
 								<label class="col-md-1 control-label">Nama Customer</label>
 								<div class="col-md-4">
 									<input type="text" class="form-control" style="width:270px" id="namelist" placeholder="Nama">
 								</div>
 							</div>
 						</div>
 						<div class="form-group">
 							<div class="col-md-12">
 								<label class="col-md-1 control-label">Nama Perusahaan</label>
 								<div class="col-md-4">
 									<input type="text" class="form-control" style="width:270px" id="companylist" placeholder="Perusahaan">
 								</div>
 							</div>
 						</div>
 						<div class="form-group">
 							<span class="col-md-1 control-label" style="font-weight:bold">Tanggal</span>
 							<div class="col-md-11">
 								<div class="col-md-3">
 									<input type="date" class="form-control" id="datelist1" placeholder="Tanggal">
 								</div>
 								<label for="" class="col-md-1 control-label" style="width:7.9px;">s/d</label>
 								<div class="col-md-3">
 									<input type="date" class="form-control" id="datelist2" placeholder="Tanggal">
 								</div>
 							</div>
 						</div>
 						<div class="form-group">
 							<label class="col-md-1 control-label">Items</label>
 							<div class="col-md-11">
 								<div class="col-md-4">
 									<input type="text" class="form-control" style="width:270px" id="itemlist" placeholder="Item">
 								</div>
 								<div class="col-md-2">
 									<button onclick="search()" type="button" class="btn btn-success">Cari <span class="glyphicon glyphicon-search"></span></button>
 								</div>
 							</div>
 						</div>
 					</form>
 				</div>
 				<!-- <div class="panel-footer">Panel Footer</div> -->
 			</div>
 		</div>
 	</div>
 	<hr>
 	<div class="table-responsive">
 		<table class="table table-bordered">
 			<thead>
 				<tr>
 					<th>No Penawaran</th>
 					<th>Nama Customer</th>
 					<th>Nama Usaha</th>
 					<th>Items</th>
 					<th>Tanggal</th>
 					<th>Aksi</th>
 					<th>Kirim</th>
 				</tr>
 			</thead>
 			<tbody>
 			</tbody>
 		</table>
 	</div>
 	<div class="page-load-status">
 		<div class="loader-ellips infinite-scroll-request">
 			<span class="loader-ellips__dot"></span>
 			<span class="loader-ellips__dot"></span>
 			<span class="loader-ellips__dot"></span>
 			<span class="loader-ellips__dot"></span>
 		</div>
 		<div class="infinite-scroll-last w3-container w3-margin w3-pale-yellow" style="display:none">
 			<p>End of content</p>
 		</div>
 		<p class="infinite-scroll-error"></p>
 	</div>
 </div>



<!-- <script type="text/javascript" src="<?= $data->base_url ?>js/jquery-ui/jquery-ui.js"></script> -->
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'></script>
<script src='https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.js'></script>
<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>
<script src=<?php echo "'".$data->base_url."/administrasi/view/quotation/dist/jquery.easy-autocomplete.min.js'"?>></script>
<script>
var $container = $('tbody').infiniteScroll({
    path: function() {
        return 'https://fac-institute.com/administrasi/quotation/api?req=table';
    },
    responseType: 'text',
    status: '.scroll-status',
    history: false,
});

var infScroll = $container.data('infiniteScroll');

$container.on('load.infiniteScroll', function(event, response) {
    var data = JSON.parse(response);
    var page_num = infScroll.pageIndex;
    var offset = (Number(page_num) - 2) * 10;
    var limit = 10;
    result1 = _(data.data).slice(offset).take(limit).value();
    var fuk = "";
    for (i = 0; i < result1.length; i++) {
        if (result1[i].inv_no_invoice == null) {
            fuk += "<tr class='danger'>" +
                "<td style='font-style: italic'>" + result1[i].qtn_id + "</td>" +
                "<td>" + result1[i].nama_cust + "</td>" +
                "<td>" + result1[i].nama_usaha + "</td>" +
                "<td style='width:300px;word-wrap: break-word'>" + myFunction(_.split(result1[i].items, '##')) + "</td>" +
                "<td>" + result1[i].qtn_date + "</td>" +
                "<td>" +
                "<a class='btn btn-info btn-xs' target='_blank' href='https://fac-institute.com/administrasi/new-preview?equot=" + result1[i].qtn_id + "'>View <span class='glyphicon glyphicon-eye-open'></span></a>" +
                "<a class='btn btn-success btn-xs' target='_blank' href='https://fac-institute.com/administrasi/new-quotation?edt=" + result1[i].qtn_id + "'>Edit <span class='glyphicon glyphicon-edit'></span></a>" +
                "<a class='btn btn-danger btn-xs' target='_blank' href='https://fac-institute.com/administrasi/new-preview?quot=" + result1[i].qtn_id + "'>PDF <span class='glyphicon glyphicon-file'></span></a>" +
                "</td>" +
                "<td><a target='_blank' href='https://fac-institute.com/administrasi/new-attachments?quot=" + result1[i].qtn_id + "' class='btn btn-primary btn-circle'><i class='glyphicon glyphicon-send'></i></a></td></tr>";
        } else {
            fuk += "<tr class='success'>" +
                "<td style='font-style: italic'>" + result1[i].qtn_id + "</td>" +
                "<td>" + result1[i].nama_cust + "</td>" +
                "<td>" + result1[i].nama_usaha + "</td>" +
                "<td style='width:300px;word-wrap: break-word'>" + myFunction(_.split(result1[i].items, '##')) + "</td>" +
                "<td>" + result1[i].qtn_date + "</td>" +
                "<td>" +
                "<a class='btn btn-info btn-xs' target='_blank' href='https://fac-institute.com/administrasi/new-preview?equot=" + result1[i].qtn_id + "'>View <span class='glyphicon glyphicon-eye-open'></span></a>" +
                "<a class='btn btn-success btn-xs' target='_blank' href='https://fac-institute.com/administrasi/new-quotation?edt=" + result1[i].qtn_id + "'>Edit <span class='glyphicon glyphicon-edit'></span></a>" +
                "<a class='btn btn-danger btn-xs' target='_blank' href='https://fac-institute.com/administrasi/new-preview?quot=" + result1[i].qtn_id + "'>PDF <span class='glyphicon glyphicon-file'></span></a>" +
                "</td>" +
                "<td><button type='button' class='btn btn-primary btn-circle'><i class='glyphicon glyphicon-send'></i></button></td></tr>";
        }
    }

    if (result1.length == 0) {
        $('.loader-ellips').hide();
        $('.infinite-scroll-last').show();
    }

    $container.infiniteScroll('appendItems', $(fuk));
});


function myFunction(name) {
    myText = ""
    for (var i = 0; i < name.length; i++) {
        myText += "&#9679;" + " " + name[i] + '<br>';
    }
    return myText;
}

$container.infiniteScroll('loadNextPage');



var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};


function search() {

    var xq = "";
    if ($('#datelist1').val() == "" ) {
        xq = {
            'src': '{"id":"","nama_cust":"' + $('#namelist').val() + '","nama_usaha":"' + $('#companylist').val() + '","tgl":[],"items":"' + $('#itemlist').val() + '"}'
        }
    } else {
        xq = {
            'src': '{"id":"","nama_cust":"' + $('#namelist').val() + '","nama_usaha":"' + $('#companylist').val() + '","tgl":["' + $('#datelist1').val() + '","' + $('#datelist2').val() + '"],"items":"' + $('#itemlist').val() + '"}'
        }
    }

    $.ajax({
        type: "POST",
        url: "https://fac-institute.com/administrasi/quotation/api/search",
        data: xq,
        cache: false,
        success: function(result) {
            // console.log("ok");
            // console.log(result);
            console.log(result.data.length);

            if (result.data.length == 0) {
              alert("Data Tidak Ditemukan")
            }

            else {
              var result1 = result.data;
              document.getElementById("tResult").innerHTML = "";
              $('#myModal').modal('show');
              for (i = 0; i < result1.length; i++) {
                  if (result1[i].inv_no_invoice == null) {
                      document.getElementById("tResult").innerHTML += "<tr class='danger'>" +
                          "<td style='font-style: italic'>" + result1[i].qtn_id + "</td>" +
                          "<td>" + result1[i].nama_cust + "</td>" +
                          "<td>" + result1[i].nama_usaha + "</td>" +
                          "<td style='width:300px;word-wrap: break-word'>" + myFunction(_.split(result1[i].items, '##')) + "</td>" +
                          "<td>" + result1[i].qtn_date + "</td>" +
                          "<td>" +
                          "<a class='btn btn-info btn-xs' target='_blank' href='https://fac-institute.com/administrasi/new-preview?equot=" + result1[i].qtn_id + "'>View <span class='glyphicon glyphicon-eye-open'></span></a>" +
                          "<a class='btn btn-success btn-xs' target='_blank' href='https://fac-institute.com/administrasi/new-quotation?edt=" + result1[i].qtn_id + "'>Edit <span class='glyphicon glyphicon-edit'></span></a>" +
                          "<a class='btn btn-danger btn-xs' target='_blank' href='https://fac-institute.com/administrasi/new-preview?quot=" + result1[i].qtn_id + "'>PDF <span class='glyphicon glyphicon-file'></span></a>" +
                          "</td>" +
                          "<td><a target='_blank' href='https://fac-institute.com/administrasi/new-attachments?quot=" + result1[i].qtn_id + "' class='btn btn-primary btn-circle'><i class='glyphicon glyphicon-send'></i></a></td></tr>";
                  } else {
                      document.getElementById("tResult").innerHTML += "<tr class='success'>" +
                          "<td style='font-style: italic'>" + result1[i].qtn_id + "</td>" +
                          "<td>" + result1[i].nama_cust + "</td>" +
                          "<td>" + result1[i].nama_usaha + "</td>" +
                          "<td style='width:300px;word-wrap: break-word'>" + myFunction(_.split(result1[i].items, '##')) + "</td>" +
                          "<td>" + result1[i].qtn_date + "</td>" +
                          "<td>" +
                          "<a class='btn btn-info btn-xs' target='_blank' href='https://fac-institute.com/administrasi/new-preview?equot=" + result1[i].qtn_id + "'>View <span class='glyphicon glyphicon-eye-open'></span></a>" +
                          "<a class='btn btn-success btn-xs' target='_blank' href='https://fac-institute.com/administrasi/new-quotation?edt=" + result1[i].qtn_id + "'>Edit <span class='glyphicon glyphicon-edit'></span></a>" +
                          "<a class='btn btn-danger btn-xs' target='_blank' href='https://fac-institute.com/administrasi/new-preview?quot=" + result1[i].qtn_id + "'>PDF <span class='glyphicon glyphicon-file'></span></a>" +
                          "</td>" +
                          "<td><button type='button' class='btn btn-primary btn-circle'><i class='glyphicon glyphicon-send'></i></button></td></tr>";
                  }
              }
            }

        }
    });
}

$.ajax({
    type: "POST",
    url: "https://fac-institute.com/administrasi/quotation/api?req=table",
    cache: false,
    success: function(data) {

        myItem = [];

        for (var i = 0; i < data.data.length; i++) {
            myItem.push(String(data.data[i].items));
        }

        var itemsq = (_.uniqWith(myItem, _.isEqual));
        // console.log(_.uniqWith(myItem, _.isEqual));

        var option1 = {
            data: data.data,
            getValue: "nama_cust",
            list: {
                match: {
                    enabled: true
                },
                showAnimation: {
                    type: "fade",
                    time: 200,
                    callback: function() {}
                },
                hideAnimation: {
                    type: "slide",
                    time: 400,
                    callback: function() {}
                }
            }
        };

        var option2 = {
            data: data.data,
            getValue: "nama_usaha",
            list: {
                match: {
                    enabled: true
                },
                showAnimation: {
                    type: "fade",
                    time: 200,
                    callback: function() {}
                },
                hideAnimation: {
                    type: "slide",
                    time: 400,
                    callback: function() {}
                }
            }
        };

        var option3 = {
            data: itemsq,
            list: {
                maxNumberOfElements: 99,
                match: {
                    enabled: true
                },
                sort: {
                    enabled: true
                }
            }
        };
        $("#namelist").easyAutocomplete(option1);
        $("#companylist").easyAutocomplete(option2);
        $("#itemlist").easyAutocomplete(option3);
    }
});
</script>
<script>
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>
</body>
</html>
