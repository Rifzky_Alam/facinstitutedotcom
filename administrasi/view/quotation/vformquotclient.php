<html>
<head>

	<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?= $data->judul ?></title>

	<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
	<link rel="icon" href="https://fac-institute.com/images/facnew.png">
	
	<link rel="stylesheet" type="text/css" href=<?php echo "'".$data->base_url."css/datepicker/css/datepicker.css'"; ?>>
	<link rel="stylesheet" type="text/css" href=<?php echo "'".$data->base_url."css/multiselect/css/bootstrap-multiselect.css'"; ?>>
	<!-- <link rel="stylesheet" href=<?php echo "'".$data->base_url."css/styles.css"."'"; ?> />
	<link rel="stylesheet" type="text/css" href=<?php echo "'".$data->base_url."css/custom.css"."'"; ?>>
	<link rel="stylesheet" type="text/css" href=<?php echo "'".$data->base_url."css/set1.css"."'"; ?> /> -->


	<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/linearicons.css">
	<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/bootstrap.css">
	<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/magnific-popup.css">
	<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/nice-select.css">
	<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/animate.min.css">
	<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/owl.carousel.css">
	<link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/main.css">

	<style media="screen">
		.banner-content h1 {
		font-size: 40px;
		}
		.nav-menu {
		padding-top: 15px;
		}
		.nav-menu a {
		padding: 1px 8px 1px 8px;
		font-size: 16px;
		font-weight: 500;
		}
		.nav-menu ul li a {
		font-size: 14px;
		}
		.menu-active {
		border-bottom: 5px solid #df003a;
		}
		.navbar {
		padding: 0.2rem 1rem;
		}
	</style>
	<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">


	<style type="text/css">
	#top-gap{
		padding-top: 80px;
	}
	</style>
</head>

<body>
	<?php $page='orderan' ?>
	<div id='top-gap'></div>
	<div class='container'>

		<?php include_once $data->homedir.'view/homepage/header.homepage.php'; ?>

	<div class="page-header">
		<h2><?= $data->subtitle ?></h2>
		<p>By FAC-Institute</p>
	</div>

		<form id="myform" class="mx-2 my-auto d-inline w-100" action='' method='POST'>


            <?php foreach($data->itemtrans as $key): ?>
                
                <div class="row">
    			    <div class="col-md-6">
    			        <div class="card">
                            <div class="card-header"><?= $key['nama_item'] ?></div>
                            <div class="card-body">
                                <table style="margin:0;padding:0" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                        <tr>
                                            <td>Jumlah/Kuantitas</td>
                                            <td style="padding:5px">:</td>
                                            <td>
                                                <input type="hidden" name="in[id][]" value="<?= $key['id'] ?>" />
                                                <input type="hidden" <?= "id='pkt".$key['id_item']."'" ?> value="<?= $key['total_hari'] ?>" />
                                                <input onkeyup="calculatepaket(this)" <?= "id='qty".$key['id_item']."'" ?>  type="text" name="in[qty][]" value="0" placeholder="Hanya angka" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Jumlah Hari</td>
                                            <td style="padding:5px">:</td>
                                            <td <?= "id='jmlhari".$key['id_item']."'" ?>>0</td>
                                            <input type="hidden" <?= "id='tltemp".$key['id_item']."'" ?> class="ttlz" value="0" />
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
    			    </div>
    			</div>    
                <br><br>
            <?php endforeach ?>

			<input type="hidden" id="tl" value="0"/>
                    
            <br>
            <hr>
            <p>Jumlah traning yang akan dilaksanakan adalah <span id="sptl">0</span> hari</p>
            <br>
		</form>
        <div class="col-md-10 mb-40">
				<button id="btnsubmit" class='btn btn-lg btn-primary' style="width:200px">Submit</button>
			</div>
		</div>
	</div>

	<?php include_once $data->homedir.'view/homepage/footer.homepage.php'; ?>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/jquery-2.2.4.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/bootstrap.min.js"></script>
			<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/easing.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/hoverIntent.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/superfish.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.ajaxchimp.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.magnific-popup.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/owl.carousel.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.sticky.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.nice-select.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/parallax.min.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/mail-script.js"></script>
			<script src="<?= $data->base_url ?>assets/homepage2019/js/main.js"></script>
			<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>


	<script src=<?php echo "'".$data->base_url."js/bootstrap.min.js'"; ?>></script>
	<script src=<?php echo "'".$data->base_url."js/custom.js"."'"; ?>></script>
	<script src=<?php echo "'".$data->base_url."js/slippry.min.js"."'"; ?>></script>
	<script type="text/javascript" src=<?php echo "'".$data->base_url."css/multiselect/js/bootstrap-multiselect.js'"; ?>></script>
	<script type="text/javascript" src=<?php echo "'".$data->base_url."css/datepicker/js/bootstrap-datepicker.js'"; ?>></script>
	<script type="text/javascript">

    $('#btnsubmit').click(function(){
        var jumlahtraining = parseInt($('#sptl').html());
        if(jumlahtraining==0){
            alert('Hari training tidak boleh kosong, pastikan anda telah memasukkan data jumlah dari item yang tertera pada penawaran diatas.');
            return;
        }
        $('#myform').submit();
    //   alert('Submit Form NOW!!'); 
    });

    function calculatepaket(obj){
        var k = obj.id;
        var aidi = k.substring(3,k.length);
        var paket = $('#pkt'+aidi).val();
        var total = paket * obj.value;
        var tot=0;
        $('#tltemp'+aidi).val(total);
        
        var arr = document.getElementsByClassName('ttlz');
        
        for(var i=0;i<arr.length;i++){
            if(parseInt(arr[i].value)){
                tot += parseInt(arr[i].value);
            }
        }
        // var tmpval = parseInt($('#tl').val());
        // var alltotal = parseInt(tmpval)+parseInt(total);
        // $('#tl').val(alltotal);
        
        // console.log("Paket: " + paket);
        // console.log("objvalue: "+obj.value);
        $('#jmlhari'+aidi).html(paket * obj.value);
        $('#sptl').html(tot);
    }

	function removeElement(id) {
	    $('#'+id).remove();
	}




	function getValue(){
	  var x=document.getElementById("example-getting-started");
	  for (var i = 0; i < x.options.length; i++) {
	     if(x.options[i].selected){
	          if (x.options[i].value=='lainnya') {
	          	$('#fr-accurateLainnya').show();
	          }else{
	          	$('#fr-accurateLainnya').hide();
	          };
	      }
	  }
	}

	</script>
</body>
</html>