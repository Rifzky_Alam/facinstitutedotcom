<?php 
function PenawaranCompare($data){
    return '<table style="margin:0;padding:0" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr style="background-color: #0066ff;">
                            <td style="color:white;margin:0 auto;font-size:22px" align="center" valign="middle" height="60"><strong>FAC-Institute</strong></td>
                        </tr>
                    </tbody>
                </table>            
            </td>
        </tr>
        <tr>
            <table  align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="padding-top:10px;">
            <tbody>
            <tr>
            <td style="width:50%"><center><img src="http://fac-institute.com/images/logoaccurate5.png" style="width:50%;height:60px"></center></td>
            <td style="width:50%"><center><img src="http://fac-institute.com/images/facnew.png" style="width:35%;height:70px"></center></td>
            </tr>
            </tbody>
            </table>
        </tr>
        <tr>
            <td valign="middle" height="60"><h4>Dear '.$data->nama_cust.',</h4></td>
        </tr>
        <tr>
            <td>
            '.HeaderContent($data->jenistr, $data->nama_usaha).'
            </td>
        </tr>
        
        <tr>
            <td><br>
            <table valign="top" align="center" border="0" style="padding-bottom:30;" bgcolor="#f9ffee" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                    <tr>
                        <td>
                            <table valign="top" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tbody>
                                    <tr><td><center><h3><strong>BIAYA</strong></h3></center></td></tr>
                                    <tr>
                                    <td style="margin-bottom:10px">
                                    <table border="1" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Keterangan</th>
                                        <th>Biaya</th>
                                        <th>Qty</th>
                                        <th>Jumlah</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    '.itemsToCompare($data->items).'
                                    </tbody>
                                    </table>
                                    </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>

            </table>
                
            </td>
        </tr>
        <tr>
            <br><p>Anda dapat memilih produk/item yang kami tawarkan dengan mengisi form atau membalas email ini (dengan format yang kami kirimkan pada lampiran email ini)</p>
            <a href="'.$data->base_url.'client/penawaran/'.$data->idtransaksi.'">Klik tautan/tulisan ini untuk mengisi form online</a>
            '.Notes($data->notes).'
            <td>
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                <tr><td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr style="background-color: #660568;">
                            <td style="color:white;margin:0 auto" align="center" valign="middle" height="40">PEMBAYARAN</td>
                        </tr>
                    </tbody>
                </table>
                <p>Harga diatas adalah tarif nett yang diterima oleh kami (tidak dipotong pph)<br>
                Kami bukan PKP(Pengusaha Kena Pajak), tidak ada faktur pajak. Juga tidak ada NPWP untuk bukti potong PPh 23.
                </p>
                '.syaratBayar('').'
                <ul type="square">
                    <li><strong>Bank BCA KCU Bekasi No Acc. 0663162851 a/n : Fajar Shodiq</strong></li>
                    <li><strong>Bank Mandiri KCP Bekasi Ahmad Yani No. Acc. 1670001377901 a/n : Fajar Shodiq</strong></li>
                </ul></td>
                </tr>
                </tbody>
                </table> 
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr style="background-color: #660000;">
                            <td style="color:white;margin:0 auto" align="center" valign="middle" height="40">SYARAT DAN KETENTUAN</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                '.syaratDanKetentuan($data->syarat).'
            </td>
        </tr>
        <tr>
            <td>
                <p>Demikianlah semoga menjadi maklum adanya. Terimakasih atas perhatian dan kerjasamanya.</p>
                <p>
                Jika ada hal-hal yang ingin ditanyakan, jangan sungkan untuk senantiasa menghubungi kami. Terimakasih, <br><br>Salam <br><br>
                </p>
                <p><strong>'.$data->petugas.'</strong> <br> <strong>Officer of Marketing Division</strong></p>
            </td>
        </tr>
        <tr>
            <td style="padding:0 0 0 0" align="center" bgcolor="#0066ff">
          <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:#0066ff">
            <tbody>
              <tr>
                <td>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr height="30">
                        <td align="center" width="100%" height="16"></td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr>
                        <td align="center" valign="top">
                          <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                              <tr>
                                <td align="left" width="20"></td>
                                <td align="left" valign="top">
                                  <table align="center" border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                      <tr height="15">
                                        <td align="left" height="15">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td style="line-height:18px" align="center" valign="top">
                                          <span style="font-family:'."'"."Open Sans"."'".',sans-serif;color:white;font-size:12px;text-align:center">Email ini dikirim oleh: <strong><a href="http://www.fac-institute.com" style="color:white">FAC-Institute</a></strong><br>
                                          Jl. Pahlawan Revolusi 10GG No.3, RT.9/RW.7, Pd. Bambu, Kec. Duren Sawit, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta Indonesia 13430<br>
                                          Telp. <a href="tel://+6281290083983" style="color:white">+6281290083983</a></span><br>
                                          <br>
                                            <a href="http://www.fac-institute.com" style="font-family:'."'"."Open Sans"."'".',sans-serif;font-size:22px;color:white">www.fac-institute.com</a><br><br>
                                            <p style="margin:0 0 1em 0;font-family:'."'"."Open Sans"."'".',sans-serif;font-size:16px;line-height:26px;color:white;font-weight:300;text-align:center">Konsultan Akuntansi dan Lembaga Pendidikan Komputerisasi Akuntansi</p>
                                        </td>
                                      </tr>
                                      <tr height="15">
                                        <td align="left" height="15">&nbsp;</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                                <td align="left" width="20"></td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr height="30">
                        <td align="left" width="100%" height="16"></td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
        </tr>
    </tbody>
</table>';
}


function Penawaran($data){
    return '<table style="margin:0;padding:0" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr style="background-color: #0066ff;">
                            <td style="color:white;margin:0 auto;font-size:22px" align="center" valign="middle" height="60"><strong>FAC-Institute</strong></td>
                        </tr>
                    </tbody>
                </table>            
            </td>
        </tr>
        <tr>
            <table  align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="padding-top:10px;">
            <tbody>
            <tr>
            <td style="width:50%"><center><img src="http://fac-institute.com/images/logoaccurate5.png" style="width:50%;height:60px"></center></td>
            <td style="width:50%"><center><img src="http://fac-institute.com/images/facnew.png" style="width:35%;height:70px"></center></td>
            </tr>
            </tbody>
            </table>
        </tr>
        <tr>
            <td valign="middle" height="60"><h4>Dear '.$data->nama_cust.',</h4></td>
        </tr>
        <tr>
            <td>
            '.HeaderContent($data->jenistr, $data->nama_usaha).'
            </td>
        </tr>
        
        <tr>
            <td><br>
            <table valign="top" align="center" border="0" style="padding-bottom:30;" bgcolor="#f9ffee" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                    <tr>
                        <td>
                            <table valign="top" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tbody>
                                    <tr><td><center><h3><strong>BIAYA</strong></h3></center></td></tr>
                                    <tr>
                                    <td style="margin-bottom:10px">
                                    <table border="1" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Keterangan</th>
                                        <th>Biaya</th>
                                        <th>Qty</th>
                                        <th>Jumlah</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    '.items($data->items).'
                                    </tbody>
                                    </table>
                                    </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>

            </table>
                
            </td>
        </tr>
        <tr>
            '.Notes($data->notes).'
            <td>
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                <tr><td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr style="background-color: #660568;">
                            <td style="color:white;margin:0 auto" align="center" valign="middle" height="40">PEMBAYARAN</td>
                        </tr>
                    </tbody>
                </table>
                <p>Harga diatas adalah tarif nett yang diterima oleh kami (tidak dipotong pph)<br>
                Kami bukan PKP(Pengusaha Kena Pajak), tidak ada faktur pajak. Juga tidak ada NPWP untuk bukti potong PPh 23.
                </p>
                '.syaratBayar('').'
                <ul type="square">
                    <li><strong>Bank BCA KCU Bekasi No Acc. 0663162851 a/n : Fajar Shodiq</strong></li>
                    <li><strong>Bank Mandiri KCP Bekasi Ahmad Yani No. Acc. 1670001377901 a/n : Fajar Shodiq</strong></li>
                </ul></td>
                </tr>
                </tbody>
                </table> 
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr style="background-color: #660000;">
                            <td style="color:white;margin:0 auto" align="center" valign="middle" height="40">SYARAT DAN KETENTUAN</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                '.syaratDanKetentuan($data->syarat).'
            </td>
        </tr>
        <tr>
            <td>
                <p>Demikianlah semoga menjadi maklum adanya. Terimakasih atas perhatian dan kerjasamanya.</p>
                <p>
                Jika ada hal-hal yang ingin ditanyakan, jangan sungkan untuk senantiasa menghubungi kami. Terimakasih, <br><br>Salam <br><br>
                </p>
                <p><strong>'.$data->petugas.'</strong> <br> <strong>Officer of Marketing Division</strong></p>
            </td>
        </tr>
        <tr>
            <td style="padding:0 0 0 0" align="center" bgcolor="#0066ff">
          <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:#0066ff">
            <tbody>
              <tr>
                <td>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr height="30">
                        <td align="center" width="100%" height="16"></td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr>
                        <td align="center" valign="top">
                          <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                              <tr>
                                <td align="left" width="20"></td>
                                <td align="left" valign="top">
                                  <table align="center" border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                      <tr height="15">
                                        <td align="left" height="15">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td style="line-height:18px" align="center" valign="top">
                                          <span style="font-family:'."'"."Open Sans"."'".',sans-serif;color:white;font-size:12px;text-align:center">Email ini dikirim oleh: <strong><a href="http://www.fac-institute.com" style="color:white">FAC-Institute</a></strong><br>
                                          Jl. Pahlawan Revolusi 10GG No.3, RT.9/RW.7, Pd. Bambu, Kec. Duren Sawit, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta Indonesia 13430<br>
                                          Telp. <a href="tel://+6281290083983" style="color:white">+6281290083983</a></span><br>
                                          <br>
                                            <a href="http://www.fac-institute.com" style="font-family:'."'"."Open Sans"."'".',sans-serif;font-size:22px;color:white">www.fac-institute.com</a><br><br>
                                            <p style="margin:0 0 1em 0;font-family:'."'"."Open Sans"."'".',sans-serif;font-size:16px;line-height:26px;color:white;font-weight:300;text-align:center">Konsultan Akuntansi dan Lembaga Pendidikan Komputerisasi Akuntansi</p>
                                        </td>
                                      </tr>
                                      <tr height="15">
                                        <td align="left" height="15">&nbsp;</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                                <td align="left" width="20"></td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr height="30">
                        <td align="left" width="100%" height="16"></td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
        </tr>
    </tbody>
</table>';
}

function Notes($value){
    if (count($value)=='0'||$value==''||$value=='-'||(count($value)<2&&$value[0]=='')) {
        return '';
    } else {
        $hasil = "<br><tr><td><h4 style='margin-bottom:0px;'>Catatan:</h4><ul>";
    
        foreach ($value as $key) {
            $hasil .= "<li>".$key."</li>";
        }

        $hasil .= "</ul></td></tr>";
        return $hasil;
    }
        
}

function itemsToCompare($data){
        $hasil = "";
        $totalamount = 0;
        if (count($data)=='0') {
            $hasil .= '<tr>
                            <td> - </td>
                            <td style="text-align:right"> 0 IDR</td>
                            <td style="text-align:center"> 0 </td>
                            <td style="text-align:right"> 0 IDR</td>
                           </tr>';
        } else {
            foreach ($data as $key ) {
                $totalprice = intval($key->harga)*intval($key->itm_qty);
                $totalamount += $totalprice;
                $hasil .= '<tr>
                            <td>'.$key->nama_item.'</td>
                            <td style="text-align:right">'.number_format($key->harga).' IDR</td>
                            <td style="text-align:center">...</td>
                            <td style="text-align:right">... IDR</td>
                           </tr>';
            }

            $hasil .= '<tr>
                        <td colspan="3"><strong>Total</strong></td>
                        <td style="text-align:right"><strong> ... IDR</strong></td>
                        </tr>';
        }

        return $hasil;
}

function items($data){
        $hasil = "";
        $totalamount = 0;
        if (count($data)=='0') {
            $hasil .= '<tr>
                            <td> - </td>
                            <td style="text-align:right"> 0 IDR</td>
                            <td style="text-align:center"> 0 </td>
                            <td style="text-align:right"> 0 IDR</td>
                           </tr>';
        } else {
            foreach ($data as $key ) {
                $totalprice = intval($key->harga)*intval($key->itm_qty);
                $totalamount += $totalprice;
                $hasil .= '<tr>
                            <td>'.$key->nama_item.'</td>
                            <td style="text-align:right">'.number_format($key->harga).' IDR</td>
                            <td style="text-align:center">'.$key->itm_qty.'</td>
                            <td style="text-align:right">'.number_format($totalprice).' IDR</td>
                           </tr>';
            }

            $hasil .= '<tr>
                        <td colspan="3"><strong>Total</strong></td>
                        <td style="text-align:right"><strong> '.number_format($totalamount).' IDR</strong></td>
                        </tr>';
        }

        return $hasil;
}

function syaratBayar($value){
    if ($value=='1') {
        return'
            <p>Pembayaran dimuka dilakukan sebelum training/jasa dilaksanakan dan pembayaran di bayarkan ke No Rekening di bawah ini.</p>
        ';
    }elseif($value=='2'){
        return'
            <p>Pembayaran dimuka 50% yaitu sebelum implementasi dilaksanakan dan pembayaran di bayarkan ke No Rekening di bawah ini.</p>
        ';
    }elseif($value=='3') {
        return '
        <p>Pembayaran dimuka 50% yaitu sebelum implementasi dilaksanakan dan pembayaran di bayarkan ke No Rekening di bawah ini.</p>
            ';
    }else {
    return'
    <p>Pembayaran dimuka dilakukan sebelum training/jasa dilaksanakan dan pembayaran di bayarkan ke No Rekening di bawah ini.</p>
    ';
    }
}

function syaratDanKetentuan($value){
        if (count($value)=='0'||$value==''||$value=='-') {
            return '
                <ol>
                    <li>Training dan implementasi bersifat time oriented, bukan result oriented.</li>
                    <li>Training dan implementasi berfokus pada kegiatan mentransfer <strong>How To UseACCURATE</strong>.</li>
                    <li>Training dan implementasi terdiri dari 1 implementator dengan waktu kerja maksimal 7 jam (termasuk 1 jam istirahat), yaitu jam 09.00 s/d 16.00 atau jam 10.00 s/d 17.00.</li>
                    <li>Jika lembur, mohon di print dan diisi form lembur terlebih dahulu yang telah kami lampirkan.</li>
                    <li>Menyediakan makan siang bagi implementator (Jabodetabek)</li>
                    <li>Implementasi diluar Jabodetabek, akomodasi perjalanan, makan dan penginapan ditanggung customer.</li>
                    <li>Kelebihan jam training akan dikenakan tarif overtime Rp 75.000.- per jam</li>
                    <li>Pembatalan atau perubahan jadwal implementasi oleh Customer harus disampaikan ke FAC paling lambat 2 hari (terhitung hari kerja senin-jumat diluar hari libur nasional) sebelum waktu pelaksanaan implementasi, apabila dilakukan 1 (satu) hari sebelumnya atau di hari pelaksanaan maka dikenakan denda Rp 250.000/hari. Selanjutnya waktu pelaksanaan akan direschedule kembali.</li>
                    <li>Jika anda pengguna awal atau pengguna lama tapi ingin membuat database baru, pada email ini kami lampirkan format excel untuk saldo awal meliputi daftar akun, daftar pelanggan, daftar pemasok, daftar barang dan daftar fix asset. Harap disiapkan sebelum hari training.</li>
                    <li>Training di hari sabtu, minggu dan hari libur nasional dikenakan biaya Rp 250.000.-</li>
                    <li>Untuk implementasi di Jabodetabek akomodasi perjalanan ditagihkan sesuai jarak dan area implementasi.</li>
        
                </ol>
                ';
        } else {
            $hasil = '<ol>';
            foreach ($value as $key) {
                $hasil .= '<li>'.$key.'</li>';
            }
            $hasil .= '</ol>';
            return $hasil;
        }
}

function HeaderContent($value,$namausaha){
    if ($value=='1') {
        return '<p>
                    Kami dari FAC Institute sebagai divisi onsite training Accurate. Terimakasih telah memilih Accurate sebagai program akuntansi di '.HCifNamausaha($namausaha).'.
                     Terimakasih pula atas order menggunakan jasa FAC Institute dalam mengimplementasikan Accurate.<br> 
                    Berikut ini kami kirimkan penawaran training Accurate : 
                </p>
                <br>';
    }elseif ($value=='2') {
        return '<p>
                    Kami dari <b>FAC Institute</b> sebagai divisi Accounting Service. Terimakasih telah memilih FAC Institute sebagai penyedia jasa accounting service di '.HCifNamausaha($namausaha).'. Kami selalu berusaha untuk memberikan pelayanan terbaik di setiap jasa yang kami berikan. <br>
                    Berikut ini kami kirimkan penawaran jasa yang kami berikan:
                </p>
                <br>';
    } elseif($value=='3') {
        return '<p>
                    Kami dari <b>FAC Institute & Bakat Cendekia</b>. Kami merupakan partner dari pihak Accurate Accounting Software, sebagai Accurate Authorized Training Center (AATC) yang secara resmi menyelenggarakan Pelatihan Accurate. Kami memberikan penawaran <b>TERBAIK</b> berupa: 
                </p>
                <br>';
    }
}

function HCifNamausaha($namausaha){
    if ($namausaha=='-') {
        return 'perusahaan anda';
    } else {
        return '<b>'.$namausaha.'</b>';
    }
    
}

 ?>