<!DOCTYPE html>
<html>


<?php include_once 'view/main-component/header.php'; ?>

<body>
<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
?>
<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->header.' ~ '.$data->id_transaksi ?></h3>
	</div>
	<div class="row">
     <form action=""  method="post">

        <div class="row">
            <div class="col-md-10">
                <h4>Perusahaan: </h4>
                <h3><?php echo $data->nama_usaha ?></h3>
                <?php if ($data->nama_usaha==''): ?>
                    <script type="text/javascript">alert('Nama perusahaan tidak ada, silahkan memilih nama perusahaan kembali!');</script>
                    <script type="text/javascript">location.replace('perusahaan');</script>
                    <?php else: ?>

                <?php endif ?>
            </div>
        </div>
        <br><br>

         <div class="row">
             <div class="col-md-10">
                 <div class="form-group">
                    <label>Subjek</label>
                    <input type="text" name="in[subjek]" class="form-control" placeholder="diisi hanya untuk tipe transaksi training accurate" maxlength="75">                     
                 </div>
             </div>
         </div>

         <div class="row">
             <div class="col-md-10">
                 <div class="form-group">
                    <label>Judul Quotation</label>
                    <input type="text" name="in[judul]" class="form-control" value="Penawaran Jasa Training Accurate" maxlength="75">                     
                 </div>
             </div>
         </div>

         <div class="row">
             <div class="col-md-10">
                <div class="form-group">
                    <label>Notes/Catatan</label>
                     <input type="text" class="form-control" name="in[notes]" placeholder="Pisahkan dengan ##, Contoh: Harap Bawa Laptop##Isi data Akun" maxlength="800" />
                </div>  
             </div>
         </div>

         <div class="row">
             <div class="col-md-10">
                <div class="form-group">
                    <label>Tanggal Penawaran</label>
                     <input type="text" class="form-control" value="<?php echo date('Y-m-d') ?>" name="in[tanggal]" id="tanggal-quotation" />
                </div>  
             </div>
         </div>

         <div class="row">
             <div class="col-md-10">
                <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>        
             </div>
         </div>
         
     </form>   
    </div>

</div><!--end container-->
<script type="text/javascript" src=<?php echo "'".$data->base_url."js/jquery-ui/jquery-ui.js'"; ?>></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#tanggal-quotation').datepicker({
            dateFormat: 'yy-mm-dd'
        });
    });
</script>
</body>
</html> 