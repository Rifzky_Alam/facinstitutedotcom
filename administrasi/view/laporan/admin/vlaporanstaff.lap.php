
<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<body>
<style type="text/css" media="screen">
    .paddingtd{
        padding:2px;
    }
</style>

<style>

.checkbox-menu li label {
display: block;
padding: 3px 10px;
clear: both;
font-weight: normal;
line-height: 1.42857143;
color: #333;
white-space: nowrap;
margin:0;
transition: background-color .4s ease;
}
.checkbox-menu li input {
/* margin: 0px 5px; */
top: 2px;
/* position: relative; */
}

.checkbox-menu li.active label {
background-color: #cbcbff;
font-weight:bold;
}

.checkbox-menu li label:hover,
.checkbox-menu li label:focus {
background-color: #f5f5f5;
}

.checkbox-menu li.active label:hover,
.checkbox-menu li.active label:focus {
background-color: #b8b8ff;
}




input[type="checkbox"], input[type="radio"]{
	position: absolute;
	right: 9000px;
}

/*Check box*/
input[type="checkbox"] + .label-text:before{
	content: "\f096";
	font-family: "FontAwesome";
	speak: none;
	font-style: normal;
	font-weight: normal;
	font-variant: normal;
	text-transform: none;
	line-height: 1;
	-webkit-font-smoothing:antialiased;
	width: 1em;
	display: inline-block;
	margin-right: 5px;
}

input[type="checkbox"]:checked + .label-text:before{
	content: "\f14a";
	color: #2980b9;
	animation: effect 250ms ease-in;
}

input[type="checkbox"]:disabled + .label-text{
	color: #aaa;
}

input[type="checkbox"]:disabled + .label-text:before{
	content: "\f0c8";
	color: #ccc;
}


@keyframes effect{
	0%{transform: scale(0);}
	25%{transform: scale(1.3);}
	75%{transform: scale(1.4);}
	100%{transform: scale(1);}
}


</style>




<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?= $data->headertext ?></h3>
	</div>

    <div class="row">
        <div class="col-md-12">
            <a href="#modal-cariz" data-toggle="modal" title="Search" class="btn btn-warning">Cari</a>
            <?php if ($data->enabledownload): ?>
                <a href="<?= $data->base_url.'administrasi/download-excel?data[username]='.$data->searchuser.'&data[tanggalAwal]='.$data->searchfromdate.'&data[tanggalAkhir]='.$data->searchuntildate ?>" title="Download" class="btn btn-primary">Download</a>
            <?php else: ?>
                <button class="btn btn-success" disabled="disabled">Menu Download Tersedia Setelah Pencarian</button>
            <?php endif ?>
                <a href="<?= $data->base_url.'administrasi/user/reimlist' ?>" title="Reimbursement Lists" class="btn btn-danger">Reimbursement</a>
        </div>
    </div>
    <br>
    <?php $totaltransport = 0; ?>
    <?php $totalparkir = 0; ?>
    <?php $totalmakansiang = 0; ?>
    <?php $totalmakanmalam = 0; ?>
    <?php $totalkesehatan = 0; ?>
    <?php $totalallowance = 0; ?>
    <?php $totalbiayalain = 0; ?>
    <?php $totalinsentifaccurate = 0; ?>
    <?php $totalinsentifharikerja = 0; ?>
    <?php $totalinsentifluarkota = 0; ?>
    <?php $totalinsentif = 0; ?>


    <div class="dropdown">
      <button class="btn btn-default dropdown-toggle" type="button"
              id="dropdownMenu1" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="true">
        <i class="glyphicon glyphicon-cog"></i>
        <span class="caret"></span>
      </button>
      <ul class="dropdown-menu checkbox-menu allow-focus" aria-labelledby="dropdownMenu1">
        <li>
           <label>
           <input type="checkbox" class="check" id="checkAll"><span class="label-text">Check All</span>
           </label>
        </li>
        <li><label>
           <input type="checkbox" id="cbtimestamp" onclick="hideshow('cbtimestamp','laptimestamp')" checked><span class="label-text"> timestamp</span>
           </label>
        </li>
        <li><label>
           <input type="checkbox" id="cbnama" onclick="hideshow('cbnama','lapnama')" checked><span class="label-text"> nama</span>
           </label>
        </li>
        <li><label>
           <input type="checkbox" id="cbtgl" onclick="hideshow('cbtgl','laptgl')" checked><span class="label-text"> tanggal</span>
           </label>
        </li>
        <li><label>
           <input type="checkbox" id="cbjhk" onclick="hideshow('cbjhk','lapjhk')" checked> <span class="label-text">Jns Hari Kerja</span>
           </label>
        </li>
        <li><label>
           <input type="checkbox" id="cbjmsk" onclick="hideshow('cbjmsk','lapjmsk')" checked><span class="label-text"> Jam Masuk</span>
           </label>
        </li>
        <li><label>
           <input type="checkbox" id="cbjmplg" onclick="hideshow('cbjmplg','lapjmplg')" checked><span class="label-text"> Jam Pulang</span>
           </label>
        </li>
        <li><label>
           <input type="checkbox" id="cbovt" onclick="hideshow('cbovt','lapovt')" checked><span class="label-text"> Overtime</span>
           </label>
        </li>
        <li><label>
           <input type="checkbox" id="cbklien" onclick="hideshow('cbklien','lapklien')" checked><span class="label-text"> Klien</span>
           </label>
        </li>
        <li><label>
           <input type="checkbox" id="cbkeg" onclick="hideshow('cbkeg','lapkeg')" checked> <span class="label-text">Kegiatan</span>
           </label>
        </li>
        <li><label>
           <input type="checkbox" id="cbpkeg" onclick="hideshow('cbnama','lappkeg')" checked><span class="label-text"> P.Kegiatan</span>
           </label>
        </li>
        <li><label>
           <input type="checkbox" id="cbskerja" onclick="hideshow('cbskerja','lapstatkrj')" checked><span class="label-text"> S.Kerja</span>
           </label>
        </li>
        <li><label>
           <input type="checkbox" id="cbprn" onclick="hideshow('cbprn','lapprn')" checked> <span class="label-text">Peran</span>
           </label>
        </li>
        <li><label>
           <input type="checkbox" id="cbrkn" onclick="hideshow('cbrkn','laprkn')" checked> <span class="label-text">Rekan</span>
           </label>
        </li>
        <li><label>
           <input type="checkbox" id="cbtrans" onclick="hideshow('cbtrans','laptrans')" checked><span class="label-text"> Transport</span>
           </label>
        </li>
        <li><label>
           <input type="checkbox" id="cbbtrans" onclick="hideshow('cbbtrans','lapbtrans')" checked><span class="label-text"> B.Transport</span>
           </label>
        </li>
        <li><label>
           <input type="checkbox" id="cbkbtrans" onclick="hideshow('cbkbtrans','lapkbtrans')" checked><span class="label-text"> K.B.Transport</span>
           </label>
        </li>
        <li><label>
           <input type="checkbox" id="cbbpark" onclick="hideshow('cbbpark','lapbpark')" checked> <span class="label-text">B.Parkir</span>
           </label>
        </li>
        <li><label>
           <input type="checkbox" id="cbblch" onclick="hideshow('cbblch','lapblnch')" checked> <span class="label-text">B.Mkn Siang</span>
           </label>
        </li>
        <li><label>
           <input type="checkbox" id="cbbdnr" onclick="hideshow('cbbdnr','lapbdnr')" checked><span class="label-text"> B.Mkn Mlm</span>
           </label>
        </li>
        <li><label>
           <input type="checkbox" id="cbbmed" onclick="hideshow('cbbmed','lapbmed')" checked><span class="label-text"> B.Kshtn</span>
           </label>
        </li>
        <li><label>
           <input type="checkbox" id="cbbsvc" onclick="hideshow('cbbsvc','lapbsvc')" checked> <span class="label-text">B.Allwnc</span>
           </label>
        </li>
        <li><label>
           <input type="checkbox" id="cbblain" onclick="hideshow('cbblain','lapblain')" checked><span class="label-text"> B.Lain</span>
           </label>
        </li>
        <li><label>
           <input type="checkbox" id="cbkblain" onclick="hideshow('cbkblain','lapkblain')" checked><span class="label-text"> K.B.Lain</span>
           </label>
        </li>
        <li><label>
           <input type="checkbox" id="cbinstr" onclick="hideshow('cbinstr','lapinstr')" checked><span class="label-text"> Ins.Training</span>
           </label>
        </li>
        <li><label>
           <input type="checkbox" id="cbinshol" onclick="hideshow('cbinshol','lapinshol')" checked><span class="label-text"> Ins.H.Lib</span>
           </label>
        </li>
        <li><label>
           <input type="checkbox" id="cbinsoot" onclick="hideshow('cbinsoot','lapinsoot')" checked><span class="label-text"> Ins.L.Kota</span>
           </label>
        </li>

      </ul>
    </div>
        <script type="text/javascript">
            $(".checkbox-menu").on("change", "input[type='checkbox']", function() {
       $(this).closest("li").toggleClass("active", this.checked);
    });

    $(document).on('click', '.allow-focus', function (e) {
      e.stopPropagation();
    });

    $("#checkAll").click(function () {
    $('input[type="checkbox"]').prop('checked', $(this).prop('checked'));
    hideshow('cbnama','lapnama');
hideshow('cbtgl','laptgl');
hideshow('cbjhk','lapjhk');
hideshow('cbjmsk','lapjmsk');
hideshow('cbjmplg','lapjmplg');
hideshow('cbovt','lapovt');
hideshow('cbklien','lapklien');
hideshow('cbkeg','lapkeg');
hideshow('cbnama','lappkeg');
hideshow('cbskerja','lapstatkrj');
hideshow('cbprn','lapprn');
hideshow('cbrkn','laprkn');
hideshow('cbtrans','laptrans');
hideshow('cbbtrans','lapbtrans');
hideshow('cbkbtrans','lapkbtrans');
hideshow('cbbpark','lapbpark');
hideshow('cbblch','lapblnch');
hideshow('cbbdnr','lapbdnr');
hideshow('cbbmed','lapbmed');
hideshow('cbbsvc','lapbsvc');
hideshow('cbblain','lapblain');
hideshow('cbkblain','lapkblain');
hideshow('cbinstr','lapinstr');
hideshow('cbinshol','lapinshol');
hideshow('cbinsoot','lapinsoot');
});

        </script>


	<div class="row">
        <div class="col-md-12" style="overflow:auto;">
            <!-- <form action="" method="post">
                <select id="cbs" multiple='multiple'>
                    <option id="cbtimestamp" onclick="hideshow('cbnama','laptimestamp')"></option>
                </select>
            </form> -->
            <!-- <table>
                <tbody>
                    <tr>
                        <td>

                        </td>

                        <td class="paddingtd">
                            <input type="checkbox" id="cbtimestamp" onclick="hideshow('cbtimestamp','laptimestamp')" checked> timestamp
                        </td>
                        <td class="paddingtd">
                            <input type="checkbox" id="cbnama" onclick="hideshow('cbnama','lapnama')" checked> nama
                        </td>
                        <td class="paddingtd">
                            <input type="checkbox" id="cbtgl" onclick="hideshow('cbtgl','laptgl')" checked> tanggal
                        </td>
                        <td class="paddingtd">
                            <input type="checkbox" id="cbjhk" onclick="hideshow('cbjhk','lapjhk')" checked> Jns Hari Kerja
                        </td>
                        <td class="paddingtd">
                            <input type="checkbox" id="cbjmsk" onclick="hideshow('cbjmsk','lapjmsk')" checked> Jam Masuk
                        </td>
                        <td class="paddingtd">
                            <input type="checkbox" id="cbjmplg" onclick="hideshow('cbjmplg','lapjmplg')" checked> Jam Pulang
                        </td>
                        <td class="paddingtd">
                            <input type="checkbox" id="cbovt" onclick="hideshow('cbovt','lapovt')" checked> Overtime
                        </td>
                        <td class="paddingtd">
                            <input type="checkbox" id="cbklien" onclick="hideshow('cbklien','lapklien')" checked> Klien
                        </td>
                        <td class="paddingtd">
                            <input type="checkbox" id="cbkeg" onclick="hideshow('cbkeg','lapkeg')" checked> Kegiatan
                        </td>
                        <td class="paddingtd">
                            <input type="checkbox" id="cbpkeg" onclick="hideshow('cbnama','lappkeg')" checked> P.Kegiatan
                        </td>
                        <td class="paddingtd">
                            <input type="checkbox" id="cbskerja" onclick="hideshow('cbskerja','lapstatkrj')" checked> S.Kerja
                        </td>
                        <td class="paddingtd">
                            <input type="checkbox" id="cbprn" onclick="hideshow('cbprn','lapprn')" checked> Peran
                        </td>
                        <td class="paddingtd">
                            <input type="checkbox" id="cbrkn" onclick="hideshow('cbrkn','laprkn')" checked> Rekan
                        </td>
                        <td class="paddingtd">
                            <input type="checkbox" id="cbtrans" onclick="hideshow('cbtrans','laptrans')" checked> Transport
                        </td>
                        <td class="paddingtd">
                            <input type="checkbox" id="cbbtrans" onclick="hideshow('cbbtrans','lapbtrans')" checked> B.Transport
                        </td>
                        <td class="paddingtd">
                            <input type="checkbox" id="cbkbtrans" onclick="hideshow('cbkbtrans','lapkbtrans')" checked> K.B.Transport
                        </td>
                        <td class="paddingtd">
                            <input type="checkbox" id="cbbpark" onclick="hideshow('cbbpark','lapbpark')" checked> B.Parkir
                        </td>
                        <td class="paddingtd">
                            <input type="checkbox" id="cbblch" onclick="hideshow('cbblch','lapblnch')" checked> B.Mkn Siang
                        </td>
                        <td class="paddingtd">
                            <input type="checkbox" id="cbbdnr" onclick="hideshow('cbbdnr','lapbdnr')" checked> B.Mkn Mlm
                        </td>
                        <td class="paddingtd">
                            <input type="checkbox" id="cbbmed" onclick="hideshow('cbbmed','lapbmed')" checked> B.Kshtn
                        </td>
                        <td class="paddingtd">
                            <input type="checkbox" id="cbbsvc" onclick="hideshow('cbbsvc','lapbsvc')" checked> B.Allwnc
                        </td>
                        <td class="paddingtd">
                            <input type="checkbox" id="cbblain" onclick="hideshow('cbblain','lapblain')" checked> B.Lain
                        </td>
                        <td class="paddingtd">
                            <input type="checkbox" id="cbkblain" onclick="hideshow('cbkblain','lapkblain')" checked> K.B.Lain
                        </td>
                        <td class="paddingtd">
                            <input type="checkbox" id="cbinstr" onclick="hideshow('cbinstr','lapinstr')" checked> Ins.Training
                        </td>
                        <td class="paddingtd">
                            <input type="checkbox" id="cbinshol" onclick="hideshow('cbinshol','lapinshol')" checked> Ins.H.Lib
                        </td>
                        <td class="paddingtd">
                            <input type="checkbox" id="cbinsoot" onclick="hideshow('cbinsoot','lapinsoot')" checked> Ins.L.Kota
                        </td>
                    </tr>
                </tbody>
            </table> -->

            <table class="table table-bordered">
                <thead>

                    <tr>
                        <th class="laptimestamp" style="vertical-align: middle;">Timestamp</th>
                        <th class="lapnama" style="vertical-align: middle;">Nama</th>
                        <th class="laptgl" style="vertical-align: middle;">Tanggal</th>
                        <th class="lapjhk" style="vertical-align: middle;">Jenis Hari Kerja</th>
                        <th class="lapjmsk" style="vertical-align: middle;">Jam Masuk</th>
                        <th class="lapjmplg" style="vertical-align: middle;">Jam Pulang</th>
                        <th class="lapovt" style="vertical-align: middle;">Overtime</th>
                        <th class="lapklien" style="vertical-align: middle;">Klien</th>
                        <th class="lapkeg" style="vertical-align: middle;">Kegiatan</th>
                        <th class="lappkeg" style="vertical-align: middle;">Peserta Kegiatan</th>
                        <th class="lapstatkrj" style="vertical-align: middle;">Status</th>
                        <th class="lapprn" style="vertical-align: middle;">Peran</th>
                        <th class="laprkn" style="vertical-align: middle;">Rekan</th>
                        <th class="laptrans" style="vertical-align: middle;">Transportasi</th>
                        <th class="lapbtrans" style="vertical-align: middle;">Biaya Transport</th>
                        <th class="lapkbtrans" style="vertical-align: middle;">Keterangan Transport</th>
                        <th class="lapbpark" style="vertical-align: middle;">Biaya Parkir</th>
                        <th class="lapblnch" style="vertical-align: middle;">Biaya Makan Siang</th>
                        <th class="lapbdnr" style="vertical-align: middle;">Biaya Makan Malam</th>
                        <th class="lapbmed" style="vertical-align: middle;">Biaya Kesehatan</th>
                        <th class="lapbsvc" style="vertical-align: middle;">Biaya Allowance</th>
                        <th class="lapblain" style="vertical-align: middle;">Biaya Lain</th>
                        <th class="lapkblain" style="vertical-align: middle;">Ket Biaya Lain</th>
                        <th class="lapinstr" style="vertical-align: middle;">Insentif Training</th>
                        <th class="lapinshol" style="vertical-align: middle;">Insentif Hari Libur</th>
                        <th class="lapinsoot" style="vertical-align: middle;">Insentif Luar Kota</th>
                        <th style="vertical-align: middle;">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($data->listdata)=='0'): ?>
                        <tr>
                            <td colspan="25" style="text-align:center;">Tidak Ada data di dalam database kami</td>
                        </tr>
                    <?php else: ?>
                        <?php foreach ($data->listdata as $key): ?>
                            <?php if ($key['status']=='5'): ?>
                            <tr class="success">
                            <?php else: ?>
                            <tr>
                            <?php endif ?>
                                <td class="laptimestamp" style="vertical-align: middle;"><?= $key['timestamp'] ?></td>
                                <td class="lapnama" style="vertical-align: middle;"><?= $key['nama'] ?></td>
                                <td class="laptgl" style="vertical-align: middle;"><?= $key['tanggal'] ?></td>
                                <td class="lapjhk" style="vertical-align: middle;"><?= $key['jns_hari_kerja'] ?></td>
                                <td class="lapjmsk" style="vertical-align: middle;"><?= $key['absen_masuk'] ?></td>
                                <td class="lapjmplg" style="vertical-align: middle;"><?= $key['absen_pulang'] ?></td>
                                <td class="lapovt" style="vertical-align: middle;"><?= 'overtime' ?></td>
                                <td class="lapklien" style="vertical-align: middle;"><?= $key['klien'] ?></td>
                                <td class="lapkeg" style="vertical-align: middle;"><?= $key['ket_kegiatan'] ?></td>
                                <td class="lappkeg" style="vertical-align: middle;"><?= $key['peserta_kegiatan'] ?></td>
                                <td class="lapstatkrj" style="vertical-align: middle;"><?= $key['status_kerja'] ?></td>
                                <td class="lapprn" style="vertical-align: middle;"><?= $key['peran'] ?></td>
                                <td class="laprkn" style="vertical-align: middle;">
                                <?php $rekan = explode(' # ', $key['rekan']) ?>
                                    <ul>
                                    <?php foreach ($data->users as $value): ?>
                                        <?php if (in_array($value['username'], $rekan)): ?>
                                        <li><?= $value['nama'] ?></li>
                                        <?php endif ?>
                                    <?php endforeach ?>
                                    </ul>
                                </td>
                                <td class="laptrans" style="vertical-align: middle;"><?= $key['jns_transportasi'] ?></td>
                                <?php if ($key['itrans']==''||$key['itrans']==$key['biaya_transport']): ?>
                                    <td class="lapbtrans" style="vertical-align: middle;"><?= $key['biaya_transport'] ?></td>
                                <?php else: ?>
                                    <td class="lapbtrans" style="vertical-align: middle;">
                                        <?= $key['biaya_transport'] ?>
                                        |
                                        <span style="color:red;"><?= $key['itrans'] ?></span>
                                    </td>
                                <?php endif ?>
                                <?php $totaltransport += $key['biaya_transport'] ?>
                                <td class="lapkbtrans" style="vertical-align: middle;"><?= $key['ket_transport'] ?></td>
                                <?php if ($key['ipark']==''||$key['ipark']==$key['biaya_parkir']): ?>
                                    <td class="lapbpark" style="vertical-align: middle;">
                                        <?= $key['biaya_parkir'] ?>
                                    </td>
                                <?php else: ?>
                                    <td class="lapbpark" style="vertical-align: middle;">
                                        <?= $key['biaya_parkir'] ?>
                                        |
                                        <span style="color:red;"><?= $key['ipark'] ?></span>
                                    </td>
                                <?php endif ?>
                                <?php $totalparkir += $key['biaya_parkir'] ?>
                                <?php if ($key['ilunch']==''||$key['ilunch']==$key['biaya_lunch']): ?>
                                    <td class="lapblnch" style="vertical-align: middle;">
                                        <?= $key['biaya_lunch'] ?>
                                    </td>
                                <?php else: ?>
                                    <td class="lapblnch" style="vertical-align: middle;">
                                        <?= $key['biaya_lunch'] ?>
                                        |
                                        <span style="color:red;"><?= $key['ilunch'] ?></span>
                                    </td>
                                <?php endif ?>
                                <?php $totalmakansiang += $key['biaya_lunch'] ?>
                                <?php if ($key['idnr']==''||$key['idnr']==$key['biaya_dinner']): ?>
                                    <td class="lapbdnr" style="vertical-align: middle;">
                                        <?= $key['biaya_dinner'] ?>
                                    </td>
                                <?php else: ?>
                                    <td class="lapbdnr" style="vertical-align: middle;">
                                        <?= $key['biaya_dinner'] ?>
                                        |
                                        <span style="color:red;"><?= $key['idnr'] ?></span>
                                    </td>
                                <?php endif ?>
                                <?php $totalmakanmalam += $key['biaya_dinner'] ?>
                                <?php if ($key['imed']==''||$key['imed']==$key['biaya_kesehatan']): ?>
                                    <td class="lapbmed" style="vertical-align: middle;">
                                        <?= $key['biaya_kesehatan'] ?>
                                    </td>
                                <?php else: ?>
                                    <td class="lapbmed" style="vertical-align: middle;">
                                        <?= $key['biaya_kesehatan'] ?>
                                        |
                                        <span style="color:red;"><?= $key['imed'] ?></span>
                                    </td>
                                <?php endif ?>
                                <?php $totalkesehatan += $key['biaya_kesehatan'] ?>
                                <?php if ($key['iallow']==''||$key['biaya_allowance']==$key['iallow']): ?>
                                    <td class="lapbsvc" style="vertical-align: middle;">
                                        <?= $key['biaya_allowance'] ?>
                                    </td>
                                <?php else: ?>
                                    <td class="lapbsvc" style="vertical-align: middle;">
                                        <?= $key['biaya_allowance'] ?>
                                        |
                                        <span style="color:red;"><?= $key['iallow'] ?></span>
                                    </td>
                                <?php endif ?>

                                <?php $totalallowance += $key['biaya_allowance'] ?>
                                <?php if ($key['iothr']==''||$key['iothr']==$key['biaya_lain']): ?>
                                    <td class="lapblain" style="vertical-align: middle;">
                                        <?= $key['biaya_lain'] ?>
                                    </td>
                                <?php else: ?>
                                    <td class="lapblain" style="vertical-align: middle;">
                                        <?= $key['biaya_lain'] ?>
                                        |
                                        <span style="color:red;"><?= $key['iothr'] ?></span>
                                    </td>
                                <?php endif ?>

                                <?php $totalbiayalain += $key['biaya_lain'] ?>
                                <td class="lapkblain" style="vertical-align: middle;">
                                    <?= $key['ket_biaya_lain'] ?>
                                </td>
                                <td class="lapinstr" style="vertical-align: middle;">
                                    <a target="_blank" href="<?= $data->base_url.'administrasi/user/laporan-harian?insdet='.$key['id'] ?>" title="Detail Insentif">
                                    <?= number_format(KalkulasiInsentifAcc($key['tarif_accurate'],$key['tarif_peran'],$key['ijt_acc'])) ?>
                                    </a>
                                </td>
                                <td style="vertical-align: middle;">
                                    <a target="_blank" href="<?= $data->base_url.'administrasi/user/laporan-harian?insdet='.$key['id'] ?>" title="Detail Insentif">
                                    <?= KalkulasiInsentifHaker($key['tarif_hari_kerja'],$key['ijt_hari']) ?>
                                    </a>
                                </td>
                                <td class="lapinshol" style="vertical-align:middle;">
                                    <a target="_blank" href="<?= $data->base_url.'administrasi/user/laporan-harian?insdet='.$key['id'] ?>" title="Detail Insentif">
                                    <?= KalkulasiInsentifLuarKota($key['tarif_wilayah'],$key['ijt_wilayah'],$key['is_ttl_attnd']) ?>
                                    </a>
                                </td>
                                <td class="lapinsoot" style="vertical-align: middle;">
                                    <a target="_blank" class="btn btn-info" href="<?= $data->base_url.'administrasi/user/reimapv?id='.$key['id'].'&index=0' ?>" title="Reimbursement">
                                        edit
                                    </a>
                                </td>
                                <?php $totalinsentifaccurate += KalkulasiInsentifAcc($key['tarif_accurate'],$key['tarif_peran'],$key['ijt_acc']) ?>
                                <?php $totalinsentifharikerja += KalkulasiInsentifHaker($key['tarif_hari_kerja'],$key['ijt_hari']) ?>
                                <?php $totalinsentifluarkota += KalkulasiInsentifLuarKota($key['tarif_wilayah'],$key['ijt_wilayah'],$key['is_ttl_attnd']) ?>
                                <?php $totalinsentif += KalkulasiInsentifAcc($key['tarif_accurate'],$key['tarif_peran'],$key['ijt_acc']) + KalkulasiInsentifHaker($key['tarif_hari_kerja'],$key['ijt_hari']) + KalkulasiInsentifLuarKota($key['tarif_wilayah'],$key['ijt_wilayah'],$key['is_ttl_attnd']) ?>
                            </tr>
                        <?php endforeach ?>
                        <tr>
                            <td colspan="14" style="vertical-align: middle;"><b> Subtotal</b></td>
                            <td colspan="2" style="text-align: right;vertical-align: middle;">
                                <?= number_format($totaltransport) ?> IDR
                            </td>
                            <td style="text-align: right;vertical-align: middle;"><?= number_format($totalparkir) ?> IDR</td>
                            <td style="text-align: right;vertical-align: middle;"><?= number_format($totalmakansiang) ?> IDR</td>
                            <td style="text-align: right;vertical-align: middle;"><?= number_format($totalmakanmalam) ?> IDR</td>
                            <td style="text-align: right;vertical-align: middle;"><?= number_format($totalkesehatan) ?> IDR</td>
                            <td style="text-align: right;vertical-align: middle;"><?= number_format($totalallowance) ?> IDR</td>
                            <td colspan="2" style="text-align: right;vertical-align: middle;"><?= number_format($totalbiayalain) ?> IDR</td>
                            <td><?= number_format($totalinsentifaccurate) ?> IDR</td>
                            <td><?= number_format($totalinsentifharikerja) ?> IDR</td>
                            <td colspan="2"><?= number_format($totalinsentifluarkota) ?> IDR</td>
                            <!--<td></td>-->
                            <?php $sumtotal = $totaltransport + $totalparkir + $totalmakansiang + $totalmakanmalam + $totalkesehatan + $totalallowance + $totalbiayalain + $totalinsentif; ?>
                        </tr>
                        <tr>
                            <td colspan="22"><b>Total</b></td>
                            <td colspan="5" style="text-align: right;"><b><?= number_format($sumtotal) ?> IDR</b></td>
                        </tr>
                    <?php endif ?>

                </tbody>
            </table>
        </div>
    </div>

</div><!--end container-->
<!-- Modal -->
  <div class='modal fade' id='modal-cariz' role='dialog'>
    <div class='modal-dialog'>

      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pencarian Data</h3>

        </div>
        <div class='modal-body'>
        <div style='padding:15px; height: 300px;' class='row'>

          <div class="col-md-12">
            <form action="" method="GET">
                    <div class="row">
                        <div class="form-group">
                            <label for="dari-tanggal">Username</label>
                            <select name="src[username]" class="form-control" id="usernamez">
                                <option value="">-- Pilih User --</option>
                                <?php foreach ($data->users as $key): ?>
                                    <option value="<?= $key['username'] ?>"><?= $key['nama'] ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="dari-tanggal">Mulai Tanggal</label>
                            <input type="text" name="src[tglawal]" id="tanggal-awal" class="form-control" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label for="dari-tanggal">Sampai Tanggal</label>
                            <input type="text" name="src[tglakhir]" id="tanggal-akhir" class="form-control" />
                        </div>
                    </div>

                    <div class="row">
                        <button class="btn btn-primary" id='btn-downloadLaporan'>Cari</button>
                    </div>
            </form>

          </div>



        </div>


        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>FAC-Institute 2016</span>
                </div>
                <div class="col-md-6">
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
            </div>

        </div>
      </div>

    </div>
  </div>
  <!-- end modal -->
  <script type="text/javascript" src=<?php echo "'".$data->base_url."js/jquery-ui/jquery-ui.js'"; ?>></script>

<script type="text/javascript" src="<?= $data->base_url ?>css/multiselect/js/bootstrap-multiselect.js"></script>
  <script>
      $(document).ready(function(){

        $('#tanggal-awal').datepicker({
            dateFormat: 'yy-mm-dd'
        });

      $('#tanggal-akhir').datepicker({
        dateFormat: 'yy-mm-dd'
      });

    });


    function hideshow(idname,classname){
        if ($('#'+idname).is(":checked")) {
            $('.'+classname).show(500);
        }else{
            $('.'+classname).hide(500);
        }
    }
    // $('#cbs').multiselect({
    //     buttonWidth: '400px',
    //     includeSelectAllOption: true
    // });
  </script>
</body>
</html>
