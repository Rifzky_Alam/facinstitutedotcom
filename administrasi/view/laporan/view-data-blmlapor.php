<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3>Data Laporan Belum Terlapor</h3>
	</div>
	
    <!--
	<div class="row">
		<div class="col-sm-12 col-md-12" style="text-align:right"><a class="" id="cari-data" href="#modal-cari" data-toggle="modal">Cari Data?</a></div>
	</div>
    -->

    <div class='row' style="overflow:auto;">
        <div class='col-md-12'>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th>Perusahaan</th>
                        <th>Nama Petugas</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($data->lists)=='0'): ?>
                        <tr>
                            <td colspan="3" class="tengah">Laporan Anda Sudah Lengkap</td>
                        </tr>
                    <?php else: ?>

                            <?php foreach ($data->lists as $key): ?>
                                <tr>
                                    <td><?= $key->tanggal ?></td>
                                    <td><?= $key->nama_usaha ?></td>
                                    <td><?= $key->petugas ?></td>
                                    <td><a href="<?= $data->base_url.'administrasi/user/laporan-harian?id='.$key->id ?>" title="Edit Laporan" class="btn btn-primary" target="_blank">Buat Laporan</a></td>
                                </tr>
                            <?php endforeach ?>
                    <?php endif ?>
                </tbody>
            </table>
        </div>

    </div>
	    
</div>
</body>
</html> 