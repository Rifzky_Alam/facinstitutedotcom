<!DOCTYPE html>
<html>
<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>

<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->subtitle ?></h3>
	</div>
	
     

	
	<div class="row">
		      <div class="col-md-3">
            <div class="square-service-block">
               <a href="<?= $data->base_url ?>administrasi/laporan/marketing/">
                 <div class="ssb-icon"><i class="fa fa-paint-brush" aria-hidden="true"></i></div>
                 <h2 class="ssb-title">Marketing</h2>  
               </a>
            </div>
          </div>
          
          <div class="col-md-3">
            <div class="square-service-block">
               <a href="<?= $data->base_url ?>administrasi/laporan/usaha/">
                 <div class="ssb-icon"> <i class="fa fa-globe" aria-hidden="true"></i> </div>
                 <h2 class="ssb-title">Perusahaan</h2>  
               </a>
            </div>
          </div>

          <div class="col-md-3">
            <div class="square-service-block">
               <a href="<?= $data->base_url ?>administrasi/laporan/transaksi/bulanan.php">
                 <div class="ssb-icon"> <i class="fa fa-globe" aria-hidden="true"></i> </div>
                 <h2 class="ssb-title">Transaksi</h2>  
               </a>
            </div>
          </div>
          
          <div class="col-md-3">
            <div class="square-service-block">
               <a href="<?= $data->base_url ?>administrasi/laporan/staff/">
                 <div class="ssb-icon"><i class="fa fa-camera" aria-hidden="true"></i></div>
                 <h2 class="ssb-title">Staff</h2>  
               </a>
            </div>
          </div>
          <!--=======================-->
          <div class="col-md-3">
            <div class="square-service-block">
               <a href="<?= $data->base_url ?>administrasi/laporan/marketing/bulanan.php">
                 <div class="ssb-icon"><i class="fa fa-paint-brush" aria-hidden="true"></i></div>
                 <h2 class="ssb-title">Marketing Bulanan</h2>  
               </a>
            </div>
          </div>
          
          <div class="col-md-3">
            <div class="square-service-block">
               <a href="<?= $data->base_url ?>administrasi/laporan/usaha/">
                 <div class="ssb-icon"> <i class="fa fa-globe" aria-hidden="true"></i> </div>
                 <h2 class="ssb-title">Perusahaan Bulanan</h2>  
               </a>
            </div>
          </div>

          <div class="col-md-3">
            <div class="square-service-block">
               <a href="<?= $data->base_url ?>administrasi/laporan/transaksi/bulanan.php/bulanan">
                 <div class="ssb-icon"> <i class="fa fa-globe" aria-hidden="true"></i> </div>
                 <h2 class="ssb-title">Transaksi Bulanan</h2>  
               </a>
            </div>
          </div>
          
          <div class="col-md-3">
            <div class="square-service-block">
               <a href="<?= $data->base_url ?>administrasi/laporan/staff/bulanan.php">
                 <div class="ssb-icon"><i class="fa fa-camera" aria-hidden="true"></i></div>
                 <h2 class="ssb-title">Staff Bulanan</h2>  
               </a>
            </div>
          </div>
          
          <!--=======================-->
          <div class="col-md-3">
            <div class="square-service-block">
               <a href="<?= $data->base_url ?>administrasi/reports">
                 <div class="ssb-icon"><i class="fa fa-paint-brush" aria-hidden="true"></i></div>
                 <h2 class="ssb-title">Lap. Bulanan</h2>  
               </a>
            </div>
          </div>
        
	</div>

</div>

     
</body>
</html> 