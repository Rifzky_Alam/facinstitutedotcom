<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>


<div class="container" id="isi" style="padding-bottom:40px;">
	<div class="page-header" id="top-logo">
		<h3><?= $data->subtitle ?></h3>
	</div>

        <div class="row">
            <div class="col-md-12" style="text-align:right;">
                <a href="<?= $data->base_url.'administrasi//marketing/new-followup' ?>" class="btn btn-success">Tambah</a> 
                <a id="cari-data" href="#modal-cari" data-toggle="modal" class="btn btn-info">Search</a>    
            </div>
        </div>
        <br>
        <div class='row'>
            <div class='col-md-12'>
                
                <table class='table table-bordered'>
                    <thead>
                        <tr>
                            <th class='tengah'>TimeStamp</th>
                            <th class='tengah'>Tanggal Follow-Up</th>
                            <th class='tengah'>Customer</th>
                            <th class='tengah'>Agenda</th>
                            <th class='tengah'>Status</th>
                            <th class='tengah'>Jumlah Hari</th>
                            <th class='tengah'>Petugas</th>
                            <th class='tengah'>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php if (count($data->listdata)=='0'): ?>
                        <tr>
                            <td colspan="8" class="tengah">
                                Tidak ada data dalam database kami.
                            </td>
                        </tr>    
                    <?php else: ?>
                        <?php foreach ($data->listdata as $key): ?>
                            <tr>
                                <td><?= $key['date_created'] ?></td>
                                <td><?= $key['tanggal'] ?></td>
                                <td><?= $key['customer'] ?></td>
                                <td><?= $key['agenda'] ?></td>
                                <td><?= $key['desc_stat'] ?></td>
                                <td><?= $key['jumlah_hari'] ?></td>
                                <td><?= $key['petugas'] ?></td>
                                <td>
                                    <a href="<?= $data->base_url.'administrasi/marketing/followup-edit?id='.$key['num'] ?>" title="Edit" class="btn btn-warning">Edit</a>
                                    <a href="<?= $data->base_url.'administrasi/marketing/followup-delete?id='.$key['num'] ?>" title="Hapus" class="btn btn-danger">Hapus</a>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                    </tbody>
                </table>
                
            </div>
        </div>
</div>

<!-- Modal -->
  <div class='modal fade' id='modal-cari' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pencarian Data</h3>

        </div>
       <div class='modal-body' style='overflow-y:scroll'>
        <div style='padding:15px; height: 300px;' class='row'>
          
          <div class="col-md-12">
        
                <form action="" method="GET">

                    <div class="row">
                        <div class="form-group">
                          <label>Nama Perusahaan/Customer</label>
                          <input type="text" name="src[np]" class="form-control">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                          <label>Tanggal Awal</label>
                          <input type="text" id="tanggal-awal" name="src[fd]" class="form-control">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                          <label>Tanggal Akhir</label>
                          <input type="text" id="tanggal-akhir" name="src[ld]" class="form-control">
                        </div>
                    </div>

                    <div class="row">
                        <button class="btn btn-lg btn-success" style='width:100%'>Cari</button>
                    </div>

                </form>
          </div>
        </div>
       </div>


        <div class='modal-footer'>

            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>FAC-Institute 2016</span>
                </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->

<script type="text/javascript" src=<?php echo "'".$data->base_url."js/jquery-ui/jquery-ui.js'"; ?>></script>
  <script type="text/javascript">
    $(document).ready(function(){
        $('#editTgl').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('#tanggal-awal').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('#tanggal-akhir').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('.tanggalPelaksanaan').datepicker({
            dateFormat: 'yy-mm-dd'
        });
    });

    

    function removeElement(id) {
        $('#'+id).remove();
    }

  </script>   
</body>
</html> 



