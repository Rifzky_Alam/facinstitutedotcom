<!doctype html>
<html>

<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>

<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.js"></script>
    <script src="https://fac-institute.com/js/charts/util.js"></script>
<?php include_once $data->homedir.'administrasi/view/main-component/top-nav.php'; ?>
<div class="container">

	<div class="row">
		<div class="page-header">
			<h2><?= $data->subtitle ?></h2>
		</div>
	</div>

	<!-- <div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">
			
		</div>
	</div> -->
	<!-- <br><br -->
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">
            <div class="col-md-6">
                <div class="chart-container" style="position: relative;">
                    <canvas id="chart-area" ></canvas>
                </div>
                <br>
                <h3>Marketing Bulan Ini</h3>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="tengah" style="vertical-align:middle;">Nama Marketing</th>
                            <th class="tengah" style="vertical-align:middle;">Omset (A)</th>
                            <th class="tengah" style="vertical-align:middle;">% A</th>
                            <th class="tengah" style="vertical-align:middle;">Invoice Terbayar (B)</th>
                            <th class="tengah" style="vertical-align:middle;">% B</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php Rows($data->datamarketing) ?>
                    </tbody>
                </table>
                <br><br>
                <h3>Cabang Bulan Ini</h3>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="tengah" style="vertical-align:middle;">Nama Marketing</th>
                            <th class="tengah" style="vertical-align:middle;">Omset (A)</th>
                            <th class="tengah" style="vertical-align:middle;">% A</th>
                            <th class="tengah" style="vertical-align:middle;">Invoice Terbayar (B)</th>
                            <th class="tengah" style="vertical-align:middle;">% B</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php Rows($data->cabangblnini) ?>
                    </tbody>
                </table>
            </div>
    		<div class="col-md-6">
                <div class="chart-container" style="position: relative;">
                    <canvas id="chart-area2" ></canvas>
                </div>
                <br>
                <h3>Marketing Bulan Lalu</h3>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="tengah" style="vertical-align:middle;">Nama Marketing</th>
                            <th class="tengah" style="vertical-align:middle;">Omset (A)</th>
                            <th class="tengah" style="vertical-align:middle;">% A</th>
                            <th class="tengah" style="vertical-align:middle;">Invoice Terbayar (B)</th>
                            <th class="tengah" style="vertical-align:middle;">% B</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php Rows($data->datamarketingblnlalu) ?>
                    </tbody>
                </table>
                <br><br>
                <h3>Cabang Bulan Lalu</h3>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="tengah" style="vertical-align:middle;">Nama Marketing</th>
                            <th class="tengah" style="vertical-align:middle;">Omset (A)</th>
                            <th class="tengah" style="vertical-align:middle;">% A</th>
                            <th class="tengah" style="vertical-align:middle;">Invoice Terbayar (B)</th>
                            <th class="tengah" style="vertical-align:middle;">% B</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php Rows($data->cabangblnlalu) ?>
                    </tbody>
                </table>
            </div>			
		</div>
	</div>

</div>
    
    
    <script>
    
    var config = {
        type: 'pie',
        showTooltips: true,
        data: {
            datasets: [{
                data: [
                    <?php DataValue($data->datamarketing) ?>
                ],
                backgroundColor: [
                    <?php GetColors(count($data->datamarketing)) ?>
                ],
                label: 'Data Invoice Terbayar'
            }],
            labels: [
                <?php NamaMarketing($data->datamarketing) ?>
            ]
        },
        options: {
            legend: {
                display: false
            },
            
            responsive: true
        }
    };
    var configx = {
        type: 'pie',
        showTooltips: true,
        data: {
            datasets: [{
                data: [
                    <?php DataValue($data->datamarketingblnlalu) ?>
                ],
                backgroundColor: [
                    <?php GetColors(count($data->datamarketingblnlalu)) ?>
                ],
                label: 'Data Invoice Terbayar'
            }],
            labels: [
                <?php NamaMarketing($data->datamarketingblnlalu) ?>
            ]
        },
        options: {
            legend: {
                display: false
            },
            
            responsive: true
        }
    };
    window.onload = function() {
        var ctxx = document.getElementById("chart-area2").getContext("2d");
        var ctx = document.getElementById("chart-area").getContext("2d");
        window.myPie = new Chart(ctx, config);
        window.myPie = new Chart(ctxx, configx);
    };
    
    var colorNames = Object.keys(window.chartColors);
    
    </script>
</body>
</html>
<?php function Rows($value){ ?>
    <?php $totalomset = 0;$totalterbayar = 0; ?>
	<?php if (count($value)=='0'): ?>
		<tr>
			<td colspan="3">Tidak ada dalam database kami.</td>
		</tr>
	<?php else: ?>
        
        <?php foreach ($value as $key): ?>
            <?php $totalomset += $key->omset; $totalterbayar += $key->invoice_terbayar; ?>    
        <?php endforeach ?>

		<?php foreach ($value as $key): ?>
			<tr>
				<td><a href="<?= 'personal?m='.$key->aidi ?>" title="<?= $key->nama ?>"><?= $key->nama ?></a></td>
                
				<td style="text-align:right;vertical-align:middle;"><?= number_format($key->omset) .' IDR' ?></td>
				<td style="text-align:center;vertical-align:middle;">
                    <?= number_format((float)$key->omset/$totalomset*100,2,'.','') ?> %
                </td>
                <td style="text-align:right;vertical-align:middle;"><?= number_format($key->invoice_terbayar). ' IDR' ?></td>
                <?php if ($totalterbayar>0): ?>
                    <td style="text-align:center;vertical-align:middle;">
                        <?= number_format((float)$key->invoice_terbayar/$totalterbayar*100,2,'.','') ?> %
                    </td>
                <?php else: ?>
                    <td style="text-align:center;vertical-align:middle;">
                        <?= '0' ?> %
                    </td>
                <?php endif ?>
                    
			</tr>
		<?php endforeach ?>
            <tr>
                <td style="vertical-align:middle;"><b>Total</b></td>
                <td style="text-align: right;vertical-align:middle;"><b><?= number_format($totalomset) .' IDR' ?></b></td>
                <?php if ($totalomset>0): ?>
                    <td class="tengah" style="vertical-align:middle;">
                        <?= number_format((float)$totalomset/$totalomset*100,2,'.','') ?> %
                    </td>
                <?php else: ?>
                    <td class="tengah" style="vertical-align:middle;">
                        <?= '0' ?> %
                    </td>
                <?php endif ?>
                    
                <td style="text-align: right;vertical-align:middle;"><b><?= number_format($totalterbayar) . ' IDR' ?></b></td>
                <?php if ($totalterbayar>0): ?>
                    <td class="tengah" style="vertical-align:middle;">
                        <?= number_format((float)$totalterbayar/$totalterbayar*100,2,'.','') ?> %
                    </td>
                <?php else: ?>
                    <td class="tengah" style="vertical-align:middle;">
                        <?= '0' ?> %
                    </td>
                <?php endif ?>
                    
            </tr>
	<?php endif ?>
<?php } ?>
