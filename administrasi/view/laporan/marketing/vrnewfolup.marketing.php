<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>


<div class="container" id="isi" style="padding-bottom:40px;">
	<div class="page-header" id="top-logo">
		<h3><?= $data->subtitle ?></h3>
	</div>


        <div class='row'>
            <div class='col-md-12'>
                <form action="" method="post" accept-charset="utf-8">
                <div class="form-group">
                    <label>Tanggal Follow Up</label>
                    <input id="tglfolup" type="text" name="in[tanggal]" class="form-control" placeholder="Tanggal Follow Up" required>
                </div>
                <div class="form-group">
                    <label>Nama Cust/Perusahaan</label>
                    <input type="text" name="in[customer]" class="form-control" placeholder="Nama Customer/Perusahaan" required>
                </div>
                <div class="form-group">
                    <label>Agenda</label>
                    <textarea name="in[agenda]" placeholder="Agenda" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <label>Status</label>
                    <select required class="form-control" name="in[status]">
                        <option value="">--Pilih Status Follow-Up--</option>
                        <?php foreach ($data->listdesc as $key): ?>
                            <option value="<?= $key['dt_flag'] ?>"><?= $key['dt_desc'] ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Jumlah Hari</label>
                    <input type="text" name="in[jmlhari]" class="form-control" placeholder="Tanggal Follow Up">
                </div>
                <div class="form-group">
                    <label>Deskripsi Jumlah Hari (Dalam Tanggal)</label>
                    <input type="text" name="in[descjmlhari]" class="form-control" placeholder="Tanggal Follow Up">
                </div>
                <button class="btn btn-lg btn-primary">Submit</button>
                </form>
            </div>
        </div>
</div>
   <script type="text/javascript" src=<?php echo "'".$data->base_url."js/jquery-ui/jquery-ui.js'"; ?>></script>
  <script type="text/javascript">
    $(document).ready(function(){
        $('#tglfolup').datepicker({
            dateFormat: 'yy-mm-dd'
        });
    });

    

    function removeElement(id) {
        $('#'+id).remove();
    }

  </script>
</body>
</html> 



