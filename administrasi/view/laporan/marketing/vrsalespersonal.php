<!doctype html>
<html>

<head>
    <meta charset='utf-8'>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $data->title ?></title>
        <?= $data->View('main-component/links',$data); ?>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.js"></script>
    <script src="https://fac-institute.com/js/charts/util.js"></script>
    <style>
    canvas{
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
    }
    </style>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="page-header">
                <h2>Rekapitulasi Penjualan <?= $data->marketing ?></h2>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table>
                    <tbody>
                        <tr>
                            <td>Tahun</td><td>:</td><td><?= $data->tahun ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div style="width:75%;">
                    <canvas id="canvas"></canvas>
                </div> 
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <form action="" method="post">
                <div class="form-group">
                    <label for="tahun">Tahun</label>
                    <input type="text" name="thn" class="form-control" style="width:75%;" placeholder="contoh: 2017">
                </div>
                <button class="btn btn-lg btn-primary" style="width:75%;">Submit</button>
                </form>
            </div>
        </div>


    </div>

        
    
    
    <script>
        var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var config = {
            type: 'line',
            data: {
                labels: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli","Agustus","September","Oktober","November","Desember"],
                datasets: [{
                    label: "Omset",
                    backgroundColor: window.chartColors.red,
                    borderColor: window.chartColors.red,
                    data: [
                        <?= $data->omset_jan ?>,
                        <?= $data->omset_feb ?>,
                        <?= $data->omset_mar ?>,
                        <?= $data->omset_apr ?>,
                        <?= $data->omset_mei ?>,
                        <?= $data->omset_jun ?>,
                        <?= $data->omset_jul ?>,
                        <?= $data->omset_agu ?>,
                        <?= $data->omset_sep ?>,
                        <?= $data->omset_okt ?>,
                        <?= $data->omset_nov ?>,
                        <?= $data->omset_des ?>
                    ],
                    fill: false,
                }, {
                    label: "Terbayar",
                    fill: false,
                    backgroundColor: window.chartColors.blue,
                    borderColor: window.chartColors.blue,
                    data: [
                        <?= $data->net_jan ?>,
                        <?= $data->net_feb ?>,
                        <?= $data->net_mar ?>,
                        <?= $data->net_apr ?>,
                        <?= $data->net_mei ?>,
                        <?= $data->net_jun ?>,
                        <?= $data->net_jul ?>,
                        <?= $data->net_agu ?>,
                        <?= $data->net_sep ?>,
                        <?= $data->net_okt ?>,
                        <?= $data->net_nov ?>,
                        <?= $data->net_des ?>
                    ],
                }]
            },
            options: {
                responsive: true,
                title:{
                    display:true,
                    text:'Rekapitulasi Penjualan'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Bulan'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Jumlah'
                        }
                    }]
                }
            }
        };

        window.onload = function() {
            var ctx = document.getElementById("canvas").getContext("2d");
            window.myLine = new Chart(ctx, config);
        };

        document.getElementById('randomizeData').addEventListener('click', function() {
            config.data.datasets.forEach(function(dataset) {
                dataset.data = dataset.data.map(function() {
                    return randomScalingFactor();
                });

            });

            window.myLine.update();
        });

        var colorNames = Object.keys(window.chartColors);
        document.getElementById('addDataset').addEventListener('click', function() {
            var colorName = colorNames[config.data.datasets.length % colorNames.length];
            var newColor = window.chartColors[colorName];
            var newDataset = {
                label: 'Dataset ' + config.data.datasets.length,
                backgroundColor: newColor,
                borderColor: newColor,
                data: [],
                fill: false
            };

            for (var index = 0; index < config.data.labels.length; ++index) {
                newDataset.data.push(randomScalingFactor());
            }

            config.data.datasets.push(newDataset);
            window.myLine.update();
        });

        document.getElementById('addData').addEventListener('click', function() {
            if (config.data.datasets.length > 0) {
                var month = MONTHS[config.data.labels.length % MONTHS.length];
                config.data.labels.push(month);

                config.data.datasets.forEach(function(dataset) {
                    dataset.data.push(randomScalingFactor());
                });

                window.myLine.update();
            }
        });

        document.getElementById('removeDataset').addEventListener('click', function() {
            config.data.datasets.splice(0, 1);
            window.myLine.update();
        });

        document.getElementById('removeData').addEventListener('click', function() {
            config.data.labels.splice(-1, 1); // remove the label first

            config.data.datasets.forEach(function(dataset, datasetIndex) {
                dataset.data.pop();
            });

            window.myLine.update();
        });
    </script>
</body>

</html>
