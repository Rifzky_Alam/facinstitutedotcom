<!doctype html>
<html>

<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>

<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.js"></script>
    <script src="https://fac-institute.com/js/charts/util.js"></script>

<div class="container">

	<div class="row">
		<div class="page-header">
			<h2><?= $data->subtitle ?></h2>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<div id="canvas-holder" style="width:100%">
        		<canvas id="chart-area" />
    		</div>
		</div>
	</div>
	<br>
    <div class="row">
        <div class="col-md-12" style="text-align:right;">
            <a href="#modal-cariz" data-toggle="modal" title="Search" class="btn btn-primary">Search</a>
        </div>
    </div>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">
            <h3>Rincian Per Sales</h3>
            
			<table class="table table-bordered">
				<thead>
					<tr>
						<th class="tengah">Nama Marketing</th>
						<th class="tengah" style="vertical-align:middle;">Omset (A)</th>
                        <th class="tengah" style="vertical-align:middle;">% A</th>
                        <th class="tengah" style="vertical-align:middle;">Invoice Terbayar (B)</th>
                        <th class="tengah" style="vertical-align:middle;">% B</th>
					</tr>
				</thead>
				<tbody>
					<?php Rows($data->datamarketing) ?>
				</tbody>
			</table>	
            <br>
            <h3>Rincian Per Cabang</h3>	
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="tengah">Nama Marketing</th>
                        <th class="tengah" style="vertical-align:middle;">Omset (A)</th>
                        <th class="tengah" style="vertical-align:middle;">% A</th>
                        <th class="tengah" style="vertical-align:middle;">Invoice Terbayar (B)</th>
                        <th class="tengah" style="vertical-align:middle;">% B</th>
                    </tr>
                </thead>
                <tbody>
                    <?php Rows($data->datacabang) ?>
                </tbody>
            </table>
		</div>
	</div>

</div>


<!-- Modal -->
  <div class='modal fade' id='modal-cariz' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pencarian Data</h3>

        </div>
        <div class='modal-body'>
        <div style='padding:15px; height: 300px;' class='row'>
          
          <div class="col-md-12">
            <form action="" method="GET">
                    <div class="row">
                        <div class="form-group">
                            <label for="dari-tanggal">Mulai Tanggal</label>
                            <input type="text" name="src[tglawal]" id="tanggal-awal" class="form-control" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label for="dari-tanggal">Sampai Tanggal</label>
                            <input type="text" name="src[tglakhir]" id="tanggal-akhir" class="form-control" />
                        </div>
                    </div>

                    <div class="row">
                        <button class="btn btn-primary" id='btn-downloadLaporan'>Cari</button>
                    </div>
            </form>

          </div>
             
          

        </div>
          

        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>FAC-Institute 2016</span>
                </div>
                <div class="col-md-6">
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->
    <script type="text/javascript" src='https://fac-institute.com/js/jquery-ui/jquery-ui.js'></script>
    <script>

    $(document).ready(function(){

        $('#tanggal-awal').datepicker({
            dateFormat: 'yy-mm-dd'
        });

      $('#tanggal-akhir').datepicker({
        dateFormat: 'yy-mm-dd'
      });

    });
    
    var randomScalingFactor = function() {
        return Math.round(Math.random() * 100);
    };
    var config = {
        type: 'pie',
        data: {
            datasets: [{
                data: [
                    <?php DataValue($data->datamarketing) ?>
                ],
                backgroundColor: [
                    <?php GetColors(count($data->datamarketing)) ?>
                ],
                label: 'Data Invoice Terbayar'
            }],
            labels: [
                <?php NamaMarketing($data->datamarketing) ?>
            ]
        },
        options: {
            legend: {
                display: false
            },
            responsive: true
        }
    };
    window.onload = function() {
        var ctx = document.getElementById("chart-area").getContext("2d");
        window.myPie = new Chart(ctx, config);
    };
    
    var colorNames = Object.keys(window.chartColors);
    document.getElementById('addDataset').addEventListener('click', function() {
        var newDataset = {
            backgroundColor: [],
            data: [],
            label: 'New dataset ' + config.data.datasets.length,
        };
        for (var index = 0; index < config.data.labels.length; ++index) {
            newDataset.data.push(randomScalingFactor());
            var colorName = colorNames[index % colorNames.length];;
            var newColor = window.chartColors[colorName];
            newDataset.backgroundColor.push(newColor);
        }
        config.data.datasets.push(newDataset);
        window.myPie.update();
    });
    document.getElementById('removeDataset').addEventListener('click', function() {
        config.data.datasets.splice(0, 1);
        window.myPie.update();
    });
    </script>
</body>
</html>
<?php function Rows($value){ ?>
	<?php if (count($value)=='0'): ?>
		<tr>
			<td colspan="3">Tidak ada dalam database kami.</td>
		</tr>
	<?php else: ?>
        <?php $totalomset = 0;$totalterbayar = 0; ?>
        <?php foreach ($value as $key): ?>
            <?php $totalomset += $key->omset; $totalterbayar += $key->invoice_terbayar; ?>    
        <?php endforeach ?>

		<?php foreach ($value as $key): ?>
			<tr>
				<td><a href="<?= 'personal?m='.$key->aidi ?>" title="<?= $key->nama ?>"><?= $key->nama ?></a></td>
				<td style="text-align:right;vertical-align:middle;"><?= number_format($key->omset) .' IDR' ?></td>
                <td style="text-align:center;vertical-align:middle;">
                    <?= number_format((float)$key->omset/$totalomset*100,2,'.','') ?> %
                </td>
                <td style="text-align:right;vertical-align:middle;"><?= number_format($key->invoice_terbayar). ' IDR' ?></td>
                <?php if ($totalterbayar>0): ?>
                    <td style="text-align:center;vertical-align:middle;">
                        <?= number_format((float)$key->invoice_terbayar/$totalterbayar*100,2,'.','') ?> %
                    </td>
                <?php else: ?>
                    <td style="text-align:center;vertical-align:middle;">
                        <?= '0' ?> %
                    </td>
                <?php endif ?>
			</tr>
		<?php endforeach ?>
            <tr>
                <td style="vertical-align:middle;"><b>Total</b></td>
                <td style="text-align: right;vertical-align:middle;"><b><?= number_format($totalomset) .' IDR' ?></b></td>
                <?php if ($totalomset>0): ?>
                    <td class="tengah" style="vertical-align:middle;">
                        <?= number_format((float)$totalomset/$totalomset*100,2,'.','') ?> %
                    </td>
                <?php else: ?>
                    <td class="tengah" style="vertical-align:middle;">
                        <?= '0' ?> %
                    </td>
                <?php endif ?>
                    
                <td style="text-align: right;vertical-align:middle;"><b><?= number_format($totalterbayar) . ' IDR' ?></b></td>
                <?php if ($totalterbayar>0): ?>
                    <td class="tengah" style="vertical-align:middle;">
                        <?= number_format((float)$totalterbayar/$totalterbayar*100,2,'.','') ?> %
                    </td>
                <?php else: ?>
                    <td class="tengah" style="vertical-align:middle;">
                        <?= '0' ?> %
                    </td>
                <?php endif ?>
                    
            </tr>
	<?php endif ?>
<?php } ?>
