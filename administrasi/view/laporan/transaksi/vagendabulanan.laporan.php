<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.js"></script>
    <script src="<?= $data->base_url ?>js/charts/util.js"></script>
<div class="container" id="isi">
  <div class="page-header" id="top-logo">
    <h3><?= $data->headertext ?></h3>
  </div>

    

  <div class="row">
        <div class="col-md-12">
            <div class="col-md-6">
                <h3 style="text-align:center;"><b>Bulan Lalu</b></h3>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Nama Agenda</th>
                            <th>Jumlah</th>
                            <th>Persentase</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (count($data->listdatalastmonth)=='0'): ?>
                            <tr>
                                <td colspan="2" class="tengah">Tidak ada data agenda di dalam database kami</td>
                            </tr>
                        <?php else: ?>
                            <?php $total=0; ?>
                            <?php foreach ($data->listdatalastmonth as $value =>$key): ?>
                                <?php $total += intval($key['jumlah']) ?>
                            <?php endforeach ?>
                            <?php foreach ($data->listdatalastmonth as $value => $key): ?>
                                <tr>
                                    <td><?= $key['nama'] ?></td>
                                    <td><?= $key['jumlah'] ?></td>
                                    <td><?= number_format((float)$key['jumlah']/$total * 100,2,'.','')  ?> %</td>
                                </tr>
                            <?php endforeach ?>
                        <?php endif ?>
                            
                    </tbody>
                </table>
          </div>
          <div class="col-md-6">
                <h3 style="text-align:center;"><b>Bulan Ini</b></h3>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Nama Agenda</th>
                            <th>Jumlah</th>
                            <th>Persentase</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $total = 0; ?>
                        <?php if (count($data->listdata)=='0'): ?>
                            <tr>
                                <td colspan="2" class="tengah">Tidak ada data agenda di dalam database kami</td>
                            </tr>
                        <?php else: ?>
                            <?php foreach ($data->listdata as $value=>$key): ?>
                                <?php $total += intval($key['jumlah']) ?>
                            <?php endforeach ?>

                            <?php foreach ($data->listdata as $value => $key): ?>
                                <tr>
                                    <td><?= $key['nama'] ?></td>
                                    <td><?= $key['jumlah'] ?></td>
                                    <td><?= number_format((float)$key['jumlah']/$total * 100,2,'.','')  ?> %</td>
                                </tr>
                            <?php endforeach ?>
                        <?php endif ?>
                        
                    </tbody>
                </table>
          </div>
        </div> 
    </div>

</div><!--end container-->
<script type="text/javascript" src='https://fac-institute.com/js/jquery-ui/jquery-ui.js'></script>
<script>
    window.onload = function() {
        
    };
</script>
</body>
</html>

<?php 
function RemoveSlash($value){
    $hasil = '';
    $hasil = str_replace('/','',$value);
    $hasil = str_replace('-', '', $hasil);
    return $hasil;  
}


function GetJumlah($value=''){
    $hasil = '';
    foreach($value as $key){
        $hasil .= $key['jumlah'].',';
    }
    return $hasil;
}

function GetLabel($value=''){
    $hasil = '';
    foreach($value as $key){
        $hasil .= '"'.$key['nama'].'",';
    }
    return $hasil;
}
function WarnaDiagram($value=''){
	$warna = array('red','orange','yellow','green','blue','purple','black','silver','maroon','olive','olivegreen','khaki','chocolate','slategrey','deeppink','magenta','thisle','cadetblue','cyan','mediumtortoquise','paleturquoise','seagreen','indigo','royalblue','wheat','brown','lightsteelblue','tan','salmon','yellowgreen','grey','navy');
		// $arr = array();
    $hasil = '';
	for ($i = 0; $i < $value; $i++) {
	    if($i==$value-1){
	        $hasil .= 'window.chartColors.'.$warna[$i];
	    }else{
	        $hasil .= 'window.chartColors.'.$warna[$i].',';
	    }
		
	}
	return $hasil;
}
?>