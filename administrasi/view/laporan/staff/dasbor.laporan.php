<!doctype html>
<html>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/view/main-component/header.php'; ?>


<body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.js"></script>
<script src="<?= $data->base_url ?>js/charts/util.js"></script>
<?php include_once $data->homedir.'administrasi/view/main-component/top-nav.php'; ?>
<div class="container" style="padding-bottom:40px;">
    
	<div class="row">
		<div class="page-header">
			<h2><?= $data->subtitle ?></h2>
		</div>
	</div>

    <div class="row" style="text-align:right;">
        <div class="col-md-12">
            <a data-toggle="modal" href="#<?= $data->modal->getIdmodal() ?>" class="btn btn-danger">Cari Data?</a>
        </div>
    </div><br>

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12">
            
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
            
        </div>
    </div>
    <br><br>
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-12">
    		<div id="canvas-holder" style="width:100%;overflow:scroll;">
                <canvas id="chart-area" />
            </div>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="tengah">Kegiatan</th>
                        <th>Jumlah</th>
                    </tr>
                </thead>
                <tbody>
                    <?php RowsKegiatan($data->datakegiatan) ?>
                </tbody>
            </table>
            
		</div>
	   <div class="col-lg-6 col-md-6 col-sm-12">
            <h2 style="text-align:center;">Laporan Peran Staff (Bulan Ini)</h2>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="tengah">Nama Staff</th>
                        <th><a href="<?= $data->base_url.'administrasi/laporan/staff/chart/laporanperan?idp=1' ?>" title="">Utama</a></th>
                        <th>
                            <a href="<?= $data->base_url.'administrasi/laporan/staff/chart/laporanperan?idp=2' ?>" title="">
                                Pendamping
                            </a>
                        </th>
                        <th>
                            <a href="<?= $data->base_url.'administrasi/laporan/staff/chart/laporanperan?idp=3' ?>" title="">
                                Peninjau
                            </a>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($data->dataperancurmonth)=='0'): ?>
                        <tr>
                            <td colspan="4">Tidak ada data dalam database kami.</td>
                        </tr>
                    <?php else: ?>
                        <?php foreach ($data->dataperancurmonth as $key): ?>
                            <tr>
                                <td><a href="personal?usr=<?= $key->aidi.$data->nextlink ?>" title=""><?= $key->nama ?></a></td>
                                <td><?= $key->jumlah_utama ?></td>
                                <td><?= $key->jumlah_pendamping ?></td>
                                <td><?= $key->jumlah_peninjau ?></td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                </tbody>
            </table>
            <br>
            <h2 style="text-align:center;">Laporan Peran Staff (Laporan Harian)</h2>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="tengah">Nama Staff</th>
                        <th><a href="<?= $data->base_url.'administrasi/laporan/staff/chart/laporanperan?idp=1' ?>" title="">Utama</a></th>
                        <th>
                            <a href="<?= $data->base_url.'administrasi/laporan/staff/chart/laporanperan?idp=2' ?>" title="">
                                Pendamping
                            </a>
                        </th>
                        <th>
                            <a href="<?= $data->base_url.'administrasi/laporan/staff/chart/laporanperan?idp=3' ?>" title="">
                                Peninjau
                            </a>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($data->dataperan)=='0'): ?>
                        <tr>
                            <td colspan="4">Tidak ada data dalam database kami.</td>
                        </tr>
                    <?php else: ?>
                        <?php foreach ($data->dataperan as $key): ?>
                            <tr>
                                <td><a href="personal?usr=<?= $key->aidi.$data->nextlink ?>" title=""><?= $key->nama ?></a></td>
                                <td><?= $key->jumlah_utama ?></td>
                                <td><?= $key->jumlah_pendamping ?></td>
                                <td><?= $key->jumlah_peninjau ?></td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                </tbody>
            </table>
       </div>
    </div>
	

</div>

<?php ModalHead($data->modal) ?>
    <div class="col-md-12">
        <form action="" method="get">        
        <div class="form-group">
            <label>Tanggal Awal</label>
            <input type="text" name="fy" id="tglawal" placeholder="" class="form-control">
        </div>

        <div class="form-group">
            <label>Tanggal Akhir</label>
            <input type="text" name="ty" id="tglakhir" placeholder="" class="form-control">
        </div>
        <button class="btn btn-lg btn-primary" style="width:100%;">Submit</button>
        </form>
    </div>
<?php ModalFooter($data->modal) ?> 
    <script type="text/javascript" src='https://fac-institute.com/js/jquery-ui/jquery-ui.js'></script>
    <script>
    $(document).ready(function(){
        $('#tglawal').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('#tglakhir').datepicker({
            dateFormat: 'yy-mm-dd'
        });
    });

    var randomScalingFactor = function() {
        return Math.round(Math.random() * 100);
    };
    var config = {
        type: 'pie',
        data: {
            datasets: [{
                data: [
                    <?php $data->DataValue($data->datakegiatan) ?>
                ],
                backgroundColor: [
                    <?php $data->GetColors(count($data->datakegiatan)) ?>
                ],
                label: 'Data Kegiatan Staff'
            }],
            labels: [
                <?php $data->NamaKegiatan($data->datakegiatan) ?>
            ]
        },
        options: {
            responsive: true
        }
    };

    window.onload = function() {
        var ctx = document.getElementById("chart-area").getContext("2d");
        window.myPie = new Chart(ctx, config);
    };
    
    var colorNames = Object.keys(window.chartColors);
    </script>


    <script>
    
    </script>
</body>
</html>
<?php function RowsKegiatan($value){ ?>
	<?php if (count($value)=='0'): ?>
		<tr>
			<td colspan="2">Tidak ada data dalam database kami.</td>
		</tr>
	<?php else: ?>
		<?php foreach ($value as $key): ?>
			<tr>
				<td><a href="#" title=""><?= $key->lkk_ket ?></a></td>
				<td><?= $key->jumlah ?></td>
			</tr>
		<?php endforeach ?>
	<?php endif ?>
<?php } ?>
<?php function RowsPeran($value){ ?>
    
<?php } ?>
<?php 


function NamaJenisAccurate($value){
    foreach ($value as $key) {
        echo '"'.$key->va_nama.'",';
    }
}
?>