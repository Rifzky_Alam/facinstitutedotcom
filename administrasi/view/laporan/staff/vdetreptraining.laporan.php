<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<style>
.btn {
    margin-right :4px;
    margin-bottom:4px;
}
</style>
<body>

<?php
  include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?= $data->subtitle ?></h3>
	</div>
	<div class="row" style="overflow:auto;">



        <div class="col-md-12">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="tengah">Nama Usaha</th>
                        <th class="tengah">Tanggal Transaksi</th>
                        <th class="tengah">Nomor Invoice</th>
                        <th class="tengah">Tanggal Training</th>
                        <th class="tengah">Nama Trainer</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($data->listdata)=='0'): ?>
                        <tr>
                            <td colspan="4" style="text-align:center;">Belum ada data tersedia</td>
                        </tr>
                    <?php else: ?>
                        <?php foreach ($data->listdata as $key): ?>
                            <tr>
                              <td><?= $key['nama_usaha'] ?> </td>
                              <td><?= $key['trans_date'] ?></td>
                              <td><?= $key['no_inv'] ?></td>
                              <td><?= $key['tanggal_training'] ?></td>
                              <td><?= $key['nama'] ?></td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>

                </tbody>
            </table>
        </div>


    </div>

</div><!--end container-->


</body>
</html>
