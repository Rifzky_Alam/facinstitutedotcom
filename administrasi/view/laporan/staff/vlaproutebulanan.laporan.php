<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
  <div class="page-header" id="top-logo">
    <h3><?= $data->headertext ?></h3>
  </div>

    

  <div class="row">
        <div class="col-md-12">
          <section>
            <h5>Menu</h5>
            <div class="list-group">
              <a href="<?= $data->base_url.'administrasi/laporan/staff/bulanan.php/byperan' ?>" class="list-group-item">
                  Laporan By Peran
                  </a>
              <a href="<?= $data->base_url.'administrasi/laporan/staff/bulanan.php/byjenisacc' ?>" class="list-group-item">
                  Laporan By Jenis Accurate
              </a>
              <a href="<?= $data->base_url.'administrasi/laporan/staff/bulanan.php/byjenisusaha' ?>" class="list-group-item">
                Laporan By Jenis Usaha
              </a>
              <a href="<?= $data->base_url.'administrasi/laporan/staff/bulanan.php/bystatuskerja' ?>" class="list-group-item">
                Laporan By Status Kerja
              </a>
              <a href="<?= $data->base_url.'administrasi/laporan/staff/bulanan.php/bywilayah' ?>" class="list-group-item">
                Laporan By Wilayah
              </a>
              <a href="<?= $data->base_url.'administrasi/laporan/staff/bulanan.php/bytransport' ?>" class="list-group-item">
                Laporan By Alat Transport
              </a>
              
            </div>
          </section>
          
        </div> 
    </div>

</div><!--end container-->
<script type="text/javascript" src='https://fac-institute.com/js/jquery-ui/jquery-ui.js'></script>

</body>
</html>