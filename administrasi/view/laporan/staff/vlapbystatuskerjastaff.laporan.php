<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.js"></script>
    <script src="<?= $data->base_url ?>js/charts/util.js"></script>
<div class="container" id="isi">
  <div class="page-header" id="top-logo">
    <h3><?= $data->headertext ?></h3>
  </div>

    

  <div class="row">
        <div class="col-md-12">
            <div class="col-md-6">
                <h3 style="text-align:center;"><b>Bulan Lalu</b></h3>
                <?php foreach ($data->listdatalastmonth as $value => $key) { ?>
                    <?php foreach ($key as $val=>$k) { ?>
                <div class="chart-container" style="position: relative;">
                    <canvas <?= 'id="chartlast'.str_replace(' ','',$val).'"' ?> ></canvas>
                </div>
                <br/>
                <section>
                    <h5><b><?= ucwords($val) ?></b></h5>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th class="tengah">Nama Staff</th>
                                    <th class="tengah">Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($k as $valz) { ?>
                                    <tr>
                                        <td><?= $valz['nama_staff'] ?></td>
                                        <td><?= $valz['jumlah'] ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                </section>
                <br/>
            <?php } ?>                
          <?php } ?>
          </div>
          <div class="col-md-6">
                <h3 style="text-align:center;"><b>Bulan Ini</b></h3>
                <?php foreach ($data->listdata as $value => $key) { ?>
                    <?php foreach ($key as $val=>$k) { ?>
                <div class="chart-container" style="position: relative;">
                    <canvas <?= 'id="chart'.str_replace(' ','',$val).'"' ?> ></canvas>
                </div>
                <br/>
                <section>
                    <h5><b><?= ucwords($val) ?></b></h5>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th class="tengah">Nama Staff</th>
                                    <th class="tengah">Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($k as $valz) { ?>
                                    <tr>
                                        <td><?= $valz['nama_staff'] ?></td>
                                        <td><?= $valz['jumlah'] ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                </section>
                <br/>
            <?php } ?>                
          <?php } ?>
          </div>
        </div> 
    </div>

</div><!--end container-->
<script type="text/javascript" src='https://fac-institute.com/js/jquery-ui/jquery-ui.js'></script>
<script>
    window.onload = function() {
        <?php foreach ($data->listdata as $value => $key) { ?>
            <?php foreach ($key as $val=>$k) { ?>
                <?php 
                    echo 'var config'.str_replace(' ','',$val).' = { '.
                    'type: "pie",'.
                    'showTooltips: true,'.
                    'data: { '.
                    'datasets: [{ '.
                    'data: ['.GetJumlah($k).'],'.
                    'backgroundColor: ['.WarnaDiagram(count($k)).'],'.
                    'label: "Data '.$val.' Bulan Ini"'.
                    '}], '.
                    'labels: ['.GetLabel($k).']'.
                    '}, '.
                    'options: {'.
                    'legend: {'.
                    'display: false'.
                    '},'.
                    'responsive: true'.
                    '}'.
                    '};';
                
                
                
                    echo 'var ct'.str_replace(' ','',$val).' =  document.getElementById("chart'.str_replace(' ','',$val).'").getContext("2d");   ';
                    echo 'window.myPie = new Chart(ct'.str_replace(' ','',$val).', config'.str_replace(' ','',$val).');';
                ?>
            <?php } ?>
        <?php } ?> 
        <?php foreach ($data->listdatalastmonth as $value => $key) { ?>
            <?php foreach ($key as $val=>$k) { ?>
                <?php 
                    echo 'var configlast'.str_replace(' ','',$val).' = { '.
                    'type: "pie",'.
                    'showTooltips: true,'.
                    'data: { '.
                    'datasets: [{ '.
                    'data: [ '.GetJumlah($k).' ],'.
                    'backgroundColor: ['.WarnaDiagram(count($k)).'],'.
                    'label: "Data '.$val.' Bulan Ini"'.
                    '}], '.
                    'labels: ['.GetLabel($k).']'.
                    '}, '.
                    'options: {'.
                    'legend: {'.
                    'display: false'.
                    '},'.
                    'responsive: true'.
                    '}'.
                    '};';
                
                
                
                    echo 'var ctlast'.str_replace(' ','',$val).' =  document.getElementById("chartlast'.str_replace(' ','',$val).'").getContext("2d");   ';
                    echo 'window.myPie = new Chart(ctlast'.str_replace(' ','',$val).', configlast'.str_replace(' ','',$val).');';
                ?>
            <?php } ?>
        <?php } ?>
    };
</script>
</body>
</html>

<?php 
function GetJumlah($value=''){
    $hasil = '';
    foreach($value as $key){
        $hasil .= $key['jumlah'].',';
    }
    return $hasil;
}

function GetLabel($value=''){
    $hasil = '';
    foreach($value as $key){
        $hasil .= '"'.$key['nama_staff'].'",';
    }
    return $hasil;
}
function WarnaDiagram($value=''){
	$warna = array('red','orange','yellow','green','blue','purple','black','silver','maroon','olive','olivegreen','khaki','chocolate','slategrey','deeppink','magenta','thisle','cadetblue','cyan','mediumtortoquise','paleturquoise','seagreen','indigo','royalblue','wheat','brown','lightsteelblue','tan','salmon','yellowgreen','grey','navy');
		// $arr = array();
    $hasil = '';
	for ($i = 0; $i < $value; $i++) {
	    if($i==$value-1){
	        $hasil .= 'window.chartColors.'.$warna[$i];
	    }else{
	        $hasil .= 'window.chartColors.'.$warna[$i].',';
	    }
		
	}
	return $hasil;
}
?>