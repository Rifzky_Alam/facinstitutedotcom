<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title><?= $data->title ?></title>
  <?= $data->View('main-component/links',$data); ?>
    <?php DatePicker($data->base_url) ?>
</head>

<body>

<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->subtitle. ' ~ '. $data->namastaff ?></h3>
	</div>
	
     <div class="row" style="text-align:right;">
        <div class="col-md-12">
            <a data-toggle="modal" href="#<?= $data->modal->getIdmodal() ?>" class="btn btn-danger">Cari Data?</a>
        </div>
    </div><br>


	
	<div class="row">
		<div class="col-md-12">
        <table class="table table-bordered">
          <tbody>
            <tr>
              <td>Biaya Transport</td>
              <td><?= $data->biayatransport ?></td>
            </tr>
            <tr>
              <td>Biaya Makan Siang</td>
              <td><?= $data->makansiang ?></td>
            </tr>
            <tr>
              <td>Biaya Makan Malam</td>
              <td><?= $data->makanmalam ?></td>
            </tr>
            <tr>
              <td>Biaya Parkir</td>
              <td><?= $data->biayaparkir ?></td>
            </tr>
            <tr>
              <td>Biaya Kesehatan</td>
              <td><?= $data->biayakesehatan ?></td>
            </tr>
            <tr>
              <td>Biaya Allowance</td>
              <td><?= $data->biayaallowance ?></td>
            </tr>
            <tr>
              <td>Biaya Lainnya</td>
              <td><?= $data->biayalain ?></td>
            </tr>
          </tbody>
        </table>   

        <a href="personal?usr=<?= $data->usr ?>" title="<< Kembali Ke Laporan" class="btn btn-lg btn-warning"><< Kembali Ke Laporan</a>   
		</div>
        
	</div>

</div>
<?php ModalHead($data->modal) ?>
    <div class="col-md-12">
        <form action="" method="get">        
        <div class="form-group">
            <label>Tanggal Awal</label>
            <input type="hidden" name="usr" value="<?= $data->usr ?>">
            <input type="text" name="fy" id="tglawal" placeholder="" class="form-control">
        </div>

        <div class="form-group">
            <label>Tanggal Akhir</label>
            <input type="text" name="ty" id="tglakhir" placeholder="" class="form-control">
        </div>
        <button class="btn btn-lg btn-primary" style="width:100%;">Submit</button>
        </form>
    </div>
<?php ModalFooter($data->modal) ?> 
    
    <script>
      $(document).ready(function(){
        $('#tglawal').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('#tglakhir').datepicker({
            dateFormat: 'yy-mm-dd'
        });
    });

    </script>
     
</body>
</html> 