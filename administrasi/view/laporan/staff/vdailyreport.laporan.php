<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?= $data->headertext ?></h3>
	</div>


	<div class="row" style="overflow:auto;">
        <div class="col-md-12">
            <a id="cari-data" href="#modal-cariz" data-toggle="modal" class="btn btn-danger">Download</a> <a data-toggle="modal" href="#modal-cari" class="btn btn-info">Cari Data</a><br><br>
            <table>
                <tbody>
                    <tr>
                        <td>Laporan Belum Terlapor</td><td style="padding-left:5px;"> : </td><td id="worning" style="padding-left:10px;">0</td>
                    </tr>
                    <tr>
                        <td>Laporan Terlambat </td><td style="padding-left:5px;"> : </td><td id="bloman" style="padding-left:10px;">0</td>
                    </tr>
                    <tr>
                        <td>Total Insentif</td><td style="padding-left:5px;"> : </td><td id="totalinsentif" style="padding-left:10px;">Belum Tersedia</td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="text-align:center;">Tanggal Laporan</th>
                        <th style="text-align:center;">Perusahaan</th>
                        <th style="text-align:center;">Insentif</th>
                        <th style="text-align:center;">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($data->listdata)=='0'): ?>
                        <tr>
                            <td colspan="4" style="text-align:center;">Tidak Ada data laporan di bulan ini, silahkan untuk mencari data berdasarkan bulan dan tahun terlebih dahulu.</td>
                        </tr>
                    <?php else: ?>
                      <?php $totalinss = 0; ?>
                        <?php foreach ($data->listdata as $key): ?>
                            <?php 
                              if ($key['statuskerja']=='1') {
                                $persentaseluarkota = '1';
                              }else{
                                $persentaseluarkota = $key['is_ttl_attnd'];
                              }
                            ?>
                            <?php if ($key['peran']=='' && strtotime($key['tanggal']) > strtotime('-3 day')): ?>
                                <tr class="warning">
                            <?php elseif($key['peran']=='' && strtotime($key['tanggal']) < strtotime('-3 day')): ?>
                                <tr class="danger">
                            <?php elseif($key['status_laporan']=='5'): ?>
                                <tr class="success">
                            <?php else: ?>
                                <tr>
                            <?php endif ?>
                                <td style="vertical-align:middle;"><?= $key['tanggal'] ?></td>
                                <td style="vertical-align:middle;"><?= $key['nama'] ?></td>
                                <?php $tarifaccurate = KalkulasiInsentifAcc($key['tarif_accurate'],$key['tarif_peran'],$key['ijt_acc']); ?>
                                <?php $tarifharikerja = KalkulasiInsentifHaker($key['tarif_hari_kerja'],$key['ijt_hari']); ?>
                                <?php $tarifwilayah = KalkulasiInsentifLuarKota($key['tarif_wilayah'],$key['ijt_wilayah'],$persentaseluarkota) ?>
                                <td style="text-align:right;vertical-align: middle;">
                                  <a href="<?= $data->base_url.'administrasi/user/laporan-harian?detin='.$key['id'] ?>" title="Detail">
                                    <?= number_format($tarifaccurate + $tarifharikerja + $tarifwilayah). ' IDR' ?>
                                  </a>
                                </td>
                                <?php $totalinss+= $tarifaccurate + $tarifharikerja + $tarifwilayah ?>
                                <td style="text-align:center;vertical-align:middle;">
                                  <?php if ($key['status_laporan']=='5'): ?>
                                  <button class="btn btn-primary" disabled>Buat Laporan</button>
                                  <?php else: ?>
                                  <a href="<?= $data->base_url.'administrasi/user/laporan-harian?id='.$key['id'] ?>" class="btn btn-primary" target="_blank">Buat Laporan</a>
                                  ||
                                  <a href="<?= $data->base_url.'administrasi/user/reimupld?id='.$key['id'] ?>" class="btn btn-info" target="_blank">Upload Reimburse~</a>
                                  <?php endif ?>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                <input type="hidden" value="<?= number_format(@$totalinss).' IDR' ?>" id="tots">                    
                </tbody>
            </table>  
        </div> 
    </div>

</div><!--end container-->

<!-- Modal -->
  <div class='modal fade' id='modal-cari' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pencarian Data</h3>

        </div>
        <div class='modal-body'>
        <div style='padding-left:15px;padding-right:15px;padding-top:0px; height: 300px;overflow-y:auto;' class='row'>
          


          <div class="col-md-12">
            <form action="" method="get" accept-charset="utf-8">
                <div class="form-group">
                    <label>Bulan</label>
                    <input type="text" name="src[bln]" class="form-control" placeholder="Hanya Angka: 01">
                </div>
                <div class="form-group">
                    <label>Tahun</label>
                    <input type="text" name="src[thn]" class="form-control" placeholder="Hanya Angka: 2018">
                </div>
                <button class="btn btn-lg btn-primary">Search</button> 
            </form>
          </div>
             
          

        </div>
          

        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>FAC-Institute 2016</span>
                </div>
                <div class="col-md-6">
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->
  <!-- Modal -->
  <div class='modal fade' id='modal-cariz' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pencarian Data</h3>

        </div>
        <div class='modal-body'>
        <div style='padding:15px; height: 300px;' class='row'>
          
          <div class="col-md-12">
            
              

                    <div class="row">
                        <div class="form-group">
                            <label for="dari-tanggal">Mulai Tanggal</label>
                            <input type="text" name="tanggalAwal" id="tanggal-awal" class="form-control" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label for="dari-tanggal">Sampai Tanggal</label>
                            <input type="text" name="tanggalAkhir" id="tanggal-akhir" class="form-control" />
                        </div>
                    </div>
                  

                    <div class="row">
                        <button class="btn btn-primary" id='btn-downloadLaporan'>Download Laporan</button>
                    </div>

                
          </div>
             
          

        </div>
          

        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>FAC-Institute 2016</span>
                </div>
                <div class="col-md-6">
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->
  <script type="text/javascript" src=<?php echo "'".$data->base_url."js/jquery-ui/jquery-ui.js'"; ?>></script>
  <script>

    $(document).ready(function(){
      var jumlahlaporantelat = $("tr[class=danger]").length;
      var jumlahbelomterlapor = $("tr[class=warning]").length;
        $('#tanggal-awal').datepicker({
            dateFormat: 'yy-mm-dd'
        });

      $('#tanggal-akhir').datepicker({
        dateFormat: 'yy-mm-dd'
      });
      $('#bloman').html(jumlahlaporantelat);
      $('#worning').html(jumlahbelomterlapor);
      $('#totalinsentif').html($('#tots').val());

    });
      $('#btn-downloadLaporan').click(function(){

        var tanggalAwalLapor = $('#tanggal-awal').val();
        var tanggalAkhirLapor = $('#tanggal-akhir').val();
                
        window.open(<?= "'".$data->base_url."administrasi/'" ?>+'download-excel?data[username]=' + <?php echo "'".$_SESSION['admin']['username']."'"; ?>  + '&data[tanggalAwal]=' + tanggalAwalLapor + '&data[tanggalAkhir]='+ tanggalAkhirLapor);
    });
  </script>
</body>
</html> 