<!DOCTYPE html>
<html>
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/view/main-component/header.php'; ?>

<body>

<?php 
include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/view/main-component/sidebar.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->subtitle ?></h3>
	</div>
	
     
  <div class="row">
    <div class="col-md-12">
      <table>
        <tbody>
          <tr>
            <td style="padding-right:15px;">Utama</td><td style="padding-right:10px;">:</td><td style="padding-left: :5px;" id="totalutama"></td>
          </tr>
          <tr>
            <td style="padding-right:15px;">Pendamping</td><td style="padding-right:10px;">:</td><td style="padding-left: :5px;" id="totalpendamping"></td>
          </tr>
          <tr>
            <td style="padding-right:15px;">Peninjau</td><td style="padding-right:10px;">:</td><td style="padding-left: :5px;" id="totalpeninjau"></td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
	
	<div class="row" style="overflow:auto;">
		<div class="col-md-12">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama Perusahaan</th>
              <th>Tanggal</th>
              <th>Jenis Hari Kerja</th>
              <th>Hari</th>
              <th>Kegiatan</th>
              <th>Keterangan Kegiatan</th>
              <th>Peserta Kegiatan</th>
              <th>Status Kerja</th>
              <th>Peran</th>
              <th>Rekan</th>
              <th>Jenis Transportasi</th>
            </tr>
          </thead>
          <tbody>
            <?php if (count($data->datalap)=='0'): ?>
              <tr>
                <td colspan="11" style="text-align:center;">Tidak ada data dalam database kami.</td>
              </tr>
            <?php else: ?>
              <?php $no = 1; ?>
              <?php $totalutama=0;$totalpendamping=0;$totalpeninjau=0; ?>
              <?php foreach ($data->datalap as $key): ?>
                <tr>
                  <td><?= $no ?></td>
                  <td>
                    <a href="<?= $data->base_url.'administrasi/new-usaha?id='.$key->id_usaha ?>" title="">
                      <?= $key->nama ?>    
                    </a>
                  </td>
                  <td><?= $key->tanggal ?></td>
                  <td><?= $key->jns_hari_kerja ?></td>
                  <td><?= $key->hari ?></td>
                  <td><?= $data->CekKeterangan($key->kegiatan,$data->datakegiatan) ?></td>
                  <td><?= $key->ket_kegiatan ?></td>
                  <td><?= $key->peserta_kegiatan ?></td>
                  <td><?= $key->status_kerja ?></td>
                  <td><?= $key->perankerja ?></td>
                  <?php 
                    if ($key->peran=='1') {
                      $totalutama +=1;
                    } elseif ($key->peran=='2') {
                      $totalpendamping +=1;
                    } elseif ($key->peran=='3') {
                      $totalpeninjau +=1;
                    }
                  ?>
                  <td><?= $data->CekRekan($key->rekan,$data->users) ?></td>
                  <td><?= $key->jns_transportasi ?></td>
                </tr>
                <?php $no++; ?>
              <?php endforeach ?>
            <?php endif ?>
            <input type="hidden" id="tutama" value="<?= $totalutama ?>">
            <input type="hidden" id="tpendamping" value="<?= $totalpendamping ?>">
            <input type="hidden" id="tpeninjau" value="<?= $totalpeninjau ?>">
          </tbody>
        </table>
		</div>
        
	</div>

</div>
<script>
  $(document).ready(function(){
    $('#totalutama').html($('#tutama').val());
    $('#totalpendamping').html($('#tpendamping').val());
    $('#totalpeninjau').html($('#tpeninjau').val());
  });
</script>
     
</body>
</html> 