<!DOCTYPE html>
<html>
<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>

<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->subtitle ?></h3>
	</div>
	
     

	
	<div class="row">
		<div class="col-md-12">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>No</th>
              <th>Tanggal Absen</th>
              <th>Nama Perusahaan</th>
            </tr>
          </thead>
          <tbody>
            <?php if (count($data->datalap)=='0'): ?>
              <tr>
                <td colspan="11" style="text-align:center;">Tidak ada data dalam database kami.</td>
              </tr>
            <?php else: ?>
              <?php $no = 1; ?>
              <?php foreach ($data->datalap as $key): ?>
                <tr>
                  <td><?= $no ?></td>
                  <td><?= $key->nama ?></td>
                  <td><?= $key->tanggal_absen ?></td>
                </tr>
                <?php $no++; ?>
              <?php endforeach ?>
            <?php endif ?>
          </tbody>
        </table>      
		</div>
        
	</div>

</div>

     
</body>
</html> 