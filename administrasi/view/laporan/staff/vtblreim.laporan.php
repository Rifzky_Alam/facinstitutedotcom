<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>


<div class="container" id="isi" style="padding-bottom:40px;">
	<div class="page-header" id="top-logo">
		<h3><?= $data->subtitle ?></h3>
	</div>


        <div class='row' style="overflow:auto;">
            <div class='col-md-12'>
                <a href="#modal-cari" data-toggle="modal" title="Search" class="btn btn-danger">Search</a>
                <br><br>
                <table class='table table-bordered'>
                    <thead>
                        <tr>
                            <th class="tengah">Nama Staff</th>
                            <th class="tengah">Jumlah Laporan Reimbursement</th>
                            <th class="tengah">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($data->listdata)=='0'): ?>
                            <tr>
                                <td colspan="3" class="tengah">
                                    TIDAK ADA DATA TERSEDIA.
                                </td>
                            </tr>
                        <?php else: ?>
                            <?php foreach($data->listdata as $key): ?>
                                <tr>
                                    <td><?= $key['nama'] ?></td>
                                    <td class="tengah">
                                        <?= $key['jumlah'] ?>
                                    </td>
                                    <td class="tengah">
                                        <?php if($data->searchbln!=''&&$data->searchthn!=''): ?>
                                            <a href="<?= $data->base_url.'administrasi/user/reimbport?bln='.$data->searchbln.'&thn='.$data->searchthn.'&user='.$key['username'] ?>" class="btn btn-primary">
                                                Mulai Verif
                                            </a>
                                        <?php else: ?>
                                            <a href='#' class="btn btn-primary">
                                                Tentukan Bulan & Tahun Terlebih Dahulu
                                            </a>
                                        <?php endif ?>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>
            </div>
        </div>
        
</div>

<!-- Modal -->
  <div class='modal fade' id='modal-cari' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pencarian Data</h3>

        </div>
        <div class='modal-body'>
        <div style='padding-left:15px;padding-right:15px;padding-top:0px; height: 300px;overflow-y:auto;' class='row'>
          


          <div class="col-md-12">
            <form action="" method="get" accept-charset="utf-8">
                <div class="form-group">
                    <label>Bulan</label>
                    <input type="text" name="src[bln]" class="form-control" placeholder="Hanya Angka: 01">
                </div>
                <div class="form-group">
                    <label>Tahun</label>
                    <input type="text" name="src[thn]" class="form-control" placeholder="Hanya Angka: 2018">
                </div>
                <button class="btn btn-lg btn-primary">Search</button> 
            </form>
          </div>
             
          

        </div>
          

        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>FAC-Institute 2016</span>
                </div>
                <div class="col-md-6">
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->

<script type="text/javascript" src=<?php echo "'".$data->base_url."js/jquery-ui/jquery-ui.js'"; ?>></script>
  <script type="text/javascript">
    // $(document).ready(function(){
    //     $('#setuju').click(function(){
    //         if ($(this).is(':checked')) {
    //             $("#hidform").collapse('hide');
    //         }else{
    //             $("#hidform").collapse('show');
    //         }
    //     });
    // });

    

    function removeElement(id) {
        $('#'+id).remove();
    }

  </script>   
</body>
</html> 


