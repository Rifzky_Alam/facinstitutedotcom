<!DOCTYPE html>
<html>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/view/main-component/header.php'; ?>

<body>

<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->subtitle ?></h3>
	</div>
	
     <div class="row" style="text-align:right;">
        <div class="col-md-12">
            <a data-toggle="modal" href="#<?= $data->modal->getIdmodal() ?>" class="btn btn-danger">Cari Data?</a>
        </div>
    </div><br>

	
	<div class="row">
		<div class="col-md-6">

        <div class="row">
          <div class="col-md-12">
            <h3>Personal Detail</h3>
            <table class="table table-bordered">
              <thead>
                <tr><th colspan="2" style="text-align:center;"><h3><?= $data->namalengkap ?></h3></th></tr>
              </thead>
              <tbody>
                <tr>
                  <td>Tanggal Bergabung</td>
                  <td><?= $data->tglbergabung ?></td>
                </tr>
                <tr>
                  <td>Email</td>
                  <td><?= $data->email ?></td>
                </tr>
                <tr>
                  <td>Telepon</td>
                  <td><?= $data->telepon ?></td>
                </tr>
                <tr>
                  <td>Lahir</td>
                  <td><?= $data->lahir ?></td>
                </tr>
                <tr>
                  <td>Tipe</td> 
                  <td><?= $data->tipe ?></td>
                </tr>
                <tr>
                  <td>Team</td> 
                  <td><?= $data->team ?></td>
                </tr>
                <tr>
                  <td>Jumlah Absen</td>
                  <td><?= $data->totalabsen ?></td>
                </tr>
                <tr>
                  <td>Jumlah Laporan</td> 
                  <td><?= $data->totallaporan ?></td>
                </tr>
                <tr>
                  <td>Total Overtime (Laporan Harian)</td> 
                  <td><?= $data->totalovertime ?></td>
                </tr>
                <tr>
                  <td>Total Kerja di Jabodetabek</td>
                  <?php if (isset($_GET['fy'],$_GET['ty'])): ?>
                    <td><a href="<?= $data->base_url.'administrasi/laporan/staff/dtljabodetabek?fd='.$_GET['fy'].'&ld='.$_GET['ty'].'&usr='.$data->usr ?>" title="Detail Absen"><?= $data->jabodetabek ?></a></td>
                    <?php else: ?>
                    <td><a href="#" title="Detail Absen"><?= $data->jabodetabek ?></a></td>    
                  <?php endif ?>
                </tr>
                <tr>
                  <td>Total Kerja Luar Kota</td>
                  <?php if (isset($_GET['fy'],$_GET['ty'])): ?>
                     <td><a href="<?= $data->base_url.'administrasi/laporan/staff/dtlluarkota?fd='.$_GET['fy'].'&ld='.$_GET['ty'].'&usr='.$data->usr ?>" title="Detail Absen"><?= $data->luarkota ?></a></td>
                     <?php else: ?>
                      <td><a href="#" title="Detail Absen"><?= $data->luarkota ?></a></td>
                   <?php endif ?> 
                  
                </tr>
                <tr>
                  <td>Status Kerja (Sendiri)</td> 
                  <td><?= $data->statuskerja1 ?></td>
                </tr>
                <tr>
                  <td>Status Kerja (Tidak Sendiri)</td> 
                  <td><?= $data->statuskerja2 ?></td>
                </tr>
                <tr>
                  <td>Total Reimbursement (Laporan Harian)</td> 
                  <td><a href="personal-biaya?usr=<?= $data->usr.$data->nextlink ?>" title="Klik untuk detail"><?= number_format($data->totalreimbursement) ?> IDR</a></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>

        <div class="row">
            <div class="col-md-12">
              <h3>Laporan Kegiatan</h3>
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>Kegiatan</th>
                    <th>Jumlah</th>
                  </tr>
                </thead>
                <tbody>
                  <?php RowsKegiatan($data->laporankegiatan) ?>
                </tbody>
              </table>
            </div>
          </div>

        <div class="row">
          <div class="col-md-12">
            <h2 style="text-align:center;">Laporan Per Jenis Usaha</h2>
            <table class="table table-bordered">
                <thead>
                  <tr>
                    <th><b>Jenis Usaha</b></th>
                    <th><b>Jumlah</b></th>
                  </tr>
                </thead>
                <tbody>
                    <?php if (count($data->laporanjenisusaha)=='0'): ?>
                      <tr>
                        <td colspan="2" style="text-align:center;">Tidak Ada Data</td>
                      </tr>
                    <?php else: ?>
                      <?php foreach ($data->laporanjenisusaha as $key): ?>
                        <tr>
                          <td><?= $key->jenis_usaha ?></td>
                          <?php if (isset($_GET['fy'],$_GET['ty'])): ?>
                              <td><a href="<?= $data->base_url.'administrasi/laporan/staff/lapjenisusaha?usr='.$data->usr.'&ju='.$key->aidi.'&fd='.$_GET['fy'].'&ld='.$_GET['ty'] ?>" title="Detail"><?= $key->jumlah ?></a></td>
                            <?php else: ?>
                              <td><a href="<?= $data->base_url.'administrasi/laporan/staff/lapjenisusaha?usr='.$data->usr.'&ju='.$key->aidi ?>" title="Detail"><?= $key->jumlah ?></a></td>
                          <?php endif ?>
                        </tr>
                      <?php endforeach ?>
                    <?php endif ?>
                    
                </tbody>
            </table>
          </div>
        </div>
            
		</div>
        <div class="col-md-6">

          <div class="row">
            <div class="col-md-12">
              <h3>Data jenis Accurate</h3>
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>Jenis Accurate</th>
                    <th>Jumlah</th>
                  </tr>
                </thead>
                <tbody>
                  <?php //RowsJenisAccurate($data->laporanacc,$data->usr) ?>
                  <?php if (count($data->laporanacc)=='0'): ?>
                    <tr>
                      <td colspan="2">Tidak ada dalam database kami.</td>
                    </tr>
                  <?php else: ?>
                    <?php foreach ($data->laporanacc as $key): ?>
                      <tr>
                        <td><?= $key->va_nama ?></td>
                        <?php if (isset($_GET['fy'],$_GET['ty'])): ?>
                          <td><a href="acclaphar?va=<?= $key->va_id ?>&usr=<?= $data->usr ?>&fd=<?= $_GET['fy'] ?>&ld=<?= $_GET['ty'] ?>" title="Detail"><?= $key->jumlah ?></a></td>
                          <?php else: ?>
                        <td><a href="acclaphar?va=<?= $key->va_id ?>&usr=<?= $data->usr ?>" title="Detail"><?= $key->jumlah ?></a></td>
                        <?php endif ?>
                      </tr>
                    <?php endforeach ?>
                  <?php endif ?>
                </tbody>
              </table>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <h3>Data Jenis Transaksi</h3>
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>Jenis Transaksi</th>
                    <th>Jumlah</th>
                  </tr>
                </thead>
                <tbody>
                  <?php RowsJenisTransaksi($data->laporantrans) ?>
                </tbody>
              </table>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <h3>Data Jenis Transportasi</h3>
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>Jenis Transportasi</th>
                    <th>Jumlah</th>
                  </tr>
                </thead>
                <tbody>
                  <?php RowsJenisTransport($data->laporantransport) ?>
                </tbody>
              </table>
            </div>
          </div>

        </div>
	</div>

</div>
<?php ModalHead($data->modal) ?>
    <div class="col-md-12">
        <form action="" method="get">        
        <div class="form-group">
            <label>Tanggal Awal</label>
            <input type="hidden" name="usr" value="<?= $data->usr ?>">
            <input type="text" name="fy" id="tglawal" placeholder="" class="form-control">
        </div>

        <div class="form-group">
            <label>Tanggal Akhir</label>
            <input type="text" name="ty" id="tglakhir" placeholder="" class="form-control">
        </div>
        <button class="btn btn-lg btn-primary" style="width:100%;">Submit</button>
        </form>
    </div>
<?php ModalFooter($data->modal) ?> 
    <script type="text/javascript" src='https://fac-institute.com/js/jquery-ui/jquery-ui.js'></script>
    <script>
      $(document).ready(function(){
        $('#tglawal').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('#tglakhir').datepicker({
            dateFormat: 'yy-mm-dd'
        });
    });

    </script>
     
</body>
</html> 
<?php function RowsJenisAccurate($value,$user){ ?>
  <?php if (count($value)=='0'): ?>
    <tr>
      <td colspan="2">Tidak ada dalam database kami.</td>
    </tr>
  <?php else: ?>
    <?php foreach ($value as $key): ?>
      <tr>
        <td><?= $key->va_nama ?></td>
        <td><a href="acclaphar?va=<?= $key->va_id ?>&usr=<?= $user ?>" title="Detail"><?= $key->jumlah ?></a></td>
      </tr>
    <?php endforeach ?>
  <?php endif ?>
<?php } ?>

<?php function RowsJenisTransaksi($value){ ?>
  <?php if (count($value)=='0'): ?>
    <tr>
      <td colspan="2">Tidak ada dalam database kami.</td>
    </tr>
  <?php else: ?>
    <?php foreach ($value as $key): ?>
      <tr>
        <td><?= $key->jt_ket ?></td>
        <td><?= $key->jumlah ?></td>
      </tr>
    <?php endforeach ?>
  <?php endif ?>
<?php } ?>

<?php function RowsJenisTransport($value){ ?>
  <?php if (count($value)=='0'): ?>
    <tr>
      <td colspan="2">Tidak ada dalam database kami.</td>
    </tr>
  <?php else: ?>
    <?php foreach ($value as $key): ?>
      <tr>
        <td><?= $key->lt_ket ?></td>
        <td><?= $key->jumlah ?></td>
      </tr>
    <?php endforeach ?>
  <?php endif ?>
<?php } ?>

<?php function RowsKegiatan($value){ ?>
  <?php if (count($value)=='0'): ?>
    <tr>
      <td colspan="2">Tidak ada dalam database kami.</td>
    </tr>
  <?php else: ?>
    <?php foreach ($value as $key): ?>
      <tr>
        <td><?= $key->lkk_ket ?></td>
        <td><?= $key->jumlah ?></td>
      </tr>
    <?php endforeach ?>
  <?php endif ?>
<?php } ?>
