<!doctype html>
<html>

<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>
<body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.js"></script>
    <script src="https://fac-institute.com/js/charts/util.js"></script>
<?php
  include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/view/main-component/sidebar.php';
  include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/view/main-component/top-nav.php';
 ?>
<div class="container" style="padding-bottom:40px;">

	<div class="row">
		<div class="page-header">
			<h2><?= $data->subtitle ?></h2>
		</div>
	</div>

    
    <br><br>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">
    		<div id="canvas-holder" style="width:100%;overflow:auto;">
                <canvas id="chart-area" />
            </div>
		</div>
    </div>
    
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Nama Staff</th>
                        <th>Jumlah</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data->listdata as $key): ?>
                        <tr>
                            <td><?= $key['nama_staff'] ?></td>
                            <td class="tengah"><?= $key['jumlah'] ?></td>
                            <td>
                                <?php if (@$data->laman=='jenisacc'): ?>
                                    <a href="<?= $data->base_url.'administrasi/laporan/staff/acclaphar?va='.@$data->idacc.'&usr='.@$key['username'] ?>" title="">Detail</a>
                                <?php elseif(@$data->laman=='jenisusaha'): ?>
                                    <a href="<?= $data->base_url.'administrasi/laporan/staff/lapjenisusaha?usr='.@$key['username'].'&ju='.@$data->idjenisusaha ?>" title="">Detail</a>    
                                <?php endif ?>
                                
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>	

</div>
    
    
    <script>
    var randomScalingFactor = function() {
        return Math.round(Math.random() * 100);
    };
    var config = {
        type: 'pie',
        data: {
            datasets: [{
                data: [
                    <?php 
                        foreach ($data->listdata as $key) {
                            echo $key['jumlah'].',';
                        }
                    ?>
                ],
                backgroundColor: [
                    <?php WarnaDiagram(count($data->listdata)) ?>
                ],
                label: 'Data Laporan Berdasarkan Jenis Usaha'
            }],
            labels: [
                <?php 
                foreach ($data->listdata as $key) {
                    echo '"'.$key['nama_staff'].'",';
                }
                ?>
            ]
        },
        options: {
            responsive: true
        }
    };

    window.onload = function() {
        var ctx = document.getElementById("chart-area").getContext("2d");
        window.myPie = new Chart(ctx, config);

    };
    
    var colorNames = Object.keys(window.chartColors);
    </script>
</body>
</html>
