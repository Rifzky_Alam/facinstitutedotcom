<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?= $data->headertext ?></h3>
	</div>


	<div class="row" style="overflow:auto;">
        <div class="col-md-12">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <td>Insentif Training Accurate</td>
                        <td><?= $data->insentifacc ?></td>
                    </tr>
                    <tr>
                      <td>Insentif Hari Kerja</td>
                      <td><?= $data->insentifharikerja ?></td>
                    </tr>
                    <tr>
                      <td>Insentif Luar Kota</td>
                      <td><?= $data->insentifwilayah ?></td>
                    </tr>                    
                </tbody>
            </table>
            <?php if ($data->userlevel=='0'): ?>
              <p style="color:red;">--Level user belum ditandai.</p>
            <?php endif ?>
            <?php if ($data->ringkasanlaporan=='-'): ?>
              <p style="color:red;">--Belum ada ringkasan insentif.</p>
            <?php endif ?>
            <?php if ($data->transs=='-'): ?>
              <p style="color:red;">--Tidak ada transaksi di ringkasan.</p>
            <?php endif ?>
            <?php if ($data->transs=='-'): ?>
              <p style="color:red;">--Tidak ada transaksi dalam kalender.</p>
            <?php endif ?>
            <?php if ($data->accurate=='0'): ?>
              <p style="color:red;">--Accurate tidak terdeteksi dalam transaksi.</p>
            <?php endif ?>
            <?php if (!is_numeric($data->prov)): ?>
              <p style="color:red;">--Data wilayah perusahaan belum di perbaharui.</p>
            <?php endif ?>

            <form action="" method="post" accept-charset="utf-8">
              <input type="hidden" name="in[token]" value="<?= $data->idlaporan ?>">
              <a href="<?= $data->base_url.'administrasi/user/laporan-harian' ?>" class="btn btn-warning"><< Kembali Ke Laporan</a> 
              <button class="btn btn-primary">Kalkulasi Ulang</button>
            </form>
             
        </div> 
    </div>

</div><!--end container-->

<!-- Modal -->
  <div class='modal fade' id='modal-cari' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pencarian Data</h3>

        </div>
        <div class='modal-body'>
        <div style='padding-left:15px;padding-right:15px;padding-top:0px; height: 300px;overflow-y:auto;' class='row'>
          


          <div class="col-md-12">
            <form action="" method="get" accept-charset="utf-8">
                <div class="form-group">
                    <label>Bulan</label>
                    <input type="text" name="src[bln]" class="form-control" placeholder="Hanya Angka: 01">
                </div>
                <div class="form-group">
                    <label>Tahun</label>
                    <input type="text" name="src[thn]" class="form-control" placeholder="Hanya Angka: 2018">
                </div>
                <button class="btn btn-lg btn-primary">Search</button> 
            </form>
          </div>
             
          

        </div>
          

        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>FAC-Institute 2016</span>
                </div>
                <div class="col-md-6">
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->
  <!-- Modal -->
  <div class='modal fade' id='modal-cariz' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pencarian Data</h3>

        </div>
        <div class='modal-body'>
        <div style='padding:15px; height: 300px;' class='row'>
          
          <div class="col-md-12">
            
              

                    <div class="row">
                        <div class="form-group">
                            <label for="dari-tanggal">Mulai Tanggal</label>
                            <input type="text" name="tanggalAwal" id="tanggal-awal" class="form-control" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label for="dari-tanggal">Sampai Tanggal</label>
                            <input type="text" name="tanggalAkhir" id="tanggal-akhir" class="form-control" />
                        </div>
                    </div>
                  

                    <div class="row">
                        <button class="btn btn-primary" id='btn-downloadLaporan'>Download Laporan</button>
                    </div>

                
          </div>
             
          

        </div>
          

        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>FAC-Institute 2016</span>
                </div>
                <div class="col-md-6">
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->
  
</body>
</html> 