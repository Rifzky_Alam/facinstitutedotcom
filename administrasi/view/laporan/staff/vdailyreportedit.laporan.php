<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?= $data->headertext ?></h3>
	</div>


	<div class="row">
        <div class="col-md-12">

            <section>
                <h3>Perusahaan</h3>
                <p>
                    <span style="font-size:large;" class="label label-primary"><?= $data->mainrow['nama'] ?></span> 
                <a id='laporan-namaPerusahaan' href='#modal-editAbsen' data-toggle='modal' class="btn btn-xs btn-warning">Edit <span class="glyphicon glyphicon-edit"></span></a>
                </p>
            </section>
                

            <form id="myform" action="" method="post">
            <input type="hidden" name="in[insper]" value="<?= $data->insper ?>">
            <input type="hidden" name="in[idut]" value="<?= $data->idutama ?>">
            <div class="form-group">
                <label>Peserta Kegiatan</label>
                <input type="text" name="in[peserta]" id="peserta" value="<?= $data->mainrow['peserta_kegiatan'] ?>" class="form-control" placeholder="Peserta Kegiatan">
            </div>
            <div class="form-group">
                <label>Kegiatan</label>
                <?php $kegiatan = explode(' # ', $data->mainrow['kegiatan']) ?>
                <?php foreach ($data->kegiatan as $key): ?>
                    <div class="checkbox">
                        <?php if (in_array($key['dkl_id'], $kegiatan)): ?>
                            <label>
                                <input checked="checked" class="kegiatanc" required name="in[kegiatan][]" value="<?= $key['dkl_id'] ?>" type="checkbox"> <?= $key['dkl_desc'] ?>
                            </label>
                        <?php else: ?>
                            <label>
                                <input class="kegiatanc" required name="in[kegiatan][]" value="<?= $key['dkl_id'] ?>" type="checkbox"> <?= $key['dkl_desc'] ?>
                            </label>
                        <?php endif ?>
                            
                    </div>
                <?php endforeach ?>
            </div>
            <div class="form-group">
                <label>Keterangan Kegiatan</label>
                <textarea rows="6" id="ketkegiatanid" required class="form-control" name="in[ketkegiatan]"><?= $data->mainrow['ket_kegiatan'] ?></textarea>
            </div>

            <div class="form-group">
                <label>Status Kerja</label>
                <select required name="in[statuskerja]" id="statuskerja" class="form-control">
                    <?php foreach ($data->statuskerjaa as $key): ?>
                        <?php if ($data->tidakbolehsendiri=='1'): ?>
                            <?php if ($data->tidakbolehsendiri==$key['dsk_id']): ?>
                                <option value="" disabled="disabled">Laporan lain ditandai dengan status tidak sendiri</option>    
                            <?php else: ?>
                                <option value="<?= $key['dsk_id'] ?>"><?= $key['dsk_desc'] ?></option> 
                            <?php endif ?>
                        <?php else: ?>
                            <?php if ($data->mainrow['status_kerja']==$key['dsk_id']): ?>
                                <option value="<?= $key['dsk_id'] ?>" selected="selected"><?= $key['dsk_desc'] ?></option>    
                            <?php else: ?>
                                <option value="<?= $key['dsk_id'] ?>"><?= $key['dsk_desc'] ?></option> 
                            <?php endif ?>
                        <?php endif ?>
                    <?php endforeach ?>
                </select>
            </div>

            <?php if ($data->gocal=='1'): ?>
                
                <div class="form-group">
                    <label>Peran Kerja</label>
                    <select required name="in[peran]" id="peran" class="form-control">
                        <?php foreach ($data->perankerjalist as $key): ?>
                            <?php if ($data->perangocal==$key['pk_id']): ?>
                                <option value="<?= $key['pk_id'] ?>" selected="selected">
                                    <?= $key['pk_peran'] ?>
                                </option>
                            <?php else: ?>
                                <?php if ($data->peranutama!=''&&$key['pk_id']=='1'): ?>
                                    <option value="" disabled="disabled">
                                        <?= $key['pk_peran'] ?> (<?= $data->peranutama ?>)
                                    </option>
                                <?php elseif($data->peranpendamping!=''&&$key['pk_id']=='2'): ?>
                                    <option value="" disabled="disabled">
                                        <?= $key['pk_peran'] ?> (<?= $data->peranpendamping ?>)
                                    </option>
                                <?php else: ?>
                                    <option value="<?= $key['pk_id'] ?>" disabled="disabled">
                                        <?= $key['pk_peran'] ?>
                                    </option>
                                <?php endif ?>
                            <?php endif ?>
                        <?php endforeach ?>
                    </select>
                </div>

            <?php else: ?>
                <div class="form-group">
                    <label>Peran Kerja</label>
                    <select required name="in[peran]" id="peran" class="form-control">
                        <?php foreach ($data->perankerjalist as $key): ?>
                            <?php if ($data->mainrow['peran']==$key['pk_id']): ?>
                                <option value="<?= $key['pk_id'] ?>" selected="selected">
                                    <?= $key['pk_peran'] ?>
                                </option>
                            <?php else: ?>
                                <?php if ($data->peranutama!=''&&$key['pk_id']=='1'): ?>
                                    <option value="" disabled="disabled">
                                        <?= $key['pk_peran'] ?> (<?= $data->peranutama ?>)
                                    </option>
                                <?php elseif($data->peranpendamping!=''&&$key['pk_id']=='2'): ?>
                                    <option value="" disabled="disabled">
                                        <?= $key['pk_peran'] ?> (<?= $data->peranpendamping ?>)
                                    </option>
                                <?php else: ?>
                                    <option value="<?= $key['pk_id'] ?>">
                                        <?= $key['pk_peran'] ?>
                                    </option>
                                <?php endif ?>
                            <?php endif ?>
                        <?php endforeach ?>
                    </select>
                </div>
            <?php endif ?>
            
            <div class="form-group">
                <label>Rekan Kerja</label>
                <?php $orang2 = explode(' # ', $data->mainrow['rekan']) ?>
                <?php foreach ($data->daftarpegawai as $key): ?>
                    <?php if (in_array($key['username'], $orang2)): ?>
                        <div class="checkbox">
                            <label>
                                <input name="in[rekan][]" class="rekan" value="<?= $key['username'] ?>" type="checkbox" checked> <?= $key['nama'] ?>
                            </label>
                        </div>
                    <?php else: ?>
                        <div class="checkbox">
                            <label>
                                <input name="in[rekan][]" class="rekan" value="<?= $key['username'] ?>" type="checkbox"> <?= $key['nama'] ?>
                            </label>
                        </div>
                    <?php endif ?>
                <?php endforeach ?>
            </div>
            <div class="form-group">
                <label>Jenis Transportasi</label>
                <?php foreach ($data->transportasi as $key): ?>
                    <?php if ($key['lt_id']==$data->mainrow['jns_transportasi']): ?>
                        <div class="radio"><label><input required name="in[jnstrans]" value="<?= $key['lt_id'] ?>" type="radio" checked="checked" class="transportasic"><?= $key['lt_ket'] ?></label></div>
                    <?php else: ?>
                        <div class="radio"><label><input required name="in[jnstrans]" value="<?= $key['lt_id'] ?>" type="radio" class="transportasic"><?= $key['lt_ket'] ?></label></div>
                    <?php endif ?>
                        
                <?php endforeach ?>
            </div>

            <div class="row">
                <div class="page-header">
                    <h3>Biaya</h3>
                </div>
            </div>
            <div class="form-group">
                <label>Biaya Transportasi</label>
                <input id="biayatransid" name="in[biaya_transport]" type="number" class="form-control" value="<?= $data->mainrow['biaya_transport'] ?>" placeholder="Biaya Transportasi (hanya angka)">
            </div>
            <div class="form-group">
                <label>Keterangan Biaya Transportasi</label>
                <textarea id="ketbiayatransid" rows="5" name="in[ket_biaya_trans]" class="form-control" id="kettrasport"><?= $data->mainrow['ket_transport'] ?></textarea>
            </div>
            <div class="form-group">
                <label>Biaya Parkir</label>
                <input name="in[biaya_parkir]" type="number" class="form-control" value="<?= $data->mainrow['biaya_parkir'] ?>" placeholder="Biaya Parkir (hanya angka)">
            </div>
            <div class="form-group">
                <label>Biaya Makan Siang</label>
                <input name="in[biaya_makan_siang]" type="number" class="form-control" value="<?= $data->mainrow['biaya_lunch'] ?>" placeholder="Biaya Makan Siang (hanya angka)">
            </div>
            <div class="form-group">
                <label>Biaya Makan Malam</label>
                <input name="in[biaya_makan_malam]" type="number" class="form-control" value="<?= $data->mainrow['biaya_dinner'] ?>" placeholder="Biaya Makan Malam (hanya angka)">
            </div>
            <div class="form-group">
                <label>Biaya Kesehatan</label>
                <input name="in[biaya_kesehatan]" type="number" class="form-control" value="<?= $data->mainrow['biaya_kesehatan'] ?>" placeholder="Biaya Kesehatan (hanya angka)">
            </div>
            <div class="form-group">
                <label>Biaya Allowance</label>
                <input name="in[biaya_allowance]" type="number" class="form-control" value="<?= $data->mainrow['biaya_allowance'] ?>" placeholder="Biaya Allowance (hanya angka)">
            </div>
            <div class="form-group">
                <label>Biaya Lainnya</label>
                <input type="number" name="in[biayalain]" class="form-control" value="<?= $data->mainrow['biaya_lain'] ?>" placeholder="Biaya Lainnya (hanya angka)">
            </div>
            <div class="form-group">
                <label>Keterangan Biaya Lainnya</label>
                <textarea name="in[ket_biaya_lain]" class="form-control"><?= $data->mainrow['ket_biaya_lain'] ?></textarea>
            </div>
            </form>
            <button id="btnsubmit" class="btn btn-lg btn-success" style="width:100%;">Submit</button>
        </div> 
    </div>

</div><!--end container-->

<!-- Modal -->
  <div class='modal fade' id='modal-cari' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pencarian Data</h3>

        </div>
        <div class='modal-body'>
        <div style='padding-left:15px;padding-right:15px;padding-top:0px; height: 300px;overflow-y:auto;' class='row'>
          


          <div class="col-md-12">
            <form action="" method="get" accept-charset="utf-8">
                <div class="form-group">
                    <label>Nama Perusahaan</label>
                    <select class="form-control">
                        <option value="">-- Nama Perusahaan --</option>
                    </select>
                </div>
                
                <button class="btn btn-lg btn-primary">Ubah</button> 
            </form>
          </div>
             
          

        </div>
          

        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>FAC-Institute 2016</span>
                </div>
                <div class="col-md-6">
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->

  <!--modal edit nama perusahaan-->
  <div class='modal fade' id='modal-editAbsen' role='dialog'>
    <div class='modal-dialog'>
    <?php 
        $comboBoxData = $data->listperusahaan;
    ?>
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pencarian Data</h3>

        </div>
        <div class='modal-body'>
            <div style='padding:15px; height: 300px;' class='row'>
              
                <form action="" method='post'>
                  <div class="row">
                    <div class='form-group'>
                        <div class='col-md-10'>
                            <label>Nama Perusahaan</label>
                            <select name='editAbsen[namaPerusahaan]' class='form-control'>
                                <option value='f4e745e1015af1c15ebcb70b77f1426c'>FAC-Institute</option>
                                <option value='d5eb8c090d191587bb8a4b875bae4fed'>FAC-Remote</option>
                                <?php for ($i=0; $i < count($comboBoxData) ; $i++) { ?>
                                <option value=<?php echo $comboBoxData[$i]->id; ?>><?php echo $comboBoxData[$i]->perusahaan; ?></option>
                                <?php } ?>
                            </select>
                            <input style='display:none' name='editAbsen[idAbsen]' value="<?= $data->mainrow['id_absen'] ?>">
                        </div>
                    </div>                
                  </div>
                  <br>
                  <div class='row'>
                    <div class='col-md-10'>
                        <button class='btn btn-primary'>Submit</button>
                    </div>
                  </div>
                </form>  
            </div>
        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>FAC-Institute 2016</span>
                </div>
                <div class="col-md-6">
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
<!--end modal edit nama perusahaan-->

  <script>

    $(document).ready(function(){
        var nilai = $('#statuskerja').val();
        if (nilai=='1') {
            $('input[class=rekan]').each(function(){
                $(this).removeAttr("checked");
                $(this).attr("disabled", true);
            });
        } else {
            $('input[class=rekan]').each(function(){
                $(this).removeAttr("disabled");
            });
        }
    });

    $('#statuskerja').on('change',function(){
        var nilai = $('#statuskerja').val();
        if (nilai=='1') {
            $('input[class=rekan]').each(function(){
                $(this).prop('checked', false);
                $(this).attr("disabled", true);
            });
        } else {
            $('input[class=rekan]').each(function(){
                $(this).removeAttr("disabled");
            });
        } 
    });
    $('#btnsubmit').click(function(){
        var validasi = true;
        var jmlkegiatan = $("input[class=kegiatanc]:checked").length;
        var ketkegiatan = $('#ketkegiatanid').val();
        var stkerja = $('#statuskerja').val();
        var perankerja = $('#peran').val();
        var rekan2 = $("input[class=rekan]:checked").length;
        var jenistrans = $('input[class=transportasic]:checked').val();
        var biayatranss = $('#biayatransid').val();
        var ketbiayatrans = $('#ketbiayatransid').val();
        // cek kegiatan
        if (jmlkegiatan=='0') {
            alert('Kegiatan Harus di isi.');
            return;
            validasi = false;
        }

        // cek ket kegiatan
        if (ketkegiatan=='') {
            alert('Keterangan Kegiatan Harus di isi.');
            validasi = false;
            return;
        }

        if (stkerja!='1'&&rekan2=='0') {
            alert('Tidak Sendiri tapi tidak ada rekan, ente aneh sekali!!');
            validasi = false;
            return;
        }

        if (jenistrans===undefined) {
            alert('Ente ke tempat kerja pake pintu ajaib?? isi jenis transportasi dulu!');
            validasi = false;
            return;
        }

        if ((biayatranss!='0'&&biayatranss!='')&&ketbiayatrans=='') {
            alert('Isi keterangan biaya transport dulu!');
            return;
        }

        if (validasi) {
            $('#myform').submit();    
            // alert(biayatranss);
        }
    });
  </script>
</body>
</html> 