<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<style>
.btn {
    margin-right :4px;
    margin-bottom:4px;
}
</style>
<body>

<?php
  include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?= $data->subtitle ?></h3>
	</div>
	<div class="row" style="overflow:auto;">



        <div class="col-md-12">
                <a href="#modal-cariz" data-toggle="modal" title="Search" class="btn btn-info">
                    Search
                </a>
                <br><br>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="tengah">Nama Trainer</th>
                        <th class="tengah">Perusahaan</th>
                        <th class="tengah">Total Hari Training</th>
                        <th class="tengah">Detail</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($data->listdata)=='0'): ?>
                        <tr>
                            <td colspan="4" style="text-align:center;">Belum ada data tersedia</td>
                        </tr>
                    <?php else: ?>
                        <?php foreach ($data->listdata as $key): ?>
                            <tr>
                              <td><?= $key['nama'] ?> </td>
                              <td><?= $key['nama_usaha'] ?></td>
                              <td class="tengah"><?= $key['total_hari_training'] ?></td>
                              <td class="tengah">
                                <a href="<?= $data->base_url.'administrasi/googlecal/reptraining?det='.$key['idusahaa'] ?>" class="btn btn-info" title="Detail">Detail</a>
                              </td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>

                </tbody>
            </table>
        </div>


    </div>

</div><!--end container-->

<!-- Modal -->
  <div class='modal fade' id='modal-cariz' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pencarian Data</h3>

        </div>
        <div class='modal-body'>
        <div style='padding:15px; height: 300px;' class='row'>
          
          <div class="col-md-12">
            <form action="" method="GET">
                    <div class="row">
                        <div class="form-group">
                            <label for="dari-tanggal">Mulai Tanggal</label>
                            <input type="text" name="src[tglawal]" id="tanggal-awal" class="form-control" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label for="dari-tanggal">Sampai Tanggal</label>
                            <input type="text" name="src[tglakhir]" id="tanggal-akhir" class="form-control" />
                        </div>
                    </div>

                    <div class="row">
                        <button class="btn btn-primary" id='btn-downloadLaporan'>Cari</button>
                    </div>
            </form>

          </div>
             
          

        </div>
          

        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>FAC-Institute 2016</span>
                </div>
                <div class="col-md-6">
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->

  <script type="text/javascript" src="<?= $data->base_url ?>js/jquery-ui/jquery-ui.js"></script>
  <script>
    $(document).ready(function(){

        $('#tanggal-awal').datepicker({
            dateFormat: 'yy-mm-dd'
        });

      $('#tanggal-akhir').datepicker({
        dateFormat: 'yy-mm-dd'
      });

    });
  </script>
</body>
</html>
