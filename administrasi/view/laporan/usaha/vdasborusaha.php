<!doctype html>
<html>

<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>
<body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.js"></script>
    <script src="https://fac-institute.com/js/charts/util.js"></script>
<?php
  include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/view/main-component/sidebar.php';
  include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/view/main-component/top-nav.php';
 ?>
<div class="container" style="padding-bottom:40px;">

	<div class="row">
		<div class="page-header">
			<h2><?= $data->subtitle ?></h2>
		</div>
	</div>

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12">
            
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
            
        </div>
    </div>
    <br><br>
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-12">
    		<div id="canvas-holder" style="width:100%;overflow:scroll;">
                <canvas id="chart-area" />
            </div>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="tengah">Jenis Usaha</th>
                        <th>Jumlah Pengguna</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($data->datajenisusaha)=='0'): ?>
                        <tr>
                            <td colspan="3">Tidak ada dalam database kami.</td>
                        </tr>
                    <?php else: ?>
                        <?php foreach ($data->datajenisusaha as $key): ?>
                            <tr>
                                <td><a href="company-detail?ju=<?= $key->id ?>" title="<?= $key->jenis_usaha ?>"><?= $key->jenis_usaha ?></a></td>
                                <td><?= $key->jumlah ?></td>
                                <td>
                                    <a href="<?= $data->base_url.'administrasi/laporan/staff/chart/jenisusahapercentage?id='.$key->id ?>" title="">Detail Trainer</a>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                </tbody>
            </table>
            
		</div>
	   <div class="col-lg-6 col-md-6 col-sm-12">
            <div id="canvas-holder2" style="width:100%;">
                <canvas id="chart-area2" style="overflow:scroll;" />
            </div>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="tengah">Jenis Accurate</th>
                        <th>Jumlah Pengguna</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($data->datajenisaccurate)=='0'): ?>
                        <tr>
                            <td colspan="3">Tidak ada data dalam database kami.</td>
                        </tr>
                    <?php else: ?>
                        <?php foreach ($data->datajenisaccurate as $key): ?>
                            <tr>
                                <td><a href="company-detail?va=<?= $key->va_id ?>" title="<?= $key->va_nama ?>"><?= $key->va_nama ?></a></td>
                                <td><?= $key->jumlah ?></td>
                                <td>
                                    <a href="<?= $data->base_url.'administrasi/laporan/staff/chart/jenisaccpercentage?id='.$key->va_id ?>" title="">Detail Trainer</a>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                </tbody>
            </table>
       </div>
    </div>
	

</div>
    
    
    <script>
    var randomScalingFactor = function() {
        return Math.round(Math.random() * 100);
    };
    var config = {
        type: 'pie',
        data: {
            datasets: [{
                data: [
                    <?php DataValue($data->datajenisusaha) ?>
                ],
                backgroundColor: [
                    <?php GetColors(count($data->datajenisusaha)) ?>
                ],
                label: 'Data Jenis Usaha'
            }],
            labels: [
                <?php NamaJenisUsaha($data->datajenisusaha) ?>
            ]
        },
        options: {
            responsive: true
        }
    };

    var configz = {
        type: 'pie',
        data: {
            datasets: [{
                data: [
                    <?php DataValue($data->datajenisaccurate) ?>
                ],
                backgroundColor: [
                    <?php GetColors(count($data->datajenisaccurate)) ?>
                ],
                label: 'Data Jenis Accurate'
            }],
            labels: [
            
            ]
        },
        options: {
            responsive: true
        }
    };

    window.onload = function() {
        var ctx = document.getElementById("chart-area").getContext("2d");
        window.myPie = new Chart(ctx, config);

        var ctxx = document.getElementById("chart-area2").getContext("2d");
        window.myPie = new Chart(ctxx, configz);
    };
    
    var colorNames = Object.keys(window.chartColors);
    var colorNamess = Object.keys(window.chartColors);
    </script>


    <script>
    
    </script>
</body>
</html>

<?php 
function NamaJenisUsaha($value){
    foreach ($value as $key) {
        echo '"'.$key->jenis_usaha.'",';
    }
}

function NamaJenisAccurate($value){
    foreach ($value as $key) {
        echo '"'.$key->va_nama.'",';
    }
}
?>