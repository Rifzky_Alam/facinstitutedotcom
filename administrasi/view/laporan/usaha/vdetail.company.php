<!DOCTYPE html>
<html>
<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>

<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->subtitle ?></h3>
	</div>
	
     

	
	<div class="row">
		<div class="col-md-12">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>Nama Perusahaan</th>
              <th>Keterangan Usaha</th>
              <th>Alamat</th>
              <th>Kota</th>
              <th>Provinsi</th>
            </tr>
          </thead>
          <tbody>
            <?php if (count($data->datalap)=='0'): ?>
              <tr>
                <td colspan="5" style="text-align:center;">Tidak ada data dalam database kami.</td>
              </tr>
            <?php else: ?>
              <?php foreach ($data->datalap as $key): ?>
                <tr>
                  <td><?= $key->nama ?></td>
                  <td><?= $key->ket_jenis_usaha ?></td>
                  <td><?= $key->alamat ?></td>
                  <td><?= $key->kota ?></td>
                  <td><?= $key->provinsi ?></td>
                </tr>
              <?php endforeach ?>
            <?php endif ?>
          </tbody>
        </table>      
		</div>
        
	</div>

</div>

     
</body>
</html> 