<!DOCTYPE html>
<html>
<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>

<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->subtitle ?></h3>
	</div>
	
     

	
	<div class="row">
        <div class="col-md-12">
            <div class="form-inline">
                <input type="text" class="form-control" placeholder="Dari tanggal" id="fromdate" />
                <input type="text" class="form-control" placeholder="Sampai tanggal" id="todate" />
                <button id="downloadbtn" class="btn btn-primary" disabled>Download Excel</button>
            </div>
        </div>
	</div>
	<br><br>
	<div class="row">
	    <div class="col-md-12">
	        <h5>Menu Laporan</h5>
	    </div>
	</div>
	
	<div class="row">
	    <div class="col-md-10">
            <div class="checkbox">
              <label><input type="checkbox" value="custsekalitraining">Laporan Customer Sekali Training</label>
            </div>
	    </div>
	</div>

</div>

     
</body>
</html> 