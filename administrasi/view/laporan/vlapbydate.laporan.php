<!DOCTYPE html>
<html>
<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>

<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->subtitle ?></h3>
	</div>
	
     

	
	<div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Dari Tanggal</label>
                <input type="text" id="fd" class="form-control">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Sampai Tanggal</label>
                <input type="text" id="td" class="form-control">
            </div>
        </div>
	</div>
	
	<div class="row">
	    <div class="col-md-12">
	        <div class="form-group">
	            <label for="cbmenu"></label>
	            <select class="form-control" id="cbmenu">
	                <option value="nc">New Customer</option>
	                <option value="notyet">Net Yet</option>
	            </select>
	        </div>
	    </div>
	</div> 
</div>
<script>
    $(document).ready(function(){
        alert('ready!');
    });
</script>
</body>
</html> 