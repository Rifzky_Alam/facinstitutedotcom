<?php 
/*
  this file is editted by rifzky on 7 April 2018
*/
?>
<!DOCTYPE html>
<html>
<?php include_once 'view/main-component/header.php'; ?>

<body>

<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->subtitle ?></h3>
	</div>
	
     

	
	<div class="row">
		<div class="col-md-6">
			<table class="table table-bordered">
				<thead>
					<tr><th>Atribut</th><th>Keterangan</th></tr>
				</thead>

				<tbody>
                    <tr>
                      <td>Nama Customer</td> 
                      <td><?= $data->nama_cust ?></td>
                    </tr>
                    <tr>
                      <td>Telepon</td>
                      <td><?php echo $data->telp_cust ?></td>
                    </tr>
                    <tr>
                      <td>Email</td>
                      <td><?php echo $data->email_cust ?></td>
                    </tr>
                    <tr>
                      <td>Jabatan</td> 
                      <td><?php echo $data->jabatan ?></td>
                    </tr>
                    <tr>
                      <td>Marketing</td> 
                      <td><?= $data->marketing ?></td>
                    </tr>
                    <tr>
                      <td>Cabang</td> 
                      <td><?= $data->cabang ?></td>
                    </tr>
                    <tr>
                      <td>Agenda</td>
                      <td><?php echo $data->agenda ?></td>
                    </tr>
                    <tr>
                      <td>Jenis Pengguna</td> 
                      <td>
                        <?php if ($data->jenis_pengguna=='0'): ?>
                          <?php echo $data->jenis_pengguna . ' ( Baru )' ?>
                        <?php else: ?>
                          <?php echo $data->jenis_pengguna . ' ( Lama )' ?>
                        <?php endif ?>    
                      </td>
                    </tr>
                    <tr>
                      <td>Timestamp Register</td> 
                      <td><?= $data->timestamp ?></td>
                    </tr>
                    
        </tbody>
			</table>

      <br>
      <br>
      <!-- datausaha -->
      <table class="table table-bordered">
        <thead>
          <tr><th>Atribut</th><th>Keterangan</th></tr>
        </thead>

        <tbody>
                    <tr>
                      <td>Nama Usaha</td> 
                      <td><?= $data->namausaha ?></td>
                    </tr>
                    <tr>
                      <td>Telepon</td>
                      <td><?php echo $data->teleponusaha ?></td>
                    </tr>
                    <tr>
                      <td>Email</td>
                      <td><?php echo $data->emailusaha ?></td>
                    </tr>
                    <tr>
                      <td>Alamat</td> 
                      <td><?php echo $data->alamatusaha ?></td>
                    </tr>
                    <tr>
                      <td>Kecamatan</td> 
                      <td id="kecamatanrow"><?= $data->kecamatan ?></td>
                    </tr>
                    <tr>
                      <td>Kota</td>
                      <td id="kotarow"><?php echo $data->kota ?></td>
                    </tr>
                    <tr>
                      <td>Provinsi</td> 
                      <td id="provinsirow"><?php echo $data->provinsi ?></td>
                    </tr>
                    <tr>
                      <td>Jenis Usaha</td> 
                      <td><?= $data->jenisusaha ?></td>
                    </tr>
                    <tr>
                      <td>Ket Jenis Usaha</td> 
                      <td><?= $data->ketjenisusaha ?></td>
                    </tr>
                    <tr>
                      <td>Versi Accurate</td> 
                      <td><?= $data->versiaccurate ?></td>
                    </tr>
                    <tr>
                      <td>Map Lokasi</td> 
                      <td><?= $data->mapusaha ?></td>
                    </tr>
        </tbody>
      </table>


		</div>
    <div class="col-md-6">
      <div class="form-group">
        <form action="" method="post" accept-charset="utf-8">
        <label>Nama Customer</label>
        <input type="text" name="in[namacust]" class="form-control" value="<?= $data->nama_cust ?>" placeholder="">
        <input type="hidden" name="in[idusaha]" value="<?= @$data->idusaha ?>">
      </div>
      <div class="form-group">
        <label>Telepon Customer</label>
        <input type="text" name="in[telpcust]" class="form-control" value="<?= $data->telp_cust ?>" placeholder="">
      </div>
      <div class="form-group">
        <label>Email Customer</label>
        <input type="email" name="in[emailcust]" class="form-control" value="<?= $data->email_cust ?>" placeholder="">
      </div>
      <div class="form-group">
        <label>Jabatan Customer</label>
        <input type="text" name="in[jabatancust]" class="form-control" value="<?= $data->jabatan ?>" placeholder="">
      </div>
      <div class="form-group">
        <label>Nama marketing</label>
        <select name="in[marketing]" class="form-control" required>
          <option value="" disabled selected>-- Marketing belum terpilih --</option>
          <?php foreach ($data->listmarketing as $key): ?>
            <?php if ($key['marketing_id']==$data->marketing): ?>
              <option value="<?= $key['marketing_id'] ?>" selected="selected"><?= $key['marketing_nama'] ?></option>
            <?php else: ?>
              <option value="<?= $key['marketing_id'] ?>"><?= $key['marketing_nama'] ?></option>
            <?php endif ?>
            
          <?php endforeach ?>
        </select>
      </div>

      <div class="form-group">
        <label>Jenis Pengguna</label>
        <input type="text" name="in[jenispengguna]" class="form-control" value="<?= $data->jenis_pengguna ?>" placeholder="">
      </div>
      <button class="btn btn-lg btn-success">Export Data</button>
      </form>
      <br>
      <br>

      <hr>
      <!-- data usaha -->
      <form action="" method="post" accept-charset="utf-8">
      <div class="form-group">
        <label>Nama Usaha</label>
        <input type="text" name="inu[namausaha]" class="form-control" value="<?= $data->namausaha ?>" required placeholder="Nama Usaha">
      </div>

      <div class="form-group">
        <label>Email Usaha</label>
        <input type="email" name="inu[emailusaha]" class="form-control" value="<?= $data->emailusaha ?>" placeholder="Email Aktif">
      </div>

      <div class="form-group">
        <label>Alamat Usaha</label>
        <textarea name="inu[alamatusaha]" class="form-control" placeholder="Alamat Usaha"><?= $data->alamatusaha ?></textarea>
      </div>

      <div class="form-group">
        <label>Provinsi</label>
        <select required name="inu[provinsi]" class="form-control" id="cbprovinsi">
          <option value="">-- pilih provinsi --</option>
        </select>
      </div>

      <div class="form-group">
        <label>Kota/Kabupaten</label>
        <select name="inu[kota]" class="form-control" required id="cbkota">
          <option value="">-- pilih Kota/Kab --</option>
        </select>
      </div>

      <div class="form-group">
        <label>Kecamatan</label>
        <select name="inu[kecamatan]" class="form-control" id="cbkecamatan">
          <option value="">-- pilih Kecamatan --</option>
        </select>
      </div>

      <div class="form-group">
        <h4>Jenis Usaha</h4>

        <?php foreach ($data->jenisusahalist as $key): ?>
          <?php if (count($data->jenisusahaids)==0): ?>
            <div class="checkbox">
              <label><input name="inu[jnsUsaha][]" value="<?= $key['id'] ?>" type="checkbox"><?= $key['jenis_usaha'] ?></label>
            </div>
          <?php else: ?>
            <?php $yess = false; ?>
            <?php foreach ($data->jenisusahaids as $value): ?>
              <?php if ($value==$key['id']): ?>
                <?php $yess = true; ?>
              <?php endif ?>
            <?php endforeach ?>
            <?php if ($yess): ?>
              <div class="checkbox">
                <label><input name="inu[jnsUsaha][]" value="<?= $key['id'] ?>" checked type="checkbox"><?= $key['jenis_usaha'] ?></label>
              </div>
            <?php else: ?>
              <div class="checkbox">
                  <label><input name="inu[jnsUsaha][]" value="<?= $key['id'] ?>" type="checkbox"><?= $key['jenis_usaha'] ?></label>
              </div>
            <?php endif ?>

          <?php endif ?>
            
        <?php endforeach ?>
      </div>

      <div class="form-group">
        <label>Keterangan Jenis Usaha</label>
        <textarea class="form-control" name="inu[ketjenisusaha]"><?= $data->ketjenisusaha ?></textarea>
      </div>

      <div class="form-group">
        <h4>Versi Accurate</h4>
        <?php foreach ($data->versiaccuratelist as $key): ?>
          <?php if (count($data->vaids)==0): ?>
            <div class="checkbox">
              <label><input name="inu[jnsUsaha][]" value="<?= $key['va_id'] ?>" type="checkbox"><?= $key['va_nama'] ?></label>
            </div>
          <?php else: ?>
            <?php $yess = false; ?>
            <?php foreach ($data->vaids as $value): ?>
              <?php if ($value==$key['va_id']): ?>
                <?php $yess = true; ?>
              <?php endif ?>
            <?php endforeach ?>
            <?php if ($yess): ?>
                <div class="checkbox">
                  <label><input checked name="inu[jnsUsaha][]" value="<?= $key['va_id'] ?>" type="checkbox"><?= $key['va_nama'] ?></label>
                </div>
              <?php else: ?>
                <div class="checkbox">
                  <label><input name="inu[jnsUsaha][]" value="<?= $key['va_id'] ?>" type="checkbox"><?= $key['va_nama'] ?></label>
                </div>
              <?php endif ?>
          <?php endif ?>
            
        <?php endforeach ?>
      </div>
      <button class="btn btn-lg btn-primary">Export Usaha</button>
      </form>
    </div>
	</div>

</div>
<script>

  $(document).ready(function(){
    GetProvinsi();
  });

  function GetProvinsi(){
    $.ajax({
      type: "GET",
      url: "https://fac-institute.com/administrasi/wilayah/provinsi/",
      data: {
        'id':''
      },
      cache: false,
      success: function(data){
        console.log(data);
      }
    }); //end ajax
  }
</script>
     
</body>
</html> 
