<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?= $data->headertext ?></h3>
	</div>
	<div class="row">
     <form id="my-form" enctype="multipart/form-data" method="post">

         <div class="row">
             <div class="col-md-12">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Nama Cust</th>
                            <th>Perusahaan</th>
                            <th>Email</th>
                            <th>Telepon</th>
                            
                            <th>V.Accurate</th>
                            <th>Jenis Usaha</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php if (count($data->lists)=='0'): ?>
                            <tr>
                                <td colspan="7" style="text-align:center;">Tidak ada data client terdaftar online dalam sistem.</td>
                            </tr>
                        <?php else: ?>
                            <?php foreach ($data->lists as $key): ?>
                                <tr>
                                    <td><a href="<?= $data->base_url.'administrasi/daftar-online/detailcust/'.$key['tc_id'] ?>" title=""><?= @$key['tc_nama'] ?></a></td>
                                    <td><a href="<?= $data->base_url.'administrasi/daftar-online/detailusaha/'.$key['tu_id'] ?>" title="Detail Perusahaan"><?= @$key['tu_nama'] ?></a></td>
                                    <td><?= @$key['tc_email'] ?></td>
                                    <td><?= @$key['tc_telepon'] ?></td>
                                    
                                    <td><?= @$key['tu_va'] ?></td>
                                    <td><?= @$key['tu_kju'] ?></td>
                                </tr>
                            <?php endforeach ?>
                        <?php endif ?>
                        
                    </tbody>
                </table>
             </div>
         </div>         
         <hr>

     </form>   
    </div>

</div><!--end container-->
</body>
</html> 