<!DOCTYPE html>
<html>
<?php include_once 'view/main-component/header.php'; ?>

<body>

<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->subtitle ?></h3>
	</div>
	
     

	
	<div class="row">
		<div class="col-md-6">
			<table class="table table-bordered">
				<thead>
					<tr><th>Atribut</th><th>Keterangan</th></tr>
				</thead>

				<tbody>
                    <tr>
                      <td>Nama perusahaan</td> 
                      <td><?= $data->nama ?></td>
                    </tr>
                    <tr>
                      <td>Email Perusahaan</td>
                      <td><?php echo $data->email ?></td>
                    </tr>
                    <tr>
                      <td>Telepon Perusahaan</td>
                      <td><?php echo $data->telepon ?></td>
                    </tr>
                    <tr>
                      <td>Jenis Usaha</td>
                      <td><?php echo $data->jenisusaha ?></td>
                    </tr>
                    <tr>
                      <td>Keterangan Jenis Usaha</td>
                      <td><?php echo $data->ketjenisusaha ?></td>
                    </tr>
                    <tr>
                      <td>Kota</td> 
                      <td><?php echo $data->kota ?></td>
                    </tr>
                    <tr>
                      <td>Provinsi</td> 
                      <td><?php echo $data->provinsi ?></td>
                    </tr>
                    <tr>
                      <td>Alamat perusahaan</td>
                      <td><?php echo $data->alamat ?></td>
                    </tr>
                    <tr>
                      <td>Varian Accurate</td> 
                      <td><?php echo $data->varian ?></td>
                    </tr>
        </tbody>
			</table>
    <form action="" method="post" accept-charset="utf-8">
      <div class="row">
        <input type="hidden" name="in[namausaha]" value="<?= $data->nama ?>">
        <input type="hidden" name="in[emailusaha]" value="<?= $data->email ?>">
        <input type="hidden" name="in[teleponusaha]" value="<?= $data->telepon ?>">
        <input type="hidden" name="in[jenisusaha]" value="<?= $data->jenisusaha ?>">
        <input type="hidden" name="in[ketjenisusaha]" value="<?= $data->ketjenisusaha ?>">
        <input type="hidden" name="in[kota]" value="<?= $data->kota ?>">
        <input type="hidden" name="in[provinsi]" value="<?= $data->provinsi ?>">
        <input type="hidden" name="in[alamat]" value="<?= $data->alamat ?>">
        <input type="hidden" name="in[varian]" value="<?= $data->varian ?>">
        <input type="hidden" name="in[map]" value="<?= $data->map ?>">
        <a class="btn btn-lg btn-primary" href="<?= $data->base_url.'administrasi/new-usaha?imp='.$data->idusaha ?>" style="width:100%;">Export Data</a>
      </div>
    </form>
      
		</div>
        <div class="col-md-6">
            <iframe width="100%" height="450" frameborder="1" style="border:1" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyB9cGqJ4nUrq-Lje2Q1B1Hx1_X3-RofwsY&q=<?php echo ChangeChar($data->map) ?>" allowfullscreen>
            </iframe>
        </div>
	</div>

</div>

     
</body>
</html> 

<?php function LinkAssistLocation($isadmin,$baseurl,$idtrans){
  if ($isadmin) {
    echo "<td title='Klik untuk membagikan lokasi training ke customer'><a target='_blank' href='".$baseurl."member/lokasi-training?tr=".$idtrans."'>Share map to customer</a></td>";
  } else {

  }
  
} ?>

<?php function NamaUsaha($isadmin,$idusaha,$data){
  if ($isadmin) {
    echo "<td title='Klik untuk detail perusahaan'><a target='_blank' href='new-usaha?id=".$idusaha."'>".$data."</a></td>";
  } else {
    echo "<td>".$data."</td>";
  }
  
} ?>

<?php 
function ChangeChar($string){
$string=str_replace('<','&lt;',$string);
$string=str_replace('>','&gt;',$string);
$string=str_replace('"','&quot;',$string);
$string=str_replace("'",'&#39;',$string);
$string=str_replace("&",'%26',$string);
return $string;


}
 ?>
