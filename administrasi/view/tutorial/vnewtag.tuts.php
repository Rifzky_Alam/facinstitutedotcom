<!DOCTYPE html>
<html>
<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>

<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->subtitle ?></h3>
	</div>
	
     

	
	<div class="row">
		<div class="col-md-12">
      <div class="jumbotron">
        <h2>Input Data Baru</h2>
        <p>
          Silahkan unuk mengisi form di bawah ini untuk mengisi tag yang belum tersedia di database kami.
        </p>
      </div>
		</div>
	</div>

  <div class="row">
    <div class="col-md-12">
      <form action="" method="POST" accept-charset="utf-8">
      <div class="form-group">
        <label>Nama Tag</label>
        <input type="text" name="tagname" class="form-control" placeholder="Nama Tag Baru">
      </div>
      <button class="btn btn-lg btn-primary">Submit</button>
    </form>
    </div>
  </div>

</div>

     
</body>
</html> 

<?php function LinkAssistLocation($isadmin,$baseurl,$idtrans){
  if ($isadmin) {
    echo "<td title='Klik untuk membagikan lokasi training ke customer'><a target='_blank' href='".$baseurl."member/lokasi-training?tr=".$idtrans."'>Share map to customer</a></td>";
  } else {

  }
  
} ?>

<?php function NamaUsaha($isadmin,$idusaha,$data){
  if ($isadmin) {
    echo "<td title='Klik untuk detail perusahaan'><a target='_blank' href='new-usaha?id=".$idusaha."'>".$data."</a></td>";
  } else {
    echo "<td>".$data."</td>";
  }
  
} ?>

<?php 
function ChangeChar($string){
$string=str_replace('<','&lt;',$string);
$string=str_replace('>','&gt;',$string);
$string=str_replace('"','&quot;',$string);
$string=str_replace("'",'&#39;',$string);
$string=str_replace("&",'%26',$string);
return $string;


}
 ?>
