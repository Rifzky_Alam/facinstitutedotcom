<!DOCTYPE html>
<html>
<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>

<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->subtitle ?></h3>
	</div>

  <div class="row">
    <div class="col-md-12">
      <div class="jumbotron">
        <h4>Data tag akan dihapus</h4>
        <p>
          Menghapus tag berarti anda akan menghapus data yang terhubung ke tag tersersebut, apakah anda yakin?
        </p>
      </div>
      <form action="" method="post" accept-charset="utf-8">
        <div class="form-group">
          <input type="hidden" name="rmvtag" value="<?= $data->tagid ?>">
          <button class="btn btn-lg btn-danger">Ya, Hapus Tag</button>
          <a href="<?= $data->base_url.'administrasi/cms/tags/lists' ?>" title="Kembali ke daftar tag" class="btn btn-lg btn-warning">Tidak, kembali saja.</a>
        </div>
      </form>
    </div>
  </div>

</div>
  
     
</body>
</html> 