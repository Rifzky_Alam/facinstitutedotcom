<!DOCTYPE html>
<html>
<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>

<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->subtitle ?></h3>
	</div>

  <div class="row">
    <div class="col-md-12">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Nama Tag</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php if (count($data->tags)=='0'): ?>
            <tr>
              <td style="text-align:center;">Tidak Ada data dalam database kami</td>
            </tr>
            <?php else: ?>
          <?php foreach ($data->tags as $key): ?>
            <tr>
              <td><?= $key['tags_txt'] ?></td>
              <td style="text-align:center;"><a href="<?= $data->base_url.'administrasi/cms/tags/removetag/'.$key['tags_id'] ?>" title="Remove Tag">Remove Tag</a></td>
            </tr>    
          <?php endforeach ?>
          <?php endif ?>
          
          
        </tbody>
      </table>
    </div>
  </div>

</div>
  
     
</body>
</html> 