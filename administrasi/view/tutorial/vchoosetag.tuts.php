<!DOCTYPE html>
<html>
<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>

<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->subtitle ?></h3>
	</div>
	
     

	
	<div class="row">
		<div class="col-md-12">
      <div class="jumbotron">
        <h2>Input/Edit Tag Tutorial</h2>
        <p>
          Silahkan untuk memilih data tag yang tersedia di dalam database kami untuk tutorial ini. Untuk input tag yang anda inginkan silahkan cari dengan mengetik di kolom "nama tag" di bawah ini. Bila tag yang anda maksud terdapat dalam list database kami, klik tombol "+" untuk memasukkan nya ke dalam box pilihan. Setelah tags ada di box pilihan tekan submit. <br>Apabila tag yang anda inginkan tidak terdapat di list kami silahkan untuk mendaftarkan nya terlebih dahulu pada link di bawah ini.<br><br>
          Untuk melihat list tag -> <a href="<?= $data->base_url.'administrasi/cms/tags/lists' ?>" title="Table Tag">Link</a><br>
          Untuk input tag baru -> <a href="<?= $data->base_url.'administrasi/cms/tags/addnew' ?>" title="">Link</a><br>
          Unmoved tags -> <?= $data->tags['tags'] ?>, <a href="<?= $data->base_url.'administrasi/cms/tags/clearoldtag/'.$data->idtus ?>" title="">click here to clear the old tags</a>
        </p>
      </div>
		</div>
	</div>

  <div class="row">
    <div class="col-md-12">
      <form action="" method="POST" accept-charset="utf-8">
      <div class="form-group">
        <label>Nama Tag</label>
        <input type="text" name="tagname" id="namatag" class="form-control" placeholder="Nama Tag Baru">
      </div>
      <div class="row" id="msg"></div>
      <div class="row">
        <br><br>
        <ul style="list-style-type:none;" id="avtag">
        <?php if ($data->tags['tags']!=''): ?>

          <?php $r = explode(', ', $data->tags['tags']); ?>
          <?php for ($i = 0; $i < count($r); $i++) { ?>
            <?php for ($j = 0; $j < count($data->deftag); $j++) {
              if ($r[$i]==$data->deftag[$j]['tags_id']) {
                echo "<li id='c".$data->deftag[$j]['tags_id']."' class='ilist'><label id='l".$data->deftag[$j]['tags_id']."' title='".$data->deftag[$j]['tags_txt']."'><input type='checkbox' > ".$data->deftag[$j]['tags_txt']."</label> <span id='g".$data->deftag[$j]['tags_id']."' class='glyphicon glyphicon-plus' onclick='IWantThis(this)'></span></li>";
              }
            } ?>
          <? } ?>  
          <?php endif ?>  
          
        </ul>
      </div>
      <div class="well">
        <h6>Box Pilihan</h6>
        <ul style="list-style-type:none;" id="chosenbox">
          <?php if (count($data->newtags)!='0'): ?>
            <?php foreach ($data->newtags as $key): ?>
              <li id="cb<?= $key['fi_t_id'] ?>">
                <label><input type="checkbox" name="tag[]" value="<?= $key['fi_t_id'] ?>"> <?= $key['tags_txt'] ?></label> <span id="h<?= $key['fi_t_id'] ?>" class="glyphicon glyphicon-remove" onclick="IDislikeThis(this)"></span> || <a href="<?= $data->base_url.'administrasi/cms/tags/remove/'.$key['fi_t_id'] ?>" title="Remove From DB">Remove from database</a>
              </li>
            <?php endforeach ?>
          <?php endif ?>
        </ul>
      </div>
      <button class="btn btn-lg btn-primary">Submit</button>
    </form>
    </div>
  </div>

</div>
  <script>

    $('#namatag').keyup(function(){
      var ini = $('#namatag').val();
      if (ini=='') {
        $('.ilist').remove();
      }else{
        $('#msg').append('<p class="ilist">Koneksi ke database, Harap Tunggu ..</p>');
        $.ajax({
              type: "GET",
              url: "<?= $data->base_url.'administrasi/cms/tags/tagnameapi' ?>",
              data: {
                'tag':ini
                },
              cache: false,
              success: function(data){
                $('.ilist').remove();
                var myjson = JSON.parse(data);
                // console.log(myjson);
                for(x in myjson.data){
                  $('#avtag').append('<li id="c'+myjson.data[x].tags_id+'" class="ilist"><label title="'+myjson.data[x].tags_txt+'" id="l'+myjson.data[x].tags_id+'"><input type="checkbox" value="'+myjson.data[x].tags_id+'"> '+myjson.data[x].tags_txt+'</label> <span id="g'+myjson.data[x].tags_id+'" class="glyphicon glyphicon-plus" onclick="IWantThis(this)"></span></li>');
                  // console.log(myjson.data[x]);
                }
                
              }
            }); //end ajax
      }
    });



    function IWantThis(obj) {
      
      var aidi = obj.id.replace('g','');

      $('#chosenbox').append('<li id="cb'+aidi+'""><label><input type="checkbox" name="tag[]" checked value="'+aidi+'"> '+$('#l'+aidi).prop('title')+'</label> <span id="h'+aidi+'" class="glyphicon glyphicon-remove" onclick="IDislikeThis(this)"></span> || <a href="https://fac-institute.com/administrasi/cms/tags/remove/'+aidi+'" title="">Remove from database</a></li>');
      // $('#chosenbox').append('ok');
      $('#c'+aidi).remove();
    }


    function IDislikeThis(obj){
      var aidi = obj.id.replace('h','');
      $('#cb'+aidi).remove();
    }
  </script>
     
</body>
</html> 