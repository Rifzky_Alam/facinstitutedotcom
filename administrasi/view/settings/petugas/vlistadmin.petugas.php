<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?= $data->header ?></h3>
	</div>
	
    <!--
	<div class="row">
		<div class="col-sm-12 col-md-12" style="text-align:right"><a class="" id="cari-data" href="#modal-cari" data-toggle="modal">Cari Data?</a></div>
	</div>
    -->

    <div class='row'>
        <div class='col-md-12'>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Telepon</th>
                        <th>Status</th>
                        <th>Level</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($data->listpetugas)=='0'): ?>
                        <tr>
                            <td colspan="5" class="tengah">Tidak Ada Data Dalam Database Kami</td>
                        </tr>    
                        <?php else: ?>
                        <?php foreach ($data->listpetugas as $key): ?>
                            <tr>
                                <td><?= $key['nama'] ?></td>
                                <td><?= $key['username'] ?></td>
                                <td><?= $key['email'] ?></td>
                                <td><?= $key['telepon'] ?></td>
                                <td><?= $key['status'] ?> 
                                    || 
                                    <?php if ($key['status']=='inactive'): ?>
                                        <a href="<?= $data->base_url.'administrasi/settings/cspetugas/'.$key['username'].'/1' ?>" title="">Aktifkan</a>
                                    <?php else: ?>
                                        <a href="<?= $data->base_url.'administrasi/settings/cspetugas/'.$key['username'].'/0' ?>" title="">Non Aktifkan</a>
                                    <?php endif ?>
                                </td>
                                <td><?= $key['intr_level'] ?> | <a href="<?= $data->base_url.'administrasi/settings/leveltrainer?usr='.$key['username'] ?>" title="Ubah"><span class="glyphicon glyphicon-edit"></span></a></td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                </tbody>
            </table>
        </div>

    </div>
	    
</div>
</body>
</html> 



