<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?= $data->headertext ?></h3>
	</div>

    

	<div class="row">
        <div class="col-md-12">
            <form action="" method="post" accept-charset="utf-8">
                <div class="form-group">
                    <label>Peran Kerja</label>
                    <select name="in[peran]" class="form-control" required>
                        <option value="">-- pilih peran kerja --</option>
                        <?php foreach ($data->listperan as $key): ?>
                            <option value="<?= $key['pk_id'] ?>"><?= $key['pk_peran'] ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Persentase %</label>
                    <input type="number" name="in[perseninsentif]" class="form-control" placeholder="Persentase">
                </div>
                <button class="btn btn-lg btn-primary">Submit</button>
            </form>
        </div> 
    </div>

</div><!--end container-->
</body>
</html> 