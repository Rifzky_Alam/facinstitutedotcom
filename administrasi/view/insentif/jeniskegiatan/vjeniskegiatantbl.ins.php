<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
    <div class="page-header" id="top-logo">
        <h3><?= $data->headertext ?></h3>
    </div>
    <div class="row">
        <div class="col-md-12" style="overflow: auto;">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="text-align:center;">Jenis Kegiatan</th>
                        <th style="text-align:center;">Status Insentif</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($data->listdata)=='0'): ?>
                        <tr>
                            <td style="text-align:center;" colspan="2">
                                Tidak ada data di konfigurasi ini, harap hubungi admin sistem kami.
                            </td>
                        </tr>
                    <?php else: ?>
                        <?php foreach ($data->listdata as $key): ?>
                            <tr>
                                <td><?= $key['dkl_desc'] ?></td>
                                <td style="text-align:center;">
                                    <?php if ($key['dkl_insentif']=='0'): ?>
                                        <a href="<?= $data->base_url.'administrasi/insentif/edit-jnskegiatan/1/'.$key['dkl_id'] ?>" title="Klik untuk mengubah data">Aktifkan</a>
                                        ||
                                        <span style="color:red;">Non Aktif</span>
                                    <?php else: ?>
                                        <span style="color:#53f442;">Aktif</span>
                                        ||
                                        <a href="<?= $data->base_url.'administrasi/insentif/edit-jnskegiatan/0/'.$key['dkl_id'] ?>" title="Klik untuk mengubah data">Non Aktifkan</a>
                                    <?php endif ?>
                                </td>
                            </tr>                            
                        <?php endforeach ?>                        
                    <?php endif ?>
                </tbody>
            </table>
        </div> 
    </div>

</div><!--end container-->
</body>
</html> 