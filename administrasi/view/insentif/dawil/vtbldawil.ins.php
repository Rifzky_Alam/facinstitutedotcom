<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
    <div class="page-header" id="top-logo">
        <h3><?= $data->headertext ?></h3>
    </div>

    <div class="row">
        <div class="col-md-12">
            <a href="<?= $data->base_url.'administrasi/insentif/new-dawil' ?>" class="btn btn-primary">New Data</a>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Wilayah</th>
                        <th>Tariff</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($data->listdata)=='0'): ?>
                        <tr>
                            <td colspan="4" style="text-align:center;">Tidak Ada data di dalam database kami</td>
                        </tr>
                    <?php else: ?>
                        <?php foreach ($data->listdata as $key): ?>
                            <tr>
                                <td><?= $key['ind_wilayah'] ?></td>
                                <td><?= $key['ind_tariff'] ?></td>
                                <td><?= $key['dt_desc'] ?></td>
                                <td>
                                    <a href="<?= $data->base_url.'administrasi/insentif/edit-statusdawil/'.$key['ind_id'].'?st=0' ?>" title="Remove">Non-Aktifkan</a>
                                    ||
                                    <a href="<?= $data->base_url.'administrasi/insentif/edit-statusdawil/'.$key['ind_id'].'?st=1' ?>" title="Activate">Aktifkan</a>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                    
                </tbody>
            </table>  
        </div> 
    </div>

</div><!--end container-->
</body>
</html> 