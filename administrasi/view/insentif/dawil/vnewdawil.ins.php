<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?= $data->headertext ?></h3>
	</div>

    

	<div class="row">
        <div class="col-md-12">
            <form action="" method="post" accept-charset="utf-8">
                <div class="form-group">
                    <label>Wilayah</label>
                    <input type="text" name="in[wilayah]" class="form-control" placeholder="Ketik misal: Wilayah I, Wilayah II" required>
                </div>
                <div class="form-group">
                    <label>Tariff</label>
                    <input type="number" name="in[tarif]" class="form-control" placeholder="Hanya angka tanpa koma dan titik" required>
                </div>
                <button class="btn btn-lg btn-primary">Submit</button>
            </form>
        </div> 
    </div>

</div><!--end container-->
</body>
</html> 