<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
    <div class="page-header" id="top-logo">
        <h3><?= $data->headertext ?></h3>
    </div>

    <div class="row">
        <div class="col-md-12">
            <a href="<?= $data->base_url.'administrasi/insentif/new-daftardawil' ?>" class="btn btn-info">Data Baru</a>
        </div>
    </div><br>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Wilayah</th>
                        <th>Provinsi</th>
                        <th>Kota</th>
                        <th>Kecamatan</th>
                        <th>Tarif Insentif</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($data->listdata)=='0'): ?>
                        <tr>
                            <td colspan="5" style="text-align:center;">Tidak Ada data di dalam database kami</td>
                        </tr>
                    <?php else: ?>
                        <?php $row = 0; ?>
                        <?php foreach ($data->listdata as $key): ?>
                            <tr>
                                <td><?= $key['ind_wilayah'] ?></td>
                                <input type="hidden" <?= 'id="prv-'.$row.'"' ?>  value="<?= $key['itw_provinsi'] ?>">
                                <input type="hidden" <?= 'id="kot-'.$row.'"' ?>  value="<?= $key['itw_kota'] ?>">
                                <input type="hidden" <?= 'id="kec-'.$row.'"' ?>  value="<?= $key['itw_kecamatan'] ?>">
                                <td <?= 'id="prvr-'.$row.'"' ?> class="provinsii">
                                    <?= $key['itw_provinsi'] ?> 
                                </td>
                                <td <?= 'id="kotr-'.$row.'"' ?> class="kotaa">
                                    <?php if ($key['itw_kota']=='88888888'): ?>
                                        Seluruhnya
                                    <?php else: ?>
                                        <?= $key['itw_kota'] ?> 
                                    <?php endif ?>    
                                </td>
                                <td <?= 'id="kecr-'.$row.'"' ?> class="kecamatanz">
                                    <?php if ($key['itw_kecamatan']=='88888888'): ?>
                                        Seluruhnya
                                    <?php else: ?>
                                        <?= $key['itw_kecamatan'] ?> 
                                    <?php endif ?>
                                </td>
                                <td><?= $key['ind_tariff'] ?></td>
                                <td>
                                    <a href="<?= $data->base_url.'administrasi/insentif/deldawil?id='.$key['itw_id'] ?>" title="Remove">Remove</a>
                                </td>
                            </tr>
                            <?php $row++; ?>
                        <?php endforeach ?>
                    <?php endif ?>
                    
                </tbody>
            </table>  
        </div> 
    </div>

</div><!--end container-->
<script>
    
</script>
</body>
</html> 