<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
    <div class="page-header" id="top-logo">
        <h3><?= $data->headertext ?></h3>
    </div>

    <div class="row">
        <div class="col-md-12">
            <a href="<?= $data->base_url.'administrasi/insentif/new-harlibnas' ?>" class="btn btn-warning">Data Baru</a>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th>Keterangan</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($data->listdata)=='0'): ?>
                        <tr>
                            <td colspan="4" style="text-align:center;">Tidak Ada data di dalam database kami</td>
                        </tr>
                    <?php else: ?>
                        <?php foreach ($data->listdata as $key): ?>
                            <tr>
                                <td><?= $key['hlb_tanggal'] ?></td>
                                <td><?= $key['hlb_keterangan'] ?></td>
                                <td>
                                    <a href="<?= $data->base_url.'administrasi/insentif/deleteharlibnas?id='.$key['hlb_id'] ?>" title="Remove">Hapus</a>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                    
                </tbody>
            </table>  
        </div> 
    </div>

</div><!--end container-->
</body>
</html> 