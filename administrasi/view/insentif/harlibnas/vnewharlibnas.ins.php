<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?= $data->headertext ?></h3>
	</div>

    

	<div class="row">
        <div class="col-md-12">
            <form action="" method="post" accept-charset="utf-8">
                <div class="form-group">
                    <label>Tanggal</label>
                    <input type="text" id="tanggalan" name="in[tanggal]" class="form-control" placeholder="Tanggal" required>
                </div>
                <div class="form-group">
                    <label>Keterangan</label>
                    <textarea name="in[ketlibur]" placeholder="Keterangan Libur" class="form-control"></textarea>
                </div>
                <button class="btn btn-lg btn-primary">Submit</button>
            </form>
        </div> 
    </div>

</div><!--end container-->
<script type="text/javascript" src='https://fac-institute.com/js/jquery-ui/jquery-ui.js'></script>
<script>
    $(document).ready(function(){
        $('#tanggalan').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        // alert('Success');
    });
</script>
</body>
</html> 