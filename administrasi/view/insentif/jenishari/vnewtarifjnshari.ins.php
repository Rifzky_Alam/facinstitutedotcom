<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?= $data->headertext ?></h3>
	</div>
	<div class="row">
     <form id="my-form" action="" method="post">

         <div class="row">
             <div class="col-md-10">
                <div class="form-group">
                    <label>Jenis Hari</label>
                    <select name="in[jnshari]" class="form-control" required>
                        <option value="">-- Pilih Jenis Hari --</option>
                        <?php foreach ($data->listjenishari as $key): ?>
                            <option value="<?= $key['int_hk_id'] ?>"><?= $key['int_hk_hari'] ?></option>
                        <?php endforeach ?>
                    </select>
                </div>

                <div class="form-group">
                    <label>Tarif</label>
                    <input type="text" name="in[tarif]" class="form-control" placeholder="Hanya Angka" required>
                </div>

                <button class="btn btn-lg btn-success">Submit</button>

             </div>
         </div>         
         <hr>

     </form>   
    </div>

</div><!--end container-->
</body>
</html> 