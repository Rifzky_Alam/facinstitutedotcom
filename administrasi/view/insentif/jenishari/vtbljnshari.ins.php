<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?= $data->headertext ?></h3>
	</div>

    <div class="row">
        <div class="col-md-12">
            <section>
                <p>Pastikan status data aktif agar data dapat di baca oleh sistem pada kalkulasi insentif laporan harian trainer</p>
            </section>
        </div>
    </div>


	<div class="row">
        <div class="col-md-12">
            <a href="<?= $data->base_url.'administrasi/insentif/new-harikerja' ?>" title="">Data Baru</a>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Hari Kerja</th>
                        <th>Tariff</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($data->listharikerja)=='0'): ?>
                        <tr>
                            <td colspan="5" style="text-align:center;">Tidak Ada data di dalam database kami</td>
                        </tr>
                    <?php else: ?>
                        <?php foreach ($data->listharikerja as $key): ?>
                            <tr>
                                <td><?= $key['int_hk_hari'] ?></td>
                                <td style="text-align:right;"><?= number_format($key['ihk_tariff']) ?> IDR</td>
                                <td><?= $key['dt_desc'] ?></td>
                                <td>
                                    <a href="<?= $data->base_url.'administrasi/insentif/edit-statusjnshr/'.$key['ihk_id'].'?st=0' ?>" title="Remove">Non-Aktifkan</a>
                                    ||
                                    <a href="<?= $data->base_url.'administrasi/insentif/edit-statusjnshr/'.$key['ihk_id'].'?st=1' ?>" title="Activate">Aktifkan</a>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                    
                </tbody>
            </table>  
        </div> 
    </div>

</div><!--end container-->
</body>
</html> 