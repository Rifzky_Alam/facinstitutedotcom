<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
    <div class="page-header" id="top-logo">
        <h3><?= $data->headertext ?></h3>
    </div>
    <div class="row">
        <div class="col-md-12">
            <form action="" method="POST" accept-charset="utf-8">
            <div class="form-group">
                 <select class="form-control" name="in[jnstrans]">
                     <?php foreach ($data->listdata as $key): ?>
                         <option value="<?= $key['jt_id'] ?>"><?= $key['jt_ket'] ?></option>
                     <?php endforeach ?>
                 </select>
             </div>
             <div class="form-group">
                 <label>Aktifkan Insentif Accurate</label>
                 <select class="form-control" name="in[acc]">
                     <option value="0">Tidak</option>
                     <option value="1">Ya</option>
                 </select>
             </div>
             <div class="form-group">
                 <label>Aktifkan Insentif SML</label>
                 <select class="form-control" name="in[harikerja]">
                     <option value="0">Tidak</option>
                     <option value="1">Ya</option>
                 </select>
             </div>
             <div class="form-group">
                 <label>Aktifkan Insentif Wilayah</label>
                 <select class="form-control" name="in[wilayah]">
                     <option value="0">Tidak</option>
                     <option value="1">Ya</option>
                 </select>
             </div>
             <button class="btn btn-primary">Submit</button> 
             </form>
        </div> 
    </div>

</div><!--end container-->
</body>
</html> 