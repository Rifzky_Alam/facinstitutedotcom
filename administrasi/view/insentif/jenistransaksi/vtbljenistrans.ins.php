<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
    <div class="page-header" id="top-logo">
        <h3><?= $data->headertext ?></h3>
    </div>
    
    <?php if (count($data->listdata)<$data->datatrans): ?>
        <div class="row">
            <div class="col-md-12">
                <a href="<?= $data->base_url.'administrasi/insentif/new-jnstrans' ?>" class="btn btn-default">New Data</a>
            </div>
        </div>
        <br>
    <?php endif ?>
        
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Jenis Transaksi</th>
                        <th>Insentif Accurate</th>
                        <th>Insentif SML</th>
                        <th>Insentif Luar Kota</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($data->listdata)=='0'): ?>
                        <tr>
                            <td colspan="6" style="text-align:center;">Tidak Ada data di dalam database kami</td>
                        </tr>
                    <?php else: ?>
                        <?php $row = 0; ?>
                        <?php foreach ($data->listdata as $key): ?>
                            <tr>
                                <td><?= $key['jt_ket'] ?></td>
                                <td><?= $key['ijt_acc'] ?></td>
                                <td><?= $key['ijt_hari'] ?></td>
                                <td><?= $key['ijt_wilayah'] ?></td>
                                <?php if ($key['ijt_status']=='1'): ?>
                                <td>Valid</td>
                                <?php else: ?>
                                <td>Invalid</td>
                                <?php endif ?>
                                <td>
                                    <a href="<?= $data->base_url.'administrasi/insentif/edit-jnstrans/'.$key['ijt_jns_trans'].'?st=1' ?>" title="Validasi">
                                    Validate
                                    </a>
                                    ||
                                    <a href="<?= $data->base_url.'administrasi/insentif/edit-jnstrans/'.$key['ijt_jns_trans'].'?st=0' ?>" title="Non Aktifkan">
                                    Invalidate
                                    </a>
                                </td>
                            </tr>
                            <?php $row++; ?>
                        <?php endforeach ?>
                    <?php endif ?>
                    
                </tbody>
            </table>  
        </div> 
    </div>

</div><!--end container-->
</body>
</html> 