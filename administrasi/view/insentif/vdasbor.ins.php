<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
  <div class="page-header" id="top-logo">
    <h3><?= $data->headertext ?></h3>
  </div>

    

  <div class="row">
        <div class="col-md-12">
          <section>
            <h5>Konfigurasi</h5>
            <div class="list-group">
              <a href="<?= $data->base_url.'administrasi/insentif/config-leveltrainer' ?>" class="list-group-item">Level Trainer</a>
              <a href="<?= $data->base_url.'administrasi/insentif/config-jeniskegiatan' ?>" class="list-group-item">Jenis Kegiatan</a>
              <a href="<?= $data->base_url.'administrasi/insentif/config-jenisaccurate' ?>" class="list-group-item">Jenis Accurate</a>
              <a href="<?= $data->base_url.'administrasi/insentif/config-jnstransaksi' ?>" class="list-group-item">Jenis Transaksi</a>
              <a href="<?= $data->base_url.'administrasi/insentif/config-harikerja' ?>" class="list-group-item">Jenis Hari Kerja</a>
              <a href="<?= $data->base_url.'administrasi/insentif/config-harlibnas' ?>" class="list-group-item">Hari Libur Nasional</a>
              <a href="<?= $data->base_url.'administrasi/insentif/config-dawil' ?>" class="list-group-item">Group Wilayah</a>
              <a href="<?= $data->base_url.'administrasi/insentif/config-tbldawil' ?>" class="list-group-item">Daftar Daerah Wilayah</a>
              <a href="<?= $data->base_url.'administrasi/insentif/config-peran' ?>" class="list-group-item">Insentif Peran</a>
            </div>
          </section>
          
        </div> 
    </div>

</div><!--end container-->
<script type="text/javascript" src='https://fac-institute.com/js/jquery-ui/jquery-ui.js'></script>

</body>
</html>