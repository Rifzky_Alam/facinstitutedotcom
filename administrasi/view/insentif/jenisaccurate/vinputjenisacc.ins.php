<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?= $data->headertext ?></h3>
	</div>

	<div class="row">
        <div class="col-md-12">
            <form action="" method="post" accept-charset="utf-8">
                <div class="form-group">
                    <label>Level Trainer</label>
                    <select name="in[leveltrainer]" class="form-control" required>
                        <option value="">-- pilih level trainer --</option>
                        <?php foreach ($data->listleveltrainer as $key): ?>
                            <option value="<?= $key['intr_id'] ?>"><?= $key['intr_level'] ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Jenis Accurate</label>
                    <select name="in[jenisaccurate]" class="form-control" required>
                        <option value="">-- pilih jenis accurate --</option>
                        <?php foreach ($data->listacc as $key): ?>
                            <option value="<?= $key['va_id'] ?>"><?= $key['va_nama'] ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Tariff Insentif</label>
                    <input type="number" name="in[insentif]" class="form-control" placeholder="Hanya Angka">
                </div>
                <button class="btn btn-lg btn-primary">Submit</button>
            </form>
        </div> 
    </div>

</div><!--end container-->
</body>
</html> 