<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?= $data->headertext ?></h3>
	</div>

    

	<div class="row">
        <div class="col-md-12">
            <form action="" method="post" accept-charset="utf-8">
                <div class="form-group">
                    <label>Level Trainer</label>
                    <input type="text" name="in[level]" class="form-control" placeholder="Beri nama level untuk trainer di sistem insentif">
                </div>
                <button class="btn btn-lg btn-primary">Submit</button>
            </form>
        </div> 
    </div>

</div><!--end container-->
</body>
</html> 