<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?= $data->headertext ?></h3>
	</div>

    <div class="row">
        <div class="col-md-12">
            <section>
                <p>Pastikan status data aktif agar data dapat di baca oleh sistem di perubahan data user</p>
            </section>
        </div>
    </div>


	<div class="row">
        <div class="col-md-12">
            <a href="<?= $data->base_url.'administrasi/insentif/new-leveltrainer' ?>" title="">Data Baru</a>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Level Trainer</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($data->listdata)=='0'): ?>
                        <tr>
                            <td colspan="3" style="text-align:center;">Tidak Ada data di dalam database kami</td>
                        </tr>
                    <?php else: ?>
                        <?php foreach ($data->listdata as $key): ?>
                            <tr>
                                <td><?= $key['intr_level'] ?></td>
                                <td><?= $key['dt_desc'] ?></td>
                                <td><a href="<?= $data->base_url.'administrasi/insentif/edit-statuslvltrainer/'.$key['intr_id'].'?st=1' ?>" title="Edit">Aktifkan</a> || <a href="<?= $data->base_url.'administrasi/insentif/edit-statuslvltrainer/'.$key['intr_id'].'?st=0' ?>" title="Remove">Non-Aktifkan</a></td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                    
                </tbody>
            </table>  
        </div> 
    </div>

</div><!--end container-->
</body>
</html> 