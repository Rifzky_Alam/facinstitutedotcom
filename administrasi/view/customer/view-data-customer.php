<!DOCTYPE html>
<html>


<?php include_once 'view/main-component/header.php'; ?>

<body>



<?php
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3>Input Attachments</h3>
	</div>
    <div class="row">
      <div class="col-md-12">
        <a data-toggle="modal" href="#modal-cari" href="#">Search Data <span class="glyphicon glyphicon-search"></span></a>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <button type="button" style="width:100%;" class="btn btn-info" data-toggle="collapse" data-target="#laporanmarketing">Lihat Laporan Marketing</button>
      </div>
    </div>

    <div class="row collapse" id="laporanmarketing">
      <div class="col-md-12">
        <table class="table table-hover">
          <thead>
            <tr>
              <th class="tengah">Nama Marketing</th>
              <th class="tengah">Jumlah Customer</th>
            </tr>
          </thead>
          <tbody>
            <?php DataMarketing($data->report_marketing) ?>
          </tbody>
        </table>
      </div>
    </div>

    <div class="row" style="margin-top:15px;overflow:auto;">
        <div class="col-md-12">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="tengah">No</th>
                        <th class="tengah">Nama Customer</th>
                        <th class="tengah">Perusahaan</th>
                        <th class="tengah">Email</th>
                        <th class="tengah">Telepon</th>
                        <th class="tengah">Transaksi</th>
                        <th class="tengah">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                <?php Rows($data->list) ?>
                </tbody>
            </table>
        </div>
    </div>

	<?php Pagination($data->url,30,$data->pagenum,$data->totaldata,5) ?>

</div><!--end container-->

<!-- Modal -->
  <div class='modal fade' id='modal-cari' role='dialog'>
    <div class='modal-dialog'>

      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pencarian Data</h3>

        </div>
        <div class='modal-body'>
        <div style='padding-left:15px;padding-right:15px;padding-top:0px; height: 300px;overflow-y:auto;' class='row'>



          <div class="col-md-12">
            <form action="" method="get">
            <div class="row">
                <div class="form-group">
                    <label>Nama Customer</label>
                    <input type="text" class="form-control" name="src[nc]">
                </div>
                <div class="form-group">
                    <label>Nama Marketing</label>
                    <select class="form-control" name="src[nm]">
                      <option value="">--Pilih Nama Marketing--</option>
                      <?php DataSelectMarketing($data->report_marketing) ?>
                    </select>
                </div>


            </div>

            <div class="row">
                <button class="btn btn-lg btn-success" style="width:100%">Cari</button>
            </div>
            </form>
          </div>



        </div>


        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>FAC-Institute 2016</span>
                </div>
                <div class="col-md-6">
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
            </div>

        </div>
      </div>

    </div>
  </div>
  <!-- end modal -->

</body>
</html>


<?php function Rows($data){ ?>
    <?php if (count($data)=='0'): ?>
        <tr>
            <td colspan="6">Tidak ada data yang masuk ke dalam sistem kami.</td>
        </tr>
    <?php else: ?>
        <?php $num = 0; ?>
        <?php foreach ($data as $key) { ?>
            <?php $num +=1 ?>
            <tr>
                <td style="vertical-align: middle;" class="tengah"><?php echo $num ?></td>
                <td style="vertical-align: middle;"><?php echo $key->nama_cust ?></td>
                <td style="vertical-align: middle;"><?php echo $key->nama_usaha ?></td>
                <td style="vertical-align: middle;"><?php echo $key->email_cust ?></td>
                <td style="vertical-align: middle;"><a href="tel:\\<?php echo str_replace('-', '', $key->telp_cust) ?>"><?php echo $key->telp_cust ?></a></td>
                <td style="vertical-align: middle;">
                    <?php $tr = explode(' # ', $key->data_tr) ?>
                    <ul style="padding-left:15px">
                    <?php foreach ($tr as $keyz) {
                        echo "<li><a href='new-perusahaan-info?tr=$keyz'>".$keyz."</a></li>";
                    }  ?>
                    </ul>
                </td>
                <td style="vertical-align: middle;">
                    <a href="new-customer?ec=<?php echo $key->id_cust ?>">Ubah Perusahaan</a>
                    ||
                    <a href="new-transaksiz?c=<?php echo $key->id_cust ?>">Tambah Transaksi</a>
                    ||
                    <a href="new-customer?edt=<?php echo $key->id_cust ?>">Edit</a>
                </td>
            </tr>
        <?php } ?>
    <?php endif ?>
<?php } ?>

<?php function DataMarketing($data){ ?>
        <?php foreach ($data as $key) { ?>
            <tr>
                <td style="vertical-align: middle;"><?php echo $key->marketing_nama ?></td>
                <td style="vertical-align: middle;">
                    <a href="<?php echo 'data-customer?src[nc]=&src[nm]='.$key->marketing_id ?>" title="Klik untuk detail">
                        <?php echo $key->total_cust ?>        
                    </a>
                </td>
            </tr>
        <?php } ?>
<?php } ?>

<?php function DataSelectMarketing($data){ ?>
        <?php foreach ($data as $key) { ?>
            <option value="<?php echo $key->marketing_id ?>"><?php echo $key->marketing_nama ?></option>
        <?php } ?>
<?php } ?>

<?php function Emails($arr,$data){ ?>
    <?php foreach ($arr as $key){ ?>
        <?php if ($key==$data): ?>
            <div class="radio">
                <label>
                    <input checked type="radio" name="ubh[email]" value="<?php echo $key ?>">
                    <?php echo $key ?>
                </label>
            </div>
            <?php else: ?>
            <div class="radio">
                <label>
                    <input type="radio" name="ubh[email]" value="<?php echo $key ?>">
                    <?php echo $key ?>
                </label>
            </div>
        <?php endif ?>
    <?php } ?>

<?php } ?>


<?php function Pagination($baseurl,$limit,$page,$totaldata,$limitlist){ ?>
    <?php if ($page!='0'): ?>
        <div class="row">
            <div class="col-md-12">
                <center>
        <?php if (intval($page)*$limit<$totaldata): ?>
            Showing data <? echo (intval($page)*$limit)-$limit . ' - ' . intval($page)*$limit . ' of '.$totaldata;?>    
            <?php else: ?>
            Showing data <? echo intval($page). ' - ' . $totaldata . ' rows of '.$totaldata;?>
        <?php endif ?>
                </center>
            </div>
        </div>
    <?php else: ?>
        <div class="row">
            <div class="col-md-12">
                <center>
                    <?php if ($totaldata<=$limit): ?>
                    Showing data 0 - <?php echo $totaldata ?> rows of <?php echo $totaldata; ?>
                        <?php else: ?>
                    Showing data 0 - <?php echo $limit ?> rows of <?php echo $totaldata; ?>
                    <?php endif ?>
                    
                </center>
            </div>
        </div>
    <?php endif ?>


    <div class="row">
        <div class="col-md-12">
        <center>
            <ul class="pagination pagination-sm">

            <?php 

                // $limitlist = 5;
                if (isset($_GET['page'])) {
                    $page = $_GET['page'];
                } else {
                    $page = 0;
                }
                
                
                $b = intval($page/$limitlist);
                $c = ($b + 1) * $limitlist;
                $batas = intval($totaldata) / $limit;

                if ($page >= $limitlist) {
                    if ($page == $limitlist) {
                        $prev = $b * $limitlist - 2;
                    } else {
                        $prev = $b * $limitlist -1;
                    }

                    echo "<li><a href='".$baseurl."?page=$prev'>< Prev</a></li>";
                }

                for ($i=$b*$limit-1; $i < $c; $i++) { 
                    $j = $i +1;

                    if ($i<$batas&&$j!=0) {
                        echo "<li><a href='".$baseurl."?page=$j'>$j</a></li>";    
                    }
                }
                
                if ($c < $batas) {
                    $k = $c + 1;
                    echo "<li><a href='".$baseurl."?page=$k'>Next ></a></li>";
                }
                
                // for ($i=1; $i < intval($totalRow) / 30 + 1 ; $i++) { 
                    // echo "<li><a href='".basename(__FILE__, '.php')."?page=$i'>$i</a></li>";   
                // }
            ?>
            </ul>
        </center>
        </div>
    </div>

<? } //end pagination ?>
