<!DOCTYPE html>
<html>
<?php include_once 'view/main-component/header.php'; ?>

<body>

<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
  <div class="page-header" id="top-logo">
    <h3><?php echo $data->subtitle.'  --  '. $data->namacustomer ?></h3>
  </div>
  
  <div class="row">
    <div class="col-md-12">
      <input type="text" id="namausaha" class="form-control" placeholder="Search Data by Name ...">
    </div>
  </div>
  

  <br><br>

  <form action="" method="POST" accept-charset="utf-8">
  <div class="row">
      <div class="col-md-12">
          <table>
              <tbody id="datausaha">
                  
              </tbody>
          </table>

          <button class="btn btn-lg btn-primary" style="width:100%">Ubah Perusahaan!</button>
      </div>
  </div>
  </form>
</div>
     <script type="text/javascript" charset="utf-8">
         $('#namausaha').keyup(function(){
            
            $.ajax({ //start ajax
                  type: "GET",
                  url: "new-usaha",
                  data: {'ajxc':$('#namausaha').val()},
                  cache: false,
                  success: function(data){
                    $('.list').remove();
                     $('#datausaha').append(data);
                  }
                }); //end ajax
         });
     </script>
</body>
</html>
