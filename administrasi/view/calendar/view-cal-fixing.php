<!DOCTYPE html>
<html>

<?php include_once 'view/main-component/header.php'; ?>

<body>

<?php
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->subtitle ?></h3>
	</div>
    
    <div class="row">
        <div class="col-md-12">
            <section>
                <p>Berikut ini adalah data staff yang sudah verifikasi tanggal transaksi ini.</p>
            </section>
        </div>
    </div>

    <div class="row" style="margin-top:15px;">
        <div class="col-md-12">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="tengah">Staff</th>
                        <th class="tengah">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($data->listdata)=='0'): ?>
                        <tr>
                            <td colspan="2" class="tengah">Tidak ada data</td>
                        </tr>
                        <?php else: ?>
                        <?php foreach ($data->listdata as $key): ?>
                            <tr>
                                <td><?= $key['nama'] ?></td>
                                <td><a href="#" title="batalkan">Batalkan</a></td>
                            </tr>
                        <?php endforeach ?>        
                    <?php endif ?>
                    

                </tbody>
            </table>
            <a href="#" class="btn btn-lg btn-warning" title="">Kembali ke transaksi</a>
        </div>
    </div>

	<?php #Pagination($data->url,30,$data->pagenum,$data->totaldata,5) ?>

</div><!--end container-->

<!-- Modal -->
  <div class='modal fade' id='modal-cari' role='dialog'>
    <div class='modal-dialog'>

      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pencarian Data</h3>

        </div>
        <div class='modal-body'>
        <div style='padding-left:15px;padding-right:15px;padding-top:0px; height: 300px;overflow-y:auto;' class='row'>

          <div class="col-md-12">
            <form action="" method="get">
            <div class="row">
                <div class="form-group">
                    <label>Nama Perusahaan</label>
                    <input type="text" class="form-control" name="src[nu]">
                </div>
                <div class="form-group">
                    <label>Nama Agenda</label>
                    <input class="form-control" type="text" name="src[na]">
                </div>
                <div class="form-group">
                    <label>Nama Peserta (Staff/Trainer)</label>
                    <input class="form-control" type="text" name="src[np]">
                </div>
            </div>

            <div class="row">
                <button class="btn btn-lg btn-success" style="width:100%">Cari</button>
            </div>
            </form>
          </div>

        </div>


        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>FAC-Institute 2017 - Rifzky Alam</span>
                </div>
                <div class="col-md-6">
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
            </div>

        </div>
      </div>

    </div>
  </div>
  <!-- end modal -->

</body>
</html>