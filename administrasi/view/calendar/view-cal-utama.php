<!DOCTYPE html>
<html>


<?php include_once 'view/main-component/header.php'; ?>

<body>



<?php
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->subtitle ?></h3>
	</div>
    <div class="row">
      <div class="col-md-12">
        <a data-toggle="modal" class="btn btn-info" href="#modal-cari" href="#">Search Data <span class="glyphicon glyphicon-search"></span></a>
        <a href="<?= $data->base_url.'administrasi/calendar?gcal=table' ?>" class="btn btn-danger" title="Data Google Calender">Google Calendar</a>
      </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            Total Data: <?php echo $data->totalrow ?>
        </div>
    </div>

    <div class="row" style="margin-top:15px;overflow:auto;">
        <div class="col-md-12">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="tengah">Tanggal</th>
                        <th class="tengah">Lokasi Acara</th>
                        <th class="tengah">Deskripsi Acara</th>
                        <th class="tengah">Peserta</th>
                    </tr>
                </thead>
                <tbody>
                <?php Rows($data->list) ?>
                </tbody>
            </table>
        </div>
    </div>

	<?php #Pagination($data->url,30,$data->pagenum,$data->totaldata,5) ?>

</div><!--end container-->

<!-- Modal -->
  <div class='modal fade' id='modal-cari' role='dialog'>
    <div class='modal-dialog'>

      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pencarian Data</h3>

        </div>
        <div class='modal-body'>
        <div style='padding-left:15px;padding-right:15px;padding-top:0px; height: 300px;overflow-y:auto;' class='row'>



          <div class="col-md-12">
            <form action="" method="get">
            <div class="row">
                <div class="form-group">
                    <label>Nama Perusahaan</label>
                    <input type="text" class="form-control" name="src[nu]">
                </div>
                <div class="form-group">
                    <label>Nama Agenda</label>
                    <input class="form-control" type="text" name="src[na]">
                </div>
                <div class="form-group">
                    <label>Nama Peserta (Staff/Trainer)</label>
                    <input class="form-control" type="text" name="src[np]">
                </div>
            </div>

            <div class="row">
                <button class="btn btn-lg btn-success" style="width:100%">Cari</button>
            </div>
            </form>
          </div>



        </div>


        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>FAC-Institute 2017 - Rifzky Alam</span>
                </div>
                <div class="col-md-6">
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
            </div>

        </div>
      </div>

    </div>
  </div>
  <!-- end modal -->

</body>
</html>


<?php function Rows($data){ ?>
    <?php if (count($data)=='0'): ?>
        <tr>
            <td colspan="6">Tidak ada data yang masuk ke dalam sistem kami.</td>
        </tr>
    <?php else: ?>
        <?php $num = 0; ?>
        <?php foreach ($data as $key) { ?>
            <?php $num +=1 ?>
            <tr>
                <td style="vertical-align: middle;" class="tengah"><?php echo Tanggal($key->tanggal) ?></td>
                <td style="vertical-align: middle;">
                    <a href="new-detail?tr=<?= $key->transaksi ?>" title="klik untuk melihat detail">
                        <?php echo $key->acara ?>                
                    </a>
                </td>
                <td style="vertical-align: middle;"><?php echo $key->nama_agenda ?></td>
                <td style="vertical-align: middle;"><?php echo $key->petugas ?></td>
            </tr>
        <?php } ?>
    <?php endif ?>
<?php } ?>

<?php function Tanggal($value){
    $tgl = explode('-',$value);
    return $tgl[2].'-'.$tgl[1].'-'.$tgl[0];
} ?>

