<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>


<div class="container" id="isi" style="padding-bottom:40px;">
	<div class="page-header" id="top-logo">
		<h3><?= $data->subtitle ?></h3>
	</div>


        <div class='row' style="overflow:auto;">
            <div class='col-md-6'>
                <h3>Pengajuan</h3>
                <?php $idata = $data->rowdata  ?>
                <table class='table table-bordered'>
                    <tbody>
                        <tr>
                            <td>Nama Staff</td>
                            <td><?= $idata['nama'] ?></td>
                        </tr>
                        <tr>
                            <td>Hari/Tanggal</td>
                            <td><?= $idata['tanggal'] ?></td>
                        </tr>
                        <tr>
                            <td>Nama Perusahaan</td>
                            <td><?= $idata['nama_usaha'] ?></td>
                        </tr>
                        <tr>
                            <td>Alamat Perusahaan</td>
                            <td><?= $idata['alamat'] ?></td>
                        </tr>
                        <tr>
                            <td>Biaya Transport</td>
                            <td><?= $idata['biaya_transport'] ?></td>
                        </tr>
                        <tr>
                            <td>Keterangan Biaya Transport</td>
                            <td><?= $idata['ket_transport'] ?></td>
                        </tr>
                        <tr>
                            <td>Biaya Makan Siang</td>
                            <td><?= $idata['biaya_lunch'] ?></td>
                        </tr>
                        <tr>
                            <td>Biaya Makan Malam</td>
                            <td><?= $idata['biaya_dinner'] ?></td>
                        </tr>
                        <tr>
                            <td>Biaya Parkir</td>
                            <td><?= $idata['biaya_parkir'] ?></td>
                        </tr>
                        <tr>
                            <td>Biaya Kesehatan</td>
                            <td><?= $idata['biaya_kesehatan'] ?></td>
                        </tr>
                        <tr>
                            <td>Biaya Service</td>
                            <td><?= $idata['biaya_allowance'] ?></td>
                        </tr>
                        <tr>
                            <td>Biaya Lainnya</td>
                            <td><?= $idata['biaya_lain'] ?></td>
                        </tr>
                        <tr>
                            <td>Keterangan Biaya Lainnya</td>
                            <td><?= $idata['ket_biaya_lain'] ?></td>
                        </tr>
                    </tbody>
                </table>
                <div class="row">
                    <?php //print_r($data->images) ?>
                    <?php foreach ($data->images as $key): ?>
                        <div class="col-md-6">
                            <div class="thumbnail">
                                <a href="#">
                                    <img src="<?= $data->base_url.'administrasi/reimproof/'.$key['sfilename'] ?>" class="img-responsive" alt="Bukti Reimburse">
                                </a>
                            </div>
                            <div class="caption" style="text-align:center;">
                                <span><?= $key['desk'] ?></span>
                            </div>
                        </div>
                    <?php endforeach ?>

                </div>
            </div>
            <div class='col-md-6'>
                <h3>Disetujui</h3>
                <hr/>
                <!--
                    input hidden buat history pengisian oleh staff
                    
                -->
                <form action="" method="post">
                <div class="checkbox">
                    <label><input type="checkbox" name="in[setuju]" id="setuju" value="1" checked>Laporan disetujui</label>
                </div>
                <div id="hidform" class="collapse">
                <div class="form-group">
                    <label>Biaya Transport</label>
                    <input type="text" name="in[transport]" class="form-control" placeholder="hanya angka" value="<?= $idata['biaya_transport'] ?>" />
                </div>
                <div class="form-group">
                    <label>Biaya Makan Siang</label>
                    <input type="text" name="in[lunch]"  class="form-control" placeholder="hanya angka" value="<?= $idata['biaya_lunch'] ?>" />
                </div>
                <div class="form-group">
                    <label>Biaya Makan Malam</label>
                    <input type="text" class="form-control" placeholder="hanya angka" name="in[dinner]" value="<?= $idata['biaya_dinner'] ?>" />
                </div>
                <div class="form-group">
                    <label>Biaya Parkir</label>
                    <input type="text" class="form-control" placeholder="hanya angka" name="in[parkir]" value="<?= $idata['biaya_parkir'] ?>" />
                </div>
                <div class="form-group">
                    <label>Biaya Kesehatan</label>
                    <input type="text" class="form-control" placeholder="hanya angka" name="in[medic]" value="<?= $idata['biaya_kesehatan'] ?>" />
                </div>
                <div class="form-group">
                    <label>Biaya Servis</label>
                    <input type="text" class="form-control" placeholder="hanya angka" name="in[allow]" value="<?= $idata['biaya_allowance'] ?>" />
                </div>
                <div class="form-group">
                    <label>Biaya Lainnya</label>
                    <input type="text" class="form-control" placeholder="hanya angka" name="in[other]" value="<?= $idata['biaya_lain'] ?>" />
                </div>
                <div class="form-group">
                    <label>Alasan Ditolak</label>
                    <textarea class="form-control" placeholder="Alasan Ditolak" name="in[reason]" ></textarea>
                </div>
                </div>
                <button class="btn btn-lg btn-primary">Submit</button>
                <?php $tgl = explode('-',$idata['tgl']) ?>
                
                <a href="<?= $data->base_url.'administrasi/user/reimbport?bln='.intval($tgl[1]).'&thn='.$tgl[0].'&user='.$idata['username'].'&idx='.intval($data->idx +1) ?>" class="btn btn-lg btn-warning">Skip</a>
                </form>
                
            </div>
        </div>
        
</div>

<script type="text/javascript" src="<?= $data->base_url.'js/jquery-ui/jquery-ui.js' ?>"></script>
  <script type="text/javascript">
    $(document).ready(function(){
        $('#setuju').click(function(){
            if ($(this).is(':checked')) {
                $("#hidform").collapse('hide');
            }else{
                $("#hidform").collapse('show');
            }
        });
    });

    

    function removeElement(id) {
        $('#'+id).remove();
    }

  </script>   
</body>
</html> 


