<!DOCTYPE html>
<html>
<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCUn6Q6sg0JVJBlgiz4tL2kF1zmVoC0ScE&libraries=places&sensor=true"></script>
<?php 
// include_once $data->homedir.'administrasi/map-script.php'; 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->subtitle ?></h3>
	</div>

  <div class="row">
    <div class="col-md-10">
      <p>Nama Petugas: <?= $data->userdata['nama'] ?></p>
    </div>
  </div>

  <div class="row">
    <div class="col-md-10">
      <form action="" method="POST" accept-charset="utf-8">
      
                <div class="form-group">
                    <label for="alamat">User Level</label>
                    <select class="form-control" name="in[level]">
                      <?php foreach ($data->leveluser as $key): ?>
                        <?php if ($data->userdata['usr_level']==$key['intr_id']): ?>
                          <option value="<?= $key['intr_id'] ?>" selected="selected"><?= $key['intr_level'] ?></option>
                        <?php else: ?>
                          <option value="<?= $key['intr_id'] ?>"><?= $key['intr_level'] ?></option>
                        <?php endif ?>
                        
                      <?php endforeach ?>
                    </select>                    
                </div>
      
      <button class="btn btn-lg btn-primary">Submit</button>
      </form>
    </div>
  </div>

</div>
<script type="text/javascript" src="<?= $data->base_url ?>js/jquery-ui/jquery-ui.js"></script>
<script>
</script>
     
</body>
</html> 