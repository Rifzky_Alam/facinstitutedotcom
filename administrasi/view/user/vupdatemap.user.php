<!DOCTYPE html>
<html>
<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCUn6Q6sg0JVJBlgiz4tL2kF1zmVoC0ScE&libraries=places&sensor=true"></script>
<?php 
include_once $data->homedir.'administrasi/map-script.php'; 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->subtitle ?></h3>
	</div>

  <div class="row">
    <div class="col-md-10">
      <form action="" method="POST" accept-charset="utf-8">
      
                <div class="form-group">
                    <label for="alamat">Domisili</label>
                    <input type="text" name="in[map]" id="alamatz" class="form-control" placeholder="Cari Nama Tempat atau Tandai Peta di Bawah ini">                    
                </div>
                
                <br>
                <div class="row" style="margin-bottom:20px">
                    <div class="col-md-10">
                        <div id="map-canvas" style="width:100%;height:400px;border:1px solid"></div>
                    </div>
                </div>
      
      <button class="btn btn-lg btn-primary">Submit</button>
      </form>
    </div>
  </div>

</div>
<script type="text/javascript" src="<?= $data->base_url ?>js/jquery-ui/jquery-ui.js"></script>
<script>
  $(document).ready(function(){
    $('#tgl').datepicker({
      dateFormat: 'yy-mm-dd'
    });
  });

  $('#samadenganinvoice').click(function(){
    var cek = $('#samadenganinvoice').is(":checked");
    if (cek) {
      // alert('iya, jumlah sama dengan invoice.');
      $('#totaltelahterbayar').fadeOut(1000);
    }else{
      // alert('tidak, saya punya jumlah yang lain.');
      $('#totaltelahterbayar').fadeIn(1000);
    }
  });
</script>
     
</body>
</html> 