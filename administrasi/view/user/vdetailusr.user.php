<!DOCTYPE html>
<html>
<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>
<?php 
// include_once $data->homedir.'administrasi/map-script.php'; 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->subtitle ?></h3>
	</div>


  <div class="row">
    <div class="col-md-3">
      <div class="row">
        <?php if (@$data->picture==''||empty(@$data->picture)): ?>
          <img src="https://www.ravensbourne.ac.uk/content/img/default-pupil-profile.png" class="img img-responsive">
        <?php else: ?>
          <img src="<?= $data->base_url ?>images/member/<?= $data->picture ?>" class="img img-responsive">        
        <?php endif ?>            
      </div>
      <br>
      <div class="row">
        <form id="my-form" action="" enctype="multipart/form-data" method="post">
          <center>
            <input id="file-3" type="file" class="file" name="cobaz">
          </center>
        </form>
      </div>
    </div>
    <div class="col-md-9">
      <table class="table table-bordered">
        <tbody>
          <tr>
            <td colspan="2" class="tengah"><?= $data->namalengkap ?></td>
          </tr>
          <tr>
            <td>Username</td>
            <td><?= $data->usrname ?></td>
          </tr>
          <tr>
            <td>Tanggal Lahir</td>
            <td><?= $data->tgllahir ?></td>
          </tr>
          <tr>
            <td>Email</td>
            <td><?= $data->email ?></td>
          </tr>
          <tr>
            <td>Telepon</td>
            <td><?= $data->telepon ?></td>
          </tr>
          <tr>
            <td>Team</td>
            <td><?= $data->team ?></td>
          </tr>
          <tr>
            <td>Level</td>
            <td><?= $data->level ?></td>
          </tr>
          <tr>
            <td>Status</td>
            <td><?= $data->status ?></td>
          </tr>
        </tbody>
      </table>
      <form action="" method="post">
          <input type="hidden" name="in[tanggal]" value="<?= date('Y-m-d') ?>" />
          <button class="btn btn-success">Set Sebagai Best Trainer</button>
      </form>
    </div>
  </div>

</div>
<script type="text/javascript" src="<?= $data->base_url ?>js/jquery-ui/jquery-ui.js"></script>
<script type="text/javascript" src="<?= $data->base_url ?>css/datepicker/js/bootstrap-datepicker.js"></script>
<script>
  $(document).ready(function(){
    $('#tglLahir').datepicker({
       format: "yyyy-mm-dd"
    });
  });
</script>
     
</body>
</html> 