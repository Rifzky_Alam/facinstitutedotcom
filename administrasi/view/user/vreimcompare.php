<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>


<div class="container" id="isi" style="padding-bottom:40px;">
	<div class="page-header" id="top-logo">
		<h3><?= $data->subtitle ?></h3>
	</div>


        <div class='row' style="overflow:auto;">
            <div class='col-md-6'>
                <h3>Pengajuan</h3>
                <table class='table table-bordered'>
                    <tbody>
                        <tr>
                            <td>Biaya Transport</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Biaya Makan Siang</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Biaya Makan Malam</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Biaya Parkir</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Biaya Kesehatan</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Biaya Allowance</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Biaya Lainnya</td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class='col-md-6'>
                <h3>Disetujui</h3>
                <table class='table table-bordered'>
                    <tbody>
                        <tr>
                            <td>Biaya Transport</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Biaya Makan Siang</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Biaya Makan Malam</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Biaya Parkir</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Biaya Kesehatan</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Biaya Allowance</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Biaya Lainnya</td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <?php Pagination($data,30,$data->pagenum,$data->totaldata,5) ?>
</div>

<script type="text/javascript" src=<?php echo "'".$data->base_url."js/jquery-ui/jquery-ui.js'"; ?>></script>
  <script type="text/javascript">
    // $(document).ready(function(){
    //     $('#todate').datepicker({
    //         dateFormat: 'yy-mm-dd'
    //     });
    //     $('#fromdate').datepicker({
    //         dateFormat: 'yy-mm-dd'
    //     });
    //     $('#tanggal-akhir').datepicker({
    //         dateFormat: 'yy-mm-dd'
    //     });
    //     $('.tanggalPelaksanaan').datepicker({
    //         dateFormat: 'yy-mm-dd'
    //     });
    // });

    

    function removeElement(id) {
        $('#'+id).remove();
    }

  </script>   
</body>
</html> 


