<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>


<div class="container" id="isi" style="padding-bottom:40px;">
	<div class="page-header" id="top-logo">
		<h3><?= $data->subtitle ?></h3>
	</div>


        <div class='row' style="overflow:auto;">
            <div class='col-md-6'>
                <h3>Pengajuan</h3>
                <?php $idata = $data->rowdata  ?>
                <table class='table table-bordered'>
                    <tbody>
                        <tr>
                            <td>Nama Staff</td>
                            <td><?= $idata['nama'] ?></td>
                        </tr>
                        <tr>
                            <td>Hari/Tanggal</td>
                            <td><?= $idata['tanggal'] ?></td>
                        </tr>
                        <tr>
                            <td>Biaya Transport</td>
                            <td><?= $idata['biaya_transport'] ?></td>
                        </tr>
                        <tr>
                            <td>Keterangan Biaya Transport</td>
                            <td><?= $idata['ket_transport'] ?></td>
                        </tr>
                        <tr>
                            <td>Biaya Makan Siang</td>
                            <td><?= $idata['biaya_lunch'] ?></td>
                        </tr>
                        <tr>
                            <td>Biaya Makan Malam</td>
                            <td><?= $idata['biaya_dinner'] ?></td>
                        </tr>
                        <tr>
                            <td>Biaya Parkir</td>
                            <td><?= $idata['biaya_parkir'] ?></td>
                        </tr>
                        <tr>
                            <td>Biaya Kesehatan</td>
                            <td><?= $idata['biaya_kesehatan'] ?></td>
                        </tr>
                        <tr>
                            <td>Biaya Allowance</td>
                            <td><?= $idata['biaya_allowance'] ?></td>
                        </tr>
                        <tr>
                            <td>Biaya Lainnya</td>
                            <td><?= $idata['biaya_lain'] ?></td>
                        </tr>
                        <tr>
                            <td>Keterangan Biaya Lainnya</td>
                            <td><?= $idata['ket_biaya_lain'] ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class='col-md-6'>
                <h3>Bukti</h3>
                <hr/>
                <form action=''method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Bukti Reimburse</label>
                        <input id="file-3" type="file" class="file" name="ifile">
                    </div>
                </form>
            </div>
        </div>
        
</div>

<script type="text/javascript" src=<?php echo "'".$data->base_url."js/jquery-ui/jquery-ui.js'"; ?>></script>
  <script type="text/javascript">
    // $(document).ready(function(){
    //     $('#setuju').click(function(){
    //         if ($(this).is(':checked')) {
    //             $("#hidform").collapse('hide');
    //         }else{
    //             $("#hidform").collapse('show');
    //         }
    //     });
    // });

    

    function removeElement(id) {
        $('#'+id).remove();
    }

  </script>   
</body>
</html> 


