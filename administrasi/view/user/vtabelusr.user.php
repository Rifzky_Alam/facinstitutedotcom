<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?= $data->subtitle ?></h3>
	</div>
	
    <!--
	<div class="row">
		<div class="col-sm-12 col-md-12" style="text-align:right"><a class="" id="cari-data" href="#modal-cari" data-toggle="modal">Cari Data?</a></div>
	</div>
    -->

    <div class='row'>
        <div class='col-md-12'>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Username</th>
                        <th>Tanggal Lahir</th>
                        <th>Email</th>
                        <th>Telepon</th>
                        <th>Team</th>
                        <th>Status</th>
                        <th>Level</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($data->listpetugas)=='0'): ?>
                        <tr>
                            <td colspan="5" class="tengah">Tidak Ada Data Dalam Database Kami</td>
                        </tr>    
                        <?php else: ?>
                        <?php foreach ($data->listpetugas as $key): ?>
                            <?php if($key['besttrainer']=='1'): ?>
                                <tr class="success">
                            <?php else: ?>
                                <tr>
                            <?php endif ?>
                            
                                <td>
                                    <a href="<?= $data->base_url.'administrasi/user/detail/'.$key['username'] ?>" title="Detail">
                                        <?= $key['nama'] ?>            
                                    </a>
                                </td>
                                <td><?= $key['username'] ?></td>
                                <td><?= $key['tanggal_lahir'] ?></td>
                                <td><?= $key['email'] ?></td>
                                <td><?= $key['telepon'] ?></td>
                                <td><?= $key['team'] ?></td>
                                <td><?= $key['statususer'] ?></td>
                                <td><?= $key['leveluser'] ?></td>
                                <td><?= $key['picture'] ?></td>
                            </tr>

                        <?php endforeach ?>
                    <?php endif ?>
                </tbody>
            </table>
        </div>

    </div>
	    
</div>
</body>
</html> 



