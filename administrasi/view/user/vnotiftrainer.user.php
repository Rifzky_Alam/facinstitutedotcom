<!DOCTYPE html>
<html>
<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>
<?php 
// include_once $data->homedir.'administrasi/map-script.php'; 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->subtitle ?></h3>
	</div>

  <div class="row">
    <div class="col-md-10">
      <form action="" method="POST" accept-charset="utf-8">
      <div class="form-group">
        <label>Username</label>
        <select class="form-control" name="in[username]">
          <option>--Pilih Username--</option>
          <?php foreach ($data->listusers as $key): ?>
            <option value="<?= $key['username'] ?>"><?= $key['nama'] ?></option>
          <?php endforeach ?>
        </select>
      </div>

      <div class="form-group">
        <label>Notif Dasbor</label>
        <textarea class="form-control" name="in[notifdasbor]"></textarea>
      </div>

      <button class="btn btn-lg btn-primary">Submit</button>
      </form>
    </div>
  </div>

</div>
<script type="text/javascript" src="<?= $data->base_url ?>js/jquery-ui/jquery-ui.js"></script>
<script type="text/javascript" src="<?= $data->base_url ?>css/datepicker/js/bootstrap-datepicker.js"></script>
<script>
  $(document).ready(function(){
    $('#tglLahir').datepicker({
       format: "yyyy-mm-dd"
    });
  });
</script>
     
</body>
</html> 