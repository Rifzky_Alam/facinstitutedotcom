<!DOCTYPE html>
<html>
<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>
<?php 
// include_once $data->homedir.'administrasi/map-script.php'; 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->subtitle ?></h3>
	</div>

  <div class="row">
    <div class="col-md-10">
      <a href="<?= $data->base_url.'administrasi/settings/petugas' ?>" class="btn btn-default">
          <span class="glyphicon glyphicon-user"></span>
        </a>
    </div>
  </div>
  <br>

  <div class="row">
    <div class="col-md-10">
      <form action="" method="POST" accept-charset="utf-8">
      <div class="form-group">
        <label>Username</label>
        <input id="username" type="text" name="in[username]" class="form-control nospace" placeholder="username for login" required>
      </div>

      <div class="form-group">
        <label>Nama Lengkap</label>
        <input type="text" name="in[nama]" class="form-control" placeholder="Nama Lengkap" required>
      </div>

      <div class="form-group">
        <label>Tanggal Lahir</label>
        <input id="tglLahir" type="text" name="in[tgllahir]" class="form-control" placeholder="Tanggal Lahir" data-date-viewmode="years" data-date="02-05-1994" required>
      </div>

      <div class="form-group">
        <label>Email</label>
        <input type="email" name="in[email]" class="form-control nospace" placeholder="Email Aktif" required>
      </div>

      <div class="form-group">
        <label>Telepon</label>
        <input type="text" name="in[telepon]" class="form-control" placeholder="Telepon" required>
      </div>

      <div class="form-group">
        <label>Tipe</label>
        <select class="form-control" name="in[tipe]" required>
          <option value="">-- pilih tipe user --</option>
          <?php foreach ($data->listtipe as $key): ?>
            <option value="<?= $key['ut_id'] ?>"><?= $key['ut_ket'] ?></option>
          <?php endforeach ?>
        </select>
      </div>

      <div class="form-group">
        <label>Team</label>
        <select class="form-control" name="in[team]" required>
          <option value="">-- pilih team user --</option>
          <?php foreach ($data->listteam as $key): ?>
            <option value="<?= $key['ukt_id'] ?>"><?= $key['ukt_ket'] ?></option>
          <?php endforeach ?>
        </select>
      </div>


      <button class="btn btn-lg btn-primary">Submit</button>
      </form>
    </div>
  </div>

</div>
<script type="text/javascript" src="<?= $data->base_url ?>js/jquery-ui/jquery-ui.js"></script>
<script type="text/javascript" src="<?= $data->base_url ?>css/datepicker/js/bootstrap-datepicker.js"></script>
<script>
  $(document).ready(function(){
    $('#tglLahir').datepicker({
       format: "yyyy-mm-dd"
    });
  });

  $('#username').keypress(function(e){
    if(e.which===32){
      return false;
    }
  });
</script>
     
</body>
</html> 