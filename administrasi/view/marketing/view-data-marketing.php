<!DOCTYPE html>
<html>


<?php include_once 'view/main-component/header.php'; ?>

<body>



<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->subtitle ?></h3>
	</div>

    <div class="row">
        <div class="col-md-12">
            <a href="<?php echo $data->url ?>?action=new" class="btn btn-default"><span>New <i class="glyphicon glyphicon-plus"></i></span></a>
        </div>
    </div><br>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th class="tengah">Nama Marketing</th>
                        <th class="tengah">Cabang</th>
                        <th class="tengah">Email</th>
                        <th class="tengah">Telepon</th>
                        <th class="tengah">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php Rows($data->list) ?>
                </tbody>
            </table>
        </div>
    </div>
	<div class="row">

    </div>

</div><!--end container-->
</body>
</html>


<?php function Rows($data){ ?>
    <?php if (count($data)=='0'): ?>
        <tr>
            <td colspan="9">Tidak ada data yang masuk ke dalam sistem kami atau sistem sedang dalam perbaikan.</td>
        </tr>
    <?php else: ?>
        <?php foreach ($data as $key) { ?>
            <tr>
                <td style="vertical-align: middle;"><?php echo $key->marketing_nama ?></td>
                <td style="vertical-align: middle;"><?php echo $key->nama_usaha ?></td>
                <td style="vertical-align: middle;"><?php echo $key->marketing_email ?></td>
                <td style="vertical-align: middle;"><?php echo $key->marketing_telp ?></td>
                <td style="vertical-align: middle;">
                    <a href="<?php echo 'new-marketing?edt='. $key->marketing_id ?>">Edit</a>
                </td>
            </tr>
        <?php } ?>
    <?php endif ?>
<?php } ?>

<?php 
function GetPreviewLink($value){
    if ($value=='1') {
        return 'einv=';
    }elseif ($value=='2') {
        return 'asinv=';
    }elseif ($value=='3') {
        return 'kinv=';
    }else{
        return 'einv=';
    }
}

?>