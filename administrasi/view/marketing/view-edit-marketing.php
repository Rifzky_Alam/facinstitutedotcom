<!DOCTYPE html>
<html>


<?php include_once 'view/main-component/header.php'; ?>

<body>
<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
?>    

    <div class="container" style='margin-bottom:40px;margin-top:40px'>

            <div class='row'>
                <div class='page-header'>                   
                    <h1>Input New Marketing</h1>
                </div>
            </div>
        
            <div class='row'>
                <div class='col-md-12'>
                    <form action="" method="post">
                    <div class="form-group">
                        <label>Nama Lengkap</label>
                        <input value="<?php echo $data->nama ?>" type="text" name="in[namalengkap]" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input value="<?php echo $data->email ?>" type="email" name="in[email]" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Telepon</label>
                        <input value="<?php echo $data->telepon ?>" type="text" name="in[telepon]" class="form-control">
                    </div>
                    
                    <div class="form-group">
                        <label>Cabang</label>
                        <select class="form-control" name="in[cabang]" required>
                            <option>--Pilih Cabang--</option>
                            <?php ListCabang($data->cabang,$data->listcabang) ?>
                        </select>
                    </div>
                    <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>
                    </form> 
                </div>
            </div>


        
    </div>

         
<script type="text/javascript" src=<?php echo "'".getBaseUrl()."css/datepicker/js/bootstrap-datepicker.js'"; ?>></script>
<script type="text/javascript">
    function coba(aidi,myTitle){
        //alert(id);
        document.getElementById("judul-delete").innerHTML=myTitle;
        document.getElementById('konfirm-id').value=aidi;
    }

    $('#tglLahir').datepicker({
           format: "yyyy-mm-dd"
    });
</script>

</body>
</html>

<?php function ListCabang($cabang,$data){ ?>
    <?php foreach ($data as $key) { ?>
        <?php if ($key->id_cabang==$cabang): ?>
                <option value="<?php echo $key->id_cabang ?>" selected><?php echo $key->nama_usaha ?></option>
            <?php else: ?>
                <option value="<?php echo $key->id_cabang ?>"><?php echo $key->nama_usaha ?></option>
        <?php endif ?>
        
    <?php } ?>
<? } ?>