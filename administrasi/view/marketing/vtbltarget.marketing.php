<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>


<div class="container" id="isi" style="padding-bottom:40px;">
	<div class="page-header" id="top-logo">
		<h3><?= $data->subtitle ?></h3>
	</div>


        <div class='row'>
            <div class='col-md-12'>
                <a href="<?= $data->base_url.'administrasi/marketing/config-targetharian' ?>" class="btn btn-primary" title="New Data">New Data</a>
                <br>
                <table class='table table-bordered'>
                    <thead>
                        <tr>
                            <th class='tengah'>Tanggal</th>
                            <th class='tengah'>Expert</th>
                            <th class='tengah'>Satuan</th>
                            <th class='tengah'>Super Expert</th>
                            <th class='tengah'>Satuan</th>
                            <th class='tengah'>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php if (count($data->listdata)=='0'): ?>
                        <tr>
                            <td colspan="8" class="tengah">
                                Tidak ada data dalam database kami.
                            </td>
                        </tr>    
                    <?php else: ?>
                        <?php foreach ($data->listdata as $key): ?>
                            <tr>
                                <td><?= $key['ddate'] ?></td>
                                <td><?= $key['iextarget'] ?></td>
                                <td><?= $key['iexprice'] ?></td>
                                <td><?= $key['isupextarget'] ?></td>
                                <td><?= $key['isupexprice'] ?></td>
                                <td>
                                    <a href="<?= $data->base_url.'administrasi/marketing/data-targetharian?del='.$key['sid'] ?>" title="Hapus" class="btn btn-danger">Hapus</a>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                    </tbody>
                </table>
                
            </div>
        </div>
</div>

<script type="text/javascript" src=<?php echo "'".$data->base_url."js/jquery-ui/jquery-ui.js'"; ?>></script>
  <script type="text/javascript">
    $(document).ready(function(){
        $('#editTgl').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('#tanggal-awal').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('#tanggal-akhir').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('.tanggalPelaksanaan').datepicker({
            dateFormat: 'yy-mm-dd'
        });
    });

    

    function removeElement(id) {
        $('#'+id).remove();
    }

  </script>   
</body>
</html> 



