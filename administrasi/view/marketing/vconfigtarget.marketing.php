<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php 
include_once $data->homedir.'administrasi/view/main-component/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>


<div class="container" id="isi" style="padding-bottom:40px;">
	<div class="page-header" id="top-logo">
		<h3><?= $data->subtitle ?></h3>
	</div>


        <div class='row'>
            <div class='col-md-12'>
                <form action="" method="post" accept-charset="utf-8">
                <div class="form-group">
                    <label>Tanggal</label>
                    <input id="tglfolup" type="text" name="in[tanggal]" class="form-control" placeholder="Tanggal Follow Up" required>
                </div>
                <div class="form-group">
                    <label>Target Expert</label>
                    <input type="text" name="in[targetexpert]" class="form-control" placeholder="hanya angka" required>
                </div>
                <div class="form-group">
                    <label>Harga Satuan Expert (IDR)</label>
                    <input type="text" name="in[hargaexpert]" placeholder="hanya angka" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Target Super Expert</label>
                    <input type="text" name="in[targetsuperexpert]" class="form-control" placeholder="Target Super Expert" required>
                </div>
                <div class="form-group">
                    <label>Harga Satuan Super Expert (IDR)</label>
                    <input type="text" name="in[hargasuperexpert]" class="form-control" placeholder="Harga Super Expert">
                </div>
                <div class="form-group">
                    <label>Notes</label>
                    <textarea name="in[notes]" class="form-control"></textarea>
                </div>
                <button class="btn btn-lg btn-primary">Submit</button>
                </form>
            </div>
        </div>
</div>
   <script type="text/javascript" src=<?php echo "'".$data->base_url."js/jquery-ui/jquery-ui.js'"; ?>></script>
  <script type="text/javascript">
    $(document).ready(function(){
        $('#tglfolup').datepicker({
            dateFormat: 'yy-mm-dd'
        });
    });

    

    function removeElement(id) {
        $('#'+id).remove();
    }

  </script>
</body>
</html> 



