
<!DOCTYPE html>
<html>
<?php include_once HomeDirectory().'administrasi/view/main-component/header.php'; ?>
<body>
<script type="text/javascript">
function initialize() {
	if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position){
        	var myCurrentLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
        	
        	
        	//canvas absen masuk
        	var inputKoordinatMasuk = document.getElementById('koordinat-absenMasuk');
        	inputKoordinatMasuk.value=myCurrentLocation;
        	var mapProp1 = {
    			center:myCurrentLocation,
    			zoom:16,
    			mapTypeId:google.maps.MapTypeId.ROADMAP
  			};

  			var myMarker=new google.maps.Marker({
  				position:myCurrentLocation,
  			});

  			
		  	var map = new google.maps.Map(document.getElementById("map-canvasMasuk"),mapProp1);
		  	var infowindow = new google.maps.InfoWindow({
  				content:"Anda berada disini"
  			});
  			infowindow.open(map,myMarker);

		  	myMarker.setMap(map);

		  	//end canvas masuk
        	
        });
    }
}
	google.maps.event.addDomListener(window, 'load', initialize);
</script>
<?php 
include_once HomeDirectory().'administrasi/sidebar.php';
include_once HomeDirectory().'administrasi/view/main-component/top-nav.php';
?>

<div class="container" id="isi">
	 <div class="page-header">
	 	<h2><?= $data->subtitle ?></h2>
	 </div>

	 <div class="row">
	 	<div class="col-md-6">
	 		<label>Nama Staff : </label><span style="margin-left:10px"><?= $data->nama_petugas ?></span>
	 	</div>
	 </div>


	 <div class="row">
	 	<div class="col-md-6">

	 		<div class="row" style="margin-top:15px">
	 			<div class="col-md-12">
	 				<h3>Absen Masuk</h3>
	 			</div>
	 		</div>
			
    	 	
    	
	 		<!--absen masuk-->
	 		<form action='absensi' method="POST">
	 		<div class="row" style="margin-top:15px">
	 		   <div class="col-md-12">
	 		   	<select name="deskripsi[client]" class='form-control' id='cbNamaClient'>
	 		   		<option value=''>-- Pilih nama client --</option>
	 		   		<option value='f4e745e1015af1c15ebcb70b77f1426c'>FAC-Institute</option>
	 		   		<option value='d5eb8c090d191587bb8a4b875bae4fed'>FAC-Remote</option>
	 		   		<option value="08964cc6406ac24b00bc774eceb799c1">Accurate Business Center MM</option>
            <?php DataClient($data->dataclient) ?>
	 		   		<option value='request'>Request Data (Nama client tidak ada)</option>
	 		   	</select>
	 		   	<br>
				<input name="deskripsi[request]" placeholder="Isi Nama Perusahaan, contoh: PT Asuransi Jasa Raharja" id='autoCompleteTXT' class="form-control" />	 		   	
	 		   </div>
	 		</div>
	 		
	 		<div class="row" style="margin-top:15px">
	 			<div class="col-md-12">
		 			
		 				<input type="text" id="koordinat-absenMasuk" style="display:none" name="deskripsi[koordMasuk]">
		 				<button class="btn btn-success" style="width:100%" id="btn-masuk" name="btn-masuk">Absen Masuk</button>
		 			</form>
	 			</div>
	 		</div>

	 		<div class="row" style="margin-top:15px">
	 			<div class="col-md-12">
	 				<div id="map-canvasMasuk" style="width:100%;height:400px;border:1px solid">Map is here</div>
	 			</div>
	 		</div>

	 	</div><!--end col-6 -->

	 	<div class="col-md-6">
	 		<div class="row" style="margin-top:15px">
	 			<div class="col-md-12">
	 				<h3>Absen Pulang</h3>
	 			</div>
	 		</div>

	 		<div class="row">
	 			<div class="col-md-12">
	 				<p>Anda Belum Absen Masuk</p>
	 			</div>
	 		</div>

    
</div> <!--end container-->
<script type="text/javascript" src="<?= $data->base_url ?>js/jquery.js"></script>
<script type="text/javascript" src="<?= $data->base_url ?>js/jquery-ui/jquery-ui.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
               var availableTags = [<?= $data->DataAutoComplete ?>];
                $("#autoCompleteTXT").autocomplete({
                  source: availableTags
                });

                $('#autoCompleteTXT').css('display','none');

	});			

	$('#cbNamaClient').change(function(){
		var myValue = $(this).val();
		if (myValue=='request'){
			$('#autoCompleteTXT').css('display','');			
		}else{
			$('#autoCompleteTXT').css('display','none');			
		};
	});


</script>
</body>
</html>