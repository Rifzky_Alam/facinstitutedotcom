<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>
<link rel="stylesheet" type="text/css" href=<?php echo "'".$data->base_url."js/jquery-datepicker/jquery-ui.min.css'"; ?>>	
	<script type="text/javascript" src="<?= $data->base_url ?>js/jquery-ui/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="<?= $data->base_url ?>assets/timepicker/css/timepicki.css">
<?php //include_once $data->homedir.'administrasi/view/main-component/links.php'; ?>	  


<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?= $data->subtitle ?></h3>
	</div>
	
    <div class='row'>
        <div class='col-md-12'>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Tanggal Absen</th>
                        <th>Perusahaan</th>
                    </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><?= $data->tglabsen ?></td>
                    <td><?= $data->nama_usaha ?></td>
                  </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <form action="" method="post" accept-charset="utf-8">
        
        <div class="form-group">
        <label for="absenpulang">Absen pulang anda</label> 
          <input type="text" name="in[jam]" class="form-control" id="absenpulang" required>
        </div>

        <div class="form-group">
          <label for="absenpulang">Alasan tidak absen</label> 
          <textarea name="in[alasan]" class="form-control" placeholder="kurang dari 500 karakter" maxlength="500" required></textarea>
        </div>
        <button style="width:100%" class="btn btn-lg btn-primary">Submit</button>

        </form>
      </div>
    </div>
	    
</div>

<script type="text/javascript" src="<?= $data->base_url ?>assets/jqnumber/jquery.number.js"></script>
<script type="text/javascript" src="<?= $data->base_url ?>assets/timepicker/js/timepicki.js"></script>
<script>
    $('#absenpulang').timepicki({
      // show_meridian:false,
      start_time: ["05", "00","PM"],
      disable_keyboard_mobile: true
    });
    // $('#timepicker2').timepicki();
    // $('#txtInvestasi').number( true, 0 );
</script>
<script type="text/javascript" src='https://fac-institute.com/js/jquery-ui/jquery-ui.js'></script>

</body>
</html> 

