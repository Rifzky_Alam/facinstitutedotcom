<!DOCTYPE html>
<html>


<head>
<meta charset='utf-8'>
<meta name="viewport" content="width=device-width, initial-scale=1">


  <title><?= $data->title ?></title>
	<?php $data->View('main-component/links',$data); ?>	  

</head>

<body>

<?php 
$data->IncludeFileWithData('administrasi/sidebar.php',$data);
$data->View('main-component/top-nav',$data);
?>


<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?= $data->subtitle ?></h3>
	</div>
	
    <div class='row'>
        <div class='col-md-12'>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Tanggal</th>
                        <th>Nama Perusahaan</th>
                        <th>Hari</th>
                        <th>Jenis Hari</th>
						<th>Jam Masuk</th>
						<th>Jam Keluar</th>
                        <th>Overtime</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                 <?php if (count($data->tablelist)=='0'): ?>
                   <tr>
                     <td colspan="7" style="text-align:center;">
                       Tidak ada data dalam database kami
                     </td>
                   </tr>
                 <?php else: ?>
                  <?php foreach ($data->tablelist as $key): ?>
                    <tr>
                      <td><?= $key['nama_user'] ?></td>
                      <td><?= $key['tanggal_absen'] ?></td>
                      <td><?= $key['nama_usaha'] ?></td>
                      <td><?= $data->objlaporan->cariNamaHari(FindDay($key['tanggal_absen'])) ?></td>					  
                      <td><?= $data->objlaporan->cariHariKerjaNasional(FindDay($key['tanggal_absen'])) ?></td>
					  <td><?= $key['absen_masuk'] ?></td>
					  <td><?= $key['absen_pulang'] ?></td>
                      <td><?= RoundOvertime(intval($data->objlaporan->carioverTime($key['absen_masuk'],$key['absen_pulang']))) ?></td>
                      <td>
                        <a href="<?= $data->base_url.'administrasi/attendance/createreport/'.$key['id'] ?>">bentuk laporan</a>
                        ||
                        <a href="<?= $data->base_url.'administrasi/attendance/rejectrequest/'.$key['id'] ?>">Tolak Perubahan</a>
                      </td>
                    </tr>
                  <?php endforeach ?>
                 <?php endif ?>
                </tbody>
            </table>
        </div>

    </div>
	    
</div>
</body>
</html> 
<?php function Rows($data){ ?>
  <?php if (count($data)=='0'): ?>
    <tr>
      <td colspan="3">Tidak Ada data bahwa absen anda belum lengkap.</td>
    </tr>
  <?php else: ?>
    <?php foreach ($data as $key): ?>
      <tr>
        <td><?= $key->tanggal_absen ?></td>
        <td><?= $key->nama_usaha ?></td>
        <td>
          <a href="attendance/edit/<?= $key->id ?>" title="Edit Absen">Edit Absen</a>
        </td>
      </tr>
    <?php endforeach ?>
  <?php endif ?>
<?php } ?>
