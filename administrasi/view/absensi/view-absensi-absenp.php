<!DOCTYPE html>
<html>
<?php include_once HomeDirectory().'administrasi/view/main-component/header.php'; ?>
<body>


<script type="text/javascript">
function initialize() {
	if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position){
        	var myCurrentLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
        	
        	        		//-6.1744,106.8294

		  	//canvas absen pulang
		  	var inputKoordinatPulang = document.getElementById('koordinat-absenPulang');
		  	inputKoordinatPulang.value=myCurrentLocation;
        	var mapProp2 = {
    			center:myCurrentLocation,
    			zoom:16,
    			mapTypeId:google.maps.MapTypeId.ROADMAP
  			};

  			var myMarker2=new google.maps.Marker({
  				position:myCurrentLocation,
  			});

  			
		  	var map = new google.maps.Map(document.getElementById("map-canvasPulang"),mapProp2);
		  	var infowindow2 = new google.maps.InfoWindow({
  				content:"Anda berada disini"
  			});
  			infowindow2.open(map,myMarker2);

		  	myMarker2.setMap(map);
		  	//end canvas absen pulang
		 	
        });



    }
}
	google.maps.event.addDomListener(window, 'load', initialize);
</script>
<?php 
include_once HomeDirectory().'administrasi/sidebar.php';
include_once HomeDirectory().'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	 <div class="page-header">
	 	<h2>Absensi Staff FAC-Institute</h2>
	 </div>

	 <div class="row">
	 	<div class="col-md-6">
	 		<label>Nama Staff : </label><span style="margin-left:10px"><?= $data->nama_petugas ?></span>
	 	</div>
	 </div>


	 <div class="row">
	 	<div class="col-md-6">

	 		<div class="row" style="margin-top:15px">
	 			<div class="col-md-12">
	 				<h3>Absen Masuk</h3>
	 			</div>
	 		</div>
			
       		
    		<div class="row">
    			<div class="col-md-12">
    				<p>Anda sudah absen pada pukul: <?= $data->absen_masuk ?></p>
    			</div>
    		</div>

    	</div><!--col 6 from absen masuk-->

    	<div class="col-md-6">
	 		<div class="row" style="margin-top:15px">
	 			<div class="col-md-12">
	 				<h3>Absen Pulang</h3>
	 			</div>
	 		</div>

	 		<!--absen pulang-->
	 		<div class="row" style="margin-top:15px">
	 			<div class="col-md-12">
		 			<form action='absensi' method="POST">
		 				<input type="text" id="koordinat-absenPulang" style="display:none" name="in[koordPulang]">
		 				<button class="btn btn-warning" style="width:100%" id="btn-pulang" name="btn-pulang">Absen Pulang</button>
		 			</form>
	 			</div>
	 		</div>

	 		<div class="row" style="margin-top:15px">
	 			<div class="col-md-12">
	 				<div id="map-canvasPulang" style="width:100%;height:400px;border:1px solid">Map is here</div>
	 			</div>
	 		</div>

	 	</div><!--end col-6 -->
	 </div>

	      
</div> <!--end container-->
<script type="text/javascript" src="<?= $data->base_url ?>js/jquery.js"></script>
<script type="text/javascript" src="<?= $data->base_url ?>js/jquery-ui/jquery-ui.js"></script>
<script type="text/javascript">

</script>
</body>
</html>