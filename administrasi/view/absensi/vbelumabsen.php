<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?= $data->subtitle ?></h3>
	</div>
	
  <div class="row">
    <div class="col-md-12">
      <h5>Total baris: <?= count($data->dataabsensi) ?></h5>
    </div>
  </div>

    <div class='row'>
        <div class='col-md-12'>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Tanggal Absen</th>
                        <th>Perusahaan</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                <?php if (count($data->dataabsensi)=='0'): ?>
                    <tr>
                      <td colspan="3">Tidak Ada data bahwa absen anda belum lengkap.</td>
                    </tr>
                <?php else: ?>
                    <?php foreach ($data->dataabsensi as $key): ?>
                      <tr>
                        <td><?= $key->tanggal_absen ?></td>
                        <td><?= $key->nama_usaha ?></td>
                        <td>
                          <a href="<?= $data->base_url.'administrasi/attendance/edit/'.$key->id ?>" title="Edit Absen">Edit Absen</a>
                        </td>
                      </tr>
                    <?php endforeach ?>
                <?php endif ?>
                </tbody>
            </table>
        </div>

    </div>
	    
</div>
</body>
</html> 