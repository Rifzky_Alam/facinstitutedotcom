<!DOCTYPE html>
<html>


<?php include_once 'view/main-component/header.php'; ?>

<body>



<?php 
include_once 'view/main-component/sidebar.php';
include_once 'view/main-component/top-nav.php';
include_once 'functions/DateFunc.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?php echo $data->subtitle ?></h3>
	</div>

    

    <div class="row">
        <div class="col-md-12">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th class="tengah">Tanggal Absen</th>
                        <th class="tengah">Nama Staff</th>
                        <th class="tengah">Perusahaan</th>
                        <th class="tengah">Aksi</th>                        
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($data->listrequest)=='0'): ?>
                        <tr>
                            <td colspan="4" style="text-align:center;">Tidak Ada Data di dalam database kami</td>
                        </tr>
                    <?php else: ?>
                        <?php foreach ($data->listrequest as $key): ?>
                            <tr>
                                <td><?= $key->tanggal_absen ?></td>
                                <td><?= $key->nama ?></td>
                                <td><?= $key->nama_perusahaan ?></td>
                                <td><a href="<?= $data->base_url.'administrasi/table?ac=cgatt&tanggal='.$key->tanggal_absen.'&v='.$key->id ?>" title="Edit">Edit</a></td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                </tbody>
            </table>
        </div>
    </div>

</div><!--end container-->




<script type="text/javascript" src='https://fac-institute.com/js/jquery-ui/jquery-ui.js'></script>
    
</body>
</html>



?>


