<!DOCTYPE html>
<html>
<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>
<body>
<?php 
include_once $data->homedir.'administrasi/sidebar.php';
include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
?>
<div class="container" id="isi">
	 <div class="page-header">
	 	<h2><?= $data->subtitle ?></h2>
	 </div>

	 <div class="row">
	 	<div class="col-md-6">
	 		<label>Nama Staff : </label><span style="margin-left:10px"><?= $data->username ?></span>
	 	</div>
	 </div>


	 <div class="row">
	 	
	      <div class="col-md-12">

	      	<div class="jumbotron">
	      		<h3><?= $data->is_success ?></h3>
	      		<p><?= $data->notif ?></p>
	      	</div>
	 		    
	      </div>
	  </div>
	      <div class="row">
	      	<div class="col-md-12">
	      		<h4>Bila ada masalah, hubungi admin berikut.</h4>
	      	</div>
	      </div>

	      <div class="row">
	      	<div class="col-md-12">
	      		
				<div class="col-lg-4 col-sm-6">

		            <div class="card hovercard">
		                <div class="cardheader">

		                </div>
		                <div class="avatar">
		                    <img alt="" src="https://fac-institute.com/images/ourteam/avatar-rifzky-300x300.png">
		                </div>
		                <div class="info">
		                    <div class="title">
		                        <a target="_blank" href="https://www.facebook.com/zeke.rifzky.alam">Rifzky Alam</a>
		                    </div>
		                    <div class="desc">Admin :: Software Developer</div>
		                    <div class="desc">FAC :: IT Division</div>
		                    <div class="desc">Cool as Always</div>
		                </div>
		                <div class="bottom">
		                    <a href="https://www.facebook.com/zeke.rifzky.alam">
		                        <img style="width:20px;height:20px;" src="https://fac-institute.com/images/socialicon/twitter-black.png">
		                    </a>
		                    <a href="https://twitter.com/Rezt_zek3">
		                        <img style="width:20px;height:20px;" src="https://fac-institute.com/images/socialicon/facebook-black.png">
		                    </a>
		                    <a href="https://www.linkedin.com/in/rifzky-alam-005874a7">
		                        <img style="width:20px;height:20px;" src="https://fac-institute.com/images/socialicon/linkedin-black.png">
		                    </a>
		                </div>
		            </div>

		        </div>

		        <div class="col-lg-4 col-sm-6">

		            <div class="card hovercard">
		                <div class="cardheader" style="background:url('https://fac-institute.com/images/ourteam/coverimagefood.jpeg');">

		                </div>
		                <div class="avatar">
		                    <img alt="" src="https://fac-institute.com/images/ourteam/avatar-imelia.jpeg">
		                </div>
		                <div class="info">
		                    <div class="title">
		                        <a target="_blank" href="https://www.facebook.com/imelia.silviana">Imelia Silviana</a>
		                    </div>
		                    <div class="desc">Admin :: Finance</div>
		                    <div class="desc">FAC :: Finance Division</div>
		                    <div class="desc">Bad as Burning Stove</div>
		                </div>
		                <div class="bottom">
		                    <a href="#">
		                        <img style="width:20px;height:20px;" src="https://fac-institute.com/images/socialicon/twitter-black.png">
		                    </a>
		                    <a href="https://www.facebook.com/imelia.silviana">
		                        <img style="width:20px;height:20px;" src="https://fac-institute.com/images/socialicon/facebook-black.png">
		                    </a>
		                    <a href="#">
		                        <img style="width:20px;height:20px;" src="https://fac-institute.com/images/socialicon/linkedin-black.png">
		                    </a>
		                </div>
		            </div>

		        </div>

		        <div class="col-lg-4 col-sm-6">

		            <div class="card hovercard">
		                <div class="cardheader" style="background:url('https://fac-institute.com/images/ourteam/coverimagedino.jpeg');">

		                </div>
		                <div class="avatar">
		                    <img alt="" src="<?= $data->base_url ?>images/ourteam/dino.jpeg">
		                </div>
		                <div class="info">
		                    <div class="title">
		                        <a target="_blank" href="https://www.facebook.com/profile.php?id=100010652464604">Dino Damara</a>
		                    </div>
		                    <div class="desc">Admin :: IT Staff</div>
		                    <div class="desc">FAC :: IT Division</div>
		                    <div class="desc">Silent but Deadly</div>
		                </div>
		                <div class="bottom">
		                    <a href="#">
		                        <img style="width:20px;height:20px;" src="<?= $data->base_url ?>images/socialicon/twitter-black.png">
		                    </a>
		                    <a href="https://www.facebook.com/imelia.silviana">
		                        <img style="width:20px;height:20px;" src="<?= $data->base_url ?>images/socialicon/facebook-black.png">
		                    </a>
		                    <a href="#">
		                        <img style="width:20px;height:20px;" src="<?= $data->base_url ?>images/socialicon/linkedin-black.png">
		                    </a>
		                </div>
		            </div>

	        	</div>

	      	</div>
	      </div>
	      
</div> <!--end container-->
<script type="text/javascript" src="<?= $data->base_url ?>js/jquery.js"></script>
<script type="text/javascript" src='https://fac-institute.com/js/jquery-ui/jquery-ui.js'></script>
</body>
</html>