<?php

function whoes($data){
    $attendees = explode('#', $data->attendeez);
    $hasil = '';
    foreach ($attendees as $key) {
        $hasil.= '<li>' . $key . '</li>';
    }   
    return $hasil;
}

function agendas($data){
    $hasil = '';
    if ($data->agendas!='-') {
        $agd = explode('#', $data->agendas);
        foreach ($agd as $key) {
            $hasil.= '<li>' . $key . '</li>';
        }
    }
    return $hasil;
}

function listtanggal($data){
    $hasil = '';
    if ($data->tanggals!='-') {
        $tgl = explode('#', $data->tanggals);
        foreach ($tgl as $key) {
            if ($key==$data->tanggal) {
                $hasil.='<li style="color:red;">'.$key.' < </li>';
            } else {
                $hasil.='<li>'.$key.'</li>';
            }
        }
    }
    return $hasil;
}

function BodySample($data){
    return EmailHeader($data).'
                                        <tr>
                                            <td class="m_-5588023840110578919m_3185550004040353724featured-story__copy" style="background:#fff" width="640" align="left">
                                                <table cellspacing="0" cellpadding="0">
                                                    <tbody><tr>
                                                        <td style="font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;font-size:16px;line-height:22px;color:#555555;padding-top:16px" align="left">
                                                        Dear '.$data->nama_cust.', <br><br>
                                                        You have been invited to the following event.<br>
                                                        <h3>'.$data->mytitle.'</h3>
                                                        <table>
                                                            <tbody>
                                                                <tr>
                                                                    <td>When</td>
                                                                    <td>:</td>
                                                                    <td>'.$data->tanggal.'</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Where</td>
                                                                    <td>:</td>
                                                                    <td>'.$data->tempattraining.'</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="vertical-align:middle;">Who</td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <ul style="padding-left: 15px;margin-bottom:0px;">
                                                                            <li>Fac Web System <small>- creator</small></li>
                                                                            '.whoes($data).'
                                                                        </ul>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <br>
                                                        Jenis Accurate: '.$data->jenisaccurate.'<br>
                                                        Jenis Usaha: '.$data->jenisusaha.' <br>
                                                        Agenda Kegiatan:
                                                        <br>
                                                        <ol>
                                                            '.agendas($data).'
                                                        </ol>

                                                        CP: '.$data->namacust.' <br>
                                                        ('.$data->telpcust.') <br>
                                                        Email: '.$data->emailcust.' <br>
                                                        Cust: '.$data->marketing.' <br>
                                                        Marketing: '.$data->petugas.' <br>
                                                        Pengguna: '.$data->tipepengguna.' <br>
                                                        Last Trainer: '.$data->lastrainer.' <br>
                                                        Tanggal Training: 
                                                        <br>
                                                        <ul>
                                                            '.listtanggal($data).'
                                                        </ul>
                                                        <br>
                                                        *Notes:  <br>
                                                        <b>'.str_replace('##', '<br>', $data->desc).'</b>
                                                        <br>
                                                        <br>
                                                        Terimakasih, <br><br>
                                                        <b>FAC System.</b>
                                                        </td>
                                                    </tr>
                                                </tbody></table>
                                            </td>
                                        </tr>
                                    </tbody></table>
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>'.EmailFooter($data);
}