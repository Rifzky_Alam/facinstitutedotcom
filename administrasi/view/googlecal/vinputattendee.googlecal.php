<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
    <div class="page-header" id="top-logo">
        <h3><?= $data->headertext ?></h3>
    </div>

    <div class="row">
      <div class="col-md-10">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th style="text-align:center;">Peserta</th>
              <th style="text-align:center;">Peran</th>
              <th style="text-align:center;">Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php if (count($data->listpeserta)=='0'): ?>
              <tr>
                <td colspan="3" style="text-align:center;">Tidak Ada Peserta Untuk Kalender Ini.</td>
              </tr>
            <?php else: ?>
              <?php foreach ($data->listpeserta as $key): ?>
                <tr>
                  <td><?= $key['nama'] ?></td>
                  <td><?= $key['pk_peran'] ?></td>
                  <td class="tengah">
                    <a href="<?= $data->base_url.'administrasi/googlecal/delattendee?id='.$key['gca_id'] ?>" title="Delete">Hapus</a>
                  </td>
                </tr>
              <?php endforeach ?>
            <?php endif ?>
              
          </tbody>
        </table>
      </div>
    </div>

    <div class="row">
     <form id="my-form" enctype="multipart/form-data" method="post">

             <div class="col-md-10">
                
                <div class="form-group">
                    <label>Peserta</label>
                    <select class="form-control" name="in[peserta]" required>
                      <option value="">--Pilih Peserta--</option>
                      <?php foreach ($data->listuser as $key): ?>
                        <option value="<?= $key['email'] ?>"><?= $key['nama'] ?></option>
                      <?php endforeach ?>
                    </select>
                </div>

                <div class="form-group">
                    <label>Peran</label>
                    <select class="form-control" name="in[peran]" required>
                      <option value="">Pilih Peran</option>
                      <?php foreach ($data->listperan as $key): ?>
                        <option value="<?= $key['pk_id'] ?>"><?= $key['pk_peran'] ?></option>
                      <?php endforeach ?>
                    </select>
                </div>

                <button class="btn btn-lg btn-success">Submit</button>
                <a href="<?= $data->base_url.'administrasi/googlecal/summarylist?tr='.$data->idtrans ?>" title="Back to Summary" class="btn btn-lg btn-info"><< Halaman Utama</a>
             </div>

     </form>   
    </div>

</div><!--end container-->
</body>
</html> 