<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<style>
.btn {
    margin-right :4px;
    margin-bottom:4px;
}
</style>
<body>

  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
          </div>

          <div class="modal-body">
          <p>Do you want to proceed?</p>
          <p class="debug-url"></p>
          </div>

          <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <a id="b2" class="btn btn-danger btn-ok">Delete</a>
          </div>
      </div>
    </div>
  </div>

<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?= $data->headertext ?></h3>
	</div>
	<div class="row" style="overflow:auto;">



        <div class="col-md-12">
                <a href="<?= $data->base_url.'administrasi/data-transaksi' ?>" class="btn btn-info">
                    Tabel Transaksi
                </a>
                <br><br>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th>Perusahaan</th>
                        <th>Trainer</th>
                        <th>Verif By</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($data->listdata)=='0'): ?>
                        <tr>
                            <td colspan="5" style="text-align:center;">Belum ada data tersedia</td>
                        </tr>
                    <?php else: ?>
                        <?php foreach ($data->listdata as $key): ?>
                            <tr>
                              <td><?= $key['tanggal'] ?> </td>
                              <td><?= $key['nama_usaha'] ?></td>
                              <td><?= $key['trainer'] ?></td>
                              <td><?= $key['marketing'] ?></td>
                              <td>
                                <a href="#" title="">-</a>
                              </td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>

                </tbody>
            </table>
        </div>


    </div>

</div><!--end container-->

</body>
</html>
