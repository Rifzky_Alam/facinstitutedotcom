<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<style>
.btn {
    margin-right :4px;
    margin-bottom:4px;
}
</style>
<body>

  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
          </div>

          <div class="modal-body">
          <p>Do you want to proceed?</p>
          <p class="debug-url"></p>
          </div>

          <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <a id="b2" class="btn btn-danger btn-ok">Delete</a>
          </div>
      </div>
    </div>
  </div>

<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?= $data->headertext ?></h3>
	</div>
	<div class="row" style="overflow:auto;">



        <div class="col-md-12">
                <a href="<?= $data->base_url.'administrasi/googlecal/sendtoapi?tr='.$data->idtrans ?>" class="btn btn-primary">
                    Buat Baru
                </a>
                <a href="<?= $data->base_url.'administrasi/new-perusahaan-info?tr='.$data->idtrans ?>" class="btn btn-info">
                    Info Transaksi
                </a>
                <br><br>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th>Summary</th>
                        <th>Desc</th>
                        <th>Verif By</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($data->g_cal_list)=='0'): ?>
                        <tr>
                            <td colspan="4" style="text-align:center;">Belum ada data tersedia</td>
                        </tr>
                    <?php else: ?>
                        <?php foreach ($data->g_cal_list as $key): ?>
                            <?php if ($key['cal_status']=='63'): ?>
                                <tr class="info">
                            <?php else: ?>
                                <tr>
                            <?php endif ?>
                                <td>
                                    <input type="hidden">
                                    <input type="hidden" id="gocalhash" value="<?= $key['gcv_hash'] ?>">
                                    <?= $key['tanggal'] ?>
                                </td>
                                <td><?= $key['g_summary'] ?></td>
                                <td><?= $key['g_description'] ?></td>
                                <td><?= $key['petugas_verifikasi'] ?></td>
                                <td>
                                    <a class='btn btn-danger btn-xs' href="#" <?= 'id="r-'.$key['g_row'].'"' ?> value="<?= $key['g_row'] ?>" title="Delete"  onclick="del_('<?= $key['g_row'] ?>','<?= $key['gcv_hash'] ?>')" >Hapus</a>
                                    <?php if ($data->roleuser=='admin'&&$data->teamuser!='3'): ?>
                                    <a class='btn btn-success btn-xs' href="<?= $data->base_url.'administrasi/googlecal/updateadmin?idcal='.$key['g_id_cal'] ?>" title="Verifikasi">Verifikasi</a>
                                    <a class='btn btn-info btn-xs' href="<?= $data->base_url.'administrasi/googlecal/midware?id='.$key['g_id_cal'] ?>" title="Kirim ke Google Cal">Send to Google Cal </a>
                                    <?php else: ?>
                                    <a class='btn btn-success btn-xs' href="<?= $data->base_url.'administrasi/googlecal/verify?id='.$key['g_row'] ?>" title="Verifikasi">Verifikasi</a>

                                    <?php endif ?>
                                    <a class='btn btn-default btn-xs' id="authorize_button" style="display:none;" href="#" title="authorize" onclick="" >Authorize</a>
                                    <a class='btn btn-default btn-xs' id="signout_button" style="display: none;" href="#" title="signout" onclick="" >Sign Out</a>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>

                </tbody>
            </table>
        </div>


    </div>

</div><!--end container-->

<!-- <button id="authorize_button" style="display: none;">Authorize</button>
<button id="signout_button" style="display: none;">Sign Out</button> -->

<div id="content"></div>

<script type="text/javascript">
  // Client ID and API key from the Developer Console
  var CLIENT_ID = '701011072123-nrnu6pufmlefed0d5ba0pqsl6asfqptj.apps.googleusercontent.com';
  var API_KEY = 'AIzaSyBS1__bIttRN3ZYxK7DJ3l-R6nVVMxf8Mo';

  // Array of API discovery doc URLs for APIs used by the quickstart
  var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"];

  // Authorization scopes required by the API; multiple scopes can be
  // included, separated by spaces.
  var SCOPES = "https://www.googleapis.com/auth/calendar";

  var authorizeButton = document.getElementById('authorize_button');
  var signoutButton = document.getElementById('signout_button');

  /**
   *  On load, called to load the auth2 library and API client library.
   */
  function handleClientLoad() {
    gapi.load('client:auth2', initClient);
  }

  /**
   *  Initializes the API client library and sets up sign-in state
   *  listeners.
   */
  function initClient() {
    gapi.client.init({
      apiKey: API_KEY,
      clientId: CLIENT_ID,
      discoveryDocs: DISCOVERY_DOCS,
      scope: SCOPES
    }).then(function () {
      // Listen for sign-in state changes.
      gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

      // Handle the initial sign-in state.
      updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
      authorizeButton.onclick = handleAuthClick;
      signoutButton.onclick = handleSignoutClick;
    });
  }

  /**
   *  Called when the signed in status changes, to update the UI
   *  appropriately. After a sign-in, the API is called.
   */
  function updateSigninStatus(isSignedIn) {
    if (isSignedIn) {
      $("#authorize_button").hide();
      $("#signout_button").show();
    } else {
      $("#signout_button").hide();
      $("#authorize_button").show();
    }
  }

  /**
   *  Sign in the user upon button click.
   */
  function handleAuthClick(event) {
    gapi.auth2.getAuthInstance().signIn();
  }

  /**
   *  Sign out the user upon button click.
   */
  function handleSignoutClick(event) {
    gapi.auth2.getAuthInstance().signOut();
  }

  /**
   * Append a pre element to the body containing the given message
   * as its text node. Used to display the results of the API call.
   *
   * @param {string} message Text to be placed in pre element.
   */
  function appendPre(message) {
    var pre = document.getElementById('content');
    var textContent = document.createTextNode(message + '\n');
    pre.appendChild(textContent);
  }



</script>




<script async defer src="https://apis.google.com/js/api.js"
  onload="this.onload=function(){};handleClientLoad()"
  onreadystatechange="if (this.readyState === 'complete') this.onload()">
</script>

<script>

function del_(val_,valx) {
  alert("WOY")
  console.log(val_);
  console.log(valx);
var str = val_ ;
var res = str.replace("r-", "");
$('#myModal').modal('show');
$("#b2").click(function(){


  gapi.client.request({
    'path': '/calendar/v3/calendars/mteeem90orpfs9qoot75lgu6js%40group.calendar.google.com/events/'+ valx +'?sendNotifications=true&sendUpdates=all',
    'method': 'DELETE'
  }).then(function(resp) {
    console.log(resp);

    $.ajax({
        type: "POST",
        url: "https://fac-institute.com/administrasi/googlecal/deleteparent",
        data: { 'id' :  res } ,
        cache: false,
        success: function(result) {
          console.log(result);
          location.reload();
        }
    });


  });



});
}

</script>

</body>
</html>
