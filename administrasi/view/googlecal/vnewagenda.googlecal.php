<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?= $data->headertext ?></h3>
	</div>
	<div class="row">
     <form id="my-form" enctype="multipart/form-data" method="post">

         <div class="row">
             <div class="col-md-10">
                <div class="form-group">
                    <label>Tanggal</label>
                    <select name="in[tgl]" class="form-control" required>
                        <option value="">-- pilih tanggal tersedia --</option>
                        <?php foreach ($data->tanggaltransaksi as $key): ?>
                            <option value="<?= $key['id'] ?>"><?= $key['tanggal'] ?></option>
                        <?php endforeach ?>
                    </select>
                </div>

                <div class="form-group">
                    <label>Title</label>
                    <textarea name="in[summary]" class="form-control"><?= $data->nama_usaha ?></textarea>
                </div>

                <div class="form-group">
                    <label>Agenda</label>
                    <textarea name="in[agenda]" class="form-control" placeholder="Pakai ## untuk mengganti enter"></textarea>
                </div>

                <div class="form-group">
                    <label>Note/Patokan</label>
                    <textarea name="in[desc]" class="form-control" placeholder="Pakai ## untuk mengganti enter"></textarea>
                </div>

                <div class="form-group">
                    <label>Start / Waktu Agenda Dimulai</label>
                    <p><?= $data->waktumulai ?> 
                    <a href="<?= $data->base_url ?>administrasi/new-transaksiz?edt=<?= $data->idtrans ?>" title="Edit">Edit</a></p>
                </div>

                <div class="form-group">
                    <label>End / Waktu Agenda Selesai</label>
                    <p><?= $data->waktuselesai ?> 
                    <a href="<?= $data->base_url ?>administrasi/new-transaksiz?edt=<?= $data->idtrans ?>" title="Edit">Edit</a></p>
                </div>

                <button class="btn btn-lg btn-success">Submit</button>

             </div>
         </div>         
         <hr>

     </form>   
    </div>

</div><!--end container-->
</body>
</html> 