<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
    <div class="page-header" id="top-logo">
        <h3><?= $data->headertext ?></h3>
    </div>
    <div class="row">
     <form id="my-form" enctype="multipart/form-data" method="post">

         <div class="row">
             <div class="col-md-6">
                <div class="form-group">
                    <label>Tanggal</label>
                    <input type="hidden" name="in[row]" value="<?= $data->row ?>">
                    <select name="in[tgl]" class="form-control" required>
                        <option value="">-- pilih tanggal tersedia --</option>
                        <?php foreach ($data->tanggaltransaksi as $key): ?>
                            <?php if ($key['id']==$data->currentdate): ?>
                              <option value="<?= $key['id'] ?>" selected="selected"><?= $key['tanggal'] ?></option>
                            <?php else: ?>
                              <option value="<?= $key['id'] ?>"><?= $key['tanggal'] ?></option>
                            <?php endif ?>
                        <?php endforeach ?>
                    </select>
                </div>

                <div class="form-group">
                    <label>Title</label>
                    <textarea name="in[summary]" class="form-control"><?= $data->summary ?></textarea>
                </div>

                <div class="form-group">
                    <label>Agenda Kalender</label>
                    <textarea name="in[agenda]" class="form-control"><?= $data->agendacal ?></textarea>
                </div>

                <div class="form-group">
                    <label>Keterangan Lain</label>
                    <textarea name="in[desc]" class="form-control"><?= str_replace('<br/>', '##', $data->description) ?></textarea>
                </div>

                <div class="form-group">
                    <label>Start / Waktu Agenda Dimulai</label>
                    <p><?= $data->waktumulai ?> 
                    <a href="<?= $data->base_url ?>administrasi/new-transaksiz?edt=<?= $data->idtrans ?>" title="Edit">Edit</a></p>
                </div>

                <div class="form-group">
                    <label>End / Waktu Agenda Selesai</label>
                    <p><?= $data->waktuselesai ?> 
                    <a href="<?= $data->base_url ?>administrasi/new-transaksiz?edt=<?= $data->idtrans ?>" title="Edit">Edit</a></p>
                </div>

                <div class="form-group">
                    <?php if ($data->statusgcal=='2'): ?>
                      <label>
                        <a href="<?= $data->base_url.'administrasi/googlecal/newattendee?id='.$data->idgcal ?>" title="New Attendee">
                          Attendees / Peserta    
                        </a>
                      </label>
                    <?php else: ?>
                      <label>Attendees / Peserta</label>
                    <?php endif ?>
                    
                    
                </div>

                <button class="btn btn-lg btn-success">Submit</button>
                
             </div>
             <div class="col-md-6">
              <div style="height:1200px;" class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="<?= $data->base_url.'administrasi/new-detail?tr='.$data->transid ?>" allowfullscreen></iframe>
              </div>
             </div> 
         </div>         
         <hr>

     </form>   
    </div>

</div><!--end container-->
</body>
</html> 