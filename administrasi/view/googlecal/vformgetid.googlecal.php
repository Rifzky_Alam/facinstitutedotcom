<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>


<body>

<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?= $data->headertext ?></h3>
	</div>
	<div class="row">
     <form id="my-form" enctype="multipart/form-data" method="post">

         <div class="row">
             <div class="col-md-10">

                <div class="form-group">
                    <label>Google Calendar Token</label>
                    <input type="hidden" id="mware" value=<?= "'". $data->formidware ."'" ?>>
                    <textarea id="dead" name="in[summary]" class="form-control"></textarea>
                    <button type="button" class="btn btn-primary btn-sm btn-block" href="#" id="buttonid" onclick="makeRequest0()" title="Get Google Calendar ID">GetId</button>
                </div>

                <!-- <button class="btn btn-lg btn-success">Submit</button> -->

             </div>
         </div>
         <hr>
     </form>
    </div>

</div><!--end container-->

<script src="https://cdn.jsdelivr.net/npm/lodash@4.17.11/lodash.min.js"></script>


<script>

    $(document).ready(function(){
      $("#buttonid").on("click", function() {
          $(this).prop("disabled", true);
      });
    });

    //buat fungsi google cal klo berhasil ke updatestatus

    var CLIENT_ID = '701011072123-nrnu6pufmlefed0d5ba0pqsl6asfqptj.apps.googleusercontent.com';
    var API_KEY = 'AIzaSyBS1__bIttRN3ZYxK7DJ3l-R6nVVMxf8Mo';
    var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"];
    var SCOPES = "https://www.googleapis.com/auth/calendar";

    function handleClientLoad() {
      gapi.load('client:auth2', initClient);
    }

    function initClient() {
      gapi.client.init({
        apiKey: API_KEY,
        clientId: CLIENT_ID,
        discoveryDocs: DISCOVERY_DOCS,
        scope: SCOPES
      }).then(function () {
      });
    }

    var colorId = "7";


      function makeRequest0() {

        var myArrayX = [];
        var xyz = JSON.parse($('#mware').val()).summary;

        var ok = "https://www.googleapis.com/calendar/v3/calendars/mteeem90orpfs9qoot75lgu6js%40group.calendar.google.com/events?timeMax="
        + JSON.parse($('#mware').val()).tanggal+ "T23%3A00%3A00%2B07%3A00"
        + "&timeMin="+ JSON.parse($('#mware').val()).tanggal +"T00%3A00%3A00%2B07%3A00"
        gapi.client.request({
          'path': ok,
          'method': 'GET'
        }).then(function(resp) {

          // console.log((JSON.parse(resp.body)).items[0].summary);
          // console.log(resp.body)

          var sum41 = (JSON.parse(resp.body)).items ;
          for (i = 0; i < sum41.length; i++) {
            console.log(sum41[i].summary);
            myArrayX.push(sum41[i].summary);
          }

          console.log(myArrayX);
          console.log();

          if ( _.some(myArrayX, _.unary(_.partialRight(_.includes, xyz))) == true ) {
          alert("udah ada")
          }
          else {
            makeRequest();
          }

        });
      }


    function makeRequest() {

      if ( JSON.parse($('#mware').val()).userlogin == "Sarah") {
        colorId = "5"
      }
      else {
        colorId = "7"
      }

      var resources = {
        "end": {
          "dateTime": JSON.parse($('#mware').val()).tanggal + "T"+ JSON.parse($('#mware').val()).waktuselesai + ":00+07:00",
          "timeZone": "Asia/Jakarta"
        },
        "start": {
          "dateTime": JSON.parse($('#mware').val()).tanggal + "T"+ JSON.parse($('#mware').val()).waktumulai + ":00+07:00",
          "timeZone": "Asia/Jakarta"
        },
        "summary": JSON.parse($('#mware').val()).summary,
        "colorId": colorId,
        "description": JSON.parse($('#mware').val()).summarydesc
      };

      console.log(resources);

      gapi.client.request({
        'path': 'https://www.googleapis.com/calendar/v3/calendars/mteeem90orpfs9qoot75lgu6js%40group.calendar.google.com/events?sendNotifications=true',
        'method': 'POST',
        'body': resources
      }).then(function(resp) {
        console.log(resp);
        console.log(resp.result.id);
        $('#dead').val(resp.result.id);

        document.getElementById("my-form").submit();

      });
    }

</script>

<script async defer src="https://apis.google.com/js/api.js"
  onload="this.onload=function(){};handleClientLoad()"
  onreadystatechange="if (this.readyState === 'complete') this.onload()">
</script>

</body>
</html>
