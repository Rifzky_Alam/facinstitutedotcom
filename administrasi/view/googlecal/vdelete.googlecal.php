<!DOCTYPE html>
<html>


<?php include_once $data->homedir.'administrasi/view/main-component/header.php'; ?>

<body>



<?php
  include_once $data->homedir.'administrasi/sidebar.php';
  include_once $data->homedir.'administrasi/view/main-component/top-nav.php';
 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3><?= $data->headertext ?></h3>
	</div>
	<div class="row">
        <div class="col-md-10">
            <section>
                <h3>Dengan menekan tombol 'Ya' Maka anda telah setuju menghapus data</h3>
                <form action="" method="post" accept-charset="utf-8">
                    <input type="hidden" name="idg" value="<?= $data->idgooglecal ?>">
                    <button class="btn btn-lg btn-danger">Ya</button>
                    <a href="<?= $data->base_url ?>administrasi/googlecal/summarylist?tr=<?= $data->idtrans ?>" title="Cancel">Tidak</a>                                        
                </form>
            </section>
        </div>
    </div>

</div><!--end container-->
</body>
</html> 