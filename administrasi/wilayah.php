<?php 
// include_once '/Applications/XAMPP/xamppfiles/htdocs/facinstitute/administrasi/controller/tutorial.controller.php';
include_once '/home/facinsti/public_html/administrasi/controller/perusahaan.controller.php';

session_start();

if (isset($_SERVER['PATH_INFO'])) {
	@$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
	$key = array_shift($url_segment);
	switch ($key) {
		case 'tes':
			if (isset($_POST['in'])) {
				Usaha::NewPerusahaanPost($_POST['in']);
			} else {
				Usaha::NewPerusahaan();	
			}			
			break;
		case 'provinsi':
			Usaha::CariProvinsi();
			break;
		case 'kota':
			if (isset($url_segment[0])&&!empty($url_segment[0])) {
				Usaha::ListKota($url_segment[0]);
			}else{
				header('location:https://fac-institute.com/administrasi/');	
			}
			break;
		case 'subdist':
			if (isset($url_segment[0])&&!empty($url_segment[0])) {
				Usaha::CariSubdistrict($url_segment[0]);
			}else{
				header('location:https://fac-institute.com/administrasi/');	
			}
			break;
		case '':
			header('location:https://fac-institute.com/administrasi/');
			break;
	}
}else{
	header('location:https://fac-institute.com/administrasi/');
}