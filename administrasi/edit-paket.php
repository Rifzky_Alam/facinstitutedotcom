
<?php 
session_start();
if (!isset($_SESSION['admin'])) {
    header('Location:../login/index.php');
}
?>




<!DOCTYPE html>
<html>

<?php 
//echo $tipeAdmin=$_SESSION['admin']['tipe'];
$page = 'home';
include_once 'header.php'; 
include_once '../model/Pendaftar.php';
$pendaftar = new Pendaftar();

?>

<?php $dataPaket = json_decode($pendaftar->fetchPaketTraining()); ?>

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<body>



<?php 
include_once 'sidebar.php';
include_once 'top-nav.php';
?>


<div class="container">
  <h2>Table</h2>
  <p>The .table-responsive class creates a responsive table which will scroll horizontally on small devices (under 768px). When viewing on anything larger than 768px wide, there is no difference:</p>                                                                                      
  <div class="table-responsive">          
  <table class="table">
    <thead>
      <tr>
        <th>#</th>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Age</th>
        <th>City</th>
        <th>Country</th>
      </tr>
    </thead>
    <tbody>


    <?php  
                    for ($i=0; $i < count($dataPaket); $i++) { 
                        if ($dataPaket[$i]->id==$datas->id_paket) {
                            echo "<option selected value='".$dataPaket[$i]->id."'>".$dataPaket[$i]->nama_item." -- ".$dataPaket[$i]->price_for." -- ".number_format($dataPaket[$i]->harga)."</value>";
                        }else{
                            echo "<option value='".$dataPaket[$i]->id."'>".$dataPaket[$i]->nama_item." -- ".$dataPaket[$i]->price_for." -- ".number_format($dataPaket[$i]->harga)."</value>";    
                        }
                    }
                ?>



      <tr>
        <td>1</td>
        <td>Anna</td>
        <td>Pitt</td>
        <td>35</td>
        <td>New Yorak</td>
        <td>
            <input id="toggle-event"  type="checkbox" checked data-toggle="toggle" data-on="Ready" data-off="Not Ready" data-onstyle="success" data-offstyle="danger">
<div id="console-event"></div>
<script>
  $(function() {
    $('#toggle-event').change(function() {
      $('#console-event').html('Toggle: ' + $(this).prop('checked'))
    })
  })
</script>
        </td>
      </tr>
    </tbody>
  </table>
  </div>
</div>
</body>
</html> 