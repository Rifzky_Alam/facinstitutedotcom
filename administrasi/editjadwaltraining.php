<?php 
session_start();
if (!isset($_SESSION['admin'])) {
    header('Location:../login/index.php');
}
?>




<!DOCTYPE html>
<html>

<?php 
//echo $tipeAdmin=$_SESSION['admin']['tipe'];
$judul="Edit Jadwal Training";
$page = 'penawaran';
include_once 'header.php'; 

?>

<body>



<?php 

include_once 'sidebar.php';

include_once 'top-nav.php';

include_once '../model/Pendaftar.php';
$pendaftar = new Pendaftar();

 ?>


    
<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3>Ubah Jadwal</h3>
	</div>
<?php if (isset($_GET['kd'])&&!empty($_GET['kd'])): ?>
        <?php 
        
        $dataJadwal = $pendaftar->fetchDataAgendaByID($_GET['kd']);
        if (isset($_POST['edit'])) {
                
                if (!isset($_POST['edit']['attachments'])||$_POST['edit']['attachments']==''||empty($_POST['edit']['attachments'])) {
                    $att='';
                } else {
                    $att= implode(" # ", $_POST['edit']['attachments']);
                }
                


                $inputData = array(
                    'id' => $_GET['kd'],
                    'tanggal'=> implode(" # ", $_POST['edit']['tanggalPelaksanaan']),
                    'waktu'=> $_POST['edit']['waktu'],
                    'tempat'=> $_POST['edit']['tempat'],
                    'alamat'=> $_POST['edit']['alamat'],
                    'cc'=> $_POST['edit']['email'],
                    'attachments'=> $att
                );
                $inputData = (object) $inputData;



                $objek = array(
                    'id' => $_GET['kd'],
                    'tanggal' => implode(" # ", $_POST['edit']['tanggalPelaksanaan']),
                    'tempat' => $_POST['edit']['tempat'],
                    'alamat' => $_POST['edit']['alamat'],
                    'email' => $_POST['edit']['email'],
                    'waktu' => $_POST['edit']['waktu'],
                    'attachments' => $att
                );

                $objek = (object) $objek;

                if ($pendaftar->editTempJadwal($objek)) {
                    echo "<script>alert('Data berhasil disimpan!');</script>";
                    echo "<script>location.replace('".basename(__FILE__, '.php')."?s=2&kj=".$objek->id."');</script>";

                }else{
                    echo "<script>alert('Data gagal disimpan!');</script>";
                }



                /*
                echo "<div class='row'>";
                echo "ID: ".$inputData->id."<br>";
                // echo "ID Pendaftar: ".$inputData->idpendaftar."<br>";
                echo "Tanggal: ".$inputData->tanggal."<br>";
                echo "Waktu: ".$inputData->waktu."<br>";
                echo "Tempat: ".$inputData->tempat."<br>";
                echo "Alamat: ".$inputData->alamat."<br>";
                echo "CC: ".$inputData->cc."<br>";
                echo "Attachments: ".$inputData->attachments."<br>";
                echo "</div>";*/
                
            }




        ?>
	    <form id='my-form' action='' method='post'>

        <div class="row">
            <div class="col-md-10">
                <h4>Tanggal Pelaksanaan</h4>
            </div>
        </div>
        
        <?php $tanggal = explode(' # ', $dataJadwal->tanggal) ?>
        <?php for ($i=0; $i < count($tanggal); $i++) { ?>
        <div class="row" <?php echo "id='tanggal-$i'" ?>>
            <div class="col-md-10">
                    <div class="form-inline" >
                    <input type="text" id="tgl-pelaksanaan" name='edit[tanggalPelaksanaan][]' class="form-control tglPelaksanaan" placeholder="klik disini untuk memilih tanggal" value=<?php echo "'".$tanggal[$i]."'"; ?>/>   
                    <span class='btn btn-danger glyphicon-minus' <?php echo "onclick='removeElement(".'"tanggal-'.$i.'"'.")'"; ?>></span>
                    </div>
            </div>
        </div>
        <?php } ?>

        <div class="row" id='jajal'>
            <div class="col-md-10">
                    <div class="form-inline" >
                        <input type="text" id="tgl-pelaksanaan" name='edit[tanggalPelaksanaan][]' class="form-control tglPelaksanaan" placeholder="klik disini untuk memilih tanggal"/>   
                            <span class='btn btn-sm btn-primary glyphicon glyphicon-plus' id='tambahTombol'></span>
                    </div>
            </div>
        </div>
        <br>

        <div class='row'>
            <div class='form-group col-sm-8'>
                <label>Tempat</label>
                <input type='text' class='form-control' id='txt-passLama' name='edit[tempat]' value="<?php echo $dataJadwal->tempat ?>">
            </div>
        </div>

        <div class='row'>
            <div class='form-group col-sm-8'>
                <label>Alamat</label>
                <textarea class="form-control" name="edit[alamat]"><?php echo $dataJadwal->alamat ?></textarea>
            </div>
        </div>

        <div class='row'>
            <div class='form-group col-sm-8'>
                <label>Waktu</label>
                <input type='text' class='form-control' id='txt-passLama' name='edit[waktu]' value="<?php echo $dataJadwal->waktu ?>">
            </div>
        </div>

        <div class='row'>
            <div class='form-group col-sm-8'>
                <label>Email CC</label>
                <input type='text' class='form-control' id='txt-passLama' name='edit[email]' value="<?php echo $dataJadwal->email ?>" placeholder="CC Email">
            </div>
        </div>



        <?php 
                $lampiranzz=explode(" # ", $dataJadwal->attachment);
                $files = scandir('attachments/');
                for ($i=0; $i < count($files); $i++) { 
                    if ($files[$i]!='.'&&$files[$i]!='..') {
                        if (in_array($files[$i], $lampiranzz)) {
                            echo "<div class='checkbox'><label><input type='checkbox' checked name='edit[attachments][]' value='".$files[$i]."'>".
                            $files[$i].
                            "</label></div>";                         
                        }else{
                            echo "<div class='checkbox'><label><input type='checkbox' name='edit[attachments][]' value='".$files[$i]."'>".
                            $files[$i].
                            "</label></div>";
                        }
                    }
                }
        ?>




    <div class='row'>
        <div class='form-inline col-sm-8'>
            <button class='btn btn-lg btn-primary' style='width:100%'>Next</button>
        </div>
    </div>

    </form>

<?php elseif (isset($_GET['s'])&&$_GET['s']=='2'&&isset($_GET['kj'])&&!empty($_GET['kj'])): ?>
<?php $dataz = $pendaftar->fetchDataAgendaByID($_GET['kj']); ?>
     <div class="row" id="tes">
        <div class="form-group col-md-10">
            <label><a href=<?php echo "'schedules?step=3&kd=".$_GET['kj']."'"; ?> target="_blank" >Nama Agenda</a></label>
            <input type="text" name="agenda" class="form-control" id="contoh" />
        </div>
     </div>

     <div class="row">
     <?php 
     
     if (isset($_POST['in'])) {
        $inz = array(
            'agenda' => json_encode($_POST['in']['agenda']),
            'id' =>  $_GET['kj']
        );
        $inz = (object) $inz;
        if ($pendaftar->editAgenda($inz)) {
            echo "<script>alert('Data berhasil disimpan!');</script>";
            echo "<script>location.replace('daftar-agenda?p=".$_POST['in']['idp']."');</script>";
        }else{
            echo "<script>alert('Data gagal disimpan!');</script>";
        }
           
     }


     ?>
     </div>


     <form action="" method="post">
     <div class="row" id="siap">
        <input type="text" name="in[idp]" value=<?php echo "'".$dataz->id_pendaftar."'"; ?> style="display:none;">
        <ul id="ok">
            <?php 
              $myAgenda = json_decode($dataz->agenda);
              $allAgenda = json_decode($pendaftar->fetchAllAgenda());
              for ($i=0; $i < count($allAgenda) ; $i++) { 
                  for ($j=0; $j < count($myAgenda); $j++) { 
                      if ($myAgenda[$j]==$allAgenda[$i]->id) {
                          echo "<li class='ilang' id='li$i'>".$allAgenda[$i]->nama_agenda."<input type='text' name='in[agenda][]' value='".$allAgenda[$i]->id."' style='display:none;'><a id='i$i' onclick='removeList(this)' href='#'><span class='glyphicon glyphicon-remove' style='padding-left:10px;'></span></a></li>";
                      }
                  }
              }
              // print_r($myAgenda);
            ?>
        </ul>
     </div>
     <hr>
    

     <div class="row">
        <div class="form-group col-md-10" id='bisalah'>
            
        </div>
     </div>


     <div class="row">
        <div class="col-md-10">
            <button class="btn btn-primary">Submit</button>
        </div>
     </div>
     </form>

<?php endif ?>    
</div>

    <script type="text/javascript" src=<?php echo "'".getBaseUrl()."css/multiselect/js/bootstrap-multiselect.js'"; ?>></script>
    <script type="text/javascript" src=<?php echo "'".getBaseUrl()."css/datepicker/js/bootstrap-datepicker.js'"; ?>></script>
    
    <?php if (isset($_GET['s'])&&$_GET['s']=='2'&&isset($_GET['kj'])&&!empty($_GET['kj'])): ?>
        <script type="text/javascript">
        $(document).ready(function(){
            //var availableTags = ['Rifzky Alam','Avril Lavigne'];
            //$('#contoh').autocomplete({
                //source: availableTags
            //});
        });

        $('#contoh').keyup(function(){
            if ($('#contoh').val()==''){
                $('.mydata').remove();
            }else{
            $.ajax({
              type: "GET",
              url: "reverse",
              data: {
                'kda':$('#contoh').val()
                },
              cache: false,
              success: function(data){
                $('.mydata').remove();
                 $('#bisalah').append(data);
                 //alert(data);
              }
            }); //end ajax  
            }
            
        });

        function coba(haha) {//haha.value
            $('#ok').append("<li class='ilang' id='li" + haha.id + "'>" + $('#y' + haha.id).text() + " <input type='text' name='in[agenda][]' value='" + haha.value + "' style='display:none;'> <a id='i" + haha.id + "' onclick='removeList(this)' href='#'><span class='glyphicon glyphicon-remove' style='padding-left:10px;'></span></a></li>");
            $('#z' + haha.id).remove();
        }

        function removeList(nama) {
            $('#l' + nama.id).remove();
            //alert('oke');
        }

    </script>
        <?php else: ?>
    <script type="text/javascript">

    $(document).ready(function(){
        $('.tglPelaksanaan').datepicker({
            format: 'yyyy-mm-dd'
        });
    });


    function removeElement(id) {
        $('#'+id).remove();
    }

        $('#tambahTombol').click(function(){
    
            var cobayah = $('.tglPelaksanaan').length;
            var iseng = "tambahan" + cobayah;
            if ($('#tambahan').length>1){

        if (cobayah==2) {
            $('#tambahan').after("<div class='row' id='"+iseng+"'><div class='col-md-10'><div class='form-inline'><input name='edit[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"'+iseng+'"'+")'></span></div></div></div>");
        }else{
            var okeh = cobayah-1;
            var iseng = "tambahan" + okeh;
            $("#"+iseng).after("<div class='row' id='tambahan"+cobayah+"'><div class='col-md-10'><div class='form-inline'><input name='edit[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"tambahan'+cobayah+'"'+")'></span></div></div></div>");
        };
            //nilai +=1;
         }else{
            $('#jajal').after("<div class='row' id='tambahan'><div class='col-md-10'><div class='form-inline'><input name='edit[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"'+"tambahan"+'"'+")'></span></div></div></div>");
        };

        $('.tglPelaksanaan').datepicker({
            format: 'yyyy-mm-dd'
        });

        $('.tglPelaksanaanz').datepicker({
            format: 'yyyy-mm-dd'
        });
    });
    </script>
    <?php endif ?>
    
</body>
</html> 