<?php 
// include_once '/Applications/XAMPP/xamppfiles/htdocs/facinstitute/administrasi/controller/tutorial.controller.php';
session_start();
if (isset($_SERVER['PATH_INFO'])) {
	@$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
	$key = array_shift($url_segment);
	switch ($key) {
		case 'invoice':
			include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/controller/transaksi.controller.php';
			Transaksi::TabelMarketing();
		break;
		case 'data-followup':
			include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/controller/user.controller.php';
			User::DataFollowUpMarketing();
			break;
		case 'data-targetharian':
			include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/controller/settings.controller.php';
			if (isset($_GET['del'])) {
				Settings::SoftDeleteTargetMarketing($_GET['del']);	
			} else {
				Settings::TabelTargetMarketing();	
			}
			break;
		case 'data-pendaftar':
			include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/controller/transaksi.controller.php';
				Transaksi::DataPendaftar();
			break;
		case 'pendaftar':
			include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/controller/transaksi.controller.php';
			if (isset($_GET['mar'])&&!empty($_GET['mar'])) {
				Transaksi::MarkAsRead($_GET['mar']);
			}
			break;
		case 'config-targetharian':
			include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/controller/settings.controller.php';
			Settings::ConfigTargetMarketing();
			break;
		case 'followup-edit':
		    include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/controller/user.controller.php';
		    if(isset($_GET['id'])&&!empty($_GET['id'])){
		        User::EditFolUp($_GET['id']);    
		    }else{
		        header('location: location: https://fac-institute.com/administrasi/marketing/data-followup');
		    }
		    break;
		case 'new-followup':
			include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/controller/user.controller.php';
			User::NewFollowupMarketing();
			break;
		default:
		header('location: https://fac-institute.com/administrasi/');
		break;
	}
}else{
	header('location: https://fac-institute.com/administrasi/');
}