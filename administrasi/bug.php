<?php 
session_start();
if (!isset($_SESSION['admin']['username'])) {
	header('location: https://fac-institute.com/administrasi/accessdenied');
}
include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/controller/system.controller.php';
if (isset($_SERVER['PATH_INFO'])) {
	$_SERVER['PATH_INFO'] = str_replace('.php', '', $_SERVER['PATH_INFO']);
	@$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
	$key = array_shift($url_segment);
	switch ($key) {
		//list config
		case 'new-bug':
			if (isset($_POST['in'])&&!empty($_POST['in'])) {
				FacSystem::SubmitNewBugPost($_POST['in']);
			}else{
				FacSystem::NewBugForm();
			}
			break;
		case 'listdata':
			FacSystem::ListOfError();
			break;
		case 'editbug':
			if (isset($url_segment[0])) {
				if (isset($_POST['in'])) {
					FacSystem::EditBugPost($url_segment[0],$_POST['in']);	
				}else {
					FacSystem::EditBug($url_segment[0]);	
				}
			}
			break;
		case '':
			header('Location: https://fac-institute.com/administrasi/settings/admin');
			break;
	}
}