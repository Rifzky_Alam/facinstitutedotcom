<?php 
session_start();
if (!isset($_SESSION['admin'])){
    //echo $_SESSION['admin']['tipe'];
    header('Location:index.php');
}

// include_once '../model/page.php';
// include_once '../model/Petugas.php';
// $ctrl_page = new Page();
// $ctrlPetugas = new ControlPetugas();
// $usernames = $ctrlPetugas->fetchUsernameAndName();
 

?>
<!DOCTYPE html>
<html>


<?php 
$judul="Data Profile || FAC Institute";

$page='upload';
include_once 'header.php'; 

?>

<body>
<?php 
//echo $_SESSION['admin']['tipe'];
?>


<?php 
include_once 'sidebar.php';
include_once 'top-nav.php';
?>

<?php 
include_once '../model/Petugas.php';
include_once '../model/File.php';

$file = new File();
$petugas = new ControlPetugas();
$petugas->setUsername($_SESSION['admin']['username']);
$data=$petugas->fetchAllProfile();
$r_alamat=$petugas->getAllAlamatUser();
$telps=$petugas->getAllTeleponUser();
?>



<div class="container" id="isi">

<?php 
if (isset($_FILES['cobaz'])) {
    $filetype = pathinfo($_FILES['cobaz']['name'],PATHINFO_EXTENSION);
    
    $file->setNamafile($_FILES['cobaz']['name']);
    $petugas->setPicture($_FILES['cobaz']['name']);
    $file->setFolder('../images/member/');
    $file->setUkuran($_FILES['cobaz']['size']);
    $file->setEkstensi($filetype);
    $file->setTemp($_FILES['cobaz']['tmp_name']);

    if (!file_exists($file->getFolder().$file->getNamafile())) {
        if ($filetype=='jpg'||$filetype=='png'||$filetype=='jpeg') {
            echo $file->uploadFile();
            $petugas->editPicture();
            echo "<script>location.replace('".$_SERVER['SCRIPT_NAME']."')</script>";
        } else {
            echo "<script>alert('Hanya File JPG, JPEG, PNG yang dapat di upload!')</script>";
        }        
    }else{
        echo "<script>alert('Hanya File JPG, JPEG, PNG yang dapat di upload!')</script>";
    }
}
?>

    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="page-header">
                <h2>Profil User</h2>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-3">
            <div class="row">
                <?php if (@$data->picture==''||empty(@$data->picture)): ?>
                    <img src="https://www.ravensbourne.ac.uk/content/img/default-pupil-profile.png" class="img img-responsive">
                    <?php else: ?>
                    <img src="<?= getBaseUrl() ?>images/member/<?= $data->picture ?>" class="img img-responsive">        
                <?php endif ?>
                    
            </div>
            <br>
            <div class="row">
                <form id="my-form" action="" enctype="multipart/form-data" method="post">
                    <center>
                        <input id="file-3" type="file" class="file" name="cobaz">
                    </center>
                </form>
            </div>
        </div>

        <div class="col-md-9">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th colspan="2">
                                    <?= $data->nama ?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><strong>Lahir</strong></td><td><?= $data->lahir ?></td>
                            </tr>
                            <tr>
                                <td><strong>Alamat</strong></td><td><?= $data->alamat ?></td>
                            </tr>
                            <tr>
                                <td><strong>Telepon</strong></td><td><?= $data->telepon ?></td>
                            </tr>
                            <tr>
                                <td><strong>Jabatan</strong></td><td><?= $data->nama_jabatan ?></td>
                            </tr>                    
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button id="btnSlide" class="btn btn-lg btn-warning" type="button" data-toggle="collapse" data-target="#divformEdit" style="width:100%">Edit</button>
                </div>
            </div>
            <br>
            <div class="row collapse" id="divformEdit">
                
                <div class="col-md-12">
                    
                    
                        <?php if (count($r_alamat)>0): ?>
                        
                            <?php for ($i = 0; $i < count($r_alamat); $i++) { ?>
                                <?php if ($i==0): ?>
                                <div class="form-group" id="jjl<?php echo $i;?>">
                                    <label>Alamat</label>
                                    <div class="form-inline">
                                        <textarea class="form-control" <?= "id='taz".$r_alamat[$i]->no."'" ?> name="edt[alamat]" style="width:70%"><?= $r_alamat[$i]->alamat ?></textarea>
                                        <span class="btn btn-danger" <?= "id='tazd".$r_alamat[$i]->no."'" ?> onclick="eas(this.id)" title="remove">
                                            <i class="glyphicon glyphicon-remove"></i>
                                        </span>
                                        <span class="btn btn-info" title="edit and submit" <?= "id='tazb".$r_alamat[$i]->no."'" ?> onclick="editAlamatz(this.id)">
                                            <i class="glyphicon glyphicon-edit"></i>
                                        </span>
                                    </div>

                                <?php else: ?>
                                <div class="form-group" >
                                    <div class="form-inline">
                                        <textarea class="form-control" <?= "id='taz".$r_alamat[$i]->no."'" ?> name="edt[alamat]" style="width:70%"><?= $r_alamat[$i]->alamat ?></textarea>
                                        <span class="btn btn-danger" <?= "id='tazd".$r_alamat[$i]->no."'" ?> onclick="eas(this.id)" title="remove">
                                            <i class="glyphicon glyphicon-remove"></i>
                                        </span>
                                        <span class="btn btn-info" title="edit and submit" <?= "id='tazb".$r_alamat[$i]->no."'" ?> onclick="editAlamatz(this.id)">
                                            <i class="glyphicon glyphicon-edit"></i>
                                        </span>
                                    </div>
                                </div>                            
                                <?php endif ?>


                            <?php } ?>
                        <div class="form-group" id="jajal">
                            <?php //klo data sudah ada di database maka hanya muncul tombol negatif ?>
                            <?php //kalau belum ada maka ada dua tombol, negatif dan submit ?>
                            <div class="form-inline">
                                <textarea class="form-control tanggalPelaksanaan" id="taa" name="edt[alamat]" style="width:70%"></textarea>
                                <span class="btn btn-default" id="tambahTombol" title="add more">
                                    <i class="glyphicon glyphicon-plus"></i>
                                </span>
                                <span class="btn btn-info" title="edit and submit" id="taab" onclick="submitAlamat(this.id)">
                                    <i class="glyphicon glyphicon-edit"></i>
                                </span>
                            </div>
                        </div>


                            <?php else: ?>
                        <div class="form-group" id="jajal">
                            <?php //klo data sudah ada di database maka hanya muncul tombol negatif ?>
                            <?php //kalau belum ada maka ada dua tombol, negatif dan submit ?>
                            <label>Alamat</label>
                            <div class="form-inline">
                                <textarea class="form-control tanggalPelaksanaan" id="taa" name="edt[alamat]" style="width:70%"></textarea>
                                <span class="btn btn-default" id="tambahTombol" title="add more">
                                    <i class="glyphicon glyphicon-plus"></i>
                                </span>
                                <span class="btn btn-info" title="edit and submit" id="taab" onclick="submitAlamat(this.id)">
                                    <i class="glyphicon glyphicon-edit"></i>
                                </span>
                            </div>
                        </div>
                        <?php endif ?>



                        
                                        <!--
                    <div class='form-group' id=''>
                        <div class='form-inline'>
                            <textarea class='form-control' name='edt[alamat]' style='width:80%'></textarea>
                            <span class='btn btn-default'><i class='glyphicon glyphicon-plus'></i></span>
                            <span class='btn btn-primary' title='submit'><i class='glyphicon glyphicon-edit'></i></span>
                        </div>
                    </div>
                    -->

                    <?php if (count($telps)>0): ?>
                        
                            <?php for ($i = 0; $i < count($telps); $i++) { ?>
                                <?php if ($i==0): ?>
                                    <div class="form-group" id="cb">
                                        <label>Telepon</label>
                                        <div class="form-inline">
                                            <input type="text" name="edt[telepon]" <?php echo "id='tft".$telps[$i]->no."'"; ?> class="form-control" value="<?= $telps[$i]->no_telepon ?>">
                                            <span class="btn btn-danger" onclick="ets(this.id)" title="remove" <?php echo "id='tfth".$telps[$i]->no."'"; ?>>
                                                <i class="glyphicon glyphicon-remove"></i>
                                            </span>
                                            <span class="btn btn-info" <?php echo "id='tftb".$telps[$i]->no."'"; ?> onclick="et(this.id)" title="edit and submit">
                                                <i class="glyphicon glyphicon-edit"></i>
                                            </span>
                                        </div>
                                    </div>
                                <?php else: ?>
                                    <div class="form-group" id="cb">
                                        <div class="form-inline">
                                            <input type="text" name="edt[telepon]" <?php echo "id='tft".$telps[$i]->no."'"; ?> class="form-control" value="<?= $telps[$i]->no_telepon ?>">
                                            <span class="btn btn-danger" onclick="ets(this.id)" <?php echo "id='tfth".$telps[$i]->no."'"; ?> title="remove">
                                                <i class="glyphicon glyphicon-remove"></i>
                                            </span>
                                            <span class="btn btn-info" <?php echo "id='tftb".$telps[$i]->no."'"; ?> onclick="et(this.id)" title="edit and submit">
                                                <i class="glyphicon glyphicon-edit"></i>
                                            </span>
                                        </div>
                                    </div>
                                <?php endif ?>
                            <?php } ?>

                    <div class="form-group" id="coba">
                        <div class="form-inline">
                            <input type="text" name="edt[telepon]" id="tft" class="form-control telepon">
                            <span class="btn btn-default" id="addNew" title="add more">
                                <i class="glyphicon glyphicon-plus"></i>
                            </span>
                            <span class="btn btn-info" id="tftb" onclick="submitTelepon(this.id)" title="edit and submit">
                                <i class="glyphicon glyphicon-edit"></i>
                            </span>
                        </div>
                    </div>

                    <?php else: ?>

                    <div class="form-group" id="coba">
                        <label>Telepon</label>
                        <div class="form-inline">
                            <input type="text" name="edt[telepon]" id="tft" class="form-control telepon">
                            <span class="btn btn-default" id="addNew" title="add more">
                                <i class="glyphicon glyphicon-plus"></i>
                            </span>
                            <span class="btn btn-info" id="tftb" onclick="submitTelepon(this.id)" title="edit and submit">
                                <i class="glyphicon glyphicon-edit"></i>
                            </span>
                        </div>
                    </div>
                    <?php endif ?>
                </div>
            </div>

        </div><!--end col-md-9-->
    </div>        
	
</div>



<script type="text/javascript" src=<?php echo "'".getBaseUrl()."js/jquery-datepicker/jquery-ui.js'"; ?>></script>
<script type="text/javascript">
    $(document).ready(function(){

        $('#tambahTombol').click(function(){
    
            var cobayah = $('.tanggalPelaksanaan').length;
            var iseng = "tambahan" + cobayah;
            if ($('#tambahan').length){

            if (cobayah==2) {
                $('#tambahan').after("<div class='form-group tanggalPelaksanaan' id='"+iseng+"'><div class='form-inline'><textarea class='form-control' name='edt[alamat]' style='width:80%' id='ta"+iseng+"'></textarea> <span class='btn btn-default' onclick='removeElement("+'"'+iseng+'"'+")'><i class='glyphicon glyphicon-minus'></i></span><span class='btn btn-primary' title='submit' onclick='submitAlamatz("+'"ta'+iseng+'"'+")'><i class='glyphicon glyphicon-edit'></i></span></div></div>");
            }else{
                var okeh = cobayah-1;
                var iseng = "tambahan" + okeh;
                $("#"+iseng).after("<div class='form-group tanggalPelaksanaan' id='tambahan"+cobayah+"'><div class='form-inline'><textarea class='form-control' name='edt[alamat]' id='ta"+cobayah+"' style='width:80%'></textarea> <span class='btn btn-default' onclick='removeElement("+'"tambahan'+cobayah+'"'+")'><i class='glyphicon glyphicon-minus'></i></span><span class='btn btn-primary' title='submit' onclick='submitAlamatz("+'"ta'+cobayah+'"'+")'><i class='glyphicon glyphicon-edit'></i></span></div></div>");
            };
            //nilai +=1;
            }else{
                $('#jajal').after("<div class='form-group tanggalPelaksanaan' id='tambahan'><div class='form-inline'><textarea class='form-control' id='tambahanta' name='edt[alamat]' style='width:80%'></textarea> <span class='btn btn-default' onclick='removeElement("+'"'+"tambahan"+'"'+")'><i class='glyphicon glyphicon-minus'></i></span><span class='btn btn-primary' title='submit' onclick='submitAlamatz("+'"'+"tambahanta"+'"'+")'><i class='glyphicon glyphicon-edit'></i></span></div></div>");
            };      
        }); //end tambahTombol

        $('#addNew').click(function(){
    
            var cobayah = $('.telepon').length;
            var iseng = "tbh" + cobayah;
            if ($('#tbh').length){

            if (cobayah==2) {
                $('#tbh').after("<div class='form-group telepon' id='"+iseng+"'><div class='form-inline'><input id='tf"+iseng+"' type='text' name='edt[telepon]' class='form-control'><span class='btn btn-default' id='addNew' title='add more' onclick='removeElement("+'"'+iseng+'"'+")'><i class='glyphicon glyphicon-minus'></i></span><span class='btn btn-primary' title='submit' onclick='submitTeleponz("+'"'+"tf"+iseng+'"'+")'><i class='glyphicon glyphicon-edit'></i></span></div></div>");
            }else{
                var okeh = cobayah-1;
                var iseng = "tbh" + okeh;
                $("#"+iseng).after("<div class='form-group telepon' id='tbh"+cobayah+"'><div class='form-inline'><input id='tbhtf"+cobayah+"' type='text' name='edt[telepon]' class='form-control'><span class='btn btn-default' id='addNew' title='add more' onclick='removeElement("+'"tbh'+cobayah+'"'+")'><i class='glyphicon glyphicon-minus'></i></span><span class='btn btn-primary' title='edit and submit' onclick='submitTeleponz("+'"tbhtf'+cobayah+'"'+")'><i class='glyphicon glyphicon-edit'></i></span></div></div>");
            };
            //nilai +=1;
            }else{
                $('#coba').after("<div class='form-group telepon' id='tbh'><div class='form-inline'><input type='text' id='tbhtf' name='edt[telepon]' class='form-control'><span class='btn btn-default' id='addNew' title='add more' onclick='removeElement("+'"'+"tbh"+'"'+")'><i class='glyphicon glyphicon-minus'></i></span></i></span><span class='btn btn-primary' title='submit' onclick='submitTeleponz("+'"'+"tbhtf"+'"'+")'><i class='glyphicon glyphicon-edit'></i></span></div></div>");
            };      
        }); //end tambahTombol
    }); //end document ready function


    function submitAlamat(id) {
        var ida = id.substring(0, 3);
        var alamat = $('#'+ida).val();

        // alert(alamat);
        if (alamat=='') {
            alert('Harap isi alamat anda!');
            return;
        }

        var jsonDatas = {
            'alamat':alamat,
            'username':<?php echo "'".$_SESSION['admin']['username']."'"; ?>,
        }
        $('#'+id).attr('disabled','disabled');
        $.ajax({
            type: "POST",
            url: "reverse",
            data: {'addalamat':jsonDatas},
            cache: false,
            success: function(data){
                alert(data);
                window.location.replace("<?php echo $_SERVER['SCRIPT_NAME']; ?>");
            }
        }); //end ajax
    }//tested

    function editAlamatz(id) {
        var ida = id.substring(4, id.length);
        var alamat = $('#taz'+ida).val();
        // alert(alamat);
        var jsonDatas = {
            'addr':alamat,
            'id':ida,
        }
        $('#'+id).attr('disabled','disabled');
        $.ajax({
            type: "POST",
            url: "reverse",
            data: {'editalamat':jsonDatas},
            cache: false,
            success: function(data){
                alert(data);
                window.location.replace("<?php echo $_SERVER['SCRIPT_NAME']; ?>");
            }
        }); //end ajax
    } //tested

    function eas(id) {
        var ida = id.substring(4, id.length);
        var alamat = $('#taz'+ida).val();
        // alert(alamat);
        var jsonDatas = {
            'st':'0',
            'id':ida,
        }
        $('#'+id).attr('disabled','disabled');
        $.ajax({
            type: "POST",
            url: "reverse",
            data: {'editstalamat':jsonDatas},
            cache: false,
            success: function(data){
                alert(data);
                window.location.replace("<?php echo $_SERVER['SCRIPT_NAME']; ?>");
            }
        }); //end ajax
    } //tested

    function submitTelepon(id) {
        var idt = id.substring(0, 3);
        var telepon = $('#'+idt).val();

        var jsonDatas = {
            'telp':telepon,
            'username':<?php echo "'".$_SESSION['admin']['username']."'"; ?>,
        }
        $('#'+id).attr('disabled','disabled');
        $.ajax({
            type: "POST",
            url: "reverse",
            data: {'addtelp':jsonDatas},
            cache: false,
            success: function(data){
                alert(data);
                window.location.replace("<?php echo $_SERVER['SCRIPT_NAME']; ?>");
            }
        }); //end ajax  
    }//tested

    function et(id) {

        var ida = id.substring(4, id.length);
        var telepon = $('#tft'+ida).val();
        var jsonDatas = {
            'telepon':telepon,
            'id':ida,
        }
        $('#'+id).attr('disabled','disabled');
        $.ajax({
            type: "POST",
            url: "reverse",
            data: {'etu':jsonDatas},
            cache: false,
            success: function(data){
                alert(data);
                window.location.replace("<?php echo $_SERVER['SCRIPT_NAME']; ?>");
            }
        }); //end ajax
    }

    function ets(id) {

        var ida = id.substring(4, id.length);
        var telepon = $('#tft'+ida).val();
        var jsonDatas = {
            'st':'0',
            'id':ida,
        }
        $('#'+id).attr('disabled','disabled');
        $.ajax({
            type: "POST",
            url: "reverse",
            data: {'editsttelp':jsonDatas},
            cache: false,
            success: function(data){
                alert(data);
                window.location.replace("<?php echo $_SERVER['SCRIPT_NAME']; ?>");
            }
        }); //end ajax
    }//tested


    function removeElement(id) {
        $('#'+id).remove();
    }

    $('#btnSlide').click(function(){
        // alert('oke');
    });

</script>
</body>
</html> 