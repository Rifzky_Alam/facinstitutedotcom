<?php 
session_start();
if (isset($_SERVER['PATH_INFO'])) {
	@$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
	$key = array_shift($url_segment);
	switch ($key) {
		case '':
			header('location:https://fac-institute.com/administrasi/absensi');
			break;
		case 'databelomabsenjson':
			include_once '/home/facinsti/public_html/administrasi/controller/user.controller.php';
			User::JsonDataAbsenBelomLengkap($_SESSION['admin']['username']);
			break;
	}
}else{
	include_once 'session/session-class.php';
	include_once '../baseurl.php';
	include_once '../model/Petugas.php';
	$petugas = new ControlPetugas();
	$sesi = new Sessionz();
	$sesi->Staff();
	$dataabsen = $petugas->sesiAbsen($_SESSION['admin']['username']);
	include_once HomeDirectory().'administrasi/view/absensi/mail-content.php';
	// print_r($dataabsen);
	$tanggal = date('Y-m-d');
	$tanggal = explode('-', $tanggal);
	$tanggal = $tanggal[2].'-'.$tanggal[1].'-'.$tanggal[0];
	$jam = date('H');
	$jambuka = 6;
	$jamtutup = 23;
	if ($jam > $jamtutup || $jam < $jambuka) { //jam tutup absen
		
		$data = array(
			'base_url' => getBaseUrl(), //base data
			'url'=> basename(__FILE__, '.php'),
			'judul' => 'Absensi/FAC Institute',
		    'nama_petugas' => $_SESSION['admin']['nama'],
		    'subtitle' => 'Absensi - FAC Institute', 
		    'page' => 'absensi', // end base data
		    'username' => $_SESSION['admin']['nama'],
		    'is_success' => 'Pemberitahuan',
		    'notif' => 'Absen Sudah ditutup, Silahkan absen setelah pukul '.$jambuka.':00:00 dan sebelum pukul '.$jamtutup.':00:00',
		    'absen_pulang' => $dataabsen->absen_pulang,
		    'absen_masuk' => $dataabsen->absen_masuk
		);

		$data = (object) $data;
		include_once 'view/absensi/view-success-absenp.php';



	}else{ //absen masuk

		if (empty($dataabsen)) {
			include_once '../model/Pendaftar.php';
			include_once 'view/absensi/view-abs-function.php';	
			$pendaftar = new Pendaftar();
			if (isset($_POST['deskripsi'])) { //post absen masuk
				
				if ($_POST['deskripsi']['koordMasuk']==''||empty($_POST['deskripsi']['koordMasuk'])) {
					echo "<script>alert('Harap mengaktifkan location service pada browser anda, Absen Gagal!');</script>";
					echo "<script>location.replace('".basename(__FILE__,'.php')."');</script>";
				}elseif ($_POST['deskripsi']['client']==''&&$_POST['deskripsi']['request']=='') {
					echo "<script>alert('Harap Memilih Perusahaan!');</script>";
					echo "<script>location.replace('".basename(__FILE__,'.php')."');</script>";
				} else{
					

					$_POST['deskripsi']['koordMasuk'] = str_replace('(', '', $_POST['deskripsi']['koordMasuk']);
					$_POST['deskripsi']['koordMasuk'] = str_replace(')', '', $_POST['deskripsi']['koordMasuk']); 
					$absen = [
						'username' => $_SESSION['admin']['username'],
						'usaha' => NamaUsaha($_POST['deskripsi']['request'],$_POST['deskripsi']['client']),
						'lokasimasuk' => $_POST['deskripsi']['koordMasuk']
					];
					$absen = (object)$absen;
					$petugas->setAbsen($absen);
					if ($petugas->AbsenMasukk()) {
						$dataabsen = $petugas->sesiAbsen($_SESSION['admin']['username']);
						echo "<script>alert('Absen masuk berhasil!');</script>";

						$data = [
					    	'petugas' => $dataabsen->nama,
					    	'time' => $dataabsen->absen_masuk,
					    	'jenisabsen' => '1',
					    	'lokasi' => $dataabsen->lokasi_absen_masuk 
						];
						$data = (object)$data;

						include_once '../model/Mail.php';
						$surat = new FACMail();

						$dataEmail = array(
					    	'to' => $dataabsen->email,
					    	'nama'=> '',
					    	'subject'=> 'Absen Masuk - FAC Institute ('.$tanggal.')',
					    	'content'=> Absensi($data)
					    );
						$dataEmail = (object) $dataEmail;
						$surat->sendMail($dataEmail);

						if ($dataabsen->absen_status=='0') {
							include_once 'controller/adminabsen.controller.php';
							Absen::SendEmail($dataabsen->id,$_POST['deskripsi']['client']);
						}
						
						echo "<script>location.replace('".basename(__FILE__,'.php')."');</script>";
					} else {
						echo "<script>alert('Absen masuk gagal!');</script>";
					}
				}
			} //end post absen masuk

			$data = array(
				'base_url' => getBaseUrl(), //base data
				'url'=> basename(__FILE__, '.php'),
				'judul' => 'Absensi/FAC Institute',
			    'nama_petugas' => $_SESSION['admin']['nama'],
			    'subtitle' => 'Absensi - FAC Institute', 
			    'page' => 'absensi', // end base data
			    'username' => $_SESSION['admin']['nama'],
			    'datepicker' => '1',
			    'dataclient' => json_decode($petugas->getDataPendaftarToday(date('Y-m-d'))),
			    'DataAutoComplete' => NamaUsahaList(json_decode($pendaftar->autoCompleteNamaPerusahaan())),
			);

			$data = (object) $data;
			// print_r($data->dataclient);
			include_once 'view/absensi/view-absensi-absen.php';	
		}else {
			
			if (!empty($dataabsen->absen_pulang)) {
				// echo 'sudah absen pulang yah!';
				include_once '../model/Laporan.php';
				include_once 'view/absensi/view-abs-function.php';	
				$laporan = new ControlLaporan();

				$datainputlaporan = [
					'username' => $dataabsen->username,
					'jamMulai' => $dataabsen->absen_masuk,
					'jamSelesai' => $dataabsen->absen_pulang,
					'idAbsen' => $dataabsen->id 
				];

				// print_r($dataabsen);
				$datainputlaporan = (object)$datainputlaporan;
				$laporan->setAbsen($datainputlaporan);
				$data = array(
					'base_url' => getBaseUrl(), //base data
					'url'=> basename(__FILE__, '.php'),
					'judul' => 'Absensi/FAC Institute',
				    'nama_petugas' => $_SESSION['admin']['nama'],
				    'subtitle' => 'Absensi - FAC Institute', 
				    'page' => 'absensi', // end base data
				    'username' => $_SESSION['admin']['nama'],
				    'is_success' => 'Pemberitahuan',
				    'notif' => LaporanNotif($laporan->NewLaporan()),
				    'absen_pulang' => $dataabsen->absen_pulang,
				    'absen_masuk' => $dataabsen->absen_masuk
				);

				$data = (object) $data;
				include_once 'view/absensi/view-success-absenp.php';


			} else {
				// absen pulang
				
				if (isset($_POST['in'])) {
					// include_once 'view/absensi/view-abs-function.php';	

					if ($_POST['in']['koordPulang']==''||empty($_POST['in']['koordPulang'])) {
						echo "<script>alert('Harap mengaktifkan location service pada browser anda, Absen Gagal!')</script>;";
						echo "<script>location.replace('".basename(__FILE__,'.php')."');</script>;";
					}else{

						$_POST['in']['koordPulang'] = str_replace('(', '', $_POST['in']['koordPulang']);
						$_POST['in']['koordPulang'] = str_replace(')', '', $_POST['in']['koordPulang']); 
						$absen = [
							'username' => $_SESSION['admin']['username'],
							'lokasipulang' => $_POST['in']['koordPulang']
						];
						$absen = (object)$absen;
						$petugas->setAbsen($absen);
						if ($petugas->absenPulangg()) {
							echo "<script>alert('Absen pulang berhasil!');</script>;";

							$dataabsen = $petugas->sesiAbsen($_SESSION['admin']['username']);

							$data = [
					    		'petugas' => $dataabsen->nama,
					    		'time' => $dataabsen->absen_pulang,
					    		'jenisabsen' => '2',
					    		'lokasi' => $dataabsen->lokasi_absen_pulang 
							];
							$data = (object)$data;


							include_once '../model/Mail.php';
							$surat = new FACMail();

							$dataEmail = array(
					    		'to' => $dataabsen->email,
					    		'nama'=> '',
					    		'subject'=> 'Absen Pulang - FAC Institute ('.$tanggal.')',
					    		'content'=> Absensi($data)
					    	);
							$dataEmail = (object) $dataEmail;
							$surat->sendMail($dataEmail);


							echo "<script>location.replace('".basename(__FILE__,'.php')."');</script>";
						} else {
							echo "<script>alert('Absen pulang gagal!');</script>";
						}
					}
				} // end post data

				$data = array(
					'base_url' => getBaseUrl(), //base data
					'url'=> basename(__FILE__, '.php'),
					'judul' => 'Absensi/FAC Institute',
				    'nama_petugas' => $_SESSION['admin']['nama'],
				    'subtitle' => 'Absensi - FAC Institute', 
				    'page' => 'absensi', // end base data
				    'username' => $_SESSION['admin']['nama'],
				    'absen_masuk' => $dataabsen->absen_masuk,
				);

				$data = (object) $data;
				include_once 'view/absensi/view-absensi-absenp.php';


			}
					
		}
	}	
}
?>