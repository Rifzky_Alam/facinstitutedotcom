<?php 
session_start();
if (!isset($_SESSION['admin'])||empty($_SESSION['admin']['nama'])||($_SESSION['admin']['tipe']!='admin'&&$_SESSION['admin']['tipe']!='marketing')) {
    header('Location:../sessions/index.php');
}
?>


<html>
<?php 
$judul ='Input Data Transaksi';
$page = 'orderNew';
include_once 'header.php'; 

?>
<body>
<?php //include_once 'map-script.php'; ?>
<?php 

include_once 'sidebar.php';
include_once 'top-nav.php';
include_once '../model/Perusahaan.php';
include_once '../model/Pendaftar.php';
include_once '../model/page.php';
$ctrPage= new Page();
$ctrTransaksi = new Pendaftar();
$perusahaan= new Perusahaan();
$dataAutoComplete = json_decode($ctrTransaksi->autoCompleteNamaPerusahaan());
$btnActive=false;

if (isset($_GET['ac'])&&$_GET['ac']=='in') {
    if (isset($_POST['data'])) {
        //
        $transaksi = array(
                    'id' => md5($_POST['data']['namaPerusahaan'].'--'.$_POST['data']['paket'].'--'.date('Y-m-d')),
                    'namaPerusahaan'=>$_POST['data']['namaPerusahaan'],
                    'namaPersonal'=>$_POST['data']['namaPersonal'],
                    'email'=>$_POST['data']['emailPersonal'],
                    'telepon'=>$_POST['data']['teleponPersonal'],
                    'jabatan'=>$_POST['data']['jabatan'],
                    'jenisPengguna'=>$_POST['data']['jnsPengguna'],
                    'versiAccurate'=>implode(" # ", $_POST['data']['jnsAccurate']),
                    'paket'=>$_POST['data']['paket'],
                    'qty'=>$_POST['data']['qty'],
                    'transport'=>str_replace(',', '', $_POST['data']['transport']),
                    'qtyTrans'=> $_POST['data']['qtyTrans'],
                    'statusTransaksi'=>4,
                    'sales'=>$_POST['data']['sales'],
                    'tempatBeli'=>$_POST['data']['tempatBeli'],
                    'tanggalDaftar'=>date('Y-m-d')
            );

            $transaksi = (object) $transaksi;

            if ($ctrTransaksi->masukDataTransaksi($transaksi)) {
                $ctrPage->addLogForLogin($_SESSION['admin']['username'],'added a new transaction',$_SERVER['REMOTE_ADDR']);
                echo "<script>alert('Data berhasil disimpan');</script>";
                echo "<script>location.replace('data-training');</script>";
            }else{
                echo "<script>alert('Data gagal disimpan, harap hubungi administrator system!');</script>";
                //biasanya double primary key
            }        
    }
}


?>


	<div class="container">
	<div class="row">
		<section>
        <div class="wizard">
            <div class="wizard-inner">
                <div class="connecting-line"></div>
                <ul class="nav nav-tabs" role="tablist">

                    <li role="presentation" class="active">
                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Data Perusahaan">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-briefcase" style="top:18px"></i>
                            </span>
                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Data Training">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-list-alt" style="top:18px"></i>
                            </span>
                        </a>
                    </li>
                    <li role="presentation" class="disabled">
                        <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Personal Kontak">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-user" style="top:18px"></i>
                            </span>
                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                        <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-ok" style="top:18px"></i>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>

            
                <div class="tab-content">
                    <div class="tab-pane active" role="tabpanel" id="step1">
                        <h3>Step 1 -- Data Perusahaan</h3>
                        <br>
                        <form action=<?php echo "'".basename(__FILE__, '.php')."'"; ?> method='get'>
                        <div class='row'>
                            <p style='padding-left:15px'>Biasakan untuk mengetik nama perusahaan, jangan copy-paste</p>
                        	<div class='col-md-8'>
                        		<input id='namaPerusahaan' class='form-control' name="search[np]" placeholder='Cari data berdasarkan nama perusahaan, di ketik jangan copy-paste!' />
                        	</div>
                            <div class='col-md-4'>
                                <button class='btn btn-primary'>Search</button>
                            </div>
                        </div>
                        </form>

                        <br><br><br>
                        
                        <form role="form" action=<?php echo "'".basename(__FILE__, '.php')."?ac=in'"; ?> method='post'>
                        <table cellspacing='10'>
                            <tbody>
                                <?php if (isset($_GET['search'])&&!empty($_GET['search']['np'])): ?>
                                <?php  
                                

                                $obj = json_encode($_GET['search']);
                                $obj = json_decode($obj);
                                $myDatas= json_decode($perusahaan->selectByNamaPerusahaan($obj));
                                
                                for ($i=0; $i < count($myDatas); $i++) { 
                                
                                ?>
                                <tr>
                                    <td>
                                      <input type="radio" name="data[namaPerusahaan]" value=<?php echo $myDatas[$i]->id ?>>
                                    </td>
                                    <td class='geser'>
                                        <div class="row">
                                                <div class="col-md-12">
                                                
                                                    <div class="panel-group" <?php echo "id='".$myDatas[$i]->id."'" ?>>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a href=<?php echo "'#".$myDatas[$i]->id.$i."'"; ?> data-parent=<?php echo "'#".$myDatas[$i]->id."'"; ?> class='tengah' data-toggle="collapse"><?php echo $myDatas[$i]->nama ?></a>
                                                                </h4>
                                                            </div>
                                                            <div class="panel-collapse in" <?php echo "id='".$myDatas[$i]->id.$i."'" ?>>
                                                                <div class="panel-body">
                                                                    <section class='col-md-3'>
                                                                        <label>Jenis usaha</label>
                                                                        <p><?php echo $myDatas[$i]->ket_jenis_usaha; ?></p>
                                                                    </section>
                                                                    <section class='col-md-3'>
                                                                        <label>Kota</label>
                                                                        <p><?php echo $myDatas[$i]->kota; ?></p>
                                                                    </section>
                                                                    <section class='col-md-3'>
                                                                        <label>Telepon</label>
                                                                        <p><?php echo $myDatas[$i]->telepon; ?></p>
                                                                    </section>
                                                                    <section class='col-md-3'>
                                                                        <label>Map</label>
                                                                        <p><?php echo $myDatas[$i]->map; ?></p>
                                                                    </section>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                    </td>
                                </tr>                                
                                <?php } ?>

                                <?php endif ?>

                                <?php if (isset($_GET['kp'])&&!empty($_GET['kp'])): ?>
                                    <?php $dataPer = $perusahaan->selectByID($_GET['kp']) ?>
                                    <?php $btnActive=true; ?>
                                <tr>
                                    <td>
                                      <input type="radio" name="data[namaPerusahaan]" value=<?php echo "'".$dataPer->id."'"; ?> checked>
                                    </td>
                                    <td class='geser'>
                                        <div class="row">
                                                <div class="col-md-12">
                                                
                                                    <div class="panel-group" <?php echo "id='".$dataPer->id."'" ?>>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a href=<?php echo "'#".$dataPer->id.$i."'"; ?> data-parent=<?php echo "'#".$dataPer->id."'"; ?> class='tengah' data-toggle="collapse"><?php echo $myDatas[$i]->nama ?></a>
                                                                </h4>
                                                            </div>
                                                            <div class="panel-collapse in" <?php echo "id='".$dataPer->id.$i."'" ?>>
                                                                <div class="panel-body">
                                                                    <section class='col-md-3'>
                                                                        <label>Jenis usaha</label>
                                                                        <p><?php echo $dataPer->ket_jenis_usaha; ?></p>
                                                                    </section>
                                                                    <section class='col-md-3'>
                                                                        <label>Kota</label>
                                                                        <p><?php echo $dataPer->kota; ?></p>
                                                                    </section>
                                                                    <section class='col-md-3'>
                                                                        <label>Telepon</label>
                                                                        <p><?php echo $dataPer->telepon; ?></p>
                                                                    </section>
                                                                    <section class='col-md-3'>
                                                                        <label>Map</label>
                                                                        <p><?php echo $dataPer->map; ?></p>
                                                                    </section>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                    </td>
                                </tr> 
                                <?php endif ?>
           
                            </tbody>
                        </table>



                        <br>
                        <?php if (count(@$myDatas)!=0||$btnActive): ?>
                            <ul class="list-inline pull-right">
                                <li><button type="button" class="btn btn-primary next-step" id="btnSavePerusahaan">Save and continue</button></li>
                            </ul>
                            <?php else: ?>
                            <div class='row'>
                                <div class='col-md-10'>
                                    <a href=<?php echo "'".getBaseUrl()."administrasi/perusahaan.php?app=add'"; ?> class='btn btn-danger'>Jika tidak ada data perusahaan, silahkan isi terlebih dahulu disini</a>
                                </div>
                            </div>                         
                        <?php endif ?>
                        
                    </div>
                    <div class="tab-pane" role="tabpanel" id="step2">
                        <h3>Step 2 -- Data kebutuhan training</h3>
                        <br>

                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <h4>Jenis Pengguna</h4>
                                    <div class="radio"><label><input type="radio" name="data[jnsPengguna]" value="baru">Baru</label></div>
                                    <div class="radio"><label><input type="radio" name="data[jnsPengguna]" value="lama">Lama</label></div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <h4>Versi Accurate</h4>
                                    <div class="checkbox"><label><input type="checkbox" name="data[jnsAccurate][]" value="accurate 4 standard edition">ACCURATE 4 Standar Edition</label></div>
                                    <div class="checkbox"><label><input type="checkbox" name="data[jnsAccurate][]" value="accurate 4 deluxe edition">ACCURATE 4 Deluxe Edition</label></div>
                                    <div class="checkbox"><label><input type="checkbox" name="data[jnsAccurate][]" value="accurate 4 enterprise edition">ACCURATE 4 Enterprise Edition</label></div>
                                    <div class="checkbox"><label><input type="checkbox" name="data[jnsAccurate][]" value="accurate 5 standard edition">ACCURATE 5 Standar Edition</label></div>
                                    <div class="checkbox"><label><input type="checkbox" name="data[jnsAccurate][]" value="accurate 5 deluxe edition">ACCURATE 5 Deluxe Edition</label></div>
                                    <div class="checkbox"><label><input type="checkbox" name="data[jnsAccurate][]" value="accurate 5 enterprise edition">ACCURATE 5 Enterprise Edition</label></div>
                                    <div class="checkbox"><label><input type="checkbox" name="data[jnsAccurate][]" value="accurate cloud">ACCURATE Online/Cloud</label></div>
                                    <div class="checkbox"><label><input type="checkbox" name="data[jnsAccurate][]" value="rene point of sales">RENE Point Of Sales</label></div>
                                    <div class="checkbox"><label><input type="checkbox" name="data[jnsAccurate][]" value="rene dan accurate">Gabungan RENE dan ACCURATE</label></div>
                                    <div class="form-inline">
                                        <input type="text" class="form-control" name="data[jnsAccurate][]" id="jns-accurateLainnya" placeholder='lainnya'>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <?php $dataPaket = json_decode($ctrTransaksi->fetchPaketTraining()); ?>
						<div class="row">
				            <div class="col-md-10">
				                <label><a href="input-paket">Nama Paket</a></label>
				                <select class="form-control" id="nitem" name="data[paket]">
				                    <?php  
				                        for ($i=0; $i < count($dataPaket); $i++) { 
				                            echo "<option value='".$dataPaket[$i]->id."'>".$dataPaket[$i]->nama_item." -- ".$dataPaket[$i]->price_for." -- (".$dataPaket[$i]->paket_hari.") [".number_format($dataPaket[$i]->harga)."]</option>";    
				                        }
				                    ?>
				                </select>
				            </div>
				        </div>
				        <br>
				        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <label for="qty">Qty</label>
                                    <input class="form-control" type="number" value="1" name="data[qty]" id="qty" placeholder="Berapa kali jumlah paket akan di order" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <label for="email">Biaya Transport</label>
                                    <input class="form-control" type="text" name="data[transport]" id="biaya-transport" placeholder="Hanya Angka" value="150000">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <label for="email">Jumlah Hari Transport</label>
                                    <input class="form-control" type="number" name="data[qtyTrans]" id="harian-transport" placeholder="Hanya Angka" value="1" maxlength="1">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <label for="email">Tempat Membeli Accurate</label>
                                    <input class="form-control" type="text" name="data[tempatBeli]" id="tempat-beli" placeholder="Jika Anda Tahu Tempat Membeli Software Kami">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <label for="email">Salesman Accurate</label>
                                    <input class="form-control" type="text" name="data[sales]" id="sales" placeholder="Jika Anda Tahu Nama Salesman yang Menjual Software Accurate Kepada Anda">
                                </div>
                            </div>
                        </div>

                        <ul class="list-inline pull-right">
                            <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                            <li><button type="button" class="btn btn-primary next-step" id="btnSaveTraining">Save and continue</button></li>
                        </ul>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="step3">
                        <h3>Step 3 -- Personal Kontak</h3>
                        <br>
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <label for="form-namaPersonal">Nama Personal Pendaftar</label>
                                    <input class="form-control" name="data[namaPersonal]" id="form-namaPersonal" placeholder=''>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <label for="telepon">Telepon Personal</label>
                                    <input class="form-control" name="data[teleponPersonal]" id="form-teleponCP" placeholder=''>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <label for="email">E-mail Personal Kontak</label>
                                    <input class="form-control" type="email" name="data[emailPersonal]" id="form-emailCP" placeholder=''>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <label for="email">Jabatan Personal Kontak</label>
                                    <input class="form-control" type="text" name="data[jabatan]" id="form-jabatanCP" placeholder=''>
                                </div>
                            </div>
                        </div>
                        
                        <ul class="list-inline pull-right">
                            <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                            <li><button type="button" class="btn btn-default next-step">Skip</button></li>
                            <li><button type="button" class="btn btn-primary btn-info-full next-step" id="btnSaveKontak">Save and continue</button></li>
                        </ul>
                    </div>

                    <div class="tab-pane" role="tabpanel" id="complete">
                        <h3>Data Preview</h3>
                        <div class="row">
                            <div class="col-md-10">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr><th colspan="2" style="text-align:center"><strong>Data Perusahaan</strong></th></tr>
                                    </thead>
                                    <tbody>
                                        <tr><td>Nama Perusahaan</td><td id="pre-namaPerusahaan">-</td></tr>
                                        <tr><td>Email Perusahaan</td><td id="pre-emailPerusahaan">-</td></tr>
                                        <tr><td>Alamat Perusahaan</td><td id="pre-alamatPerusahaan">-</td></tr>
                                        <tr><td>Kota</td><td id="pre-kotaPerusahaan">-</td></tr>
                                        <tr><td>Provinsi</td><td id="pre-provinsiPerusahaan">-</td></tr>
                                        <tr><td>Telepon</td><td id="pre-teleponPerusahaan">-</td></tr>
                                        <tr><td>Jenis Usaha</td><td id="pre-jenisPerusahaan">-</td></tr>
                                        <tr><td>Keterangan Jenis Usaha</td><td id="pre-ketJenisUsaha">-</td></tr>
                                        <tr><td>Map</td><td id="pre-mapPerusahaan">-</td></tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-10">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr><th colspan="2" style="text-align:center"><strong>Data Personal Kontak</strong></th></tr>
                                    </thead>
                                    <tbody>
                                        <tr><td>Jenis Pengguna</td><td id="pre-jnsPengguna">-</td></tr>
                                        <tr><td>Versi Accurate</td><td id="pre-versiAccurate">-</td></tr>
                                        <tr><td>Tempat Beli</td><td id="pre-tempatBeli">-</td></tr>
                                        <tr><td>Salesman Accurate</td><td id="pre-salesmanAccurate">-</td></tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <input name='data[tanggalPelaksanaanz]' id='pelaksanaanz' style='display:none' />  
                        <br>
                        <div class="row">
                            <div class="col-md-10">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr><th colspan="2" style="text-align:center"><strong>Kontak</strong></th></tr>
                                    </thead>
                                    <tbody>
                                        <tr><td>Nama</td><td id="pre-cpNama">-</td></tr>
                                        <tr><td>Telepon</td><td id="pre-cpTelepon">-</td></tr>
                                        <tr><td>Email</td><td id="pre-cpEmail">-</td></tr>
                                        <tr><td>Jabatan</td><td id="pre-cpJabatan">-</td></tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>                      
                        <button class='btn btn-success'>Save the data</button>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </section>
   </div>
</div>

<script type="text/javascript" src=<?php echo "'".getBaseUrl()."js/jquery-ui/jquery-ui.js'"; ?>></script>
<script type="text/javascript" src=<?php echo "'".getBaseUrl()."assets/jqnumber/jquery.number.js'"; ?>></script>
<script type="text/javascript">


$(document).ready(function(){

var availableTags = [ 'FAC Institute','FAC-Remote',
                <?php for ($i=0; $i < count($dataAutoComplete) ; $i++) { 
                    if ($i<count($dataAutoComplete)-1) {
                        echo "'".$dataAutoComplete[$i]->nama_perusahaan."',";
                    } else{
                        echo "'".$dataAutoComplete[$i]->nama_perusahaan."'";
                    };
                    
                } ?>
                ];
    $("#namaPerusahaan").autocomplete({
        source: availableTags
    });

    $('#biaya-transport').number( true, 0 );

    $('#btnSavePerusahaan').click(function(){
        var aidi = $('input[name="data[namaPerusahaan]"]:checked').val();
        $.ajax({
          type: "GET",
          url: "reverse",
          data: {
            'kp':aidi
            },
          cache: false,
          success: function(data){
                $('#pre-namaPerusahaan').html($(data).filter('#np').html());
                $('#pre-emailPerusahaan').html($(data).filter('#ep').html());
                $('#pre-alamatPerusahaan').html($(data).filter('#ap').html());
                $('#pre-kotaPerusahaan').html($(data).filter('#kp').html());
                $('#pre-provinsiPerusahaan').html($(data).filter('#pp').html());
                $('#pre-teleponPerusahaan').html($(data).filter('#tp').html());
                $('#pre-jenisPerusahaan').html($(data).filter('#jp').html());
                $('#pre-ketJenisUsaha').html($(data).filter('#kjup').html());
                $('#pre-mapPerusahaan').html($(data).filter('#mp').html()); 
          }
        }); //end ajax

        
    });
});

$('#nitem').change(function(){
        $.ajax({
              type: "GET",
              url: "reverse",
              data: {
                'npkt':$('#nitem').val()
                },
              cache: false,
              success: function(data){
                 $('#harian-transport').val($(data).filter('#paketHari').html());
              }
            }); //end ajax      
});

$('#btnSaveTraining').click(function(){

    $('#pre-ketJenisUsaha').html($('#ketJenisUsaha').val());
    $('#pre-mapPerusahaan').html($('#alamatz').val());

    $('#pre-jnsPengguna').html($('input[name="data[jnsPengguna]"]:checked').val());
    $('#pre-jumlahHari').html($('#hari-training').val());
    $('#pre-versiAccurate').html(
        $("input[name='data[jnsAccurate][]']:checked").map(function () {return this.value;}).get().join(" # ")+$('#jns-accurateLainnya').val()
    );
    
    $('#pre-tempatBeli').html($('#tempat-beli').val());
    $('#pre-salesmanAccurate').html($('#sales').val());

});

$('#btnSaveKontak').click(function(){
    $('#pre-cpNama').html($('#form-namaPersonal').val());
    $('#pre-cpTelepon').html($('#form-teleponCP').val());
    $('#pre-cpEmail').html($('#form-emailCP').val());
    $('#pre-cpJabatan').html($('#form-jabatanCP').val());
});

function removeElement(id) {
    $('#'+id).remove();
}


</script>


</body>
</html>