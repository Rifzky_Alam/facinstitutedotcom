<?php 
session_start();
if (!isset($_SESSION['admin']['username'])) {
	if ($_SESSION['admin']['tipe']!='admin') {
		header('location: https://fac-institute.com/administrasi/accessdenied');
	}
}
if (isset($_SERVER['PATH_INFO'])) {
	$_SERVER['PATH_INFO'] = str_replace('.php', '', $_SERVER['PATH_INFO']);
	@$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
	$key = array_shift($url_segment);
	switch ($key) {
		case 'adminwidget':

			echo 'setting widget for admin!';
			break;
		case 'petugas':
			include_once 'controller/settings.controller.php';
			Settings::Petugas();
			break;
		case 'cspetugas':
			include_once 'controller/settings.controller.php';
			if (isset($url_segment)&&count($url_segment)<2) {
				header('Location: http://fac-institute.com/administrasi/settings/petugas');
			}
			Settings::ChangeStaffStatus(@$url_segment[0],$url_segment[1]);
			break;
		case 'leveltrainer':
			if (isset($_GET['usr'])) {
				include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/controller/user.controller.php';
				User::ChangeUserLevelStatus($_GET['usr']);
			}
			break;
		case '':
			header('Location: https://fac-institute.com/administrasi/settings/admin');
			break;
	}
}