<?php 
session_start();
if (!isset($_SESSION['admin'])) {
    header('Location:../login/index.php');
}
?>




<!DOCTYPE html>
<html>

<?php 
//echo $tipeAdmin=$_SESSION['admin']['tipe'];
$judul='FAC-Edit Data Order';
$page = 'editOrderTraining';
include_once 'header.php'; 
include_once '../model/Pendaftar.php'; 
$pendaftar = new Pendaftar();
?>




<body>

<?php 

include_once 'sidebar.php';

include_once 'top-nav.php';

 ?>


<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3>Edit Data Order Training</h3>
	</div>

        <?php if (isset($_GET['v'])&&!empty($_GET['v'])): ?>
        <?php 

function removeLastString($value){
        if (substr($value,strlen($value)-3)==' # ') {
            return substr($value, 0,strlen($value)-3);
        }else{
            return $value;
        }
    }

        if (isset($_POST['edit'])) {

            $obj = (object) $_POST['edit'];

            if($pendaftar->editData($obj)){
                echo "<script>alert('data berhasil di ubah');</script>";
                echo "<script>location.replace('data-training');</script>";
            }else{
                echo "<script>alert('data gagal di ubah');</script>";
            }
            /*
            echo 'ID: '.$obj->id."<br>";
            echo 'Nama Personal: '.$obj->namaPersonal."<br>";
            echo 'Telepon: '.$obj->telepon."<br>";
            echo 'Email: '.$obj->email."<br>";
            echo 'Jabatan: '.$obj->jabatan."<br>";
            echo 'Jenis Pengguna: '.$obj->jnspengguna."<br>";
            echo 'Jenis Accurate: '.removeLastString(implode(" # ", $obj->jnsaccurate))."<br>";
            echo 'Agenda training: '.removeLastString(implode(" # ", $obj->agendatraining))."<br>";
            echo 'Tempat Beli: '.$obj->tempatbeli."<br>";
            echo 'Sales: '.$obj->sales."<br>";
            */
        }

        ?>
        <?php 
        $datas = $pendaftar->selectForEditByID($_GET['v']);
        ?>
        <form action='' method='post'>  
        <div class="row">
            <div class="col-md-9">
                <div class="form-group">
                    <label for="nama" >Nama Perusahaan *</label>
                    <h4><?php echo $datas->nama_perusahaan ?></h4>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label for="form-namaPersonal">Nama Personal Pendaftar</label>
                    <input name='edit[id]' style='display:none;' value=<?php echo "'".$_GET['v']."'"; ?> />
                    <input class="form-control" name="edit[namaPersonal]" id="form-namaPersonal" value=<?php echo "'".$datas->nama_personal."'"; ?> placeholder=''>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label for="telepon">Telepon Personal</label>
                    <input class="form-control" name="edit[telepon]" id="form-teleponCP" value=<?php echo "'".$datas->telepon."'"; ?> placeholder=''>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label for="email">E-mail Personal Kontak</label>
                    <input class="form-control" type="text" name="edit[email]" id="form-emailCP" value=<?php echo "'".$datas->email_kontak."'"; ?> placeholder=''>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label for="email">Jabatan Personal Kontak</label>
                    <input class="form-control" type="text" name="edit[jabatan]" id="form-jabatanCP" value=<?php echo "'".$datas->jabatan."'"; ?> placeholder=''>
                </div>
            </div>
        </div>

        <br>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <h4>Jenis Pengguna</h4>

                    <?php if ($datas->jenis_pengguna=='baru'):?>
                        <div class="radio"><label><input type="radio" name="edit[jnspengguna]" value="baru" checked>Baru</label></div>
                    <?php else: ?>
                        <div class="radio"><label><input type="radio" name="edit[jnspengguna]" value="baru">Baru</label></div>
                    <?php endif ?>

                    <?php if ($datas->jenis_pengguna=='lama'): ?>
                        <div class="radio"><label><input type="radio" name="edit[jnspengguna]" value="lama" checked>Lama</label></div>
                    <?php else: ?>
                        <div class="radio"><label><input type="radio" name="edit[jnspengguna]" value="lama">Lama</label></div>
                    <?php endif ?>

                    
                </div>
            </div>
        </div>

        <?php 
            $myAccurate = explode(" # ", $datas->versi_accurate);
        ?>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <h4>Versi Accurate</h4>

                    <?php if (in_array("accurate 4 standard edition", $myAccurate)): ?>
                        <div class="checkbox"><label><input type="checkbox" name="edit[jnsaccurate][]" value="accurate 4 standard edition" checked>ACCURATE 4 Standar Edition</label></div>
                    <?php else: ?>
                        <div class="checkbox"><label><input type="checkbox" name="edit[jnsaccurate][]" value="accurate 4 standard edition">ACCURATE 4 Standar Edition</label></div>
                    <?php endif ?>

                    <?php if (in_array("accurate 4 deluxe edition", $myAccurate)): ?>
                        <div class="checkbox"><label><input type="checkbox" name="edit[jnsaccurate][]" value="accurate 4 deluxe edition" checked>ACCURATE 4 Deluxe Edition</label></div>
                    <?php else: ?>
                        <div class="checkbox"><label><input type="checkbox" name="edit[jnsaccurate][]" value="accurate 4 deluxe edition">ACCURATE 4 Deluxe Edition</label></div>
                    <?php endif ?>

                    <?php if (in_array("accurate 4 enterprise edition", $myAccurate)): ?>
                        <div class="checkbox"><label><input type="checkbox" name="edit[jnsaccurate][]" value="accurate 4 enterprise edition" checked>ACCURATE 4 Enterprise Edition</label></div>
                    <?php else: ?>
                        <div class="checkbox"><label><input type="checkbox" name="edit[jnsaccurate][]" value="accurate 4 enterprise edition">ACCURATE 4 Enterprise Edition</label></div>
                    <?php endif ?>

                    <?php if (in_array("accurate 5 standard edition", $myAccurate)): ?>
                        <div class="checkbox"><label><input type="checkbox" name="edit[jnsaccurate][]" value="accurate 5 standard edition" checked>ACCURATE 5 Standar Edition</label></div>
                    <?php else: ?>
                        <div class="checkbox"><label><input type="checkbox" name="edit[jnsaccurate][]" value="accurate 5 standard edition">ACCURATE 5 Standar Edition</label></div>
                    <?php endif ?>

                    <?php if (in_array("accurate 5 deluxe edition", $myAccurate)): ?>
                        <div class="checkbox"><label><input type="checkbox" name="edit[jnsaccurate][]" value="accurate 5 deluxe edition" checked>ACCURATE 5 Deluxe Edition</label></div>
                    <?php else: ?>
                        <div class="checkbox"><label><input type="checkbox" name="edit[jnsaccurate][]" value="accurate 5 deluxe edition">ACCURATE 5 Deluxe Edition</label></div>
                    <?php endif ?>

                    <?php if (in_array("accurate 5 enterprise edition", $myAccurate)): ?>
                        <div class="checkbox"><label><input type="checkbox" name="edit[jnsaccurate][]" value="accurate 5 enterprise edition" checked>ACCURATE 5 Enterprise Edition</label></div>
                    <?php else: ?>
                        <div class="checkbox"><label><input type="checkbox" name="edit[jnsaccurate][]" value="accurate 5 enterprise edition">ACCURATE 5 Enterprise Edition</label></div>
                    <?php endif ?>

                    <?php if (in_array("accurate cloud", $myAccurate)): ?>
                        <div class="checkbox"><label><input type="checkbox" name="edit[jnsaccurate][]" value="accurate cloud" checked>ACCURATE Online/Cloud</label></div>
                    <?php else: ?>
                        <div class="checkbox"><label><input type="checkbox" name="edit[jnsaccurate][]" value="accurate cloud">ACCURATE Online/Cloud</label></div>
                    <?php endif ?>

                    <?php if (in_array("rene point of sales", $myAccurate)): ?>
                        <div class="checkbox"><label><input type="checkbox" name="edit[jnsaccurate][]" value="rene point of sales" checked>RENE Point Of Sales</label></div>
                    <?php else: ?>
                        <div class="checkbox"><label><input type="checkbox" name="edit[jnsaccurate][]" value="rene point of sales">RENE Point Of Sales</label></div>
                    <?php endif ?>

                    <?php if (in_array("rene dan accurate", $myAccurate)): ?>
                        <div class="checkbox"><label><input type="checkbox" name="edit[jnsaccurate][]" value="rene dan accurate" checked>Gabungan RENE dan ACCURATE</label></div>
                    <?php else: ?>
                        <div class="checkbox"><label><input type="checkbox" name="edit[jnsaccurate][]" value="rene dan accurate">Gabungan RENE dan ACCURATE</label></div>
                    <?php endif ?>

                    <div class="form-inline">
                        <?php 
                            for ($i=0; $i < count($myAccurate); $i++) { 
                                    
                                if ($myAccurate[$i]!='accurate 4 standard edition'&&$myAccurate[$i]!='accurate 4 deluxe edition'&&$myAccurate[$i]!='accurate 4 enterprise edition'&&$myAccurate[$i]!='accurate 5 standard edition'&&$myAccurate[$i]!='accurate 5 deluxe edition'&&$myAccurate[$i]!='accurate 5 enterprise edition'&&$myAccurate[$i]!='accurate cloud'&&$myAccurate[$i]!='rene point of sales'&&$myAccurate[$i]!='rene dan accurate') {
                                    echo "<input type='text' style='width:50%' class='form-control' name='edit[jnsaccurate][]' value='".$myAccurate[$i]."'>";
                                    
                                }
                            }

                        ?>
                    </div>

                </div>
            </div>
        </div>


        <?php $dataPaket = json_decode($pendaftar->fetchPaketTraining()); ?>
        <div class="row">
            <div class="col-md-10">
                <label><a href="input-paket">Nama Paket</a></label>
                <select class="form-control" name="edit[paket]" id="nitem">
                <?php  
                    for ($i=0; $i < count($dataPaket); $i++) { 
                        if ($dataPaket[$i]->id==$datas->id_paket) {
                            echo "<option selected value='".$dataPaket[$i]->id."'>".$dataPaket[$i]->nama_item." -- ".$dataPaket[$i]->price_for." -- ".number_format($dataPaket[$i]->harga)."</value>";
                        }else{
                            echo "<option value='".$dataPaket[$i]->id."'>".$dataPaket[$i]->nama_item." -- ".$dataPaket[$i]->price_for." -- ".number_format($dataPaket[$i]->harga)."</value>";    
                        }
                    }
                ?>
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label for="qty">Qty</label>
                    <input class="form-control" type="number" name="edit[qty]" id="qty" value=<?php echo "'".$datas->qty."'"; ?> placeholder="Jumlah paket yang akan di order">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label for="biayaTransport">Biaya Transport</label>
                    <input class="form-control" type="text" name="edit[transport]" id="biayaTransport" value=<?php echo "'".$datas->biaya_trans."'"; ?> placeholder="Jumlah biaya transport per harian" required>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label for="biayaTransport2">Jumlah Hari Biaya Transport</label>
                    <input class="form-control" type="text" name="edit[qtyTrans]" id="biayaTransport2" value=<?php echo "'".$datas->qty_trans."'"; ?> placeholder="Jumlah biaya transport per harian" required>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label for="tempatbeliaccurate">Tempat Membeli Accurate</label>
                    <input class="form-control" type="text" name="edit[tempatbeli]" id="tempatbeliaccurate" value=<?php echo "'".$datas->tempat_beli_accurate."'"; ?> placeholder="Jika Anda Tahu Tempat Membeli Software Kami">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label for="email">Salesman Accurate</label>
                    <input class="form-control" type="text" name="edit[sales]" id="sales" value=<?php echo "'".$datas->salesman_accurate."'"; ?> placeholder="Jika Anda Tahu Nama Salesman yang Menjual Software Accurate Kepada Anda">
                </div>
            </div>
        </div>

        <div class="row" style="margin-bottom:20px">
            <div class="col-md-10">
                <div class="form-inline">
                    <button class="btn btn-success" id="btn-orderz" style="width:30%;">Edit Order</button>
                    <a href="#" class="btn btn-danger" style="width:30%">Reset</a>
                </div>
            </div>
        </div>
        </form>  



        <?php endif ?>




</div>
<script type="text/javascript" src=<?php echo "'".getBaseUrl()."js/jquery-ui/jquery-ui.js'"; ?>></script>
<script type="text/javascript" src=<?php echo "'".getBaseUrl()."assets/jqnumber/jquery.number.js'"; ?>></script>

<script type="text/javascript">

$(document).ready(function(){
    $('#biayaTransport').number( true, 0 );
});

$('#nitem').change(function(){
        $.ajax({
              type: "GET",
              url: "reverse",
              data: {
                'npkt':$('#nitem').val()
                },
              cache: false,
              success: function(data){
                 $('#biayaTransport2').val($(data).filter('#paketHari').html());
              }
        }); //end ajax 
});

  
</script>

    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCUn6Q6sg0JVJBlgiz4tL2kF1zmVoC0ScE&libraries=places&callback=initMap">
    </script>

</body>
</html> 