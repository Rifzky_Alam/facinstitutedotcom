<?php 
session_start();

include_once '../baseurl.php';
include_once 'session/session-class.php';
include_once 'controller/todo.controller.php';
include_once 'functions/StaffFunc.php';
$session = new Sessionz();
if (isset($_SESSION['admin'])) {
    if ($_SESSION['admin']['tipe']=='admin'){ //superadmin
        if ($_SESSION['admin']['username']=='Imelia'||$_SESSION['admin']['username']=='charlez'||$_SESSION['admin']['username']=='JulianaDAP'||$_SESSION['admin']['username']=='Fajar'||$_SESSION['admin']['username']=='nurrachman.iman@gmail.com') {
        
            include_once HomeDirectory().'model/Petugas.php';
            include_once HomeDirectory().'model/Querybuilder.php';
            include_once HomeDirectory().'model/Invoice.php';
            include_once HomeDirectory().'model/Calendar.php';
            include_once HomeDirectory().'model/page.php';
            $petugas = new ControlPetugas();
            $inv= new Invoice();
            $cal = new Calender();
            $page = new Page();
            $db = new QueryBuilder();

            $bulanlalu = date('m') - 1;
            $tahun = date('Y');
            if ($bulanlalu=='0'){
                $bulanlalu='12';
                $tahun = date('Y') - 1;
            }

            $petugas->setUsername($_SESSION['admin']['username']);

            $data = array(
                'base_url' => getBaseUrl(), //base data
                'url'=> basename(__FILE__, '.php'),
                'judul' => 'Administrasi/FAC Institute',
                'username'=>$_SESSION['admin']['nama'],
                'page' => 'home',
                'petugas'=>$_SESSION['admin']['nama'], // end base data
                'unreportdata'=>$petugas->unreportedData($_SESSION['admin']['username']),
                'my_request'=> $petugas->jumlahDataRequest($_SESSION['admin']['username']),
                'agendaharian' => json_decode($cal->agendaHariIni()),
                'omset_tahun_ini'=> $inv->OmsetTahunan(date('Y')),
                'omset_tahun_lalu'=> $inv->OmsetTahunan(date('Y')-1),
                'inv_this_year'=> $inv->PaidInvoiceByYear(date('Y')),
                'inv_last_year'=> $inv->PaidInvoiceByYear(date('Y')-1),
                'unsent_inv' => $inv->PendingInvoice(),
                'datarequest'=> json_decode($petugas->dataRequest('')),
                'subscribers' => $petugas->FetchSubsribers(),
                'viewers'=> json_decode($page->fetchAll()),
                'unreportedatt' => $petugas->CountBelumAbsen(),
                'fu_data'=> $petugas->CountFollowUPThisMonth()
            );
            $data['unreported']=$data['unreportdata']->jumlah;
            $data['count_subsribers']=count($data['subscribers']);
            $data['count_viewer'] = count($data['viewers']);
            $data['pendingabsen'] = $petugas->GetTotalPendingAbsen();
            $data['todolistdata'] = Todo::ListKerjaan();

            $data = (object) $data;
            include_once HomeDirectory().'administrasi/view/home/view-functions.php';
            include_once HomeDirectory().'administrasi/view/home/view-home-superadmin.php';

        } else { //else not super admin
            include_once HomeDirectory().'model/Petugas.php';
            include_once HomeDirectory().'model/Calendar.php';
            include_once HomeDirectory().'model/page.php';
            include_once HomeDirectory().'model/Querybuilder.php';
            
            $db = new QueryBuilder();
            $page = new Page();
            $petugas = new ControlPetugas();
            $cal = new Calender();
            $petugas->setUsername($_SESSION['admin']['username']);

            $data = array(
                'base_url' => getBaseUrl(), //base data
                'url'=> basename(__FILE__, '.php'),
                'judul' => 'Administrasi/FAC Institute',
                'username'=>$_SESSION['admin']['nama'],
                'page' => 'home',
                'petugas'=>$_SESSION['admin']['nama'], // end base data
                'agendaharian' => json_decode($cal->agendaHariIni()),
                'unreportdata'=>$petugas->unreportedData($_SESSION['admin']['username']),
                'my_request'=> $petugas->jumlahDataRequest($_SESSION['admin']['username']),
                'subscribers'=> $petugas->FetchSubsribers(),
                'unreportedatt' => $petugas->CountBelumAbsen(),
                'viewers'=> json_decode($page->fetchAll()),
                'fu_data'=> $petugas->CountFollowUPThisMonth()
            );
            $data['unreported']=$data['unreportdata']->jumlah;
            $data['count_subsribers']=count($data['subscribers']);
            $data['count_viewer'] = count($data['viewers']);
            $data['pendingabsen'] = $petugas->GetTotalPendingAbsen();
            $data['todolistdata'] = Todo::ListKerjaan();
            $data = (object) $data;
            include_once HomeDirectory().'administrasi/view/home/view-functions.php';
            include_once HomeDirectory().'administrasi/view/home/view-home-admin.php';
        }
    }elseif ($_SESSION['admin']['team']=='3') { // else session is marketing
        include_once HomeDirectory().'model/Petugas.php';
        include_once HomeDirectory().'model/Invoice.php';
        include_once HomeDirectory().'model/Calendar.php';

        $petugas = new ControlPetugas();
        $inv= new Invoice();
        $cal = new Calender();
        $datadasbor = $petugas->NotifDasbor($_SESSION['admin']['username']);
        $bulanlalu = date('m') - 1;
        $tahun = date('Y');
        if ($bulanlalu=='0'){
            $bulanlalu='12';
            $tahun = date('Y') - 1;
        }

        $data = array(
            'base_url' => getBaseUrl(), //base data
            'url'=> basename(__FILE__, '.php'),
            'judul' => 'Administrasi/FAC Institute',
            'username'=>$_SESSION['admin']['nama'],
            'page' => 'home',
            'petugas'=>$_SESSION['admin']['nama'], // end base data
            'agendaharian' => json_decode($cal->agendaHariIni()),
            'datarequest'=> json_decode($petugas->dataRequest('')),
            'subscribers' => $petugas->FetchSubsribers()
        );
        $data['todolistdata'] = Todo::ListKerjaan();
        $data['pengumuman']=$datadasbor['nd_content'];
        $data = (object) $data;
        include_once HomeDirectory().'administrasi/view/home/view-functions.php';
        include_once HomeDirectory().'administrasi/view/home/view-home-marketing.php';
    }elseif ($_SESSION['admin']['team']=='4') { //else session is trainer
        include_once HomeDirectory().'model/Petugas.php';
        include_once HomeDirectory().'model/Calendar.php';
        $petugas = new ControlPetugas();
        $cal = new Calender();
        $petugas->setUsername($_SESSION['admin']['username']);
        $datadasbor = $petugas->NotifDasbor($_SESSION['admin']['username']);
        $data = array(
                'base_url' => getBaseUrl(), //base data
                'url'=> basename(__FILE__, '.php'),
                'judul' => 'Administrasi/FAC Institute',
                'username'=>$_SESSION['admin']['nama'],
                'page' => 'home',
                'petugas'=>$_SESSION['admin']['nama'], // end base data
                'agendaharian' => json_decode($cal->agendaHariIni()),
                'unreportdata'=>$petugas->unreportedData($_SESSION['admin']['username']),
                'unreportedatt' => $petugas->CountBelumAbsen(),
                'my_request'=> $petugas->jumlahDataRequest($_SESSION['admin']['username'])
        );
        $data['unreported']=$data['unreportdata']->jumlah;
        $data['pengumuman']=$datadasbor['nd_content'];
        $data['todolistdata'] = Todo::ListKerjaan();
        $data = (object) $data;
        include_once HomeDirectory().'administrasi/view/main-component/sidebar-staffs.php';
        include_once HomeDirectory().'administrasi/view/home/view-functions.php';
        include_once HomeDirectory().'administrasi/view/home/view-home-trainer.php';
    }elseif ($_SESSION['admin']['team']=='5') { // else session is acc service
        include_once HomeDirectory().'model/Petugas.php';
        include_once HomeDirectory().'model/Calendar.php';
        $petugas = new ControlPetugas();
        $cal = new Calender();
        $petugas->setUsername($_SESSION['admin']['username']);
        $datadasbor = $petugas->NotifDasbor($_SESSION['admin']['username']);
        $data = array(
                'base_url' => getBaseUrl(), //base data
                'url'=> basename(__FILE__, '.php'),
                'judul' => 'Administrasi/FAC Institute',
                'username'=>$_SESSION['admin']['nama'],
                'page' => 'home',
                'petugas'=>$_SESSION['admin']['nama'], // end base data
                'agendaharian' => json_decode($cal->agendaHariIni()),
                'unreportdata'=>$petugas->unreportedData($_SESSION['admin']['username']),
                'unreportedatt' => $petugas->CountBelumAbsen(),
                'my_request'=> $petugas->jumlahDataRequest($_SESSION['admin']['username'])
        );
        $data['unreported']=$data['unreportdata']->jumlah;
        $data['todolistdata'] = Todo::ListKerjaan();
        $data['pengumuman']=$datadasbor['nd_content'];
        $data = (object) $data;
        include_once HomeDirectory().'administrasi/view/main-component/sidebar-staffs.php';
        include_once HomeDirectory().'administrasi/view/home/view-functions.php';
        include_once HomeDirectory().'administrasi/view/home/view-home-accservice.php';

    }elseif ($_SESSION['admin']['team']=='7') { // else session is freelance
        include_once HomeDirectory().'model/Petugas.php';
        include_once HomeDirectory().'model/Calendar.php';
        $petugas = new ControlPetugas();
        $cal = new Calender();
        $petugas->setUsername($_SESSION['admin']['username']);

        $data = array(
                'base_url' => getBaseUrl(), //base data
                'url'=> basename(__FILE__, '.php'),
                'judul' => 'Administrasi/FAC Institute',
                'username'=>$_SESSION['admin']['nama'],
                'page' => 'home',
                'petugas'=>$_SESSION['admin']['nama'], // end base data
                'agendaharian' => json_decode($cal->agendaHariIni()),
                'unreportdata'=>$petugas->unreportedData($_SESSION['admin']['username']),
                'unreportedatt' => $petugas->CountBelumAbsen(),
                'my_request'=> $petugas->jumlahDataRequest($_SESSION['admin']['username'])
        );
        $data['unreported']=$data['unreportdata']->jumlah;

        $data = (object) $data;
        include_once HomeDirectory().'administrasi/view/main-component/sidebar-staffs.php';
        include_once HomeDirectory().'administrasi/view/home/view-functions.php';
        include_once HomeDirectory().'administrasi/view/home/view-home-freelance.php';
    }elseif ($_SESSION['admin']['team']=='8') {
        header('location: https://fac-institute.com/admin-freelance/');
    }
}else { // else user has no session
    header('location: https://fac-institute.com/login/');
}

?>