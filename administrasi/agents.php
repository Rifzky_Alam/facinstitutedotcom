<?php 
session_start();

if (!isset($_SESSION['admin'])&&!$_SESSION['admin']['type']=='admin'){
	header('Location:../index.php');
}
$page='dataPegawai'
?>

<?php 

//include_once 'variables.php';

?>

<!DOCTYPE html>

<html>

<head>
	<title>FAC - Agency</title>
<?php include_once 'header.php'; ?>

</head>

<body>
	<?php include_once 'sidebar.php'; ?>
	<?php include_once 'top-nav.php'; ?>

	<?php 
	include_once '../model/Agents.php';
	$agents = new ControlAgents();
	$dataAgents = json_decode($agents->fetchAllDatas());
	?>
	


	<div class="container" style='margin-bottom:40px;margin-top:40px'>

		<div class='row'>
			<div class='page-header'>
				
            		<h1>Data Agents</h1>
        		
			</div>
		</div>
		<div class='row'>
			<div class='form-inline'>
				<a href=<?php echo "'".basename(__FILE__, '.php')."?k=1'"; ?> title='Tambah Akun baru' class='btn btn-primary'><span class='glyphicon glyphicon-plus'></span></a>
				<a href=<?php echo "'".basename(__FILE__, '.php')."'"; ?> title='Dashboard' class='btn btn-primary'><span class='glyphicon glyphicon-dashboard'></span></a>
				
			</div>
		</div>
		<br><br>
		
			<div class='row'>
				<div class='col-md-12'>
					<table class='table'>
						<thead>
							<tr>
								<th>Username</th>
								<th>No Telepon</th>
								<th>Email</th>
								<th>Rekening</th>
								<th>Link</th>
								<th>Rating</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							for ($i=0; $i < count($dataAgents); $i++) { 
							?>
							<tr>
								<td><?php echo $dataAgents[$i]->username ?></td>
								<td><?php echo $dataAgents[$i]->no_telepon ?></td>
								<td><?php echo $dataAgents[$i]->email ?></td>
								<td><?php echo $dataAgents[$i]->rekening ?></td>
								<td>
								<?php if ($dataAgents[$i]->links==''): ?>
									No link available for this user
									<?php else: ?>
									http://fac-institute.com/order/?u=<?php echo $dataAgents[$i]->links ?>
								<?php endif ?>
								
									

								</td>
								<td><?php echo $dataAgents[$i]->rating ?></td>
								
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>

			<script type="text/javascript">
				$('#')
			</script>


		
	</div>

	    <!-- Modal -->
  <div class='modal fade' id='modal-konfirmasi' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pesan Konfirmasi</h3>

        </div>
        <div class='modal-body'>
        <div style='padding:15px' class='row'>
          
          <div class="col-md-12">
            
            		<div class="row" id='message-body'>
            			Anda Yakin Ingin Menghapus Data <span id='judul-delete' style="font-weight: bold;"></span> ?
            		</div>           		


          </div>
        </div>

        </div>
        <div class='modal-footer'>
        	<div class="row">
        		        <form method='post' action=<?php echo "'".$_SERVER['SCRIPT_NAME']."'"; ?>>
        					<input type='text' name='delete-id' id='konfirm-id' style='display:none' />
        		<div class="col-md-6" style="text-align:left">
							<button class='btn btn-danger'>Ya</button>
						</form>
						<button class='btn btn-info' data-dismiss='modal'>Tidak</button>        			
        		</div>

        		<div class="col-md-6">
        			<div class='form-inline'>
						<span>FAC-Institute 2016</span>
        			</div>
        			
        		</div>
        	</div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->  
<script type="text/javascript" src=<?php echo "'".getBaseUrl()."css/datepicker/js/bootstrap-datepicker.js'"; ?>></script>
<script type="text/javascript">
	function coba(aidi,myTitle){
		//alert(id);
		document.getElementById("judul-delete").innerHTML=myTitle;
		document.getElementById('konfirm-id').value=aidi;
	}

	$('#tglLahir').datepicker({
		   format: "yyyy-mm-dd"
	});
</script>

</body>

</html>