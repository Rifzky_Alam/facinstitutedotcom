<?php 
class Sessionz{
	public function keluar(){
		session_start();
		session_destroy();
		header('location: https://fac-institute.com/login');
	}

	public function SetPage($value){
		$_SESSION['page'] = $value;
	}

	public function OnlyAdmin(){
		if (!isset($_SESSION['admin'])||$_SESSION['admin']['tipe']!='admin') {
			header('location: https://fac-institute.com/administrasi/accessdenied.php');	
		}
	}

	public function OnlyMe($username){
		if (!isset($_SESSION['admin'])||$_SESSION['admin']['username']!=$username) {
			header('location: https://fac-institute.com/administrasi/accessdenied.php');	
		}
	}

	public function AdminMarketing(){
		if (!isset($_SESSION['admin'])||($_SESSION['admin']['tipe']!='admin'&&$_SESSION['admin']['tipe']!='marketing')) {
			header('location: https://fac-institute.com/administrasi/accessdenied.php');
		}
	}

	public function IsSuperUser(){
		if ($_SESSION['admin']['username']!='Imelia'&&$_SESSION['admin']['username']!='charlez'&&$_SESSION['admin']['username']!='JulianaDAP'&&$_SESSION['admin']['username']!='Fajar'&&$_SESSION['admin']['username']!='nurrachman.iman@gmail.com') {
			header('location: https://fac-institute.com/administrasi/accessdenied.php');
		}
	}

	public function Staff(){
		if (!isset($_SESSION['admin'])||empty($_SESSION['admin']['username'])) {
			header('location: https://fac-institute.com/login');
		}
	}

	public function Login(){
		if (isset($_SESSION['admin'])) {
			header('Location: ../administrasi/index.php');
		}
	}

}

?>