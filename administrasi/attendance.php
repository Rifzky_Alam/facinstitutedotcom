<?php 
include_once 'controller/attendance.controller.php';
session_start();
if (isset($_SERVER['PATH_INFO'])) {
	@$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
	$key = array_shift($url_segment);
	switch ($key) {
		case 'edit':
			if (count($url_segment)<1) {
				header('Location: https://fac-institute.com/administrasi/attendance');
			}	
			Attendance::Edit($url_segment[0]);
			break;
		case 'admin':
			Attendance::ListAdmin();
			break;
		case 'hari':
			Attendance::Cekbuatinputlaporan();
			break;
		case 'rejectrequest':
			if (count($url_segment)<1) {
				header('Location: https://fac-institute.com/administrasi/attendance');
			}
			Attendance::RejectRequest($url_segment[0]);
			break;
		case 'createreport':
			
			Attendance::CreateReport($url_segment[0]);

			break;
		case 'request':
			Attendance::RequestAbsenLists();
			break;
		case '':
			Attendance::index();
			break;
	}
}else{
	Attendance::index();
}