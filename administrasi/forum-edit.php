<?php 
session_start();
if (!isset($_SESSION['admin'])||($_SESSION['admin']['tipe']!='admin'&&$_SESSION['admin']['tipe']!='marketing'&&$_SESSION['admin']['tipe']!='staff')) {
    header('Location:../login/index.php');
}
?>

<!DOCTYPE html>
<html>

<?php 
//echo $tipeAdmin=$_SESSION['admin']['tipe'];
$judul='Forum FAC';
$page = 'home';
include_once 'header.php';
include_once '../model/Forum.php';
include_once '../model/page.php';

$myPage = new Page();
$forum = new Forum();
?>

<body>

<?php 

include_once 'sidebar.php';

include_once 'top-nav.php';

 ?>

<div class="container" id="isi">
	
<?php if (isset($_GET['idq'])&&!empty($_GET['idq'])): ?>
    <?php $forum->setAnswer($_GET['idq']); ?>
    <?php $data=$forum->getEditAnswer() ?>
    <?php 
    if ($data->user!=$_SESSION['admin']['username']) {
        echo "<script>alert('Anda tidak memiliki akses halaman ini!')</script>";
        echo "<script>location.replace('forum-table?v=".$data->id_faq."');</script>";
    }
    
    ?>
    <?php 
    if (isset($_POST['ej'])) {
        $forum->setIdAnswer($_GET['idq']);
        $forum->setAnswer($_POST['ej']['jawaban']);
        
        if ($forum->EditAnswer()) {
            echo "<script>alert('Jawaban berhasil diubah!');</script>";
            echo "<script>location.replace('forum-question?v=".$_POST['ej']['vq']."');</script>";
        } else {
            echo "<script>alert('Jawaban belum bisa tersimpan, bila ada masalah harap hubungi amin system!');</script>";
            echo "<script>location.replace('forum-table?v=".$_POST['ej']['vq']."');</script>";
        }
        // print_r($forum->getAnswer());
    }    

    ?>
    
    <div class="page-header" id="top-logo">
		<h3><?php echo $data->title ?></h3>
	</div>



    <div class="row">
        <div class="col-md-10">
            <?php echo $data->question; ?>

            <div>
                <?php $mylist = explode(",", $data->tag); ?>
                <ul class="pagination pagination-sm" style="margin-bottom:0px;">
                <?php for ($i=0; $i < count($mylist); $i++) { ?>
                    <li><a href='#'><?php echo trim($mylist[$i]); ?></a></li>
                <?php } ?>          
                </ul>
            </div>
            <div style="text-align:right;">
                <?php echo $data->nama ?> || <span><?php echo $data->time_post ?></span>
            </div>           
        </div>
    </div>

    <div class="row">
        <div class="col-md-10">
            <hr>
        </div>
    </div>
    <br>

    
    <br>
    <div id="forEdit" class="row">
        
    </div>

    <form action="" method="POST">
    <div class="row" style="overflow:auto;">
        <div class="col-md-10">
            <div class="form-group">
                <label>Jawaban anda</label>
                <textarea name="ej[jawaban]" class="form-control" id="editor1"><?= $data->answer ?></textarea>
                <input type="text" name="ej[vq]" value="<?= $data->id_faq ?>" style="display:none;">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10">
            <button class="btn btn-lg btn-primary">Submit</button>
        </div>
    </div>
    </form>
<?php endif ?>
        

	
</div>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
    <script type="text/javascript">
        CKEDITOR.replace('editor1');
    </script>
</body>
</html> 