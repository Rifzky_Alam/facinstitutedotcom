<?php
include_once '../model/Laporan.php';
include_once '../model/Petugas.php';
include_once '../model/page.php';
include_once '../model/Pendaftar.php';
$pendaftarz = new Pendaftar();
$myPage = new Page();
$ctrlLaporan = new ControlLaporan();
$laporan = new objekLaporan();
$petugas = new ControlPetugas();


if (isset($_GET['npkt'])) {
    $data=$pendaftarz->getPaketByID($_GET['npkt']);

    echo "<div id='nmitem'>".$data->nama_item."</div>";
    echo "<div id='harga'>".$data->harga."</div>";
    echo "<div id='paketHari'>".$data->paket_hari."</div>";

}

if (isset($_POST['jsonData'])){
    $objek = json_encode($_POST['jsonData']);

    if($ctrlLaporan->updateLaporan($objek)){
        $objek = json_decode($objek);
        $myPage->addLogForLogin($objek->username,'successfully updated daily report',$objek->ip);
        echo "Data Berhasil Disimpan";
    }else{
        echo "database error";
    }
}

if (isset($_POST['editalamat'])) {
    $alamat = array(
        'id' => $_POST['editalamat']['id'],
        'alamat' => $_POST['editalamat']['addr'] 
    );
    $petugas->setAlamat($alamat);

    if ($petugas->editAlamat()) {
        echo "Data berhasil disimpan!";
    }else{
        echo "Data gagal disimpan!";
    }    

}

if (isset($_POST['editstalamat'])) {
    $alamat = array(
        'id' => $_POST['editstalamat']['id'],
        'status' => $_POST['editstalamat']['st']
    );
    $petugas->setAlamat($alamat);
    // $data= $petugas->getAlamat();
    // echo $data['id']."<br>";
    // echo $data['alamat'];
    if ($petugas->editStatusAlamat()) {
        echo "Data berhasil disimpan!";
    }else{
        echo "Data gagal disimpan!";
    }
}

if (isset($_POST['addalamat'])) {
    // $objek=json_encode($_POST['addalamat']);
    // $objek=json_decode($objek);
    // echo $_POST['addalamat']['username']."<br>";
    // echo $_POST['addalamat']['alamat']."<br>";
    $petugas->setUsername($_POST['addalamat']['username']);
    $petugas->setAlamat($_POST['addalamat']['alamat']);
    if ($petugas->insertAlamat()) {
        echo "Data berhasil disimpan!";
    }else{
        echo "Data gagal disimpan!";
    }

}

if (isset($_POST['addtelp'])) {
    $petugas->setUsername($_POST['addtelp']['username']);
    $petugas->setTelepon($_POST['addtelp']['telp']);
    if ($petugas->insertTelepon()){
        echo "Data berhasil disimpan!";
    } else {
        echo "Data gagal disimpan, kontak administrator!";
    }
}

if (isset($_POST['etu'])){
    $telepon = array(
        'id'=>$_POST['etu']['id'],
        'telp'=>$_POST['etu']['telepon']
    );

    $petugas->setTelepon($telepon);
    
    if ($petugas->editNoTelepon()){
        echo "Data berhasil disimpan!";
    } else {
        echo "Data gagal disimpan, kontak administrator!";
    }


}

if (isset($_POST['editsttelp'])){
    $telepon = array(
        'id'=>$_POST['editsttelp']['id'],
        'status'=>$_POST['editsttelp']['st']
    );
    $petugas->setTelepon($telepon);
    if ($petugas->editStatusTelp()){
        echo "Data berhasil disimpan!";
    } else {
        echo "Data gagal disimpan, kontak administrator!";
    }

}

if (isset($_POST['tes'])){
    $objek = json_encode($_POST['tes']);
    //$coba = json_decode($objek);
    //echo $coba->username;
    if ($ctrlLaporan->updateLaporanByAdmin($objek)) {
        echo "Data Berhasil disimpan";
    }else{
        echo "Data cant be saved";
    }
}

if (isset($_GET['kda'])&&!empty($_GET['kda'])) {
    $myData = json_decode($pendaftarz->getDaftarAgenda($_GET['kda']));
    
    for ($i=0; $i < count($myData); $i++) { 
        echo "<div class='checkbox mydata' id='z".$myData[$i]->id."'>
              <label id='y".$myData[$i]->id."'><input type='checkbox' id='".$myData[$i]->id."' onclick='coba(this)' value='".$myData[$i]->id."'>".$myData[$i]->nama_agenda."</input></label>
              </div>";
    }
}


if (isset($_GET['kp'])) {
    include_once '../model/Perusahaan.php';
    $usaha = new Perusahaan();

    $data = $usaha->selectByID($_GET['kp']);

    echo "<div id='np'>".$data->nama."</div>";
    echo "<div id='ep'>".$data->email."</div>";
    echo "<div id='ap'>".$data->alamat."</div>";
    echo "<div id='kp'>".$data->kota."</div>";
    echo "<div id='pp'>".$data->provinsi."</div>";
    echo "<div id='tp'>".$data->telepon."</div>";
    echo "<div id='jp'>".$data->jenis_usaha."</div>";
    echo "<div id='kjup'>".$data->ket_jenis_usaha."</div>";
    echo "<div id='mp'>".$data->map."</div>";
}

if (isset($_GET['tescb'])&&!empty($_GET['tescb'])) {
    include_once '../model/Petugas.php';
    $petugas = new ControlPetugas();
    $comboBoxData = json_decode($petugas->getDataPendaftarToday($_GET['tescb']));
    foreach ($comboBoxData as $key ) {
        echo "<option value='$key->id'>$key->perusahaan</option>";
    }
    echo "<option value='f4e745e1015af1c15ebcb70b77f1426c' selected>FAC-Institute</option><option value='d5eb8c090d191587bb8a4b875bae4fed'>FAC-Remote</option>";
    
}


if (isset($_POST['updateAbsen'])){
    include_once '../model/Laporan.php';
    $pelapor = new objekLaporan();
    $controlLaporan = new ControlLaporan();
    
    $objek = json_encode($_POST['updateAbsen']);
    $objek = json_decode($objek);


    //$pelapor->setID($objek->username);
    //$pelapor->setJamMulai($objek->absenMasuk);
    //$pelapor->setJamSelesai($objek->absenPulang);
    //$pelapor->setNamaKantor($objek->namaPerusahaan);

    if ($objek->statusLaporan=='aktif'){
        //echo $objek->namaPerusahaan;
        if ($petugas->updateAbsen($objek)){
            echo "Data Absen Berhasil Diubah";
        }else{
            echo "Data Absen Gagal Diubah";
        }
     }elseif ($objek->statusLaporan=='inactive'){
            
            if ($petugas->updateAbsen($objek)) {
                if ($controlLaporan->insertLaporanForEdit($objek)) {
                    echo "Data Absen Telah diubah dan laporan harian telah terbuat";   
                }else{
                    echo "Error: data laporan belum bisa di buat";
                }
            }else{
                echo "Data Absen Gagal Diubah";
            }

     }
    /*
    if ($petugas->updateAbsen($objek)){
        echo "Data Absen Berhasil Diubah";
    }else{
        echo "Data Absen Gagal Diubah";
    }
    */


    
    //echo $petugas->updateAbsen($objek);
}


if (isset($_POST['inputOrder'])) {
    include_once '../model/Pendaftar.php';
    $ctrlPendaftar = new Pendaftar();
    
    $objek = json_encode($_POST['inputOrder']);
    //$objek = json_encode($myArray);
    $objek = json_decode($objek);
    if ($ctrlPendaftar->masukDataJSON($objek)){
        $my_message="System has detected that ".$objek->petugas." tried to submit datas as shown below:\n
     Company Name   : ".$objek->namaPerusahaan."\n
     Address        : ".$objek->alamatLengkap."\n
     Contact Person : ".$objek->namaPersonal."\n
     Phone Number   : ".$objek->telepon."\n
     On Date        : ".$objek->tglTraining."\n
     Agenda         : ".$objek->agendaTraining."\n\nOrder Form submitted at ".date('H:i:s')."
                    ";

        mail("training@fac-institute.com , admin@fac-institute.com , training.facinstitute@gmail.com , rifzky.mail@gmail.com", "Order Training Accurate From Admin Page", $my_message);
        echo "Data telah tersimpan!";
    }else{
        echo "data failed to save!";
    }
}

if (isset($_POST['onchangeNamaPerusahaan'])) {
    include_once '../model/Pendaftar.php';
    $ctrlPendaftar = new Pendaftar();
    $objek = json_encode($_POST['onchangeNamaPerusahaan']);
    $objek = json_decode($objek);

    $myObject = json_decode($ctrlPendaftar->selectByNamaPerusahaan($objek));

    echo "<div id='alamatLengkap'>".$myObject[0]->alamat_lengkap."</div>";
    echo "<div id='alamat'><a href='https://www.google.co.id/maps/place/".$myObject[0]->alamat_perusahaan."' target='_blank'  >".$myObject[0]->alamat_perusahaan."</a> <input style='display:none' id='linkDetailAlamat' value='".$myObject[0]->alamat_perusahaan."'></div>";
    echo "<div id='emailnya'>".$myObject[0]->email_perusahaan."</div>";
    echo "<div id='jenisUsaha'>".$myObject[0]->jenis_usaha."</div>";
    echo "<div id='ketJenisUsaha'>".$myObject[0]->keterangan_jenis_usaha."</div>";  
}

if (isset($_POST['inputAbsen'])) {
    include_once '../model/Laporan.php';
    $objek = json_encode($_POST['inputAbsen']);
    $objek = json_decode($objek);
    //echo md5($objek->username);
    //$myEmployee = $petugas->selectByID($objek->username);
    /*
    
    echo "<br>";
    echo $objek->username;
    echo "<br>";
    echo $objek->absenMasuk;
    echo "<br>";
    echo $objek->lokasiMasuk;
    echo "<br>";
    echo $objek->tanggal;
    echo "<br>";
    echo $objek->absenPulang;
    echo "<br>";
    echo $objek->lokasiPulang;
    */
    
    if ($petugas->editInputAbsen($objek)) {
        echo "Data Berhasil Disimpan";
        $dataz = $petugas->selectEditAbsen($objek->username, $objek->tanggal);
        $notif="Dengan ini, System menginformasikan bahwa:\n\nNama Pegawai: ".$dataz->nama."\nUntuk Absen Tanggal: ".$objek->tanggal."\n\nAbsen Dilakukan oleh ". $objek->editor." Dengan Detail:\n\nAbsen Masuk: ".$objek->absenMasuk."\nAbsen Pulang: ".$objek->absenPulang."\nNama Perusahaan: ".$dataz->perusahaannya;
        mail('rifzky.mail@gmail.com, fajarfifa@gmail.com', 'Notifikasi Absen Tidak Normal', $notif);
    }else{
        echo "Data pegawai sudah ada pada tanggal tersebut.";
    }
}

if (isset($_POST['editOrder'])) {
    include_once '../model/Pendaftar.php';
    $pendaftar = new Pendaftar();
    
    $objek = json_encode($_POST['editOrder']);
    $objek = json_decode($objek);

    if ($pendaftar->editData($objek)) {
        echo "Data Telah Di ubah";
    }else{
        echo "Kesalahan terjadi, harap hubungi admin system";
    }
}

if (isset($_GET['dp'])) {
    include_once '../model/Perusahaan.php';
    $perusahaan = new Perusahaan();

    $objek = json_encode($_GET['dp']);
    $objek = json_decode($objek);
    //echo $objek->np;
    echo $perusahaan->selectByNamaPerusahaan($objek);
}
?>