<?php 
session_start();
if (!isset($_SESSION['admin'])||empty($_SESSION['admin']['nama'])) {
    header('Location:../login/index.php');
}elseif (!$_SESSION['admin']['tipe']=='marketing'&&!$_SESSION['admin']['tipe']=='admin'){
    header('Location:../login/index.php');
}
?>




<!DOCTYPE html>
<html>

<?php 
//echo $tipeAdmin=$_SESSION['admin']['tipe'];
$page = 'orderTraining';
include_once 'header.php'; 

?>




<body>
<script type="text/javascript">
var latitude;
var longitude

function getLocation(){
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else { 
        alert("Geolocation is not supported by this browser.");
    }
}

function showPosition(position) {
     var myCurrentLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
     return myCurrentLocation;
}

function cobaaa() {
    getLocation();

    return [position.coords.latitude,position.coords.longitude];
}

function showError(error){
    switch(error.code){
        case error.PERMISSION_DENIED:
            //alert("User denied the request for Geolocation.");
            
            var myCurrentLocation = new google.maps.LatLng(-6.248123, 106.907952);
            var mapProp = {
                center:myCurrentLocation,
                zoom:11,
                mapTypeId:google.maps.MapTypeId.ROADMAP
            };

            var input = document.getElementById('alamatz');
            input.value = "";
            var sBox = new google.maps.places.SearchBox(input);
                //marker
            var myMarker=new google.maps.Marker({
                position:myCurrentLocation,
            });


    
            var markers = [];

            markers.push(myMarker);



            var map = new google.maps.Map(document.getElementById("map-canvas"),mapProp);
            var infowindow = new google.maps.InfoWindow({
                content:"FAC Institute Office"
            });
            infowindow.open(map,myMarker);

            myMarker.setMap(map);

            //event listener 

            //event listener for search box
            sBox.addListener('places_changed', function() {
              var places = sBox.getPlaces();
              if (places.length == 0) {
                return;
              };
              deleteMarkers();

              // For each place, get the icon, name and location.
              var bounds = new google.maps.LatLngBounds();
              places.forEach(function(place) {
                var icon = {
                  url: place.icon,
                  size: new google.maps.Size(71, 71),
                  origin: new google.maps.Point(0, 0),
                  anchor: new google.maps.Point(17, 34),
                  scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                markers.push(new google.maps.Marker({
                  map: map,
                  //icon: icon,
                  title: place.name,
                  position: place.geometry.location
                }));

                if (place.geometry.viewport) {
                  // Only geocodes have viewport.
                  bounds.union(place.geometry.viewport);
                } else {
                  bounds.extend(place.geometry.location);
                }
              });
              map.fitBounds(bounds);

            });

            //event listener for google map
            google.maps.event.addListener(map, 'click', function(event) {
            placeMarker(event.latLng);
            });


            //marker

            // Sets the map on all markers in the array.
            function setMapOnAll(map) {
              for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
              }
            }

            function placeMarker(location){
                deleteMarkers();
                var alamatku = document.getElementById('alamatz');
                var myMarker = new google.maps.Marker({
                    position: location, 
                    map: map
                });
                alamatku.value = location;
                var myString = alamatku.value;
                myString=myString.replace('(','');myString=myString.replace(')','');myString=myString.replace(', ',',');

                alamatku.value = myString;
                markers.push(myMarker);
                //alert(markers.length);
            }
            // Deletes all markers in the array by removing references to them.
            function deleteMarkers() {
              clearMarkers();
              markers = [];
            }

            function clearMarkers() {
              setMapOnAll(null);
            }



            break;
        case error.POSITION_UNAVAILABLE:
            x.innerHTML = "Location information is unavailable."
            break;
        case error.TIMEOUT:
            x.innerHTML = "The request to get user location timed out."
            break;
        case error.UNKNOWN_ERROR:
            x.innerHTML = "An unknown error occurred."
            break;
    }
}


function initialize() {



    if (navigator.geolocation) {


        navigator.geolocation.getCurrentPosition(function(position){
            var myCurrentLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
            var mapProp = {
                center:myCurrentLocation,
                zoom:16,
                mapTypeId:google.maps.MapTypeId.ROADMAP
            };

            var input = document.getElementById('alamatz');
            input.value = position.coords.latitude + ',' + position.coords.longitude;
            var sBox = new google.maps.places.SearchBox(input);
                //marker
            var myMarker=new google.maps.Marker({
                position:myCurrentLocation,
            });


    
            var markers = [];

            markers.push(myMarker);



            var map = new google.maps.Map(document.getElementById("map-canvas"),mapProp);
            var infowindow = new google.maps.InfoWindow({
                content:"Anda berada disini"
            });
            infowindow.open(map,myMarker);

            myMarker.setMap(map);

            //event listener 

            //event listener for search box
            sBox.addListener('places_changed', function() {
              var places = sBox.getPlaces();
              if (places.length == 0) {
                return;
              }
              deleteMarkers()

              // For each place, get the icon, name and location.
              var bounds = new google.maps.LatLngBounds();
              places.forEach(function(place) {
                var icon = {
                  url: place.icon,
                  size: new google.maps.Size(71, 71),
                  origin: new google.maps.Point(0, 0),
                  anchor: new google.maps.Point(17, 34),
                  scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                markers.push(new google.maps.Marker({
                  map: map,
                  //icon: icon,
                  title: place.name,
                  position: place.geometry.location
                }));

                if (place.geometry.viewport) {
                  // Only geocodes have viewport.
                  bounds.union(place.geometry.viewport);
                } else {
                  bounds.extend(place.geometry.location);
                }
              });
              map.fitBounds(bounds);

            });

            //event listener for google map
            google.maps.event.addListener(map, 'click', function(event) {
            placeMarker(event.latLng);
            });


            //marker

            // Sets the map on all markers in the array.
            function setMapOnAll(map) {
              for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
              }
            }

            function placeMarker(location){
                deleteMarkers();
                var alamatku = document.getElementById('alamatz');
                var myMarker = new google.maps.Marker({
                    position: location, 
                    map: map
                });
                alamatku.value = location;
                var myString = alamatku.value;
                myString=myString.replace('(','');myString=myString.replace(')','');myString=myString.replace(', ',',');

                alamatku.value = myString;
                markers.push(myMarker);
                //alert(markers.length);
            }
            // Deletes all markers in the array by removing references to them.
            function deleteMarkers() {
              clearMarkers();
              markers = [];
            }

            function clearMarkers() {
              setMapOnAll(null);
            }

            //end marker

      },showError); //end function

    } else { 
        alert("Geolocation is not supported by this browser.");
    }
}
    google.maps.event.addDomListener(window, 'load', initialize);



</script>


<?php 

include_once 'sidebar.php';

include_once 'top-nav.php';

 ?>


<div class="container" id="isi">
    <div class="page-header" id="top-logo">
        <h3>Input Data Order Training</h3>
    </div>

<?php if (isset($_GET['app'])&&$_GET['app']=='7'): ?>
    <?php 
        include_once '../model/Pendaftar.php'; 
        $ctrlPendaftar = new Pendaftar();
        $dataAutoComplete = json_decode($ctrlPendaftar->autoCompleteNamaPerusahaan());
        
    ?>

        <div class="row">
            <div class="col-md-9">
                <div class="form-group">
                    <label for="nama">Nama Perusahaan *</label>
                    
                        <input class="form-control" id="auto-namaPerusahaan" placeholder=''/>


                </div>
            </div>
        </div>

        <div class="row" id='jajal'>
            <div class="col-md-10">
                <label for="tgl-pelaksanaan">Rencana tanggal Pelaksanaan</label>
                <div class="form-inline" >
                        <input type="text" id="tgl-pelaksanaan" name='tanggalPelaksanaan' class="form-control tglPelaksanaan" placeholder="klik disini untuk memilih tanggal">   
                        <button class='btn btn-sm btn-primary' id='tambahTombol'>Tambah Tanggal</button>
                 </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label for="email">Jumlah Hari Training</label>
                    <input class="form-control" type="text" name="jumlah-hari" id="hari-training" placeholder="masukkan angka">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <h4>Versi Accurate</h4>
                    <div class="checkbox"><label><input type="checkbox" name="jns-accurate" value="accurate 4 standard edition">ACCURATE 4 Standar Edition</label></div>
                    <div class="checkbox"><label><input type="checkbox" name="jns-accurate" value="accurate 4 deluxe edition">ACCURATE 4 Deluxe Edition</label></div>
                    <div class="checkbox"><label><input type="checkbox" name="jns-accurate" value="accurate 4 enterprise edition">ACCURATE 4 Enterprise Edition</label></div>
                    <div class="checkbox"><label><input type="checkbox" name="jns-accurate" value="accurate 5 standard edition">ACCURATE 5 Standar Edition</label></div>
                    <div class="checkbox"><label><input type="checkbox" name="jns-accurate" value="accurate 5 deluxe edition">ACCURATE 5 Deluxe Edition</label></div>
                    <div class="checkbox"><label><input type="checkbox" name="jns-accurate" value="accurate 5 enterprise edition">ACCURATE 5 Enterprise Edition</label></div>
                    <div class="checkbox"><label><input type="checkbox" name="jns-accurate" value="accurate cloud">ACCURATE Online/Cloud</label></div>
                    <div class="checkbox"><label><input type="checkbox" name="jns-accurate" value="rene point of sales">RENE Point Of Sales</label></div>
                    <div class="checkbox"><label><input type="checkbox" name="jns-accurate" value="rene dan accurate">Gabungan RENE dan ACCURATE</label></div>
                    <div class="form-inline">
                        <div class="checkbox"><label><input type="checkbox" name="jns-accurate"> Lainnya</label></div>
                        <input type="text" class="form-inline" name="jns-accurate" id="jns-accurateLainnya">
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <h4>Agenda Training</h4>
                    <div class="checkbox"><label><input type="checkbox" name="agenda-training" value="training fitur accurate">Training Fitur ACCURATE</label></div>
                    <div class="checkbox"><label><input type="checkbox" name="agenda-training" value="setup database accurate">Setup Database ACCURATE</label></div>
                    <div class="checkbox"><label><input type="checkbox" name="agenda-training" value="troubleshoot">Trouble Shooting ACCURATE</label></div>
                    <div class="checkbox"><label><input type="checkbox" name="agenda-training" value="review">Review Penggunaan ACCURATE</label></div>
                    <div class="form-inline">
                        <div class="checkbox"><label><input type="checkbox" name="agenda-training" value="lainnya"> Lainnya</label></div>
                        <input type="text" class="form-inline" id="agenda-lainnya">
                    </div>
                </div>
            </div>
        </div>


        <div class='row'>
            <div class='col-md-12'>
                <button id='btnInputDaftar' class='btn btn-lg btn-success'>Submit</button>
            </div>
        </div>


<!-- Modal -->
  <div class='modal fade' id='modal-detail' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pencarian Data</h3>

        </div>
        <div class='modal-body'>
        <div style='padding:15px; height: 300px;' class='row'>
          
          <div class="col-md-12">
            <div class='row'>
                <table class='table table-bordered'>
                    <thead><tr><th colspan="2" style='text-align:center'>Nama Perusahaan</th></tr></thead>
                    <tbody>
                        <tr>
                            <td>Alamat Perusahaan</td><td id="detail-alamat">-</td>
                        </tr>
                        <tr>
                            <td>Email Perusahaan</td><td id="detail-email">-</td>
                        </tr>
                        <tr>
                            <td>Alamat Map</td><td><a id="detail-alamatMap" href="#">tidak ada link</a></td>
                        </tr>
                        <tr>
                            <td>Jenis Usaha</td><td id="detail-jenisUsaha">-</td>
                        </tr>
                        <tr>
                            <td>Keterangan Jenis Usaha</td><td id="detail-ketJenisUsaha">-</td>
                        </tr>
                    </tbody>
                </table>

            </div>
          </div>                            

        </div>
          

        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <div class='form-inline'>
                        <button type='button' id='btnInputDataValid' class='btn btn-success'>Data Valid & Input</button>
                        <button type='button' class='btn btn-warning' data-dismiss='modal'>Batal</button>
                    </div>
                    
                </div>
                <div class="col-md-6">
                    
                </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->  


        

        <script type="text/javascript" src=<?php echo "'".getBaseUrl()."js/jquery-ui/jquery-ui.js'"; ?>></script>
        <script>

              $(function(){

                var nilai = 0;
                var jumlahHariTraining = 1;
                $('#hari-training').val(jumlahHariTraining)
                var availableTags = [
                <?php for ($i=0; $i < count($dataAutoComplete) ; $i++) { 
                    if ($i<count($dataAutoComplete)-1) {
                        echo "'".$dataAutoComplete[$i]->nama_perusahaan."',";
                    } else{
                        echo "'".$dataAutoComplete[$i]->nama_perusahaan."'";
                    };
                    
                } ?>
                ];
                $( "#auto-namaPerusahaan" ).autocomplete({
                  source: availableTags
                });
                $('#tgl-pelaksanaan').datepicker({
                    dateFormat: 'yy-mm-dd'
                });


                $('#tambahTombol').click(function(){
                    jumlahHariTraining +=1;
                $('#hari-training').val(jumlahHariTraining);


                    var iseng = "tambahan" + nilai;
                    if ($('#tambahan').length) {

                        if (nilai==0) {
                            $('#tambahan').after("<div class='row' id='"+iseng+"'><div class='col-md-10'><div class='form-inline'><input name='tanggalPelaksanaan' class='form-control tglPelaksanaan'></div></div></div>");
                        }else{
                            var okeh = nilai - 1;
                            var iseng = "tambahan" + okeh;
                            $("#"+iseng).after("<div class='row' id='tambahan"+nilai+"'><div class='col-md-10'><div class='form-inline'><input name='tanggalPelaksanaan' class='form-control tglPelaksanaan'></div></div></div>");
                        };
                            nilai +=1;
                         } else{
                        $('#jajal').after("<div class='row' id='tambahan'><div class='col-md-10'><div class='form-inline'><input name='tanggalPelaksanaan' class='form-control tglPelaksanaan'></div></div></div>");
                    };

                    $('.tglPelaksanaan').datepicker({
                        dateFormat: 'yy-mm-dd'
                    });
                });//end tambah tombol


                   $('#btnInputDataValid').click(function(){ 
                        var namaPerusahaan = $('#auto-namaPerusahaan').val();
                        //alert(namaPerusahaan);
                        var tanggalPelaksanaan = $("input[name=tanggalPelaksanaan]").map(function () {return this.value;}).get().join(" # ");
                        //alert(tanggalPelaksanaan);
                        var jmlHariTraining = $('#hari-training').val();
                        //alert(jmlHariTraining);
                        var versiAccurate = $("input[name=jns-accurate]:checked").map(function () {return this.value;}).get().join(" # ") + '#' + $('#jns-accurateLainnya').val();
                        //alert(versiAccurate);
                        var agendaTraining = $("input[name=agenda-training]:checked").map(function () {return this.value;}).get().join(" # ") + ' # ' + $('#agenda-lainnya').val();
                        //alert(agendaTraining);
                        var alamatPerusahaan = $('#detail-alamat').html();
                        //alert(alamatPerusahaan);
                        var emailPerusahaan = $('#detail-email').html();
                        //alert(emailPerusahaan);
                        var alamatMap = $('#linkDetailAlamat').val();
                        //alert(alamatMap);
                        var jenisUsaha = $('#detail-jenisUsaha').html();
                        //alert(jenisUsaha);
                        var ketJenisUsaha= $('#detail-ketJenisUsaha').html();
                        //alert(ketJenisUsaha);
                        var dataValue ={
                                    'namaPerusahaan':namaPerusahaan, 
                                    'namaPersonal':'-',
                                    'alamat':alamatMap,
                                    'telepon':'-',
                                    'teleponKantor':'-',
                                    'email':'-',
                                    'emailKantor':emailPerusahaan,
                                    'jabatan':'-',
                                    'jnsUsaha':jenisUsaha,
                                    'ketJnsUsaha': ketJenisUsaha,
                                    'jnsPengguna':'-',
                                    'versiAccurate':versiAccurate,
                                    'agendaTraining':agendaTraining,
                                    'tglTraining':tanggalPelaksanaan,
                                    'tempatBeli':'-',
                                    'salesman':'-',
                                    'alamatLengkap':alamatPerusahaan,
                                    'hariTraining':jmlHariTraining,
                                    'petugas': <?php echo "'". $_SESSION['admin']['nama'] ."'"; ?>
                        };

                        if (namaPerusahaan=='') {
                            alert('Nama Perusahaan Harap di isi');
                            return;
                        }else if(alamatMap==''){
                            alert('Alamat Map Harap di isi');
                            return;
                        }else if(emailPerusahaan==''){
                            alert('Alamat Map Harap di isi');
                            return;
                        }else if(jenisUsaha==''){
                            alert('Jenis Usaha Harap di isi');
                            return;
                        }else if(versiAccurate==''){
                            alert('Versi Accurate Harap di isi');
                            return;
                        }else if(agendaTraining==''){
                            alert('Agenda Training Harap di isi');
                            return;
                        }else if(tanggalPelaksanaan==''){
                            alert('tanggalPelaksanaan Harap di isi');
                            return;
                        };

                        //alert('end of function here!')

                        $.ajax({
                          type: "POST",
                          url: "reverse.php",
                          data: {'inputOrder':dataValue},
                          cache: false,
                          success: function(data){
                             alert(data);
                             window.location.replace(<?php echo "'".$_SERVER['SCRIPT_NAME']."'"; ?>);
                          }
                        }); //end ajax

                        $('#modal-detail').modal('hide');
                    });


                   $('#btnInputDaftar').click(function(){
                        var namaPerusahaan = $('#auto-namaPerusahaan').val();
                        var dataValue ={'namaPerusahaan':namaPerusahaan};


                        $('#btnInputDaftar').attr("disabled", true);
                        
                        $.ajax({
                          type: "POST",
                          url: "reverse.php",
                          data: {'onchangeNamaPerusahaan':dataValue},
                          cache: false,
                          success: function(data){
                            
                              var alamatLengkapz = $(data).filter('#alamatLengkap').html();
                              var alamatMap = $(data).filter('#alamat').html();
                              var emailz = $(data).filter('#emailnya').html();
                              var jenisUsahaa = $(data).filter('#jenisUsaha').html();
                              var keteranganJenisUsaha = $(data).filter('#ketJenisUsaha').html();

                              $('#detail-alamat').html(alamatLengkapz);
                              $('#detail-alamatMap').html(alamatMap);
                              $('#detail-email').html(emailz);
                              $('#detail-jenisUsaha').html(jenisUsahaa);
                              $('#detail-ketJenisUsaha').html(keteranganJenisUsaha);
                            $('#btnInputDaftar').removeAttr("disabled");
                             $('#modal-detail').modal('show');
                             //alert(data);
                          },
                          error: function(){
                            $('#btnInputDaftar').removeAttr("disabled");
                          }
                        }); //end ajax
                        //alert(namaPerusahaan);
                        
                    });

              });


            
          </script>


        <!--start normal form-->
    <?php else: ?>

    <div class='row' style='text-align:right;margin-right:10px'>
        <a href=<?php echo "'".$_SERVER['SCRIPT_NAME']."?app=7'"; ?>>Input dari data sebelumnya</a>
    </div>
    
    <?php 
        
        include_once '../model/Petugas.php'; 
        $petugas = new ControlPetugas();
        $myRecord = $petugas->selectByID($_SESSION['admin']['username']);


    ?>

        <div class="row">
            <div class="col-md-9">
                <div class="form-group">
                    <label for="nama">Nama Perusahaan *</label>
                    <input class="form-control" name="nama" id="form-namaPerusahaan" placeholder=''/>
                    
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label for="form-namaPersonal">Nama Personal Pendaftar</label>
                    <input class="form-control" name="namaPersonal" id="form-namaPersonal" placeholder=''>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label for="te">Telepon Kantor *</label>
                    <input class="form-control" name="instansi" id="form-teleponKantor" placeholder=''>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label for="email">E-mail Kantor *</label>
                    <input class="form-control" type="email" name="email" id="form-emailKantor" placeholder=''>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label for="alamat-lengkap">Alamat Lengkap Kantor *</label>
                    <textarea id="alamat-lengkap" placeholder="Contoh: Jl. Jatiwaringin No.8 Ruko Bakso Laguna Lt.2 Kalimalang Kota Bekasi" class="form-control" rows="7"></textarea>
                </div>
            </div>
        </div>

        <div class='row'>
            <div class='col-md-10'>
                <div class='form-group'>
                    <label for='kota'>Kota</label>
                    <input class='form-control' type='text' name='myKota' id='kotaku'>
                </div>
            </div>
        </div>

        <div class='row'>
            <div class='col-md-10'>
                <div class='form-group'>
                    <label for='provinsiku'>Provinsi</label>
                    <select name='myProv' id='provinsiku' class='form-control'>
                        <option value='aceh'>Nanggroe Aceh Darussalam</option>
                        <option value='sumatera utara'>Sumatera Utara</option>
                        <option value='riau'>Riau</option>
                        <option value='kepulauan riau'>Kepulauan Riau</option>
                        <option value='sumatera barat'>Sumatera Barat</option>
                        <option value='jambi'>Jambi</option>
                        <option value='bengkulu'>Bengkulu</option>
                        <option value='bangka belitung'>Bangka Belitung</option>
                        <option value='sumatera selatan'>Sumatera Selatan</option>
                        <option value='lampung'>Lampung</option>

                        <option value='banten'>Banten</option>
                        <option value='jawa barat'>Jawa Barat</option>
                        <option value='jakarta' selected>Jakarta</option>
                        <option value='jawa tengah'>Jawa tengah</option>
                        <option value='jogjakarta'>Jogjakarta</option>
                        <option value='jawa timur'>Jawa Timur</option>

                        <option value='bali'>Bali</option>
                        <option value='ntb'>Nusa tenggara Barat</option>
                        
                        <option value='kalbar'>Kalimantan Barat</option>
                        <option value='kaltim'>Kalimantan Timur</option>
                        <option value='kalteng'>Kalimantan Tengah</option>
                        <option value='kalsel'>Kalimantan Selatan</option>
                        <option value='kalut'>Kalimantan Utara</option>

                        <option value='sulbar'>Sulawesi Barat</option>
                        <option value='sulsel'>Sulawesi Selatan</option>
                        <option value='sulteng'>Sulawesi Tengah</option>
                        <option value='sultra'>Sulawesi Tenggara</option>
                        <option value='gorontalo'>Gorontalo</option>
                        <option value='sulut'>Sulawesi Utara</option>

                        <option value='maluku utara'>Maluku Utara</option>
                        <option value='maluku'>Maluku</option>

                        <option value='papua barat'>Papua Barat</option>
                        <option value='papua'>Papua</option>
                        
                    </select>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label for="telepon">Telepon Personal</label>
                    <input class="form-control" name="telepon" id="form-teleponCP" placeholder=''>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label for="email">E-mail Personal Kontak</label>
                    <input class="form-control" type="email" name="emailCP" id="form-emailCP" placeholder=''>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label for="email">Jabatan Personal Kontak</label>
                    <input class="form-control" type="text" name="jabatanCP" id="form-jabatanCP" placeholder=''>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <h4>Jenis Usaha</h4>
                    <div class="checkbox">
                        <label><input type="checkbox" name="jns-usaha" value="perdagangan">Perdagangan</label>
                    </div>
                    <div class="checkbox"><label><input type="checkbox" name="jns-usaha" value="jasa">Jasa</label></div>
                    <div class="checkbox"><label><input type="checkbox" name="jns-usaha" value="konstruksi">Konstruksi</label></div>
                    <div class="checkbox"><label><input type="checkbox" name="jns-usaha" value="pabrikasi">Pabrikasi</label></div>
                    <div class="form-inline">
                        <div class="checkbox"><label><input type="checkbox" value="lainnya"> Lainnya</label></div>
                        <input type="text" class="form-inline" name="jns-usaha" id="jns-usahaLainnya" placeholder=''>
                    </div>
                    
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label for="ket-jenisUsaha">Keterangan Jenis Usaha</label>
                    <textarea id="ket-jenisUsaha" placeholder="Sebutkan secara spesifik produk atau jasa yang Anda jual agar kami bisa menyiapkan materi training yang sesuai dengan jenis usaha Anda" class="form-control" rows="7"></textarea>
                </div>
            </div>
        </div>

        <div class="row" id='jajal'>
            <div class="col-md-10">
                <label for="tgl-pelaksanaan">Rencana tanggal Pelaksanaan</label>
                <div class="form-inline" >
                        <input type="text" id="tgl-pelaksanaan" name='tanggalPelaksanaan'  class="form-control tglPelaksanaan" placeholder="klik disini untuk memilih tanggal">   
                        <button class='btn btn-sm btn-primary' id='tambahTombol'>Tambah Tanggal</button>
                 </div>
            </div>
        </div>

        <br>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label for="email">Jumlah Hari Training</label>
                    <input class="form-control" type="text" name="jumlah-hari" id="hari-training" placeholder="masukkan angka">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <h4>Jenis Pengguna</h4>
                    <div class="radio"><label><input type="radio" name="jns-pengguna" value="baru">Baru</label></div>
                    <div class="radio"><label><input type="radio" name="jns-pengguna" value="lama">Lama</label></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <h4>Versi Accurate</h4>
                    <div class="checkbox"><label><input type="checkbox" name="jns-accurate" value="accurate 4 standard edition">ACCURATE 4 Standar Edition</label></div>
                    <div class="checkbox"><label><input type="checkbox" name="jns-accurate" value="accurate 4 deluxe edition">ACCURATE 4 Deluxe Edition</label></div>
                    <div class="checkbox"><label><input type="checkbox" name="jns-accurate" value="accurate 4 enterprise edition">ACCURATE 4 Enterprise Edition</label></div>
                    <div class="checkbox"><label><input type="checkbox" name="jns-accurate" value="accurate 5 standard edition">ACCURATE 5 Standar Edition</label></div>
                    <div class="checkbox"><label><input type="checkbox" name="jns-accurate" value="accurate 5 deluxe edition">ACCURATE 5 Deluxe Edition</label></div>
                    <div class="checkbox"><label><input type="checkbox" name="jns-accurate" value="accurate 5 enterprise edition">ACCURATE 5 Enterprise Edition</label></div>
                    <div class="checkbox"><label><input type="checkbox" name="jns-accurate" value="accurate cloud">ACCURATE Online/Cloud</label></div>
                    <div class="checkbox"><label><input type="checkbox" name="jns-accurate" value="rene point of sales">RENE Point Of Sales</label></div>
                    <div class="checkbox"><label><input type="checkbox" name="jns-accurate" value="rene dan accurate">Gabungan RENE dan ACCURATE</label></div>
                    <div class="form-inline">
                        <div class="checkbox"><label><input type="checkbox" name="jns-accurate"> Lainnya</label></div>
                        <input type="text" class="form-inline" name="jns-accurate" id="jns-accurateLainnya">
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <h4>Agenda Training</h4>
                    <div class="checkbox"><label><input type="checkbox" name="agenda-training" value="training fitur accurate">Training Fitur ACCURATE</label></div>
                    <div class="checkbox"><label><input type="checkbox" name="agenda-training" value="setup database accurate">Setup Database ACCURATE</label></div>
                    <div class="checkbox"><label><input type="checkbox" name="agenda-training" value="troubleshoot">Trouble Shooting ACCURATE</label></div>
                    <div class="checkbox"><label><input type="checkbox" name="agenda-training" value="review">Review Penggunaan ACCURATE</label></div>
                    <div class="form-inline">
                        <div class="checkbox"><label><input type="checkbox" name="agenda-training" value="lainnya"> Lainnya</label></div>
                        <input type="text" class="form-inline" id="agenda-lainnya">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label for="email">Tempat Membeli Accurate</label>
                    <input class="form-control" type="text" name="tempat-beli" id="tempat-beli" placeholder="Jika Anda Tahu Tempat Membeli Software Kami">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label for="email">Salesman Accurate</label>
                    <input class="form-control" type="text" name="tempat-beli" id="sales" placeholder="Jika Anda Tahu Nama Salesman yang Menjual Software Accurate Kepada Anda">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label for="alamat">Lokasi Training / Tempat Bertemu</label>
                    <input type="text" name="alamat" id="alamatz" class="form-control" placeholder="Cari Nama Tempat atau Tandai Peta di Bawah ini">
                    <!--<textarea class="form-control" id="alamatz" name="alamat"></textarea>-->
                </div>
            </div>
        </div>

        <div class="row" style="margin-bottom:20px">
            <div class="col-md-10">
                    <div id="map-canvas" style="width:100%;height:400px;border:1px solid"></div>
            </div>
        </div>


        <div class="row" style="margin-bottom:20px">
            <div class="col-md-10">
                <div class="form-inline">
                    <button class="btn btn-success" id="btn-orderz" style="width:30%;">Order</button>
                    <a href="" class="btn btn-danger" style="width:30%">Reset</a>
                </div>
            </div>
        </div>

</div>
<script type="text/javascript" src=<?php echo "'".getBaseUrl()."js/jquery-ui/jquery-ui.js'"; ?>></script>
<script type="text/javascript">



$(document).ready(function(){
var nilai = 0;
var jumlahHariTraining = 1;

$('#hari-training').val(jumlahHariTraining);
$('.tglPelaksanaan').datepicker({
    dateFormat: 'yy-mm-dd'
});


$('#tambahTombol').click(function(){
    jumlahHariTraining +=1;
$('#hari-training').val(jumlahHariTraining);


    var iseng = "tambahan" + nilai;
    if ($('#tambahan').length){

        if (nilai==0) {
            $('#tambahan').after("<div class='row' id='"+iseng+"'><div class='col-md-10'><div class='form-inline'><input name='tanggalPelaksanaan' class='form-control tglPelaksanaan'></div></div></div>");
        }else{
            var okeh = nilai - 1;
            var iseng = "tambahan" + okeh;
            $("#"+iseng).after("<div class='row' id='tambahan"+nilai+"'><div class='col-md-10'><div class='form-inline'><input name='tanggalPelaksanaan' class='form-control tglPelaksanaan'></div></div></div>");
        };
            nilai +=1;
         } else{
        $('#jajal').after("<div class='row' id='tambahan'><div class='col-md-10'><div class='form-inline'><input name='tanggalPelaksanaan' class='form-control tglPelaksanaan'></div></div></div>");
    };

    $('.tglPelaksanaan').datepicker({
        dateFormat: 'yy-mm-dd'
    });
});

$('#btn-orderz').click(function(){
    //alert('oke');
var namaPerusahaan = $('#form-namaPerusahaan').val();
var namaPersonal = $('#form-namaPersonal').val();
var teleponKantor = $('#form-teleponKantor').val();
var telepon = $('#form-teleponCP').val();
var email = $('#form-emailCP').val();
var emailKantor = $('#form-emailKantor').val();
var alamat = $('#alamatz').val();
var kota = $('#kotaku').val();
var provinsi = $('#provinsiku').val();
var alamatLengkap =$('#alamat-lengkap').val();
var jabatan = $('#form-jabatanCP').val();
var jenisUsaha = $("input[name=jns-usaha]:checked").map(function () {return this.value;}).get().join(" # ");
//alert(jenisUsaha);
if ($('#jns-usahaLainnya').val()!='') {
  if (jenisUsaha=='') {
    jenisUsaha = $('#jns-usahaLainnya').val();
  }else{
    jenisUsaha = $("input[name=jns-usaha]:checked").map(function () {return this.value;}).get().join(" # ") + ' # ' + $('#jns-usahaLainnya').val();
  };
};
//alert(jenisUsaha);

var ketJenisUsaha = $('#ket-jenisUsaha').val();
var tanggalTraining = $("input[name=tanggalPelaksanaan]").map(function () {return this.value;}).get().join(" # ");//$('#tgl-pelaksanaan').val();
//alert(tanggalTraining);
var lamaTraining = $('#hari-training').val();
var jenisAccurate = $("input[name=jns-accurate]:checked").map(function () {return this.value;}).get().join(" # ");

if ($('#jns-accurateLainnya').val()!=''){
    if (jenisAccurate==''){
        jenisAccurate = $('#jns-accurateLainnya').val();
    } else{
        jenisAccurate = $("input[name=jns-accurate]:checked").map(function () {return this.value;}).get().join(" # ") + ' # ' + $('#jns-accurateLainnya').val();
    };
};
//alert(jenisAccurate);

var jenisPengguna = $('input[name=jns-pengguna]:checked').val();
var agendaTraining = $("input[name=agenda-training]:checked").map(function () {return this.value;}).get().join(" # ");
if ($('#agenda-lainnya').val()!=''){
      if (agendaTraining==''){
        agendaTraining = $('#agenda-lainnya').val();
      }else{
        agendaTraining = $("input[name=agenda-training]:checked").map(function () {return this.value;}).get().join(" # ") + ' # ' + $('#agenda-lainnya').val();
      };
};
//alert(agendaTraining);

var tempatBeli = $('#tempat-beli').val();
var sales = $('#sales').val();


if (namaPerusahaan=='') {
    alert('Nama Perusahaan Harap Diisi');
    return;
}else if(namaPersonal==''){
    alert('Nama Personal harus diisi');
    return; 
}else if(teleponKantor=='' || telepon == '' ){
    alert('Telepon Kantor atau C.P Harap Diisi');
    return;
}else if(alamat==''){
    alert('Alamat Map Harap Diisi');
    return;
}else if(alamatLengkap==''){
  alert('Alamat Lengkap Harap Diisi');
  return;
}else if(kota==''){
  alert('Detail kota harap di isi');
  return;
}else if(provinsi==''){
  alert('Detail provinsi Harap Diisi');
  return;  
}else if(jenisUsaha==''){
  alert('Jenis Usaha Harap Diisi');
  return;
}else if(tanggalTraining==''){
  alert('Tanggal Training Harap Diisi');
  return;
}else if(lamaTraining==''){
  alert('Durasi Training Harap Diisi');
  return;
}else if(jenisAccurate==''){
  alert('Jenis Software Harap Diisi');
  return;
}else if(jenisPengguna==''){
  alert('Jenis Pengguna Harap Diisi');
  return;
}else if(agendaTraining==''){
  alert('Agenda Training Harap Diisi');
  return;
};
//$("#btn-orderz").attr("disabled", true);
//alert('nama perusahaan: '+ namaPerusahaan);
//alert('nama personal: '+ namaPersonal);
//alert('telepon kantor: '+ teleponKantor);
//alert('alamat: '+ alamat);
//alert('alamat lengkap: '+ alamatLengkap);
//alert('jenis usaha: ' + jenisUsaha);
//alert('tanggal training: ' + tanggalTraining);
//alert('lama training: ' + lamaTraining);
//alert('jenis accurate: ' + jenisAccurate);
//alert('jenis pengguna: ' + jenisPengguna);
//alert('agenda training: ' + agendaTraining);
//alert('tempat beli: ' + tempatBeli);
//alert('sales: ' + sales);

var dataValue ={
            'namaPerusahaan':namaPerusahaan, 
            'namaPersonal':namaPersonal,
            'emailKantor':emailKantor,
            'email':email,
            'alamat':alamat,
            'kota':kota,
            'provinsi': provinsi,
            'alamatLengkap':alamatLengkap,
            'telepon':telepon,
            'teleponKantor':teleponKantor,
            'jabatan':jabatan,
            'jnsUsaha':jenisUsaha,
            'ketJnsUsaha': ketJenisUsaha,
            'tglTraining':tanggalTraining,
            'agendaTraining':agendaTraining,
            'hariTraining':lamaTraining,
            'jnsPengguna':jenisPengguna,
            'versiAccurate':jenisAccurate,
            'tempatBeli':tempatBeli,
            'salesman':sales,
            'petugas': <?php echo "'". $_SESSION['admin']['nama'] ."'"; ?>
};
$('#btn-orderz').prop('disabled', true);
$("#btnSubmit").html('Harap Tunggu ...');
//alert('end of function here!')

$.ajax({
  type: "POST",
  url: "reverse.php",
  data: {'inputOrder':dataValue},
  cache: false,
  success: function(data){
     alert(data);
     window.location.replace(<?php echo "'".$_SERVER['SCRIPT_NAME']."'"; ?>);
  }
}); //end ajax

//alert(namaPerusahaan + " " + teleponKantor + " " + telepon + " " + email + " " + emailKantor + " " + alamat + " " + jabatan + " " + jenisUsaha);
 //alert(tanggalTraining + " " + lamaTraining);
}); //end function click

});


  
</script>
<?php endif ?>



</body>
</html> 