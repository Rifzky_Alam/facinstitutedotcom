<?php 
session_start();
if (!isset($_SESSION['admin'])) {
    header('Location:../login/index.php');
}
?>




<!DOCTYPE html>
<html>

<?php 
$page = 'home';
include_once 'header.php'; 

?>

<body>



<?php 

include_once 'sidebar.php';

include_once 'top-nav.php';

 ?>


<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3>Permintaan Order Client</h3>
	</div>
	<?php 
		
		include_once '../model/Pendaftar.php'; 
        include_once '../model/page.php';
        $myPage = new Page();
		$pendaftar = new Pendaftar();
		//$myRecord = $petugas->selectByID($_SESSION['admin']['username']);
        $datas=json_decode($pendaftar->fetchDataRequest(1));
        // print_r($datas);
	?>


    <?php if (isset($_GET['app'])&&$_GET['app']=='request'): ?>
        
    <?php else: ?>

 <?php for ($i=0; $i < count($datas); $i++) { ?>
        <div class='row'>
            <div class='col-md-12'>
            <div class='panel panel-primary'>
                <div class='panel-heading'>
                    <h3 class='panel-title'>
                    	<?php $dataPerusahaan = json_decode($datas[$i]->nama_perusahaan);$dataJadwal=json_decode($datas[$i]->data_jadwal) ?>
                        <?php echo @$dataPerusahaan->nama.' -- '.$datas[$i]->nama_personal.'  ' ?> 
                        <button class='btn btn-info glyphicon glyphicon-chevron-down' data-toggle='collapse' data-target=<?php echo "'#".$datas[$i]->id."'"; ?> style='margin-left:5px'></button>
                    </h3>
                </div>
                <div class='panel-body collapse' <?php echo 'id="'.$datas[$i]->id.'"' ?> style='overflow-x:auto'>
                    <table class='table table-bordered'>
                        <thead>
                            <tr>
                                <th>Nama Pendaftar</th>
                                <th>Alamat</th>
                                <th>Jenis Usaha</th>
                                <th>Jabatan Pendaftar</th>
                                <th>Email</th>
                                <th>Pengguna</th>
                                <th>Versi Accurate</th>
                                <th>Agenda</th>
                                <th>Tanggal Pelaksanaan</th>
                                <th>Waktu</th>
                                <th>Tempat Beli Accurate</th>
                                <th>Sales Accurate</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><?php echo $datas[$i]->nama_personal ?></td>
                                <td><?php echo $dataPerusahaan->alamat ?></td>
                                <td><?php echo $dataPerusahaan->usaha ?></td>
                                <td><?php echo $datas[$i]->jabatan ?></td>
                                <td><?php echo $datas[$i]->email_kontak ?></td>
                                <td><?php echo $datas[$i]->jenis_pengguna ?></td>
                                <td><?php echo $datas[$i]->versi_accurate ?></td>
                                <td><?php echo $datas[$i]->agenda_training ?></td>
                                <td><?php echo $dataJadwal->tanggal ?></td>
                                <td><?php echo $dataJadwal->waktu ?></td>
                                <td><?php echo @$datas[$i]->tempat_beli_accurate ?></td>
                                <td><?php echo $datas[$i]->salesman_accurate ?></td>
                            </tr>
                        </tbody>
                    </table>
                    <br>
                    <div class='form-inline'>
                        <a href=<?php echo "'".getBaseUrl().'administrasi/penawaran?app=prepare&id='.$datas[$i]->id."'";?> class='btn btn-warning' target="_blank">Buat Invoice</a>
                        <a href=<?php echo "'".getBaseUrl().'administrasi/penawaran?app=preview&id='.$datas[$i]->id."'";?> class='btn btn-danger' target="_blank">Preview Email</a>
                        <a href=<?php echo "'".getBaseUrl().'docs/invoice-baru?id='.$datas[$i]->id."'";?> class='btn btn-primary' target="_blank">Preview Invoice</a>
                        <a href=<?php echo "'".getBaseUrl().'administrasi/penawaran?app=edit&id='.$datas[$i]->id."'";?> class='btn btn-warning' target="_blank">Buat Jadwal</a>
                        <a href="#" class='btn btn-success'>Input ke Perusahaan</a>
                        <a href=<?php echo "'tel://".$datas[$i]->telepon."'"; ?> class='btn btn-default' ><?php echo $datas[$i]->telepon ?></a>
                    </div>  
                    
                </div>
                </div>
            </div>
        </div>

    <?php } ?>

    <?php endif ?>

   

    

</div><!--end container-->
</body>
</html> 