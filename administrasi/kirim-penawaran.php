<?php 
session_start();
if (!isset($_SESSION['admin'])||empty($_SESSION['admin']['username'])||($_SESSION['admin']['tipe']!='admin'&&$_SESSION['admin']['tipe']!='marketing')) {
    header('Location:../login/index.php');
}
?>




<!DOCTYPE html>
<html>

<?php 
//echo $tipeAdmin=$_SESSION['admin']['tipe'];
$judul='Kirim Penawaran - FAC Institute';
$page = 'home';
include_once 'header.php'; 

?>

<body>



<?php 

include_once 'sidebar.php';

include_once 'top-nav.php';

 ?>


<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3>Kirim Penawaran</h3>
	</div>
	<?php 
		include_once '../model/Petugas.php';
        include_once '../model/Penawaran.php';  
        include_once '../model/page.php';
        $myPage = new Page();
		$petugas = new ControlPetugas();
		$myRecord = $petugas->selectByID($_SESSION['admin']['username']);
        $penawaran = new Penawaran();
        $nomor=$penawaran->getNomorPenawaran();

        

	?>

    <?php if (isset($_GET['ac'])&&$_GET['ac']=='attachment'): ?>
            <form id='my-form' action="dompdf/penawaran-baru?attachment=true" method='post'>
        <?php else: ?>
            <form id='my-form' action="dompdf/penawaran-baru" method='post'> 
    <?php endif ?>
    

    <div class='row'>
        <div class='form-group col-sm-8'>
            <label>Nama Perusahaan</label>
            <input type='text' class='form-control' id='txt-passLama' name='dataz[np]' required>
        </div>
    </div>

    <div class='row'>
        <div class='form-group col-sm-8'>
            <label>Nama Personal Kontak</label>
            <input type='text' class='form-control' id='txt-passBaru' name='dataz[cp]' required>
        </div>
    </div>

    <div class='row'>
        <div class='form-group col-sm-8'>
            <label>Email Perusahaan/Personal Kontak</label>
            <input type='email' class='form-control' id='txt-passBaru' name='dataz[email_kontak]' required>
        </div>
    </div>

    <div class='row'>
        <div class='form-group col-sm-8'>
            <label>CC</label>
            <input type='text' class='form-control' id='txt-passBaru' name='dataz[cc]' placeholder="pisahkan dengan space">
        </div>
    </div>

    <div class='row'>
        <div class='form-group col-sm-8'>
            <label>Penjualan</label>
            <select name="dataz[marketer]" class="form-control" required>
                <option value="mm">ABC MM</option>
                <option value="mgk">MGK</option>
                <option value="semanggi">Semanggi</option>
                <option value="mag">MAG</option>
                <option value="web">Website</option>
            </select>
        </div>
    </div>

    <div class="row">
    	<div class="form-group col-sm-8">
    		<label>Biaya Transport</label>
    		<select id="myTransport" class="form-control" name="dataz[biayatrans]">
    			<option value="" selected>--Tidak ada biaya transport--</option>
    			<option value="50000">50.000.00 IDR</option>
    			<option value="100000">100.000.00 IDR</option>
    			<option value="150000">150.000.00 IDR</option>
    			<option value="200000">200.000.00 IDR</option>
                <option value="relatif">Biaya transport ditanggung customer</option>
    			<option value="negosiasi">Negosiasi</option>
    		</select>
    	</div>
    </div>


    <div class="row" id="divNego" style="display:none">
    	<div class="form-group col-sm-8">
    		<input type="text" name="dataz[transnego]" class="form-control" placeholder="Tulis biaya transport berdasarkan negosiasi">
    	</div>
    </div>

    <div class='row' style="display:none">
        <div class='form-group col-sm-8'>
            <label>Petugas</label>
            <input type='number' class='form-control' id='txt-passBaru' name='dataz[petugas]' value=<?php echo "'".$_SESSION['admin']['username']."'"; ?>>
            <input type='number' class='form-control' id='txt-passBaru' name='dataz[ip]' value=<?php echo "'".$_SERVER['REMOTE_ADDR']."'"; ?>>
        </div>
    </div>

    <?php if (isset($_GET['ac'])&&$_GET['ac']=='attachment'): ?>
        <h5><strong>Attachments</strong></h5>
    <div class="row">
        <div class="col-sm-8">
        <?php 
        $files = scandir('attachments/');
        for ($i=0; $i < count($files); $i++) { 
            if ($files[$i]!='.'&&$files[$i]!='..') {
                        
            echo "<div class='checkbox'><label><input type='checkbox' name='dataz[attachments][]' value='../attachments/".$files[$i]."'>".
                            $files[$i].
                            "</label></div>";
                        
                    }
                }
        ?>


        </div>
    </div>

    <div class='row'>
            <div class='form-inline col-sm-8'>
                <button class='btn btn-lg btn-primary' style='width:25%'>Submit</button>
                <a href="<?php echo basename(__FILE__, '.php') ?>" class="btn btn-lg btn-info">Form Non Lampiran</a>
            </div>
        </div>


<?php else: ?>

        <div class='row'>
            <div class='form-inline col-sm-8'>
                <button class='btn btn-lg btn-primary' style='width:25%'>Submit</button>
                <a href="<?php echo basename(__FILE__, '.php').'?ac=attachment' ?>" class="btn btn-lg btn-info">
                Form dengan Lampiran
                </a>
                <a href="penawaran-custom" class="btn btn-lg btn-warning">
                    Penawaran Custom
                </a>
            </div>
        </div>


    <?php endif ?>
    
    </form>
</div>



<script type="text/javascript">
	$(document).ready(function(){
		
		$('#myTransport').change(function(){
			if ($(this).val()=='negosiasi') {
				$('#divNego').css('display','');	
			}else{
				$('#divNego').css('display','none');
			}
		});
	});

</script>

</body>
</html> 