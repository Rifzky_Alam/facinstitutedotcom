<?php
session_start();
$url = 'new-transaksiz';
include_once 'session/session-class.php';
include_once '../baseurl.php';
include_once '../model/Perusahaan.php';
include_once '../model/Customer.php';

$session = new Sessionz();
$session->AdminMarketing();


// page condition
if (isset($_GET['c'])&&!empty($_GET['c'])) {
    // include_once '../model/page.php';
    include_once '../model/Transaksi.php';
    $transaksi = new Transaksi();
    $perusahaan = new Perusahaan();
    $customer = new Customer();
    if (isset($_POST['in'])) {
        include_once '../model/Transaksi.php';
        $transaksi = new Transaksi();
        
        $rawID='TR-'.date('dmY');
        $rawID2 = substr($_GET['c'], 0, 4);
        $id = $rawID.$rawID2;

        $transaksi->setID($id);
        $transaksi->setCustomer($_GET['c']);
        $ppn = $_POST['in']['ppn'] / 100;
        $potongan = $_POST['in']['discount'] / 100;
        $transaksi->setPPN($ppn);
        $transaksi->setDiscount($potongan);
        $transaksi->setNotes($_POST['in']['notes']);
        $transaksi->setPetugas($_SESSION['admin']['username']);
        $transaksi->setJenistransaksi($_POST['in']['jtr']);
        $transaksi->setAccurate($_POST['in']['jac']);
        $transaksi->setDP($_POST['in']['depe']);
        $transaksi->setWaktu($_POST['in']['waktu']);
        $transaksi->setUsahaid($_POST['in']['idusaha']);
        
        $transaksi->setProvinsi($_POST['in']['provinsi']);
        $transaksi->setKota($_POST['in']['kota']);
        $transaksi->setKecamatan($_POST['in']['sbd']);
        
        $_POST['in']['tempat'] = str_replace('##', '<br>', $_POST['in']['tempat']);
        $transaksi->setTempat($_POST['in']['tempat']);

        // $transaksi->InputNewTransaction();
        
        if ($transaksi->InputNewTransaction()) {
            echo "<script>alert('Data transaksi berhasil disimpan!');</script>";
            echo "<script>location.replace('new-transaksi-paket?tr=".$transaksi->getID()."');</script>";
        } else {
            echo "<script>alert('Data transaksi gagal disimpan!');</script>";
        }
        
    }

    $customer->setID($_GET['c']);
    $datacustomer = $customer->FetchSelectedID2();
    $transaksi->setCustomer($_GET['c']);
    // print_r($datacustomer);
    $datausaha = $perusahaan->selectByID($datacustomer[0]->id_perusahaan);
    $data = array(
        'base_url' => getBaseUrl(), //base data
        'url'=> $url,
        'judul' => 'Tambah transaksi/FAC Institute',
        'username'=>$_SESSION['admin']['nama'],
        'page' => 'tambah agenda', // end base data
        'nama_usaha' => $datausaha->nama,
        'alamat_usaha'=>$datausaha->alamat,
        'provinsi' => $datausaha->provinsi,
        'kota' => $datausaha->kota,
        'kecamatan' => $datausaha->kec,
        'dtjt' => $transaksi->FetchJenisTransaksi(),
        'nama_cust' => $datacustomer[0]->nama
    );
    $data['idusaha'] = $datacustomer[0]->id_perusahaan;
    $data['listaccurate'] = $transaksi->FetchJenisAccurateTransaksi();

    $data = (object) $data;
    include_once 'view/transaksi/tambah-transaksi.php';

}elseif (isset($_GET['edt'])&&!empty($_GET['edt'])) {
        include_once '../model/Transaksi.php';
        $transaksi = new Transaksi();
        $transaksi->setID($_GET['edt']);
        $datatrans = $transaksi->FetchTransactionDataById();

        if (isset($_POST['ubh'])) {
                $ppn = $_POST['ubh']['ppn'] / 100;
                $potongan = $_POST['ubh']['discount'] / 100;

                $transaksi->setPPN($ppn);
                $transaksi->setDiscount($potongan);
                $transaksi->setNotes($_POST['ubh']['notes']);
                $transaksi->setJenistransaksi($_POST['ubh']['jtr']);
                $transaksi->setAccurate($_POST['ubh']['jac']);
                $transaksi->setDP($_POST['ubh']['depe']);
                $transaksi->setWaktu($_POST['ubh']['waktu']);
                $_POST['ubh']['tempat'] = str_replace('##', '<br>', $_POST['ubh']['tempat']);
                $transaksi->setTempat($_POST['ubh']['tempat']);
                
                $transaksi->setProvinsi($_POST['ubh']['provinsi']);
                $transaksi->setKota($_POST['ubh']['kota']);
                $transaksi->setKecamatan($_POST['ubh']['sbd']);

            if ($transaksi->EditTransaction()) {
                echo "<script>alert('Data transaksi berhasil diubah!');</script>";
                echo "<script>location.replace('new-perusahaan-info?tr=".$transaksi->getID()."');</script>";
            } else {
                echo "<script>alert('Data transaksi berhasil diubah!');</script>";
            }
            
        }


        $data = array(
            'base_url' => getBaseUrl(), //base data
            'url'=> $url,
            'judul' => 'Edit transaksi/FAC Institute',
            'username'=>$_SESSION['admin']['nama'],
            'page' => 'tambah_transaksi', // end base data
            'nama_usaha' => $datatrans->nama,
            'idusaha' => $datatrans->trans_id_usaha,
            'nama_cust' => $datatrans->nama_cust,
            'versi_acc' => $datatrans->trans_va,
            'waktu'=>$datatrans->trans_waktu,
            'dtjt' => $transaksi->FetchJenisTransaksi(),
            'jtr'=>$datatrans->trans_jenis,
            'ppn'=>$datatrans->trans_ppn * 100,
            'discount'=>$datatrans->trans_discount * 100,
            'dp'=>$datatrans->trans_dp,
            'notes'=>$datatrans->trans_notes,
            'provinsi' => $datatrans->trans_prov,
            'kota' => $datatrans->trans_kota,
            'kecamatan' => $datatrans->trans_kec
        );

        $data['tempat']=str_replace('<br>', '##', $datatrans->trans_lokasi);
        $data['listaccurate'] = $transaksi->FetchJenisAccurateTransaksiById();

    $data = (object) $data;
    include_once 'view/transaksi/edit-transaksi.php';

}elseif(isset($_GET['newdate'])&&!empty($_GET['newdate'])){
    include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/controller/transaksi.controller.php';
    Transaksi::TambahTanggal($_GET['newdate']);
}elseif (isset($_GET['detitem'])&&!empty($_GET['detitem'])) {
    include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/controller/transaksi.controller.php';
    Transaksi::DetailItem($_GET['detitem']);
}elseif (isset($_GET['uitm'])&&!empty($_GET['uitm'])) {
    include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/controller/transaksi.controller.php';
    Transaksi::EditItemTrans($_GET['uitm']);
}else{
    echo "<script>alert('harap memilih customer terlebih dahulu!');</script>";
    echo "<script>location.replace('data-transaksi');</script>";
}


// $file = new File();
 ?>