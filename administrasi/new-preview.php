<?php 
session_start();
include_once 'session/session-class.php';
$session = new Sessionz();
$session->AdminMarketing();

if (isset($_GET['einv'])&&!empty($_GET['einv'])) { //preview email training accurate
	include_once '../model/Transaksi.php';
	include_once '../model/Invoice.php';
	$transaksi = new Transaksi();
	$invoice = new Invoice();
	$invoice->setNo($_GET['einv']);

	$datainv = $invoice->GetByID();
	// print_r($datainv);
	$transaksi->setID($datainv->inv_id_trans);

	$datas = $transaksi->FetchTransactionForPreview();

	$data = array(
		'nama_cust' => $datas->nama_cust,
		'nama_usaha' => $datas->nama,
		'tanggal' => explode('##', $datas->tgl),
		'waktu' => $datas->trans_waktu,
		'tempat' => $datas->trans_lokasi, 
		'alamat' => $datas->alamat,
		'agenda' => explode('##', $datas->nama_agenda),
		'items' => $transaksi->FetchItemsForInvoice(),
		'notes' => explode('##', $datas->trans_notes),
		'metode_bayar' => $datainv->inv_metode_bayar,
		'petugas'=> $datas->petugas
	);
	$data = (object) $data;
	include_once 'view/invoice/mail-content.php';

	echo Invoice($data);
}

if (isset($_GET['asinv'])&&!empty($_GET['asinv'])) { //preview email training accurate
	include_once '../model/Transaksi.php';
	include_once '../model/Invoice.php';
	$transaksi = new Transaksi();
	$invoice = new Invoice();
	$invoice->setNo($_GET['asinv']);

	$datainv = $invoice->GetByID();
	// print_r($datainv);
	$transaksi->setID($datainv->inv_id_trans);

	$datas = $transaksi->FetchTransactionForPreview();
	$syarat = array(
		'Entry data bersifat result oriented, bukan time oriented.',
		'Kami diberi akses ke sumber data yang akan dientry.'
	);

	
	if ($datas->trans_notes!='') {
		$notes = explode('##', $datas->trans_notes);			
		foreach ($notes as $key) {
			array_push($syarat, $key);
		}

	}
	

	$data = array(
		'nama_cust' => $datas->nama_cust,
		'nama_usaha' => $datas->nama,
		'tanggal' => explode('##', $datas->tgl),
		'waktu' => $datas->trans_waktu,
		'tempat' => $datas->trans_lokasi, 
		'alamat' => $datas->alamat,
		'agenda' => explode('##', $datas->nama_agenda),
		'items' => $transaksi->FetchItemsForInvoice(),
		'notes' => explode('##', $datas->trans_notes),
		'metode_bayar' => $datainv->inv_metode_bayar,
		'syarat' => $syarat,
		'petugas'=> $datas->petugas
	);
	$data = (object) $data;
	include_once 'view/invoice/mail-content.php';

	echo InvoiceAccountingService($data);
}


if (isset($_GET['kinv'])&&!empty($_GET['kinv'])) {
	include_once '../model/Transaksi.php';
	include_once '../model/Invoice.php';
	$transaksi = new Transaksi();
	$invoice = new Invoice();
	$invoice->setNo($_GET['kinv']);

	$datainv = $invoice->GetByID();
	// print_r($datainv);
	$transaksi->setID($datainv->inv_id_trans);

	$datas = $transaksi->FetchTransactionForPreview();
	$syarat = array(
		'Entry data bersifat result oriented, bukan time oriented.',
		'Kami diberi akses ke sumber data yang akan dientry.'
	);

	$notes = explode('##', $datas->trans_notes);
	foreach ($notes as $key) {
		array_push($syarat, $key);
	}
	

	$data = array(
		'nama_cust' => $datas->nama_cust,
		'nama_usaha' => $datas->nama,
		'tanggal' => explode('##', $datas->tgl),
		'waktu' => $datas->trans_waktu,
		'tempat' => $datas->trans_lokasi, 
		'alamat' => $datas->alamat,
		'agenda' => explode('##', $datas->nama_agenda),
		'items' => $transaksi->FetchItemsForInvoice(),
		'notes' => explode('##', $datas->trans_notes),
		'metode_bayar' => $datainv->inv_metode_bayar,
		'syarat' => $syarat,
		'petugas'=> $datas->petugas
	);
	$data = (object) $data;
	include_once 'view/invoice/mail-content.php';

	echo Kursus($data);
}


if (isset($_GET['inv'])&&!empty($_GET['inv'])) {
	include_once '../model/Mypdf.php';
	include_once '../model/Transaksi.php';
	include_once '../model/Invoice.php';
	$transaksi = new Transaksi();
	$invoice = new Invoice();
	$pdf = new Mypdf();

	$invoice->setNo($_GET['inv']);

	$datainv = $invoice->GetByID();
	// print_r($datainv);
	$transaksi->setID($datainv->inv_id_trans);

	$datas = $transaksi->FetchTransactionForPreview();

	$obj1 = array('nama_item' => 'Tidak ada data yang dipilih','harga'=>'0','itm_qty'=> '0');
	
	if (count($transaksi->FetchItemsForInvoice())=='0') {
		$mylist = array();
		array_push($mylist, (object) $obj1);
	}else{
		$mylist = $transaksi->FetchItemsForInvoice(); 
	}


	$data = array(
		'no_invoice' => $_GET['inv'],
		'nama_cust' => $datas->nama_cust,
		'nama_usaha' => $datas->nama,
		'telepon_cust' => $datas->email_cust,
		'email_cust' =>$datas->telp_cust,
		'alamat' => $datainv->inv_alamat,
		'deskripsi' => $datainv->inv_deskripsi,
		'tanggal' => $datainv->inv_date,
		'metode_bayar' => $invoice->TentukanMetodeBayar($datainv->inv_metode_bayar),
		'items' => $mylist,
		'diskon' => $datas->trans_discount,
		'dp' => $datas->trans_dp 
	);
	$data = (object) $data;
	$pdf->setData($data);
	$pdf->invoice('display');
	// print_r($data);
	// echo $datas->trans_discount;
}

if (isset($_GET['quot'])&&!empty($_GET['quot'])){
	include_once '../model/Mypdf.php';
	include_once '../model/Quotation.php';
	$pdf = new Mypdf();
	$quot = new Quotation();
	$quot->setID($_GET['quot']);
	$dataquot = $quot->DisplayPDF();
	$mylist = array();
	
	$obj1 = array('nama_item' => 'Tidak ada data yang dipilih','harga'=>'0','itm_qty'=> '0');
	
	if (count($quot->FetchItems())=='0') {
		$mylist = array();
		array_push($mylist, (object) $obj1);
	}else{
		$mylist = $quot->FetchItems(); 
	}


	$data = array(
		'quot_num' => $_GET['quot'],
		'title' => $dataquot->qtn_title,
		'subject' => $dataquot->qtn_subject,
		'jenistr'=>$dataquot->trans_jenis,
		'nama_usaha' => $dataquot->nama_usaha,
		'customer' => $dataquot->nama_cust,
		'email' => $dataquot->email_cust,
		'items' => $mylist,
		'petugas' => $_SESSION['admin']['nama'] 
	);

	if ($dataquot->qtn_notes=='') {
		$data['notes'] = '';
	} else {
		$data['notes'] = $dataquot->qtn_notes;
	}
	
	
	$data['tanggal'] = $dataquot->qtn_date;


	$data = (object) $data;
	$pdf->setData($data);
	$pdf->Quotation('display');
	// print_r($dataquot);
}

if (isset($_GET['quotc'])&&!empty($_GET['quotc'])){
	include_once '../model/Mypdf.php';
	include_once '../model/Quotation.php';
	$pdf = new Mypdf();
	$quot = new Quotation();
	$quot->setID($_GET['quotc']);
	$dataquot = $quot->DisplayPDF();
	$mylist = array();
	
	$obj1 = array('nama_item' => 'Tidak ada data yang dipilih','harga'=>'0','itm_qty'=> '0');
	
	if (count($quot->FetchItems())=='0') {
		$mylist = array();
		array_push($mylist, (object) $obj1);
	}else{
		$mylist = $quot->FetchItems(); 
	}


	$data = array(
		'quot_num' => $_GET['quotc'],
		'title' => $dataquot->qtn_title,
		'subject' => $dataquot->qtn_subject,
		'jenistr'=>$dataquot->trans_jenis,
		'nama_usaha' => $dataquot->nama_usaha,
		'customer' => $dataquot->nama_cust,
		'email' => $dataquot->email_cust,
		'items' => $mylist,
		'petugas' => $_SESSION['admin']['nama'] 
	);

	if ($dataquot->qtn_notes=='') {
		$data['notes'] = '';
	} else {
		$data['notes'] = $dataquot->qtn_notes;
	}
	
	
	$data['tanggal'] = $dataquot->qtn_date;


	$data = (object) $data;
	$pdf->setData($data);
	$pdf->QuotationComparation('display');
	// print_r($dataquot);
}

if (isset($_GET['equotc'])&&!empty($_GET['equotc'])) {
	include_once 'view/quotation/quot-mail-view.php';
	include_once '../model/Quotation.php';
	include_once 'functions/Fsecurity.php';
	$quot = new Quotation();
	$quot->setID($_GET['equotc']);
	$dataquot = $quot->DisplayPDF();

	$obj1 = array('nama_item' => 'Tidak ada data yang dipilih','harga'=>'0','itm_qty'=> '0');
	$mylist = array();
	if (count($quot->FetchItems())=='0') {
		$mylist = array();
		array_push($mylist, (object) $obj1);
	}else{
		$mylist = $quot->FetchItems(); 
	}


	$data = array(
		'nama_cust' => $dataquot->nama_cust
	);
	$data['items'] = $mylist;
	$data['jenistr'] = $dataquot->trans_jenis;
	$data['nama_usaha']=$dataquot->nama_usaha;
	$data['petugas']=$dataquot->petugas;
	$data['base_url']='https://fac-institute.com/';
	$data['idtransaksi'] = AES256($dataquot->trans_id,'e');
	
	if ($data['jenistr']== '1') {
		$data['syarat'] = '';
		// $note = '';
		$data['notes'] = explode('##', $dataquot->qtn_notes);
	}elseif ($data['jenistr']== '2') {
		$data['syarat'] = array(
			'Entry data bersifat result oriented, bukan time oriented.',
			'Kami diberi akses ke sumber data yang akan dientry.'
		);
		// $note = 'iya##dah##terserah';
		$notes = explode('##', $dataquot->qtn_notes);
		foreach ($notes as $key) {
			array_push($data['syarat'], $key);
		}
		$data['notes'] = '';

	}elseif ($data['jenistr']== '3') {
		$data['syarat'] = '';
		// $note = 'kursus##akuntansi##accurate';
		$data['notes'] = explode('##', $dataquot->qtn_notes);
	}

	$data = (object) $data;
	echo PenawaranCompare($data);
}

if (isset($_GET['equot'])&&!empty($_GET['equot'])) {
	include_once 'view/quotation/quot-mail-view.php';
	include_once '../model/Quotation.php';
	
	$quot = new Quotation();
	$quot->setID($_GET['equot']);
	$dataquot = $quot->DisplayPDF();

	$obj1 = array('nama_item' => 'Tidak ada data yang dipilih','harga'=>'0','itm_qty'=> '0');
	$mylist = array();
	if (count($quot->FetchItems())=='0') {
		$mylist = array();
		array_push($mylist, (object) $obj1);
	}else{
		$mylist = $quot->FetchItems(); 
	}


	$data = array(
		'nama_cust' => $dataquot->nama_cust
	);
	$data['items'] = $mylist;
	$data['jenistr'] = $dataquot->trans_jenis;
	$data['nama_usaha']=$dataquot->nama_usaha;
	$data['petugas']=$dataquot->petugas;
	
	if ($data['jenistr']== '1') {
		$data['syarat'] = '';
		// $note = '';
		$data['notes'] = explode('##', $dataquot->qtn_notes);
	}elseif ($data['jenistr']== '2') {
		$data['syarat'] = array(
			'Entry data bersifat result oriented, bukan time oriented.',
			'Kami diberi akses ke sumber data yang akan dientry.'
		);
		// $note = 'iya##dah##terserah';
		$notes = explode('##', $dataquot->qtn_notes);
		foreach ($notes as $key) {
			array_push($data['syarat'], $key);
		}
		$data['notes'] = '';

	}elseif ($data['jenistr']== '3') {
		$data['syarat'] = '';
		// $note = 'kursus##akuntansi##accurate';
		$data['notes'] = explode('##', $dataquot->qtn_notes);
	}

	$data = (object) $data;
	echo Penawaran($data);
}

?>