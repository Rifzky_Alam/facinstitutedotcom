<?php 

class Data{

	public $homedir = '/home/facinsti/public_html/';//
	// public $homedir = '/Applications/XAMPP/xamppfiles/htdocs/rikza/';
    public $base_url = "https://fac-institute.com/";
    public $company = 'fac institute';
    public $companyaddr='Jl. Pahlawan Revolusi 10GG No.3 (Lantai 2), RT.9/RW.7, Pd. Bambu, Kec. Duren Sawit, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta Indonesia 13430';
 	public $title;
 	public $subtitle;
 	public $table;
 	public $me_url;
 	public $page;

 	public function Model($file){
		include_once $this->homedir.'model/'.$file.'.php';
	}

	public function IncludeFileWithData($path,$data){
		include_once $this->homedir.$path;
	}

	public function JustInclude($path){
		include_once $this->homedir.$path.'.php';	
	}

	public function View($path,$data){
		include_once $this->homedir.'administrasi/view/'.$path.'.php';
	}

	public function Lib($file){
		include_once $this->homedir.'library/'.$file.'.php';
	}
	
	public function Func($file){
		include_once $this->homedir.'administrasi/functions/'.$file.'.php';
	}

 	public function __construct(){
		// $currentPath = $_SERVER['PHP_SELF'];
	  	// $pathInfo = pathinfo($currentPath);
  		// $hostName = $_SERVER['HTTP_HOST'];
  		// $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https://'?'https://':'http://';
 		// $this->base_url = 'https://'.$hostName."/";
 		// $this->base_url = 'http://'.$hostName."/rikza/";
 	}    

 	public function securitycode($string,$action){
	  $secret_key = "R!f2ky_Al@m&L0el*3_N@z!f4";
	  $secret_iv = "Lulu_Nazifa_Secret_iv";

	  $output = false;
	  $encrypt_method = "AES-256-CBC";
	  $key = hash('sha256',$secret_key);
	  $iv = substr(hash('sha256', $secret_iv), 0,16);
	  if ($action=='e') {
	    $output = base64_encode(openssl_encrypt($string, $encrypt_method, $key,0,$iv));
	  }elseif ($action=='d') {
	    $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key,0,$iv);
	  }
	  return $output;
	}
    
}

?>