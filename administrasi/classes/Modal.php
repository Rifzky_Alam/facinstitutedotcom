<?php 

class IModal{
	private $idmodal;
    private $headtitle;
    private $content;
    private $footer;

    public function __construct($arr){
        $this->setIdmodal($arr['modal_id']);
        $this->setHeadtitle($arr['modal_title']);
        $this->setContent($arr['modal_content']);
        $this->setFooter($arr['footer_modal']);
    }

    public function getIdmodal(){
        return $this->idmodal;
    }

    public function setIdmodal($idmodal){
        $this->idmodal = $idmodal;
    }
    public function getHeadtitle(){
        return $this->headtitle;
    }

    public function setHeadtitle($headtitle){
        $this->headtitle = $headtitle;
    }

    public function getContent(){
        return $this->content;
    }

    public function setContent($content){
        $this->content = $content;
    }

    public function getFooter(){
        return $this->footer;
    }

    public function setFooter($footer){
        $this->footer = $footer;
    }
}

?>