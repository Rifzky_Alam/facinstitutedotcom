<?php 
include_once 'View.php';
class Laporan extends Data
{
    public function __construct(){
        
    }

	protected function GetColors($value){
		$warna = array('red','orange','yellow','green','blue','purple','black','silver','maroon','olive','olivegreen','khaki','chocolate','slategrey','deeppink','magenta','thisle','cadetblue','cyan','mediumtortoquise','paleturquoise','seagreen','indigo','royalblue','wheat','brown','lightsteelblue','tan','salmon','yellowgreen','grey','navy');
		$arr = array();

		for ($i = 0; $i < $value; $i++) {
			echo 'window.chartColors.'.$warna[$i].',';
		}
	}


	protected function DataValue($value){
		foreach ($value as $key) {
			echo $key->jumlah.',';		
		}	
	}

	protected function NamaMarketing($value){
		foreach ($value as $key) {
			echo '"'.$key->nama.'",';
		}
	}

	protected function IndoTanggal($value){
		if ($value==1) {
			return " Januari ";
		}elseif ($value==2) {
			return " Februari ";
		}elseif ($value==3) {
			return " Maret ";
		}elseif ($value==4) {
			return " April ";
		}elseif ($value==5) {
			return " Mei ";
		}elseif ($value==6) {
			return " Juni ";
		}elseif ($value==7) {
			return " Juli ";
		}elseif ($value==8) {
			return " Agustus ";
		}elseif ($value==9) {
			return " September ";
		}elseif ($value==10) {
			return " Oktober ";
		}elseif ($value==11) {
			return " November ";
		}elseif ($value==12) {
			return " Desember ";
		}
	}

}

?>