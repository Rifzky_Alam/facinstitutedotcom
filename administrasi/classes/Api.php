<?php 
class Api{
	public static $ins = null;    
    private function __construct(){
        
    }


    public static function getInstance(){
    	if (self::$ins==null) {
    		self::$ins=new Api();
    	}

    	return self::$ins;
    }

    public function Test(){
    	return "okay!";
    }


    public function GetOutput($data){
    	if (count($data)==0) {
    		$arr = [
	    	    'status' => ['code' => '0','description' => 'Data is empty'],
	    	    'data' => [], 
	    	];
    	}else{
    		$arr = [
	    	    'status' => ['code' => '1','description' => 'Data is available'],
	    	    'data' => $data, 
	    	];
    	}
	    
	    return json_encode($arr);
    }
}


?>