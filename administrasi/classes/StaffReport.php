<?php 
include_once 'Laporan.php';
class Staff extends Laporan
{
    
    public function __construct(){
        
    }

    protected function NamaKegiatan($value){
	    foreach ($value as $key) {
	        echo '"'.$key->lkk_ket.'",';
	    }
	}

	public function GetTanggal($value){
		$value = explode(' ', $value);
		$tgl = $value[0];
		$tgl = explode('-', $tgl);
		return $tgl[2].$this->IndoTanggal($tgl[1]).$tgl[0];
	}

	public function CekKeterangan($value,$stack){
		$value = explode(' # ', $value);
		$retval = array();
		for ($i = 0; $i < count($value); $i++) {
			for ($j = 0; $j < count($stack); $j++) {
				if ($value[$i]==$stack[$j]->lkk_id) {
					array_push($retval,$stack[$j]->lkk_ket);
				}elseif (preg_match('/9-/i', $value[$i])) {
					$value[$i] = str_replace('9-', 'Lainnya-', $value[$i]);
					array_push($retval,$value[$i]);
				}
			}
		}
		if (count($retval)=='0') {
			return "";
		}else{
			return implode(' # ', $retval);
		}
	}

	public function CekRekan($value,$stack){
		$value = explode(' # ', $value);
		$retval = array();
		for ($i = 0; $i < count($value); $i++) {
			for ($j = 0; $j < count($stack); $j++) {
				if ($value[$i]==$stack[$j]->username) {
					array_push($retval,$stack[$j]->nama);
				}
			}
		}
		if (count($retval)=='0') {
			return "";
		}else{
			return implode(' # ', $retval);
		}
	}

}

?>