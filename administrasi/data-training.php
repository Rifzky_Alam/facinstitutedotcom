<?php 
session_start();
if (!isset($_SESSION['admin'])||($_SESSION['admin']['tipe']!='admin'&&$_SESSION['admin']['tipe']!='marketing')) {
    //echo $_SESSION['admin']['tipe'];
    header('Location:index.php');
}

include_once '../model/Pendaftar.php';
$control = new Pendaftar();

?>
<!DOCTYPE html>
<html>


<?php 
$judul='Data Transaksi - FAC Institute';
$page='dataOrderTraining';
include_once 'header.php'; 

?>

<body>
<?php 
//echo $_SESSION['admin']['tipe'];
?>


<?php 

include_once 'sidebar.php';

include_once 'top-nav.php';

 ?>

<?php 

    if (isset($_GET['data'])) {
        $by = array('cond' => 'search',
         'perusahaan'=> $_GET['data']['namaPerusahaan'],
         'usaha'=>$_GET['data']['jenisUsaha'],
         'tanggalAwal'=> $_GET['data']['tanggalAwal'],
         'tanggalAkhir'=> $_GET['data']['tanggalAkhir'],
         'pengguna'=>$_GET['data']['jenisPengguna'],
         'versi'=>$_GET['data']['versi']
         );
        $by = (object) $by;
        $dataSelect = json_decode($control->fetchDataTraining($by)); 
    }else if(isset($_GET['d'],$_GET['kp'])){
        if ($_GET['d']=='1'&&!empty($_GET['kp'])) {
            include_once '../model/Calendar.php';
            $cal = new Calender();
            if ($_SESSION['admin']['tipe']=='admin') {
                if ($control->deleteByID($_GET['kp'])) {
                    echo "<script>alert('Data berhasil dihapus')</script>";
                    $cal->deleteDataByAcara($_GET['kp']);
                    echo "<script>window.location.replace('".$_SERVER['SCRIPT_NAME']."');</script>";
                }
            }else{
                echo "<script>alert('Hanya Administrator yang berhak melakukan operasi ini');window.location.replace('".$_SERVER['SCRIPT_NAME']."');</script>";
            }
        }
    }else{
    $bulan = intval(date('m'));
    $by = array('cond' => 'month' , 'val'=> $bulan );
    $by = (object) $by;
    $dataSelect = json_decode($control->fetchDataTraining($by)); 
    }
    //$dataPendaftar = $control->selectDataPendaftarByThisMonth();
?>

<?php 
    if (isset($_GET['tanggalAwal'],$_GET['tanggalAkhir'],$_GET['nama'])) {

        if ($_GET['tanggalAwal']==''){
            $_GET['tanggalAwal']=date('Y-m-d');
        }

        if ($_GET['tanggalAkhir']==''){
            $_GET['tanggalAkhir']=date('Y-m-d');
        }

        //$dataAbsen = $control->daftarHadirByParameter($_GET['tanggalAwal'],$_GET['tanggalAkhir'],$_GET['nama']);
    }
?>

<div class="container" id="isi">
    <?php if (isset($_GET['kp'])&&!empty($_GET['kp'])): ?>
    <!--start edit request-->
    <?php 
    include_once '../model/Petugas.php';
    $ctrPetugas = new ControlPetugas();
    $myDataz = json_decode($ctrPetugas->dataRequest('forEdit'));
    $dataPendaftarz = $control->selectForRequestByID($_GET['kp']);

    if (isset($_POST['perusahaanID'],$_POST['idAbsen'])) {
        
        if (!empty($_POST['perusahaanID'])) {
            //echo $_POST['perusahaanID'].'<br>';
            for ($i=0; $i < count($_POST['idAbsen']) ; $i++) { 
                $isiData = array('id' => $_POST['idAbsen'][$i],'val'=> $_POST['perusahaanID']);
                echo $ctrPetugas->updatePerusahaan($isiData);
                //echo ."<br>";
            }
            echo "<script>window.location.replace('".$_SERVER['SCRIPT_NAME']."');</script>";    
        }else{
            echo "<script>alert('data nama perusahaan tidak ada, harap cek kembali atau kontak admin');</script>";
        }
        
    }


    ?>

    <div class="row">
        <div class="col-md-6 col-sm-6">
            <h1>Pemberian Tugas Training</h1>
        </div>

        <div class="col-md-6 col-sm-6" style="padding-top:30px;text-align:right;">
            <button class="btn btn-primary" style="text-align:right" id="cari-data" href="#modal-cari" data-toggle="modal" >Cari Data <span class="glyphicon glyphicon-search"></span></button>
        </div>
    </div>
    <br><br>
    <div class='row'>
        <div class='col-md-12'>
        <table><tbody><tr><td>Untuk Perusahaan:</td><td style='padding-left:15px'><strong><?php echo $dataPendaftarz->nama_asli ?></strong></td></tr></tbody></table>
        </div>
    </div>
    <br>
    <div class='row'>
        <div class='col-md-12'>
            <form action='' method='post'>
            <input name='perusahaanID' value=<?php echo "'".$dataPendaftarz->nama_perusahaan."'"; ?> style='display:none' />
            <table class='table'>
                <thead>
                    <tr>
                        <th>-</th>
                        <th>Nama Petugas</th>
                        <th>Tanggal Request</th>
                        <th>Data Request</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    for ($i=0; $i < count($myDataz) ; $i++) { 
                        echo "<tr>";
                        echo "<td><input type='checkbox' name='idAbsen[]' value='".$myDataz[$i]->id."' /></td>";
                        echo "<td>".$myDataz[$i]->nama."</td>";
                        echo "<td>".$myDataz[$i]->tanggal_absen."</td>";
                        echo "<td>".$myDataz[$i]->nama_perusahaan."</td>";
                        echo "</tr>";
                    }
                    ?>
                </tbody>
            </table>
            <button class='btn btn-primary' style='width:100%'>Submit</button>
            </form>
        </div>
    </div>
            

        <!--end edit request-->
        <?php else: ?>
    <div class="row">
        <div class="col-md-6 col-sm-6">
            <h1>Data Jadwal Training</h1>
        </div>

        <div class="col-md-6 col-sm-6" style="padding-top:30px;text-align:right;">
            <button class="btn btn-primary" style="text-align:right" id="cari-data" href="#modal-cari" data-toggle="modal" >Cari Data <span class="glyphicon glyphicon-search"></span></button>
        </div>
    </div>
    
    <div class="row" style="overflow:auto">
        <div class="col-md-12">

            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="tengah">Nama perusahaan</th>
                        <th class="tengah">Nama Kontak</th>
                        <th class="tengah">Usaha</th>
                        <th class="tengah">Jumlah Hari Training</th>
                        <th class="tengah">Nama Paket</th>
                        <th class="tengah">Qty</th>
                        <th class="tengah">Tanggal Daftar</th>
                        <?php if ($_SESSION['admin']['tipe']=='admin'||$_SESSION['admin']['tipe']=='marketing'): ?>
                            <th class='tengah'>Aksi</th>   
                        <?php endif ?>
                        
                    </tr>
                </thead>
                <tbody>
                    <!--<td>Rifzky</td><td>10:10</td><td>somewhere new</td><td>12:12</td><td>Somwhere new</td>-->
                    <?php for ($i=0; $i < count($dataSelect); $i++) { ?>
                    <tr>
                    	<td><?php echo "<a target='_blank' href='detail.php?data=".$dataSelect[$i]->id."'>". $dataSelect[$i]->nama_perusahaan ."</a>"; ?></td>
                    	<td><?php echo $dataSelect[$i]->nama_kontak; ?></td>
                    	<td><?php echo $dataSelect[$i]->usaha; ?></td>
                    	<td><?php echo $dataSelect[$i]->jumlah_hari;?></td>
                    	<td><?php echo $dataSelect[$i]->nama_item;?></td>
                        <td><?php echo $dataSelect[$i]->qty;?></td>
                    	<td><?php echo $dataSelect[$i]->tanggal_daftar;?></td>
                        <?php if ($_SESSION['admin']['tipe']=='admin'||$_SESSION['admin']['tipe']=='marketing'): ?>
                            <td> <a href=<?php echo "'edit-order.php?v=".$dataSelect[$i]->id."'"; ?>>Edit</a> || <a href=<?php echo "'assignment?idp=".$dataSelect[$i]->id."'"; ?>>Assign</a> || <a href="daftar-agenda?p=<?php echo $dataSelect[$i]->id ?>">Jadwal</a> || <a href=<?php echo "'".$_SERVER['SCRIPT_NAME']."?d=1&kp=".$dataSelect[$i]->id."'"; ?>>Delete</a> </td>
                        <?php endif ?>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php endif ?>
	
</div>

<!-- Modal -->
  <div class='modal fade' id='modal-cari' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pencarian Data</h3>

        </div>
       <div class='modal-body' style='overflow-y:scroll'>
        <div style='padding:15px; height: 300px;' class='row'>
          
          <div class="col-md-12">
            
              
                <form action=<?php echo "'".$_SERVER['SCRIPT_NAME']."'"; ?> method="GET">

                    <div class="row">
                        <div class="form-group">
                        	<label>Nama Perusahaan</label>
                        	<input type='text' name='data[namaPerusahaan]' class='form-control' />
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label for="tanggal-awal">Tanggal Awal</label>
                            <input type="text" name="data[tanggalAwal]" id="tanggal-awal" class="form-control" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label for="tanggal-awal">Tanggal Akhir</label>
                            <input type="text" name="data[tanggalAkhir]" id='tanggal-akhir' class="form-control" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                        	<label>Jenis Usaha</label>
                        	<input type='text' name='data[jenisUsaha]' class='form-control' />
                        </div>
                    </div>
                  
                    <div class="row">
                        <div class="form-group">
                        	<label>Jenis Pengguna</label>
                        	<select name='data[jenisPengguna]' class='form-control' >
                                <option value='' selected>Semua</option>
                                <option value='baru'>Baru</option>
                                <option value='lama'>Lama</option>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                        	<label>Versi Accurate</label>
                        	<input type='text' name='data[versi]' class='form-control' />
                        </div>
                    </div>

                    <div class="row">
                        <button class="btn btn-success" style='width:100%'>Cari</button>
                    </div>

                </form>
          </div>
             
          

        </div>
       </div>


        <div class='modal-footer'>

            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>FAC-Institute 2016</span>
                </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->  
<script type="text/javascript" src=<?php echo "'".getBaseUrl()."js/jquery-datepicker/jquery-ui.js'"; ?>></script>
<script type="text/javascript" src=<?php echo "'".getBaseUrl()."js/daftar-absen.js'"; ?>></script>
</body>
</html> 