<?php 
session_start();
if (!isset($_SESSION['admin'])||empty($_SESSION['admin']['username'])) {
	header('location: https://fac-institute.com/administrasi/accessdenied');
}
if (isset($_SERVER['PATH_INFO'])) {
	@$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
	$key = array_shift($url_segment);
	switch ($key) {
		case 'bulanan':
			include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/controller/transaksi.controller.php';
			Transaksi::TotalLaporanAgendaTransBulanan();
			break;
		default;
	        header('location: https://fac-institute.com/administrasi/');
		    break;
	}
}else{
    include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/controller/transaksi.controller.php';
// 	User::MenuGeneralReport();
}