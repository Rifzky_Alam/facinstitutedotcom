<?php 
session_start();
if (!isset($_SESSION['admin'])||empty($_SESSION['admin']['username'])) {
	header('location: https://fac-institute.com/administrasi/accessdenied');
}
if (isset($_SERVER['PATH_INFO'])) {
	@$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
	$key = array_shift($url_segment);
	switch ($key) {
		case 'byperan':
			include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/controller/user.controller.php';
			User::LaporanByPeran();
			break;
		case 'byjenisacc':
			include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/controller/user.controller.php';
			User::GenLaporanByJenisAccurate();
			break;
		case 'byjenisusaha':
			include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/controller/user.controller.php';
		    User::GenLaporanByJenisUsaha();
			break;
		case 'bystatuskerja':
		    include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/controller/user.controller.php';
		    User::GenLaporanByStatus();
			break;
		case 'bywilayah':
			include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/controller/user.controller.php';
		    User::GenLaporanByWilayah();
			break;
		case 'bytransport':
		    include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/controller/user.controller.php';
		    User::GenLaporanByTrans();
		    break;
		default;
	        header('location: https://fac-institute.com/administrasi/');
		    break;
	}
}else{
    include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/controller/user.controller.php';
	User::MenuGeneralReport();
}