<?php 
session_start();
include_once '../../classes/StaffReport.php';
$data = new Staff();

include_once $data->homedir.'administrasi/session/session-class.php';
$isessions= new Sessionz();
$isessions->IsSuperUser();

$data->Model('Petugas');
$petugas = new ControlPetugas();
if (isset($_GET['usr'])&&!empty($_GET['usr'])) {
	$petugas->setUsername($_GET['usr']);
		
	$data->title = "Laporan Staff";
	$data->subtitle="Laporan Reimbursment";
	@$data->username=$_SESSION['admin']['nama'];

	$datastaff = $petugas->GetLaporanUser();

	$data->namastaff = $datastaff->nama;
	$data->usr = $_GET['usr'];
	if (isset($_GET['fy'],$_GET['ty'])) {
		$data->biayatransport=number_format($petugas->GetSumLaporanByDate('biaya_transport',$_GET['fy'],$_GET['ty']));
		$data->makansiang=number_format($petugas->GetSumLaporanByDate('biaya_lunch',$_GET['fy'],$_GET['ty']));
		$data->makanmalam=number_format($petugas->GetSumLaporanByDate('biaya_dinner',$_GET['fy'],$_GET['ty']));
		$data->biayaparkir=number_format($petugas->GetSumLaporanByDate('biaya_parkir',$_GET['fy'],$_GET['ty']));
		$data->biayakesehatan=number_format($petugas->GetSumLaporanByDate('biaya_kesehatan',$_GET['fy'],$_GET['ty']));
		$data->biayaallowance=number_format($petugas->GetSumLaporanByDate('biaya_allowance',$_GET['fy'],$_GET['ty']));
		$data->biayalain=number_format($petugas->GetSumLaporanByDate('biaya_lain',$_GET['fy'],$_GET['ty']));
	} else {
		$data->biayatransport=number_format($petugas->GetSumLaporan('biaya_transport'));
		$data->makansiang=number_format($petugas->GetSumLaporan('biaya_lunch'));
		$data->makanmalam=number_format($petugas->GetSumLaporan('biaya_dinner'));
		$data->biayaparkir=number_format($petugas->GetSumLaporan('biaya_parkir'));
		$data->biayakesehatan=number_format($petugas->GetSumLaporan('biaya_kesehatan'));
		$data->biayaallowance=number_format($petugas->GetSumLaporan('biaya_allowance'));
		$data->biayalain=number_format($petugas->GetSumLaporan('biaya_lain'));
	}
		

	$data->JustInclude('administrasi/classes/Modal');
	$data->modal = new IModal(['modal_id' => 'modal-coba','modal_title' => 'Cari Data Berdasarkan Tanggal','modal_content' => '','footer_modal' => 'FAC Institute']); 
	$data->JustInclude('administrasi/view/main-component/modal');
	$data->View('laporan/staff/laporan.reimbursement',$data);
}
?>