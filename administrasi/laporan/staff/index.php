<?php 
session_start();
include_once '../../classes/StaffReport.php';
$data = new Staff();

include_once $data->homedir.'administrasi/session/session-class.php';
$isessions= new Sessionz();
$isessions->IsSuperUser();

$data->Model('Petugas');
$petugas = new ControlPetugas();

if (isset($_GET['fy'],$_GET['ty'])) {

	$data->datakegiatan = $petugas->LaporanKegiatanByDate($_GET['fy'],$_GET['ty']);
	$data->dataperan=$petugas->LaporanPeranByDate($_GET['fy'],$_GET['ty']);
	$data->dataperancurmonth = $petugas->LaporanPeranBulanini();
	$data->title = "Laporan Staff";
	$data->subtitle="Laporan Staff";
	@$data->username=$_SESSION['admin']['nama'];
	$data->totalrow = count($data->datakegiatan);
	$data->JustInclude('administrasi/view/laporan/functions');
	// print_r($data->datakegiatan);
	$data->JustInclude('administrasi/classes/Modal');
	$data->modal = new IModal(['modal_id' => 'modal-coba','modal_title' => 'Coba Modal','modal_content' => '','footer_modal' => 'FAC Institute']); 
	$data->nextlink="&fy=".$_GET['fy']."&ty=".$_GET['ty'];
	$data->datepicker='1';
	$data->JustInclude('administrasi/view/main-component/modal');
	// print_r($data->modal);
	$data->View('laporan/staff/dasbor.laporan',$data);
} else {
	$data->datakegiatan = $petugas->LaporanKegiatan();
	$data->dataperan=$petugas->LaporanPeran();
	$data->dataperancurmonth = $petugas->LaporanPeranBulanini();
	$data->title = "Laporan Staff";
	$data->subtitle="Laporan Staff";
	@$data->username=$_SESSION['admin']['nama'];
	$data->totalrow = count($data->datakegiatan);
	$data->JustInclude('administrasi/view/laporan/functions');
	$data->JustInclude('administrasi/classes/Modal');
	$data->modal = new IModal(['modal_id' => 'modal-coba','modal_title' => 'Coba Modal','modal_content' => '','footer_modal' => 'FAC Institute']); 
	$data->nextlink="";
	$data->datepicker='1';
	$data->JustInclude('administrasi/view/main-component/modal');
	$data->View('laporan/staff/dasbor.laporan',$data);	
}
	

?>