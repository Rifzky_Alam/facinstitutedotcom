<?php 
session_start();
include_once '../../classes/StaffReport.php';
$data = new Staff();

include_once $data->homedir.'administrasi/session/session-class.php';
$isessions= new Sessionz();
$isessions->IsSuperUser();

$data->Model('Petugas');
$petugas = new ControlPetugas();
$data->datepicker='1';
if (isset($_GET['usr'])&&!empty($_GET['usr'])) {
	
	
	$petugas->setUsername($_GET['usr']); //setting username before calling some actions on petugas model
	if (isset($_GET['fy'],$_GET['ty'])) {
		$data->title = "Laporan Staff";
		$data->subtitle="Laporan Staff Tanggal ".$data->GetTanggal($_GET['fy'])." Sd ".$data->GetTanggal($_GET['ty']);
		@$data->username=$_SESSION['admin']['nama'];
		
		$datastaff = $petugas->GetLaporanUserByDate($_GET['fy'],$_GET['ty']);
		$data->laporanacc = $petugas->LaporanJenisAccurateByDate($_GET['fy'],$_GET['ty']);
		$petugas->setNama($datastaff->nama);
		$data->laporantrans=$petugas->LaporanJenisTransaksiByDate($_GET['fy'],$_GET['ty']);
		$data->laporantransport = $petugas->LaporanJenisTransportasiByDate($_GET['fy'],$_GET['ty']);
		$data->laporankegiatan = $petugas->LaporanKegiatanStaffByDate($_GET['fy'],$_GET['ty']);
		$data->laporanjenisusaha = $petugas->LaporanJenisUsahaByDate($_GET['fy'],$_GET['ty']);
		$data->jabodetabek = $petugas->LaporanTotalTrainingJabodetabekByDate($_GET['fy'],$_GET['ty']);
		$data->luarkota = $petugas->LaporanLuarKotaByDate($_GET['fy'],$_GET['ty']);
		$data->nextlink="&fy=".$_GET['fy']."&ty=".$_GET['ty'];
	} else {
		$data->title = "Laporan Staff";
		$data->subtitle="Laporan Staff";
		@$data->username=$_SESSION['admin']['nama'];
		
		$datastaff = $petugas->GetLaporanUser();
		$data->laporanacc = $petugas->LaporanJenisAccurate();
		$petugas->setNama($datastaff->nama);
		$data->laporantrans=$petugas->LaporanJenisTransaksi();
		$data->laporantransport = $petugas->LaporanJenisTransportasi();
		$data->laporankegiatan = $petugas->LaporanKegiatanStaff();
		$data->laporanjenisusaha = $petugas->LaporanJenisUsaha();
		$data->jabodetabek = $petugas->LaporanTotalTrainingJabodetabek();
		$data->luarkota = $petugas->LaporanLuarKota();
		$data->nextlink="";	
	}
	$data->usr = $_GET['usr'];
	$data->namalengkap=$datastaff->nama;
	$data->tglbergabung = $data->GetTanggal($datastaff->created_at);
	$data->email=$datastaff->email;
	$data->telepon=$datastaff->telepon;
	$data->lahir=$data->GetTanggal($datastaff->lahir);
	$data->tipe=$datastaff->tipe;
	$data->team=$datastaff->ukt_ket;
	$data->totalabsen=$datastaff->total_absen;
	$data->totallaporan=$datastaff->total_laporan;
	$data->totalovertime=$datastaff->total_overtime;
	$data->statuskerja1=$datastaff->total_sendiri;
	$data->statuskerja2=$datastaff->total_ts;
	$data->totalreimbursement=$datastaff->total_reimbursement;


	$data->JustInclude('administrasi/classes/Modal');
	$data->modal = new IModal(['modal_id' => 'modal-coba','modal_title' => 'Cari Data Berdasarkan Tanggal','modal_content' => '','footer_modal' => 'FAC Institute']); 
	$data->JustInclude('administrasi/view/main-component/modal');

	$data->View('laporan/staff/vlaporan.personal',$data);

}elseif (isset($_GET['act'])&&$_GET['act']=='rmb') {
	if (isset($_GET['usr'])) {
		$petugas->setUsername($_GET['usr']);
		
		$data->title = "Laporan Staff";
		$data->subtitle="Laporan Reimbursment";
		@$data->username=$_SESSION['admin']['nama'];

		echo $data->biayatransport=number_format($petugas->GetSumLaporan('biaya_transport'));
		echo $data->makansiang=number_format($petugas->GetSumLaporan('biaya_lunch'));
		echo $data->makanmalam=number_format($petugas->GetSumLaporan('biaya_dinner'));
		echo $data->biayaparkir=number_format($petugas->GetSumLaporan('biaya_parkir'));
		echo $data->biayakesehatan=number_format($petugas->GetSumLaporan('biaya_kesehatan'));
		echo $data->biayaallowance=number_format($petugas->GetSumLaporan('biaya_allowance'));
		echo $data->biayalain=number_format($petugas->GetSumLaporan('biaya_lain'));
	}
		

}
	
?>