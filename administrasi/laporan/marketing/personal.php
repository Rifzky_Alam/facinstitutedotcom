<?php 
session_start();
include_once '../../classes/View.php';
$data = new Data();

include_once $data->homedir.'administrasi/session/session-class.php';
$isessions= new Sessionz();
$isessions->IsSuperUser();

$data->Model('Marketing');
$data->Model('Invoice');
$marketing = new Marketing();
$invoice = new Invoice();

$data->title = "Sales Report";
$data->subtitle="Laporan Sales";

if (isset($_GET['m'])) {
	$marketing->setID($_GET['m']);
	$datamarketing = $marketing->FetchByID();
	
	@$data->username=$_SESSION['admin']['nama'];
	$data->marketing = $datamarketing->marketing_nama;
	$data->tahun = date('Y');

	if (isset($_POST['thn'])) {
		$data->tahun = $_POST['thn'];	
	}
		
	$data->omset_jan = $invoice->CariUntung('omset',$_GET['m'],'1',$data->tahun);
	$data->omset_feb = $invoice->CariUntung('omset',$_GET['m'],'2',$data->tahun);
	$data->omset_mar = $invoice->CariUntung('omset',$_GET['m'],'3',$data->tahun);
	$data->omset_apr = $invoice->CariUntung('omset',$_GET['m'],'4',$data->tahun);
	$data->omset_mei = $invoice->CariUntung('omset',$_GET['m'],'5',$data->tahun);
	$data->omset_jun = $invoice->CariUntung('omset',$_GET['m'],'6',$data->tahun);
	$data->omset_jul = $invoice->CariUntung('omset',$_GET['m'],'7',$data->tahun);
	$data->omset_agu = $invoice->CariUntung('omset',$_GET['m'],'8',$data->tahun);
	$data->omset_sep = $invoice->CariUntung('omset',$_GET['m'],'9',$data->tahun);
	$data->omset_okt = $invoice->CariUntung('omset',$_GET['m'],'10',$data->tahun);
	$data->omset_nov = $invoice->CariUntung('omset',$_GET['m'],'11',$data->tahun);
	$data->omset_des = $invoice->CariUntung('omset',$_GET['m'],'12',$data->tahun);

	$data->net_jan = $invoice->CariUntung('net',$_GET['m'],'1',$data->tahun);
	$data->net_feb = $invoice->CariUntung('net',$_GET['m'],'2',$data->tahun);
	$data->net_mar = $invoice->CariUntung('net',$_GET['m'],'3',$data->tahun);
	$data->net_apr = $invoice->CariUntung('net',$_GET['m'],'4',$data->tahun);
	$data->net_mei = $invoice->CariUntung('net',$_GET['m'],'5',$data->tahun);
	$data->net_jun = $invoice->CariUntung('net',$_GET['m'],'6',$data->tahun);
	$data->net_jul = $invoice->CariUntung('net',$_GET['m'],'7',$data->tahun);
	$data->net_agu = $invoice->CariUntung('net',$_GET['m'],'8',$data->tahun);
	$data->net_sep = $invoice->CariUntung('net',$_GET['m'],'9',$data->tahun);
	$data->net_okt = $invoice->CariUntung('net',$_GET['m'],'10',$data->tahun);
	$data->net_nov = $invoice->CariUntung('net',$_GET['m'],'11',$data->tahun);
	$data->net_des = $invoice->CariUntung('net',$_GET['m'],'12',$data->tahun);

	$data->View('laporan/marketing/vrsalespersonal',$data);
}


	

	

	





?>