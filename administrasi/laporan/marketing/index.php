<?php 
session_start();
include_once '../../classes/View.php';
$data = new Data();

include_once $data->homedir.'administrasi/session/session-class.php';
$isessions= new Sessionz();
$isessions->IsSuperUser();

$data->Model('Marketing');
$marketing = new Marketing();
$data->datepicker='1';
if(isset($_GET['src'])&&!empty($_GET['src'])){

    $data->datamarketing = $marketing->CountInvoiceByDate($_GET['src']['tglawal'],$_GET['src']['tglakhir']);
    $data->datacabang = $marketing->CountInvoiceCabangByDate($_GET['src']['tglawal'],$_GET['src']['tglakhir']);
    $data->title = "Sales Report";
    $data->subtitle="Laporan Sales";
    @$data->username=$_SESSION['admin']['nama'];
    $data->totalrow = count($data->datamarketing);
    $data->JustInclude('administrasi/view/laporan/functions');
    $data->View('laporan/marketing/vrsalesinvoice',$data);
}else{
    
    $data->datamarketing = $marketing->CountInvoice();
    $data->datacabang = $marketing->CountInvoiceForCabang();
    $data->title = "Sales Report";
    $data->subtitle="Laporan Sales";
    @$data->username=$_SESSION['admin']['nama'];
    $data->totalrow = count($data->datamarketing);
    $data->JustInclude('administrasi/view/laporan/functions');
    $data->View('laporan/marketing/vrsalesinvoice',$data);
}
    

?>