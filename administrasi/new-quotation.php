<?php 
session_start();
include_once 'session/session-class.php';
include_once '../baseurl.php';
include_once '../model/Quotation.php';
$quot = new Quotation();
$session = new Sessionz();
$session->AdminMarketing();

if (isset($_GET['tr'])&&!empty($_GET['tr'])) {
	include_once '../model/Transaksi.php';

	if (isset($_POST['in'])) {
		// print_r($_POST['in']);
		$quot->setSubject($_POST['in']['subjek']);
		$quot->setTitle($_POST['in']['judul']);
		$quot->setNotes($_POST['in']['notes']);
		$quot->setIdtransaksi($_GET['tr']);
		$quot->setDate($_POST['in']['tanggal']);

		if ($quot->InputData()) {
			echo "<script>alert('Data penawaran berhasil disimpan!');</script>";
            echo "<script>location.replace('new-perusahaan-info?tr=".$_GET['tr']."');</script>";      
		} else {
			echo "<script>alert('Data Customer gagal disimpan!');</script>";
		}
		

	}

	$transaksi = new Transaksi();
	$transaksi->setID($_GET['tr']);
	$datatrans = $transaksi->FetchTransactionDataById();
	$data = array(
        'base_url' => getBaseUrl(), //base data
        'url'=> basename(__FILE__, '.php'),
        'judul' => 'New Quotation/FAC Institute',
        'header' => 'Input Penawaran Baru',
        'username'=>$_SESSION['admin']['nama'],
        'datepicker' =>1,
        'page' => 'tambah agenda', // end base data
        'nama_usaha' => $datatrans->nama,
        'id_transaksi' => $_GET['tr'],
        'petugas' => $_SESSION['admin']['nama']
    );

    $data = (object) $data;
	include_once 'view/quotation/view-input-quot.php';
}elseif(isset($_GET['edt'])&&!empty($_GET['edt'])) {
	$quot->setID($_GET['edt']);
	$dataquot = $quot->FetchByID();
	
	if (isset($_POST['in'])) {
		// print_r($_POST['in']);
		// $quot->setID();
		$quot->setSubject($_POST['in']['subjek']);
		$quot->setTitle($_POST['in']['judul']);
		$quot->setNotes($_POST['in']['notes']);
		$quot->setDate($_POST['in']['tanggal']);

		if ($quot->EditData()) {
			echo "<script>alert('Data penawaran berhasil diubah!');</script>";
            echo "<script>location.replace('new-perusahaan-info?tr=".$dataquot->qtn_id_trans."');</script>";      
		} else {
			echo "<script>alert('Data Customer gagal disimpan!');</script>";
		}

	}
	include_once '../model/Transaksi.php';
	$transaksi = new Transaksi();
	
	$transaksi->setID($dataquot->qtn_id_trans);
	$datatrans = $transaksi->FetchTransactionDataById();

	$data = array(
        'base_url' => getBaseUrl(), //base data
        'url'=> basename(__FILE__, '.php'),
        'judul' => 'New Quotation/FAC Institute',
        'header' => 'Input Penawaran Baru',
        'username'=>$_SESSION['admin']['nama'],
        'datepicker' =>1,
        'page' => 'tambah agenda', // end base data
        'nama_usaha' => $datatrans->nama,
        'id_transaksi' => $dataquot->qtn_id_trans,
        'subjek'=> $dataquot->qtn_subject,
        'judul'=> $dataquot->qtn_title,
        'notes'=> $dataquot->qtn_notes,
        'tanggal'=> $dataquot->qtn_date,
		'petugas' => $_SESSION['admin']['nama']
    );
    $data = (object) $data;
	include_once 'view/quotation/view-edit-quot.php';
}elseif(isset($_GET['send'])&&!empty($_GET['send'])) {
	include_once '../model/Mail.php';
	include_once 'view/quotation/quot-mail-view.php';
	include_once '../model/Quotation.php';

	//declaration class
    $mail = new FACMail();

	$quot = new Quotation();
	$quot->setID($_GET['send']);
	$datasurat = $quot->FetchDataSurat(); //assign data for email
	$dataquot = $quot->DisplayPDF();

	$path = getcwd().'/'.$quot->getFolder().'Penawaran '.$dataquot->qtn_id_trans.'.pdf';

	if(!file_exists($quot->getFolder().'Penawaran '.$dataquot->qtn_id_trans.'.pdf')){
		echo "<script>location.replace('new-attachments?quote=".$_GET['send']."');</script>";
	}

	//creating email content
	$obj1 = array('nama_item' => 'Tidak ada data yang dipilih','harga'=>'0','itm_qty'=> '0');
	$mylist = array();
	if (count($quot->FetchItems())=='0') {
		$mylist = array();
		array_push($mylist, (object) $obj1);
	}else{
		$mylist = $quot->FetchItems(); 
	}


	$data = array(
		'nama_cust' => $dataquot->nama_cust
	);
	$data['items'] = $mylist;
	$data['jenistr'] = $dataquot->trans_jenis;
	$data['nama_usaha']=$dataquot->nama_usaha;
	$data['petugas']=$dataquot->petugas;
	
	if ($data['jenistr']== '1') {
		$data['syarat'] = '';
		// $note = '';
		$data['notes'] = explode('##', $dataquot->qtn_notes);
	}elseif ($data['jenistr']== '2') {
		$data['syarat'] = array(
			'Entry data bersifat result oriented, bukan time oriented.',
			'Kami diberi akses ke sumber data yang akan dientry.'
		);
		// $note = 'iya##dah##terserah';
		$notes = explode('##', $dataquot->qtn_notes);
		foreach ($notes as $key) {
			array_push($data['syarat'], $key);
		}
		$data['notes'] = '';

	}elseif ($data['jenistr']== '3') {
		$data['syarat'] = '';
		// $note = 'kursus##akuntansi##accurate';
		$data['notes'] = explode('##', $dataquot->qtn_notes);
	}

	$data = (object) $data;
	$content = Penawaran($data);
	//end creating email content

	// -----------------------------------------------------------------------------

	//creating email object and attach files (not done yet)
	// $lampiran = explode("##", $datasurat->emd_attachments);

    $arr = array();
    array_push($arr, $quot->getFolder().'Penawaran '.$dataquot->qtn_id_trans.'.pdf');

    $lampiran = $quot->CekDataLampiran($datasurat->emd_attachments);
    if ($lampiran!='') {
    	for ($i=0; $i < count($lampiran) ; $i++) { 
	        array_push($arr, 'attachments/'.$lampiran[$i]);
	    }
    }
	    

    if (!empty($datasurat->emd_cc)||$datasurat->emd_cc!='') {
    	$cece = explode(" ", $datasurat->emd_cc);
    } else {
        $cece = '';
    }
    
    if ($dataquot->nama_usaha=='-') {
    	$judulpenawaran = $dataquot->nama_cust;
    } else {
    	$judulpenawaran = $dataquot->nama_usaha;
    }
    

    // data for email
    $dataEmail = array(
    'to' => $datasurat->emd_to,
    'cc'=> $cece,
    'nama'=> '',
    'subject'=> 'Penawaran '.$judulpenawaran,
    'content'=>$content,
    'attachments'=> $arr
);
    //change invoice status to 'sent'
    // $invoice->setStatus('1');
    if ($quot->EditStatus()) {
        $mail->KirimJadwal((object) $dataEmail);
        unlink($path);
        $data = array(
            'base_url' => getBaseUrl(), //base data
            'url'=> 'new-customer',
            'judul' => 'Edit Invoice/FAC Institute',
            'username'=>$_SESSION['admin']['nama'],
            'page' => 'tambah_customer', // end base data
            'id_transaksi' => $_GET['send'],
            'judul_berita' => 'Email telah terkirim!',
            'content_berita' => 'Invoice berhasil dikirim, anda dapat membuka email admin@fac-institute.com dalam beberapa menit untuk mengecek apakah invoice benar-benar terkirim ke tujuan. Bila terjadi kesalahan atau email tidak terkirim lebih dari 3 kali mencoba, harap menghubungi admin sistem.'
        );    
        $data = (object) $data;
        include_once 'view/invoice/mail-report.php';

    } else {
        echo "<script>alert('Terjadi kesalahan pada database, silahkan untuk refresh halaman ini!');</script>";    
    }
	//end creating email object and attach files

}



?>