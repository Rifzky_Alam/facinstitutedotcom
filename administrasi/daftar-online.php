<?php 
session_start();
include_once '../controller/client.controller.php';

if (isset($_SERVER['PATH_INFO'])) {
	@$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
	$key = array_shift($url_segment);
	switch ($key) {
		case '':
			header('Location: '.Client::base_url().'error/404');
			break;
		case 'listcust':
			Client::RegisteredClientData();
			break;
		case 'detailcust':
			if (!isset($url_segment[0])&&empty($url_segment[0])) {
				header('Location: '.Client::base_url().'error/404');
			}
			Client::DetailRegCust($url_segment[0]);
			break;
		case 'detailusaha':
			if (!isset($url_segment[0])&&empty($url_segment[0])) {
				header('Location: '.Client::base_url().'error/404');
			}
			Client::DetailRegCompany($url_segment[0]);
			break;
		default:
			header('Location: '.Client::base_url().'error/404');
			break;
	}
}else {
	header('Location: '.Client::base_url().'error/404');
}