<?php 
session_start();
if (!isset($_SESSION['admin'])||($_SESSION['admin']['tipe']!='admin'&&$_SESSION['admin']['tipe']!='marketing')) {
    header('Location:../login/index.php');
}
include_once 'write-html.php';
include_once '../../model/Pendaftar.php';
include_once '../../model/Surat.php';
include_once '../../model/Mail.php';

$pdf = new PDF_HTML();
$pendaftar = new Pendaftar();
$surat = new Surat();
$mail = new FACMail();


if (isset($_GET['quotval'])&&!empty($_GET['quotval'])) {
	
$pendaftar->updateQuotationStatus($_GET['quotval']);

$pdf->AddPage('p','A4',0);
//$pdf->myFooter();
$pdf->Image('../../images/logo-fac.jpg',35,10,50,30);
$pdf->Image('../../images/logoaccurate5.png',105,15,70,20);
$pdf->Image('stempelfac.jpg',50,207,30,20);

$q_data=$pendaftar->getQuotationData($_GET['quotval']);
$datas = array(
	'title' => $q_data->quotation_title,
	'no_quotation' => $q_data->id,
	'nama_usaha' => $q_data->nama_usaha,
	'nama_personal' => $q_data->nama_personal,
	'email' => $q_data->email,
	'subjek' => $q_data->subjek,
	'produk' => $q_data->nama_item,
	'jumlah_hari'=>$q_data->jumlah_hari,
	'harga' => $q_data->harga,
	'biaya_transport' => $q_data->biaya_transport,
	'qtytrans'=> $q_data->transport_qty,
	'petugas' => $q_data->petugas
);

$objekz = array(
	'nama_personal' => $q_data->nama_personal,
	'subject'=> $q_data->subjek,
	'nama_usaha' => $q_data->nama_usaha,
	'produk' => $q_data->nama_item,
	'biaya' => $q_data->harga * $q_data->jumlah_hari,
	'biaya_transport' => $q_data->biaya_transport * $q_data->transport_qty,
	'notes'=> $q_data->q_notes,
	'petugas' => $q_data->petugas
);


//convert all arrays into object (WOLOLO)
$objekz = (object) $objekz;
$datas = (object) $datas;
// end convert file
$content = $surat->quotation($objekz);

$lampir = array();
array_push($lampir, 'Quotation '.$q_data->nama_usaha.'.pdf');

if (!empty($q_data->attachments)) {
	$lampirz = explode(" # ", $q_data->attachments);
	for ($i=0; $i < count($lampirz); $i++) { 
		array_push($lampir, $lampirz[$i]);
	}
}

if (!empty($q_data->email_cc)||$q_data->email_cc!='') {
	$cece = explode(" ", $q_data->email_cc);
} else {
	$cece = '';
}


$dataEmail = array(
	'to' => $q_data->email,
	'nama'=> '',
	'subject'=> 'Penawaran '.$datas->nama_usaha,
	'cc'=> $cece,
	'content'=>$content,
	'attachments'=> $lampir
);
$dataEmail = (object) $dataEmail;

Kepala($pdf,$datas);


$pdf->Ln();
//opening is here
Opening($pdf,$datas);

$pdf->Ln();
$pdf->Ln();

Badan($pdf,$datas);



$pdf->Ln();
$pdf->Ln();
//transfer is here
Transfer($pdf);

//forms is here
FormQuotation($pdf);

//notes is here
Notes($pdf);


//signature is here
Signature($pdf,$datas);


//send output to display or to a file
// $pdf->Output();
	if(!file_exists('Quotation '.$datas->nama_usaha.'.pdf')){
		$pdf->Output('/home/facinsti/public_html/administrasi/pdf/Quotation '.$datas->nama_usaha.'.pdf','F');
		// $pdf->Output();
		$mail->KirimJadwal($dataEmail);
		unlink('Quotation '.$datas->nama_usaha.'.pdf');
		
		exit("<a href='../daftar-quotation'>Successfully sent an email.</a>");
	} else { 
		exit("<a href='../index'>Path Not Writable</a>");
	}


}



function Kepala($pdf,$data){
	$pdf->setY(42);
	$pdf->SetFont('Arial','BU',12.5);
	$pdf->Cell(185, 7, $data->title, 0, 0, "C");
	$pdf->Ln();
	$pdf->SetFont('Arial','',8);
	$pdf->Cell(180, 2, 'Nomor : '.$data->no_quotation, 0, 2, 'C');
	$pdf->Ln();
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(10);
	$pdf->Cell(30,5,'Kepada Yth,');
	$pdf->Cell(85);
	$pdf->Cell(30,5,'Jakarta, '.date('d-m-Y'));
	$pdf->Ln();
	$pdf->SetFont('Arial','',10);
	$pdf->Cell(10);
	$pdf->Cell(30,5,$data->nama_usaha);
	$pdf->Ln();
	$pdf->Cell(10);
	$pdf->Cell(10,5,'UP');
	$pdf->Cell(5);
	$pdf->Cell(5,5,':');
	//$pdf->Cell(5);
	$pdf->Cell(30,5,$data->nama_personal);
	$pdf->Ln();
	$pdf->Cell(10);
	$pdf->Cell(10,5,'Email');
	$pdf->Cell(5);
	$pdf->Cell(5,5,':');
	//$pdf->Cell(5);
	$pdf->Cell(30,5,$data->email);
	$pdf->Ln();
	$pdf->Cell(10);
	$pdf->Cell(30,5,'Dengan hormat,');

}

function Opening($pdf,$data){
	$pdf->SetLeftMargin(20);
	$pdf->WriteHTML('Sebelumnya kami ucapkan terimakasih atas ketertarikan menggunakan program ACCURATE di <b>'.$data->nama_usaha.'</b> Untuk <b>'.$data->subjek.'</b>, Kami memberikan penawaran <b>TERBAIK</b> berupa:');
}

function Badan($pdf,$data){
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(60,5,'Nama Produk',1,0,'C');
	$pdf->Cell(60,5,'Harga',1,0,'C');
	$pdf->Cell(20,5,'Qty',1,0,'C');
	$pdf->Cell(30,5,'Jumlah',1,0,'C');
	$pdf->Ln();
	$pdf->SetFont('Arial','',10);
	$pdf->SetFillColor(255,255,255); //set background color for multi cell

	$pdf->SetWidths(array(60,60,20,30));
	$pdf->SetAligns(array('L','R','C','C'));

	if ($data->jumlah_hari!='1') {
		$total1 = $data->harga * $data->jumlah_hari;
		$pdf->Row(array($data->produk,'Rp '.number_format($data->harga),$data->jumlah_hari.' hari',number_format($data->harga * $data->jumlah_hari)));
	} else {
		$pdf->Row(array($data->produk,'Rp '.number_format($data->harga),"....hari",""));
	}	

	//$pdf->MultiCell(60,5,'Rp 1.000.000.-',1,'C');
	//$pdf->MultiCell(20,5,'....hari',1,'C');
	//$pdf->Cell(30,5,'',1,0,'C');
	//$pdf->Ln();
	if ($data->qtytrans!='1') {
		$total2=$data->biaya_transport * $data->qtytrans;
		$pdf->Cell(60,5,'Biaya Transportasi per Hari',1,0,'L');
		$pdf->Cell(60,5,'Rp '.number_format($data->biaya_transport),1,0,'R');
		$pdf->Cell(20,5,$data->qtytrans . ' hari',1,0,'C');
		$pdf->Cell(30,5,number_format($data->biaya_transport * $data->qtytrans),1,0,'C');	
	} else {
		$pdf->Cell(60,5,'Biaya Transportasi per Hari',1,0,'L');
		$pdf->Cell(60,5,'Rp '.number_format($data->biaya_transport),1,0,'R');
		$pdf->Cell(20,5,'....hari',1,0,'C');
		$pdf->Cell(30,5,'',1,0,'C');
	}

	$pdf->Ln();
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(60,5,'Total',1,0,'L');
	$pdf->Cell(60,5,'',1,0,'C');
	$pdf->Cell(20,5,'',1,0,'C');
	if ($data->jumlah_hari!='1'){
		$pdf->Cell(30,5,'Rp '.number_format($total1+$total2),1,0,'C');
	}else{
		$pdf->Cell(30,5,'',1,0,'C');
	}
	
}



function Transfer($pdf){
	$pdf->WriteHTML('<b>[  ] Transfer via Bank BCA KCU Bekasi No. Acc. 0663162851  a/n : Fajar Shodiq</b><br>');
	$pdf->WriteHTML('<b>[  ] Transfer via Bank Mandiri KCP Bekasi Ahmad Yani No. Acc. 1670001377901  a/n : Fajar Shodiq</b><br>');
}

function FormQuotation($pdf){
	$pdf->WriteHTML('<b>Untuk kebutuhan informasi, mohon diisi data berikut dengan lengkap.</b><br>');
	$pdf->Ln();
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(10,5,'Nama Perusahaan');
	$pdf->Cell(35);
	$pdf->Cell(5,5,':');
	$pdf->Cell(100,5,'............................................................................................................');
	$pdf->Ln();
	$pdf->Cell(10,5,'Jenis Usaha');
	$pdf->Cell(35);
	$pdf->Cell(5,5,':');
	$pdf->Cell(100,5,'............................................................................................................');
	$pdf->Ln();
	$pdf->Cell(10,5,'Alamat Perusahaan');
	$pdf->Cell(35);
	$pdf->Cell(5,5,':');
	$pdf->Cell(100,5,'............................................................................................................');
	$pdf->Ln();
	$pdf->Cell(10,5,'Waktu Pelaksanaan');
	$pdf->Cell(35);
	$pdf->Cell(5,5,':');
	$pdf->Cell(100,5,'............................................................................................................');
	$pdf->Ln();
	$pdf->Cell(10,5,'Varians Accurate');
	$pdf->Cell(35);
	$pdf->Cell(5,5,':');
	$pdf->Cell(30,5,'Standar/Deluxe/Enterprise');
	$pdf->SetFont('Arial','B',5);
	$pdf->Cell(15);
	$pdf->Cell(30,5,'* Coret yang tidak perlu');
	$pdf->Ln();
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(10,5,'Agenda/Kegiatan Training');
	$pdf->Cell(35);
	$pdf->Cell(5,5,':');
	$pdf->Cell(100,5,'1 ............................................................................................................',0,1);
	$pdf->Cell(50);
	$pdf->Cell(100,5,'2 ............................................................................................................',0,1);

}

function Notes($pdf){
	$pdf->SetFont('Arial','',10);
	$pdf->Cell(10,5,'Notes');
	$pdf->SetFont('Arial','',8);
	$pdf->Ln();
	$pdf->Cell(100,5,'- Mendapatkan support by email.',0,1);
	$pdf->Cell(100,5,'- Pembayaran dilakukan sebelum implementasi dimulai.',0,1);
	$pdf->Cell(100,5,'- Menyediakan makan siang bagi implementator.',0,1);
	$pdf->Cell(100,5,'- Implementasi maksimal 7 jam termasuk istirahat makan siang, training bersifat time oriented, bukan result oriented.',0,1);
	$pdf->Cell(100,5,'- Implementasi diluar Jabodetabek, transport dan akomodasi ditanggung customer.',0,1);
}

function ClosingText($pdf){
	$pdf->SetFont('Arial','',10);
	$pdf->Ln();
	$pdf->MultiCell(175,5,'Silakan isi kuantitas yang Anda butuhkan di tabel diatas, dan setelah membubuhkan tanda tangan/stempel perusahaan di bawah sebagai persetujuan pemesanan Implementasi mohon discan dan diemail kembali. Demikian disampaikan, atas perhatian dan kerjasamanya kami ucapkan banyak terima kasih.');
}

function Signature($pdf,$data){
	$pdf->Ln();
	$pdf->Cell(87.5,5,'Hormat  kami,',0,0,'C');
	$pdf->Cell(87.5,5,'Pemesan,',0,0,'C');
	$pdf->Ln();
	$pdf->Ln();

	$pdf->Ln();
	$pdf->SetFont('Arial','BU',10);
	$pdf->Cell(87.5,5,$data->petugas,0,0,'C');
	$pdf->Cell(87.5,5,$data->nama_usaha.',',0,0,'C');
	$pdf->Ln();
	$pdf->SetFont('Arial','B',6);
	$pdf->Cell(87.5,2,'FAC Institute',0,0,'C');
	$pdf->Ln();
	$pdf->Ln();
	$pdf->Ln();
}


?>