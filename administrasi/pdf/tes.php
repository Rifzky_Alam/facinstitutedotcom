<?php 
include_once '../../model/Mail.php';
include_once '../../model/Surat.php';
include_once '../../model/Pendaftar.php';

$pendaftar = new Pendaftar();
$mail = new FACMail();
$surat = new Surat();

$el_data=$pendaftar->fetchDataRequestByID('c604f845fe20b047997b3e88b262d9ec');
$dataPerusahaan = json_decode($el_data->nama_perusahaan);
$dataJadwal = json_decode($el_data->data_jadwal);
$dataInvoice = json_decode($el_data->temp_invoice);
$paketTraining = json_decode($pendaftar->getPaketTraining($dataInvoice->paket));

$data = array(
        'to' => $el_data->email_kontak,
        'nama'=> $el_data->nama_personal,
        'subject'=>'Penawaran Training Software Accurate',
        'perusahaan'=>$dataPerusahaan->nama,
        'tanggalz'=>explode(' # ', $dataJadwal->tanggal),
        'agenda'=>explode(" # ",$el_data->agenda_training),
        'waktu'=>$dataJadwal->waktu,
        'tempat'=>@$dataJadwal->tempat,
        'lokasi'=>$el_data->alamat_perusahaan,
        'harga'=>$paketTraining[0]->harga,
        'x'=>$dataInvoice->x,
        'transport'=>$dataInvoice->transport,
        'jmlhari'=>$dataInvoice->jml_hari,
        'total'=>850000,
        'officer'=>'si fulan',
);
$content= $surat->penawaran((object)$data);
$lampiran = explode(" # ", $dataInvoice->attach);
$arr = array();

for ($i=0; $i < count($lampiran) ; $i++) { 
	if (preg_match('/.pdf/i', $lampiran[$i])){
		array_push($arr, '../attachments/'.$lampiran[$i]);
	}
}



//print_r($arr);


$dataEmail = array(
	'to' => 'rifzky.mail@gmail.com',
	'nama'=> '',
	'subject'=> 'Penawaran Training FAC-Institute',
	'content'=>$content,
	'attachments'=> $arr
);

$mail->testMailAttachments((object) $dataEmail);

/*
$data = (object) $dataEmail;

$attachments = $data->attachments;
for ($i=0; $i < count($attachments) ; $i++) { 
	if (!empty($attachments[$i])) {
		echo $attachments[$i];
	}
}
*/

?>