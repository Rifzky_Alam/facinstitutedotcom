<?php 
require_once '../dompdf/autoload.inc.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;
// instantiate and use the dompdf class
$dompdf = new Dompdf();


$content='

<html>
<head>
	<title>Rifzky Alam</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="page-header">
				<img src="images/logo-fac.jpg" style="width:80px;height:50px;"/>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-10">
			<button class="btn btn-primary">Submit</button>
		</div>
	</div>
</body>
</html>



';


$dompdf->loadHtml($content);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'potrait');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream('my.pdf',array('Attachment'=>0));
//$output = $dompdf->output();
//file_put_contents('Brochure.pdf', $output);
?>

