<?php 
session_start();
if (!isset($_SESSION['admin'])||($_SESSION['admin']['tipe']!='admin'&&$_SESSION['admin']['tipe']!='marketing')) {
    header('Location:../login/index.php');
}
?>




<!DOCTYPE html>
<html>

<?php 
//echo $tipeAdmin=$_SESSION['admin']['tipe'];
$judul='Daftar Invoice FAC Institute';
$page = 'home';
include_once 'header.php';
include_once '../model/Pendaftar.php';
$pendaftar = new Pendaftar();
?>

<body>



<?php 

include_once 'sidebar.php';

include_once 'top-nav.php';

 ?>
<?php if (isset($_GET['ac'])&&$_GET['ac']=='edtstat'&&isset($_GET['ni'])&&!empty($_GET['ni'])): ?>
    <?php 

    if ($_SESSION['admin']['tipe']=='admin') {
        if ($pendaftar->updateStatusInvoice($_GET['ni'],'2')) {
            echo "<script>alert('Invoice No. ".$_GET['ni']." telah terbayar');</script>";
            echo "<script>location.replace('".basename(__FILE__, '.php')."');</script>";
        }else{
            echo "<script>alert('Data gagal diubah, Kontak admin sistem!');</script>";
        }
    }else{
        echo "<script>alert('Anda tidak memiliki privasi untuk mengubah data ini!');</script>";
    }

    ?>
<?php endif ?>


<?php if (isset($_GET['ac'])&&$_GET['ac']=='edtztat'&&isset($_GET['ni'])&&!empty($_GET['ni'])): ?>
    <?php 

    if ($_SESSION['admin']['tipe']=='admin') {
        
        if ($pendaftar->updateStatusInvoice($_GET['ni'],'1')) {
            echo "<script>alert('Status invoice No. ".$_GET['ni']." telah diubah!');</script>";
        } else {
            echo "<script>alert('Status invoice gagal diubah, Kontak admin sistem!');</script>";
        }

    } else {
        echo "<script>alert('Anda tidak memiliki privasi untuk mengubah data ini!');</script>";
    }
    
        



    ?>
<?php endif ?>

<?php if (isset($_GET['ac'])&&$_GET['ac']=='cancelinv'&&isset($_GET['ni'])&&!empty($_GET['ni'])): ?>
    <?php 

    if ($_SESSION['admin']['tipe']=='admin') {
        if ($pendaftar->updateStatusInvoice($_GET['ni'],'-1')) {
            echo "<script>alert('Invoice No. ".$_GET['ni']." telah di batalkan');</script>";
            echo "<script>location.replace('".basename(__FILE__, '.php')."');</script>";
        }else{
            echo "<script>alert('Data gagal diubah, Kontak admin sistem!');</script>";
        }
    }else{
        echo "<script>alert('Anda tidak memiliki privasi untuk mengubah data ini!');</script>";
    }

    ?>
<?php endif ?>

<?php if (isset($_GET['ac'])&&$_GET['ac']=='npaid'&&isset($_GET['ni'])&&!empty($_GET['ni'])): ?>
    <?php 

    if ($_SESSION['admin']['tipe']=='admin') {
        if ($pendaftar->updateStatusInvoice($_GET['ni'],'1')) {
            echo "<script>alert('Invoice No. ".$_GET['ni']." belum terbayar');</script>";
            echo "<script>location.replace('".basename(__FILE__, '.php')."');</script>";
        }else{
            echo "<script>alert('Data gagal diubah, Kontak admin sistem!');</script>";
        }
    }else{
        echo "<script>alert('Anda tidak memiliki privasi untuk mengubah data ini!');</script>";
    }
    ?>
<?php endif ?>


<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3>Daftar Invoice FAC Institute</h3>
	</div>

    <div class="row">
        <div class="col-md-12" style="text-align:right;">
            <a class="btn btn-sm btn-primary" data-toggle="modal" href="#modal-cari">Search <span class="glyphicon glyphicon-search"></span></a>
        </div>
    </div>
        <?php 
        if (isset($_GET['page'])&&!empty($_GET['page'])) {
            $datas=json_decode($pendaftar->getDaftarPendingInvoice($_GET['page']));
        }elseif (isset($_GET['np'],$_GET['nk'],$_GET['nmpkt'],$_GET['di'],$_GET['fd'],$_GET['ld'])) {
            $objek = array(
                'np' => $_GET['np'],
                'nc' => $_GET['nk'],
                'ni' => $_GET['nmpkt'],
                'di' => $_GET['di'],
                'st' => $_GET['st']
            );

            $objek = (object) $objek;
            $datas=json_decode($pendaftar->srcPendingInvoice($objek));
        }elseif (isset($_GET['iv'])&&!empty($_GET['iv'])) {
            $datas=$pendaftar->getPendingInvoice($_GET['iv']);
        }else{
            $datas=json_decode($pendaftar->getDaftarPendingInvoice('1'));
        }        

        ?>
        <div class="row" style="overflow:auto;">
        <div class="col-md-12">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>No Invoice</th>
                        <th>Nama Perusahaan</th>
                        <th>Nama Kontak</th>
                        <th>Nama Paket</th>
                        <th>Deskripsi</th>
                        <th>Jumlah</th>
                        <th>Tgl Kirim</th>
                        <th>Aksi</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                <?php  for ($i=0; $i < count($datas) ; $i++) { ?> 
                    <?php if ($datas[$i]->status_invoice=='2'): ?>
                        <tr class="success">
                        <?php elseif($datas[$i]->status_invoice=='1'): ?>
                        <tr class="warning">
                        <?php elseif($datas[$i]->status_invoice=='-1'): ?>
                        <tr style="background-color:#660066;color:#fff">
                        <?php else: ?>
                        <tr class="danger">
                    <?php endif ?>
                    
                        <td><?php echo $datas[$i]->no_invoice ?></td>
                        <td><a href="detail?data=<?php echo $datas[$i]->id_daftar ?>"><?php echo $datas[$i]->nama ?></a></td>
                        <td><?php echo $datas[$i]->nama_personal ?></td>
                        <td><?php echo $datas[$i]->nama_item ?></td>
                        <td><?php echo $datas[$i]->deskripsi ?></td>
                        <?php 
                        $myAmount = intval($datas[$i]->harga) * intval($datas[$i]->qty); 
                        $transport = intval($datas[$i]->biaya_trans) * intval($datas[$i]->qty_trans);
                        ?>
                        <td><?php echo number_format( $myAmount + $transport ) ?></td>
                        <td><?php echo $datas[$i]->sent_date ?></td>
                        <td>
                            <a href="<?php echo 'edit-order?v='.$datas[$i]->id_daftar ?>" target="_blank">Edit transaksi</a> || 
                            <a href="preview?an=ji&id=<?php echo $datas[$i]->id_agenda ?>" target="_blank">Preview</a> || 
                            <a href="<?php echo getBaseUrl().'docs/invoice-baru?inv='.$datas[$i]->id_agenda ?>" target="_blank">PDF</a> 
                            <?php if ($_SESSION['admin']['tipe']=='admin'): ?>
                                ||
                                <a href="<?php echo 'pdf/invoiceadmin?inv='.$datas[$i]->no_invoice ?>">Kirim</a>    
                            <?php endif ?>
                        </td>
                        <td class="tengah">
                            <?php if ($datas[$i]->status_invoice=='2'): ?>
                                <span class="glyphicon glyphicon-check"></span> || 
                                <a href="<?php echo basename(__FILE__, '.php').'?ac=npaid&ni='.$datas[$i]->no_invoice ?>"><span class="glyphicon glyphicon-remove"></span></a>
                            <?php elseif($datas[$i]->status_invoice=='1'): ?>
                                <a href="<?php echo basename(__FILE__, '.php').'?ac=edtstat&ni='.$datas[$i]->no_invoice ?>">
                                    Mark as Paid
                                </a> || 
                                <a href="<?php echo basename(__FILE__, '.php').'?ac=cancelinv&ni='.$datas[$i]->no_invoice ?>">Mark as canceled</a>


                            <?php elseif($datas[$i]->status_invoice=='0'&&intval($datas[$i]->jumlah_training)>0): ?>
                                <a href="<?php echo basename(__FILE__, '.php').'?ac=edtztat&ni='.$datas[$i]->no_invoice ?>">Mark as sent </a> ||
                            <?php elseif($datas[$i]->status_invoice=='-1'): ?>
                            To change this, contact administrator!
                            <?php else: ?>
                                not sent yet <?= $datas[$i]->jumlah_training ?> || <a href="<?php echo basename(__FILE__, '.php').'?ac=cancelinv&ni='.$datas[$i]->no_invoice ?>">Mark as canceled</a>
                            <?php endif ?>
                        </td>
                    </tr>    
                <?php } ?>
                </tbody>
            </table>            
        </div>
    </div>
    <?php $totalRow = $pendaftar->getNumberInvoice(); ?>
    <?php if (isset($_GET['page'])&&!empty($_GET['page'])): ?>
        
        <div class="row">
            <div class="col-md-12">
                <center>
                    Showing data <?php 
                        if (intval($_GET['page'])*30 < $totalRow) {
                            echo (intval($_GET['page'])*30)-30 . ' - ' . intval($_GET['page'])*30 . ' of '.$totalRow; 
                        } else {
                            echo (intval($_GET['page'])*30)-30 . ' - ' . $totalRow . ' rows of '.$totalRow;
                        }

                    ?>
                </center>
            </div>
        </div>

    <?php else: ?>

        <div class="row">
            <div class="col-md-12">
                <center>
                    Showing data 0 - 30 rows of <?php echo $totalRow; ?>
                </center>
            </div>
        </div>



    <?php endif ?>
    

    <div class="row">
        <div class="col-md-12">
        <center>
            <ul class="pagination pagination-sm">

            <?php 

                $limit = 5;
                if (isset($_GET['page'])) {
                    $page = $_GET['page'];
                } else {
                    $page = 0;
                }
                
                
                $b = intval($page/$limit);
                $c = ($b + 1) * 5;
                $batas = intval($totalRow) / 30;

                if ($page >= $limit) {
                    if ($page == $limit) {
                        $prev = $b * $limit - 2;
                    } else {
                        $prev = $b * $limit -1;
                    }

                    echo "<li><a href='".basename(__FILE__, '.php')."?page=$prev'>< Prev</a></li>";
                }

                for ($i=$b*$limit-1; $i < $c; $i++) { 
                    $j = $i +1;

                    if ($i<$batas&&$j!=0) {
                        echo "<li><a href='".basename(__FILE__, '.php')."?page=$j'>$j</a></li>";    
                    }
                }
                
                if ($c < $batas) {
                    $k = $c + 1;
                    echo "<li><a href='".basename(__FILE__, '.php')."?page=$k'>Next ></a></li>";
                }
                
                // for ($i=1; $i < intval($totalRow) / 30 + 1 ; $i++) { 
                    // echo "<li><a href='".basename(__FILE__, '.php')."?page=$i'>$i</a></li>";   
                // }
            ?>
            </ul>
        </center>
        </div>
    </div>
	
</div>

<!-- Modal -->
  <div class='modal fade' id='modal-cari' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pencarian Data</h3>

        </div>
        <div class='modal-body'>
        <div style='padding-left:15px;padding-right:15px;padding-top:0px; height: 300px;overflow-y:auto;' class='row'>
          


          <div class="col-md-12">
            <form action="" method="get">
            <div class="row">
                <div class="form-group">
                    <label>Nama Perusahaan</label>
                    <input type="text" class="form-control" name="np">
                </div>
                <div class="form-group">
                    <label>Nama Personal Kontak</label>
                    <input type="text" class="form-control" name="nk">
                </div>
                <div class="form-group">
                    <label>Nama Paket</label>
                    <input type="text" class="form-control" name="nmpkt">
                </div>
                <div class="form-group">
                    <label>Deskripsi Invoice</label>
                    <input type="text" class="form-control" name="di">
                </div>
                <div class="form-group">
                    <label>Tanggal Awal</label>
                    <input type="text" class="form-control" name="fd">
                </div>
                <div class="form-group">
                    <label>Tanggal Akhir</label>
                    <input type="text" class="form-control" name="ld">
                </div>
                <div class="form-group">
                    <label>Status</label>
                    <select name="st" class="form-control">
                        <option value="-" selected>--pilih status invoice--</option>
                        <option value="1">Belum Terbayar</option>
                        <option value="2">Sudah Terbayar</option>
                        <option value="0">Belum terkirim</option>
                        <option value="-1">Dibatalkan</option>
                    </select>
                </div>
            </div>

            <div class="row">
                <button class="btn btn-lg btn-success" style="width:100%">Cari</button>
            </div> 
            </form>
          </div>
             
          

        </div>
          

        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>FAC-Institute 2016</span>
                </div>
                <div class="col-md-6">
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->


</body>
</html> 