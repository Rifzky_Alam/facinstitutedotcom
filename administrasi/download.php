<?php 
session_start();

if (isset($_GET['type'])&&$_GET['type']=='laporan'){
	if (isset($_GET['tanggalAwal'])&&isset($_GET['tanggalAkhir'])&&isset($_GET['nama'])) {
		
	
		include_once '../model/Laporan.php'; 
		include_once '../model/Petugas.php';
		$petugas = new ControlPetugas();
		$ctrl_laporan = new ControlLaporan();

		$dataSelect = $petugas->fetchUsernameAndName();

		$dataPegawai = array();
		
        for ($i=0; $i < count($dataSelect) ; $i++) { 
        	array_push($dataPegawai,array($dataSelect[$i]->username,$dataSelect[$i]->nama));
        }

		
		$datas = json_decode($ctrl_laporan->downloadLaporan($_GET['nama'],$_GET['tanggalAwal'],$_GET['tanggalAkhir']));
		$namaFile=date('d-m-Y');

		//body of page

		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment;filename=Laporan '.$namaFile.' '.$_GET['username'].'.csv');

		//create a file pointer connected to the output stream
		$output=fopen('php://output', 'w');

		fputcsv($output, array('Tanggal','Nama','Hari','Jenis Hari Kerja','Jam Masuk','Jam Pulang','Overtime','Klien','Keterangan Kegiatan','Perserta Kegiatan','Status Kerja','Peran','Rekan','Jenis Transportasi','Biaya Transport','Keterangan Transport','Biaya Parkir','Biaya Makan Siang','Biaya Makan Malam','Biaya Kesehatan', 'Biaya Allowance', 'Biaya Lain', 'Keterangan Biaya Lain','Waktu'),";");
		
		for ($i=0; $i < count($datas); $i++) { 
			$myRekan = explode(" # ",$datas[$i]->rekan);
			$dataRekan = array();
			for ($j=0; $j < count($myRekan) ; $j++) { 
				for ($k=0; $k < count($dataPegawai); $k++) { 
					if ($dataPegawai[$k][0]==$myRekan[$j]) {
						array_push($dataRekan, $dataPegawai[$k][1]);
					}
				}
			}
			$realRekan = implode(" # ", $dataRekan);

			$row = array(
							$datas[$i]->tanggal,
							$datas[$i]->nama,
							$datas[$i]->hari,
							$datas[$i]->jns_hari_kerja,
							$datas[$i]->jam_mulai,
							$datas[$i]->jam_selesai,
							$datas[$i]->jumlah_overtime,
							$datas[$i]->id_klien,
							$datas[$i]->ket_kegiatan,
							$datas[$i]->peserta_kegiatan,
							$datas[$i]->status_kerja,
							$datas[$i]->peran,
							$realRekan,
							$datas[$i]->jns_transportasi,
							$datas[$i]->biaya_transport,
							$datas[$i]->ket_transport,
							$datas[$i]->biaya_parkir,
							$datas[$i]->biaya_lunch,
							$datas[$i]->biaya_dinner,
							$datas[$i]->biaya_kesehatan,
							$datas[$i]->biaya_allowance,
							$datas[$i]->biaya_lain,
							$datas[$i]->ket_biaya_lain,
							$datas[$i]->waktu_lapor
						);
			fputcsv($output, $row,";");

		}
	//end body
	}
}


if (isset($_GET['type'])&&$_GET['type']=='1') {
	if (isset($_GET['tanggalAwal'],$_GET['tanggalAkhir'],$_GET['username'])) {
		if ($_SESSION['admin']['username']==$_GET['username']) {
		 	

			include_once '../model/Laporan.php'; 
			$ctrl_laporan = new ControlLaporan();
			$datas = json_decode($ctrl_laporan->downloadLaporan($_GET['username'],$_GET['tanggalAwal'],$_GET['tanggalAkhir']));
			$namaFile=date('d-m-Y');

			//body of page

			header('Content-Type: text/csv; charset=utf-8');
			header('Content-Disposition: attachment;filename=Laporan '.$namaFile.' '. $_GET['username'] .'.csv');

			//create a file pointer connected to the output stream
			$output=fopen('php://output', 'w');

			fputcsv($output, array('Tanggal','Nama','Hari','Jenis Hari Kerja','Jam Masuk','Jam Pulang','Overtime','Klien','Keterangan Kegiatan','Perserta Kegiatan','Status Kerja','Peran','Rekan','Jenis Transportasi','Biaya Transport','Keterangan Transport','Biaya Parkir','Biaya Makan Siang','Biaya Makan Malam','Biaya Kesehatan', 'Biaya Allowance', 'Biaya Lain', 'Keterangan Biaya Lain','Waktu'),";");
			
			for ($i=0; $i < count($datas); $i++) { 
				$row = array(
								$datas[$i]->tanggal,
								$datas[$i]->nama,
								$datas[$i]->hari,
								$datas[$i]->jns_hari_kerja,
								$datas[$i]->jam_mulai,
								$datas[$i]->jam_selesai,
								$datas[$i]->jumlah_overtime,
								$datas[$i]->id_klien,
								$datas[$i]->ket_kegiatan,
								$datas[$i]->peserta_kegiatan,
								$datas[$i]->status_kerja,
								$datas[$i]->peran,
								$datas[$i]->rekan,
								$datas[$i]->jns_transportasi,
								$datas[$i]->biaya_transport,
								$datas[$i]->ket_transport,
								$datas[$i]->biaya_parkir,
								$datas[$i]->biaya_lunch,
								$datas[$i]->biaya_dinner,
								$datas[$i]->biaya_kesehatan,
								$datas[$i]->biaya_allowance,
								$datas[$i]->biaya_lain,
								$datas[$i]->ket_biaya_lain,
								$datas[$i]->waktu_lapor
							);
				fputcsv($output, $row,";");

			}
			//end body

		 }else{
		 	echo "Anda tidak memiliki otoritas untuk mengunduh file ini!";
		 } 
	}
}

if (isset($_GET['type'])&&$_GET['type']=='laporanAdmin'){
	if (isset($_GET['tanggalAwal'])&&isset($_GET['tanggalAkhir'])&&isset($_GET['nama'])) {


		include_once '../model/Laporan.php'; 
		include_once '../model/Petugas.php';
		$petugas = new ControlPetugas();
		$ctrl_laporan = new ControlLaporan();

		$dataSelect = $petugas->fetchUsernameAndName();

		$dataPegawai = array();
		
        for ($i=0; $i < count($dataSelect) ; $i++) { 
        	array_push($dataPegawai,array($dataSelect[$i]->username,$dataSelect[$i]->nama));
        }

		
		$datas = json_decode($ctrl_laporan->downloadLaporanAdmin($_GET['tanggalAwal'],$_GET['tanggalAkhir'],$_GET['nama']));
		$namaFile=date('d-m-Y');

		//body of page

		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment;filename=Laporan '.$namaFile.'.csv');

		//create a file pointer connected to the output stream
		$output=fopen('php://output', 'w');

		fputcsv($output, array('Tanggal','Nama','Hari','Jenis Hari Kerja','Jam Masuk','Jam Pulang','Overtime','Klien','Keterangan Kegiatan','Perserta Kegiatan','Status Kerja','Peran','Rekan','Jenis Transportasi','Biaya Transport','Keterangan Transport','Biaya Parkir','Biaya Makan Siang','Biaya Makan Malam','Biaya Kesehatan', 'Biaya Allowance', 'Biaya Lain', 'Keterangan Biaya Lain','Waktu'),";");
		
		for ($i=0; $i < count($datas); $i++) { 
			$myRekan = explode(" # ",$datas[$i]->rekan);
			$dataRekan = array();
			for ($j=0; $j < count($myRekan) ; $j++) { 
				for ($k=0; $k < count($dataPegawai); $k++) { 
					if ($dataPegawai[$k][0]==$myRekan[$j]) {
						array_push($dataRekan, $dataPegawai[$k][1]);
					}
				}
			}
			$realRekan = implode(" # ", $dataRekan);

			$row = array(
							$datas[$i]->tanggal,
							$datas[$i]->nama,
							$datas[$i]->hari,
							$datas[$i]->jns_hari_kerja,
							$datas[$i]->jam_mulai,
							$datas[$i]->jam_selesai,
							$datas[$i]->jumlah_overtime,
							$datas[$i]->id_klien,
							$datas[$i]->ket_kegiatan,
							$datas[$i]->peserta_kegiatan,
							$datas[$i]->status_kerja,
							$datas[$i]->peran,
							$realRekan,
							$datas[$i]->jns_transportasi,
							$datas[$i]->biaya_transport,
							$datas[$i]->ket_transport,
							$datas[$i]->biaya_parkir,
							$datas[$i]->biaya_lunch,
							$datas[$i]->biaya_dinner,
							$datas[$i]->biaya_kesehatan,
							$datas[$i]->biaya_allowance,
							$datas[$i]->biaya_lain,
							$datas[$i]->ket_biaya_lain,
							$datas[$i]->waktu_lapor
						);
			fputcsv($output, $row, ";");

		}
	//end body
	}
}


?>