<?php 
session_start();

if (isset($_SERVER['PATH_INFO'])) {
	@$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
	$key = array_shift($url_segment);
	switch ($key) {
		case 'outstanding':
		    include_once '/home/facinsti/public_html/administrasi/controller/transaksi.controller.php';
		    Transaksi::TabelOutstanding();
		break;
		case 'items':
			include_once '/home/facinsti/public_html/administrasi/controller/transaksi.controller.php';
		    Transaksi::TabelItems();
			break;
        case 'api':
            if (isset($_GET['getemails'])&&!empty($_GET['getemails'])) { // $_GET['getemails'] value is transaction ID
                include_once '/home/facinsti/public_html/administrasi/controller/transaksi.controller.php';
                Transaksi::GetEmailsTrans($_GET['getemails']);
            }elseif (isset($_GET['getphones'])&&!empty($_GET['getphones'])) { // $_GET['getphones'] value is trans ID
                include_once '/home/facinsti/public_html/administrasi/controller/transaksi.controller.php';
                Transaksi::GetPhonesTrans($_GET['getphones']);
            }
            break;
        case 'feedback':
            if(count($url_segment)==1){
                include_once '/home/facinsti/public_html/administrasi/controller/transaksi.controller.php';
                Transaksi::DaftarTrainerUtama($url_segment[0]);
            }
            else{
                header('location: https://fac-institute.com/administrasi/data-transaksi');
            }
            break;
	}
}else{
	include_once '../baseurl.php';
    include_once 'session/session-class.php';
    include_once '../model/Transaksi.php';
    $transaksi = new Transaksi();
    $session = new Sessionz();
    $session->AdminMarketing();
    
    if (isset($_GET['src'])) {
    	$offset = 0;
    	$limit = 30;
    	
    	$search = array(
    		'nama_usaha' => $_GET['src']['np'],
    		'nama_cust' =>$_GET['src']['nk'],
    		'tanggal_awal' =>$_GET['src']['fd'],
    		'tanggal_akhir' =>$_GET['src']['ld']
    	);
    
    	$transaksi->setData((object) $search);
    
    	$data = array(
    		'base_url' => getBaseUrl(), //base data
            'url'=> basename(__FILE__, '.php'),
            'judul' => 'Data Transaksi/FAC Institute',
            'username'=>$_SESSION['admin']['nama'],
            'subtitle' => 'Data Transaksi',
            'page' => 'data-transaksi', // end base data
            'totaldata' => $transaksi->CountTransactionData(),
            'pagenum'=>$offset,
            'list' => $transaksi->SearchData()
    	);
    	$data = (object) $data;
    	include_once 'view/transaksi/view-data-trans.php';
    
    
    
    
    }elseif (isset($_GET['page'])&&!empty($_GET['page'])) {
    	$limit = 30;
    	$offset = ($_GET['page']-1)*$limit;
    	$data = array(
    		'base_url' => getBaseUrl(), //base data
            'url'=> basename(__FILE__, '.php'),
            'judul' => 'Data Transaksi/FAC Institute',
            'username'=>$_SESSION['admin']['nama'],
            'subtitle' => 'Data Transaksi',
            'page' => 'data-transaksi', // end base data
            'totaldata' => $transaksi->CountTransactionData(),
            'pagenum'=>$offset,
            'list' => $transaksi->ListTable($offset,$limit)
    	);
    	$data = (object) $data;
    	include_once 'view/transaksi/view-data-trans.php';
    } else {
    	$offset = 0;
    	$limit = 30;
    
    	$data = array(
    		'base_url' => getBaseUrl(), //base data
            'url'=> basename(__FILE__, '.php'),
            'judul' => 'Data Transaksi/FAC Institute',
            'username'=>$_SESSION['admin']['nama'],
            'subtitle' => 'Data Transaksi',
            'page' => 'data-transaksi', // end base data
            'totaldata' => $transaksi->CountTransactionData(),
            'pagenum'=>$offset,
            'list' => $transaksi->ListTable($offset,$limit)
    	);
    	$data = (object) $data;
    	include_once 'view/transaksi/view-data-trans.php';
    }
}
