<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="col-md-12">
      <div class="navbar-header">
          <span class="navbar-brand" style="font-size:20px;cursor:pointer;color:white" onclick="openNav()"><span class="glyphicon glyphicon-th-list"></span> Menu</span>
      </div>
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"><span class="glyphicon glyphicon-user"></span> <?php echo $_SESSION['admin']['nama']; ?></a>
          <ul class="dropdown-menu">
            <li><a href=<?php echo "'".getBaseUrl()."administrasi/profile-user'"; ?> >Profilku</a></li>
            <li><a href=<?php echo "'".getBaseUrl()."administrasi/absensi'"; ?> >Absen</a></li>
            <li><a href=<?php echo "'".getBaseUrl()."administrasi/user/laporan-harian'"; ?>>Laporan</a></li>
            <li><a href=<?php echo "'".getBaseUrl()."administrasi/bug/new-bug'"; ?>>Keluhan</a></li>
            <li><a href=<?php echo "'".getBaseUrl()."administrasi/ubah-password'"; ?>>Ubah Password</a></li>
            <li><a href=<?php echo "'".getBaseUrl()."administrasi/logout'"; ?>>Log Out</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

<div class="container-fluid">
  <div class="col-md-12">
    <img src=<?php echo "'".getBaseUrl()."images/logo-fac.jpg'"; ?> style="z-index:-11;position:relative;top:0;left:0;width: 300px;height: 120px;">
  </div>
</div>