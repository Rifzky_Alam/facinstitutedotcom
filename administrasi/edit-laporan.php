<?php 
session_start();
if (!isset($_SESSION['admin'])&&$_SESSION['admin']['tipe']=='admin') {
    header('Location:../login/index.php');
}
?>
<!DOCTYPE html>
<html>

<head>
<?php 
$page = 'editLaporan';
include_once 'header.php'; 
?>
</head>

<body>


<?php 
include_once 'sidebar.php';
include_once 'top-nav.php';
include_once '../model/Pendaftar.php';
include_once '../model/Petugas.php';
include_once '../model/Laporan.php';

$control = new Pendaftar();
$controlPetugas = new ControlPetugas();
$controlLaporan = new ControlLaporan();
$petugases = $controlPetugas->fetchUsernameAndName();



$dataComboBox = $control->dataCombobox();
$dataTanggalLapor = $controlLaporan->selectTanggal($_SESSION['admin']['username']);

?>


<div class="container" style="margin-top:40px;margin-bottom:40px;">

    <form action=<?php echo "'".basename(__FILE__, '.php')."'"; ?> method="POST">
    <div class="row" style="margin-bottom:10px">
        
        <div class="col-md-4">
            <select id="namaPegawai" name="pegawai" class="form-control">
                <option value="" selected>-- Pilih Pegawai --</option>
                <?php 
                for ($i=0; $i < count($petugases) ; $i++) { 
                    echo "<option value='".$petugases[$i]->getUsername()."'>";
                    echo $petugases[$i]->getNama();
                    echo "</option>"; 
                }
                ?>
            </select>
        </div>

        <div class="col-md-4">
            <input id="tanggal-lapor" name="tanggal-lapor" class="form-control" placeholder="pilih tanggal" />
        </div>

        <div class="col-md-4">
            <button class="btn btn-primary">Cari Data</button>
        </div>

    </form>

    </div>

    <hr>

    <?php 

        if (isset($_POST['pegawai'],$_POST['tanggal-lapor'])){
            $comboBoxData = json_decode($controlPetugas->getDataPendaftarToday($_POST['tanggal-lapor']));
            $dataEdit =$controlLaporan->SelectByTanggalAndID($_POST['pegawai'],$_POST['tanggal-lapor']);
    ?>

    <!--edit page-->
    <div class="row">
        
        <div class="row">
            <div class="col-md-12">
                <label>Nama Pegawai :</label>
                <span><?php echo @$dataEdit->nama;?></span>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <label>Edit Untuk Tanggal :</label>
                <span id="editTanggal"><?php echo @$_POST['tanggal-lapor'];?></span>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <select name="jenisHariKerja" id="editJnsHariKerja" class="form-control">
                    <option value="">-- Jenis Hari Kerja --</option>
                    <?php 
                        if ($dataEdit->jns_hari_kerja=='hari kerja normal') {
                            echo "<option value='hari kerja normal' selected>Hari Kerja Normal</option>";
                            echo "<option value='hari kerja sabtu'>Hari Kerja Sabtu</option>";
                            echo "<option value='hari kerja minggu'>Hari Kerja Minggu</option>";
                            echo "<option value='hari kerja libur nasional'>Hari Kerja Libur Nasional</option>";
                        }elseif ($dataEdit->jns_hari_kerja=='hari kerja sabtu') {
                            echo "<option value='hari kerja normal' selected>Hari Kerja Normal</option>";
                            echo "<option value='hari kerja sabtu'>Hari Kerja Sabtu</option>";
                            echo "<option value='hari kerja minggu'>Hari Kerja Minggu</option>";
                            echo "<option value='hari kerja libur nasional'>Hari Kerja Libur Nasional</option>";
                        }elseif ($dataEdit->jns_hari_kerja=='hari kerja minggu') {
                            echo "<option value='hari kerja normal' selected>Hari Kerja Normal</option>";
                            echo "<option value='hari kerja sabtu'>Hari Kerja Sabtu</option>";
                            echo "<option value='hari kerja minggu'>Hari Kerja Minggu</option>";
                            echo "<option value='hari kerja libur nasional'>Hari Kerja Libur Nasional</option>";
                        }elseif ($dataEdit->jns_hari_kerja=='hari kerja libur nasional') {
                            echo "<option value='hari kerja normal'>Hari Kerja Normal</option>";
                            echo "<option value='hari kerja sabtu'>Hari Kerja Sabtu</option>";
                            echo "<option value='hari kerja minggu'>Hari Kerja Minggu</option>";
                            echo "<option value='hari kerja libur nasional' selected>Hari Kerja Libur Nasional</option>";
                        }else{
                            echo "<option value='hari kerja normal'>Hari Kerja Normal</option>";
                            echo "<option value='hari kerja sabtu'>Hari Kerja Sabtu</option>";
                            echo "<option value='hari kerja minggu'>Hari Kerja Minggu</option>";
                            echo "<option value='hari kerja libur nasional'>Hari Kerja Libur Nasional</option>";
                        }
                    ?>

                    
                </select>
            </div>
        </div>
        
        <div class='row'>
            <input class='form-control' value=<?php echo "'".$dataEdit->id."'" ?> style='display:none' id='edit-id' />
        </div>

        <div class="row" style="margin-top:10px">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Nama Klien</label>
                    
                    <select id="editNamaKlien"  class="form-control" style="width:70%">
                        <option value="f4e745e1015af1c15ebcb70b77f1426c">FAC Institute</option>
                        <option value="d5eb8c090d191587bb8a4b875bae4fed">FAC Remote</option>
                        <?php 
                        for ($i=0; $i < count($comboBoxData); $i++) { 
                            echo "<option value='".$comboBoxData[$i]->id."'>";
                            echo $comboBoxData[$i]->perusahaan.' : '. $comboBoxData[$i]->tanggal;
                            echo "</option>";
                        }
                        ?>
                    </select>
                   
                    
                </div>
            </div>
        </div>
        <input id="editUsername" style="display:none" value=<?php echo "'".@$dataEdit->username."'";?>/>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label for="jam-masuk">Overtime</label>
                    <input class="form-control" type="number" id="editOvertime" value=<?php echo "'".@$dataEdit->jumlah_overtime."'"; ?> />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label for="jam-masuk">Peserta Kegiatan</label>
                    <input class="form-control" type="text" id="editPesertaKegiatan" value=<?php echo "'".@$dataEdit->peserta_kegiatan."'"; ?> />
                    
                </div>
            </div>
        </div>
        
        <div class="row" style="margin-top:10px">
            <div class="col-md-10">
                <div class="form-group">
                    <label>Keterangan Kegiatan</label>
                    <textarea class="form-control" rows="7" id="editKeteranganKegiatan" placeholder="Jelaskan detail kegiatan"><?php echo @$dataEdit->ket_kegiatan;?></textarea>
                </div>
            </div>
        </div>

        <div class="row">
          <div class="col-md-10">
            <div class="form-group">
              <h4>Status Kerja</h4>

            <?php 
                if(@$dataEdit->status_kerja=='sendiri') {
                    echo "<div class='radio'><label><input type='radio' name='editStatusKerja' value='sendiri' checked>Sendiri</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editStatusKerja' value='tidak sendiri'>Tidak Sendiri</label></div>";
                }else if(@$dataEdit->status_kerja=='tidak sendiri'){
                    echo "<div class='radio'><label><input type='radio' name='editStatusKerja' value='sendiri'>Sendiri</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editStatusKerja' value='tidak sendiri' checked>Tidak Sendiri</label></div>";
                }else{
                    echo "<div class='radio'><label><input type='radio' name='editStatusKerja' value='sendiri'>Sendiri</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editStatusKerja' value='tidak sendiri'>Tidak Sendiri</label></div>";}
            ?>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-10">
            <div class="form-group">
              <h4>Peran Kerja</h4>

              <?php
                if (@$dataEdit->peran=='utama'){
                    echo "<div class='radio'><label><input type='radio' name='editPeran' value='utama' checked>Utama</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editPeran' value='pendamping'>Pendamping</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editPeran' value='peninjau'>Peninjau</label></div>";
                }elseif (@$dataEdit->peran=='pendamping'){
                    echo "<div class='radio'><label><input type='radio' name='editPeran' value='utama'>Utama</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editPeran' value='pendamping' checked>Pendamping</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editPeran' value='peninjau'>Peninjau</label></div>";              
                }elseif (@$dataEdit->peran=='peninjau') {
                    echo "<div class='radio'><label><input type='radio' name='editPeran' value='utama'>Utama</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editPeran' value='pendamping'>Pendamping</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editPeran' value='peninjau' checked>Peninjau</label></div>";       
                }else{
                    echo "<div class='radio'><label><input type='radio' name='editPeran' value='utama'>Utama</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editPeran' value='pendamping'>Pendamping</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editPeran' value='peninjau'>Peninjau</label></div>"; 
                }
              ?>
              
            </div>
          </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <h4>Rekan Kerja</h4>
                    <?php 
                    $dataz=$controlPetugas->fetchUsernameAndName();
                    $rekanKerja = explode(' # ', @$dataEdit->rekan);

                    for ($i=0; $i < count($dataz); $i++) { 
                        echo "<div class='checkbox'><label>";
                        if (in_array($dataz[$i]->getUsername(), $rekanKerja)){
                            echo "<input type='checkbox' name='editRekan' checked value='".$dataz[$i]->getUsername()."'>".$dataz[$i]->getNama();
                        }else{
                            echo "<input type='checkbox' name='editRekan' value='".$dataz[$i]->getUsername()."'>".$dataz[$i]->getNama();
                        }
                        echo "</label></div>";
                    }
                    
                    
                    ?>


                   
                </div>
            </div>
        </div>


        <div class="row">
          <div class="col-md-10">
            <div class="form-group">
              <h4>Jenis Transportasi</h4>
                <?php 
                if (@$dataEdit->jns_transportasi=='motor') {
                    echo "<div class='radio'><label><input type='radio' name='editJenisTransportasi' value='motor' checked>Motor</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editJenisTransportasi' value='mobil'>Mobil</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editJenisTransportasi' value='angkutan umum'>Angkutan Umum</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editJenisTransportasi' value='kereta'>Kereta Api</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editJenisTransportasi' value='pesawat'>Pesawat</label></div>";
                }elseif (@$dataEdit->jns_transportasi=='mobil'){
                    echo "<div class='radio'><label><input type='radio' name='editJenisTransportasi' value='motor'>Motor</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editJenisTransportasi' value='mobil' checked>Mobil</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editJenisTransportasi' value='angkutan umum'>Angkutan Umum</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editJenisTransportasi' value='kereta'>Kereta Api</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editJenisTransportasi' value='pesawat'>Pesawat</label></div>";
                }elseif (@$dataEdit->jns_transportasi=='angkutan umum'){
                    echo "<div class='radio'><label><input type='radio' name='editJenisTransportasi' value='motor'>Motor</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editJenisTransportasi' value='mobil'>Mobil</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editJenisTransportasi' value='angkutan umum' checked>Angkutan Umum</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editJenisTransportasi' value='kereta'>Kereta Api</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editJenisTransportasi' value='pesawat'>Pesawat</label></div>";
                }elseif (@$dataEdit->jns_transportasi=='kereta'){
                    echo "<div class='radio'><label><input type='radio' name='editJenisTransportasi' value='motor'>Motor</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editJenisTransportasi' value='mobil'>Mobil</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editJenisTransportasi' value='angkutan umum'>Angkutan Umum</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editJenisTransportasi' value='kereta' checked>Kereta Api</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editJenisTransportasi' value='pesawat'>Pesawat</label></div>";
                }elseif (@$dataEdit->jns_transportasi=='pesawat'){
                    echo "<div class='radio'><label><input type='radio' name='editJenisTransportasi' value='motor'>Motor</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editJenisTransportasi' value='mobil'>Mobil</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editJenisTransportasi' value='angkutan umum'>Angkutan Umum</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editJenisTransportasi' value='kereta'>Kereta Api</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editJenisTransportasi' value='pesawat' checked>Pesawat</label></div>";
                }else{
                    echo "<div class='radio'><label><input type='radio' name='editJenisTransportasi' value='motor'>Motor</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editJenisTransportasi' value='mobil'>Mobil</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editJenisTransportasi' value='angkutan umum'>Angkutan Umum</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editJenisTransportasi' value='kereta'>Kereta Api</label></div>";
                    echo "<div class='radio'><label><input type='radio' name='editJenisTransportasi' value='pesawat'>Pesawat</label></div>";
                }

                ?>
              
            </div>
          </div>
        </div>



        <div class="row" style="margin-top:10px">
            <div class="col-md-10">
                <div class="form-group">
                    <label>Total Biaya Transportasi</label>
                    <input class="form-control" type='number' name="biayaTransport" value=<?php echo "'".@$dataEdit->biaya_transport."'";?> id="editBiayaTransportasi" placeholder="isikan jumlah total biaya transportasi"/>
                </div>
            </div>
        </div>

        <div class="row" style="margin-top:10px">
            <div class="col-md-10">
                <div class="form-group">
                    <label>Keterangan Kegiatan Transportasi</label>
                    <textarea class="form-control" id="editKeteranganTransportasi" placeholder="Jika ada, tulis detail biaya dalam satu baris, pisahkan dengan koma. Misal: Angkot ke terminal 20.000, bus ke bandung 50.000, dst."><?php echo @$dataEdit->ket_transport;?></textarea>
                </div>
            </div>
        </div>

        <div class="row" style="margin-top:10px">
            <div class="col-md-10">
                <div class="form-group">
                    <label>Biaya Parkir</label>
                    <input class="form-control" type='number' name="biaya-parkir"  id="editBiayaParkir" placeholder="Jika ada, lampirkan bukti" value=<?php echo "'".@$dataEdit->biaya_parkir."'";?> />
                </div>
            </div>
        </div>

        <div class="row" style="margin-top:10px">
            <div class="col-md-10">
                <div class="form-group">
                    <label>Biaya Makan Siang</label>
                    <input class="form-control" type='number' name="biaya-makanSiang"  id="editBiayaLunch" placeholder="Jika ada, lampirkan bukti" value=<?php echo "'".@$dataEdit->biaya_lunch."'";?> />
                </div>
            </div>
        </div>

        <div class="row" style="margin-top:10px">
            <div class="col-md-10">
                <div class="form-group">
                    <label>Biaya makan Malam</label>
                    <input class="form-control" type='number' name="biaya-makanMalam"  id="editBiayaDinner" placeholder="Jika ada, lampirkan bukti" value=<?php echo "'".@$dataEdit->biaya_dinner."'";?> />
                </div>
            </div>
        </div>

        <div class="row" style="margin-top:10px">
            <div class="col-md-10">
                <div class="form-group">
                    <label>Biaya Allowance</label>
                    <input class="form-control" type='number' name="biaya-allowance"  id="editBiayaAllowance" placeholder="Jika ada, lampirkan bukti" value=<?php echo "'".@$dataEdit->biaya_allowance."'";?> />
                </div>
            </div>
        </div>

        <div class="row" style="margin-top:10px">
            <div class="col-md-10">
                <div class="form-group">
                    <label>Biaya Kesehatan</label>
                    <input class="form-control" type='number' name="biaya-kesehatan" type="number"  id="editBiayaKesehatan" placeholder="Jika ada, lampirkan bukti" value=<?php echo "'".@$dataEdit->biaya_kesehatan."'";?> />
                </div>
            </div>
        </div>

        <div class="row" style="margin-top:10px">
            <div class="col-md-10">
                <div class="form-group">
                    <label>Biaya Lainnya</label>
                    <input class="form-control" type='number' name="biaya-lain" placeholder="Jika ada, lampirkan bukti" id="editBiayaLainnya" value=<?php echo "'".@$dataEdit->biaya_lain."'";?> />
                </div>
            </div>
        </div>

        <div class="row" style="margin-top:10px">
            <div class="col-md-10">
                <div class="form-group">
                    <label>Keterangan Biaya Lainnya</label>
                    <textarea class="form-control" id="editKeteranganBiayaLain" placeholder="Tulis keterangan biaya lain dengan lengkap dalam satu baris, pisahkan dengan koma."><?php echo @$dataEdit->ket_biaya_lain;?></textarea>
                </div>
            </div>
        </div>

        <div class="row" style="margin-top:10px">
            <div class="col-md-10">
                <div class="form-group">
                  <button class="btn btn-primary" style="width:100%" id="btnEditSubmit" name="submit">Submit Data</button>
                </div>
            </div>
        </div>

    </div>

    <!--end edit page-->


    <?php

        }else{

    ?>


    <div class="row">
       <div class="col-md-10">
        <p>Silahkan pilih Tanggal dan petugas terlebih dahulu</p>
       </div>
    </div>

<?php } ?>
</div>

    <script type="text/javascript" src=<?php echo "'".getBaseUrl()."js/jquery.js'"; ?>></script>
    <script type="text/javascript" src=<?php echo "'".getBaseUrl()."js/jquery-ui/jquery-ui.js'"; ?>></script>
    
    <script type="text/javascript">
        

        

        $(document).ready(function(){

          $('#tanggal-lapor').datepicker({
            dateFormat: 'yy-mm-dd'
          });

            $('#btnEditSubmit').click(function(){
                var editUsername = $('#editUsername').val();
                var editTanggal = $('#editTanggal').html();
                var editJnsHariKerja = $('#editJnsHariKerja').val();
                var peserta = $('#editPesertaKegiatan').val();
                var editJamMasuk = $('#editJamMasuk').val();
                var editJamPulang = $('#editJamPulang').val();
                var editOvertime = $('#editOvertime').val();
                var aidi = $('#edit-id').val();
                var editKeteranganKegiatan = $('#editKeteranganKegiatan').val();
                var editStatusKerja = $('input[name=editStatusKerja]:checked').val();
                var editNamaKlien = $('#editNamaKlien').val();
                var editPeran = $('input[name=editPeran]:checked').val();
                var editRekan = $("input[name=editRekan]:checked").map(function () {return this.value;}).get().join(" # ");
                var editJenisTransportasi = $('input[name=editJenisTransportasi]:checked').val();
                var editBiayaTransportasi = $('#editBiayaTransportasi').val();
                var editKeteranganTransportasi = $('#editKeteranganTransportasi').val();
                var editBiayaParkir = $('#editBiayaParkir').val();
                var editBiayaLunch = $('#editBiayaLunch').val();
                var editBiayaDinner = $('#editBiayaDinner').val();
                var editBiayaAllowance = $('#editBiayaAllowance').val();
                var editBiayaKesehatan = $('#editBiayaKesehatan').val();
                var editBiayaLainnya = $('#editBiayaLainnya').val();
                var editKeteranganBiayaLain = $('#editKeteranganBiayaLain').val();


                var jsonDatas = {
                    'peserta':peserta,
                    'keteranganKegiatan':editKeteranganKegiatan,
                    'statusKerja':editStatusKerja,
                    'peran':editPeran,
                    'rekan':editRekan,
                    'jenisTransportasi':editJenisTransportasi,
                    'biayaTransportasi':editBiayaTransportasi,
                    'keteranganTransportasi':editKeteranganTransportasi,
                    'biayaParkir': editBiayaParkir,
                    'biayaMakanSiang':editBiayaLunch,
                    'biayaMakanMalam':editBiayaDinner,
                    'biayaAllowance':editBiayaAllowance,
                    'biayaKesehatan':editBiayaKesehatan,
                    'biayaLain':editBiayaLainnya,
                    'aidi':aidi,
                    'jenisHariKerja':editJnsHariKerja,
                    'keteranganBiayaLain':editKeteranganBiayaLain
                }
                $(this).attr("disabled", true);
                $(this).html('Harap Tunggu ...');
//alert(peserta);

                $.ajax({
                  type: "POST",
                  url: "reverse.php",
                  data: {
                    'jsonData':jsonDatas
                    },
                  cache: false,
                  success: function(data){
                     alert(data);
                     window.location.replace("index.php");
                  }
                }); //end ajax




                //alert(editUsername);
                //alert(editTanggal);
                //alert(editJnsHariKerja);
                //alert(editJamMasuk);
                //alert(editJamPulang);
                //alert(editOvertime);
                //alert(editStatusKerja);
                //alert(editNamaKlien);
                //alert(editKeteranganKegiatan);
                
                //alert(editPeran);
                //alert(editRekan);
                //alert(editJenisTransportasi);
                //alert(editBiayaTransportasi);
                //alert(editKeteranganTransportasi);
                //alert(editBiayaParkir);
                //alert(editBiayaLunch);
                //alert(editBiayaDinner);
                //alert(editBiayaAllowance);
                //alert(editBiayaKesehatan);
                //alert(editBiayaLainnya);
                //alert(editKeteranganBiayaLain);




            });


        });

    </script>
</body>
</html> 


