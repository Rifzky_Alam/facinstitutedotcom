<?php 
if (isset($data->base_url)) {
  $baseUrl = $data->base_url;
} else {
  $baseUrl = getBaseUrl();
}
?>

<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><font style="font-size:14px"><< Back  </font></a>
  <a href=<?php echo "'".$baseUrl."administrasi'"; ?>>Beranda</a>

    <?php if ($_SESSION['admin']['tipe']=='admin'): ?>
      <a href="javascript:;" data-toggle="collapse" data-target="#team">Data <span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
      <?php else: ?>
      <a href="javascript:;" data-toggle="collapse" data-target="#team">Team <span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
    <?php endif ?>

  
    <ul id='team' class='collapse' style='list-style-type:none'>
      <li><a href=<?php echo "'".$baseUrl."administrasi/daftar-hadir'"; ?> style='padding-left:0px;color:chartreuse;font-size:14px'>Daftar Absensi Harian</a></li>
      <li><a href=<?php echo "'".$baseUrl."administrasi/data-laporan'"; ?> style='padding-left:0px;color:chartreuse;font-size:14px'>Data Laporan Harian</a></li>
      <li><a href=<?php echo "'".$baseUrl."administrasi/customerservice/data-support'"; ?> style='padding-left:0px;color:chartreuse;font-size:14px'>Data Support</a></li>
      
      <?php if ($_SESSION['admin']['tipe']=='admin'): ?>
        <li>
          <a href=<?php echo "'".$baseUrl."administrasi/calendar'"; ?> style='padding-left:0px;color:chartreuse;font-size:14px'>
          Kalender
          </a>
        </li>
        <li>
          <a href=<?php echo "'".$baseUrl."administrasi/new-usaha'"; ?> style='padding-left:0px;color:chartreuse;font-size:14px'>Perusahaan
          </a>
        </li>
        <li>
          <a href=<?php echo "'".$baseUrl."administrasi/forum-table'"; ?> style='padding-left:0px;color:chartreuse;font-size:14px'>Forum
          </a>
        </li>
        <li>
          <a href=<?php echo "'".$baseUrl."administrasi/bug/listdata'"; ?> style='padding-left:0px;color:chartreuse;font-size:14px'>List Error
          </a>
        </li>
        <li>
          <a href=<?php echo "'".$baseUrl."administrasi/insentif/dasbor'"; ?> style='padding-left:0px;color:aqua;font-size:14px'>
            Data Insentif
          </a>
        </li>
      <?php elseif($_SESSION['admin']['tipe']=='marketing'): ?>
      <li>
        <a href=<?php echo "'".$baseUrl."administrasi/forum-table'"; ?> style='padding-left:0px;color:chartreuse;font-size:14px'>
          Forum
        </a>
      </li>
      <?php elseif($_SESSION['admin']['tipe']=='staff'): ?>  
      <li>
        <a href=<?php echo "'".$baseUrl."administrasi/table?ac=map'"; ?> style='padding-left:0px;color:chartreuse;font-size:14px'>
          Map
        </a>
      </li>
      <li>
        <a href=<?php echo "'".$baseUrl."administrasi/forum-table'"; ?> style='padding-left:0px;color:chartreuse;font-size:14px'>
          Forum
        </a>
      </li>
      <?php endif ?>
      
      <?php if ($_SESSION['admin']['username']=='charlez'||$_SESSION['admin']['username']=='Fajar'||$_SESSION['admin']['username']=='Imelia'): ?>
      <li><a href=<?php echo "'".$baseUrl."administrasi/log-data'"; ?> style='padding-left:0px;color:chartreuse;font-size:14px'>Web User Log Datas</a></li>
      
      <?php endif ?>
    </ul>

  <?php if ($_SESSION['admin']['tipe']=='staff'): ?>
     <a href="javascript:;" data-toggle="collapse" data-target="#input-artikel">Artikel <span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
    <ul id='input-artikel' class='collapse' style='list-style-type:none'>
      <li><a href=<?php echo "'".$baseUrl."administrasi/artikel/'"; ?> style='padding-left:0px;color:aqua;font-size:14px'>Data Artikel</a></li>
      <li><a href=<?php echo "'".$baseUrl."administrasi/artikel/new-data'"; ?> style='padding-left:0px;color:aqua;font-size:14px'>Input Artikel</a></li>
      <li><a href=<?php echo "'".$baseUrl."administrasi/artikel/edit-data'"; ?> style='padding-left:0px;color:aqua;font-size:14px'>Edit Artikel</a></li>
    </ul>
    <a href="javascript:;" data-toggle="collapse" data-target="#input-tulisan">Tutorial <span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
    <ul id='input-tulisan' class='collapse' style='list-style-type:none'>
      <li><a href=<?php echo "'".$baseUrl."administrasi/cms/tutorial-table'"; ?> style='padding-left:0px;color:aqua;font-size:14px'>Data Tutorial</a></li>
      <li><a href=<?php echo "'".$baseUrl."administrasi/cms/'"; ?> style='padding-left:0px;color:aqua;font-size:14px'>Input Tutorial</a></li>
      <li><a href=<?php echo "'".$baseUrl."administrasi/cms/tutorial-edit'"; ?> style='padding-left:0px;color:aqua;font-size:14px'>Edit Tutorial</a></li>
    </ul>
  <?php endif ?>  

  <?php if ($_SESSION['admin']['tipe']=='marketing'): ?>
    <a href="javascript:;" data-toggle="collapse" data-target="#edit-kegiatan-marketing">Order<span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
    <ul id='edit-kegiatan-marketing' class='collapse' style='list-style-type:none'>

      

      <li>
        <a href=<?php echo "'".$baseUrl."administrasi/marketing/data-followup'"; ?> style='padding-left:0px;color:aqua;font-size:14px'>
          Laporan follow-up
        </a>
      </li>
      <li>
        <a href=<?php echo "'".$baseUrl."administrasi/data-transaksi'"; ?> style='padding-left:0px;color:aqua;font-size:14px'>
          Data Transaksi
        </a>
      </li>
      <li>
        <a href=<?php echo "'".$baseUrl."administrasi/data-customer'"; ?> style='padding-left:0px;color:aqua;font-size:14px'>
          Data Customer
        </a>
      </li>
      
      <li>
        <a href=<?php echo "'".$baseUrl."administrasi/calendar'"; ?> style='padding-left:0px;color:aqua;font-size:14px'>
          Kalender
        </a>
      </li>
      <li>
        <a href=<?php echo "'".$baseUrl."administrasi/new-usaha'"; ?> style='padding-left:0px;color:aqua;font-size:14px'>
          List Perusahaan
        </a>
      </li>
      <li>
      <a href="<?php echo $baseUrl.'administrasi/upload-files'; ?>" style='padding-left:0px;color:aqua;font-size:14px'>
        Upload Files
      </a>
    </li>
    </ul>
    
    <a href="javascript:;" data-toggle="collapse" data-target="#data-marketing">Data<span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
    <ul id='data-marketing' class='collapse' style='list-style-type:none'>
    <li>
      <a href="<?php echo $baseUrl.'administrasi/daftar-quotation'; ?>" style='padding-left:0px;color:aqua;font-size:14px'>
        Data Quotation Custom
      </a>
    </li>
    <li>
      <a href="<?php echo $baseUrl.'administrasi/daftar-agenda'; ?>" style='padding-left:0px;color:aqua;font-size:14px'>
        Data Agenda
      </a>
    </li>
    
    <li>
        <a href=<?php echo "'".$baseUrl."administrasi/table?ac=srclm'"; ?> style='padding-left:0px;color:aqua;font-size:14px'>
          Laporan Training
        </a>
    </li>
    </ul>
    <a href="javascript:;" data-toggle="collapse" data-target="#marketingz">Marketing <span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
    <ul id='marketingz' class='collapse' style='list-style-type:none'>
      <li>
        <a href=<?php echo "'".$baseUrl."administrasi/kirim-penawaran'"; ?> style='padding-left:0px;color:aqua;font-size:14px'>
          Penawaran
        </a>
      </li>
      <li>
        <a href="<?= $baseUrl ?>administrasi/invoice/tabel-marketing" style='padding-left:0px;color:aqua;font-size:14px'>
          Invoice
        </a>
      </li>
    </ul>
  <?php endif ?>
  
  <?php  
  if($_SESSION['admin']['tipe']=='admin'){  ?>

    <a href="javascript:;" data-toggle="collapse" data-target="#edit-kegiatan">Ubah Data <span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
    <ul id='edit-kegiatan' class='collapse' style='list-style-type:none'>
      <li><a href=<?php echo "'".$baseUrl."administrasi/edit-absen'";?> style='padding-left:0px;color:aqua;font-size:14px'>Absensi Harian</a></li>
      <li><a href=<?php echo "'".$baseUrl."administrasi/edit-laporan'";?> style='padding-left:0px;color:aqua;font-size:14px'>Data Laporan Harian</a></li>
    </ul>

    <a href="javascript:;" data-toggle="collapse" data-target="#marketingz">Marketing <span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
    <ul id='marketingz' class='collapse' style='list-style-type:none'>
      <li>
        <a href=<?php echo "'".$baseUrl."administrasi/data-transaksi'"; ?> style='padding-left:0px;color:chartreuse;font-size:14px'>
          Data Transaksi
        </a>
      </li>
      <li>
        <a href=<?php echo "'".$baseUrl."administrasi/data-customer'"; ?> style='padding-left:0px;color:aqua;font-size:14px'>
          Data Customer
        </a>
      </li>
      <li>
        <a href=<?php echo "'".$baseUrl."administrasi/data-training'"; ?> style='padding-left:0px;color:chartreuse;font-size:14px'>Data Order Training</a>
      </li>
      <li>
      <li>
        <a href=<?php echo "'".$baseUrl."administrasi/kirim-penawaran'"; ?> style='padding-left:0px;color:aqua;font-size:14px'>
        Penawaran
        </a>
      </li>
      <li>
        <a href=<?php echo "'".$baseUrl."administrasi/daftar-quotation'"; ?> style='padding-left:0px;color:aqua;font-size:14px'>
        Data Penawaran Custom
        </a>
      </li>
      <li>
      <a href=<?php echo "'".$baseUrl."administrasi/daftar-agenda'"; ?> style='padding-left:0px;color:aqua;font-size:14px'>
      Daftar Jadwal
      </a>
      </li>
      <li>
      <a href=<?php echo "'".$baseUrl."administrasi/daftar-invoice'"; ?> style='padding-left:0px;color:aqua;font-size:14px'>
      Invoice
      </a>
      </li>
      <li>
        <a href=<?php echo "'".$baseUrl."administrasi/data-invoice'"; ?> style='padding-left:0px;color:chartreuse;font-size:14px'>
          Data Invoice (New)
        </a>
      </li>
      <li>
        <a href=<?php echo "'".$baseUrl."administrasi/marketing/data-followup'"; ?> style='padding-left:0px;color:aqua;font-size:14px'>
          Laporan follow-up
        </a>
      </li>
      <li>
      <a href="<?php echo $baseUrl.'administrasi/upload-files'; ?>" style='padding-left:0px;color:aqua;font-size:14px'>
        Upload Files
      </a>
    </li>
    </ul>

    <a href="javascript:;" data-toggle="collapse" data-target="#input-tulisan">Tutorial <span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
    <ul id='input-tulisan' class='collapse' style='list-style-type:none'>
      <li><a href=<?php echo "'".$baseUrl."administrasi/cms/tutorial-table'"; ?> style='padding-left:0px;color:aqua;font-size:14px'>Data Tutorial</a></li>
      <li><a href=<?php echo "'".$baseUrl."administrasi/cms/'"; ?> style='padding-left:0px;color:aqua;font-size:14px'>Input Tutorial</a></li>
      <li><a href=<?php echo "'".$baseUrl."administrasi/cms/tutorial-edit'"; ?> style='padding-left:0px;color:aqua;font-size:14px'>Edit Tutorial</a></li>
    </ul>

     <a href="javascript:;" data-toggle="collapse" data-target="#input-artikel">Artikel <span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
    <ul id='input-artikel' class='collapse' style='list-style-type:none'>
      <li><a href=<?php echo "'".$baseUrl."administrasi/artikel/'"; ?> style='padding-left:0px;color:aqua;font-size:14px'>Data Artikel</a></li>
      <li><a href=<?php echo "'".$baseUrl."administrasi/artikel/new-data'"; ?> style='padding-left:0px;color:aqua;font-size:14px'>Input Artikel</a></li>
      <li><a href=<?php echo "'".$baseUrl."administrasi/artikel/edit-data'"; ?> style='padding-left:0px;color:aqua;font-size:14px'>Edit Artikel</a></li>
    </ul>
    <a href="javascript:;" data-toggle="collapse" data-target="#adminz">Admin <span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
    <ul id='adminz' class='collapse' style='list-style-type:none'>
      <li><a href=<?php echo "'".$baseUrl."administrasi/user/new'"; ?> style='padding-left:0px;color:aqua;font-size:14px'>Pegawai Baru</a></li>
      <li>
        <a href=<?php echo "'".$baseUrl."administrasi/todo/showadmin'"; ?> style='padding-left:0px;color:aqua;font-size:14px'>TO-DO List</a>
      </li>
      <li><a href=<?php echo "'".$baseUrl."administrasi/order-request'"; ?> style='padding-left:0px;color:aqua;font-size:14px'>Request Order</a></li>
      <li><a href=<?php echo "'".$baseUrl."administrasi/agents'"; ?> style='padding-left:0px;color:aqua;font-size:14px'>Agen Training</a></li>
    </ul>

      <a href="javascript:;" data-toggle="collapse" data-target="#kursus">Kursus <span class="glyphicon glyphicon-chevron-down" style="padding-left:2px;font-size:0.6em;"></span></a>
    <ul id='kursus' class='collapse' style='list-style-type:none'>
      <li>
        <a href=<?php echo "'".$baseUrl."administrasi/jadwal-kursus'"; ?> style='padding-left:0px;color:aqua;font-size:14px'>
          Jadwal Kursus
        </a>
      </li>
      <li>
        <a href=<?php echo "'".$baseUrl."administrasi/siswa-kursus'"; ?> style='padding-left:0px;color:aqua;font-size:14px'>
          Data Siswa Kursus
        </a>
      </li>
    </ul>

    <?php
      }else{
    ?>


    <?php } ?>
      
</div>