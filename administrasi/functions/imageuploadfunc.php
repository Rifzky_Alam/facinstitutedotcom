<?php 
// path must be with '/' i.e: attachments/
function UploadImage($extensions,$file,$path){
	if (CheckFile($path.$file['name'])) {
		unlink($path.$file['name']);
	}

	$fileExtension = pathinfo($file['name'],PATHINFO_EXTENSION);
	if (in_array($fileExtension, $extensions)) { // check extension
		if (move_uploaded_file($file['tmp_name'], $path.$file['name'])) { //upload file
			if (CheckFile($path.$file['name'])) { //check file exist
				return ['result'=>true,'msg'=>'Image Uploaded'];
			}else{ //else check file_exist
				return ['result'=>false,'msg'=>'File Not found!'];	
			}	
		}else{ //else upload file
			return ['result'=>false,'msg'=>'Error occured when uploading file, check if folder exist or not'];
		}
	}else{ //else file extension
		return ['result'=>false,'msg'=>'File extensions not allowed'];
	}
}

function CheckFile($path){
	if (file_exists($path)) {
		return true;
	} else {
		return false;
	}
}

function UploadImageMin($file,$path,$extensions,$minwidth,$minheigth){
	
}
function UploadImageMinMax($file,$path,$extensions,$minwidth,$minheigth,$maxwidth,$maxheigth){
	
}