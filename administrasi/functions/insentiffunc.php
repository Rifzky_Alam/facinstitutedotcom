<?php 
function KalkulasiInsentifAcc($tarifacc,$persentaseperan,$jenistrans){
	return $tarifacc * $persentaseperan * $jenistrans;
}

function KalkulasiInsentifHaker($tarifhaker,$jenistrans){
	return $tarifhaker * $jenistrans;
}

function KalkulasiInsentifLuarKota($tarifins,$jenistrans,$peserta){
	return $tarifins*$jenistrans/$peserta;
}

function KalkulasiOvertime($waktumulai,$waktuselesai){
	$hasil = round(((strtotime($waktuselesai)-strtotime($waktumulai))/3600)-8);
	if ($hasil<0) {
		return '0';
	}else{
		return $hasil;
	}
}