<?php 
function CekProv($value){
	if ($value==''||!is_numeric($value)) {
		return '9';
	} else {
		return $value;
	}
}

function CekKota($value){
	if ($value==''||!is_numeric($value)) {
		return '55';
	} else {
		return $value;
	}
}

function CekKecamatan($value){
	if ($value==''||!is_numeric($value)) {
		return '750';
	} else {
		return $value;
	}
}