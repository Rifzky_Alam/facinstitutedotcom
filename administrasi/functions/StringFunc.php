<?php 
function GetVal($value){
	if ($value==''||!isset($value)) {
		return '';
	}else{
		return $value;
	}
}

function cleanData(&$str){
    $str = preg_replace("/\t/", "\\t", $str);
    $str = preg_replace("/\r?\n/", "\\n", $str);
    if(strstr($str, '"')){
    	$str = '"' . str_replace('"', '""', $str) . '"';
    }
 }

 function GetIntVal($value){
 	if ($value==''||!isset($value)) {
		return '0';
	}else{
		return $value;
	}
 }

 function BenerinTelepon($value){
 	$hasil = "";

 	$hasil = str_replace('-', '', $value);

 	$hasil = str_replace('+62', '0', $hasil);

 	return $hasil;
 }

 function EditPhoneNumberForDesktop($nomor){
    $hasil = '';
    if($nomor!=''){
        $nomor = str_replace('-', '', $nomor);
        $hasil = '62'.substr($nomor,1,strlen($nomor));
    }
    return $hasil;
}