<?php 
session_start();
if (!isset($_SESSION['admin'])) {
    header('Location:../login/index.php');
}
?>




<!DOCTYPE html>
<html>

<?php 
//echo $tipeAdmin=$_SESSION['admin']['tipe'];
$page = 'home';
include_once 'header.php'; 

?>

<body>



<?php 

include_once 'sidebar.php';

include_once 'top-nav.php';

 ?>


<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3>Ubah Password</h3>
	</div>
	<?php 
		
		include_once '../model/Petugas.php'; 
        include_once '../model/page.php';
        $myPage = new Page();
		$petugas = new ControlPetugas();
		$myRecord = $petugas->selectByID($_SESSION['admin']['username']);

        //echo $myRecord->password;

        if (isset($_POST['passLama'],$_POST['passBaru'],$_POST['konfirm-passBaru'])) {

            if ($_POST['konfirm-passBaru']!=$_POST['passBaru']) {
                echo "<script>";
                echo "alert('Silahkan Cek Ulang Konfirmasi Password Anda!')";
                echo "</script>";
            }else{

                if (md5($_POST['passLama'])==$myRecord->password) {

                    $updateData = array('myPassword' => md5($_POST['passBaru']),'username'=>$_SESSION['admin']['username'] );
                    $updateData = json_encode($updateData);
                    $objek = json_decode($updateData); 

                    //echo $objek->username;echo "<br>";echo $objek->myPassword;
                    echo $petugas->changePassword($objek);
                    $myPage->addLogForLogin($_SESSION['admin']['username'],'successfully changed password',$_SERVER['REMOTE_ADDR']);
                }else{
                echo "<script>";
                echo "alert('Password lama tidak sesuai!')";
                echo "</script>";                    
                }

            }

        }



	?>
    <form id='my-form' action=<?php echo "'".basename(__FILE__, '.php')."'"; ?> method='post'>

    <div class='row'>
        <div class='form-group col-sm-8'>
            <label>Password Lama</label>
            <input type='password' class='form-control' id='txt-passLama' name='passLama'>
        </div>
    </div>

    <div class='row'>
        <div class='form-group col-sm-8'>
            <label>Password Baru</label>
            <input type='password' class='form-control' id='txt-passBaru' name='passBaru'>
        </div>
    </div>

    <div class='row'>
        <div class='form-group col-sm-8'>
            <label>Ketik Ulang Password Baru</label>
            <input type='password' class='form-control' id='txt-passBaru' name='konfirm-passBaru'>
        </div>
    </div>

    <div class='row'>
        <div class='form-inline col-sm-8'>
            <button class='btn btn-lg btn-primary' style='width:25%'>Submit</button>
        </div>
    </div>

    </form>
</div>
</body>
</html> 