<?php 
session_start();
if (!isset($_SESSION['admin'])) {
    header('Location:../login/index.php');
}
?>

<!DOCTYPE html>
<html>

<?php 
$judul='Form Jadwal dan Agenda Training';
$page = 'dataPegawai';
include_once 'header.php'; 

?>

<body>

<?php 

include_once 'sidebar.php';
include_once 'top-nav.php';

 ?>


<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3>Agenda dan Jadwal Training Accurate</h3>
	</div>
	<?php 
		
		include_once '../model/Pendaftar.php'; 
        include_once '../model/page.php';
        include_once '../model/Petugas.php';
        include_once '../model/Acara.php';

        $myPage = new Page();
        $acara = new Acara();
		$pendaftar = new Pendaftar();
		$ctrPetugas = new ControlPetugas();
        $myDataz = json_decode($ctrPetugas->dataRequest('forEdit'));

        //echo $myRecord->password;

        if (isset($_GET['idp'])&&!empty($_GET['idp'])) {
            $by = array('cond' => 'jadwal','id'=> $_GET['idp']);
            $by = (object) $by;
            $datas = json_decode($pendaftar->fetchDataTraining((object) $by));
        

        	if (isset($_POST['in'])) {
                if (@$_POST['in']['attachments']==''||empty($_POST['in']['attachments'])) {
                    $att='';
                }else{
                    $att=implode(" # ", $_POST['in']['attachments']);
                }

                $tgl = $pendaftar->removeLastString(implode(" # ", $_POST['in']['tanggalPelaksanaan']));
                
                $inputData = array(
                    'id' => md5(date('Y-m-d').$_GET['idp']),
                    'idpendaftar'=> $_GET['idp'],
                    'acara'=> $_GET['idp'],
                    'tanggal'=> $tgl,
                    'waktu'=> $_POST['in']['waktu'],
                    'tempat'=> $_POST['in']['tempat'],
                    'alamat'=> $_POST['in']['alamat'],
                    'cc'=> '',
                    'attachments'=> ''
                );
                $inputData = (object) $inputData;

                if ($pendaftar->addJadwalbaru($inputData)) {
                    $inputData->tanggal = explode(" # ", $inputData->tanggal);
                    $acara->inputSimple($inputData);
                    echo "<script>alert('Data berhasil disimpan!');</script>";
                    echo "<script>location.replace('".basename(__FILE__, '.php')."?step=2&kd=".$inputData->id."');</script>";
                }else{
                    echo "<script>alert('Data gagal disimpan!');</script>";
                }




                /*
        		echo "<div class='row'>";
        		echo "ID: ".$inputData->id."<br>";
                echo "ID Pendaftar: ".$inputData->idpendaftar."<br>";
                echo "Tanggal: ".$inputData->tanggal."<br>";
        		echo "Waktu: ".$inputData->waktu."<br>";
        		echo "Tempat: ".$inputData->tempat."<br>";
        		echo "Alamat: ".$inputData->alamat."<br>";
        		echo "CC: ".$inputData->cc."<br>";
        		echo "Attachments: ".$inputData->attachments."<br>";
        		echo "</div>";*/
                
        	}
	?>
    
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <th class="tengah">Nama Perusahaan</th>
                        <th class="tengah">Nama Kontak</th>
                        <th class="tengah">Tanggal Transaksi</th>
                        <th class="tengah">Jenis Usaha</th>
                        <th class="tengah">Paket Training</th>
                        <th class="tengah">Qty</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tengah"><?php echo $datas[0]->nama_perusahaan ?></td>
                        <td class="tengah"><?php echo $datas[0]->nama_kontak ?></td>
                        <td class="tengah"><?php echo $datas[0]->tanggal_daftar ?></td>
                        <td class="tengah"><?php echo $datas[0]->usaha ?></td>
                        <td class="tengah"><?php echo $datas[0]->nama_item ?></td>
                        <td class="tengah"><?php echo $datas[0]->qty ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <section class="col-md-8">
        <form id='my-form' action='' method='post'>

            <div class="row" id='jajal'>
                <div class="col-md-12">
                    <label for="tgl-pelaksanaan">Tanggal</label>
                    <div class="form-inline" >
                        <input type="text" id="tgl-pelaksanaan" name='in[tanggalPelaksanaan][]' class="form-control tglPelaksanaan" placeholder="klik disini untuk memilih tanggal"/>   
                        <span class='btn btn-sm btn-primary' id='tambahTombol'><i class="glyphicon glyphicon-plus"></i></span>
                    </div>
                </div>
            </div>
            <br>

            <div class='row'>
                <div class='form-group col-sm-12'>
                    <label>Waktu</label>
                    <input type="text" name="in[waktu]" class="form-control" value="08:00-16:00" >
                </div>
            </div>

            <div class='row'>
                <div class='form-group col-sm-12'>
                    <label>Tempat</label>
                    <input type='text' class='form-control' name='in[tempat]'>
                </div>
            </div>

            <div class='row'>
                <div class='form-group col-sm-12'>
                    <label>Alamat</label>
                    <textarea class="form-control" name="in[alamat]"></textarea>
                </div>
            </div>

            <div class='row'>
                <div class='form-inline col-sm-12'>
                    <button class='btn btn-lg btn-primary' style='width:100%'>Selanjutnya</button>
                </div>
            </div>

        </form>

    </section>
 
    <section class="col-md-4">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Tanggal</th>
                    <th>Request</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    for ($i=0; $i < count($myDataz); $i++){ 
                        echo "<tr>";
                        echo "<td>".$myDataz[$i]->tanggal_absen."</td>";
                        echo "<td>".$myDataz[$i]->nama_perusahaan."</td>";
                        echo "</tr>";
                    }

                ?>
            </tbody>
        </table>
    </section>
    


    <?php } ?>

    <?php if (isset($_GET['step'])&&$_GET['step']=='2'&&isset($_GET['kd'])&&!empty($_GET['kd'])): ?>
        <?php $dataz = $pendaftar->fetchDataAgendaByID($_GET['kd']); ?>

     <div class="row" id="tes">
        <div class="form-group col-md-10">
            <label><a href=<?php echo "schedules?step=3&kd=".$_GET['kd']."'"; ?> target="_blank" >Nama Agenda</a></label>
            <input type="text" name="agenda" class="form-control" id="contoh" />
        </div>
     </div>

     <div class="row">
     <?php 
     if (isset($_POST['in'])) {
        $inz = array(
            'agenda' => json_encode($_POST['in']['agenda']),
            'id' =>  $_GET['kd'],
            'status'=>'-1'
        );
        $inz = (object) $inz;
        if ($pendaftar->editAgenda($inz)) {
            echo "<script>alert('Data berhasil disimpan!');</script>";
            echo "<script>location.replace('daftar-agenda?p=".$_POST['in']['idp']."');</script>";
        }else{
            echo "<script>alert('Data gagal disimpan!');</script>";
        }
           
     }

     ?>
     </div>

    <section class="col-md-8">
        <form action="" method="post">
         <div class="row" id="siap">
            <input type="text" name="in[idp]" value=<?php echo "'".$dataz->id_pendaftar."'"; ?> style="display:none;">
            <ul id="ok">
                
            </ul>
         </div>
         <hr>
        

         <div class="row">
            <div class="form-group col-md-10" id='bisalah'>
                
            </div>
         </div>


         <div class="row">
            <div class="col-md-10">
                <button class="btn btn-primary">Submit</button>
            </div>
         </div>
         </form>
    </section>

    <section class="col-md-4">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Tanggal</th>
                    <th>Request</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>2016-12-28</td>
                    <td>Bumi Putra Alam</td>
                </tr>
            </tbody>
        </table>

    </section>


     

    <?php elseif(isset($_GET['step'])&&$_GET['step']=='3'&&isset($_GET['kd'])&&!empty($_GET['kd'])): ?>

        <?php 
        if (isset($_POST['ag'])) {
            if ($pendaftar->addDaftarAgenda($_POST['ag']['namaagenda'])){
                echo "<script>alert('Data berhasil disimpan!');</script>";
                echo "<script>location.replace('daftar-agenda');</script>";
            }else{
                echo "<script>alert('Data gagal disimpan!');</script>";
            }
        }

        ?>

        <form action="" method="post">
            <div class="row">
                <div class="form-group col-md-10">
                    <label>Nama Agenda</label>
                    <textarea class="form-control" name="ag[namaagenda]"></textarea>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <button class="btn btn-lg btn-primary">Submit</button>
                </div>
            </div>
        </form>

    <?php endif ?>



</div>
<script type="text/javascript" src=<?php echo "'".getBaseUrl()."css/datepicker/js/bootstrap-datepicker.js'"; ?>></script>
<?php if (isset($_GET['step'])&&$_GET['step']=='2'&&isset($_GET['kd'])&&!empty($_GET['kd'])): ?>
    <script type="text/javascript">
        $(document).ready(function(){
            //var availableTags = ['Rifzky Alam','Avril Lavigne'];
            //$('#contoh').autocomplete({
                //source: availableTags
            //});
        });

        $('#contoh').keyup(function(){
            if ($('#contoh').val()==''){
                $('.mydata').remove();
            }else{
            $.ajax({
              type: "GET",
              url: "reverse",
              data: {
                'kda':$('#contoh').val()
                },
              cache: false,
              success: function(data){
                $('.mydata').remove();
                 $('#bisalah').append(data);
                 //alert(data);
              }
            }); //end ajax  
            }
            
        });

        function coba(haha) {//haha.value
            $('#ok').append("<li class='ilang' id='li" + haha.id + "'>" + $('#y' + haha.id).text() + " <input type='text' name='in[agenda][]' value='" + haha.value + "' style='display:none;'> <a id='i" + haha.id + "' onclick='removeList(this)' href='#'><span class='glyphicon glyphicon-remove' style='padding-left:10px;'></span></a></li>");
            $('#z' + haha.id).remove();
        }

        function removeList(nama) {
            $('#l' + nama.id).remove();
            //alert('oke');
        }

    </script>

    <?php else: ?>
<script type="text/javascript">
    $(document).ready(function(){
        var apakah = 0;
        $('#cobaa').datepicker({
           format: "yyyy-mm-dd"
        });
    $('#fr-accurateLainnya').hide();


    $('#tambahTombol').click(function(){
    
            var cobayah = $('.tglPelaksanaan').length;
            var iseng = "tambahan" + cobayah;
            if ($('#tambahan').length){

        if (cobayah==2) {
            $('#tambahan').after("<div class='row' id='"+iseng+"'><div class='col-md-10'><div class='form-inline'><input name='in[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"'+iseng+'"'+")'></span></div></div></div>");
        }else{
            var okeh = cobayah-1;
            var iseng = "tambahan" + okeh;
            $("#"+iseng).after("<div class='row' id='tambahan"+cobayah+"'><div class='col-md-10'><div class='form-inline'><input name='in[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"tambahan'+cobayah+'"'+")'></span></div></div></div>");
        };
            //nilai +=1;
         }else{
            $('#jajal').after("<div class='row' id='tambahan'><div class='col-md-10'><div class='form-inline'><input name='in[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"'+"tambahan"+'"'+")'></span></div></div></div>");
        };

        $('.tglPelaksanaan').datepicker({
            format: 'yyyy-mm-dd'
        });
        
    });

        $('.tglPelaksanaan').datepicker({
            format: 'yyyy-mm-dd'
        });

    $('#try').click(function(){
        var cobaz = $("#example-getting-started option:selected").map(function () {return this.value;}).get().join(" # ");
        alert(cobaz);
    });

    })

    function removeElement(id) {
        $('#'+id).remove();
    }
</script>
<?php endif ?>
</body>
</html> 