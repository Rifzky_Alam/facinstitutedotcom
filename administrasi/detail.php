<?php 
session_start();

?>
<!DOCTYPE html>
<html>


<?php 
$judul='Detail Client';
$page = 'home';
include_once 'header.php'; 

?>

<body>


<?php 

include_once 'sidebar.php';

include_once 'top-nav.php';

 ?>


<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3>Daftar Order Training</h3>
	</div>
	<?php 
		
		include_once '../model/Pendaftar.php'; 
    
		$pendaftar = new Pendaftar();
		
    $datas = $pendaftar->dataPendaftar();

	?>

     

	
	<div class="row">
		<div class="col-md-6">
			<table class="table table-bordered">
				<thead>
					<tr><th>Atribut</th><th>Keterangan</th></tr>
				</thead>

				<tbody>

                    <?php if (isset($_GET['data'])&&!$_GET['data']=='') { ?>
                        
                    


                    <?php 
                        include_once '../model/Pendaftar.php'; 
                        include_once '../model/Calendar.php';

                        $pendaftar = new Pendaftar();
                        $cal = new Calender();
                        
                        // @$bicik = $cal->selectAgenda();
                        $datas = $pendaftar->selectByID2($_GET['data']);

                        function cariAgenda($item,$kumpulanAgenda){
                          for ($i=0; $i < count($kumpulanAgenda) ; $i++) { 
                              if ($kumpulanAgenda[$i]->id==$item) {
                                  return $kumpulanAgenda[$i]->nama_agenda;
                              }
                          }
                        }


                    ?>
                    <tr><td>Nama perusahaan</td> <td><?php echo @$datas->nama_perusahaan;?></td></tr>
                    <tr><td>Nama Personal Kontak</td> <td><?php echo @$datas->nama_kontak;?></td></tr>
                    <tr><td>Email Perusahaan</td> <td><?php echo @$datas->email_kantor;?></td></tr>
                    <tr><td>Email Kontak</td> <td><?php echo @$datas->email_kontak;?></td></tr>
                    <tr><td>Telepon Perusahaan</td> <td><?php echo @$datas->telepon_kantor;?></td></tr>
                    <tr><td>Telepon Kontak</td> <td><?php echo @$datas->telepon_kontak;?></td></tr>
                    <tr><td>Alamat Lengkap</td><td><?php echo @$datas->alamat_kantor ?></td></tr>
                    <tr><td>Alamat Map</td> <td><?php echo "<a target='_blank' href='https://www.google.co.id/maps/place/".$datas->map."'>Lokasi</a>" ;?></td></tr>
                    <tr><td>Jabatan Pemesan</td> <td><?php echo @$datas->jabatan;?></td></tr>
                    <tr><td>Ket Jenis Usaha</td> <td><?php echo @$datas->usaha;?></td></tr>

                    <tr><td>tanggal Pesan</td> <td><?php echo @$datas->tanggal_daftar;?></td></tr>
                    <tr><td>Lama Training</td> <td><?php echo @$datas->jumlah_hari;?></td></tr>
                    <tr><td>Jenis Pengguna</td> <td><?php echo @$datas->jenis_pengguna;?></td></tr>
                    <tr><td>Versi Accurate</td> <td><?php echo @$datas->versi_accurate;?></td></tr>
                    <tr>
                      <td>Agenda Training</td> 
                      <td>
                        <?php if ($_SESSION['admin']['tipe']=='admin'||$_SESSION['admin']['tipe']=='marketing'): ?>
                          

                        
                        <?php else: ?>
                        
                        <?php echo "-"; ?>

                        <?php endif ?>
                    </td>
                    </tr>
                    <tr><td>Tempat Beli Accurate</td> <td><?php echo @$datas->tempat_beli_accurate;?></td></tr>
                    <tr><td>Salesman</td> <td><?php echo @$datas->salesman_accurate;?></td></tr>
                    <?php } ?>
				</tbody>
			</table>
		</div>
        <div class="col-md-6">
            <div id="map-canvas" style="width:100%;height:400px;border:1px solid"> Direction service not Available</div>            
        </div>
	</div>

</div>

<script>
      function initMap() {

        if (navigator.geolocation) {

            
        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;
        var map = new google.maps.Map(document.getElementById('map-canvas'), {
          zoom: 16,
          center: {lat: -6.214495207580589, lng: 106.76994860172272}
        });
        directionsDisplay.setMap(map);

          calculateAndDisplayRoute(directionsService, directionsDisplay);

        };


      }

      function calculateAndDisplayRoute(directionsService, directionsDisplay) {

            if (navigator.geolocation) {

                navigator.geolocation.getCurrentPosition(function(position){
                  var myCurrentLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
                directionsService.route({
                  origin: myCurrentLocation,
                  destination: <?php echo "'".$datas->alamat_perusahaan."'"; ?>,
                  travelMode: google.maps.TravelMode.DRIVING
                }, function(response, status) {
                  if (status === google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                  } else {
                    window.alert('Directions request failed due to ' + status);
                  }
                });



                });
      };

      }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCUn6Q6sg0JVJBlgiz4tL2kF1zmVoC0ScE&callback=initMap">
    </script>
     
</body>
</html> 
