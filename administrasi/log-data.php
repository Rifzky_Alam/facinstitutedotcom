<?php 
session_start();
if (!isset($_SESSION['admin'])||($_SESSION['admin']['username']!='charlez'&&$_SESSION['admin']['username']!='Fajar'&&$_SESSION['admin']['username']!='Imelia')){
    //echo $_SESSION['admin']['tipe'];
    header('Location:index.php');
}

include_once '../model/page.php';
include_once '../model/Petugas.php';
$ctrl_page = new Page();
$ctrlPetugas = new ControlPetugas();
$usernames = $ctrlPetugas->fetchUsernameAndName();
 

?>
<!DOCTYPE html>
<html>


<?php 

$page='dataOrderTraining';
include_once 'header.php'; 

?>

<body>
<?php 
//echo $_SESSION['admin']['tipe'];
?>


<?php 

include_once 'sidebar.php';

include_once 'top-nav.php';

 ?>

<?php 

    

    
?>

<?php 
    if (isset($_GET['data'])) {

        
        $objLog = json_decode($ctrl_page->fetchSearchLogData($_GET['data']['username'],$_GET['data']['tanggalAwal'],$_GET['data']['tanggalAkhir']));
        //$dataAbsen = $control->daftarHadirByParameter($_GET['tanggalAwal'],$_GET['tanggalAkhir'],$_GET['nama']);
    }else{
        $objLog = json_decode($ctrl_page->fetchLogData());
    }
?>

<div class="container" id="isi">
    
    <div class="row">
        <div class="col-md-6 col-sm-6">
            <h1>Log Data of Web Users</h1>
        </div>

        <div class="col-md-6 col-sm-6" style="padding-top:30px;text-align:right;">
            <button class="btn btn-primary" style="text-align:right" id="cari-data" href="#modal-cari" data-toggle="modal" >Cari Data <span class="glyphicon glyphicon-search"></span></button>
        </div>
    </div>
    
    <div class="row" style="overflow:auto">
        <div class="col-md-12">

            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="width:200px" class="tengah">Date</th>
                        <th class="tengah">Username</th>
                        <th class="tengah">Activity</th>
                        <th class="tengah">IP</th>                        
                    </tr>
                </thead>
                <tbody>
                    <!--<td>Rifzky</td><td>10:10</td><td>somewhere new</td><td>12:12</td><td>Somwhere new</td>-->
                    <?php for ($i=0; $i < count($objLog); $i++) { ?>
                    <tr>
                    	<td><?php echo $objLog[$i]->log_time;?></td>
                    	
                    	<td><?php echo $objLog[$i]->nama; ?></td>
                    	<td><?php echo $objLog[$i]->log_action; ?></td>
                    	<td><?php echo $objLog[$i]->log_ip;?></td>

                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>

	
</div>

<!-- Modal -->
  <div class='modal fade' id='modal-cari' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pencarian Data</h3>

        </div>
       <div class='modal-body' style='overflow-y:scroll'>
        <div style='padding:15px; height: 300px;' class='row'>
          
          <div class="col-md-12">
            
              
                <form action=<?php echo "'".$_SERVER['SCRIPT_NAME']."'"; ?> method="GET">

                    <div class="row">
                        <div class="form-group">
                        	<label>Username</label>
                        	<select type='text' name='data[username]' class='form-control' >
                                <option value='' selected>--Pilih username--</option>
                                <?php 
                                    for ($i=0; $i < count($usernames); $i++) { 
                                ?>
                                <option value=<?php echo "'".$usernames[$i]->getUsername()."'"; ?>>
                                    <?php echo $usernames[$i]->getNama(); ?>
                                </option>
                                <?php        
                                    }
                                ?>

                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label for="tanggal-awal">Tanggal Awal</label>
                            <input type="text" name="data[tanggalAwal]" id="tanggal-awal" class="form-control" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label for="tanggal-awal">Tanggal Akhir</label>
                            <input type="text" name="data[tanggalAkhir]" id="tanggal-akhir" class="form-control" />
                        </div>
                    </div>

                    <div class="row">
                        <button class="btn btn-success" style='width:100%'>Cari</button>
                    </div>

                </form>
          </div>
             
          

        </div>
       </div>


        <div class='modal-footer'>

            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>FAC-Institute 2016</span>
                </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->  
<script type="text/javascript" src=<?php echo "'".getBaseUrl()."js/jquery-datepicker/jquery-ui.js'"; ?>></script>
<script type="text/javascript">
    $(document).ready(function(){

        //alert('okeh');

      $('#tanggal-awal').datepicker({

        dateFormat: 'yy-mm-dd'

      });



      $('#tanggal-akhir').datepicker({

        dateFormat: 'yy-mm-dd'

      });

    });

</script>
</body>
</html> 