<?php 
session_start();
include_once 'session/session-class.php';
include_once '../baseurl.php';
include_once '../model/Perusahaan.php';
include_once '../model/Customer.php';
include_once '../model/Transaksi.php';
// include_once '../model/page.php';
include_once '../model/Invoice.php';
include_once '../model/Perusahaan.php';
include_once '../model/Customer.php';
$session = new Sessionz();
$session->AdminMarketing();


$customer = new Customer();
$transaksi = new Transaksi();
$perusahaan = new Perusahaan();
$invoice = new Invoice();
// $file = new File();
if (isset($_GET['tr'])&&!empty($_GET['tr'])) {
    $transaksi->setID($_GET['tr']);
    $datatrans = $transaksi->FetchTransactionDataById();
    $datausaha = $perusahaan->selectByID($datatrans->trans_id_usaha);
    // validate accurate version: able to make invoice when accurate version is set
    if ($datatrans->trans_jenis!='4') {
        if ($datatrans->trans_va=='0') {
            echo "<script>alert('Untuk melanjutkan, silahkan isi data versi accurate untuk transaksi ini terlebih dahulu.');</script>";
            echo "<script>location.replace('".getBaseUrl()."administrasi/new-transaksiz?edt=".$_GET['tr']."');</script>";
        }
    }

    if ($datausaha->nama!='-') {
        if (!is_numeric($datausaha->provinsi)||!is_numeric($datausaha->kota)) {
            echo "<script>alert('Harap lengkapi data provinsi dan kota perusahaan terlebih dahulu.');</script>";
            echo "<script>location.replace('".getBaseUrl()."administrasi/new-usaha?edt=".$datatrans->trans_id_usaha."');</script>";
        }
    }
        

    //post data
    if (isset($_POST['in'])) {
        // print_r($_POST['in']);
        // $invoice->setID();
        $invoice->setIdtransaksi($_GET['tr']);
        $invoice->setMetodebayar($_POST['in']['metodebayar']);
        $invoice->setAlamat($_POST['in']['alamatinv']);
        $invoice->setDeskripsi($_POST['in']['deskripsi']);
        $invoice->setTanggal($_POST['in']['tanggal']);

        if ($invoice->InputNewData()) {
            echo "<script>alert('Data Invoice berhasil disimpan!');</script>";
            echo "<script>location.replace('new-perusahaan-info?tr=".$_GET['tr']."');</script>";      
        } else {
            echo "<script>alert('Data Customer gagal disimpan!');</script>";
        }
    }
    // end post


    $data = array(
        'base_url' => getBaseUrl(), //base data
        'url'=> 'new-customer',
        'judul' => 'Tambah Invoice/FAC Institute',
        'username'=>$_SESSION['admin']['nama'],
        'page' => 'tambah-invoice', // end base data
        'nama_usaha' => $datatrans->nama,
        'alamatinv' => $datatrans->alamat,
        'id_transaksi' => $_GET['tr'],
        'petugas' => $_SESSION['admin']['nama']
    );

    // print_r($datatrans);

    $data = (object) $data;
    include_once 'view/invoice/input-invoice.php';

}elseif (isset($_GET['edt'])&&!empty($_GET['edt'])) { //edit invoice
    $invoice->setNo($_GET['edt']);
    // $datainv=$customer->FetchSelectedID();
    $datainvoice=$invoice->GetByID();
    if (isset($_POST['ubh'])) {
        // print_r($_POST['ubh']);
        $invoice->setMetodebayar($_POST['ubh']['metodebayar']);
        $invoice->setDeskripsi($_POST['ubh']['deskripsi']);
        $invoice->setAlamat($_POST['ubh']['alamatinv']);
        $invoice->setTanggal($_POST['ubh']['tanggal']);

        if ($invoice->EditData()) {
            echo "<script>alert('Data Invoice berhasil diubah!');</script>";
            echo "<script>location.replace('new-perusahaan-info?tr=".$datainvoice->inv_id_trans."');</script>"; 
        } else {
            echo "<script>alert('Data Invoice gagal diubah!');</script>"; 
        }
        

    }

    $data = array(
        'base_url' => getBaseUrl(), //base data
        'url'=> 'new-customer',
        'judul' => 'Edit Invoice/FAC Institute',
        'username'=>$_SESSION['admin']['nama'],
        'page' => 'tambah-invoice', // end base data
        'nama_usaha' => $datainvoice->nama_usaha,
        'id_transaksi' => $_GET['edt'],
        'alamatinv' => $datainvoice->inv_alamat,
        'metode_bayar'=>$datainvoice->inv_metode_bayar,
        'deskripsi'=>$datainvoice->inv_deskripsi,
        'petugas' => $_SESSION['admin']['nama']
    );    
    $data['tanggal'] = $datainvoice->inv_date;
    $data['methods'] = $invoice->FetchAllMethods();

    $data = (object) $data;
    include_once 'view/invoice/edit-invoice.php';

}elseif (isset($_GET['send'])&&!empty($_GET['send'])){ //sending invoice
    $session->OnlyAdmin();

    if ($_SESSION['admin']['tipe']=='admin') {

        include_once '../model/Mail.php';
        include_once '../model/Transaksi.php';
        include_once '../model/Invoice.php';


        //declaration class
        $mail = new FACMail();
        $transaksi = new Transaksi();
        $invoice = new Invoice();
        $invoice->setNo($_GET['send']);

        $datainv = $invoice->GetByID();
        $datasurat = $invoice->FetchDataSurat();

        // check if invoice pdf is ready to send
        $path = getcwd().'/'.$invoice->getFolder().'Invoice '.$datainv->inv_id_trans.'.pdf';
        if (!file_exists($path)) {
            echo "<script>alert('File Invoice tidak ada, harap untuk membuatnya terlebih dahulu!');</script>";
            echo "<script>location.replace('new-attachments?inv=".$_GET['send']."');</script>";        
        }

        $transaksi->setID($datainv->inv_id_trans);

        $datas = $transaksi->FetchTransactionForPreview();

        // data for mail content
        $datacontent = array(
            'nama_cust' => $datas->nama_cust,
            'telepon_cust' => $datas->telp_cust,
            'email_cust' =>$datas->email_cust,
            'nama_usaha' => $datas->nama,
            'tanggal' => explode('##', $datas->tgl),
            'waktu' => $datas->trans_waktu,
            'tempat' => $datas->trans_lokasi, 
            'alamat' => $datas->alamat,
            'agenda' => explode('##', $datas->nama_agenda),
            'items' => $transaksi->FetchItemsForInvoice(),
            'notes' => explode('##', $datas->trans_notes),
            'metode_bayar' => $datainv->inv_metode_bayar, 
            'petugas'=> $datas->petugas
        );

    
        if ($datas->trans_jenis=='1') {
            $datacontent = (object) $datacontent;
            include_once 'view/invoice/mail-content.php';

            $content = Invoice($datacontent);

        }elseif ($datas->trans_jenis=='2') {
            $datacontent['syarat']= array(
                'Entry data bersifat result oriented, bukan time oriented.',
                'Kami diberi akses ke sumber data yang akan dientry.'
            );
            if ($datas->trans_notes!='') {
                $notes = explode('##', $datas->trans_notes);            
                foreach ($notes as $key) {
                    array_push($datacontent['syarat'], $key);
                }
            }
            $datacontent = (object) $datacontent;
            include_once 'view/invoice/mail-content.php';

            $content = InvoiceAccountingService($datacontent);
        
        }elseif ($datas->trans_jenis=='3') {
            $datacontent = (object) $datacontent;
            include_once 'view/invoice/mail-content.php';

            $content = Kursus($datacontent);
        }
    
        $arr = array();
        array_push($arr, $invoice->getFolder().'Invoice '.$datainv->inv_id_trans.'.pdf');
  
        $lampiran = $invoice->CekDataLampiran($datasurat->emd_attachments);
        if ($lampiran!=''){
            for ($i=0; $i < count($lampiran) ; $i++) { 
                array_push($arr, $transaksi->getFolder().$lampiran[$i]);
            }
        }
        

        if (!empty($datasurat->emd_cc)||$datasurat->emd_cc!='') {
        $cece = explode(" ", $datasurat->emd_cc);
        } else {
            $cece = '';
        }
    
        // data for email
        $dataEmail = array(
            'to' => $datasurat->emd_to,
            'cc'=> $cece,
            'nama'=> '',
            'subject'=> 'Invoice Dan Jadwal Training '.$datas->nama,
            'content'=>$content,
            'attachments'=> $arr
        );
        //change invoice status to 'sent'
        $invoice->setStatus('1');
        if ($invoice->ChangeStatus()) {
            $mail->KirimJadwal((object) $dataEmail);
            unlink($path);
            $data = array(
                'base_url' => getBaseUrl(), //base data
                'url'=> 'new-customer',
                'judul' => 'Sending Invoice Report/FAC',
                'username'=>$_SESSION['admin']['nama'],
                'page' => 'tambah_customer', // end base data
                'id_transaksi' => $_GET['send'],
                'judul_berita' => 'Email telah terkirim!',
                'content_berita' => 'Invoice berhasil dikirim, anda dapat membuka email admin@fac-institute.com dalam beberapa menit untuk mengecek apakah invoice benar-benar terkirim ke tujuan. Bila terjadi kesalahan atau email tidak terkirim lebih dari 3 kali mencoba, harap menghubungi admin sistem.'
            );    
            $data['id_transaksi'] = $datainv->inv_id_trans;
            $data = (object) $data;
            include_once 'view/invoice/mail-report.php';

        } else {
            echo "<script>alert('Terjadi kesalahan pada database, silahkan untuk refresh halaman ini!');</script>";   
        }
    
    }elseif ($_SESSION['admin']['tipe']=='marketing') {
        include_once HomeDirectory().'model/Mail.php';
        include_once HomeDirectory().'administrasi/view/invoice/mail-content.php';
        include_once HomeDirectory().'model/Invoice.php';
        include_once HomeDirectory().'model/Transaksi.php';
        $transaksi = new Transaksi();
        $invoice = new Invoice();

        $invoice->setNo($_GET['send']);
        $datainv = $invoice->GetByID();

        $transaksi->setID($datainv->inv_id_trans);
        $datas = $transaksi->FetchTransactionForPreview();

        $data = ['petugas' => $_SESSION['admin']['nama'],'nama_usaha' => $datas->nama];
        $data = (object)$data;
        $surat = new FACMail();

        $dataEmail = array(
            'to' => 'cs.facinstitute@gmail.com',
            'cc'=>array('finance@fac-institute.com','ceso.facinstitute@gmail.com'),
            'nama'=> '',
            'subject'=> 'New Invoice of '.$datas->nama.' Has Been Created.',
            'content'=> Notif($data)
        );
        $dataEmail = (object) $dataEmail;
        // sending invoice
        $surat->sendMailWithCC($dataEmail);
    }

}elseif (isset($_GET['valid'])&&!empty($_GET['valid'])) {
    
    $session->OnlyAdmin();

    if (isset($_POST['delinv'])) {
        if(!file_exists($invoice->getFolder().$_POST['delinv'])){
            echo "<script>alert('File PDF tidak ada, anda dapat membuatnya kembali dengan klik edit data surat.');</script>";
        }else {
            // 
            if (unlink($invoice->getFolder().$_POST['delinv'])) {
                echo "<script>alert('File telah di hapus');</script>"; 
            }else {
                echo "<script>alert('File gagal di hapus, harap hubungi admin sistem kami!');</script>";
            }
        }
    }


    if ($_SESSION['admin']['tipe']=='admin') {
        $invoice->setNo($_GET['valid']);
        $datasurat = $invoice->FetchDataSurat();

        if (!isset($datasurat->emd_to)||$datasurat->emd_to==''||empty($datasurat->emd_to)) {
            echo "<script>alert('Data email belum siap, silahkan lengkapi terlebih dahulu.');</script>";
            echo "<script>location.replace('https://fac-institute.com/administrasi/new-attachments?inv=".$_GET['valid']."');</script>";
        }

        $data = array(
            'base_url' => getBaseUrl(), //base data
            'url'=> basename(__FILE__,'.php'),
            'judul' => 'Validation Invoice Report/FAC',
            'username'=>$_SESSION['admin']['nama'],
            'page' => 'tambah_customer', // end base data
            'email' => $datasurat->emd_to,
            'no_invoice' => $_GET['valid'],
            'idtransaksi' => $datasurat->inv_id_trans,
            'carboncopy' => $datasurat->emd_cc,
            'attachments' => $datasurat->emd_attachments
        );
        // echo "oke";
        $data = (object) $data;
        include_once 'view/invoice/mail-validation.php';
    }
}else{
    echo "<script>alert('harap memilih transaksi/invoice terlebih dahulu!');</script>";
    echo "<script>location.replace('index');</script>";
}




 ?>
<?php 


?>
