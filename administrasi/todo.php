<?php 
include_once 'controller/todo.controller.php';
session_start();
if (!isset($_SESSION['admin'])) {
	header('Location:http://www.fac-institute.com/administrasi/accessdenied');
}
if (isset($_SERVER['PATH_INFO'])) {
	@$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
	$key = array_shift($url_segment);
	switch ($key) {
		case 'new':
			Todo::Input();
			break;
		case 'detail':
			if (isset($url_segment)&&count($url_segment)<1) {
				echo 'id required!';
			}else{
				Todo::ShowDetail($url_segment[0]);
			}
			break;
		case 'editstaff':
			if (isset($url_segment)&&count($url_segment)<1) {
				echo 'id required!';
			}else{
				Todo::EditStaff($url_segment[0]);
			}
			break;
		case 'update':
			if (isset($url_segment)&&count($url_segment)<1) {
				echo 'id required!';
			}else{
				// Todo::EditAdmin($url_segment[0]);
			}
			break;
		case 'mytodolist':
			Todo::Showmine();
			break;
		case 'changestat':
			if (isset($url_segment)&&count($url_segment)<2) {
			header('Location:https://fac-institute.com/administrasi');
			}else{
				Todo::ChangeStatus($url_segment[1], $url_segment[0]);
			}
			break;
		case 'showadmin':
				Todo::DisplayAdmin();
			break;
		case 'setsession':
			$_SESSION['admin'] =  [
			    'username' => 'charlez',
			    'tipe' => 'admin',
			    'team' => '6',
			    'nama' => 'Rifzky Alam' 
			];
			break;
		case '':
			$result = Todo::ListKerjaan();
			print_r($result);
			break;
		default:
			
			break;
	}
}else{

}