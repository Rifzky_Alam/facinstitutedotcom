<?php 
session_start();
include_once 'session/session-class.php';
include_once '../baseurl.php';
include_once '../model/Marketing.php';
$session = new Sessionz();
$session->AdminMarketing();

$marketing = new Marketing();

// $file = new File();
if (isset($_GET['action'])&&$_GET['action']=='new') {

    //post data
    if (isset($_POST['in'])) {
        // print_r($_POST['in']);
        $marketing->setNama($_POST['in']['namalengkap']);
        $marketing->setEmail($_POST['in']['email']);
        $marketing->setTelepon($_POST['in']['telepon']);
        $marketing->setCabang($_POST['in']['cabang']);

        if ($marketing->NewData()) {
            echo "<script>alert('Data berhasil disimpan!')</script>";
            echo "<script>location.replace('new-marketing')</script>";
        } else {
            echo "<script>alert('Data berhasil disimpan!')</script>";
        }
        

    }
    // end post

    $data = array(
        'base_url' => getBaseUrl(), //base data
        'url'=> 'new-customer',
        'judul' => 'Tambah Marketing/FAC Institute',
        'username'=>$_SESSION['admin']['nama'],
        'page' => 'tambah agenda', // end base data
        'cabang' => $marketing->FetchCabang()
    );

    $data = (object) $data;
    include_once 'view/marketing/view-input-marketing.php';

}elseif (isset($_GET['edt'])&&!empty($_GET['edt'])) {
    $marketing->setID($_GET['edt']);
    $sales = $marketing->FetchByID();

    //post data
    if (isset($_POST['in'])) {
        // print_r($_POST['in']);
        $marketing->setNama($_POST['in']['namalengkap']);
        $marketing->setEmail($_POST['in']['email']);
        $marketing->setTelepon($_POST['in']['telepon']);
        $marketing->setCabang($_POST['in']['cabang']);

        if ($marketing->EditData()) {
            echo "<script>alert('Data berhasil diubah!')</script>";
            echo "<script>location.replace('new-marketing')</script>";
        } else {
            echo "<script>alert('Data berhasil diubah!')</script>";
        }
        

    }
    // end post

    $data = array(
        'base_url' => getBaseUrl(), //base data
        'url'=> 'new-customer',
        'judul' => 'Edit Marketing/FAC Institute',
        'username'=>$_SESSION['admin']['nama'],
        'page' => 'tambah agenda', // end base data
        'listcabang' => $marketing->FetchCabang()
    );
    $data['nama'] = $sales->marketing_nama;
    $data['email'] = $sales->marketing_email;
    $data['telepon'] = $sales->marketing_telp;
    $data['cabang'] = $sales->marketing_cabang;

    $data = (object) $data;
    include_once 'view/marketing/view-edit-marketing.php';


}else{
    $data = array(
        'base_url' => getBaseUrl(), //base data
        'url'=> 'new-marketing',
        'judul' => 'Data Marketing/FAC Institute',
        'subtitle' => 'Data Marketing',
        'username'=>$_SESSION['admin']['nama'],
        'page' => 'tambah agenda', // end base data
        'cabang' => $marketing->FetchCabang(),
        'list' => $marketing->ViewTableData()
    );

    $data = (object) $data;
    include_once 'view/marketing/view-data-marketing.php';


}

?>
