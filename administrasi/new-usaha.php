<?php 
session_start();
include_once 'session/session-class.php';
include_once '../baseurl.php';
include_once '../model/Perusahaan.php';
include_once '../model/Querybuilder.php';
$session = new Sessionz();
$session->AdminMarketing();
$qb = new QueryBuilder();
$usaha = new Perusahaan();


if (isset($_GET['act'])&&$_GET['act']=='new') {
	 include_once '/home/facinsti/public_html/administrasi/controller/perusahaan.controller.php';
  	if (isset($_POST['data'])) {

  		$usaha->setId(md5($_POST['data']['np'].'-'.$_POST['data']['alamat']));
		  $usaha->setNamausaha($_POST['data']['np']);
  		$usaha->setAlamat($_POST['data']['alamat']);
  		$usaha->setEmail($_POST['data']['email']);
  		$usaha->setTelepon($_POST['data']['telepon']);

      $usaha->setKota($_POST['data']['kota']);
  		$usaha->setProvinsi($_POST['data']['provinsi']);
      $usaha->setKecamatan($_POST['data']['sbd']);

  		$usaha->setTelepon($_POST['data']['telepon']);
  		$usaha->setKetjnsusaha($_POST['data']['ketJenisUsaha']);
  		$usaha->setMap($_POST['data']['map']);

      Usaha::NewPerusahaanPost($_POST['data']);
  		// print_r($_POST['data']);

  		if (isset($_POST['data']['jnsAccurate'])) {
  			$usaha->InputVersiAccurate($usaha->getId(),$_POST['data']['jnsAccurate']);
  		}

  		if (isset($_POST['data']['jnsUsaha'])) {
  			$usaha->InputJenisUsaha($usaha->getId(),$_POST['data']['jnsUsaha']);
  		}

  		if ($usaha->NewData()) {
  			echo "<script>alert('Data berhasil disimpan!');</script>";
			echo "<script>location.replace('new-usaha?id=".$usaha->getId()."');</script>";
  		} else {
  			echo "<script>alert('Data gagal disimpan!');</script>";
  		}
  		
	    
	        
	}

	$data = array(
		'base_url' => getBaseUrl(), //base data
        'url'=> basename(__FILE__, '.php'),
        'judul' => 'Input Perusahaan/FAC Institute',
        'username'=>$_SESSION['admin']['nama'],
        'subtitle' => 'Input Perusahaan Baru',
        'page' => 'perusahaan', // end base data
        'jenisusaha' => $usaha->FetchJenisUsaha(),
        'accurates'=> $usaha->FetchVersiAccurate()

    );
	$data = (object) $data;
	include_once 'view/usaha/view-input-usaha.php';

}elseif (isset($_GET['impl'])&&!empty($_GET['impl'])) {
  include_once 'controller/adminusaha.controller.php';
  Usaha::coba();
}elseif (isset($_GET['imp'])&&!empty($_GET['imp'])) {
  include_once 'controller/adminusaha.controller.php';
  Usaha::ImportData($_GET['imp']);
}elseif (isset($_GET['id'])&&!empty($_GET['id'])) {

	$datausaha = $usaha->selectByID($_GET['id']);
	$usaha->setId($_GET['id']);
	$data = array(
		'base_url' => getBaseUrl(), //base data
        'url'=> basename(__FILE__, '.php'),
        'judul' => 'Detail Perusahaan/FAC Institute',
        'username'=>$_SESSION['admin']['nama'],
        'subtitle' => 'Detail - '.$datausaha->nama,
        'page' => 'detail-transaksi', // end base data
        'id_perusahaan' => $_GET['id'],
        'nama_usaha' => $datausaha->nama,
        'email_usaha'=> $datausaha->email,
        'telepon_usaha' => $datausaha->telepon,
        'alamat'=> $datausaha->alamat,
        'map'=> $datausaha->map,
        'ket_jenis_usaha'=> $datausaha->ket_jenis_usaha,
        'jenis_usaha'=> $usaha->FetchJenisUsahaPerusahaan(),
        'versi_accurate' => $usaha->FetchVersiAccurateUsaha()
	);
  $data['provinsi'] = $datausaha->prov_nama;
  $data['kota'] = $datausaha->kota_nama;
  $data['kec'] = $datausaha->kec_nama;
  $data['tipeusaha']=$datausaha->p_status;
  
	$data = (object) $data;
	include_once 'view/usaha/view-usaha-detail.php';

}elseif (isset($_GET['iju'])&&!empty($_GET['iju'])) {
	$datausaha = $usaha->selectByID($_GET['iju']);
	$usaha->setId($_GET['iju']);

	if (isset($_POST['in'])) {

		$usaha->setId($_GET['iju']);
		$usaha->setJnsusaha($_POST['in']['ju']);
		

		// print_r($usaha);

		if ($usaha->NewJenisUsaha()) {
			echo "<script>alert('Data berhasil disimpan!');</script>";
			// echo "<script>location.replace('new-usaha?id=".$_GET['iva']."');</script>";
		} else {
			echo "<script>alert('Data gagal disimpan!');</script>";
		}	
	}


	$data = array(
		'base_url' => getBaseUrl(), //base data
        'url'=> basename(__FILE__, '.php'),
        'judul' => 'Input Jenis Usaha Perusahaan/FAC Institute',
        'username'=> $_SESSION['admin']['nama'],
        'subtitle' => 'Jenis Usaha '.$datausaha->nama,
        'page' => 'detail-transaksi', // end base data
        'id_perusahaan' => $_GET['iju'],
        'nama_usaha' => $datausaha->nama,
        'jenisusaha'=> $usaha->FetchJenisUsaha(),
        'listjenisusaha'=> $usaha->FetchJenisUsahaPerusahaan()
        
	);
	$data = (object) $data;
	include_once 'view/usaha/view-input-ju.php';

}elseif (isset($_GET['iva'])&&!empty($_GET['iva'])) {
	$datausaha = $usaha->selectByID($_GET['iva']);
	$usaha->setId($_GET['iva']);

	if (isset($_POST['in'])) {
		$usaha->setId($_GET['iva']);
		$usaha->setAccurate($_POST['in']['va']);
		if ($usaha->NewVersiAccurate()) {
			echo "<script>alert('Data berhasil disimpan!');</script>";
			// echo "<script>location.replace('new-usaha?id=".$_GET['iva']."');</script>";
		} else {
			echo "<script>alert('Data gagal disimpan!');</script>";
		}
		
	}


	$data = array(
		'base_url' => getBaseUrl(), //base data
        'url'=> basename(__FILE__, '.php'),
        'judul' => 'Input Versi Accurate Perusahaan/FAC Institute',
        'username'=>$_SESSION['admin']['nama'],
        'subtitle' => 'Versi Acc '.$datausaha->nama,
        'page' => 'detail-transaksi', // end base data
        'id_perusahaan' => $_GET['iva'],
        'nama_usaha' => $datausaha->nama,
        'accurates'=> $usaha->FetchVersiAccurate(),
        'listaccurate'=> $usaha->FetchVersiAccurateUsaha()
	);
	$data = (object) $data;
	include_once 'view/usaha/view-input-va.php';

}elseif (isset($_GET['dva'])&&!empty($_GET['dva'])) {
	$usaha->setAccurate($_GET['dva']);
	$deldata = $usaha->FetchSelectedVersiAccurate();
	$usaha->setId($deldata->uva_source);

	if (isset($_POST['in'])) {
		// print_r($_POST['in']);
		$usaha->setAccurate($_POST['in']['va']);
		if ($usaha->DeleteVersiAccurate()) {
			echo "<script>alert('Data berhasil dihapus!');</script>";
			echo "<script>location.replace('new-usaha?id=".$deldata->uva_source."');</script>";
		} else {
			echo "<script>alert('Data gagal dihapus!');</script>";
		}
		
	}

	$datausaha = $usaha->selectByID($deldata->uva_source);
	$data = array(
		'base_url' => getBaseUrl(), //base data
        'url'=> basename(__FILE__, '.php'),
        'judul' => 'Delete Versi Accurate Perusahaan/FAC',
        'username'=>$_SESSION['admin']['nama'],
        'subtitle' => 'Versi Acc '.$datausaha->nama,
        'page' => 'detail-transaksi', // end base data
        'id_perusahaan' => $deldata->uva_source,
        'nama_usaha' => $datausaha->nama,
        'va_id'=> $_GET['dva'],
        'listaccurate'=> $usaha->FetchVersiAccurateUsahaa($_GET['dva'])
	);
	$data = (object) $data;
	include_once 'view/usaha/view-delete-va.php';

}elseif (isset($_GET['dju'])&&!empty($_GET['dju'])) {
	$usaha->setJnsusaha($_GET['dju']);
	$deldata = $usaha->FetchSelectedJenisUsaha();
	$usaha->setId($deldata->id_company);

	if (isset($_POST['in'])) {
		// print_r($_POST['in']);
		$usaha->setJnsusaha($_POST['in']['ju']);
		if ($usaha->DeleteJenisUsaha()) {
			echo "<script>alert('Data berhasil dihapus!');</script>";
			echo "<script>location.replace('new-usaha?id=".$deldata->id_company."');</script>";
		} else {
			echo "<script>alert('Data gagal dihapus!');</script>";
		}
		
	}

	$datausaha = $usaha->selectByID($deldata->id_company);
	$data = array(
		'base_url' => getBaseUrl(), //base data
        'url'=> basename(__FILE__, '.php'),
        'judul' => 'Delete Jenis Usaha/FAC Institute',
        'username'=>$_SESSION['admin']['nama'],
        'subtitle' => 'Delete Jenis Usaha '.$datausaha->nama,
        'page' => 'detail-transaksi', // end base data
        'id_perusahaan' => $deldata->id_company,
        'ju_id'=>$_GET['dju'],
        'nama_usaha' => $datausaha->nama,
        'listjenisusaha'=> $usaha->FetchJenisUsahaPerusahaann($_GET['dju'])
	);
	$data = (object) $data;
	include_once 'view/usaha/view-delete-ju.php';	
}elseif (isset($_GET['namausaha'])&&!empty($_GET['namausaha'])) {
	$usaha->setNamausaha($_GET['namausaha']);

	$data = array(
		'base_url' => getBaseUrl(), //base data
        'url'=> basename(__FILE__, '.php'),
        'judul' => 'Cari Perusahaan/FAC Institute',
        'username'=>$_SESSION['admin']['nama'],
        'subtitle' => 'Perusahaan - FAC Institute',
        'page' => 'perusahaan', // end base data
        'mylists'=> $usaha->FetchNewSearch()
    );
	$data = (object) $data;
	// print_r($data->mylists);
	include_once 'view/usaha/view-usaha-search.php';
}elseif (isset($_GET['edt'])&&!empty($_GET['edt'])) {
  include_once '/home/facinsti/public_html/administrasi/controller/perusahaan.controller.php';
  include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/functions/WilayahFunc.php';
	$datausaha = $usaha->selectByID($_GET['edt']);

	if (isset($_POST['data'])) {
		// print_r($_POST['data']);
		$usaha->setId($_GET['edt']);
		$usaha->setNamausaha($_POST['data']['np']);
  		$usaha->setAlamat($_POST['data']['alamat']);
  		$usaha->setEmail($_POST['data']['email']);
  		$usaha->setTelepon($_POST['data']['telepon']);

      $usaha->setKota($_POST['data']['kota']);
  		$usaha->setProvinsi($_POST['data']['provinsi']);
      $usaha->setKecamatan($_POST['data']['sbd']);

  		$usaha->setTelepon($_POST['data']['telepon']);
  		$usaha->setKetjnsusaha($_POST['data']['ketJenisUsaha']);
  		$usaha->setMap($_POST['data']['map']);

      $alteration = [
          'nu' => $_POST['data']['np'],
          'addr' => $_POST['data']['alamat'],
          'eml' => $_POST['data']['email'],
          'phn' => $_POST['data']['telepon'],
          'kot' => $_POST['data']['kota'],
          'kec' => $_POST['data']['sbd'],
          'prv' => $_POST['data']['provinsi'],
          'ju' => $_POST['data']['ketJenisUsaha']
      ];

      // input data provinsi, kota & kecamatan from API
      Usaha::NewPerusahaanPost($_POST['data']);

  		if ($usaha->EditUsaha()) {
        $qb->InsertData('log_data',
          [
            'username' => $_SESSION['admin']['username'],
            'log_action' => 'alteration perusahaan table ( '.$_GET['edt'].' ) -> '.json_encode($alteration),
            'log_ip' => $_SERVER['REMOTE_ADDR']
          ]
        );
  			echo "<script>alert('Data berhasil disimpan!');</script>";
			echo "<script>location.replace('new-usaha?id=".$usaha->getId()."');</script>";
  		} else {
  			echo "<script>alert('Data gagal disimpan!');</script>";
  		}



	}

  // data for view
	$data = array(
		'base_url' => getBaseUrl(), //base data
        'url'=> basename(__FILE__, '.php'),
        'judul' => 'Edit Perusahaan/FAC Institute',
        'username'=>$_SESSION['admin']['nama'],
        'subtitle' => 'Edit '.$datausaha->nama,
        'page' => 'new-perusahaan', // end base data
        'id_perusahaan' => $_GET['edt'],
        'nama_usaha'=>$datausaha->nama,
        'alamat' => $datausaha->alamat,
        'email'=> $datausaha->email,
        'telepon'=>$datausaha->telepon,
        'kota'=> CekKota($datausaha->kota),
        'kecamatan' => CekKecamatan($datausaha->kec),
        'provinsi' => CekProv($datausaha->provinsi),
        'ketjenisusaha' => $datausaha->ket_jenis_usaha,
        'map'=>$datausaha->map
	);
	$data = (object) $data;
	include_once 'view/usaha/view-edit-usaha.php';	    
	
}elseif (isset($_GET['iptc'])&&!empty($_GET['iptc'])) {
	$datausaha = $usaha->selectByID($_GET['iptc']);

	if (isset($_POST['in'])) {
		// print_r($_POST['in']);
		$usaha->setId($_GET['iptc']);

		if ($usaha->NewCabang()) {
			echo "<script>alert('Cabang baru berhasil disimpan!');</script>";
			echo "<script>location.replace('new-usaha?id=".$usaha->getId()."');</script>";
		} else {
			echo "<script>alert('Cabang baru gagal disimpan!');</script>";
		}
		
	}



	$data = array(
		'base_url' => getBaseUrl(), //base data
        'url'=> basename(__FILE__, '.php'),
        'judul' => 'Input Cabang/FAC Institute',
        'username'=>$_SESSION['admin']['nama'],
        'subtitle' => 'Anda yakin memasukkan data ini sebagai cabang Market FAC Institute?',
        'page' => 'perusahaan', // end base data
        'id_perusahaan' => $_GET['iptc'],
        'nama_usaha'=>$datausaha->nama,
        'alamat' => $datausaha->nama,
        'email'=> $datausaha->email,
        'telepon'=>$datausaha->telepon,
        'kota'=> $datausaha->kota,
        'provinsi' => $datausaha->provinsi,
        'ketjenisusaha' => $datausaha->ket_jenis_usaha,
        'map'=>$datausaha->map
    );
    $data = (object) $data;
	include_once 'view/usaha/view-input-cabang.php';

}elseif (isset($_GET['ajxc'])) {
  if (!empty($_GET['ajxc'])||$_GET['ajxc']!='') {
        $usaha->setNamausaha($_GET['ajxc']);
        $data = $usaha->FetchNewSearch();

        foreach ($data as $key) {
            echo '<tr class="list">
                              <td>
                                  <input type="radio" name="in[namausaha]" value="'.$key->id.'">
                              </td>
                              <td class="geser">
                                  <div class="row">
                                      <div class="col-md-12">                    
                                          <div class="panel-group" id="'.$key->id.'">

                                          <div class="panel panel-default">
                                              <div class="panel-heading">
                                                  <h4 class="panel-title">
                                                  <a href="#'.$key->id.'0" data-parent="#'.$key->id.'" class="tengah" data-toggle="collapse">'.$key->nama.'</a>
                                                  </h4>
                                              </div>
                                              <div class="panel-collapse in" id="'.$key->id.'0">
                                                  <div class="panel-body">
                                                      <section class="col-md-3">
                                                          <label>Jenis usaha</label>
                                                              <p>'.$key->jenis_usaha.'</p>
                                                      </section>
                                                      <section class="col-md-3">
                                                          <label>Kota</label>
                                                              <p>'.$key->kota.'</p>
                                                      </section>
                                                      <section class="col-md-3">
                                                          <label>Telepon</label>
                                                          <p>'.$key->telepon.'</p>
                                                      </section>
                                                      <section class="col-md-3">
                                                          <label>Alamat</label>
                                                          <p>
                                                              '.$key->alamat.'
                                                          </p>
                                                      </section>
                                                  </div>
                                              </div>
                                          </div>

                                          </div>

                                      </div>
                                  </div>
                              </td>
                            </tr>';
        }
        

  }
  
  
  // print_r($data);


} else {
	$data = array(
		'base_url' => getBaseUrl(), //base data
        'url'=> basename(__FILE__, '.php'),
        'judul' => 'Perusahaan/FAC Institute',
        'username'=>$_SESSION['admin']['nama'],
        'subtitle' => 'Perusahaan - FAC Institute',
        'page' => 'perusahaan' // end base data
    );
	$data = (object) $data;
	include_once 'view/usaha/view-usaha-default.php';	    
}



?>