<?php
session_start();

include_once '../baseurl.php';
include_once 'session/session-class.php';
include_once '../model/Customer.php';

$session = new Sessionz();
$session->AdminMarketing();
$customer = new Customer();

if (isset($_GET['src'])) {
	$customer->setNama($_GET['src']['nc']);
	$customer->setMarketingID($_GET['src']['nm']);
	$data = array(
		'base_url' => getBaseUrl(), //base data
        'url'=> basename(__FILE__, '.php'),
        'judul' => 'Data Customer/FAC Institute',
        'username'=>$_SESSION['admin']['nama'],
        'page' => 'input-surat', // end base data
        'list' => $customer->SearchCustomer(),
		'report_marketing' => $customer->ReportCustomerPerMarketing()
	);
        

	$data['totaldata'] = count($data['report_marketing']);
	$data['pagenum'] = 0;

	$data = (object) $data;
	include_once 'view/customer/view-data-customer.php';
	
}elseif (isset($_GET['page'])&&!empty($_GET['page'])) {
	$limit = 30;
	$offset = ($_GET['page']-1)*$limit;
	$data = array(
		'base_url' => getBaseUrl(), //base data
        'url'=> basename(__FILE__, '.php'),
        'judul' => 'Data Customer/FAC Institute',
        'username'=>$_SESSION['admin']['nama'],
        'page' => 'input-surat', // end base data
        'totaldata' => $customer->CountCustData(),
        'pagenum'=>$offset,
        'list' => $customer->TableCustomerList($offset,$limit),
		'report_marketing' => $customer->ReportCustomerPerMarketing()
	);

	$data = (object) $data;
	include_once 'view/customer/view-data-customer.php';

} else {
	$offset = 0;
	$limit = 30;

	$data = array(
		'base_url' => getBaseUrl(), //base data
        'url'=> basename(__FILE__, '.php'),
        'judul' => 'Data Customer/FAC Institute',
        'username'=>$_SESSION['admin']['nama'],
        'page' => 'input-surat', // end base data
        'totaldata' => $customer->CountCustData(),
        'pagenum'=>$offset,
        'list' => $customer->TableCustomerList($offset,$limit),
		'report_marketing' => $customer->ReportCustomerPerMarketing()
	);

	$data = (object) $data;
	include_once 'view/customer/view-data-customer.php';
}

?>
