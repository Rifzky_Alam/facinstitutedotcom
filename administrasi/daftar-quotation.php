<?php 
session_start();
if (!isset($_SESSION['admin'])||($_SESSION['admin']['tipe']!='admin'&&$_SESSION['admin']['tipe']!='marketing')) {
    header('Location:../login/index.php');
}
?>




<!DOCTYPE html>
<html>

<?php 
//echo $tipeAdmin=$_SESSION['admin']['tipe'];
$judul='Daftar Quotation';
$page = 'home';
include_once 'header.php';
include_once '../model/Pendaftar.php';
include_once '../model/page.php';

$myPage = new Page();
$pendaftar = new Pendaftar();

?>

<body>



<?php 

include_once 'sidebar.php';

include_once 'top-nav.php';

 ?>


<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3>Daftar Penawaran</h3>
	</div>
    <?php $datas=json_decode($pendaftar->fetchAllQuotation('1')); ?>
    
    <div class="row">
        <div class="col-md-12">
            <a href="penawaran-custom" class="btn btn-lg btn-primary">New <span class="glyphicon glyphicon-plus"></span></a>
        </div>
    </div>

    <div class="row" style="overflow:auto;">
        <div class="col-md-12">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Nama Perusahaan</th>
                        <th>Nama Kontak</th>
                        <th>Tanggal</th>
                        <th>Paket</th>
                        <th style="text-align:center;">Aksi</th>
                        <th style="text-align:center;">Kirim</th>
                    </tr>
                </thead>
                <tbody>

                    <?php  
                    for ($i=0; $i < count($datas); $i++) { ?>
                    
                    <?php if ($datas[$i]->status=='1'): ?>
                    <tr class="success">
                    <?php elseif($datas[$i]->status=='-1'): ?>
                    <tr class="info">    
                        <?php else: ?>
                    <tr class="warning">            
                    <?php endif ?>    
                    

                        <td <?php echo "id='nu-".str_replace('/', '', $datas[$i]->id)."'";?>><?php echo $datas[$i]->nama_usaha; ?></td>
                        <td <?php echo "id='np-".str_replace('/', '', $datas[$i]->id)."'";?>><?php echo $datas[$i]->nama_personal ?></td>
                        <td><?php echo $datas[$i]->time_sent ?></td>
                        <td <?php echo "id='npkt-".str_replace('/', '', $datas[$i]->id)."'";?>>
                            <?php echo $datas[$i]->nama_item; ?>
                        </td>
                        <td style="text-align:center;">
                            <a href="preview?qn=<?php echo $datas[$i]->id ?>" target="_blank">Preview</a> 
                            ||
                            <a href="penawaran-custom?ac=edit&id=<?php echo $datas[$i]->id ?>">Edit</a>  
                            || 
                            <a href="<?php echo getBaseUrl().'docs/quotation?qv='.$datas[$i]->id ?>" target="_blank">
                                PDF
                            </a>
                        </td>
                        <td style="text-align:center;">
                            <a href="#" class="btn btn-sm btn-info" <?php echo "id='".$datas[$i]->id."'" ?> onclick="showModal(this.id)">
                                <span class="glyphicon glyphicon-send"></span>
                            </a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>            
        </div>
    </div>


	
</div>

<!-- Modal -->
  <div class='modal fade' id='modal-cari' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pencarian Data</h3>

        </div>
        <div class='modal-body'>
        <div style='padding-left:15px;padding-right:15px;padding-top:0px; height: 300px;' class='row'>
          


          <div class="col-md-12">
            
            <div class="row">
                <h2>Anda yakin ingin mengirim data ini?</h2>    
            </div>

            <div class="row">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Nama Perusahaan</th>
                            <th>Nama Kontak</th>
                            <th>Paket</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td id="md-nu"></td>
                            <td id="md-nk"></td>
                            <td id="md-paket"></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="row">
                <a class="btn btn-success" id="md-btnkirim" target="_blank" style="width:100%">Kirim</a>
            </div> 

          </div>
             
          

        </div>
          

        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>FAC-Institute 2016</span>
                </div>
                <div class="col-md-6">
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->  

<script type="text/javascript">
    function showModal(id) {
        var aidi = id.replace(/\//g,'');
        var namaPerusahaan = $('#nu-'+ aidi).html();
        var namaKontak = $('#np-'+ aidi).html();
        var namaPaket = $('#npkt-'+ aidi).html();


        $('#md-nu').html(namaPerusahaan);
        $('#md-nk').html(namaKontak);
        $('#md-paket').html(namaPaket);
        $('#md-btnkirim').attr("href", "pdf/quotation?quotval="+id);
        $("#modal-cari").modal();
        // alert(id);
    }
</script>
</body>
</html> 