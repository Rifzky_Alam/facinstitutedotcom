<?php 
session_start();
if (!isset($_SESSION['admin'])||($_SESSION['admin']['tipe']!='admin'&&$_SESSION['admin']['tipe']!='marketing')) {
    header('Location:../login/index.php');
}
?>




<!DOCTYPE html>
<html>

<?php 
//echo $tipeAdmin=$_SESSION['admin']['tipe'];
$page = 'upload';
include_once 'header.php'; 

?>

<body>



<?php 

include_once 'sidebar.php';

include_once 'top-nav.php';

include_once '../model/page.php';
include_once '../model/File.php';

$page = new Page();
$file = new File();

 ?>
<?php 
    if (isset($_GET['dn'])&&!empty($_GET['dn'])) {
        if (unlink('attachments/'.$_GET['dn'])) {
            echo "<script>alert('Data berhasil di hapus!');</script>";
            echo "<script>location.replace('data-attachments');</script>";
        }else{
            echo "<script>alert('Data gagal di hapus!');</script>";
        }
    }
?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3>Permintaan Order Client</h3>
	</div>
	<div class="row">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Nama File</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <?php $files = scandir('attachments/'); ?>
            <tbody>
                <?php for ($i=0; $i < count($files); $i++) { ?>
                    <tr>
                        <?php if ($files[$i]!='.'&&$files[$i]!='..'): ?>
                            <td><?php echo $files[$i] ?></td>
                            <td><a href="<?php echo 'data-attachments?dn='.$files[$i] ?>">Delete</a></td>
                        <?php endif ?>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>

</div><!--end container-->
</body>
</html> 