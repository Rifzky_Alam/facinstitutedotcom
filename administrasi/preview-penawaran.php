<?php if (isset($_GET['jenis'])&&$_GET['jenis']=='1'): ?>
    
<table style="margin:0;padding:0" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr style="background-color: #0066ff;">
                            <td style="color:white;margin:0 auto;font-size:22px" align="center" valign="middle" height="60"><strong>FAC-Institute</strong></td>
                        </tr>
                    </tbody>
                </table>            
            </td>
        </tr>
        <tr>
            <table  align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tbody>
            <tr>
            <td style="width:50%"><center><img src="http://fac-institute.com/images/logoaccurate5.png" style="width:50%;height:60px"></center></td>
            <td style="width:50%"><center><img src="http://fac-institute.com/images/logo-fac.jpg" style="width:50%;height:100px"></center></td>
            </tr>
            </tbody>
            </table>
        </tr>
        <tr>
            <td valign="middle" height="60"><h4>Kepada Yth,</h4></td>
        </tr>
        <tr>
            <td valign="middle" height="60"><h4>PT. Alam Sejahtera</h4></td>
        </tr>
        <tr>
            <td valign="middle" height="60"><div><label>UP: </label></td><td>Rifzky Alam</div></td>
        </tr>
        <tr>
            <td>
                <p>
                    Degan Hormat, <br>
                    Terimakasih atas ketertarikan menggunakan program Accurate sebagai program akuntansi di <strong>Nama Perusahaan</strong>.
                    untuk <strong>Jasa Training Accurate</strong> kami memberikan penawaran terbaik berupa.<br> 
                </p>
                <br>
            </td>
        </tr>
        <tr>
            <td><br>
            <table valign="top" align="center" border="0" style="padding-bottom:30;" bgcolor="#f9ffee" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                    <td>
                        <table valign="top" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                                <tr><td><center><h3><strong>BIAYA</strong></h3></center></td></tr>
                                <tr>
                                <td style="margin-bottom:10px">
                                <table border="1" width="100%">
                                <thead><tr><th>Keterangan</th><th>Biaya</th></tr></thead>
                                <tbody><tr><td>Biaya Training per hari</td><td>Rp 100.000.-</td></tr><tr><td>Biaya transport per hari</td><td>Rp 50.000.-</td></tr></tbody>
                                </table>
                                </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tbody>

            </table>
                
            </td>
        </tr>
        <tr>
            <td>
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                <tr><td>
                <h3>PEMBAYARAN</h3>
                <ul type="square">
                    <li><strong>Bank BCA KCU Bekasi No Acc. 0663162851 a/n : Fajar Shodiq</strong></li>
                    <li><strong>Bank Mandiri KCP Bekasi Ahmad Yani No. Acc. 1670001377901 a/n : Fajar Shodiq</strong></li>
                </ul></td>
                </tr>
                </tbody>
                </table> 
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr style="background-color: purple;">
                            <td style="color:white;margin:0 auto" align="center" valign="middle" height="40">NOTES</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <ol>
                    <li>Mendapatkan support by email.</li>
                    <li>Pembayaran dilakukan sebelum implementasi dimulai.</li>
                    <li>Menyediakan makan siang bagi implementator.</li>
                    <li>Implementasi maksimal 7 jam termasuk istirahat makan siang, training bersifat time oriented, bukan result oriented</li>
                    <li>Implementasi diluar Jabodetabek, transport dan akomodasi ditanggung customer.</li>
                </ol>
            </td>
        </tr>
        <tr>
            <td>
                <p>Silahkan untuk mengisi data yang ada di lampiran dan membalas email ini dengan data yang sudah terisi ini atau anda dapat <a href="http://www.fac-institute.com/daftar-training">mengisi secara online</a></p>
                <p>
                Jika ada hal-hal yang ingin ditanyakan, jangan sungkan untuk senantiasa menghubungi kami. Terimakasih, <br><br>Salam <br><br>
                </p>
                <p><strong>Rifzky Alam</strong> <br> <strong>IT Staff of FAC-Institute</strong></p>
            </td>
        </tr>
        <tr>
            <td style="padding:0 0 0 0" align="center" bgcolor="#0066ff">
          <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:#0066ff">
            <tbody>
              <tr>
                <td>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr height="30">
                        <td align="center" width="100%" height="16"></td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr>
                        <td align="center" valign="top">
                          <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                              <tr>
                                <td align="left" width="20"></td>
                                <td align="left" valign="top">
                                  <table align="center" border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                      <tr height="15">
                                        <td align="left" height="15">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td style="line-height:18px" align="center" valign="top">
                                          <span style="font-family:'Open Sans,sans-serif';color:white;font-size:12px;text-align:center">Email ini dikirim oleh: <strong><a href="http://www.fac-institute.com" style="color:white">FAC-Institute</a></strong><br>
                                          Jl. Jatiwaringin No.8 Ruko Bakso Laguna<br>
                                          Telp. <a href="tel://+6281290083983" style="color:white">+6281290083983</a></span><br>
                                          <br>
                                            <a href="http://www.fac-institute.com" style="font-family:'Open Sans,sans-serif';font-size:22px;color:white">www.fac-institute.com</a><br><br>
                                            <p style="margin:0 0 1em 0;font-family:'Open Sans,sans-serif';font-size:16px;line-height:26px;color:white;font-weight:300;text-align:center">Konsultan Akuntansi dan Lembaga Pendidikan Komputerisasi</p>
                                        </td>
                                      </tr>
                                      <tr height="15">
                                        <td align="left" height="15">&nbsp;</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                                <td align="left" width="20"></td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr height="30">
                        <td align="left" width="100%" height="16"></td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
        </tr>
    </tbody>
</table>
<?php endif ?>