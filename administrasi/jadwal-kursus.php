<?php 
session_start();

if (!isset($_SESSION['admin'])&&!$_SESSION['admin']['type']=='admin'){
	header('Location:../index.php');
}
$page='dataPegawai'
?>

<?php 
$page = 'orderNew';
//include_once 'variables.php';

?>

<!DOCTYPE html>

<html>

<head>
	<title>FAC - Jadwal Kursus</title>

<?php include_once 'header.php'; ?>
<link rel="stylesheet" type="text/css" href=<?php echo "'".getBaseUrl()."css/datepicker/css/datepicker.css'"; ?>> 
</head>

<body>
	<?php if (isset($_GET['app'])&&$_GET['app']=='3'): ?>
		<?php include_once 'map-script.php'; ?>
	<?php endif ?>
	
	<?php include_once 'sidebar.php'; ?>
	<?php include_once 'top-nav.php'; ?>

	<?php 
	include_once '../model/Kursus.php';
	$kursus = new Kursus();
	$nama_kursus = json_decode($kursus->getDatas('nama_kursus'));
	$tempat_kursus =  json_decode($kursus->getDatas('tempat_kursus'));

	?>
	


	<div class="container" style='margin-bottom:40px;margin-top:40px'>

		<div class='row'>
			<div class='page-header'>
            	<h1>Jadwal Kursus</h1>
			</div>
		</div>
		<?php if (isset($_GET['app'])&&$_GET['app']=='2'): ?>

			<?php 
				if (isset($_POST['nm'])) {
					$input = array('namaKursus' => $_POST['nm']['namaKursus']);
					echo $kursus->addNamaKursus((object)$input);
				}
			?>


			<form action="" method="post">
				<div class="row">
					<div class="col-md-10">
						<label>Nama Kursus</label>
						<input type="text" name="nm[namaKursus]" class="form-control" required>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-10">
						<button class="btn btn-lg btn-primary" style="width:100%">Submit</button>
					</div>
				</div>

			</form>

			<br><br>
			<div class="row">
				<div class="col-md-10">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>Nama Kursus</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							for ($i=0; $i < count($nama_kursus) ; $i++) { 
								echo "<tr>";
								echo "<td>".$nama_kursus[$i]->nama_kursus."</td>";
								echo "<td><a href='".basename(__FILE__, '.php')."?app=delete&id=".$nama_kursus[$i]->id."' class='btn btn-danger'>Hapus</a></td>";
								echo "</tr>";
							}

							?>
						</tbody>
					</table>
				</div>
			</div>

		<?php elseif (isset($_GET['app'])&&$_GET['app']=='3'): ?>

			<?php 
				if (isset($_POST['lk'])) {
					$input = array('lokasi' => $_POST['lk']['lokasiKursus'],'map'=>$_POST['lk']['map']);
					echo $kursus->addLokasiKursus((object)$input);
				}
			?>



			<form action="" method="post">
				<div class="row">
					<div class="col-md-10">
						<label>Lokasi Kursus</label>
						<input type="text" name="lk[lokasiKursus]" class="form-control" required>
					</div>
				</div>
				<br>

				<div class="row">
            		<div class="col-md-10">
                		<div class="form-group">
                    		<label for="alamat">Lokasi Map</label>
                    		<input type="text" name="lk[map]" id="alamatz" class="form-control" placeholder="Cari Nama Tempat atau Tandai Peta di Bawah ini" required>                    
                		</div>
            		</div>
        		</div>
        		<br>
        		<div class="row" style="margin-bottom:20px">
            		<div class="col-md-10">
                		<div id="map-canvas" style="width:100%;height:400px;border:1px solid"></div>
            		</div>
        </div>

				<br>
				<div class="row">
					<div class="col-md-10">
						<button class="btn btn-lg btn-primary" style="width:100%">Submit</button>
					</div>
				</div>
			</form>

		<?php else: ?>

			<?php 
			if (isset($_POST['in'])) {
				$cipherz = md5(implode(" # ",$_POST['in']['tanggalPelaksanaan']));
				$chiper = md5($_POST['in']['duration']);
				$myID = substr($_POST['in']['namaKelas'], 0,8).$_POST['in']['lokasiKelas'].substr($cipherz, 0,8).substr($chiper,0,8);
				$input = array(
					'id' => $myID,
					'nama_kelas'=>$_POST['in']['namaKelas'],
					'lokasi'=> $_POST['in']['lokasiKelas'],
					'investasi'=>$_POST['in']['investasi'],
					'hari'=>$_POST['in']['hariPelaksanaan'],
					'tanggal'=>implode(" # ",$_POST['in']['tanggalPelaksanaan']),
					'jam'=>$_POST['in']['jamAwal'].' - '. $_POST['in']['jamAkhir'],
					'durasi'=> $_POST['in']['duration']
				);

				$input = (object) $input;

				

				echo $kursus->inputJadwal($input);
				//echo 'id: '.$input->id."<br>";
				//echo 'id Kelas: '.$input->nama_kelas."<br>";
				//echo 'Lokasi Kelas: '.$input->lokasi."<br>";
				//echo 'Investasi: '.$input->investasi."<br>";
				//echo 'Hari Pelaksanaan: '.$input->hari."<br>";
				//echo 'Tanggal Pelaksanaan: '.implode(" # ",$_POST['in']['tanggalPelaksanaan'])."<br>";
				//echo 'Jam : '.$input->jam."<br>";
				//echo 'Jam Akhir: '.$input->id."<br>";
				//echo 'Durasi: '.$input->durasi."<br>";
			}

			?>
		


		<!-- start form for submitting schedules -->
		<form action="" method="post">
			<div class="row">
				<div class="col-md-10">
					<div class="form-group">
						<label><a href=<?php echo "'".basename(__FILE__, '.php')."?app=2'"; ?>>Nama Kelas</a></label>
						<select class="form-control" name="in[namaKelas]">
							<option value="" selected>Pilih nama kelas yang terdaftar di sistem kami</option>
							<?php 
							for ($i=0; $i < count($nama_kursus) ; $i++) { 
								echo "<option value='".$nama_kursus[$i]->id."'>";
								echo $nama_kursus[$i]->nama_kursus;
								echo "</option>";
							}
							?>
						</select>
					</div>
				</div>
			</div>

				<div class="row">
					<div class="col-md-10">
					<div class="form-group">
						<label><a href=<?php echo "'".basename(__FILE__, '.php')."?app=3'"; ?>>Lokasi Kursus</a></label>
						<select class="form-control" name="in[lokasiKelas]">
							<option selected>Pilih lokasi tempat kursus</option>
							<?php 
							for ($i=0; $i < count($tempat_kursus) ; $i++){ 
								echo "<option value='".$tempat_kursus[$i]->id."'>";
								echo $tempat_kursus[$i]->lokasi_kursus;
								echo "</option>";
							}
							?>
						</select>
					</div>
					</div>
				</div>	

			<div class="row">
				<div class="col-md-10">
					<div class="form-group">
						<label>Investasi</label>
						<input type="number" name="in[investasi]" class="form-control" id="investasi" placeholder="Biaya Kursus">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-10">
					<div class="form-group">
						<label>Hari pelaksanaan</label>
						<input type="text" name="in[hariPelaksanaan]" class="form-control" placeholder="Misal: Hari Senin, Rabu dan Jumat">
					</div>
				</div>
			</div>

			<div class="row" id='jajal'>
				<div class="col-md-10">
				<label for="tgl-pelaksanaan">Rencana tanggal kursus</label>
					<div class="form-inline" >
						<input type="text" id="tgl-pelaksanaan" name='in[tanggalPelaksanaan][]' class="form-control tglPelaksanaan" placeholder="klik disini untuk memilih tanggal"/>   
						<span class='btn btn-sm btn-primary' id='tambahTombol'>Tambah Tanggal</span>
					</div>
				</div>
            </div>
                        <br>
			<div class="row">
				<div class="col-md-10">
					<label>Jam Pelaksanaan</label>
					<div class="form-inline">
						<input type="text" name="in[jamAwal]" class="form-control" placeholder="Jam masuk kursus"> -
						<input type="text" name="in[jamAkhir]" class="form-control" placeholder="jam keluar kursus">
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-10">
					<label>Durasi Kelas dalam sebulan</label>
					<input type="text" name="in[duration]" class="form-control" placeholder="e.g: 3 hari, 2 minggu atau 6 minggu">
				</div>
			</div>

			<br>
			<div class="row">
				<div class="col-md-10">
					<button class="btn btn-lg btn-primary" style="width:100%">Submit</button>
				</div>
			</div>

		
		</form>
		<!-- 
		end form for submitting schedules, add more row for other services 
		-->
		<?php endif ?>


		
	</div>

	    <!-- Modal -->
  <div class='modal fade' id='modal-konfirmasi' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pesan Konfirmasi</h3>

        </div>
        <div class='modal-body'>
        <div style='padding:15px' class='row'>
          
          <div class="col-md-12">
            
            		<div class="row" id='message-body'>
            			Anda Yakin Ingin Menghapus Data <span id='judul-delete' style="font-weight: bold;"></span> ?
            		</div>           		


          </div>
        </div>

        </div>
        <div class='modal-footer'>
        	<div class="row">
        		        <form method='post' action=<?php echo "'".$_SERVER['SCRIPT_NAME']."'"; ?>>
        					<input type='text' name='delete-id' id='konfirm-id' style='display:none' />
        		<div class="col-md-6" style="text-align:left">
							<button class='btn btn-danger'>Ya</button>
						</form>
						<button class='btn btn-info' data-dismiss='modal'>Tidak</button>        			
        		</div>

        		<div class="col-md-6">
        			<div class='form-inline'>
						<span>FAC-Institute 2016</span>
        			</div>
        			
        		</div>
        	</div>
          
        </div>
      </div>

    </div>
  </div>
  <!-- end modal -->  
<script src=<?php echo "'".getBaseUrl()."js/jquery-1.11.1.js'"; ?>></script> 
<script type="text/javascript" src=<?php echo "'".getBaseUrl()."css/datepicker/js/bootstrap-datepicker.js'"; ?>></script>
<script type="text/javascript">

      <?php if (isset($_GET['app'])&&$_GET['app']=='3'): ?>
      	
		$(document).ready(function() {

		  $(window).keydown(function(event){
		    if(event.keyCode == 13) {
		      event.preventDefault();
		      return false;
		    }
		  });


		});
		<?php else: ?>


		$(document).ready(function() {
			$('#tgl-pelaksanaan').datepicker({
				format: "yyyy-mm-dd"
			});		 
		});
			

      <?php endif ?>




	function coba(aidi,myTitle){
		//alert(id);
		document.getElementById("judul-delete").innerHTML=myTitle;
		document.getElementById('konfirm-id').value=aidi;
	}

	$('#tambahTombol').click(function(){
    var nilai = 0;
    var cobayah = $('.tglPelaksanaan').length;
    var iseng = "tambahan" + cobayah;
    if ($('#tambahan').length){

        if (cobayah==2) {
            $('#tambahan').after("<div class='row' id='"+iseng+"'><div class='col-md-10'><div class='form-inline'><input name='in[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"'+iseng+'"'+")'></span></div></div></div>");
        }else{
            var okeh = cobayah-1;
            var iseng = "tambahan" + okeh;
            $("#"+iseng).after("<div class='row' id='tambahan"+cobayah+"'><div class='col-md-10'><div class='form-inline'><input name='in[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"tambahan'+cobayah+'"'+")'></span></div></div></div>");
        };
            nilai +=1;
         } else{
        	$('#jajal').after("<div class='row' id='tambahan'><div class='col-md-10'><div class='form-inline'><input name='in[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"'+"tambahan"+'"'+")'></span></div></div></div>");
    	};

	    $('.tglPelaksanaan').datepicker({
	        format: 'yyyy-mm-dd'
	    });
	});

	function removeElement(id) {
	    $('#'+id).remove();
	}

</script>
<script type="text/javascript" src=<?php echo "'".getBaseUrl()."js/laporan.js'"; ?>></script>
</body>

</html>