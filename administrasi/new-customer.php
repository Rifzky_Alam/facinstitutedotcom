<?php 
session_start();
include_once 'session/session-class.php';
include_once '../baseurl.php';
include_once '../model/Perusahaan.php';
include_once '../model/Customer.php';
include_once '../model/Transaksi.php';
// include_once '../model/page.php';
include_once '../model/external.php';
include_once '../model/Perusahaan.php';
include_once '../model/Customer.php';
$session = new Sessionz();
$session->AdminMarketing();


$customer = new Customer();
$transaksi = new Transaksi();
$perusahaan = new Perusahaan();
// $file = new File();
if (isset($_GET['p'])&&!empty($_GET['p'])) {

    //post data
    if (isset($_POST['in'])) {
        
        $rawID=substr(md5($_POST['in']['nama']), 0, 16);
        $rawID2 = substr($_GET['p'], 0, 16);
        $id = $rawID.$rawID2;
        $customer->setID($id);
        $customer->setPerusahaan($_GET['p']);
        $customer->setNama($_POST['in']['nama']);
        $customer->setGender($_POST['in']['gender']);
        $customer->setEmail($_POST['in']['email']);
        $customer->setTelepon($_POST['in']['telepon']);
        $customer->setJabatan($_POST['in']['jabatan']);
        $customer->setJenispengguna($_POST['in']['jp']);
        $customer->setMarketingID($_POST['in']['mi']);
    
        if ($customer->InputNewCustomer()) {
            echo "<script>alert('Data Customer berhasil disimpan!');</script>";
            echo "<script>location.replace('new-perusahaan-info?p=".$_GET['p']."&c=".$id."');</script>";
        } else {
            echo "<script>alert('Data Customer gagal disimpan!');</script>";
        }
    
    }
    // end post





    $data = array(
        'base_url' => getBaseUrl(), //base data
        'url'=> 'new-customer',
        'judul' => 'Tambah customer/FAC Institute',
        'username'=>$_SESSION['admin']['nama'],
        'page' => 'tambah agenda', // end base data
        'nama_usaha' => $perusahaan->selectNamaUsahaByID($_GET['p']),
        'petugas' => $transaksi->FetchAllMarketing()
    );

    $data = (object) $data;
    include_once 'view/transaksi/tambah-customer.php';

}elseif (isset($_GET['edt'])&&!empty($_GET['edt'])) {
    $customer->setID($_GET['edt']);
    $datacust=$customer->FetchSelectedID();

    if (isset($_POST['ubh'])) {
        $customer->setPerusahaan($datacust->id_perusahaan);
        $customer->setNama($_POST['ubh']['nama']);
        $customer->setGender($_POST['ubh']['gender']);
        $customer->setEmail($_POST['ubh']['email']);
        $customer->setTelepon($_POST['ubh']['telepon']);
        $customer->setJabatan($_POST['ubh']['jabatan']);
        $customer->setJenispengguna($_POST['ubh']['jp']);
        $customer->setMarketingID($_POST['ubh']['mi']);
        
        
        if ($customer->EditCustomer()) {
            echo "<script>alert('Data Customer berhasil diubah!');</script>";
            echo "<script>location.replace('new-perusahaan-info?c=".$_GET['edt']."');</script>";
        } else {
            echo "<script>alert('Data Customer gagal disimpan!');</script>";
        }
        
        


    }

    $data = array(
        'base_url' => getBaseUrl(), //base data
        'url'=> 'new-customer',
        'judul' => 'Edit customer/FAC Institute',
        'username'=>$_SESSION['admin']['nama'],
        'page' => 'tambah_customer', // end base data
        'nama_usaha'=>$perusahaan->selectNamaUsahaByID($datacust->id_perusahaan),
        'nama_cust' => $datacust->nama_cust,
        'email_cust' => $datacust->email_cust,
        'gender_cust' => $datacust->gender_cust,
        'telepon'=>$datacust->telp_cust,
        'jabatan'=>$datacust->jabatan_cust,
        'jenispengguna'=>$datacust->jenis_pengguna_cust,
        'petugas'=> $transaksi->FetchAllMarketing(),
        'marketing'=>$datacust->marketing_id
    );    
    $data = (object) $data;
    include_once 'view/transaksi/edit-customer.php';

}elseif (isset($_GET['ec'])&&!empty($_GET['ec'])) {
    $customer->setID($_GET['ec']);
    $datacust=$customer->FetchSelectedID();

    if (isset($_POST['in'])) {
        
        $customer->setPerusahaan($_POST['in']['namausaha']);

        if ($customer->EditPerusahaanCustomer()) {
            echo "<script>alert('Data Perusahaan Berhasil diubah!');</script>";
            echo "<script>location.replace('data-customer');</script>";
        } else {
            echo "<script>alert('Data Perusahaan Gagal diubah!');</script>";
        }
        

    }

    $data = array(
        'base_url' => getBaseUrl(), //base data
        'url'=> basename(__FILE__, '.php'),
        'judul' => 'Edit Data Customer/FAC Institute',
        'username'=>$_SESSION['admin']['nama'],
        'page' => 'edit-customer',
        'subtitle'=> 'Edit Perusahaan Customer', // end base data
        'namacustomer'=> $datacust->nama_cust
    );

    $data = (object) $data;
    include_once 'view/customer/view-editusaha-customer.php';
} else{
    echo "<script>alert('harap memilih perusahaan terlebih dahulu!');</script>";
    echo "<script>location.replace('data-customer');</script>";
}




 ?>
<?php 


?>
