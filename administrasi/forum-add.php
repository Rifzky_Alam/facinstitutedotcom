<?php 
session_start();
if (!isset($_SESSION['admin'])||($_SESSION['admin']['tipe']!='admin'&&$_SESSION['admin']['tipe']!='staff'&&$_SESSION['admin']['tipe']!='marketing')) {
    header('Location:../login/index.php');
}
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
?>

<!DOCTYPE html>
<html>

<?php 
//echo $tipeAdmin=$_SESSION['admin']['tipe'];
$judul='Forum FAC';
$page = 'home';
include_once 'header.php';
include_once '../model/Forum.php';
include_once '../model/Pendaftar.php';
include_once '../model/Petugas.php';
include_once '../model/Mail.php';

// include_once '../model/page.php';

// $myPage = new Page();
$forum = new Forum();
$petugas = new ControlPetugas();
$mail = new FACMail();

?>

<body>

<?php 

include_once 'sidebar.php';

include_once 'top-nav.php';

 ?>


<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3>Add New Discussion</h3>
	</div>

    <?php 
    if (isset($_POST['iq'])) {
        $attr = array(
            'id' => md5($_POST['iq']['title'].'-'.$_SESSION['admin']['username']),
            'judul' => $_POST['iq']['title'],
            'question' => $_POST['iq']['question'],
            'answer' => '',
            'tag' => $_POST['iq']['tag'],
            'kategori' => $_POST['iq']['kategori'],
            'timestamp' => '',
            'vote' => '',
            'attachments'=>'',
            'petugas'=> $_SESSION['admin']['username']
        );
        $forum->fillAttr($attr);
        
        if ($forum->insertNewRow()) {
            $petugas->setTipe($_POST['iq']['kategori']);
            $emails = array();

            foreach ($petugas->getEmailPetugas() as $value){
                array_push($emails, $value->email);
            }

            $myMailData = array(
                'to' => 'rifzky.mail@gmail.com',
                'nama' => '',
                'cc'=>$emails,
                'subject' => 'FAC - New Forum Discussion', 
                'content'=> 'New Discussion about on FAC Forum titled "'.$_POST['iq']['title'].'" has been submitted!' 
            );
            
            $myMailData = (object) $myMailData;
            if ($mail->sendMailWithCC($myMailData)) {
                echo "<script>alert('Data Berhasil disimpan!');</script>";
                echo "<script>location.replace('forum-table');</script>";
            } else {
                echo "<script>alert('Email tidak terkirim!');</script>";    
            }
            
            
        } else {
            echo "<script>alert('Terjadi kesalahan, harap kontak administrator!');</script>";

        }
        
    }

    ?>

    
    <form action="" method="post">
    <div class="row">
        <div class="col-md-10">
            <div class="form-group">
                <label>Title</label>
                <input type="text" name="iq[title]" class="form-control" placeholder="judul pertanyaan anda" required>
            </div>
            <div class="form-group">
                <label>Question</label>
                <textarea name="iq[question]" id="cmsForum" class="form-control" placeholder="deskripsi dari pertanyaan anda" required></textarea>
            </div>
            <div class="form-group">
                <label>Tag</label>
                <input type="text" name="iq[tag]" class="form-control" placeholder="pisahkan dengan koma" required>
            </div>
            <?php 
            $kategori=$forum->fetchAllCategories();
            ?>
            <div class="form-group">
                <label>Categories</label>
                <select class="form-control" name="iq[kategori]" required>
                    <option value="">--Pilih Kategori--</option>
                    <?php foreach ($kategori as $key) { ?>
                        <option value="<?php echo $key->id_kategori ?>"><?php echo $key->nama_kategori ?></option>
                    <?php } ?>
                </select>
            </div>
            <button class="btn btn-lg btn-primary" style="width:100%">Submit</button>
        </div>    
    </div>
    </form>
</div>
    <script type="text/javascript" src="<?php echo getBaseUrl() ?>administrasi/ckeditor/ckeditor.js"></script>

    <script type="text/javascript">
        CKEDITOR.replace('cmsForum',{
            extraPlugins: 'imageuploader'
        });
    </script>
    <!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/58be3a2b93cfd3557204996c/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
    </body>
</html> 