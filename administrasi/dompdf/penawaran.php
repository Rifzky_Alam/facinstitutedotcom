<!DOCTYPE html>
<html>
<head>
	<title>Penawaran FAC</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<style>
    	@page { margin: 180px 20px; }
    	#header { position: fixed; left: 0px; top: -180px; right: 0px; height: 90px; background-color: orange; text-align: center; }
    	#footer { position: fixed; left: 0px; bottom: -180px; right: 0px; height: 90px; background-color: orange; }
    	#footer .page:after { content: counter(page, upper-roman); }
  	</style>
</head>
<body>
	<div class="container" style="padding-bottom: 40px">

		<div class="row">
			<div class="col-md-12">
				<section class="col-md-8">
					<p>
					Kepada Yth,<br>
					<strong>PT Alam Sejahtera</strong><br>
					</p>
					<table>
						<tbody>
							<tr>
								<td style="width:50px;">Nama</td>
								<td>:</td>
								<td style="padding-left:10px;">Rifzky Alam</td>
							</tr>
							<tr>
								<td>Email</td>
								<td>:</td>
								<td style="padding-left:10px;">rifzky.mail@gmail.com</td>
							</tr>
						</tbody>
					</table>
				</section>
				<section class="col-md-4">
					<p>Jakarta, 17 November 2016</p>
				</section>		
			</div>
		</div>

		
		<div class="row">
			<div class="col-md-12">
				<center>
					<h2>Form Onsite Implementasi Accurate & RENE</h2>
				</center>
			</div>
		</div>
		<br>
		<!--start table-->
		<div class="row">
			<div class="col-md-12">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th style="text-align:center;">Nama Produk</th>
							<th style="text-align:center;">Qty</th>
							<th style="text-align:center;">Harga</th>
							<th style="text-align:center;">Jumlah</th>
						</tr>
						<tbody>
							<!--Start Standard-->
							<tr>
								<td colspan="4" style="text-align:center;">
									<h4>STANDARD, DELUXE EDITION & (RENE STANDARD)</h4>
								</td>
							</tr>
							<tr>
								<td>
								<label>Onsite Standard (OS)</label><br>
								(1 Hari implementasi, Max 7 Jam, Oleh 1 Orang)
								</td>
								<td style="text-align:center;vertical-align:middle;">.....Hari</td>
								<td style="text-align:center;vertical-align:middle;">Rp 1.000.000.-</td>
								<td></td>
							</tr>
							<tr>
								<td>
								<label>Paket Standard (PS)</label><br>
								(5 Hari implementasi, Max 7 Jam, Oleh 1 Orang)
								</td>
								<td style="text-align:center;vertical-align:middle;">....x 5 Hari</td>
								<td style="text-align:center;vertical-align:middle;">Rp 4.500.000.-</td>
								<td></td>
							</tr>
							<!--End Standard-->
							<!--Start Enterprise-->
							<tr>
								<td colspan="4" style="text-align:center;">
									<h4>ENTERPRISE (EXPERT)</h4>
								</td>
							</tr>
							<tr>
								<td>
								<label>Onsite Expert (OE)</label><br>
								(1 Hari implementasi, Max 7 Jam, Oleh 1 Orang)
								</td>
								<td style="text-align:center;vertical-align:middle;">.....Hari</td>
								<td style="text-align:center;vertical-align:middle;">Rp 1.000.000.-</td>
								<td></td>
							</tr>
							<tr>
								<td>
								<label>Paket Expert 1 (PE 1)</label><br>
								(10 Hari implementasi, Max 7 Jam, Oleh 1 Orang)
								</td>
								<td style="text-align:center;vertical-align:middle;">....x 10 Hari</td>
								<td style="text-align:center;vertical-align:middle;">Rp 9.500.000.-</td>
								<td></td>
							</tr>
							<tr>
								<td>
								<label>Paket Expert 2 (PE 2)</label><br>
								(20 Hari implementasi, Max 7 Jam, Oleh 1 Orang)
								</td>
								<td style="text-align:center;vertical-align:middle;">....x 10 Hari</td>
								<td style="text-align:center;vertical-align:middle;">Rp 18.000.000.-</td>
								<td></td>
							</tr>
							<!--End Enterprise-->
							<!--Start Accurate Online-->
							<tr>
								<td colspan="4" style="text-align:center;">
									<h4>ACCURATE ONLINE (AO)</h4>
								</td>
							</tr>
							<tr>
								<td>
								<label>Accurate Online 1 (AOL 1)</label><br>
								(1 Hari implementasi, Max 7 Jam, Oleh 1 Orang)
								</td>
								<td style="text-align:center;vertical-align:middle;">.....Hari</td>
								<td style="text-align:center;vertical-align:middle;">Rp 1.250.000.-</td>
								<td></td>
							</tr>
							<tr>
								<td>
								<label>Accurate Online 2 (AOL 2)</label><br>
								(5 Hari implementasi, Max 7 Jam, Oleh 1 Orang)
								</td>
								<td style="text-align:center;vertical-align:middle;">....x 5 Hari</td>
								<td style="text-align:center;vertical-align:middle;">Rp 6.000.000.-</td>
								<td></td>
							</tr>
							<tr>
								<td colspan="2"></td>
								<td><strong>TOTAL</strong></td>
								<td></td>
							</tr>
							<!--End Accurate Online-->
						</tbody>
					</thead>
				</table>
			</div>
		</div>
		<!--end table-->

		<div class="row">
			<div class="col-md-12">
				<ul>
					<li style="color:blue">
						Harga diatas adalah tarif nett yang diterima oleh kami (tidak dipotong PPh). Kami bukan PKP, tidak ada faktur pajak juga tidak ada NPWP untuk bukti potong PPh 23.
					</li>
					<li style="color:blue">
						Harga belum termasuk biaya transportasi.
					</li>
				</ul>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<h3>Biodata Perusahaan</h3>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<table border="0" style="width: 100%">
					<thead></thead>
					<tbody>
					<tr>
						<td style="width:150px">Nama Perusahaan</td>
						<td style="width:5px">:</td>
						<td></td>
					</tr>
					<tr style="height:10px"><td colspan="3"></td></tr>
					<tr>
						<td style="width:150px;line-height:20px;">Alamat Perusahaan (Invoice)</td>
						<td>:</td>
						<td></td>
					</tr>
					<tr style="height:25px"><td colspan="3"></td></tr>
					<tr>
						<td style="width:150px;line-height:20px;">Alamat Pelaksanaan Training</td>
						<td>:</td>
						<td></td>
					</tr>
					<tr style="height:25px"><td colspan="3"></td></tr>
					<tr>
						<td style="width:150px">Telepon & Fax</td>
						<td>:</td>
						<td></td>
					</tr>
					<tr style="height:10px"><td colspan="3"></td></tr>
					<tr>
						<td style="width:150px">Email</td>
						<td>:</td>
						<td></td>
					</tr>
					<tr style="height:10px"><td colspan="3"></td></tr>
					<tr>
						<td style="width:150px">Contact Person/Hp</td>
						<td>:</td>
						<td></td>
					</tr>
					<tr style="height:10px"><td colspan="3"></td></tr>
					<tr>
						<td style="width:150px">Jabatan</td>
						<td>:</td>
						<td></td>
					</tr>
					<tr style="height:10px"><td colspan="3"></td></tr>
					<tr>
						<td style="width:150px">Jenis Usaha</td>
						<td>:</td>
						<td></td>
					</tr>
					<tr style="height:10px"><td colspan="3"></td></tr>
					<tr>
						<td style="width:150px">Tanggal Rencana Implementasi</td>
						<td>:</td>
						<td></td>
					</tr>
					<tr style="height:10px"><td colspan="3"></td></tr>
					<tr>
						<td style="width:150px">Jenis Accurate</td>
						<td>:</td>
						<td>
						<label style="padding-left:10px"><input type="radio"> 4</label>
						<label style="padding-left:10px"><input type="radio"> 5</label>
						<span>*ceklis</span>
						</td>
					</tr>
					<tr style="height:10px"><td colspan="3"></td></tr>
					<tr>
						<td style="width:150px">Varian</td>
						<td>:</td>
						<td>
							<label style="padding-left:10px"><input type="checkbox"> Standard</label>
							<label style="padding-left:10px"><input type="checkbox"> Deluxe</label>
							<label style="padding-left:10px"><input type="checkbox"> Enterprise</label>
							<label style="padding-left:10px"><input type="checkbox"> Rene</label>
						</td>
					</tr>
					<tr style="height:10px"><td colspan="3"></td></tr>
					<tr>
						<td style="width:150px">Agenda Kegiatan</td>
						<td>:</td>
						<td style="padding-left:10px">
							<div class="checkbox"><label><input type="checkbox">Becanda</label></div>
							<div class="checkbox"><label><input type="checkbox">Maen-maen</label></div>
							<div class="checkbox"><label><input type="checkbox">Ngelamun</label></div>
						</td>
					</tr>
					</tbody>
				</table>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-12">
				<section>
				<label style="color:red">Note</label>
				<p>
				Semua bagian dan field pada form ini wajib di isi, guna membantu team implementator. Form yang tidak lengkap tidak akan diproses. Form ini wajib dibubuhkan tanda tangan dan stempel perusahaan sebagai tanda persetujuan pemesanan, kemudian dikirimkan by email ke training@fac-institute.com sebagai tanda pemesanan.
				</p>
				</section>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<h3>Syarat dan Kententuan Implementasi</h3>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<ol>
					<li>
						Mengisi Form Onsite Implementasi dan mengirimkan kembali form tersebut via email training@fac-institute.com
					</li>
					<li>
						Pemesanan implementasi paling lambat 3 hari sebelum tanggal rencana pelaksanaan.
					</li>
					<li>
						Tanggal rencana pelaksanaan implementasi yang diisi pada form implementasi adalah jadwal sementara. Kepastian jadwal akan dikonfirmasi setelah disepakati oleh Kedua Pihak.
					</li>
					<li>
						Setelah mendapat konfirmasi kepastian jadwal implementasi dari FAC Institute dan jadwal tersebut telah disepakati antara FAC Institute dengan pemesan implementasi, maka FAC Institute akan membuatkan Sales Invoice.
					</li>
					<li>
						Pembayaran onsite dapat ditransfer ke rekening dibawah ini paling lambat dua (2) hari sebelum pelaksanaan Onsite.
						<ul>
							<li><strong>Bank BCA KCU Bekasi No. Acc. 0663162851 a/n : Fajar Shodiq</strong></li>
							<li><strong>Bank Mandiri KCP Bekasi Ahmad Yani No. Acc. 1670001377901 a/n : Fajar Shodiq</strong></li>
						</ul>
					</li>
					<li>
						Kelalaian pembayaran yang mengakibatkan pembatalan onsite, sepenuhnya menjadi tanggungjawab pemesan.
					</li>
					<li>Kelalaian pembayaran yang mengakibatkan pembatalan onsite, sepenuhnya menjadi tanggungjawab pemesan.</li>
					<li>
						Waktu pelaksanaan implementasi pukul 09:00 – 16:00. <br>
						Jika Implementator kami terlambat, maka Implementator wajib mengganti waktu keterlambatan hanya di hari yang sama bukan di hari lain.
					</li>
					<li>
						Kelebihan jam training akan dikenakan tarif over time Rp. 75.000 per jam (tujuh puluh lima ribu rupiah). Jika lembur, mohon di print dan diisi form lembur terlebih dahulu yang telah kami lampirkan.
					</li>
					<li>
						Pihak pemesan onsite (customer) wajib menyediakan makan siang bagi implementator.
					</li>
					<li>
						Hari kerja implementator kami adalah Senin s/d Jumat. Diluar jam kerja tersebut harus persetujuan dengan FAC Institute dan dikenakan charge sebesar Rp 200.000.
					</li>
					<li>
						Perusahaan wajib menyiapkan data saldo awal untuk membantu jalannya proses implementasi dalam format excel,  seperti:
						<ul>
							<li>Data Pelanggan/customer dan saldo piutang.</li>
							<li>Data Pemasok/Vendor dan saldo hutang.</li>
							<li>Data Akun-akun/COA dan saldonya.</li>
							<li>Data barang/jasa dan saldonya.</li>
							<li>Data Fixed Asset dan saldonya.</li>
							<li>Data Bil Of Material (bagi perusahaan manufaktur).</li>
						</ul>
					</li>
					<li>
						Jika data belum siap selama implementasi dan waktu implementasi ini digunakan untuk membantu menyelesaikan data- data yang diperlukan, maka waktu yang digunakan tersebut tidak bisa digantikan.
					</li>
					<li>
						Untuk Paket Standard dan Expert waktu pelaksanaannya harus berurutan sesuai jumlah hari dan tidak boleh dipecah-pecah. Apabila waktunya ingin terpisah maka dikenakan biaya implementasi harian.
					</li>
					<li>
						Pembatalan atau perubahan jadwal implementasi oleh Customer harus disampaikan ke FAC paling lambat 2 hari sebelum waktu pelaksanaan implementasi, apabila dilakukan 1 (satu) hari sebelumnya atau di hari pelaksanaan maka dikenakan denda Rp.250.000/hari. Selanjutnya waktu pelaksanaan akan direschedule kembali.
					</li>
					<li>
						Jadwal implementasi yang sudah disepakati dan sedang dalam pelaksanaan tidak dapat diubah.
					</li>
					<li>
						Implementasi di dalam kota, dikenakan biaya transportasi, yaitu :
						<ul>
							<li>DKI Jakarta,Depok, Bogor, Cikarang, Tangerang dan Bekasi Rp 150.000/hari.</li>
						</ul>
					</li>
					<li>
						Implementasi diluar kota, biaya akomodasi, makan dan perjalanan ditanggung oleh Customer. Meliputi :
						<ul>
							<li>Tiket pergi dan pulang.</li>
							<li>Penginapan/Mess/hotel.</li>
							<li>
							Transportasi selama perjalanan dari tempat domisili implementator sampai tempat pelaksanaan implementasi dan sebaliknya.
							</li>
							<li>
								Biaya makan
							</li>
						</ul>
					</li>
					<li>
						Tugas implementator mencakup:
						<ul>
							<li>Membantu menyiapkan Setup Awal Database.</li>
							<li>
								Deliver "How To Use Accurate", yaitu :
								<ul>
									<li>
										Cara Input Transaksi modul penjualan, pembelian, persediaan, general ledger, aktiva tetap, RMA.
									</li>
									<li>
										Cara Input Transaksi modul project (khusus Deluxe Edition - Contractor).
									</li>
									<li>
										Cara Input Transaksi modul manufaktur (khusus Enterprise Edition).
									</li>
									<li>
										Membantu mengajarkan cara pembuatan template (bukan membuatkan template).
									</li>
								</ul>
							</li>
							<li>
								Jumlah hari implementasi untuk melakukan tugas implementasi diatas tergantung kemampuan user yang belajar.
							</li>
						</ul>
					</li>
					<li>
						Pertanyaan/kasus yang diajukan customer diluar lingkup tugas Implementor yang disebutkan dalam point 18 tidak menjadi kewajiban dari implementor untuk menjawab.
					</li>
					<li>
						Mengenai setting hardware, setting internet ataupun setting jaringan ini bukan kewajiban dan tugas dari Implementator.
					</li>
					<li>
						Implementator tidak menyediakan laptop/netbook/infokus ketika implementasi, untuk implementasi semua kebutuhan peralatan disediakan oleh pihak customer/perusahaan pemesan.
					</li>
					<li>
						Untuk setting template bukan merupakan tugas Implementator ataupun dibawa pulang untuk pengerjaannya.
					</li>
					<li>
						Jasa pembuatan design template adalah layanan diluar implementasi ACCURATE sehingga jika Customer meminta dibuatkan mohon terlebih dulu mengirimkan contoh formnya via email training@fac-institute.com agar pihak FAC Institute yang memutuskan bisa atau tidaknya design tersebut dibuat ke dalam ACCURATE. Setting atau desain template diluar jam training akan dikenakan biaya Rp 250.000 per masing-masing desain.
					</li>
					<li>
						Setelah selesai implementasi berdasarkan hitungan hari yang telah dibayar, klien tidak dapat meminta layanan tambahan (extra) implementasi tanpa dengan biaya.
					</li>
					<li>
						Klaim Onsite yang belum dilaksanakan oleh pihak FAC Institute, selambat-lambatnya dilaksanakan dalam waktu 1 bulan setelah tanggal terjadinya reschedule. Apabila melebihi waktu yang telah ditentukan maka klaim tersebut akan dianggap HANGUS.
					</li>
				</ol>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<p>
					Saya menyatakan telah membaca, mengerti dan bersedia mematuhi seluruh syarat dan ketentuan implementasi yang berlaku.
				</p>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<table border="0" style="width:100%">
					<tbody>
						<tr>
							<td style="text-align:center;width:50%;">Hormat Kami</td>
							<td style="text-align:center;width:50%;">Pemesan</td>
						</tr>
						<tr style="height:100px"><td colspan="2"></td></tr>
						<tr>
							<td style="text-align:center;"><strong>Fajar Shodiq</strong></td>
							<td style="text-align:center;">______________________________________</td>
						</tr>
						<tr>
							<td style="text-align:center;">Fac Institute</td>
							<td style="text-align:center;">Tanda tangan, Cap dan Nama Jelas</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<br>
		<div class="row" style="font-size:x-small;line-height:5px;text-align:right;">
			
		</div>
		
	</div><!--end container-->
</body>
</html>