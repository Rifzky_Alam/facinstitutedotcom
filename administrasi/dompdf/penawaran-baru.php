<?php 
require_once 'dompdf/autoload.inc.php';
include_once 'Isi.php';
include_once '../../model/Mail.php';
include_once '../../model/Surat.php';
include_once '../../model/Penawaran.php';
include_once '../../model/page.php';
include_once '../../model/MailSystem.php';

$penawaranDB = new MailSystem();
$offer = new Penawaran();
$mail = new FACMail();
$myPage = new Page();
$surat = new Surat();
// reference the Dompdf namespace
use Dompdf\Dompdf;
// instantiate and use the dompdf class
$dompdf = new Dompdf();
$content = new Content();


if (isset($_POST['dataz'])) {
	
	if ($_POST['dataz']['cc']!=''){
		$emails = explode(' ', $_POST['dataz']['cc']);
	}else{
		$emails = '';
	}

$obj = (object) $_POST['dataz'];



if (isset($_GET['attachment'])&&$_GET['attachment']=='true') {
	
	array_push($obj->attachments, 'Penawaran Training FAC Institute.pdf');

	$data = array(
		'to' => $obj->email_kontak,
		'nama'=> '',
		'subject'=> 'Penawaran Training ACCURATE '.$obj->np,
		'content'=>$surat->kirimPenawaran2($obj),
		'attachments'=> $obj->attachments,
		'cc'=>$emails
	);


} else {
	
	$data = array(
		'to' => $obj->email_kontak,
		'nama'=> '',
		'subject'=> 'Penawaran Training ACCURATE '.$obj->np,
		'content'=>$surat->kirimPenawaran2($obj),
		'attachment'=> 'Penawaran Training FAC Institute.pdf',
		'cc'=>$emails
	);

}



//input data into database
$penawaranDB->inputData($obj);

//log file
$myPage->addLogForLogin($obj->petugas,'successfully sent quotation',$obj->ip);

if ($obj->biayatrans=='negosiasi') {
	$obj->biayatrans = $obj->transnego;
}


if ($obj->marketer=='mm') {

	$hargaosbymarketer="1.000.000";
	$myData = array(
		'perusahaan' => $obj->np ,
		'nama' => $obj->cp,
		'email' => $obj->email_kontak,
		'hargaos' => $hargaosbymarketer,
		'transport'=> $obj->biayatrans,
		'petugas' => $obj->petugas
	);
	$myData = (object) $myData;
	$dompdf->loadHtml($content->penawaran($myData));

} else {

	$hargaosbymarketer="750.000";
	$myData = array(
		'perusahaan' => $obj->np ,
		'nama' => $obj->cp,
		'email' => $obj->email_kontak,
		'hargaos' => $hargaosbymarketer,
		'transport'=> $obj->biayatrans,
		'petugas' => $obj->petugas
	);
	$myData = (object) $myData;
	$dompdf->loadHtml($content->penawaran2($myData));
	
}


// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'potrait');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
//$dompdf->stream('my.pdf',array('Attachment'=>0));

// Output the generated PDF into file
if (isset($_GET['attachment'])&&$_GET['attachment']=='true') {

	if(!file_exists('Penawaran Training FAC Institute'.'.pdf')){
		$output = $dompdf->output();
		file_put_contents('Penawaran Training FAC Institute.pdf', $output);	
		$mail->KirimJadwal((object) $data);
		unlink('Penawaran Training FAC Institute'.".pdf");
		exit("<a href='../index'>Successfully sent an email.</a>");
	} else { 
		exit("<a href='../index'>Path Not Writable</a>");
	}



}else{

	if(!file_exists('Penawaran Training FAC Institute'.'.pdf')){
		$output = $dompdf->output();
		file_put_contents('Penawaran Training FAC Institute.pdf', $output);	
		$mail->sendMailAttachments((object) $data);
		unlink('Penawaran Training FAC Institute'.".pdf");
		exit("<a href='../index'>Successfully sent an email.</a>");
	} else { 
		exit("<a href='../index'>Path Not Writable</a>");
	}

}




}
?>

