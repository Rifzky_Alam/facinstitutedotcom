<?php 
session_start();

if (!isset($_SESSION['admin'])&&!$_SESSION['admin']['type']=='admin'){
	header('Location:../index.php');
}
$page='dataPegawai'
?>

<?php 
$page = 'orderNew';
//include_once 'variables.php';

?>

<!DOCTYPE html>

<html>

<head>
	<title>FAC - Jadwal Kursus</title>

<?php include_once 'header.php'; ?>
<link rel="stylesheet" type="text/css" href=<?php echo "'".getBaseUrl()."css/datepicker/css/datepicker.css'"; ?>> 
</head>

<body>
	<?php if (isset($_GET['app'])&&$_GET['app']=='3'): ?>
		<?php include_once 'map-script.php'; ?>
	<?php endif ?>
	
	<?php include_once 'sidebar.php'; ?>
	<?php include_once 'top-nav.php'; ?>

	<?php 
	include_once '../model/Kursus.php';
	$kursus = new Kursus();
	$data_daftar = json_decode($kursus->getDataAllRegister());

	?>
	


	<div class="container" style='margin-bottom:40px;margin-top:40px'>

		<div class='row'>
			<div class='page-header'>
            	<h1>Data Siswa Kursus</h1>
			</div>
		</div>

		<div class="row">
			<div class="col-md-10">
				<div class="form-inline">
					<ul class="pagination">
						<li title="Home-Kursus">
						<a href=<?php echo "'".basename(__FILE__, '.php')."'" ?>>
							<span class="glyphicon glyphicon-home"></span>
						</a>
						</li>
						<li title="Validasi siswa">
						<a href=<?php echo "'".basename(__FILE__, '.php')."?app=2'" ?>>
							<span class="glyphicon glyphicon glyphicon-check"></span>
						</a>
						</li>
					</ul>
				</div>
			</div>	
		</div>

	<?php if (isset($_GET['app'])&&$_GET['app']=='2'): ?>

		<?php 
		if (isset($_POST['r'])) {
			$inp = array('id' => $_POST['r']['kode']);
			$inp = (object) $inp;
			if ($kursus->updateStatus($inp)) {
				echo "<script>alert('Status Berhasil diubah.');</script>";
				echo "<script>location.replace('".basename(__FILE__, '.php')."');</script>";
			}else{

			}
		}

		?>

		<form action="" method="post">
			<div class="row">
				<div class="col-md-10">
					<div class="form-group">
						<label>Kode Registrasi</label>
						<input type="text" name="r[kode]" class="form-control" />
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-10">
				<button class="btn btn-lg btn-primary" style="width:100%">Validasi</button>
				</div>
			</div>
		</form>
			
	<?php elseif (isset($_GET['app'])&&$_GET['app']=='3'): ?>

	<?php else: ?>

		<div class="row">
			<div class="col-md-12">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Nama Siswa</th>
							<th>Tanggal Daftar</th>
							<th>Email</th>
							<th>Telepon</th>
							<th>Nama Kursus</th>
						</tr>
					</thead>
					<tbody>
						<?php for ($i=0; $i < count($data_daftar) ; $i++) { ?>
						<?php if ($data_daftar[$i]->status!='1'): ?>
							<tr class="warning">
							<?php else: ?>
							<tr class="success">
						<?php endif ?>
						
							<td><?php echo $data_daftar[$i]->nama ?></td>
							<td><?php echo $data_daftar[$i]->tanggal_daftar ?></td>
							<td><?php echo $data_daftar[$i]->email ?></td>
							<td><?php echo $data_daftar[$i]->telepon_user ?></td>
							<td><?php echo $data_daftar[$i]->nama_kursus ?></td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>

	<?php endif ?>


		
	</div>

	    <!-- Modal -->
  <div class='modal fade' id='modal-konfirmasi' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pesan Konfirmasi</h3>

        </div>
        <div class='modal-body'>
        <div style='padding:15px' class='row'>
          
          <div class="col-md-12">
            
            		<div class="row" id='message-body'>
            			Anda Yakin Ingin Menghapus Data <span id='judul-delete' style="font-weight: bold;"></span> ?
            		</div>           		


          </div>
        </div>

        </div>
        <div class='modal-footer'>
        	<div class="row">
        		        <form method='post' action=<?php echo "'".$_SERVER['SCRIPT_NAME']."'"; ?>>
        					<input type='text' name='delete-id' id='konfirm-id' style='display:none' />
        		<div class="col-md-6" style="text-align:left">
							<button class='btn btn-danger'>Ya</button>
						</form>
						<button class='btn btn-info' data-dismiss='modal'>Tidak</button>        			
        		</div>

        		<div class="col-md-6">
        			<div class='form-inline'>
						<span>FAC-Institute 2016</span>
        			</div>
        			
        		</div>
        	</div>
          
        </div>
      </div>

    </div>
  </div>
  <!-- end modal -->  
<script src=<?php echo "'".getBaseUrl()."js/jquery-1.11.1.js'"; ?>></script> 
<script type="text/javascript" src=<?php echo "'".getBaseUrl()."css/datepicker/js/bootstrap-datepicker.js'"; ?>></script>
<script type="text/javascript">

      <?php if (isset($_GET['app'])&&$_GET['app']=='3'): ?>
      	
		
		<?php else: ?>


		$(document).ready(function() {
			$('#tgl-pelaksanaan').datepicker({
				format: "yyyy-mm-dd"
			});		 
		});
			

      <?php endif ?>




	function coba(aidi,myTitle){
		//alert(id);
		document.getElementById("judul-delete").innerHTML=myTitle;
		document.getElementById('konfirm-id').value=aidi;
	}

	$('#tambahTombol').click(function(){
    var nilai = 0;
    var cobayah = $('.tglPelaksanaan').length;
    var iseng = "tambahan" + cobayah;
    if ($('#tambahan').length){

        if (cobayah==2) {
            $('#tambahan').after("<div class='row' id='"+iseng+"'><div class='col-md-10'><div class='form-inline'><input name='in[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"'+iseng+'"'+")'></span></div></div></div>");
        }else{
            var okeh = cobayah-1;
            var iseng = "tambahan" + okeh;
            $("#"+iseng).after("<div class='row' id='tambahan"+cobayah+"'><div class='col-md-10'><div class='form-inline'><input name='in[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"tambahan'+cobayah+'"'+")'></span></div></div></div>");
        };
            nilai +=1;
         } else{
        	$('#jajal').after("<div class='row' id='tambahan'><div class='col-md-10'><div class='form-inline'><input name='in[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"'+"tambahan"+'"'+")'></span></div></div></div>");
    	};

	    $('.tglPelaksanaan').datepicker({
	        format: 'yyyy-mm-dd'
	    });
	});

	function removeElement(id) {
	    $('#'+id).remove();
	}

</script>
</body>

</html>