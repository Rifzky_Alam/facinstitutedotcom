<?php 
session_start();
if (!isset($_SESSION['admin'])) {
    header('Location:../login/index.php');
}


function getValue($value){
    if ($value=='') {
        return '-';
    } else {
        return $value;
    }
    
}
?>




<!DOCTYPE html>
<html>

<?php 
//echo $tipeAdmin=$_SESSION['admin']['tipe'];
$judul='Tambah Data Sales - FAC Institute';
$page = 'upload';
include_once 'header.php'; 

?>

<body>



<?php 

include_once 'sidebar.php';

include_once 'top-nav.php';

 ?>

<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3>Permintaan Order Client</h3>
	</div>

    <?php 
    if (isset($_POST['sales'])) {
        include_once '../model/Pendaftar.php';
        $pendaftar = new Pendaftar();
        $aidi = md5($_POST['sales']['nama'].$_POST['sales']['dept']);
        $objek = array(
            'id' => getValue($aidi),
            'nama' => getValue($_POST['sales']['nama']),
            'institusi' => getValue($_POST['sales']['dept']),
            'telepon' => getValue($_POST['sales']['telepon']),
            'email' => getValue($_POST['sales']['email'])
        );
        $objek =  (object) $objek;
        if ($pendaftar->addSales($objek)) {
            echo "<script>alert('Data sales berhasil disimpan!');</script>";
        } else {
            echo "<script>alert('Data sales gagal disimpan!');</script>";
        }
                        
    /*echo "<div class='row'>";
    echo "Nama: " . $objek->nama . "<br>";
    echo "Institusi: " . $objek->institusi . "<br>";
    echo "Telepon: " . $objek->telepon . "<br>";
    echo "Email: " . $objek->email . "<br>";
    echo "</div>";
    */
    }

?>

    <?php if (isset($_GET['ac'])&&$_GET['ac']=='edit'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>

        <?php else: ?>

	<div class="row">
     <form action="" method="post">
         <div class="row">
             <div class="col-md-10">
                 <div class="form-group">
                     <label>Nama</label>
                     <input type="text" class="form-control" name="sales[nama]" required maxlength="40" />
                 </div>
             </div>
         </div>

         <div class="row">
             <div class="col-md-10">
                 <div class="form-group">
                     <label>Institusi/Departemen</label>
                     <input type="text" class="form-control" name="sales[dept]" required  placeholder="Tempat sales bekerja" maxlength="40" />
                 </div>
             </div>
         </div>

         <div class="row">
             <div class="col-md-10">
                 <div class="form-group">
                     <label>Telepon</label>
                     <input type="text" class="form-control" name="sales[telepon]" maxlength="15" />
                 </div>
             </div>
         </div>

         <div class="row">
             <div class="col-md-10">
                 <div class="form-group">
                     <label>Email</label>
                     <input type="email" class="form-control" name="sales[email]" maxlength="125" />
                 </div>
             </div>
         </div>
         
         <div class="row">
             <div class="col-md-10">
                 <button class="btn btn-lg btn-primary" style="width:100%;">Submit</button>
             </div>
         </div>


     </form> 

      
    </div>
    <?php endif ?>

    

</div><!--end container-->
</body>
</html> 