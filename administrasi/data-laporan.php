<?php 
session_start();
if (!isset($_SESSION['admin'])&&$_SESSION['admin']['tipe']=='admin') {
    header('Location:../login/index.php');
}
?>
<!DOCTYPE html>
<html>


<?php 

$page = 'dataLaporan';
include_once 'header.php'; 

?>

<body>



<?php 

include_once 'sidebar.php';

include_once 'top-nav.php';

 ?>


<div class="container" style="margin-top:40px;margin-bottom:40px;">
	<div class="page-header" id="top-logo">
		<h3>Data Laporan Harian</h3>
	</div>
	<?php 
		
		include_once '../model/Laporan.php'; 
        include_once '../model/Petugas.php'; 
        
		$pendaftar = new ControlLaporan();
        $petugas = new ControlPetugas();
        $dataOrang = json_decode($petugas->fetchUsernameAndName2()); 
        if (isset($_GET['nama'],$_GET['tanggalAwal'],$_GET['tanggalAkhir'])) {

            if ($_SESSION['admin']['tipe']=='admin'){
                $username=$_GET['nama'];
                $datas=$pendaftar->searchLaporanAdmin($_GET['tanggalAwal'],$_GET['tanggalAkhir'],$_GET['nama']);
                //print_r($_GET['nama']);
            //$query="SELECT nama, `jns_hari_kerja`, `hari`, `tanggal`, `jam_mulai`, `jam_selesai`, `jumlah_overtime`, `id_klien`, `ket_kegiatan`, `peserta_kegiatan`, `status_kerja`, `peran`, `rekan`, `jns_transportasi`, `biaya_transport`, `ket_transport`, `biaya_parkir`, `biaya_lunch`, `biaya_dinner`, `biaya_kesehatan`, `biaya_allowance`, `biaya_lain`, `ket_biaya_lain`, `waktu_lapor` FROM `laporan_harian`,`users` WHERE  tanggal BETWEEN '2016-06-02' AND '2016-06-10' AND users.username=laporan_harian.username";
            //for ($i=0; $i < count($username) ; $i++) { 
                //if ($i>0) {
                    //$query .=" OR users.username='".$username[$i]."' AND tanggal BETWEEN '".$tanggalAwal."' AND '".$tanggalAkhir."' AND users.username=laporan_harian.username";
                //}else{
                    //$query .= " AND users.username='".$username[0]."'";
                //}
            //}

            }else{
                $datas=$pendaftar->searchLaporan($_GET['tanggalAwal'],$_GET['tanggalAkhir'],$_GET['nama'],$_SESSION['admin']['team']);
            }
            

        }else{

            if ($_SESSION['admin']['tipe']=='admin') {
                $datas = $pendaftar->dataHarian(date('Y-m-d'),date('Y-m-d'),'');
            }else{
                $datas = $pendaftar->dataHarian(date('Y-m-d'),date('Y-m-d'),$_SESSION['admin']['team']);
            }           

        }



		
        
	?>

     
	<div class="row">
		<div class="col-sm-12 col-md-12" style="text-align:right"><a class="" id="cari-data" href="#modal-cari" data-toggle="modal">Cari Data?</a></div>
	</div>

	    <!-- Modal -->
  <div class='modal fade' id='modal-cari' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pencarian Data</h3>

        </div>

    <?php if ($_SESSION['admin']['tipe']=='admin'): ?>
        <div class='modal-body' style='padding-top:0px;overflow:auto;'>
        <div style='padding-left:15px;padding-right:15px;padding-top:15px; height: 300px;' class='row'>
       <?php else: ?>
        <div class='modal-body' style=''>
        <div style='padding:15px; height: 300px;' class='row'>        
    <?php endif ?>
        
          
          <div class="col-md-12">
            
              
                <form action='' method="GET">
                    <div class="row">
                        <div class="form-group">
                            <?php if ($_SESSION['admin']['tipe']=='admin'): ?>
                                 
                            <select name="nama" class="form-control">
                                <option value="">--Nama Pegawai--</option>
                                <?php 

                                if ($_SESSION['admin']['tipe']=='admin') {
                                    $dataSelect = $petugas->fetchUsernameAndName(); 
                                }else{
                                    $dataSelect = $petugas->fetchUsernameAndNameByTeam($_SESSION['admin']['team']); 
                                }
                                
                                $dataPegawai = array();

                                for ($i=0; $i < count($dataSelect) ; $i++) { 
                                    echo "<option value='".$dataSelect[$i]->username."'>".$dataSelect[$i]->nama."</option>";
                                    //$newDataArray = array($dataSelect[$i]->username => $dataSelect[$i]->nama);
                                    array_push($dataPegawai,array($dataSelect[$i]->username,$dataSelect[$i]->nama));
                                }

                                 ?>

                            </select>


                                <?php else: ?>

                            <select name="nama" class="form-control">
                                <option value="">--Nama Pegawai--</option>
                                <?php 

                                if ($_SESSION['admin']['tipe']=='admin') {
                                    $dataSelect = $petugas->fetchUsernameAndName(); 
                                }else{
                                    $dataSelect = $petugas->fetchUsernameAndNameByTeam($_SESSION['admin']['team']); 
                                }
                                
                                $dataPegawai = array();

                                for ($i=0; $i < count($dataSelect) ; $i++) { 
                                    echo "<option value='".$dataSelect[$i]->username."'>".$dataSelect[$i]->nama."</option>";
                                    //$newDataArray = array($dataSelect[$i]->username => $dataSelect[$i]->nama);
                                    array_push($dataPegawai,array($dataSelect[$i]->username,$dataSelect[$i]->nama));
                                }

                                 ?>

                            </select>

                            <?php endif ?>

                        </div>
                    </div>


                    <div class="row">
                        <div class="form-group">
                            <label for="dari-tanggal">Mulai Tanggal</label>
                            <input type="text" name="tanggalAwal" id="tanggal-awal" class="form-control" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label for="dari-tanggal">Sampai Tanggal</label>
                            <input type="text" name="tanggalAkhir" id="tanggal-akhir" class="form-control" />
                        </div>
                    </div>

                    <div class="row">
                        <button class="btn btn-success" style="width:100%">Cari</button>
                    </div>

                </form>
          </div>
             
          

        </div>
          

        </div>
        <div class='modal-footer'>
            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>FAC-Institute 2016</span>
                </div>
                <div class="col-md-6">
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->

	
	<div class="row" style='overflow:auto;'>
		<div class="col-md-12">
			<table id='tabel-laporan' class="table table-bordered">
				<thead>
                    <tr>
                    <?php if ($_SESSION['admin']['tipe']=='admin'): ?>
                        <th style='width:220px'>Time Stamp</th>
                        <th style='width:200px'>Nama</th>
                        <th style='width:120px'>Tanggal</th>
                        <th style='width:100px'>Hari</th>
                        <th style='width:160px'>Jenis Hari Kerja</th>
                        <th style='width:120px'>Jam Masuk</th>
                        <th style='width:120px'>Jam Pulang</th>
                        <th style='width:85px'>Overtime</th>
                        <th style='width:200px'>Klien</th>
                        <th style='width:500px'>Kegiatan</th>
                        <th style='width:500px'>Peserta Kegiatan</th>
                        <th style='width:105px'>Status</th>
                        <th style='width:105px'>Peran</th>
                        <th style='width:105px'>Rekan</th>
                        <th style='width:120px'>Transportasi</th>

                        <th style='width:120px'>Biaya Transportasi</th>
                        <th style='width:120px'>Keterangan Transportasi</th>
                        <th style='width:120px'>Biaya Parkir</th>
                        <th style='width:120px'>Biaya Makan Siang</th>
                        <th style='width:120px'>Biaya Makan Malam</th>
                        <th style='width:120px'>Biaya Kesehatan</th>
                        <th style='width:120px'>Biaya Allowance</th>
                        <th style='width:120px'>Biaya Lain</th>
                        <th style='width:220px'>Keterangan Biaya Lain</th>
                        

                        <?php else: ?>
                    
                        <th style='width:200px'>Nama</th>
                        <th style='width:120px'>Tanggal</th>
                        <th style='width:100px'>Hari</th>
                        <th style='width:160px'>Jenis Hari Kerja</th>
                        <th style='width:120px'>Jam Masuk</th>
                        <th style='width:120px'>Jam Pulang</th>
                        <th style='width:85px'>Overtime</th>
                        <th style='width:200px'>Klien</th>
                        <th style='width:500px'>Kegiatan</th>
                        <th style='width:500px'>Peserta Kegiatan</th>
                        <th style='width:105px'>Status</th>
                        <th style='width:105px'>Peran</th>
                        <th style='width:105px'>Rekan</th>
                        <th style='width:120px'>Transportasi</th>

                    <?php endif ?>

                    


                                                
                    </tr>
				</thead>

				<tbody>
                    <?php 
                    $hasil = json_decode($datas);
                    for ($i=0; $i < count($hasil) ; $i++) { 
                        
                    ?>
                    <tr>

                        <?php if ($_SESSION['admin']['tipe']=='admin'): ?>
                                <td><?php echo $hasil[$i]->waktu_lapor; ?></td>
                                <td><?php echo $hasil[$i]->nama; ?></td>
                                <td><?php echo $hasil[$i]->tanggal; ?></td>
                                <td><?php echo $hasil[$i]->hari; ?></td>
                                <td><?php echo $hasil[$i]->jns_hari_kerja; ?></td>
                                <td><?php echo $hasil[$i]->absen_masuk; ?></td>
                                <td><?php echo $hasil[$i]->absen_pulang; ?></td>
                                <td><?php echo $hasil[$i]->jumlah_overtime; ?></td>
                                <td><?php echo $hasil[$i]->klien; ?></td>
                                <td><?php echo $hasil[$i]->ket_kegiatan; ?></td>
                                <td><?php echo $hasil[$i]->peserta_kegiatan; ?></td>
                                <td><?php echo $hasil[$i]->status_kerja; ?></td>
                                <td><?php echo $hasil[$i]->peran; ?></td>

                                <?php 
                                    $myRekan = explode(" # ",$hasil[$i]->rekan);
                                ?>

                                <td>
                                    <?php 
                                        //$search_array = array('first' => 1, 'second' => 4);
                                        //echo $dataPegawai[0][0];

                                        for ($j=0; $j < count($myRekan); $j++) { 
                                            
                                            for ($k=0; $k < count($dataPegawai); $k++){ 

                                                if ($j==count($myRekan)-1) {
                                                    if ($dataPegawai[$k][0]==$myRekan[$j]){
                                                        echo $dataPegawai[$k][1];
                                                    }
                                                }else{
                                                    if ($dataPegawai[$k][0]==$myRekan[$j]){
                                                        echo $dataPegawai[$k][1].',';
                                                        echo "<br>";
                                                    }

                                                }


                                                
                                            }
                                        }
                                        
                                    ?>
                                </td>
                                

                                <td><?php echo $hasil[$i]->jns_transportasi; ?></td>   
                                <td><?php echo $hasil[$i]->biaya_transport; ?></td>   
                                <td><?php echo $hasil[$i]->ket_transport; ?></td>   
                                <td><?php echo $hasil[$i]->biaya_parkir; ?></td>   
                                <td><?php echo $hasil[$i]->biaya_lunch; ?></td>   
                                <td><?php echo $hasil[$i]->biaya_dinner; ?></td>  
                                <td><?php echo $hasil[$i]->biaya_kesehatan; ?></td>  
                                <td><?php echo $hasil[$i]->biaya_allowance; ?></td>  
                                <td><?php echo $hasil[$i]->biaya_lain; ?></td> 
                                <td><?php echo $hasil[$i]->ket_biaya_lain; ?></td> 
                                <?php @$totalBiayaTransport = $totalBiayaTransport + $hasil[$i]->biaya_transport ?>
                                <?php @$totalBiayaParkir = $totalBiayaParkir + $hasil[$i]->biaya_parkir ?>
                                <?php @$totalBiayaMakanSiang = $totalBiayaMakanSiang + $hasil[$i]->biaya_lunch ?>
                                <?php @$totalBiayaMakanMalam = $totalBiayaMakanMalam + $hasil[$i]->biaya_dinner ?>
                                <?php @$totalBiayaKesehatan = $totalBiayaKesehatan + $hasil[$i]->biaya_kesehatan ?>                    
                                <?php @$totalBiayaAllowance= $totalBiayaAllowance + $hasil[$i]->biaya_allowance ?>
                                <?php @$totalBiayaLain = $totalBiayaLain + $hasil[$i]->biaya_lain ?>
                            <?php else: ?>
                        
                                <td><?php echo $hasil[$i]->nama; ?></td>
                                <td><?php echo $hasil[$i]->tanggal; ?></td>
                                <td><?php echo $hasil[$i]->hari; ?></td>
                                <td><?php echo $hasil[$i]->jns_hari_kerja; ?></td>
                                <td><?php echo $hasil[$i]->absen_masuk; ?></td>
                                <td><?php echo $hasil[$i]->absen_pulang; ?></td>
                                <td><?php echo $hasil[$i]->jumlah_overtime; ?></td>
                                <td><?php echo $hasil[$i]->klien; ?></td>
                                <td><?php echo $hasil[$i]->ket_kegiatan; ?></td>
                                <td><?php echo $hasil[$i]->peserta_kegiatan; ?></td>
                                <td><?php echo $hasil[$i]->status_kerja; ?></td>
                                <td><?php echo $hasil[$i]->peran; ?></td>

                                <?php 
                                    $myRekan = explode(" # ",$hasil[$i]->rekan);
                                ?>

                                <td>
                                    <?php 
                                        //$search_array = array('first' => 1, 'second' => 4);
                                        //echo $dataPegawai[0][0];

                                        for ($j=0; $j < count($myRekan); $j++) { 
                                            
                                            for ($k=0; $k < count($dataOrang); $k++){ 

                                                if ($j==count($myRekan)-1) {
                                                    if ($dataOrang[$k]->username==$myRekan[$j]){
                                                        echo $dataOrang[$k]->nama;
                                                    }
                                                }else{
                                                    if ($dataOrang[$k]->username==$myRekan[$j]){
                                                        echo $dataOrang[$k]->nama.',';
                                                        echo "<br>";
                                                    }

                                                }


                                                
                                            }
                                        }
                                        
                                    ?>
                                </td>
                                
                                <td><?php echo $hasil[$i]->jns_transportasi; ?></td>

                        <?php endif ?>



                    </tr>

                    <?php } ?>
                    <?php if ($_SESSION['admin']['tipe']=='admin'): ?>
                        
                    
                    <tr>
                        <td colspan="15"><strong>TOTAL</strong></td>  
                                <td><?php echo @number_format($totalBiayaTransport); ?></td>   
                                <td><?php //echo $hasil[$i]->ket_transport; ?></td>   
                                <td><?php echo @number_format($totalBiayaParkir); ?></td>   
                                <td><?php echo @number_format($totalBiayaMakanSiang); ?></td>   
                                <td><?php echo @number_format($totalBiayaMakanMalam); ?></td>  
                                <td><?php echo @number_format($totalBiayaKesehatan); ?></td>  
                                <td><?php echo @number_format($totalBiayaAllowance); ?></td>  
                                <td><?php echo @number_format($totalBiayaLain); ?></td> 
                                <td><strong><?php @$totaal = $totalBiayaTransport+$totalBiayaParkir+$totalBiayaMakanSiang+$totalBiayaMakanMalam+$totalBiayaKesehatan+$totalBiayaAllowance+$totalBiayaLain;echo number_format($totaal) ?></strong></td>                     
                    </tr>
                    <?php endif ?>
				</tbody>
			</table>
		</div>
	</div>

    <div class='row'>
        <div class='col-md-12'>

            
        </div>
    </div>
<?php if ($_SESSION['admin']['tipe']=='admin'): ?>
    <?php if (isset($_GET['nama'],$_GET['tanggalAwal'],$_GET['tanggalAkhir'])): ?>
        <div class='row'>
            <div class='col-md-12'>
                <a href=<?php echo "'download-excel.php?data[username]=".$_GET['nama']."&data[tanggalAwal]=".$_GET['tanggalAwal']."&data[tanggalAkhir]=".$_GET['tanggalAkhir']."'"; ?> class='btn btn-primary'>Download Laporan</a>
            </div>
        </div>
    <?php endif ?>
<?php endif ?>

    <div class='row'>
        <div class='col-md-12'>
            

        </div>

    </div>

</div>

<script type="text/javascript" src=<?php echo "'".getBaseUrl()."js/jquery-datepicker/jquery-ui.js'"; ?>></script>
<script type="text/javascript" src=<?php echo "'".getBaseUrl()."js/daftar-absen.js'"; ?>></script>
     
</body>
</html> 


