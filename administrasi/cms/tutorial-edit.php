<?php 
session_start();

if (!isset($_SESSION['admin'])&&!$_SESSION['admin']['type']=='admin'){
	if (!$_SESSION['admin']['tipe']=='staff'){
		header('Location:../index.php');
	}
}

?>

<?php 

include_once '../../baseurl.php';

?>

<!DOCTYPE html>

<html>

<head>
	<title>Editor - Tutorial::Edit</title>
	<?php include_once 'header.php'; ?>
</head>

<body>
	<?php include_once '../sidebar.php'; ?>
	<?php include_once '../top-nav.php'; ?>
	<?php include_once '../classes/View.php'; ?>
	<?php include_once '../functions/imageuploadfunc.php'; ?>
	<?php include_once '../../model/Tutorial.php'; ?>
	<?php include_once '../../model/Querybuilder.php'; ?>

	<?php 
	$idata = new Data();
	$ctrlTuts = new ControlTutorial();
	$db = new QueryBuilder();
	$datas = json_decode($ctrlTuts->selectJudulAndID());
	$folder = $db->FetchOneRow(['folder_path'],'folders',array($db->GetCond('folder_relation_data','=','tuts_thumbnails')));
	$uploadthumbnailpath = $idata->homedir.$folder['folder_path'];
	// echo $idata->homedir.$folder['folder_path'];
	if (isset($_POST['edit-id'],$_POST['edit-judul'],$_POST['edit-tags'],$_POST['edit-isi'])){
		if (!empty($_POST['judul'])) {
			echo "<script>";
			echo "alert('Judul Tutorial Harus Di isi');";
			echo "</script>";
		}else{
			$dataInput = array(
								'id' => $_POST['edit-id'],
								'judul' => $_POST['edit-judul'], 
								'bdesc' => $_POST['edit-desc'],
								'isi' => $_POST['edit-isi']
							   );
			if ($ctrlTuts->updateTutorial($dataInput)){
				echo "<script>";
				echo "alert('Success: Pengubahan data berhasil! ');";
				echo "</script>";				
			}else{
				echo "<script>";
				echo "alert('Error: Data gagal di ubah! ');";
				echo "</script>";					
			}
		}
	}

	?>
	




	<div class="container" style='margin-bottom:40px;margin-top:40px'>

		


		<?php if (isset($_GET['id'])): ?>


			<?php 

			if (isset($_POST['in'])) {
				if (empty($_POST['in']['edit-judul'])) {
					echo "<script>";
					echo "alert('Judul Tutorial Harus Di isi');";
					echo "</script>";
				}else{
					
					if (isset($_FILES['thb'])&&!empty($_FILES['thb']['name'])) {
						$_FILES['thb']['name'] = $_GET['id'].'.'.pathinfo($_FILES['thb']['name'],PATHINFO_EXTENSION);
						$dataInput = array(
							'id' => $_POST['in']['edit-id'],
							'judul' => $_POST['in']['edit-judul'], 
							'bdesc' => $_POST['in']['edit-desc'],
							'isi' => $_POST['in']['edit-isi'],
							'thumbnail' => $_FILES['thb']['name']
						);
						$ru = UploadImage(['png','jpg','JPG','JPEG'],$_FILES['thb'],$uploadthumbnailpath);

						if ($ctrlTuts->updateTutorialwithimage($dataInput)) {
							echo "<script>alert('Data berhasil diubah!');</script>";
							echo "<script>location.replace('".$idata->base_url."administrasi/cms/tutorial-table');</script>";
						} else {
							echo "<script>alert('Data gagal diubah!');</script>";
						}

					}else{
						$dataInput = array(
							'id' => $_POST['in']['edit-id'],
							'judul' => $_POST['in']['edit-judul'], 
							'bdesc' => $_POST['in']['edit-desc'],
							'isi' => $_POST['in']['edit-isi']
						);
						if ($ctrlTuts->updateTutorial($dataInput)){
							echo "<script>alert('Data berhasil diubah!');</script>";
							echo "<script>location.replace('".$idata->base_url."administrasi/cms/tutorial-table');</script>";	
						}else{
							echo "<script>alert('Data gagal diubah!');</script>";
						}
					}

					
				}
			}

			?>


		<?php 
			$dataOfTuts = $ctrlTuts->selectById($_GET['id']);
			if (isset($_POST['edit-id'],$_POST['edit-judul'],$_POST['edit-tags'],$_POST['edit-isi'])){
				echo 'ok';
		if (!empty($_POST['judul'])) {
			
		}else{
					
		}
	}
		?>

		<form action='' method='post' enctype="multipart/form-data">

			<br>
			<div class='row'>
				<div class='col-md-10'>
					<div class='form-group'>
						<label>Judul</label>
						<input class='form-control' name='in[edit-judul]' value=<?php echo "'".$dataOfTuts->judul."'"; ?>>
					</div>
				</div>
			</div>

			<div class='row'>
				<div class='col-md-10'>
					<div class='form-group'>
						<label>Deskripsi Singkat</label>
						<textarea name="in[edit-desc]" class="form-control" placeholder="Deskripsi singkat tutorial."><?= $dataOfTuts->brief_desc ?></textarea>
					</div>
				</div>
			</div>

			<div class='row'>
				<div class='col-md-10'>
					<div class='form-group'>
						<label>Thumbnails</label>
						<?php if ($dataOfTuts->thumbnail!='-'): ?>
							<br>
							<img style="height:80px;width:150px" src="<?= $idata->base_url.$folder['folder_path'].$dataOfTuts->thumbnail ?>" alt="thumbnail"> <a href="<?= $idata->base_url.'administrasi/cms/services/removethumbnail/'.$dataOfTuts->id ?>" title=""><span class="glyphicon glyphicon-remove"></span></a>
							<br>
						<?php endif ?>
						<input type="file" name="thb">
					</div>
				</div>
			</div>

			<div class='row'>
				<div class='col-md-10'>
					<div class='form-group'>
						<label>Konten/Isi</label>
						<textarea id='isi-tutorial' name='in[edit-isi]'><?php echo $dataOfTuts->isi; ?></textarea>
					</div>
				</div>
			</div>
			<input style='display:none' name='in[edit-id]' value=<?php echo "'".$_GET['id']."'"; ?>>
			<button class='btn btn-primary'>Submit</button>
		</form>
		<?php else: ?>
			<script>location.replace('tutorial-table');</script>
		<?php endif ?>


	</div>

	
<       <script type="text/javascript" src=<?php echo "'".getBaseUrl()."ckeditor/ckeditor.js'"; ?>></script>


	<script type="text/javascript">

		CKEDITOR.replace('isi-tutorial',{

		   filebrowserBrowseUrl : 'kcfinder/browse.php?opener=ckeditor&type=files',

		   filebrowserImageBrowseUrl : 'kcfinder/browse.php?opener=ckeditor&type=images',

		   filebrowserFlashBrowseUrl : 'kcfinder/browse.php?opener=ckeditor&type=flash',

		   filebrowserUploadUrl : 'kcfinder/upload.php?opener=ckeditor&type=files',

		   filebrowserImageUploadUrl : 'kcfinder/upload.php?opener=ckeditor&type=images',

		   filebrowserFlashUploadUrl : 'kcfinder/upload.php?opener=ckeditor&type=flash'

		});

	</script>

</body>

</html>