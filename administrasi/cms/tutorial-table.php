<?php 
session_start();

if (!isset($_SESSION['admin'])&&!$_SESSION['admin']['type']=='admin'){
	if (!$_SESSION['admin']['tipe']=='staff'){
		header('Location:../index.php');
	}
}

?>

<?php 

include_once '../../baseurl.php';

//echo $coba = getBaseUrl();

?>

<!DOCTYPE html>

<html>

<head>
	<title>Editor - Tutorial</title>
<?php include_once 'header.php'; ?>

</head>

<body>
	<?php include_once '../sidebar.php'; ?>
	<?php include_once '../top-nav.php'; ?>
	<?php include_once '../../model/Tutorial.php'; ?>

	<?php 
	$ctrlTuts = new ControlTutorial();

	if (isset($_GET['page'])) {
		$datas = json_decode($ctrlTuts->selectAllData($_GET['page'],'6'));	
	}elseif (isset($_GET['search'])) {
		$datas = $ctrlTuts->searchJudul($_GET['search']);
	} else {
		$datas = json_decode($ctrlTuts->selectAllData('0','6'));
	}

		if (isset($_POST['delete-id'])&&!empty($_POST['delete-id'])) {
			//echo $_POST['delete-id'];
			if ($ctrlTuts->deleteTutorial($_POST['delete-id'])) {
				echo "<script>";
				echo "alert('Data Telah Dihapus');";
				echo "location.replace('".basename(__FILE__, '.php')."');";
				echo "</script>";
			}else{
				echo "<script>";
				echo "alert('Data Gagal Dihapus');";
				echo "</script>";				
			}
		}
	?>
	


	<div class="container" style='margin-bottom:40px;margin-top:40px'>

	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<form action="" method="GET">
				<?php if (isset($_GET['search'])): ?>
				<input value="<?php echo $_GET['search'] ?>" type="text" name="search" placeholder="Nama judul tutorial" class="form-control" style="width:70%">
					<?php else: ?>
				<input type="text" name="search" placeholder="Nama judul tutorial" class="form-control" style="width:70%">
				<?php endif ?>
				
				</form>
			</div>
		</div>
	</div>

		<table class='table table-bordered'>
			<thead>
				<tr>
					<th>Nama Judul</th><th>Desk. Singkat</th><th>Tags</th><th>Tanggal Posting</th><th style='text-align:center'>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				for ($i=0; $i < count($datas) ; $i++) { 
				?>	

					<tr>
						<td><?php echo $datas[$i]->judul; ?></td>
						<?php if ($datas[$i]->tags==''): ?>
						<td>-</td>	
							<?php else: ?>
						<td><?php echo substr($datas[$i]->tags, 0, 75); ?></td>
						<?php endif ?>
						<td>
							<?php if ($datas[$i]->new_tags==''): ?>
								<a href="https://fac-institute.com/administrasi/cms/tags/edittag/<?= $datas[$i]->aidi ?>" title="">
									New Tag, click here.
								</a>
							<?php else: ?>
								<a href="https://fac-institute.com/administrasi/cms/tags/edittag/<?= $datas[$i]->aidi ?>" title="">
									<?= $datas[$i]->new_tags ?>
								</a>
							<?php endif ?>
							
						</td>
						
						<td><?php echo $datas[$i]->tanggal; ?></td>
						<td>
							<div class='form-inline'>
								<a href=<?php echo "'".getBaseUrl()."administrasi/cms/tutorial-edit?id=".$datas[$i]->aidi."'"; ?> class='btn btn-warning'>Edit</a>
								<a href="https://fac-institute.com/administrasi/newtuts?del=<?= $datas[$i]->aidi ?>" class='btn btn-danger'>Delete</a>
							</div>
						</td>
					</tr>

				<?php } ?> 

				
				
			</tbody>
		</table>
		<?php if (isset($_GET['search'])): ?>
			<div class="row">
				<div class="col-md-12">
					<center>
						<a href="<?php echo basename(__FILE__, '.php') ?>" class="btn btn-primary" style="width:50%"><< Back</a>
					</center>
				</div>
			</div>
		<?php else: ?>
		
		<div class="row">
        <div class="col-md-12">
        <center>
            <ul class="pagination pagination-sm">

            <?php 
            	$totalRow = $ctrlTuts->countAllTutorial();
                $limit = 6;
                if (isset($_GET['page'])) {
                    $page = $_GET['page'];
                } else {
                    $page = 0;
                }
                
                
                $b = intval($page/$limit);
                $c = ($b + 1) * 5;
                $batas = intval($totalRow) / 6;

                if ($page >= $limit) {
                    if ($page == $limit) {
                        $prev = $b * $limit - 2;
                    } else {
                        $prev = $b * $limit - 1;
                    }

                    echo "<li><a href='".basename(__FILE__, '.php')."?page=$prev'>< Prev</a></li>";
                }

                for ($i=$b*$limit-1; $i < $c; $i++) { 
                    $j = $i +1;

                    if ($i<$batas&&$j!=0) {
                        echo "<li><a href='".basename(__FILE__, '.php')."?page=$j'>$j</a></li>";    
                    }
                }
                
                if ($c < $batas) {
                    $k = $c + 1;
                    echo "<li><a href='".basename(__FILE__, '.php')."?page=$k'>Next ></a></li>";
                }
                
                // for ($i=1; $i < intval($totalRow) / 30 + 1 ; $i++) { 
                    // echo "<li><a href='".basename(__FILE__, '.php')."?page=$i'>$i</a></li>";   
                // }
            ?>
            </ul>
        </center>
        </div>
    </div><!--end row pagination-->
    <?php endif ?>

	</div>

	    <!-- Modal -->
  <div class='modal fade' id='modal-konfirmasi' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pesan Konfirmasi</h3>

        </div>
        <div class='modal-body'>
        <div style='padding:15px' class='row'>
          
          <div class="col-md-12">
            
            		<div class="row" id='message-body'>
            			Anda Yakin Ingin Menghapus Data <span id='judul-delete' style="font-weight: bold;"></span> ?
            		</div>           		


          </div>
        </div>

        </div>
        <div class='modal-footer'>
        	<div class="row">
        		        
        					<input type='text' name='delete-id' id='konfirm-id' style='display:none' />
        		<div class="col-md-6" style="text-align:left">
							<a id="tblhapus" href="#" class='btn btn-danger'>Ya</a>
						
						<button class='btn btn-info' data-dismiss='modal'>Tidak</button>        			
        		</div>

        		<div class="col-md-6">
        			<div class='form-inline'>
						<span>FAC-Institute 2016</span>
        			</div>
        			
        		</div>
        	</div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->  

<script type="text/javascript">
	function coba(aidi,myTitle){
		//alert(id);
		document.getElementById("judul-delete").innerHTML=myTitle;
		document.getElementById('konfirm-id').value=aidi;
		alert('oke');
		var link = document.getElementById("tblhapus");
		link.setAttribute("href", "xyz.php" + aidi);
    	// $('#tblhapus').prop('href', "http://rumahiska.com/administrasi/transaksi/status.php/cancel/" + aidi);
		// $('#tblhapus').prop('href', 'https://fac-institute.com/administrasi/newtuts?del='+aidi);
	}
</script>

</body>

</html>