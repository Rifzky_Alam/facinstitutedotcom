<?php 
// include_once '/Applications/XAMPP/xamppfiles/htdocs/facinstitute/administrasi/controller/tutorial.controller.php';
include_once '/home/facinsti/public_html/administrasi/controller/tutorial.controller.php';

session_start();
// echo 'kok ga mau';
if (isset($_SERVER['PATH_INFO'])) {
	@$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
	$key = array_shift($url_segment);
	switch ($key) {
		case 'addnew':
			Tutorial::AddTags();
			break;
		case 'tagnameapi':
			if (isset($_GET['tag'])&&!empty($_GET['tag'])) {
				Tutorial::GetAjaxTags($_GET['tag']);	
			}
			break;
		case 'edittag':
			if (isset($url_segment[0])) {
				Tutorial::SelectTags($url_segment[0]);
			}else {
				echo 'parameter id required';
			}
			break;
		case 'remove':
			if (isset($url_segment[0])) {
				Tutorial::RemoveTag($url_segment[0]);
			}else {
				echo 'parameter id required';
			}
			break;
		case 'lists':
			Tutorial::GetTagLists();
			break;
		case 'clearoldtag':
			if (isset($url_segment[0])) {
				Tutorial::ClearOldTags($url_segment[0]);
			} else {
				header('Location:https://fac-institute.com/administrasi/cms/tutorial-table');
			}
			break;
		case 'removetag':
			if (isset($url_segment[0])) {
				Tutorial::HapusTag($url_segment[0]);
			} else {
				header('Location:https://fac-institute.com/administrasi/cms/tutorial-table');
			}
			break;
	}
}else{
	echo 'welcome';
}