<?php 
session_start();

if (!isset($_SESSION['admin'])&&!$_SESSION['admin']['type']=='admin'){
	header('Location:../index.php');
}

?>

<?php 

include_once '../../baseurl.php';

//echo $coba = getBaseUrl();

?>

<!DOCTYPE html>

<html>

<head>
	<title>Editor - Tutorial</title>
<?php include_once 'header.php'; ?>

</head>

<body>
	<?php include_once '../sidebar.php'; ?>
	<?php include_once '../top-nav.php'; ?>
	<?php include_once '../../model/Tutorial.php'; ?>

	<?php 
	$ctrlTuts = new ControlTutorial();
	//echo "string";
	//echo $ctrlTuts->coba();
	?>
	


	<div class="container" style='margin-bottom:40px;margin-top:40px'>

		<form action='' method='post'>

		<div class='row'>
			<div class='col-md-10'>
				<div class='form-group'>
					<label>Judul Tulisan</label>
					<input type="text" class='form-control' name='judul' placeholder="tulis judul dari tutorial disini" required>
				</div>
			</div>
		</div>

		<div class='row'>
			<div class='col-md-10'>
				<div class='form-group'>
					<label>Deskripsi singkat</label>
					<textarea name="bdesc" class="form-control" placeholder="Beri penjelasan singkat untuk mewakili tutorial ini, max 255 karakter" maxlength="255" required></textarea>
				</div>
			</div>
		</div>

		<div class='row'>
			<div class='col-md-6'>
				<div class='form-group'>
					<label>Tags</label>
					<!-- <input class='form-control' name='tags' id='tagz' /> -->
					<p>Input Tag di alihkan setelah selesai input data tutorial, silahkan input tag di menu <a href="https://fac-institute.com/administrasi/cms/tutorial-table" title="List tutorial">List data tutorial</a></p>
				</div>
			</div>
		</div>

		

			<textarea id='contoh' name='isi'></textarea>

			<br>

			<button class='btn btn-primary'>Submit</button>

		</form>

	</div>



	<div class='row'>
		<div class='col-md-12'>
		<?php 
			if (isset($_POST['judul'],$_POST['isi'])){

				$dataInserts = array(

					'judul' => $_POST['judul'],
					'isi' => $_POST['isi'],
					'tags' => '',
					'bdesc' => $_POST['bdesc']
					);

				if ($ctrlTuts->inserData($dataInserts)){
					echo "<script>";
					echo "alert('Data Berhasil Disimpan');";
					echo "</script>";
				}else{
					echo "<script>";
					echo "alert('Data Gagal Disimpan');";
					echo "</script>";					
				}
			}

		?>
		</div>
	</div>



	<script type="text/javascript" src="ckeditor/ckeditor.js"></script>

	<script type="text/javascript">

		CKEDITOR.replace('contoh');

	</script>

</body>

</html>