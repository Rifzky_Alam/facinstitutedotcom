<?php function tabel($value,$objek){ ?>

<?php if ($value=='dtrequest'): ?>

    <table class="table table-hover">
        <thead>
            <tr>
                <th>Nama</th>
                <th>Tanggal</th>
                <th>Nama Perusahaan</th>
            </tr>
        </thead>

        <tbody>
            <?php
                    for ($i=0; $i < count($objek) ; $i++) {
                    echo "<tr>";
                    echo "<td>".$objek[$i]->nama."</td>";
                    echo "<td>".$objek[$i]->tanggal_absen."</td>";
                    echo "<td>".$objek[$i]->nama_perusahaan."</td>";
                    echo "</tr>";
                }
            ?>
        </tbody>
    </table>

<?php elseif($value=='request'): ?>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>Nama</th>
                <th>Tanggal</th>
                <th>Nama Perusahaan</th>
                <th style="text-align:center;">Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php
                for ($i=0; $i < count($objek) ; $i++) {
                    echo "<tr>";
                    echo "<td>".$objek[$i]->nama."</td>";
                    echo "<td>".$objek[$i]->tanggal_absen."</td>";
                    echo "<td>".$objek[$i]->nama_perusahaan."</td>";
                    echo "<td style='text-align:center;'><a href='".basename(__FILE__, '.php')."?ac=cgatt&tanggal=".$objek[$i]->tanggal_absen."&v=".$objek[$i]->id."'><span class='glyphicon glyphicon-log-in'></span></a></td>";
                    echo "</tr>";
                }
            ?>

        </tbody>

    </table>

<?php elseif($value=='cgatt'): ?>
    <form action="" method="post">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Tanggal</th>
                    <th>Nama Perusahaan</th>
                    <th style="text-align:center;">-</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    for ($i=0; $i < count($objek) ; $i++) {
                        echo "<tr>";
                        echo "<td>".$objek[$i]->tanggal."</td>";
                        echo "<td>".$objek[$i]->perusahaan."</td>";
                        echo "<td><input type='radio' name='perusahaan' value='".$objek[$i]->id."' /></td>";
                        echo "</tr>";
                    }
                ?>
                <tr>
                    <td colspan="3"><button class="btn btn-primary" style="width:100%;">Submit</button></td>
                </tr>
            </tbody>
        </table>
    </form>
<?php elseif($value=='map'): ?>
    <table class="table">
        <thead>
            <tr>
                <th>Tanggal</th>
                <th>Nama Perusahaan</th>
                <th>Petugas</th>
                <th>Lokasi Masuk</th>
                <th>Lokasi Keluar</th>
            </tr>
        </thead>
        <tbody>
            <?php
                for ($i=0; $i < count($objek) ; $i++) {
                    echo "<tr>";
                    echo "<td>".$objek[$i]->tanggal_absen."</td>";
                    echo "<td>".$objek[$i]->nama_perusahaan."</td>";
                    echo "<td>".$objek[$i]->nama."</td>";
                    echo "<td><a href='daftar-hadir?key=".$objek[$i]->username."&loc=".$objek[$i]->lokasi_absen_masuk."'>".$objek[$i]->lokasi_absen_masuk."</a></td>";
                    echo "<td><a href='daftar-hadir?key=".$objek[$i]->username."&loc=".$objek[$i]->lokasi_absen_masuk."'>".$objek[$i]->lokasi_absen_pulang."</a></td>";
                    echo "</tr>";
                }
            ?>
        </tbody>
    </table>

<?php elseif($value=='srclm'): ?>
    <table class="table">
        <thead>
            <tr>
                <th>Nama Perusahaan</th>
                <th>Staff/Petugas</th>
                <th>Rekan</th>
                <th>Ket Jenis Usaha</th>
                <th>Kegiatan</th>
                <th>Peserta</th>
            </tr>
        </thead>
        <tbody>
            <?php
                for ($i=0; $i < count($objek) ; $i++) {
                    echo "<tr>";

                    echo "<td>".$objek[$i]->nama_perusahaan."</td>";
                    echo "<td>".$objek[$i]->nama."</td>";
                    echo "<td>".$objek[$i]->rekan."</td>";
                    echo "<td>".$objek[$i]->keterangan_jenis_usaha."</td>";
                    echo "<td>".$objek[$i]->ket_kegiatan."</td>";
                    echo "<td>".@$objek[$i]->peserta_kegiatan."</td>";

                    echo "</tr>";
                }
            ?>
        </tbody>
    </table>

<?php elseif($value=='lfm'): ?>

    <table class="table">
        <thead>
            <tr>
                <th>Timestamp</th>
                <th>Tanggal Follow-Up</th>
                <th>Customer</th>
                <th>Agenda</th>
                <th>Status</th>
                <th>Jumlah Hari</th>
                <th>Petugas</th>
                <th style="text-align:center;">Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php
                for ($i=0; $i < count($objek) ; $i++) {
                    echo "<tr>";
                    echo "<td>".@$objek[$i]->date_created."</td>";
                    $tgl = explode('-', $objek[$i]->tanggal);
                    echo "<td>".$tgl[2].'-'.$tgl[1].'-'.$tgl[0]."</td>";
                    echo "<td>".$objek[$i]->customer."</td>";
                    echo "<td>".$objek[$i]->agenda."</td>";
                    echo "<td>".$objek[$i]->keterangan."</td>";
                    echo "<td>".$objek[$i]->jumlah_hari."</td>";
                    echo "<td>".@$objek[$i]->nama."</td>";
                    echo "<td style='text-align:center;'><a href='laporan-followup?ac=edit&n=".$objek[$i]->num."' class='btn btn-warning'>Edit</a> || <a href='#' class='btn btn-danger'>Delete</a></td>";
                    echo "</tr>";
                }
            ?>
        </tbody>
    </table>

<?php elseif($value=='nppi'): ?>
  <table class="table">
      <thead>
          <tr>
              <th>Nama Paket</th>
              <th>Harga</th>
              <th>Paket Hari</th>
              <th>Status</th>
              <th>Keterangan</th>
              <th style="text-align:center;">Aksi</th>
          </tr>
      </thead>
      <tbody>
          <?php
              for ($i=0; $i < count($objek) ; $i++) {
                if ($objek[$i]->status_paket=='1') {
                    echo "<tr>";
                }elseif ($objek[$i]->status_paket=='0') {
                    echo "<tr class='danger'>";
                }elseif ($objek[$i]->status_paket=='3') {
                    echo "<tr class='info'>";
                } else {
                    echo "<tr class='warning'>";
                }
                
                  
                  // $tgl = explode('-', $objek[$i]->tanggal);
                  // echo "<td>".$tgl[2].'-'.$tgl[1].'-'.$tgl[0]."</td>";
                  echo "<td>".$objek[$i]->nama_item."</td>";
                  echo "<td>".$objek[$i]->harga."</td>";
                  echo "<td>".$objek[$i]->paket_hari."</td>";
                  echo "<td>".$objek[$i]->keterangan."</td>";
                  echo "<td>".$objek[$i]->keterangan_lain."</td>";
                  // echo "<td>".$objek[$i]->status_paket."</td>";
                  // echo "<td>".@$objek[$i]->nama."</td>";
                  echo "<td style='text-align:center;'><a href='input-paket?ac=edit&kode=1&id=".$objek[$i]->id."' class='btn btn-success'>show</a> || <a href='input-paket?ac=edit&kode=2&id=".$objek[$i]->id."' class='btn btn-warning'>hide</a> || <a href='input-paket?ac=edit&kode=3&id=".$objek[$i]->id."' class='btn btn-info'>extrnl</a></td>";
                  echo "</tr>";
              }
          ?>
      </tbody>
  </table>

<?php elseif($value=='sotd'): ?>
    <table class="table">
      <thead>
          <tr>
              <th>Nama </th>
              <th>Email</th>
              <th>Telepon</th>
              <th>Waktu daftar</th>
          </tr>
      </thead>
      <tbody>
          <?php
              for ($i=0; $i < count($objek) ; $i++) {
                  // $tgl = explode('-', $objek[$i]->tanggal);
                  // echo "<td>".$tgl[2].'-'.$tgl[1].'-'.$tgl[0]."</td>";
                  echo "<td>".$objek[$i]->name."</td>";
                  echo "<td>".$objek[$i]->email."</td>";
                  echo "<td>".$objek[$i]->phone."</td>";
                  echo "<td>".$objek[$i]->time_of_subcription."</td>";
                  echo "</tr>";
              }
          ?>
      </tbody>
  </table>

<?php endif ?>

<?php } ?>
