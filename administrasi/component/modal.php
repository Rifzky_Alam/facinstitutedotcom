<?php function modal($value){ ?>

<?php if ($value=='map'): ?>
  
<!-- Modal -->
  <div class='modal fade' id='modal-cari' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pencarian Data</h3>

        </div>
       <div class='modal-body' style='overflow-y:scroll'>
        <div style='padding:15px; height: 300px;' class='row'>
          
          <div class="col-md-12">
        
                <form action="" method="GET">

                    <div class="row">
                        <div class="form-group">
                          <label>Nama Perusahaan</label>
                          <input type="text" name="ac" style="display:none;" value="map">
                          <input type="text" name="np" class="form-control">
                        </div>
                    </div>

                    <div class="row">
                        <button class="btn btn-success" style='width:100%'>Cari</button>
                    </div>

                </form>
          </div>
        </div>
       </div>


        <div class='modal-footer'>

            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>FAC-Institute 2016</span>
                </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->  

<?php elseif($value=='srclm'): ?>

<!-- Modal -->
  <div class='modal fade' id='modal-cari' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pencarian Data</h3>

        </div>
       <div class='modal-body' style='overflow-y:scroll'>
        <div style='padding:15px; height: 300px;' class='row'>
          
          <div class="col-md-12">
        
                <form action="" method="GET">

                    <div class="row">
                        <div class="form-group">
                          <label>Nama Perusahaan</label>
                          <input type="text" name="ac" style="display:none;" value="srclm">
                          <input type="text" name="v[np]" class="form-control">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                          <label>Nama Petugas</label>
                          <input type="text" name="v[sn]" class="form-control">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                          <label>Keterangan Kegiatan</label>
                          <input type="text" name="v[kk]" class="form-control">
                        </div>
                    </div>

                    <div class="row">
                        <button class="btn btn-success" style='width:100%'>Cari</button>
                    </div>

                </form>
          </div>
        </div>
       </div>


        <div class='modal-footer'>

            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>FAC-Institute 2016</span>
                </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->

<?php elseif($value=='lfm'): ?>

<!-- Modal -->
  <div class='modal fade' id='modal-cari' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h3 class="modal-title">Pencarian Data</h3>

        </div>
       <div class='modal-body' style='overflow-y:scroll'>
        <div style='padding:15px; height: 300px;' class='row'>
          
          <div class="col-md-12">
        
                <form action="" method="POST">

                    <div class="row">
                        <div class="form-group">
                          <label>Nama Perusahaan</label>
                          <input type="text" name="ac" style="display:none;" value="srclm">
                          <input type="text" name="v[np]" class="form-control">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                          <label>Tanggal Awal</label>
                          <input type="text" id="tanggal-awal" name="v[fd]" class="form-control">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                          <label>Tanggal Akhir</label>
                          <input type="text" id="tanggal-akhir" name="v[ld]" class="form-control">
                        </div>
                    </div>

                    <div class="row">
                        <button class="btn btn-lg btn-success" style='width:100%'>Cari</button>
                    </div>

                </form>
          </div>
        </div>
       </div>


        <div class='modal-footer'>

            <div class="row">
                <div class="col-md-6" style="text-align:left">
                    <span>FAC-Institute 2016</span>
                </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
  <!-- end modal -->


<?php endif ?>



<?php } ?>


