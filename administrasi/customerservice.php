<?php 
session_start();
if(!isset($_SESSION['admin']['username'])&&empty($_SESSION['admin']['username'])){
    header('location:'.$_SERVER['HTTP_HOST'].'/administrasi/accessdenied');
}

if (isset($_SERVER['PATH_INFO'])) {
	@$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
	$key = array_shift($url_segment);
	switch ($key) {
		case 'data-support':
			include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/controller/customerservice.controller.php';
			if (isset($_GET['det'])&&!empty($_GET['det'])) {
				CustServCtr::DataSupportDetail($_GET['det']);
			}elseif(isset($_GET['edt'])&&!empty($_GET['edt'])){
			    CustServCtr::EditSupportData($_GET['edt']);
			}elseif(isset($_GET['page'])&&!empty($_GET['page'])){
			    CustServCtr::TabelDataSupport($_GET['page']);
			}else{
				CustServCtr::TabelDataSupport();
			}
		    break;
		case 'newcomplists':
			include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/controller/customerservice.controller.php';
			CustServCtr::NewCompanyList();
			break;
		case 'api':
		    include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/controller/customerservice.controller.php';
		    if(isset($_GET['req'])&&$_GET['req']=='cte') //company to export
		        CustServCtr::GetAllExportableCompany();
		    break;
		case 'exportusaha':
		    include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/controller/customerservice.controller.php';
		    if(isset($_GET['id']))
		        CustServCtr::EditStatusUsaha($_GET['id'],'1');
		    break;
		case 'newclient':
			include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/controller/customerservice.controller.php';
			if (isset($_GET['id'])) {
				CustServCtr::NewClient($_GET['id']);
			}
			break;
		case 'editclient':
		    include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/controller/customerservice.controller.php';
		    if (isset($_GET['id'])) {
				CustServCtr::EditClient($_GET['id']);
			}
		    break;
		case 'download':
		    include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/controller/customerservice.controller.php';
		    if(isset($_GET['src'])&&!empty($_GET['src'])){
		        CustServCtr::DownloadDataSupport($_GET['src']);
		    }
		    break;
		case 'downloadreportformeeting':
		    include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/controller/customerservice.controller.php';
		    if(isset($_GET['src'])&&!empty($_GET['src'])){
		        if(isset($_GET['src']['fd'])&&$_GET['src']['fd']!=''&&isset($_GET['src']['td'])&&$_GET['src']['td']!=''){
		            CustServCtr::DownloadDataSupportForMeeting($_GET['src']);
		        }
		    }
		    break;
		case 'data-usaha':
		    include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/controller/customerservice.controller.php';
		    if(isset($_GET['namausaha'])&&!empty($_GET['namausaha'])){
		        CustServCtr::SearchNamaUsaha($_GET['namausaha']);
		    }elseif (isset($_GET['id'])&&!empty($_GET['id'])) {
		    	CustServCtr::SearchNamaUsaha($_GET['id'],'byid');
		    }else{
		        CustServCtr::TabelUsaha();
		    }
		    break;
		case 'newsupport':
		    include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/controller/customerservice.controller.php';
		    if(isset($_GET['id'])&&!empty($_GET['id'])&&isset($_GET['k'])&&!empty($_GET['k'])){
		        CustServCtr::NewSupportData($_GET['id'],$_GET['k']);
		    }else{
		       CustServCtr::NewSupportNonClientData();
		    }
		    break;
		default:
		header('location: https://fac-institute.com/administrasi/');
		break;
	}
}else{
	header('location: https://fac-institute.com/administrasi/');
}