<?php 
session_start();
if (!isset($_SESSION['admin'])) {
    header('Location:../login/index.php');
}

function removeLastString($value){
        if (substr($value,strlen($value)-3)==' # ') {
            return substr($value, 0,strlen($value)-3);
        }else{
            return $value;
        }
}
?>





<?php if (isset($_GET['app'])&&isset($_GET['id'])&&$_GET['app']=='preview'): ?>
<?php include_once '../model/Surat.php'; $surat = new Surat(); ?>
<?php 
include_once '../model/Pendaftar.php'; 
$pendaftar = new Pendaftar();
$el_data=$pendaftar->fetchDataRequestByID($_GET['id']);
$dataPerusahaan = json_decode($el_data->nama_perusahaan);
$dataJadwal = json_decode($el_data->data_jadwal);
$dataInvoice = json_decode($el_data->temp_invoice);
@$paketTraining = json_decode($pendaftar->getPaketTraining($dataInvoice->paket));

$data = array(
                'to' => $el_data->email_kontak,
                'nama'=> $el_data->nama_personal,
                'subject'=>'Jadwal & Invoice Training Software Accurate',
                'perusahaan'=>$dataPerusahaan->nama,
                'tanggalz'=>explode(' # ', $dataJadwal->tanggal),
                'agenda'=>explode(" # ",$el_data->agenda_training),
                'waktu'=>$dataJadwal->waktu,
                'tempat'=>@$dataJadwal->tempat,
                'lokasi'=>$el_data->alamat_perusahaan,
                'item' => @$paketTraining[0]->nama_item,
                'harga'=> @$paketTraining[0]->harga,
                'x'=>$dataInvoice->x,
                'transport'=>$dataInvoice->transport,
                'jmlhari'=>$dataInvoice->jml_hari,
                'total'=>850000,
                'officer'=>$_SESSION['admin']['nama'],
                'attachment'=>'attachments/3.jpg'
                );
$data=(object)$data;
echo $surat->penawaran($data);
?>
	
    
    <?php //Edit data buat invoice ?>
    <?php elseif(isset($_GET['app'])&&isset($_GET['id'])&&$_GET['app']=='prepare'): ?>
        <?php 
        include_once '../model/Pendaftar.php'; 
        include_once '../model/page.php';
        $myPage = new Page();
        $pendaftar = new Pendaftar();
        $datas=$pendaftar->fetchDataRequestByID($_GET['id']);
        $dataUsaha = json_decode($datas->nama_perusahaan);
        $dataJadwal = json_decode($datas->data_jadwal);
        $dataInvoice = json_decode($datas->temp_invoice);
        @$paketTraining = json_decode($pendaftar->getPaketTraining($dataInvoice->paket));
        //$myRecord = $petugas->selectByID($_SESSION['admin']['username']);
        $dataPaket = json_decode($pendaftar->fetchPaketTraining());
      




         ?>
<!DOCTYPE html>
<html>

<?php 
$judul='persiapan data invoice -- Marketing';
$page = 'penawaran';
include_once 'header.php'; 

?>

<body>



<?php 

include_once 'sidebar.php';

include_once 'top-nav.php';

 ?>

<div class="container" id="isi">
    <div class="page-header" id="top-logo">
        <h3>Persiapan Data Penawaran</h3>
    </div>
   
    <?php 
    if (isset($_POST['pre'])) {


    if (in_array('invoice', @$_POST['pre']['attachments'])) {
        $attch = 'yes';
    }else{
        $attch = 'no';
    }

    if (@$_POST['pre']['attachments']!='') {
        $lampiranz = removeLastString(implode(' # ', $_POST['pre']['attachments']));
    }else{
        $lampiranz = "";
    }   

    $objek = array(
        'id' => $_GET['id'],
        'perusahaan' => $_POST['pre']['nama'],
        'usaha' => $_POST['pre']['usaha'],
        'email' => $_POST['pre']['email'],
        'alamatTraining' => $_POST['pre']['alamat'],
        'paket' => $_POST['pre']['paket'],
        'jam' => $_POST['pre']['waktu'],
        'tanggal' => removeLastString(implode(' # ', $_POST['pre']['tanggalPelaksanaan'])),
        'personal' => $_POST['pre']['cp'],
        'email_kontak' => $_POST['pre']['email'],
        'alamat' => $_POST['pre']['alamat'],
        'telepon' => $_POST['pre']['telepon'],
        'jabatan' => $_POST['pre']['jabatan'],
        'invoice' => $attch,
        'statustrans' => 2,
        'x'=>$_POST['pre']['x'],
        'jenis_pengguna' => $_POST['pre']['jenisPengguna'],
        'agenda' => removeLastString(implode(' # ', $_POST['pre']['agendaTraining'])),
        'tempatbeli' => $_POST['pre']['tempatbeli'],
        'sales' =>  $_POST['pre']['sales'],
        'biayatransport'=>$_POST['pre']['transport'],
        'jmlhari'=>$_POST['pre']['jmlHari'],
        'attachments'=>$lampiranz,
        'tempat'=> $_POST['pre']['tempat']
    );

    if ($pendaftar->editForInvoice((object)$objek)){
        echo "<script>alert('Data berhasil disimpan!');</script>";
        echo "<script>location.replace('".basename(__FILE__, '.php')."?app=preview&id=".$objek['id']."');</script>";
    }else{
        echo "<script>alert('Data Gagal disimpan!');</script>";
    }


        /*

        echo "<div class='row'>";

        $dataPer = array(
            'nama' => $_POST['pre']['nama'], 
            'usaha'=>$_POST['pre']['usaha'],
            'email'=>$_POST['pre']['email'],
            'alamat'=>$_POST['pre']['alamat'],
        );

        @$lampiran = removeLastString(implode(' # ', $_POST['pre']['attachments']));
        $dataJad = array(
            'waktu' => $_POST['pre']['waktu'], 
            'tanggal'=>removeLastString(implode(' # ', $_POST['pre']['tanggalPelaksanaan'])),
            'tempat'=>$_POST['pre']['tempat'],
            'biayaTraining'=>$_POST['pre']['biaya'],
            'biayaTransport'=>$_POST['pre']['transport'],
            'attachments'=>$lampiran
        );

        $editData = array('cp' => $_POST['pre']['cp'], 'namaPerusahaan'=>json_encode($dataPer));


        echo $_POST['pre']['nama']."<br>";
        echo $_POST['pre']['cp']."<br>";
        echo removeLastString(implode(' # ', $_POST['pre']['tanggalPelaksanaan']))."<br>";
        echo $_POST['pre']['waktu']."<br>";
        echo $_POST['pre']['tempat']."<br>";
        echo $_POST['pre']['alamat']."<br>";
        echo removeLastString(implode(' # ', $_POST['pre']['agendaTraining']))."<br>";
        echo $_POST['pre']['biaya']."<br>";
        echo $_POST['pre']['jmlHari']."<br>";
        echo $_POST['pre']['transport']."<br>";
        
        echo "</div>";*/
    }
    
    ?>

    <form action="" method="POST">
    <div class="row">
        <div class="col-md-10">
            <div class="form-group">
                <label>Nama Perusahaan</label>
                <input type="text" name="pre[nama]" value="<?php echo $dataUsaha->nama ?>" class="form-control" >
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-10">
            <div class="form-group">
                <label>Email</label>
                <input type="text" name="pre[email]" value="<?php echo $dataUsaha->email ?>" class="form-control" >
            </div>
        </div>
    </div>    
    
    <div class="row">
        <div class="col-md-10">
            <div class="form-group">
                <label>Keterangan Usaha</label>
                <input type="text" name="pre[usaha]" value="<?php echo $dataUsaha->usaha ?>" class="form-control" >
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-10">
            <div class="form-group">
                <label>Nama Kontak</label>
                <input type="text" name="pre[cp]" value="<?php echo $datas->nama_personal ?>" class="form-control">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-10">
            <div class="form-group">
                <label>Telepon Kontak</label>
                <input type="text" name="pre[telepon]" value="<?php echo $datas->telepon ?>" class="form-control">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-10">
            <div class="form-group">
                <label>Jabatan Kontak</label>
                <input type="text" name="pre[jabatan]" value="<?php echo $datas->jabatan ?>" class="form-control">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-10">
            <div class="form-group">
                <label>Jenis Pengguna</label>
                <select class="form-control" name="pre[jenisPengguna]">
                <?php if ($datas->jenis_pengguna=='baru'): ?>
                        <option value="baru" selected>Baru</option>
                        <option value="lama">Lama</option>
                   <?php else: ?>
                        <option value="baru">Baru</option>
                        <option value="lama" selected>Lama</option>
                <?php endif ?>
                </select>
            </div>
        </div>
    </div>
        <h5><b>Tanggal Pelaksanaan</b></h5>
        <?php $dataJadwal = json_decode($datas->data_jadwal) ?>
        <?php $tanggal = explode(' # ', $dataJadwal->tanggal) ?>
        <?php for ($i=0; $i < count($tanggal); $i++) { ?>
        <div class="row" <?php echo "id='tanggal-$i'" ?>>
            <div class="col-md-10">
                    <div class="form-inline" >
                    <input type="text" id="tgl-pelaksanaan" name='pre[tanggalPelaksanaan][]' class="form-control tglPelaksanaanz" placeholder="klik disini untuk memilih tanggal" value=<?php echo "'".$tanggal[$i]."'"; ?>/>   
                    <span class='btn btn-danger glyphicon-minus' <?php echo "onclick='removeElement(".'"tanggal-'.$i.'"'.")'"; ?>></span>
                    </div>
            </div>
        </div>
        <?php } ?>

        <div class="row" id='jajal'>
            <div class="col-md-10">
                    <div class="form-inline" >
                        <input type="text" id="tgl-pelaksanaan" name='pre[tanggalPelaksanaan][]' class="form-control tglPelaksanaan" placeholder="klik disini untuk memilih tanggal"/>   
                            <span class='btn btn-sm btn-primary glyphicon glyphicon-plus' id='tambahTombol'></span>
                    </div>
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label>Waktu</label>
                    <input type="text" name="pre[waktu]" value="<?php echo $dataJadwal->waktu ?>" class="form-control">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label>Tempat Lokasi Training</label>
                     <input type="text" name="pre[tempat]" value="<?php echo @$dataJadwal->tempat ?>" class="form-control" placeholder="Kantor atau meeting room (tidak lebih dari 20 kata)" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label>Alamat</label>
                    <textarea class="form-control" name="pre[alamat]"><?php echo $datas->alamat_perusahaan ?></textarea> 
                </div>
            </div>
        </div>

        <?php $arrAgenda =explode(' # ', $datas->agenda_training)  ?>
        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                <h4>Agenda Training</h4>
                    <?php if (in_array('training fitur accurate', $arrAgenda)||$datas->agenda_training=='training fitur accurate'): ?>
                    <div class="checkbox"><label><input type="checkbox" name="pre[agendaTraining][]" value="training fitur accurate" checked>Training Fitur ACCURATE</label></div>
                        <?php else: ?>
                    <div class="checkbox"><label><input type="checkbox" name="pre[agendaTraining][]" value="training fitur accurate">Training Fitur ACCURATE</label></div>
                    <?php endif ?>

                    <?php if (in_array('setup database accurate', $arrAgenda)||$datas->agenda_training=='setup database accurate'): ?>
                    <div class="checkbox"><label><input type="checkbox" name="pre[agendaTraining][]" value="setup database accurate" checked>Setup Database ACCURATE</label></div> 
                        <?php else: ?>
                    <div class="checkbox"><label><input type="checkbox" name="pre[agendaTraining][]" value="setup database accurate">Setup Database ACCURATE</label></div>
                    <?php endif ?>
                    
                    <?php if (in_array('troubleshoot', $arrAgenda)||$datas->agenda_training=='troubleshoot'): ?>
                    <div class="checkbox"><label><input type="checkbox" name="pre[agendaTraining][]" value="troubleshoot" checked>Trouble Shooting ACCURATE</label></div>  
                        <?php else: ?>
                    <div class="checkbox"><label><input type="checkbox" name="pre[agendaTraining][]" value="troubleshoot">Trouble Shooting ACCURATE</label></div>
                    <?php endif ?>
                    
                    <?php if (in_array('review', $arrAgenda)||$datas->agenda_training=='review'): ?>
                    <div class="checkbox"><label><input type="checkbox" name="pre[agendaTraining][]" value="review" checked>Review Penggunaan ACCURATE</label></div>
                        <?php else: ?>
                    <div class="checkbox"><label><input type="checkbox" name="pre[agendaTraining][]" value="review">Review Penggunaan ACCURATE</label></div>
                    <?php endif ?>
                    
                    <div class="form-inline">
                        

                        <?php                             
                                for ($i=0; $i < count($arrAgenda); $i++) { 
                                    
                                    if ($arrAgenda[$i]!='training fitur accurate'&&$arrAgenda[$i]!='setup database accurate'&&$arrAgenda[$i]!='troubleshoot'&&$arrAgenda[$i]!='review'&&$arrAgenda[$i]!='lainnya') {
                                        echo "<input type='text' placeholder='lainnya' style='width:50%' class='form-control' name='pre[agendaTraining][]' id='agenda-lainnya' value='".$arrAgenda[$i]."'>";
                                        $apaAda=true;
                                    }
                                }
                            
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <label>Nama Paket</label>
                <select class="form-control" name="pre[paket]">
                    <?php  
                        for ($i=0; $i < count($dataPaket); $i++) { 
                            if ($dataPaket[$i]->id==$dataInvoice->paket) {
                                echo "<option value='".$dataPaket[$i]->id."' selected>".$dataPaket[$i]->nama_item." -- ".$dataPaket[$i]->price_for."</value>";
                            }else{
                                echo "<option value='".$dataPaket[$i]->id."'>".$dataPaket[$i]->nama_item." -- ".$dataPaket[$i]->price_for."</value>";    
                            }
                            
                        }
                    ?>
                </select>
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label>Kuantitas Paket</label>
                    <input type="number" id="biayaPerHari" name="pre[x]" class="form-control" placeholder="non paket di isi berdasarkan banyaknya hari, paket di isi per jumlah" value="<?php echo $dataInvoice->x ?>">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label>Jumlah Hari</label>
                    <input type="number" name="pre[jmlHari]" class="form-control" value="<?php echo count($tanggal) ?>">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label>Biaya Transport per Hari</label>
                    <input type="number" id="biayatr" name="pre[transport]" class="form-control" value="150000">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label>Tempat Beli Accurate</label>
                    <select name="pre[tempatbeli]" class="form-control">
                        <option value="mm">ABC MM</option>
                        <option value="mgk">MGK</option>
                        <option value="semanggi">Semanggi</option>
                        <option value="website">Website</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label>Salesman Accurate</label>
                    <input type="text" id="biayatr" name="pre[sales]" class="form-control" value="<?php echo $datas->salesman_accurate ?>">
                </div>
            </div>
        </div>
        <?php $lampiranzz = explode(" # ", $dataInvoice->attach) ?>
        <div class="row">
            <div class="col-md-10">
                <h5><strong>Attachments</strong></h5>
                <?php if (in_array('invoice', $lampiranzz)): ?>
                    <div class="checkbox"><label><input checked type="checkbox" name="pre[attachments][]" value="invoice">Invoice</label></div>
                    <?php else: ?>
                    <div class="checkbox"><label><input type="checkbox" name="pre[attachments][]" value="invoice">Invoice</label></div>
                <?php endif ?>
                
                
                <?php 
                $files = scandir('attachments/');
                for ($i=0; $i < count($files); $i++) { 
                    if ($files[$i]!='.'&&$files[$i]!='..') {
                        if (in_array($files[$i], $lampiranzz)) {
                            echo "<div class='checkbox'><label><input type='checkbox' checked name='pre[attachments][]' value='".$files[$i]."'>".
                            $files[$i].
                            "</label></div>";                         
                        }else{
                            echo "<div class='checkbox'><label><input type='checkbox' name='pre[attachments][]' value='".$files[$i]."'>".
                            $files[$i].
                            "</label></div>";
                        }
                    }
                }
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <button class="btn btn-lg btn-primary" style="width:100%">Kirim Penawaran</button>
            </div>
        </div>
        </form>
</div><!--end container-->
    <script type="text/javascript" src=<?php echo "'".getBaseUrl()."css/multiselect/js/bootstrap-multiselect.js'"; ?>></script>
    <script type="text/javascript" src=<?php echo "'".getBaseUrl()."css/datepicker/js/bootstrap-datepicker.js'"; ?>></script>
    <script type="text/javascript">

    

    jQuery.fn.ForceNumericOnly =
    function(){
        return this.each(function()
        {
            $(this).keydown(function(e)
            {
                var key = e.charCode || e.keyCode || 0;
                // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
                // home, end, period, and numpad decimal
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                 // Allow: Ctrl+C
                (e.keyCode == 67 && e.ctrlKey === true) ||
                 // Allow: Ctrl+X
                (e.keyCode == 88 && e.ctrlKey === true) ||
                 // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
            
            });
        });
    };

    $(document).ready(function(){
        $('.tglPelaksanaan').datepicker({
            format: 'yyyy-mm-dd'
        });
        $('.tglPelaksanaanz').datepicker({
            format: 'yyyy-mm-dd'
        });
        $('#biayaPerHari').ForceNumericOnly();
        $('#biayatr').ForceNumericOnly();
    });


    function removeElement(id) {
        $('#'+id).remove();
    }

        $('#tambahTombol').click(function(){
    
            var cobayah = $('.tglPelaksanaan').length;
            var iseng = "tambahan" + cobayah;
            if ($('#tambahan').length){

        if (cobayah==2) {
            $('#tambahan').after("<div class='row' id='"+iseng+"'><div class='col-md-10'><div class='form-inline'><input name='pre[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"'+iseng+'"'+")'></span></div></div></div>");
        }else{
            var okeh = cobayah-1;
            var iseng = "tambahan" + okeh;
            $("#"+iseng).after("<div class='row' id='tambahan"+cobayah+"'><div class='col-md-10'><div class='form-inline'><input name='pre[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"tambahan'+cobayah+'"'+")'></span></div></div></div>");
        };
            //nilai +=1;
         }else{
            $('#jajal').after("<div class='row' id='tambahan'><div class='col-md-10'><div class='form-inline'><input name='pre[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"'+"tambahan"+'"'+")'></span></div></div></div>");
        };

        $('.tglPelaksanaan').datepicker({
            format: 'yyyy-mm-dd'
        });
    });
    </script>
</body>
</html> 










    <?php else: ?>



<!DOCTYPE html>
<html>

<?php 
$page = 'penawaran';
include_once 'header.php'; 

?>

<body>



<?php 

include_once 'sidebar.php';

include_once 'top-nav.php';

 ?>


<div class="container" id="isi">
    <div class="page-header" id="top-logo">
        <h3>Permintaan Order Client</h3>
    </div>
    <?php 
        
        include_once '../model/Pendaftar.php'; 
        include_once '../model/page.php';
        $myPage = new Page();
        $pendaftar = new Pendaftar();
        //$myRecord = $petugas->selectByID($_SESSION['admin']['username']);
        
    ?>


    <?php if (isset($_GET['app'])&&isset($_GET['id'])&&$_GET['app']=='edit'): ?>
        <?php $datas=$pendaftar->fetchDataRequestByID($_GET['id']); ?>
    

     <?php 
        if (isset($_POST['edit'])) {

            $arrEdit = array(
                                'namaUsaha' => $_POST['edit']['np'],
                                'usaha' => $_POST['edit']['jenisUsaha'],
                                'cp' => $_POST['edit']['namaAnda'],
                                'telepon' => $_POST['edit']['teleponAnda'],
                                'email' => $_POST['edit']['email'],
                                'jabatan' => $_POST['edit']['jabatan'],
                                'pengguna' => $_POST['edit']['jenisPengguna'],
                                'versi' => '',
                                'agenda' => implode(" # ", $_POST['edit']['agendaTraining']),
                                'tanggalPelaksanaan' => implode(" # ", $_POST['edit']['tanggalPelaksanaan']),
                                'waktu' => $_POST['edit']['waktu'],
                                'tempat' => $_POST['edit']['tempat'],
                                'alamat' => $_POST['edit']['tempatTraining'],
                                'tempatBeli' => '',
                                'sales' => $_POST['edit']['sales'],
                                'id' => $_POST['edit']['id']
                             );
            $objek=(object) $arrEdit;
            //echo "ID: ".$_POST['edit']['id']."<br>";
            //echo "Nama Usaha: ".$_POST['edit']['np']."<br>";
            //echo "Jenis Usaha: ".$_POST['edit']['jenisUsaha']."<br>";
            //echo "Nama Personal Kontak: ".$_POST['edit']['namaAnda']."<br>";
            //echo "Telepon: ".$_POST['edit']['teleponAnda']."<br>";
            //echo "Email: ".$_POST['edit']['email']."<br>";
            //echo "Jabatan: ".$_POST['edit']['jabatan']."<br>";
            //echo "Jenis Pengguna: ".$_POST['edit']['jenisPengguna']."<br>";
            //echo "Tanggal Pelaksanaan: ".implode(" # ", $_POST['edit']['tanggalPelaksanaan'])."<br>";
            //echo "Waktu: ".$_POST['edit']['waktu']."<br>";
            //echo "Tempat Training: ".$_POST['edit']['tempatTraining']."<br>";
            //echo "Agenda Training: ".implode(" # ", $_POST['edit']['agendaTraining'])."<br>";
          if ($pendaftar->editDataRequest($objek)){
            echo "<script>alert('Data berhasil di ubah');window.location.replace('order-request');</script>";
          }else{
            echo "<script>alert('Data gagal di ubah');</script>";
          }
        }

     ?>


<form action='' method='POST'>
        <div class='row'>
            <div class='form-group'>
                <div class='col-md-10'>
                    <label>Nama Perusahaan</label>
                    <input type="text" name="edit[id]" style="display:none" value=<?php echo "'".$datas->id."'"; ?>>
                    <input class='form-control' name='edit[np]' type='text' value=<?php echo "'".$datas->nama_perusahaan."'"; ?> >
                </div>
            </div>
        </div>
        <br>

        <div class='row'>
            <div class='form-group'>
                <div class='col-md-10'>
                    <label>Deskripsi Usaha</label>
                    <textarea class='form-control' name='edit[jenisUsaha]' placeholder='Jelaskan kepada kami seperti apa usaha anda, contoh: perusahaan saya bergerak di pertambangan'><?php echo $datas->usaha;?></textarea>
                </div>
            </div>
        </div>
        <br>

        <div class='row'>
            <div class='form-group'>
                <div class='col-md-10'>
                    <label>Nama Personal Kontak</label>
                    <input class='form-control' name='edit[namaAnda]' type='text' value=<?php echo "'$datas->contact_person'"; ?> >
                </div>
            </div>
        </div>
        <br>

        <div class='row'>
            <div class='form-group'>
                <div class='col-md-10'>
                    <label>Telepon Anda</label>
                    <input class='form-control' name='edit[teleponAnda]' type='text' value=<?php echo "'$datas->telepon'"; ?> >
                </div>
            </div>
        </div>
        <br>

        <div class='row'>
            <div class='form-group'>
                <div class='col-md-10'>
                    <label>Email</label>
                    <input class='form-control' name='edit[email]' type='email' value=<?php echo "'$datas->email'"; ?>/>
                </div>
            </div>
        </div>
        <br>

        <div class='row'>
            <div class='form-group'>
                <div class='col-md-10'>
                    <label>Jabatan anda di perusahaan</label>
                    <input class='form-control' name='edit[jabatan]' type='text' value=<?php echo "'$datas->jabatan'"; ?> />
                </div>
            </div>
        </div>
        <br>

        <div class='row'>
            <div class='form-group'>
                <div class='col-md-10'>
                    <label>Jenis Pengguna</label>
                    <select name='edit[jenisPengguna]' class='form-control'>
                        <?php echo "<option selected value='$datas->pengguna'>".ucfirst($datas->pengguna)."</option>"; ?>
                        <option value='baru'>Tidak Pernah</option>
                        <option value='lama'>Ya, Saya pernah pakai software Accurate</option>
                        <option value='AccS'>Saya ingin jasa accounting service</option>
                    </select>
                </div>
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-md-10">
                <h4>Tanggal Pelaksanaan</h4>
            </div>
        </div>

        <?php $dataJadwal = json_decode($datas->data_jadwal) ?>
        
        <?php $tanggal = explode(' # ', $dataJadwal->tanggal) ?>
        <?php for ($i=0; $i < count($tanggal); $i++) { ?>
        <div class="row" <?php echo "id='tanggal-$i'" ?>>
            <div class="col-md-10">
                    <div class="form-inline" >
                    <input type="text" id="tgl-pelaksanaan" name='edit[tanggalPelaksanaan][]' class="form-control tglPelaksanaan" placeholder="klik disini untuk memilih tanggal" value=<?php echo "'".$tanggal[$i]."'"; ?>/>   
                    <span class='btn btn-danger glyphicon-minus' <?php echo "onclick='removeElement(".'"tanggal-'.$i.'"'.")'"; ?>></span>
                    </div>
            </div>
        </div>
        <?php } ?>

        <div class="row" id='jajal'>
            <div class="col-md-10">
                    <div class="form-inline" >
                        <input type="text" id="tgl-pelaksanaan" name='edit[tanggalPelaksanaan][]' class="form-control tglPelaksanaan" placeholder="klik disini untuk memilih tanggal"/>   
                            <span class='btn btn-sm btn-primary glyphicon glyphicon-plus' id='tambahTombol'></span>
                    </div>
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-md-10">
                <label for="">Waktu</label>
                    <div class="form-inline" >
                        <input type="text"  name='edit[waktu]' class="form-control" placeholder="format: 09:00-17:00" value=<?php echo "'".$datas->waktu."'"; ?> /> 
                    </div>
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-md-10">
                <label for="">Tempat</label>
                    <div class="form-inline" >
                        <input type="text"  name='edit[tempat]' class="form-control" value=<?php echo "'".$datas->tempat."'"; ?> /> 
                    </div>
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-md-10">
                <label for="">Sales</label>
                    <div class="form-inline" >
                        <input type="text"  name='edit[sales]' class="form-control" value=<?php echo "'".$datas->sales."'"; ?> /> 
                    </div>
            </div>
        </div>
        <br>        

        <div class="row" >
            <div class="col-md-10">
                <label for="">Lokasi Training/Jasa Accounting Service Diadakan</label>
                 <textarea type="text" name='edit[tempatTraining]' class="form-control" placeholder="Alamat tempat training di adakan, bisa di isi dengan pin Google Map"><?php echo "$datas->alamat"; ?></textarea>
            </div>
        </div>
        <br>        
        <?php $arrAgenda =explode(' # ', $datas->agenda_training)  ?>
        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                <h4>Agenda Training</h4>
                    <?php if (in_array('training fitur accurate', $arrAgenda)||$datas->agenda_training=='training fitur accurate'): ?>
                    <div class="checkbox"><label><input type="checkbox" name="edit[agendaTraining][]" value="training fitur accurate" checked>Training Fitur ACCURATE</label></div>
                        <?php else: ?>
                    <div class="checkbox"><label><input type="checkbox" name="edit[agendaTraining][]" value="training fitur accurate">Training Fitur ACCURATE</label></div>
                    <?php endif ?>

                    <?php if (in_array('setup database accurate', $arrAgenda)||$datas->agenda_training=='setup database accurate'): ?>
                    <div class="checkbox"><label><input type="checkbox" name="edit[agendaTraining][]" value="setup database accurate" checked>Setup Database ACCURATE</label></div> 
                        <?php else: ?>
                    <div class="checkbox"><label><input type="checkbox" name="edit[agendaTraining][]" value="setup database accurate">Setup Database ACCURATE</label></div>
                    <?php endif ?>
                    
                    <?php if (in_array('troubleshoot', $arrAgenda)||$datas->agenda_training=='troubleshoot'): ?>
                    <div class="checkbox"><label><input type="checkbox" name="edit[agendaTraining][]" value="troubleshoot" checked>Trouble Shooting ACCURATE</label></div>  
                        <?php else: ?>
                    <div class="checkbox"><label><input type="checkbox" name="edit[agendaTraining][]" value="troubleshoot">Trouble Shooting ACCURATE</label></div>
                    <?php endif ?>
                    
                    <?php if (in_array('review', $arrAgenda)||$datas->agenda_training=='review'): ?>
                    <div class="checkbox"><label><input type="checkbox" name="edit[agendaTraining][]" value="review" checked>Review Penggunaan ACCURATE</label></div>
                        <?php else: ?>
                    <div class="checkbox"><label><input type="checkbox" name="edit[agendaTraining][]" value="review">Review Penggunaan ACCURATE</label></div>
                    <?php endif ?>
                    
                    <div class="form-inline">
                        

                        <?php                             
                                for ($i=0; $i < count($arrAgenda); $i++) { 
                                    
                                    if ($arrAgenda[$i]!='training fitur accurate'&&$arrAgenda[$i]!='setup database accurate'&&$arrAgenda[$i]!='troubleshoot'&&$arrAgenda[$i]!='review'&&$arrAgenda[$i]!='lainnya') {
                                        echo "<input type='text' placeholder='lainnya' style='width:50%' class='form-control' name='edit[agendatraining][]' id='agenda-lainnya' value='".$arrAgenda[$i]."'>";
                                        $apaAda=true;
                                    }
                                }
                            
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <br>        

        <button class='btn btn-lg btn-primary'>Submit</button>
        </form>



    <?php endif ?>



    

</div><!--end container-->
    <script type="text/javascript" src=<?php echo "'".getBaseUrl()."css/multiselect/js/bootstrap-multiselect.js'"; ?>></script>
    <script type="text/javascript" src=<?php echo "'".getBaseUrl()."css/datepicker/js/bootstrap-datepicker.js'"; ?>></script>
    <script type="text/javascript">

    $(document).ready(function(){
        $('.tglPelaksanaan').datepicker({
            format: 'yyyy-mm-dd'
        });
    });


    function removeElement(id) {
        $('#'+id).remove();
    }

        $('#tambahTombol').click(function(){
    
            var cobayah = $('.tglPelaksanaan').length;
            var iseng = "tambahan" + cobayah;
            if ($('#tambahan').length){

        if (cobayah==2) {
            $('#tambahan').after("<div class='row' id='"+iseng+"'><div class='col-md-10'><div class='form-inline'><input name='edit[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"'+iseng+'"'+")'></span></div></div></div>");
        }else{
            var okeh = cobayah-1;
            var iseng = "tambahan" + okeh;
            $("#"+iseng).after("<div class='row' id='tambahan"+cobayah+"'><div class='col-md-10'><div class='form-inline'><input name='edit[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"tambahan'+cobayah+'"'+")'></span></div></div></div>");
        };
            //nilai +=1;
         }else{
            $('#jajal').after("<div class='row' id='tambahan'><div class='col-md-10'><div class='form-inline'><input name='edit[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"'+"tambahan"+'"'+")'></span></div></div></div>");
        };

        $('.tglPelaksanaan').datepicker({
            format: 'yyyy-mm-dd'
        });

        $('.tglPelaksanaanz').datepicker({
            format: 'yyyy-mm-dd'
        });
    });
    </script>
</body>
</html> 




<?php endif ?>



