<?php 
session_start();
if (!isset($_SESSION['admin'])||($_SESSION['admin']['tipe']!='admin'&&$_SESSION['admin']['tipe']!='marketing')) {
    header('Location:../login/index.php');
}
?>
<!DOCTYPE html>
<html>


<?php 

$page = 'home';
include_once 'header.php'; 

?>

<body>



<?php 
include_once '../model/Pendaftar.php';
include_once 'sidebar.php';

include_once 'top-nav.php';
$pendaftar = new Pendaftar();
if(isset($_POST['in'])){

    $id = substr(md5($_POST['in']['namaPaket'].$_POST['in']['harga']), 0,12);

    $input = array(
            'id' => $id, 
            'namaPaket' => $_POST['in']['namaPaket'], 
            'harga' => str_replace(',', '', $_POST['in']['harga']), 
            'paketHari' => $_POST['in']['paketHari'], 
    );
    $input = (object)$input;
    
    if ($pendaftar->inputDataPaket($input)) {
        echo "<script>alert('Data berhasil disimpan!');</script>";
    }else{
        echo "<script>alert('Data gagal disimpan!');</script>";
    }



    /*
    echo "ID: ".$input->id."<br>";
    echo "Nama Paket: ".$input->namaPaket."<br>";
    echo "Harga: ".$input->harga."<br>";
    echo "Paket Hari: ".$input->paketHari."<br>";*/


}

if (isset($_GET['ac'])&&$_GET['ac']=='edit'&&isset($_GET['id'])&&!empty($_GET['id'])&&isset($_GET['kode'])&&!empty($_GET['kode'])) {
    if ($pendaftar->editStatusPaket($_GET['kode'],$_GET['id'])) {
        echo "<script>alert('Status berhasil di ubah');</script>";
        echo "<script>location.replace('table?ac=nppi');</script>";
    }else{
        echo "<script>alert('Status berhasil di ubah');</script>";
        echo "<script>location.replace('table?ac=nppi');</script>";
    }
    
}

 ?>


<div class="container" id="isi">
	<div class="page-header" id="top-logo">
		<h3>Input Paket Baru</h3>
	</div>
    <div class="row">
        <div class="col-md-12">
            <a href="table?ac=nppi" class="btn btn-info">Ubah status Paket</a>
        </div>
    </div>
    <br>

<form action="" method="POST">
    <div class="row">
        <div class="col-md-12">
            
            <div class="row">
            	<div class="form-group col-md-10">
            		<label>Nama Paket</label>
            		<input class="form-control" name="in[namaPaket]"/>
            	</div>
            </div>
            <br>
            <div class="row">
                <div class="form-group col-md-10">
                    <label>Harga</label>
                    <input class="form-control" name="in[harga]" id="txtHarga" maxlength="10" />
                </div>
            </div>
            <br>
            <div class="row">
                <div class="form-group col-md-10">
                    <label>Paket Hari</label>
                    <input class="form-control" name="in[paketHari]" maxlength="2" id="paketHari" />
                </div>
            </div>

            <div class="row">
            	<div class="form-group col-md-10">
            	<button class="btn btn-danger" style="width:100%">Submit</button>
            	</div>
            </div>
        </div>
    </div>
    
</form>
</div>


<script type="text/javascript" src="<?php echo getBaseUrl() ?>assets/jqnumber/jquery.number.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#txtHarga').number(true, 0);
        $('#paketHari').number(true, 0);
    });
</script>     
</body>
</html> 