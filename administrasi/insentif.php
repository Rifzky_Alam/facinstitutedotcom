<?php 
session_start();
if (!isset($_SESSION['admin']['username'])||@$_SESSION['admin']['tipe']!='admin') {
	header('location:https://fac-institute.com/administrasi/accessdenied');
}
include_once $_SERVER['DOCUMENT_ROOT'].'/administrasi/controller/intensif.controller.php';
if (isset($_SERVER['PATH_INFO'])) {
	$_SERVER['PATH_INFO'] = str_replace('.php', '', $_SERVER['PATH_INFO']);
	@$url_segment = explode('/', trim($_SERVER['PATH_INFO'],'/'));
	$key = array_shift($url_segment);
	switch ($key) {
		case 'dasbor':
			Intensif::Dasbor();
			break;
		//list config
		case 'config-jnstransaksi':
			Intensif::TabelJenisTransaksi();
			break;
		case 'config-leveltrainer':
			Intensif::DataLevelTrainer();
			break;
		case 'config-jenisaccurate':
			Intensif::DataJenisAccurate();
			break;
		case 'config-harikerja':
			Intensif::DaftarTariffHariKerja();
			break;
		case 'config-harlibnas':
			Intensif::TabelHarlibnas();
			break;
		case 'config-dawil':
			Intensif::TabelDawil();
			break;
		case 'config-tbldawil':
			Intensif::TabelDaftarDawil();
			break;
		case 'config-peran':
			Intensif::PeranTable();
			break;
		case 'config-jeniskegiatan':
			Intensif::SettingJenisKegiatan();
			break;
		// list input
		case 'new-leveltrainer':
			if (isset($_POST['in'])) {
				Intensif::NewLevelTrainerPost($_POST['in']);	
			} else {
				Intensif::NewLevelTrainer();	
			}
			break;
		case 'new-jenisaccurate':
			if (isset($_POST['in'])) {
				Intensif::NewJenisAccuratePost($_POST['in']);
			} else {
				Intensif::EditJenisAccurate();	
			}
			break;
		case 'new-dawil':
			if (isset($_POST['in'])) {
				Intensif::NewDawilPost($_POST['in']);	
			} else {
				Intensif::NewDawil();	
			}
			break;
		case 'new-daftardawil':
			if (isset($_POST['in'])) {
				Intensif::NewDaftarDawilPost($_POST['in']);	
			} else {
				Intensif::NewDaftarDawil();	
			}
			break;
		case 'new-harikerja':
			if (isset($_POST['in'])) {
				Intensif::NewHariKerjaPost($_POST['in']);	
			} else {
				Intensif::NewHariKerja();	
			}
			break;
		case 'new-peran':
			if (isset($_POST['in'])) {
				Intensif::NewPeranPost($_POST['in']);
			} else {
				Intensif::NewPeran();
			}
			break;
		case 'new-harlibnas':
			if (isset($_POST['in'])) {
				Intensif::NewHarlibnasPost($_POST['in']);
			} else {
				Intensif::NewHarlibnas();
			}
			break;
		case 'new-jnstrans':
			if (isset($_POST['in'])&&!empty($_POST['in'])) {
				Intensif::NewJenisTransaksiPost($_POST['in']);
			} else {
				Intensif::NewJenisTransaksi();	
			}
			break;
		//edit
		case 'edit-jnstrans':
			if (isset($url_segment[0])) {
				if (isset($_GET['st'])) {
					Intensif::EditStatusJnsTrans($url_segment[0],$_GET['st']);
				}
			}
			break;
		case 'edit-jnskegiatan':
			Intensif::EditJenisKegiatan($url_segment[1],$url_segment[0]);
			break;
		case 'edit-statuslvltrainer':
		// echo 'ok';
			if (isset($url_segment[0])) {
				if (isset($_GET['st'])) {
					Intensif::EditLevelTrainer($url_segment[0],$_GET['st']);
				}
			}
			break;
		case 'edit-statusjnsaccurate':
			if (isset($url_segment[0])) {
				if (isset($_GET['st'])) {
					Intensif::EditStatusJnsAccurate($url_segment[0],$_GET['st']);
				}
			}
			break;
		case 'edit-statusjnshr':
			if (isset($url_segment[0])) {
				if (isset($_GET['st'])) {
					Intensif::EditStatusHariKerja($url_segment[0],$_GET['st']);
				}
			}
			break;
		case 'edit-statusperan':
			if (isset($url_segment[0])) {
				if (isset($_GET['st'])) {
					Intensif::EditStatusPeran($url_segment[0],$_GET['st']);
				}
			}
			break;
		case 'edit-statusdawil':
			if (isset($url_segment[0])) {
				if (isset($_GET['st'])) {
					Intensif::EditStatusDawil($url_segment[0],$_GET['st']);
				}
			}
			break;
		case 'deleteharlibnas':
			if (isset($_GET['id'])&&!empty($_GET['id'])) {
				Intensif::HapusHarlibnas($_GET['id']);
			}
			break;
		case 'deldawil':
			if (isset($_GET['id'])&&!empty($_GET['id'])) {
				Intensif::DeleteDawil($_GET['id']);
				// echo 'oke';
			}
			break;
		// summary as of date and username
		case 'summary':
			
			break;
		case 'summarystaff':
			// as of session_username
			break;
		// detail as of id_laporanharian
		case 'summary-detail':
			
			break;
		// download as of search
		case 'download':
			
			break;
		// API total insentif as of current month and last month
		case '':
			header('Location: https://fac-institute.com/administrasi/');
			break;
	}
}