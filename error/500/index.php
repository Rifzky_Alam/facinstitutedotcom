<?php include_once '../../baseurl.php'; ?>



<!DOCTYPE html>
<html>
<head>
	<meta name="author" content="Rifzky Alam">
	<title>FAC :: Internal Server Error</title>
	<link rel="stylesheet" type="text/css" href="<?php echo getBaseUrl().'assets/bootstrap/css/bootstrap.css' ?>">

	<style type="text/css">
		.table-user-information > tbody > tr {
    		border-top: 1px solid rgb(221, 221, 221);
		}		

		.table-user-information > tbody > tr:first-child {
		    border-top: 0;
		}

		.table-user-information > tbody > tr > td {
		    border-top: 0;
		}

		.tengah{
			text-align:center;
		}
	</style>

</head>
<body>
	<div class="container-fluid" style="padding-left:0px;padding-right:0px;">
		<div class="row">
			<div class="col-md-12">
				<div class="jumbotron">
					<h1 class="tengah">Error 500 : Server sibuk, kami akan segera kembali!</h1>
					<p class="tengah">
						Server <a href="https://fac-institute.com">www.fac-institute.com</a> sedang ada perbaikan, kami akan segera kembali dalam beberapa waktu.
					</p>
					<div class="tengah">
						<a href="https://fac-institute.com" class="btn btn-lg btn-primary">Kembali ke Halaman Utama</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-info">
  					<strong>Informasi :</strong> Jika anda masih mendapatkan masalah mengakses laman website kami, silahkan untuk menghubungi web developer kami:	
  				</div>

			</div>
		</div>

		<div class="row">
			<div class="page-header">
				<h3 class="tengah">Our Site Developer</h3>
			</div>
		</div>

		<div class="row">


			<div class="col-md-4">
				<div class="panel panel-danger">
					<div class="panel-heading" style="background-color:#e60000;color:#f9f3f3;">
						<h3 class="panel-title">Rifzky Alam</h3>					
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-3 col-lg-3 " align="center"> 
								<img alt="User Pic" src="<?php echo getBaseUrl().'images/ourteam/avatar-rifzky-300x300.png' ?>" class="img-circle img-responsive"> 
							</div>
							<div class=" col-md-9 col-lg-9 ">
								<table class="table table-user-information">
									<tbody>
										<tr>
											<td>Name:</td>
											<td>Rifzky Alam</td>
										</tr>
										<tr>
											<td>Department:</td>
											<td>Full Stack Developer</td>
										</tr>
										<tr>
											<td>Phone Number:</td>
											<td><a href="tel://+6281279222250">+6281279222250</a></td>
										</tr>
										<tr>
											<td>Gender:</td>
											<td>Male</td>
										</tr>
										<tr>
											<td>Home Address:</td>
											<td>Pondok Aren - North Bogor, West Java 16156</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				<div class="panel-footer" style="text-align:right;">
                    <a href="https://www.facebook.com/zeke.rifzky.alam" target="_blank">
                    	<img style="width:20px;height:20px;" src="https://fac-institute.com/images/socialicon/facebook-black.png">
                    </a>
                    <a href="https://twitter.com/Rezt_zek3" target="_blank">
                    	<img style="width:20px;height:20px;" src="https://fac-institute.com/images/socialicon/twitter-black.png">
                    </a>
                    <a href="https://www.linkedin.com/in/rifzky-alam-005874a7" target="_blank">
                    	<img style="width:20px;height:20px;" src="https://fac-institute.com/images/socialicon/linkedin-black.png">
                    </a>
                </div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">Dino Damara</h3>					
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-3 col-lg-3 " align="center"> 
								<img alt="User Pic" src="<?php echo getBaseUrl().'images/ourteam/avatar-1-300x300.png' ?>" class="img-circle img-responsive"> 
							</div>
							<div class=" col-md-9 col-lg-9 ">
								<table class="table table-user-information">
									<tbody>
										<tr>
											<td>Name:</td>
											<td>Dino Damara Pratama</td>
										</tr>
										<tr>
											<td>Department:</td>
											<td>Front-End Developer</td>
										</tr>
										<tr>
											<td>Phone Number:</td>
											<td>-</td>
										</tr>
										<tr>
											<td>Gender:</td>
											<td>Male</td>
										</tr>
										<tr>
											<td>Home Address:</td>
											<td>Depok</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>

					<div class="panel-footer" style="text-align:right;">
	                    <a href="https://www.facebook.com/zeke.rifzky.alam" target="_blank">
	                    	<img style="width:20px;height:20px;" src="https://fac-institute.com/images/socialicon/facebook-black.png">
	                    </a>
	                    <a href="https://twitter.com/Rezt_zek3" target="_blank">
	                    	<img style="width:20px;height:20px;" src="https://fac-institute.com/images/socialicon/twitter-black.png">
	                    </a>
	                    <a href="https://www.linkedin.com/in/rifzky-alam-005874a7" target="_blank">
	                    	<img style="width:20px;height:20px;" src="https://fac-institute.com/images/socialicon/linkedin-black.png">
	                    </a>
                	</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="panel panel-success">
					<div class="panel-heading">
						<h3 class="panel-title">Ibnu Muzakkir</h3>					
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-3 col-lg-3 " align="center"> 
								<img alt="User Pic" src="<?php echo getBaseUrl().'images/ourteam/avatar-1-300x300.png' ?>" class="img-circle img-responsive"> 
							</div>
							<div class=" col-md-9 col-lg-9 ">
								<table class="table table-user-information">
									<tbody>
										<tr>
											<td>Name:</td>
											<td>Ibnu Muzakkir</td>
										</tr>
										<tr>
											<td>Department:</td>
											<td>Freelance Developer</td>
										</tr>
										<tr>
											<td>Phone Number:</td>
											<td>-</td>
										</tr>
										<tr>
											<td>Gender:</td>
											<td>Male</td>
										</tr>
										<tr>
											<td>Home Address:</td>
											<td>Tangerang</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="panel-footer" style="text-align:right;">
	                    <a href="https://www.facebook.com/zeke.rifzky.alam" target="_blank">
	                    	<img style="width:20px;height:20px;" src="https://fac-institute.com/images/socialicon/facebook-black.png">
	                    </a>
	                    <a href="https://twitter.com/Rezt_zek3" target="_blank">
	                    	<img style="width:20px;height:20px;" src="https://fac-institute.com/images/socialicon/twitter-black.png">
	                    </a>
	                    <a href="https://www.linkedin.com/in/rifzky-alam-005874a7" target="_blank">
	                    	<img style="width:20px;height:20px;" src="https://fac-institute.com/images/socialicon/linkedin-black.png">
	                    </a>
                	</div>
				</div>
			</div>




		</div>

	</div>



</body>
</html>