<? ob_start("ob_gzhandler"); ?>
<?php
    $value = 'visitor';
    setcookie("pengguna", $value,time()+600);
?>

<!DOCTYPE html>

<html lang="in">
<head>
<link rel="alternate" hreflang="in" href="https://www.fac-institute.com/" />
<meta name="google-site-verification" content="EfNVtXecxElbcbugY59SeKXJuMzO67bjVvTvlHCc_CM" />
<meta name="msvalidate.01" content="D925497D8F382164FF0CE0267D381FDA" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta content="text/html;charset=utf-8" http-equiv="Content-Type">

	<meta name="keywords" content="Training,Akuntansi,Accurate,Jasa">
	<meta name="author" content="Rifzky Alam And Ibnu Muzzakkir">


<meta property="og:site_name" content="FAC INSTITUTE" />
<meta property="og:title" content="First Asian Consulting. Konsultan Akuntansi & Lembaga Pendidikan Komputerisasi Akuntansi, Kami Melayani Training Software Akuntansi Accurate dan Accounting Service Untuk Perusahaan Anda" />
<meta property="og:image" content="https://fac-institute.com/images/logo-fac_header.png" />
<meta property="og:description" content="First Asian Consulting. Konsultan Akuntansi & Lembaga Pendidikan Komputerisasi Akuntansi, Kami Melayani Training Software Akuntansi Accurate dan Accounting Service Untuk Perusahaan Anda" />
<meta property="og:url" content=<?php echo '"https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'"'; ?> />
<meta property="fb:app_id" content="454421948087465" />
<meta property="fb:admins" content="454421948087465" />
<meta property="og:image:type" content="image/jpeg" />
<meta property="og:image:width" content="650" />
<meta property="og:image:height" content="366" />
<meta name="thumbnailUrl" content="https://www.fac-institute.com/administrasi/artikel/images/7cabb38e259c0d5b53f9bb219daf410f.jpg?w=650" />
<meta name="description" content="FAC - Kami Melayani Training Software Akuntansi Accurate dan Accounting Service Untuk Perusahaan Anda" />
<meta content="First Asian Consulting. Konsultan Akuntansi & Lembaga Pendidikan Komputerisasi Akuntansi"/>

	<title>FAC Institute</title>
	<?php include_once 'links.php'; ?>

	<?php if (isset($_GET['page'])&&$_GET['page']=='daftar') {
		echo "<link rel='stylesheet' type='text/css' href='js/jquery-ui/jquery-ui.min.css'>";
	} ?>
	<style type="text/css">
		.awesome-container { font-size: 120% }
     	.awesome-container { width: 50% }
		html,body{
			height: 100%;
		}
		body {
    		padding-bottom: 20px;
		}

		#wrapper{
			min-height: 100%;
			/*background-image: url('images/background.jpg');
			background-repeat: no-repeat;
			background-size: cover;
			background-color: snow;*/
			margin-bottom: 40px;
		}

		#visi>li{
			list-style-type: none;
			font-size: 16px;
		}

		#visi>li:before{
			content: "\e127";
			font-family: 'Glyphicons Halflings';
			font-size:12px;
			float: left;

			margin-left: -17px;
			color: black;
		}

		#misi>li{
			list-style-type: none;
			line-height: 2em;
			font-size: 16px;
		}

		#misi>li:before{
			content: "\e013";
			font-family: 'Glyphicons Halflings';
			font-size: 12px;
			float: left;
			margin-right: 5px;
			margin-left: -17px;
			color: black;
		}

		#top-navigations{
			padding-left: 50px;
			color: orange;
		}

		#top-navigations>ul>li>a{

			color: orange;
		}

		#top-brand-container{
			background-image: url('images/brand.jpg');
		}

		#top-brand{
			padding-top: 50px;
			padding-left: 30px;
			color: white;
		}

		#content{
			padding-top: 30px;


		}

		#bottom-brand{
			margin: 0px;
		}

		#content-node{
			/*padding-top: 10px;*/
		}
	#modal-welcome{z-index: 9100}
    #fl_menu{padding-top:3px;position:absolute; top:250px; left:0px; z-index:9000; width:110px; height:50px;background-color: #5C056E}
    #fl_menu .label{padding-left:15px;font-family:"ITC-Black", Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; background:#5C056E; color:#fff;}
    #fl_menu .menu{display:none;}
    #fl_menu .menu .menu_item{display:block; background:#2196F3; color:#fff; border-top:1px solid #333; padding:10px 20px; font-family:'ITC-Regular'; font-size:12px; text-decoration:none;}
    #fl_menu .menu a.menu_item:hover{background:#5C056E; color:#fff;}
	</style>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCUn6Q6sg0JVJBlgiz4tL2kF1zmVoC0ScE&libraries=places&sensor=true"></script>
</head>
<body>
	<div id="wrapper">
		<?php include 'header.php'; ?>
		<script type="text/javascript">
			jQuery(document).ready(function(){
			  jQuery('#slippry-demo').slippry()
			});
		</script>
  <?php function SectionHtml(){?>
    <section class="col-md-9" style="margin-top: 140px;">
      <div class="row" id="content-node">
        <div class="col-md-10 col-md-offset-1" style="padding-left:0;">
  <?php }?>
	<div class="container-fluid" id="content">
<script async src="//static.addtoany.com/menu/page.js"></script>
		<?php if (isset($_GET['page'])&&$_GET['page']=='home') {?>
					<div class="row" id="content-node">
					<?php include 'homebaru.php'; ?>
				</div>
			<?php } elseif(isset($_GET['page'])&&$_GET['page']=='artikel') { ?>
        <?php SectionHtml();
         include 'artikel.php';?>
				</div>
			</div>
		</section>
		<?php }  elseif(isset($_GET['page'])&&$_GET['page']=='karir') { ?>
      <?php SectionHtml();
       include 'karir.php';?>
      </div>
    </div>
  </section>

  <?php }   elseif(isset($_GET['page'])&&$_GET['page']=='detail') { ?>
    <?php SectionHtml();
     include 'detail.php';?>
    </div>
  </div>
</section>
<?php }   elseif(isset($_GET['page'])&&$_GET['page']=='tentang') { ?>
  <?php SectionHtml();
   include 'detail.php';?>
  </div>
</div>
</section>
<?php }   elseif(isset($_GET['page'])&&$_GET['page']=='daftar') { ?>
  <?php SectionHtml();
   include 'daftar-belajar.php';?>
  </div>
</div>
</section>
<?php } else { ?>
	<div class="row" id="content-node">
	<?php include 'homebaru.php'; ?>
</div>
  <?php } ?>
		<?php if(isset($_GET['page']) && $_GET['page'] =='home') {?>

		<?php } elseif(isset($_GET['page']) && ($_GET['page'] =='detail' || $_GET['page'] =='artikel' || $_GET['page'] =='tutorial' || $_GET['page'] =='karir' || $_GET['page'] == 'tentang')){ ?>
		<section class="col-md-3 sidebar-custom"  style="margin-top: 140px;">
			<?php include 'sidebar.php'; ?>
		</section>
		<?php } ?>

	</div>


	</div>

	<?php include_once 'footer.php'; ?>
	<script type="text/javascript">

		$('#top-navigations>ul>li>a').hover(function() {
			$(this).css('color','red');
		},function(){
			$(this).css('color','orange');
		});

		$('#top-navigations>ul>li').hover(function() {
			$(this).delay(2000).css('background-color','white');
		},function(){
			$(this).delay(2000).css('background-color','#222');
		});

	</script>
		<script type="text/javascript">
// create the back to top button
$('body').prepend('<a href="#" class="back-to-top">Back to Top</a>');

var amountScrolled = 300;

$(window).scroll(function() {
	if ( $(window).scrollTop() > amountScrolled ) {
		$('a.back-to-top').fadeIn('slow');
	} else {
		$('a.back-to-top').fadeOut('slow');
	}
});

$('a.back-to-top, a.simple-back-to-top').click(function() {
	$('html, body').animate({
		scrollTop: 0
	}, 700);
	return false;
});
</script>
	<?php if (isset($_GET['page'])&&$_GET['page']=='daftar') {
		echo "<script type='text/javascript' src='js/jquery-ui/jquery-ui.js'></script>";
		echo "<script type='text/javascript' src='js/daftar.js'></script>";
	} ?>


<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/576deedca4fa94c76a6d4cad/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
<script>
    //  awesomeAnalytics(); // beacon conversion metrics
</script>

</body>
</html>
