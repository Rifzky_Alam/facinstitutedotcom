<?php include_once 'baseurl.php'; ?>
<html class="no-js" lang="id">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="google-site-verification" content="EfNVtXecxElbcbugY59SeKXJuMzO67bjVvTvlHCc_CM" />
    <meta name="msvalidate.01" content="D925497D8F382164FF0CE0267D381FDA" />
    <meta name="thumbnailUrl" content="https://www.fac-institute.com/administrasi/artikel/images/7cabb38e259c0d5b53f9bb219daf410f.jpg?w=650" />
    <meta name="description" content="FAC - Kami Melayani Training Software Akuntansi Accurate dan Accounting Service Untuk Perusahaan Anda" />
    <meta content="First Asian Consulting. Konsultan Akuntansi & Lembaga Pendidikan Komputerisasi Akuntansi" />
    <meta name="keywords" content="Training,Akuntansi,Accurate,Jasa">
    <meta name="author" content="Rifzky Alam, Dino Damara">
    <title>FAC Institute</title>
    <link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/w3.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/animate.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,100,300,500,600,700c" type="text/css" />
 <style>
 .w3-top{
   font-family: "Raleway", sans-serif;
 }
 body, html, h3 {
     font-family: 'PT Sans', sans-serif;
 }
 p,li{
   font-size: 16px;
 }
 .leftside {
   text-align: center;
 }
 .homebox {
   width: 100%;
   float: center;
   margin-bottom: 10px;
   background: #fff;
   overflow: hidden;
 }
 #icons {
   list-style: none;
   margin: 0;
   padding: 0;
   text-align: center
 }
 #icons a {
   text-decoration: none;
 }
 #icons li {
   display: inline-block;
   width: 100px;
   height: 100px;
   /*border: 1px solid #ccc;*/
   margin: 0 7px 7px 0;
 }
 #icons li:hover {
 }
 #icons li img {
   /*padding: 8px 18px 2px 18px;*/
   text-align: center;
   width: 100%;
 }
 #icons li h6 {
   color: #333;
   font-size: 12px;
   text-align: center;
   margin: 0;
   padding: 0;
 }
 .mySlides {display:none;}

 .featureList, .featureList ul {
   list-style-type: none;
 }
 .featureList li:before {
   position: absolute;
   margin-left: -1.3em;
   font-weight: bold;
 }
 .featureList li.tick:before {
   content: "\2713";
   color: darkgreen;
 }
 .featureList li.cross:before {
   content: "\2717";
   color: crimson;
 }
 img {margin-bottom: -7px}
 .w3-row-padding img {margin-bottom: 12px}

 li::before {
  content: "• ";
  color: black; /* or whatever color you prefer */
}

li {
  padding-left: 1em;
  text-indent: -.7em;
}
 </style>


<link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/style.css">


</head>
<body>
    <div id="back-top">
        <a style="font-weight: bold" href="#top">
            <img src="<?= getBaseUrl() ?>_caramel/assets/img/to-top@2x.png" />
        </a>
    </div>

    <h4>header</h4>
    <?php include_once 'top-nav.php'; ?>

    <header class="w3-display-container w3-content w3-center" style="max-width:1500px">
      <img class="w3-image" src="<?php echo getBaseUrl() ?>_caramel/assets/img/Untitled design_.jpg" alt="Me" width="1500" height="600">
      <div class="w3-display-middle w3-padding-large w3-border w3-wide w3-text-light-grey w3-center">
        <h1 class="w3-hide-medium w3-hide-small w3-xxxlarge">FAC</h1>
        <h5 class="w3-hide-large" style="white-space:nowrap">FAC</h5>
        <h3 class="w3-hide-medium w3-hide-small">INSTITUTE</h3>
      </div>
      <div class="w3-display-bottommiddle w3-container w3-text-white w3-padding-32 w3-hide-small">
     <p style="font-size:20px;font-weight:light">Konsultan akuntansi & lembaga pendidikan komputerisasi akuntansi.</p>
    </div>
    </header>





<!-- Promo Section - "We know design" -->
<div class="w3-container w3-light-grey" style="padding:28px 16px">
  <div class="w3-row-padding">
    <div class="w3-col m4">
      <img class="w3-image" src="<?= getBaseUrl() ?>_caramel/assets/img/b.png" style="width:100%">
    </div>
    <div class="w3-col m8">
      <h3 style="font-weight:bold">Training Accurate</h3>
      <ul style="font-size:14px" class="w3-ul">
      <li>Kami <b>Accurate Authorized Training Center</b> (AATC) yang resmi mengirimkan tenaga trainer ke lokasi perusahaan anda.</li>
      <li>Kami mengirim trainer yang berpengalaman ke perusahaan anda.</li>
      <li>Kami menyediakan jadwal training sesuai kesepakatan.</li>
      <li>Kami melaksanakan training siklus Software ACCURATE dari A sampai Z.</li>
      <li>Kami melatih para pengguna ACCURATE sesuai kadar kemampuan para staff di perusahaan Anda.</li>
      <li>Kami memberikan free support after training via Whatsapp & E-mail pada jam kerja </li>
      <li>Kami memberikan premium support via Team Viewer</li>
      </ul>

    <div style="padding-left:16px" class="w3-text-white">
     <p><a href="#about" class="w3-button w3-blue ">Informasi Selengkapnya</a></p>
    </div>

    </div>


  </div>
</div>

<div class="w3-container" style="padding:28px 16px">
  <div class="w3-row-padding">
    <div class="w3-col m4">
      <img class="w3-image" src="<?= getBaseUrl() ?>_caramel/assets/img/b.png" style="width:100%">
    </div>
    <div class="w3-col m8">
      <h3 style="font-weight:bold" >Kelas Kursus Accurate</h3>
      <ul style="font-size:14px" class="w3-ul">
      <li>Kami Accurate Authorized Training Center (AATC) yang mendapatkan izin resmi untuk menyelenggarakan kelas kursus accurate</li>
      <li>Kami menciptakan metode pembelajaran Kursus ACCURATE tanpa Anda harus mahir akuntansi.</li>
      <li>Peserta terbatas agar kelas kondusif dan efektif</li>
      <li>Peserta bisa mengulang gratis di kelas selanjutnya</li>
      <li>Mendapatkan sertifikat resmi dari accurate</li>
      <li>Kami menyediakan kursus private </li>
      <li>Kami menyediakan ujian CAP (certified accurate profesional)</li>
      <li>Peserta mendapatkan modul software accurate</li>
      </ul>
      <div style="padding-left:16px" class="w3-text-white">
       <p><a href="#about" class="w3-button w3-blue ">Informasi Selengkapnya</a></p>
      </div>
    </div>
  </div>
</div>



    <div class="w3-container">
      <div id="id01" class="w3-modal w3-animate-opacity">
        <div class="w3-modal-content w3-card-4">
          <header class="w3-container w3-teal">
            <span onclick="document.getElementById('id01').style.display='none'"
            class="w3-button w3-large w3-display-topright">&times;</span>
          </header>
          <div class="w3-container w3-padding-16 w3-margin-top">
            <img class="w3-image" src="<?php echo getBaseUrl() ?>_caramel/assets/img/libur.jpg" alt="Me" width="100%">
          </div>
          <footer class="w3-container w3-teal">
          </footer>
        </div>
      </div>
    </div>



<div class="w3-content w3-padding-16 w3-margin-top" id="portfolio">
<div class="w3-container" id="about">

</div>




<!-- Menu Container -->
<div class="w3-container" id="menu">

</div>
</div>







    <script src="<?php echo getBaseUrl() ?>assets/homepage/js/jquery-2.1.4.min.js"></script>


    <script type="text/javascript">
        function visitorDetector(window) {
            {
                var unknown = '-';

                // browser
                var nVer = navigator.appVersion;
                var nAgt = navigator.userAgent;
                var browser = navigator.appName;
                var version = '' + parseFloat(navigator.appVersion);
                var majorVersion = parseInt(navigator.appVersion, 10);
                var nameOffset, verOffset, ix;

                // Opera
                if ((verOffset = nAgt.indexOf('Opera')) != -1) {
                    browser = 'Opera';
                    version = nAgt.substring(verOffset + 6);
                    if ((verOffset = nAgt.indexOf('Version')) != -1) {
                        version = nAgt.substring(verOffset + 8);
                    }
                }
                // Opera Next
                if ((verOffset = nAgt.indexOf('OPR')) != -1) {
                    browser = 'Opera';
                    version = nAgt.substring(verOffset + 4);
                }
                // MSIE
                else if ((verOffset = nAgt.indexOf('MSIE')) != -1) {
                    browser = 'Microsoft Internet Explorer';
                    version = nAgt.substring(verOffset + 5);
                }
                // Chrome
                else if ((verOffset = nAgt.indexOf('Chrome')) != -1) {
                    browser = 'Chrome';
                    version = nAgt.substring(verOffset + 7);
                }
                // Safari
                else if ((verOffset = nAgt.indexOf('Safari')) != -1) {
                    browser = 'Safari';
                    version = nAgt.substring(verOffset + 7);
                    if ((verOffset = nAgt.indexOf('Version')) != -1) {
                        version = nAgt.substring(verOffset + 8);
                    }
                }
                // Firefox
                else if ((verOffset = nAgt.indexOf('Firefox')) != -1) {
                    browser = 'Firefox';
                    version = nAgt.substring(verOffset + 8);
                }
                // MSIE 11+
                else if (nAgt.indexOf('Trident/') != -1) {
                    browser = 'Microsoft Internet Explorer';
                    version = nAgt.substring(nAgt.indexOf('rv:') + 3);
                }
                // Other browsers
                else if ((nameOffset = nAgt.lastIndexOf(' ') + 1) < (verOffset = nAgt.lastIndexOf('/'))) {
                    browser = nAgt.substring(nameOffset, verOffset);
                    version = nAgt.substring(verOffset + 1);
                    if (browser.toLowerCase() == browser.toUpperCase()) {
                        browser = navigator.appName;
                    }
                }
                // trim the version string
                if ((ix = version.indexOf(';')) != -1) version = version.substring(0, ix);
                if ((ix = version.indexOf(' ')) != -1) version = version.substring(0, ix);
                if ((ix = version.indexOf(')')) != -1) version = version.substring(0, ix);

                majorVersion = parseInt('' + version, 10);
                if (isNaN(majorVersion)) {
                    version = '' + parseFloat(navigator.appVersion);
                    majorVersion = parseInt(navigator.appVersion, 10);
                }

                // mobile version
                var mobile = /Mobile|mini|Fennec|Android|iP(ad|od|hone)/.test(nVer);


                // system
                var os = unknown;
                var clientStrings = [{
                        s: 'Windows 10',
                        r: /(Windows 10.0|Windows NT 10.0)/
                    },
                    {
                        s: 'Windows 8.1',
                        r: /(Windows 8.1|Windows NT 6.3)/
                    },
                    {
                        s: 'Windows 8',
                        r: /(Windows 8|Windows NT 6.2)/
                    },
                    {
                        s: 'Windows 7',
                        r: /(Windows 7|Windows NT 6.1)/
                    },
                    {
                        s: 'Windows Vista',
                        r: /Windows NT 6.0/
                    },
                    {
                        s: 'Windows Server 2003',
                        r: /Windows NT 5.2/
                    },
                    {
                        s: 'Windows XP',
                        r: /(Windows NT 5.1|Windows XP)/
                    },
                    {
                        s: 'Windows 2000',
                        r: /(Windows NT 5.0|Windows 2000)/
                    },
                    {
                        s: 'Windows ME',
                        r: /(Win 9x 4.90|Windows ME)/
                    },
                    {
                        s: 'Windows 98',
                        r: /(Windows 98|Win98)/
                    },
                    {
                        s: 'Windows 95',
                        r: /(Windows 95|Win95|Windows_95)/
                    },
                    {
                        s: 'Windows NT 4.0',
                        r: /(Windows NT 4.0|WinNT4.0|WinNT|Windows NT)/
                    },
                    {
                        s: 'Windows CE',
                        r: /Windows CE/
                    },
                    {
                        s: 'Windows 3.11',
                        r: /Win16/
                    },
                    {
                        s: 'Android',
                        r: /Android/
                    },
                    {
                        s: 'Open BSD',
                        r: /OpenBSD/
                    },
                    {
                        s: 'Sun OS',
                        r: /SunOS/
                    },
                    {
                        s: 'Linux',
                        r: /(Linux|X11)/
                    },
                    {
                        s: 'iOS',
                        r: /(iPhone|iPad|iPod)/
                    },
                    {
                        s: 'Mac OS X',
                        r: /Mac OS X/
                    },
                    {
                        s: 'Mac OS',
                        r: /(MacPPC|MacIntel|Mac_PowerPC|Macintosh)/
                    },
                    {
                        s: 'QNX',
                        r: /QNX/
                    },
                    {
                        s: 'UNIX',
                        r: /UNIX/
                    },
                    {
                        s: 'BeOS',
                        r: /BeOS/
                    },
                    {
                        s: 'OS/2',
                        r: /OS\/2/
                    },
                    {
                        s: 'Search Bot',
                        r: /(nuhk|Googlebot|Yammybot|Openbot|Slurp|MSNBot|Ask Jeeves\/Teoma|ia_archiver)/
                    }
                ];
                for (var id in clientStrings) {
                    var cs = clientStrings[id];
                    if (cs.r.test(nAgt)) {
                        os = cs.s;
                        break;
                    }
                }

                var osVersion = unknown;

                if (/Windows/.test(os)) {
                    osVersion = /Windows (.*)/.exec(os)[1];
                    os = 'Windows';
                }

                switch (os) {
                    case 'Mac OS X':
                        osVersion = /Mac OS X (10[\.\_\d]+)/.exec(nAgt)[1];
                        break;

                    case 'Android':
                        osVersion = /Android ([\.\_\d]+)/.exec(nAgt)[1];
                        break;

                    case 'iOS':
                        osVersion = /OS (\d+)_(\d+)_?(\d+)?/.exec(nVer);
                        osVersion = osVersion[1] + '.' + osVersion[2] + '.' + (osVersion[3] | 0);
                        break;
                }


            }

            window.jscd = {
                browser: browser,
                browserVersion: version,
                browserMajorVersion: majorVersion,
                mobile: mobile,
                os: os,
                osVersion: osVersion
            };

            var jsonDatas = {
                'ip': <?php echo "'".$_SERVER['REMOTE_ADDR']."'"; ?>,
                'os': jscd.os + ' ' + jscd.osVersion,
                'browser': jscd.browser + ' ' + jscd.browserMajorVersion,
                'mobile': jscd.mobile
            }
            //alert(peserta);

            $.ajax({
                type: "POST",
                url: "controller/visitor",
                data: {
                    'jsonData': jsonDatas
                },
                cache: false,
                success: function(data) {
                    // alert(data);
                    //window.location.replace("index.php");
                }
            }); //end ajax

        }(this);

        $(document).ready(function() {
            visitorDetector(window);

              // hide #back-top first
              $("#back-top").hide();

              // fade in #back-top
              $(function () {
                $(window).scroll(function () {
                  if ($(this).scrollTop() > 100) {
                    $('#back-top').fadeIn();
                  } else {
                    $('#back-top').fadeOut();
                  }
                });

                // scroll body to 0px on click
                $('#back-top .fi-arrow-up').click(function () {
                  $('body,html').animate({
                    scrollTop: 0
                  }, 800);
                  return false;
                });
              });

        });
    </script>


<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Course",
  "name": "Training Software Accurate, Audit data dan Kursus Akuntansi",
  "description": "Kami mengajari, tanpa harus anda memahami akuntansi.",
  "provider": {
    "@type": "Organization",
    "name": "FAC Institute",
    "sameAs": "http://www.fac-institute.com"
  }
}
</script>

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "ProfessionalService",
  "@id": "https://fac-institute.com/",
  "name": "FAC Institute",
  "image": "https://fac-institute.com/images/logo-fac.jpg",
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "jalan no., Jl. Pangkalan Jati I A No.8, Jatiwaringin",
    "addressLocality": "Jakarta Timur",
    "addressRegion": "Jakarta",
    "postalCode": "13620",
    "addressCountry": "Indoneia"
  },
  "geo": {
    "@type": "GeoCoordinates",
    "latitude": -6.248042,
    "longitude": 106.907903
  },
  "telephone": "+6281290083983",
  "potentialAction": {
    "@type": "ReserveAction",
    "target": {
      "@type": "EntryPoint",
      "urlTemplate": "https://fac-institute.com/order",
      "inLanguage": "id",
      "actionPlatform": [
        "http://schema.org/DesktopWebPlatform",
        "http://schema.org/IOSPlatform",
        "http://schema.org/AndroidPlatform"
      ]
    },
    "result": {
      "@type": "Reservation",
      "name": "Pesan Training Software Accurate/Accounting Service"
    }
  }
}
</script>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/576deedca4fa94c76a6d4cad/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->




<script>
var myIndex = 0;
carousel();

function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";
    }
    myIndex++;
    if (myIndex > x.length) {myIndex = 1}
    x[myIndex-1].style.display = "block";
    setTimeout(carousel, 4000); // Change image every 2 seconds
}
</script>


</body>
</html>
