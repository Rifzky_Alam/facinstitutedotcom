<!--Landing Header-->
	<div itemscope itemtype="http://schema.org/Product" class="col-md-12 landing-header">
	<ul id="slippry-demo">
			<li>
				<a ><img  class="img-responsive" alt='' src="images/slider/image-1.jpg" style="opacity:0.7;" ></a>
				<div class="alt-image">
			<h2 class="alt-image-judul">Konsultan Akuntansi & Lembaga Pendidikan Komputerisasi Akuntansi</h2>
			<span class="alt-image-sub" itemprop="name">FAC Institute </span>
	</div>
			</li>
			<li>
				<a ><img  class="img-responsive" alt='' src="images/slider/image-2.jpg" style="opacity:0.7;"></a>
				<div class="alt-image">
			<h1 class="alt-image-judul">Training Software Accurate</h1>
			<span class="alt-image-sub">Dengan metodologi pengajaran yang sederhana dan atraktif</span>
		</div>
			</li>
			<li>
				<a ><img  class="img-responsive" alt='' src="images/slider/image-3.jpg "style="opacity:0.7;"></a>
				<div class="alt-image-2">
			<span class="alt-image-sub-2">Kami Berorientasi pada pemenuhan kebutuhan sumber daya tenaga akuntan yang profesional untuk Indonesia dan dunia</span>
		</div>
			</li>
			<li>
				<a ><img  class="img-responsive" alt='' src="images/slider/image-4.jpg" style="opacity:0.7;"></a>
				<div class="alt-image-2">
			<span class="alt-image-sub-2">Kami Melaksanakan pendidikan dan pelatihan komputerisasi akuntansi dengan nilai wawasan kekinian</span>
		</div>
			</li>
			<li>
				<a ><img  class="img-responsive" alt='' src="images/slider/image-5.jpg"></a>
				<div class="alt-image-2">
			<span class="alt-image-sub-2">Kami Meningkatkan nilai perusahaan melalui kreatifitas, inovasi, dan pengembangan kompetensi sumber daya tenaga pengajar dan pendidik FAC Institute</span>
		</div>
			</li>
			<li>
				<a ><img  class="img-responsive" alt='' src="images/slider/image-6.jpg"></a>
				<div class="alt-image-2">
			<span class="alt-image-sub-2">Kami Membantu para pelaku usaha di Indonesia dalam memahami kondisi bisnisnya dengan komputerisasi akuntansi yang efektif dan efisien.</span>
		</div>
			</li>
			<li>
				<a ><img  class="img-responsive" alt='' src="images/slider/image-7.jpg"></a>
				<div class="alt-image" itemscope itemtype="http://schema.org/ProfessionalService">
							<h1 class="alt-image-judul">Layanan yang diberikan</h1>
				<span itemprop="name" class="alt-image-sub">1. Training Akuntansi</span><br/>
	<span itemprop="name">2. Layanan Akuntansi dan SOP</span><br/>
	<span itemprop="name">3. Entry Data Transaksi</span><br/>
	<span itemprop="name">4. Training Software Accurate</span>
</div>
			</li>
		</ul>
	</div>
<!-- End-Landing-Header -->
<!-- Main Landing Page -->
<section>
	<div class="col-md-12 content-visimisi">
	<h2 class="judul-artikel">Layanan Kami</h2>
	<!-- View All Article inactive -->
<!-- 	<h4><a href="#">Lihat Semua Artikel</a></h4> -->
	<div class="col-md-12 artikel">

<?php

	include_once 'model/Artikel.php';
	$ctrlArtikel = new Artikel();
	$datas = json_decode($ctrlArtikel->homeFetchAll());
	//$dataSidebar = json_decode($ctrlArtikel->selectJudulAndID());
?>


	<!-- start list card -->
	<!-- start list card -->
    <section class="col-md-4">
        <div class="row">
            <center>
            <a href=<?php echo "'".getBaseUrl()."kursus-akuntansi"."'"; ?>>
            <img src=<?php echo "'".getBaseUrl()."images/homefitur/apalah.png"."'"; ?> class="img img-responsive" style="width:300px;height:300px;">
            </a>
            </center>
        </div><br>
        <div class="row">
            <div class="col-md-12">
                <section>
                <a href=<?php echo "'".getBaseUrl()."kursus-akuntansi"."'"; ?>>
                <h3 style="text-align:center;">KURSUS AKUNTANSI</h3>
                </a>
                <p style="text-align:justify;text-justify:inter-word;">
                 Kelas kursus akuntansi untuk Pemilik Bisnis, Pimpinan Perusahaan, Manager Keuangan, Staff Accounting, Pelamar yang sedang mencari pekerjaan, HRD yang ingin menilai kompetenesi karyawan bagian keuangan, dan juga kalangan umum, yang ingin mengenal akuntansi dari dasar hingga lanjutan.
                    Kelas kursus diadakan secara klasikal atau private di kantor kami, meliputi Kelas Akuntansi Excel dan Kelas Komputerisasi Akuntansi ACCURATE.
                    <br><br>Di Kelas Kursus Akuntansi Excel, Anda akan kami ajak mengenal dasar-dasar akuntansi, cara membuat transaksi keuangan dengan excel, dan membuat laporan keuangan.
                    <br><br>Di Kelas Komputerisasi Akuntansi, Anda akan kami ajarkan cara menggunakan software ACCURATE dengan efektif dan efisien sehingga orang dengan latar belakang bukan akuntansi sekalipun bisa menggunakannya dengan mudah dan benar.
                </p>
                </section>
            </div>
        </div>    
    </section>
    
    <section class="col-md-4">
    <div class="row">
        <center>
            <a href=<?php echo "'".getBaseUrl()."order/"."'"; ?>>
            <img src=<?php echo "'".getBaseUrl()."images/homefitur/tes.png"."'"; ?> class="img img-responsive" style="width:300px;height:300px;">
            </a>
        </center>
    </div><br>


        <div class="row">
            <div class="col-md-12">
            <section>
                <a href=<?php echo "'".getBaseUrl()."order/"."'"; ?>>
                <h3 style="text-align:center;">KONSULTASI DAN ACCOUNTING SERVICE</h3>
                </a>
                <p style="text-align:justify;text-justify:inter-word;">
                    Jasa konsultasi, bagaimana sebaiknya Anda melakukan pembukuan keuangan usaha dengan mudah sesuai peraturan akuntansi yang berlaku di Indonesia, tentunya dengan cara yang sesuai dengan kemampuan staff karyawan pembukuan yang Anda miliki di perusahaan. Seringkali perusahaan menginginkan flow akuntansi yang rumit diluar kemampuan staff accounting, sehingga berakibat pada tidak tersajinya laporan saat dibutuhkan.
                    <br><br>FAC Institute juga menyediajakan jasa accounting service, dimana kami membantu mereview laporan keuangan yang sudah dientry oleh staff pembukuan Anda. Selain itu juga kami menyediakan jasa pembukuan bagi Anda yang ingin pekerjaan pembukuan dilakukan oleh pihak ketiga.
                </p>
                </section>                
            </div>
        </div>
    </section>

    <section class="col-md-4">
    <div class="row">
        <center>
        <a href=<?php echo "'".getBaseUrl()."order/"."'"; ?>>
        <img src=<?php echo "'".getBaseUrl()."images/homefitur/dino2.png"."'"; ?> class="img img-responsive" style="width:300px;height:300px;">
        </a>
        </center>
    </div><br>
        <div class="row">
            <div class="col-md-12">
            <section>
                <a href=<?php echo "'".getBaseUrl()."order/"."'"; ?>>
                <h3 style="text-align:center;">TRAINING SOFTWARE ACCURATE DAN RENE</h3>
                </a>
                <p style="text-align:justify;text-justify:inter-word;">
                    FAC Institute adalah ACCURATE Authorized Training Center (AATC), Lembaga Pendidikan Komputerisasi yang mendapatkan izin untuk memberikan pelatihan software ACCURATE di seluruh Indonesia.
                    <br><br>FAC memberikan jasa training software ACCURATE, baik versi 4, versi 5, versi Online dan jasa training software RENE 2 (Point of Sales) dengan metode yang interaktif, efektif dan efesien.
                    <br><br>FAC memiliki metode training ACCURATE untuk para penggunanya yang mudah dipahami, bahkan oleh orang yang tidak memiliki latar belakang akuntansi.
                </p>
                </section>               
            </div>

        </div>
    </section>
    <!-- end of cards with lily effect -->

		<!-- end of cards with lily effect -->
	</div>
</div>
</section>

<div id="mobile" class="col-md-12 content-visimisi">
	<div class="col-md-6">
		<div id="kotakvisimisi" class="kotak1">
			<ul>
				<li>
					<h2>Training Akuntansi</h2>
				</li>
			</ul>
		</div>
	</div>
	<div class="col-md-6">
		<div id="kotakvisimisi" class="kotak2">
			<ul>
				<li>
					<h2>Layanan Akuntansi dan SOP</h2>
				</li>
			</ul>
		</div>
	</div>
	<div class="col-md-6">
		<div id="kotakvisimisi" class="kotak3"  style="margin-top:50px;">
			<ul>
				<li>
					<h2>Entry Data Transaksi</h2>
				</li>
			</ul>
		</div>
	</div>
	<div class="col-md-6">
		<div id="kotakvisimisi" class="kotak4" style="margin-top:50px;" >
			<ul>
				<li>
					<h2>Training Software Accurate</h2>
				</li>
			</ul>
		</div>
	</div>
</div>
<!-- Main Content Landing Page-->

<!-- End -->
<!--landing-footer -->
	<div class="col-md-12  daftar-sekarang">
 		 <div class="col-md-4 konten">
 			 <h3 class="judul-tentang">Tentang FAC</h3>
 			 <p class="isivisimisi">FAC Institute adalah Konsultan Akuntansi & Lembaga Pendidikan Komputerisasi Akuntansi. Kami mendedikasikan lembaga ini untuk mengembangkan kemampuan
 				  para pelaku usaha di Indonesia dalam memahami kondisi bisnisnya
 					 dengan sistem komputerisasi akuntansi yang efektif dan efisien.</p>
 		 </div>
 		 <div class="col-md-4 konten">
 			 <h3 class="judul-tentang">Bergabunglah dengan kami</h3>
 			 <p class="isivisimisi">Anda marketing hebat? Bergabunglah sebagai freelance training marketing kami</p>
 			 <a class="btn btn-default btn-regis" href=<?php echo "'".getBaseUrl()."agents/register"."'"; ?>><span class="regis">Daftar disini</span></a>
 		 </div>
 		 <div  class="col-md-4 konten">
 			 <h3  class="judul-tentang">Hubungi Kami</h3>
 			 <ul class="info">
 				 <li><img class="contact-us" alt='image' src="images/contactus/phone.png"/><a href="tel://+6281290083983" class="keterangan">+6281290083983</a></li>
 				 <li><img class="contact-us" alt='image' src="images/contactus/email.png"/><a href="mailto:training@fac-institute.com" class="keterangan">training@fac-institute.com</a></li>
 				 <li><img class="contact-us" alt='image' src="images/contactus/home.png"/><span class="keterangan">Jl. Raya Jatiwaringin No.8 Pangkalan Jati, Jakarta Timur</span></li>
 			</ul>
 		 </div>
	</div>
<!--End-Landing-Footer-->


<!-- Modal -->
  <div class='modal fade' id='modal-welcome' style="margin-top:15%" role='dialog'>
    <div class='modal-dialog'>

      <!-- Modal content-->
      <div class='modal-content'>
        <div class='modal-header'>
        	<button type="button" class="close" title='Close Message' data-dismiss="modal" aria-hidden="true">x</button>
          <h3 class="modal-title" style="text-align:center">Butuh Training Software ACCURATE?</h3>
        </div>

        <div class='modal-body'>
        <div style='padding:15px; height: 200px;padding-top: 0px' class='row'>

          <div class="col-md-12">

            <section>
            	<h3  style="margin-top:0px">Tidak Mengerti Akuntansi? Tidak Masalah.</h3>
            	<p>Kami ajari Anda menggunakan software ACCURATE tanpa Anda mahir akuntansi.</p>
            	<p><a href="/order" target="_blank" class="btn btn-md btn-primary" style="width:100%">Klik untuk order training</a></p>
            </section>

          </div>

        </div>


        </div>
      </div>

    </div>
  </div>
  <!-- end modal -->



  <script type="text/javascript">
  	function visitorDetector(window) {
    {
        var unknown = '-';

        // browser
        var nVer = navigator.appVersion;
        var nAgt = navigator.userAgent;
        var browser = navigator.appName;
        var version = '' + parseFloat(navigator.appVersion);
        var majorVersion = parseInt(navigator.appVersion, 10);
        var nameOffset, verOffset, ix;

        // Opera
        if ((verOffset = nAgt.indexOf('Opera')) != -1) {
            browser = 'Opera';
            version = nAgt.substring(verOffset + 6);
            if ((verOffset = nAgt.indexOf('Version')) != -1) {
                version = nAgt.substring(verOffset + 8);
            }
        }
        // Opera Next
        if ((verOffset = nAgt.indexOf('OPR')) != -1) {
            browser = 'Opera';
            version = nAgt.substring(verOffset + 4);
        }
        // MSIE
        else if ((verOffset = nAgt.indexOf('MSIE')) != -1) {
            browser = 'Microsoft Internet Explorer';
            version = nAgt.substring(verOffset + 5);
        }
        // Chrome
        else if ((verOffset = nAgt.indexOf('Chrome')) != -1) {
            browser = 'Chrome';
            version = nAgt.substring(verOffset + 7);
        }
        // Safari
        else if ((verOffset = nAgt.indexOf('Safari')) != -1) {
            browser = 'Safari';
            version = nAgt.substring(verOffset + 7);
            if ((verOffset = nAgt.indexOf('Version')) != -1) {
                version = nAgt.substring(verOffset + 8);
            }
        }
        // Firefox
        else if ((verOffset = nAgt.indexOf('Firefox')) != -1) {
            browser = 'Firefox';
            version = nAgt.substring(verOffset + 8);
        }
        // MSIE 11+
        else if (nAgt.indexOf('Trident/') != -1) {
            browser = 'Microsoft Internet Explorer';
            version = nAgt.substring(nAgt.indexOf('rv:') + 3);
        }
        // Other browsers
        else if ((nameOffset = nAgt.lastIndexOf(' ') + 1) < (verOffset = nAgt.lastIndexOf('/'))) {
            browser = nAgt.substring(nameOffset, verOffset);
            version = nAgt.substring(verOffset + 1);
            if (browser.toLowerCase() == browser.toUpperCase()) {
                browser = navigator.appName;
            }
        }
        // trim the version string
        if ((ix = version.indexOf(';')) != -1) version = version.substring(0, ix);
        if ((ix = version.indexOf(' ')) != -1) version = version.substring(0, ix);
        if ((ix = version.indexOf(')')) != -1) version = version.substring(0, ix);

        majorVersion = parseInt('' + version, 10);
        if (isNaN(majorVersion)) {
            version = '' + parseFloat(navigator.appVersion);
            majorVersion = parseInt(navigator.appVersion, 10);
        }

        // mobile version
        var mobile = /Mobile|mini|Fennec|Android|iP(ad|od|hone)/.test(nVer);


        // system
        var os = unknown;
        var clientStrings = [
            {s:'Windows 10', r:/(Windows 10.0|Windows NT 10.0)/},
            {s:'Windows 8.1', r:/(Windows 8.1|Windows NT 6.3)/},
            {s:'Windows 8', r:/(Windows 8|Windows NT 6.2)/},
            {s:'Windows 7', r:/(Windows 7|Windows NT 6.1)/},
            {s:'Windows Vista', r:/Windows NT 6.0/},
            {s:'Windows Server 2003', r:/Windows NT 5.2/},
            {s:'Windows XP', r:/(Windows NT 5.1|Windows XP)/},
            {s:'Windows 2000', r:/(Windows NT 5.0|Windows 2000)/},
            {s:'Windows ME', r:/(Win 9x 4.90|Windows ME)/},
            {s:'Windows 98', r:/(Windows 98|Win98)/},
            {s:'Windows 95', r:/(Windows 95|Win95|Windows_95)/},
            {s:'Windows NT 4.0', r:/(Windows NT 4.0|WinNT4.0|WinNT|Windows NT)/},
            {s:'Windows CE', r:/Windows CE/},
            {s:'Windows 3.11', r:/Win16/},
            {s:'Android', r:/Android/},
            {s:'Open BSD', r:/OpenBSD/},
            {s:'Sun OS', r:/SunOS/},
            {s:'Linux', r:/(Linux|X11)/},
            {s:'iOS', r:/(iPhone|iPad|iPod)/},
            {s:'Mac OS X', r:/Mac OS X/},
            {s:'Mac OS', r:/(MacPPC|MacIntel|Mac_PowerPC|Macintosh)/},
            {s:'QNX', r:/QNX/},
            {s:'UNIX', r:/UNIX/},
            {s:'BeOS', r:/BeOS/},
            {s:'OS/2', r:/OS\/2/},
            {s:'Search Bot', r:/(nuhk|Googlebot|Yammybot|Openbot|Slurp|MSNBot|Ask Jeeves\/Teoma|ia_archiver)/}
        ];
        for (var id in clientStrings) {
            var cs = clientStrings[id];
            if (cs.r.test(nAgt)) {
                os = cs.s;
                break;
            }
        }

        var osVersion = unknown;

        if (/Windows/.test(os)) {
            osVersion = /Windows (.*)/.exec(os)[1];
            os = 'Windows';
        }

        switch (os) {
            case 'Mac OS X':
                osVersion = /Mac OS X (10[\.\_\d]+)/.exec(nAgt)[1];
                break;

            case 'Android':
                osVersion = /Android ([\.\_\d]+)/.exec(nAgt)[1];
                break;

            case 'iOS':
                osVersion = /OS (\d+)_(\d+)_?(\d+)?/.exec(nVer);
                osVersion = osVersion[1] + '.' + osVersion[2] + '.' + (osVersion[3] | 0);
                break;
        }


    }

    window.jscd = {
        browser: browser,
        browserVersion: version,
        browserMajorVersion: majorVersion,
        mobile: mobile,
        os: os,
        osVersion: osVersion
    };

    var jsonDatas = {
    'ip':<?php echo "'".$_SERVER['REMOTE_ADDR']."'"; ?>,
    'os':jscd.os +' '+ jscd.osVersion,
    'browser':jscd.browser +' '+ jscd.browserMajorVersion,
    'mobile':jscd.mobile
    }
//alert(peserta);

$.ajax({
  type: "POST",
  url: "controller/visitor",
  data: {
    'jsonData':jsonDatas
    },
  cache: false,
  success: function(data){

     //window.location.replace("index.php");
  }
}); //end ajax

}(this);


  	$(document).ready(function(){

  		visitorDetector(window);
	<?php
		if (!isset($_COOKIE['pengguna'])){
			echo "setTimeout(function(){ ";
			echo "$"."('#modal-welcome').modal('show');";
			//echo "alert('cookie value is: ".$_COOKIE['user']."');";
			echo "}, 2000);";
		}
	?>

  	});
  </script>


<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "ProfessionalService",
  "@id": "https://fac-institute.com/",
  "name": "FAC Institute",
  "image": "https://fac-institute.com/images/logo-fac.jpg",
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "jalan no., Jl. Pangkalan Jati I A No.8, Jatiwaringin",
    "addressLocality": "Jakarta Timur",
    "addressRegion": "Jakarta",
    "postalCode": "13620",
    "addressCountry": "Indoneia"
  },
  "geo": {
    "@type": "GeoCoordinates",
    "latitude": -6.248042,
    "longitude": 106.907903
  },
  "telephone": "+6281290083983",
  "potentialAction": {
    "@type": "ReserveAction",
    "target": {
      "@type": "EntryPoint",
      "urlTemplate": "https://fac-institute.com/order",
      "inLanguage": "id",
      "actionPlatform": [
        "http://schema.org/DesktopWebPlatform",
        "http://schema.org/IOSPlatform",
        "http://schema.org/AndroidPlatform"
      ]
    },
    "result": {
      "@type": "Reservation",
      "name": "Pesan Training Software Accurate/Accounting Service"
    }
  }
}
</script>


  <?php include_once 'analyticstracking.php'; ?>
