<?php include_once 'baseurl.php'; ?>
<html class="no-js" lang="id">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="google-site-verification" content="EfNVtXecxElbcbugY59SeKXJuMzO67bjVvTvlHCc_CM" />
    <meta name="msvalidate.01" content="D925497D8F382164FF0CE0267D381FDA" />
    <meta name="thumbnailUrl" content="https://www.fac-institute.com/administrasi/artikel/images/7cabb38e259c0d5b53f9bb219daf410f.jpg?w=650" />
    <meta name="description" content="FAC - Kami Melayani Training Software Akuntansi Accurate dan Accounting Service Untuk Perusahaan Anda" />
    <meta content="First Asian Consulting. Konsultan Akuntansi & Lembaga Pendidikan Komputerisasi Akuntansi" />
    <meta name="keywords" content="Training,Akuntansi,Accurate,Jasa">
    <meta name="author" content="Rifzky Alam, Dino Damara">
    <title>FAC Institute</title>

    <!-- <link rel="stylesheet" href="<?= getBaseUrl() ?>assets/homepage/css/foundation.min.css">
    <link href="<?php echo getBaseUrl() ?>assets/homepage/css/foundicons/foundation-icons.css" rel="stylesheet"> -->
    <!-- <link href='css/carousel.css' rel='stylesheet' type='text/css'> -->

    <link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/w3.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/animate.min.css">
    <link href="https://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,100,300,500,600,700c" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <!-- <link href="<?php echo getBaseUrl() ?>assets/homepage/css/docs.css" rel="stylesheet" /> -->
    <!-- <link href='<?php echo getBaseUrl() ?>assets/homepage/css/style.css' rel='stylesheet' type='text/css'> -->
    <!-- Important Owl stylesheet -->
    <!-- <link rel="stylesheet" href="<?php echo getBaseUrl() ?>assets/homepage/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo getBaseUrl() ?>assets/homepage/owl-carousel/owl.theme.css">
    <link rel="stylesheet" href="<?php echo getBaseUrl() ?>assets/homepage/dist/photoswipe.css"> -->
    <!-- Skin CSS file (styling of UI - buttons, caption, etc.)
     In the folder of skin CSS file there are also:
     - .png and .svg icons sprite,
     - preloader.gif (for browsers that do not support CSS animations) -->
    <!-- <link rel="stylesheet" href="<?php echo getBaseUrl() ?>assets/homepage/dist/default-skin/default-skin.css"> -->
    <!-- Core JS file -->

    <!-- <script src="<?php echo getBaseUrl() ?>assets/homepage/dist/photoswipe.min.js"></script>
    <script src="<?php echo getBaseUrl() ?>assets/homepage/dist/photoswipe-ui-default.min.js"></script> -->

 <style>
 body, html {
     height: 100%;
     font-family: "Raleway", sans-serif;
     font-size: 20px;
     line-height: 1.8;
 }
 .leftside {
   text-align: center;
 }
 .homebox {
   width: 100%;
   float: center;
   margin-bottom: 10px;
   background: #fff;
   overflow: hidden;
 }
 #icons {
   list-style: none;
   margin: 0;
   padding: 0;
   text-align: center
 }
 #icons a {
   text-decoration: none;
 }
 #icons li {
   display: inline-block;
   width: 100px;
   height: 100px;
   /*border: 1px solid #ccc;*/
   margin: 0 7px 7px 0;
 }
 #icons li:hover {
 }
 #icons li img {
   /*padding: 8px 18px 2px 18px;*/
   text-align: center;
   width: 100%;
 }
 #icons li h6 {
   color: #333;
   font-size: 12px;
   text-align: center;
   margin: 0;
   padding: 0;
 }
 .mySlides {display:none;}

 .featureList, .featureList ul {
   list-style-type: none;
 }
 .featureList li:before {
   position: absolute;
   margin-left: -1.3em;
   font-weight: bold;
 }
 .featureList li.tick:before {
   content: "\2713";
   color: darkgreen;
 }
 .featureList li.cross:before {
   content: "\2717";
   color: crimson;
 }
 img {margin-bottom: -7px}
 .w3-row-padding img {margin-bottom: 12px}

 /*.crop {
     width: 1400px;
     height: 500px;
     overflow: hidden;
 }*/

 .crop img {
     /*width: 1500px;
     height: 600px;*/
 }
 </style>


<link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/style.css">


</head>
<body>
    <div id="back-top">
        <a style="font-weight: bold" href="#top">
            <img src="<?= getBaseUrl() ?>_caramel/assets/img/to-top@2x.png" />
        </a>
    </div>

    <h4>header</h4>
    <?php include_once 'top-nav.php'; ?>
    <!-- Header -->
    <header class="w3-display-container w3-content w3-center" style="max-width:1500px">
     <div class="w3-padding-32">
      <img class="w3-image" src="<?php echo getBaseUrl() ?>_caramel/assets/img/g44508.png" alt="Me" width="1100" >
     </div>

      <!-- <div class="w3-display-middle w3-padding-large w3-border w3-wide w3-text-light-grey w3-center">
        <h1 class="w3-hide-medium w3-hide-small w3-xxxlarge">FAC</h1>
        <h5 class="w3-hide-large" style="white-space:nowrap">FAC</h5>
        <h3 class="w3-hide-medium w3-hide-small">INSTITUTE</h3>
      </div>
      <div class="w3-display-bottommiddle w3-container w3-text-white w3-padding-32 w3-hide-small">
     <p>Konsultan akuntansi & lembaga pendidikan komputerisasi akuntansi.</p>
   </div> -->
    </header>


    <div class="w3-container">
      <div id="id01" class="w3-modal w3-animate-opacity">

        <div class="w3-modal-content w3-card-4">
          <header class="w3-container w3-teal">
            <span onclick="document.getElementById('id01').style.display='none'"
            class="w3-button w3-large w3-display-topright">&times;</span>
            <!-- <h2>Modal Header</h2> -->
          </header>
          <div class="w3-container w3-padding-16 w3-margin-top">
            <img class="w3-image" src="<?php echo getBaseUrl() ?>_caramel/assets/img/libur.jpg" alt="Me" width="100%">
          </div>
          <footer class="w3-container w3-teal">
            <!-- <p>Modal Footer</p> -->
          </footer>

        </div>
      </div>
    </div>




    <!-- Page content -->
<div class="w3-content w3-padding-16 w3-margin-top" id="portfolio">
      <!-- About Container --> ``
<div class="w3-container" id="about">
  <div class="w3-content" style="">
    <h5 class="w3-center w3-padding-16"><span class="w3-tag w3-wide">Tentang Kami</span></h5>
    <p style="text-align:justify">FAC Institute adalah konsultan akuntansi & lembaga pendidikan komputerisasi akuntansi. FAC Institute berdedikasi untuk mengembangkan dan meningkatkan kemampuan usaha masyarakat dengan dukungan sistem komputerisasi akuntansi yang efektif dan efisien.</p>
    <!-- <p style="text-align:justify">FAC Institute telah mengadakan pelatihan <i>software</i> akuntansi ke lebih dari <b>400</b> klien meliputi individu, sekolah, kampus, usaha kecil dan menengah, perusahaan swasta nasional dan perusahaan multinasional di berbagai wilayah Indonesia).</p> -->
    <div class="w3-half w3-panel w3-leftbar">
      <h2 style="font-weight:bold">Lembaga Pendidikan Komputerisasi Akuntansi Terpercaya di Indonesia</h2>
      <ul class="featureList">
    <li class="tick">FAC Institute telah mengadakan pelatihan <i>software</i> akuntansi ke lebih dari <b>500</b> klien meliputi individu, sekolah, kampus, UKM, perusahaan swasta nasional dan perusahaan multinasional di berbagai wilayah Indonesia</li>
    <li class="tick">FAC Institute adalah <b>ACCURATE Authorized Training Center (AATC)</b>,
     yaitu lembaga pendidikan komputerisasi yang mendapatkan izin dari CPSSOFT untuk memberikan pelatihan software ACCURATE di seluruh Indonesia.</li>
    <li class="tick">FAC Institute memiliki metode pembelajaran yang dirancang khusus untuk peserta didik tanpa latar belakang akuntansi.</li>
    <!-- <li class="tick">Bersama kami Anda dapat terampil menggunakan software akuntansi ACCURATE tanpa Anda harus mahir akuntansi</li> -->
  </ul>

    </div>

    <div class="w3-half w3-center w3-panel">
    <img class="w3-image" src="<?php echo getBaseUrl() ?>_caramel/assets/img/g44508logo0.png" alt="Me" width="400px">
    </div>

<!-- <div id="carousel_1" class="w3-half w3-margin w3-content w3-section" style="max-width:500px">
  <img class="mySlides" src="<?php echo getBaseUrl() ?>_caramel/assets/img/13268105_1000300386691511_5269298106051400217_o.jpg" style="width:100%">
  <img class="mySlides" src="<?php echo getBaseUrl() ?>_caramel/assets/img/13235103_1000299510024932_3894420669409310754_o.jpg" style="width:100%">
  <img class="mySlides" src="<?php echo getBaseUrl() ?>_caramel/assets/img//12891768_968708069850743_7436250094960452284_o.jpg" style="width:100%">
  <img class="mySlides" src="<?php echo getBaseUrl() ?>_caramel/assets/img/12473566_968708176517399_5630018724927164532_o.jpg"  style="width:100%">
</div> -->

   <!-- <p id="carousel_1text" style="font-size:12px" class="w3-center">Dokumentasi kursus/training FAC Institute</p> -->
    <!-- <img src="<?php echo getBaseUrl() ?>images/13256189_1000296093358607_5291351703540609579_n_Fotor.jpg" style="width:100%;max-width:1200px" class="w3-margin-top"> -->
    <!-- <p><strong>desc:</strong> Pelatihan Pembukuan Hebat untuk UKM Hebat yang diselenggarakan oleh UKMC FEB UI</p> -->
  </div>
</div>



<div class="w3-padding-16" style="background-color:#26aeed;border-width:1px;border-color:#f6f6f6;"><div class="reading-box-additional">
<h1 style="text-align: center; font-size: 33.036px;"><span style="color: #ffffff;">Kami memberikan dukungan 100% </span><br>
<span style="color: #ffffff;">untuk kemudahan Training Software ACCURATE&nbsp;</span></h1>
</div></div>
<br>





    <div class="w3-container" id="about">
      <div class="w3-row-padding w3-center w3-padding-32" style="background-color: #193441; font-size:14px">
        <div class="w3-quarter">
          <img src="https://penjualanresmiaccurate.com/wp-content/uploads/2017/06/Logomakr_6IhWWp.png" width="100">
          <p class="w3-large" style="color:#00ccc5">Komprehensif</p>
          <p style="color:white">Kami melaksanakan training siklus Software ACCURATE dan RENE POS dari A sampai Z.</p>
        </div>
        <div class="w3-quarter">
          <img src="https://penjualanresmiaccurate.com/wp-content/uploads/2017/06/Logomakr_9NHQCl.png" width="100">
          <p class="w3-large" style="color:#00ccc5">Inovasi</p>
          <p style="color:white">Kami menciptakan metode pembelajaran Software ACCURATE tanpa Anda harus mahir akuntansi.</p>
        </div>
        <div class="w3-quarter">
          <img src="https://penjualanresmiaccurate.com/wp-content/uploads/2017/06/Logomakr_3yLitp.png" width="70">
          <p class="w3-large" style="color:#00ccc5">Efektif & Efisien</p>
          <p style="color:white">Kami melatih para pengguna ACCURATE sesuai kadar kemampuan para staff di perusahaan Anda.</p>
        </div>
        <div class="w3-quarter">
          <img src="https://penjualanresmiaccurate.com/wp-content/uploads/2017/06/Logomakr_9Dq3Zp.png" width="100">
          <p class="w3-large" style="color:#00ccc5">SDM</p>
          <p style="color:white">Kami Berorientasi pada pemenuhan kebutuhan sumber daya tenaga ACCURATE yang profesional untuk Indonesia.</p>
        </div>

      </div>
    </div>


<!-- Menu Container -->
<div class="w3-container" id="menu">
  <div class="w3-content" style="">
    <h5 class="w3-center w3-padding-48"><span class="w3-tag w3-wide">Layanan & Jasa Kami</span></h5>
    <div class="w3-row w3-center w3-card-2 w3-padding">
      <a href="javascript:void(0)" onclick="openMenu(event, 'Eat');" id="myLink">
        <div class="w3-col s4 tablink">Kursus Akuntansi</div>
      </a>
      <a href="javascript:void(0)" onclick="openMenu(event, 'Drinks');">
        <div class="w3-col s4 tablink">Training Software Akuntansi</div>

      </a>
      <a href="javascript:void(0)" onclick="openMenu(event, 'test');">
        <div class="w3-col s4 tablink">Accounting Service & Konsultasi</div>
      </a>
    </div>

<div id="Eat" class="w3-container menu w3-padding-48 w3-card-2">
  <div class="w3-row" id="menu">
    <div class="w3-col l6 w3-padding-large">
      <p style="font-style:italic;text-align: left">Kami menyelenggarakan kelas kursus komputer akuntansi dengan menggunakan software Accurate. Kelas kursus  diperuntukkan untuk :
          <ol>
              <li>Pemilik Bisnis</li>
              <li>Pemilik Usaha Kecil & Menengah</li>
              <li>Pimpinan Perusahaan</li>
              <li>Manajer Keuangan</li>
              <li>Staff Accounting</li>
              <li>Pelamar yang sedang mencari pekerjaan</li>
              <li>HRD yang ingin menilai kompetensi karyawan bagian keuangan</li>
              <li>SDM di bagian akuntansi pembukuan</li>
              <li>SDM yang akan ditempatkan di bidang akuntansi keuangan</li>
              <li>Praktisi IT yang bergerak dalam pemrograman akuntansi</li>
              <li>Manajer bidang lain yang ingin mengetahui akuntansi secara umum</li>
              <li>Masyarakat umum yang berminat untuk mempelajari akuntansi.</li>
          </ol>
      </p>
    </div>
    <div class="w3-col l6 w3-padding-large">
      <img src="<?php echo getBaseUrl() ?>_caramel/assets/img/Medieval2.jpg" class="w3-round w3-image" alt="Menu" width="500" height="750">
    </div>
  </div>
<div class="w3-padding-large">
  <p> Kelas kursus akuntansi dapat dilakukan secara klasikal (berkelompok) atau private (perseorangan) di kantor kami. Kelas kursus akuntansi terbagi menjadi : Kursus Microsoft Excel dan kursus ACCURATE. Materi kursus meliputi pengenalan dasar - dasar akuntansi, pembuatan transaksi keuangan dan pembuatan laporan keuangan menggunakan Microsoft Excel / ACCURATE dengan efektif dan efisien. </p>
<a href="https://bakatcendekia.com/?KELAS_ACCURATE" target="_blank">Informasi Selengkapnya ...</a>
</div>
</div>

    <div id="Drinks" class="w3-container menu w3-padding-16 w3-card-2">
      <p>FAC Institute mengadakan pelatihan software akuntansi Accurate dengan 3 pilihan tema :</p>
      <h3><strong>Training Dasar ACCURATE</strong></h3>
      <p>Ditujukan untuk akuntan pemula/lembaga pendidikan. Dalam pelatihan ini akan dibahas agenda sebagai berikut :</p>
      <ul>
      	<li>Pelatihan software ACCURATE dengan setup metode standar.</li>
      	<li>Pengenalan fitur-fitur <i>Accurate standard</i> dan modul laporan.</li>
      	<li>Simulasi dan praktek ACCURATE dengan contoh kasus standar.</li>
      </ul>
      <h3><strong>Training Mahir ACCURATE</strong></h3>
      <p>Ditujukan untuk perusahaan dan konsultan akuntansi. Dalam pelatihan ini akan dibahas agenda sebagai berikut :</p>
      <ul>
      	<li>Pelatihan software ACCURATE dengan setup metode mahir.</li>
      	<li>Pengenalan fitur-fitur Accurate dan penerapan software Accurate pada sistem perusahaan sesuai proses bisnis yang ada.</li>
      	<li>Simulasi dan praktek software ACCURATE menggunakan data/studi kasus perusahaan.</li>
      </ul>
      <h3><strong>Training ACCURATE secara klasikal atau private</strong></h3>
      <p>Ditujukan untuk individu yang ingin mempelajari Software ACCURATE. Dalam training ini akan dibahas agenda sebagai berikut :</p>
      <ul style="list-style-type:square;">
      	<li>Pelatihan software ACCURATE dengan setup metode standar/mahir.</li>
      	<li>Pengenalan fitur-fitur ACCURATE hingga modul laporan.</li>
      	<li>Simulasi dan praktek ACCURATE dengan data pelatihan/studi kasus perusahaan.</li>
      </ul>
      </div>


    <div id="test" class="w3-container menu w3-padding-16 w3-card-2">
      <h3><strong>Layanan Akuntansi</strong></h3>
      <p>Kami selalu siap membantu Anda dalam memeriksa dan meninjau kembali laporan keuangan usaha Anda. Kami menyediakan layanan berupa jasa <b>pembukuan</b> atau pencatatan transaksi keuangan perusahaan Anda.</p>
      <h3><strong>Jasa Konsultasi</strong></h3>
      <p>Bagaimana sebaiknya Anda membuat catatan transaksi keuangan perusahaan dengan mudah dan sesuai dengan peraturan yang berlaku di Indonesia? tentunya Anda membutuhkan staff accounting yang terlatih dalam hal itu. Namun seringkali, kemampuan staff accounting terbatas dalam mengerjakan cash flow perusahaan yang rumit, sehingga berakibat pada tidak tersajinya laporan saat dibutuhkan. Konsultasikan masalah Anda dengan kami dan kami siap membantu Anda.</p>
    </div>
  </div>

  <div class="w3-light-grey w3-padding-large w3-padding-32 w3-margin-top" id="contact">
    <div class="w3-row-padding" id="myGrid" style="margin-bottom:128px">
      <h2 class="w3-center" style="margin-bottom:24px;">Klien Kami</h2>
      <div class="leftside">
                                  <div class="homebox">
                                      <ul id="icons">
                                          <li>
                                                <img src="<?= getBaseUrl() ?>_caramel/assets/img/image4780.png">
                                          </li>
                                          <li>
                                                  <img src="<?= getBaseUrl() ?>_caramel/assets/img/image3634.png">
                                          </li>
                                          <li>
                                                  <img src="<?= getBaseUrl() ?>_caramel/assets/img/image4352.png">
                                          </li>
                                          <li>
                                                  <img src="<?= getBaseUrl() ?>_caramel/assets/img/image4958.png">
                                          </li>
                                          <li>
                                                  <img src="<?= getBaseUrl() ?>_caramel/assets/img/image5084.png">
                                          </li>
                                          <li>
                                                  <img src="<?= getBaseUrl() ?>_caramel/assets/img/image5594.png">
                                          </li>
                                          <li>
                                                  <img src="<?= getBaseUrl() ?>_caramel/assets/img/image6168.png">
                                          </li>
                                          <li>
                                                  <img src="<?= getBaseUrl() ?>_caramel/assets/img/image6230.png">
                                          </li>
                                          <li>
                                                  <img src="<?= getBaseUrl() ?>_caramel/assets/img/image6468.png">
                                          </li>
                                          <li>
                                                  <img src="<?= getBaseUrl() ?>_caramel/assets/img/image7042.png">
                                          </li>
                                          <li>
                                                  <img src="<?= getBaseUrl() ?>_caramel/assets/img/image3578.png">
                                          </li>
                                          <li>
                                                  <img src="<?= getBaseUrl() ?>_caramel/assets/img/image3640.png">
                                          </li>
                                          <li>
                                                  <img src="<?= getBaseUrl() ?>_caramel/assets/img/rect32011.png">
                                          </li>
                                          <li>
                                                  <img src="<?= getBaseUrl() ?>_caramel/assets/img/rect320122.png">
                                          </li>
                                          <li>
                                                  <img src="<?= getBaseUrl() ?>_caramel/assets/img/rect3201f.png">
                                          </li>
                                          <li>
                                                  <img src="<?= getBaseUrl() ?>_caramel/assets/img/rect32011e.png">
                                          </li>
                                          <li>
                                                  <img src="<?= getBaseUrl() ?>_caramel/assets/img/rect3201g.png">
                                          </li>
                                          <li>
                                                  <img src="<?= getBaseUrl() ?>_caramel/assets/img/rect3201a.png">
                                          </li>
                                          <li>
                                                  <img src="<?= getBaseUrl() ?>_caramel/assets/img/rect3201b.png">
                                          </li>
                                          <li>
                                                  <img src="<?= getBaseUrl() ?>_caramel/assets/img/rect3201c.png">
                                          </li>
                                          <li>
                                                  <img src="<?= getBaseUrl() ?>_caramel/assets/img/rect3201d.png">
                                          </li>
                                          <li>
                                                  <img src="<?= getBaseUrl() ?>_caramel/assets/img/bitmap.png">
                                          </li>
                                          <li>
                                                  <img src="<?= getBaseUrl() ?>_caramel/assets/img/bitmap2.png">
                                          </li>
                                          <li>
                                                  <img src="<?= getBaseUrl() ?>_caramel/assets/img/bitmap3.png">
                                          </li>
                                          <li>
                                                  <img src="<?= getBaseUrl() ?>_caramel/assets/img/bitmap4.png">
                                          </li>
                                          <li>
                                                  <img src="<?= getBaseUrl() ?>_caramel/assets/img/rect5843.png">
                                          </li>
                                          <li>
                                                  <img src="<?= getBaseUrl() ?>_caramel/assets/img/BG7-hKph.jpg">
                                          </li>
                                      </ul>
                                  </div>
                              </div>

      <!-- <h3 class="w3-center">Dokumentasi Kegiatan</h3> -->
      <hr>
      <!-- <div class="w3-third">
        <p>Pelatihan Pembukuan Hebat untuk UKM Hebat yang diselenggarakan oleh UKMC FEB UI</p>
        <img src="<?php echo getBaseUrl() ?>images/homepage/13268105_1000300386691511_5269298106051400217_o.jpg" alt="Image description" style="width:100%" /> </a>
        <img src="<?php echo getBaseUrl() ?>images/homepage/13235103_1000299510024932_3894420669409310754_o.jpg" alt="Image description" style="width:100%" /> </a>
      </div>
      <div class="w3-third">
        <p>Training Accurate untuk karyawan di Arsari Group</p>
        <img src="<?php echo getBaseUrl() ?>images/homepage/12891768_968708069850743_7436250094960452284_o.jpg" style="width:100%">
        <img src="<?php echo getBaseUrl() ?>images/homepage/12473566_968708176517399_5630018724927164532_o.jpg" style="width:100%">
      </div>
      <div class="w3-third">
        <p>Pelatihan Accurate untuk dosen & mahasiswa jurusan akuntansi di IBI KOSGORO 57</p>
          <img src="<?php echo getBaseUrl() ?>images/homepage/12888496_968708426517374_610873257017712311_o.jpg" style="width:100%">
        <img src="<?php echo getBaseUrl() ?>images/homepage/12672119_968708466517370_7816653324994776514_o.jpg" style="width:100%">
      </div> -->

      <div class="w3-container w3-padding-16 w3-indigo">
        <p>Perlu Training ACCURATE? Hubungi Kami di <strong>0812 9008 3983</strong></p>
        <p><span class="w3-tag">Temukan Kami!</span> Jl. Jatiwaringin Raya Blok H No.8, RT.2/RW.13, Cipinang Melayu, Makasar, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta 13620.</p>
        <p><strong>Hari buka</strong> : Senin s/d Jumat (09.00–17.00)</p>
      </div>

      <div class="w3-container w3-center w3-padding-16 ">
        <h2 class="w3-center" style="margin-bottom:24px;">Follow us on facebook/instagram</h2>
        <a href="https://www.facebook.com/FAC.Institute/" target="_blank" class="fa fa-facebook"></a>
        <a href="https://www.instagram.com/facinstitute/" target="_blank" class="fa fa-instagram"></a>
      </div>
    </div>
  </div>
</div>
</div>

<script>
function openMenu(evt, menuName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("menu");
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
     tablinks[i].className = tablinks[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(menuName).style.display = "block";
  evt.currentTarget.firstElementChild.className += " w3-dark-grey";
}
document.getElementById("myLink").click();
</script>





    <script src="<?php echo getBaseUrl() ?>assets/homepage/js/jquery-2.1.4.min.js"></script>
    <!-- <script>
    $( document ).ready(function() {
        document.getElementById('id01').style.display='block';
    });
    </script> -->
    <!-- <script src="<?php echo getBaseUrl() ?>assets/homepage/js/foundation.min.js"></script>
    <script>
        $(document).foundation();
    </script>
    <script type="text/javascript" src="<?php echo getBaseUrl() ?>assets/homepage/js/zcom.js"></script> -->
    <!-- Include js plugin -->
    <!-- <script src="<?php echo getBaseUrl() ?>assets/homepage/owl-carousel/owl.carousel.js"></script> -->
    <!-- <script src="<?php echo getBaseUrl() ?>assets/homepage/js/home.js"></script> -->
    <!-- <script src="<?php echo getBaseUrl() ?>assets/homepage/dist/photoswipe.min.js"></script> -->
    <!-- UI JS file -->
    <!-- <script src="<?php echo getBaseUrl() ?>assets/homepage/dist/photoswipe-ui-default.min.js"></script> -->
    <!-- <script src="<?= getBaseUrl() ?>assets/homepage/dist/photoswipe-ui-default.min.js"></script> -->



    <script type="text/javascript">
        function visitorDetector(window) {
            {
                var unknown = '-';

                // browser
                var nVer = navigator.appVersion;
                var nAgt = navigator.userAgent;
                var browser = navigator.appName;
                var version = '' + parseFloat(navigator.appVersion);
                var majorVersion = parseInt(navigator.appVersion, 10);
                var nameOffset, verOffset, ix;

                // Opera
                if ((verOffset = nAgt.indexOf('Opera')) != -1) {
                    browser = 'Opera';
                    version = nAgt.substring(verOffset + 6);
                    if ((verOffset = nAgt.indexOf('Version')) != -1) {
                        version = nAgt.substring(verOffset + 8);
                    }
                }
                // Opera Next
                if ((verOffset = nAgt.indexOf('OPR')) != -1) {
                    browser = 'Opera';
                    version = nAgt.substring(verOffset + 4);
                }
                // MSIE
                else if ((verOffset = nAgt.indexOf('MSIE')) != -1) {
                    browser = 'Microsoft Internet Explorer';
                    version = nAgt.substring(verOffset + 5);
                }
                // Chrome
                else if ((verOffset = nAgt.indexOf('Chrome')) != -1) {
                    browser = 'Chrome';
                    version = nAgt.substring(verOffset + 7);
                }
                // Safari
                else if ((verOffset = nAgt.indexOf('Safari')) != -1) {
                    browser = 'Safari';
                    version = nAgt.substring(verOffset + 7);
                    if ((verOffset = nAgt.indexOf('Version')) != -1) {
                        version = nAgt.substring(verOffset + 8);
                    }
                }
                // Firefox
                else if ((verOffset = nAgt.indexOf('Firefox')) != -1) {
                    browser = 'Firefox';
                    version = nAgt.substring(verOffset + 8);
                }
                // MSIE 11+
                else if (nAgt.indexOf('Trident/') != -1) {
                    browser = 'Microsoft Internet Explorer';
                    version = nAgt.substring(nAgt.indexOf('rv:') + 3);
                }
                // Other browsers
                else if ((nameOffset = nAgt.lastIndexOf(' ') + 1) < (verOffset = nAgt.lastIndexOf('/'))) {
                    browser = nAgt.substring(nameOffset, verOffset);
                    version = nAgt.substring(verOffset + 1);
                    if (browser.toLowerCase() == browser.toUpperCase()) {
                        browser = navigator.appName;
                    }
                }
                // trim the version string
                if ((ix = version.indexOf(';')) != -1) version = version.substring(0, ix);
                if ((ix = version.indexOf(' ')) != -1) version = version.substring(0, ix);
                if ((ix = version.indexOf(')')) != -1) version = version.substring(0, ix);

                majorVersion = parseInt('' + version, 10);
                if (isNaN(majorVersion)) {
                    version = '' + parseFloat(navigator.appVersion);
                    majorVersion = parseInt(navigator.appVersion, 10);
                }

                // mobile version
                var mobile = /Mobile|mini|Fennec|Android|iP(ad|od|hone)/.test(nVer);


                // system
                var os = unknown;
                var clientStrings = [{
                        s: 'Windows 10',
                        r: /(Windows 10.0|Windows NT 10.0)/
                    },
                    {
                        s: 'Windows 8.1',
                        r: /(Windows 8.1|Windows NT 6.3)/
                    },
                    {
                        s: 'Windows 8',
                        r: /(Windows 8|Windows NT 6.2)/
                    },
                    {
                        s: 'Windows 7',
                        r: /(Windows 7|Windows NT 6.1)/
                    },
                    {
                        s: 'Windows Vista',
                        r: /Windows NT 6.0/
                    },
                    {
                        s: 'Windows Server 2003',
                        r: /Windows NT 5.2/
                    },
                    {
                        s: 'Windows XP',
                        r: /(Windows NT 5.1|Windows XP)/
                    },
                    {
                        s: 'Windows 2000',
                        r: /(Windows NT 5.0|Windows 2000)/
                    },
                    {
                        s: 'Windows ME',
                        r: /(Win 9x 4.90|Windows ME)/
                    },
                    {
                        s: 'Windows 98',
                        r: /(Windows 98|Win98)/
                    },
                    {
                        s: 'Windows 95',
                        r: /(Windows 95|Win95|Windows_95)/
                    },
                    {
                        s: 'Windows NT 4.0',
                        r: /(Windows NT 4.0|WinNT4.0|WinNT|Windows NT)/
                    },
                    {
                        s: 'Windows CE',
                        r: /Windows CE/
                    },
                    {
                        s: 'Windows 3.11',
                        r: /Win16/
                    },
                    {
                        s: 'Android',
                        r: /Android/
                    },
                    {
                        s: 'Open BSD',
                        r: /OpenBSD/
                    },
                    {
                        s: 'Sun OS',
                        r: /SunOS/
                    },
                    {
                        s: 'Linux',
                        r: /(Linux|X11)/
                    },
                    {
                        s: 'iOS',
                        r: /(iPhone|iPad|iPod)/
                    },
                    {
                        s: 'Mac OS X',
                        r: /Mac OS X/
                    },
                    {
                        s: 'Mac OS',
                        r: /(MacPPC|MacIntel|Mac_PowerPC|Macintosh)/
                    },
                    {
                        s: 'QNX',
                        r: /QNX/
                    },
                    {
                        s: 'UNIX',
                        r: /UNIX/
                    },
                    {
                        s: 'BeOS',
                        r: /BeOS/
                    },
                    {
                        s: 'OS/2',
                        r: /OS\/2/
                    },
                    {
                        s: 'Search Bot',
                        r: /(nuhk|Googlebot|Yammybot|Openbot|Slurp|MSNBot|Ask Jeeves\/Teoma|ia_archiver)/
                    }
                ];
                for (var id in clientStrings) {
                    var cs = clientStrings[id];
                    if (cs.r.test(nAgt)) {
                        os = cs.s;
                        break;
                    }
                }

                var osVersion = unknown;

                if (/Windows/.test(os)) {
                    osVersion = /Windows (.*)/.exec(os)[1];
                    os = 'Windows';
                }

                switch (os) {
                    case 'Mac OS X':
                        osVersion = /Mac OS X (10[\.\_\d]+)/.exec(nAgt)[1];
                        break;

                    case 'Android':
                        osVersion = /Android ([\.\_\d]+)/.exec(nAgt)[1];
                        break;

                    case 'iOS':
                        osVersion = /OS (\d+)_(\d+)_?(\d+)?/.exec(nVer);
                        osVersion = osVersion[1] + '.' + osVersion[2] + '.' + (osVersion[3] | 0);
                        break;
                }


            }

            window.jscd = {
                browser: browser,
                browserVersion: version,
                browserMajorVersion: majorVersion,
                mobile: mobile,
                os: os,
                osVersion: osVersion
            };

            var jsonDatas = {
                'ip': <?php echo "'".$_SERVER['REMOTE_ADDR']."'"; ?>,
                'os': jscd.os + ' ' + jscd.osVersion,
                'browser': jscd.browser + ' ' + jscd.browserMajorVersion,
                'mobile': jscd.mobile
            }
            //alert(peserta);

            $.ajax({
                type: "POST",
                url: "controller/visitor",
                data: {
                    'jsonData': jsonDatas
                },
                cache: false,
                success: function(data) {
                    // alert(data);
                    //window.location.replace("index.php");
                }
            }); //end ajax

        }(this);

        $(document).ready(function() {
            visitorDetector(window);

              // hide #back-top first
              $("#back-top").hide();

              // fade in #back-top
              $(function () {
                $(window).scroll(function () {
                  if ($(this).scrollTop() > 100) {
                    $('#back-top').fadeIn();
                  } else {
                    $('#back-top').fadeOut();
                  }
                });

                // scroll body to 0px on click
                $('#back-top .fi-arrow-up').click(function () {
                  $('body,html').animate({
                    scrollTop: 0
                  }, 800);
                  return false;
                });
              });

        });
    </script>


<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Course",
  "name": "Training Software Accurate, Audit data dan Kursus Akuntansi",
  "description": "Kami mengajari, tanpa harus anda memahami akuntansi.",
  "provider": {
    "@type": "Organization",
    "name": "FAC Institute",
    "sameAs": "http://www.fac-institute.com"
  }
}
</script>

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "ProfessionalService",
  "@id": "https://fac-institute.com/",
  "name": "FAC Institute",
  "image": "https://fac-institute.com/images/logo-fac.jpg",
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "jalan no., Jl. Pangkalan Jati I A No.8, Jatiwaringin",
    "addressLocality": "Jakarta Timur",
    "addressRegion": "Jakarta",
    "postalCode": "13620",
    "addressCountry": "Indoneia"
  },
  "geo": {
    "@type": "GeoCoordinates",
    "latitude": -6.248042,
    "longitude": 106.907903
  },
  "telephone": "+6281290083983",
  "potentialAction": {
    "@type": "ReserveAction",
    "target": {
      "@type": "EntryPoint",
      "urlTemplate": "https://fac-institute.com/order",
      "inLanguage": "id",
      "actionPlatform": [
        "http://schema.org/DesktopWebPlatform",
        "http://schema.org/IOSPlatform",
        "http://schema.org/AndroidPlatform"
      ]
    },
    "result": {
      "@type": "Reservation",
      "name": "Pesan Training Software Accurate/Accounting Service"
    }
  }
}
</script>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/576deedca4fa94c76a6d4cad/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->




<script>
var myIndex = 0;
carousel();

function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";
    }
    myIndex++;
    if (myIndex > x.length) {myIndex = 1}
    x[myIndex-1].style.display = "block";
    setTimeout(carousel, 4000); // Change image every 2 seconds
}
</script>


</body>
</html>
