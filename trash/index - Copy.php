<?php include_once 'baseurl.php'; ?>
<html class="no-js" lang="id">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="google-site-verification" content="EfNVtXecxElbcbugY59SeKXJuMzO67bjVvTvlHCc_CM" />
    <meta name="msvalidate.01" content="D925497D8F382164FF0CE0267D381FDA" />
    <meta name="thumbnailUrl" content="https://www.fac-institute.com/administrasi/artikel/images/7cabb38e259c0d5b53f9bb219daf410f.jpg?w=650" />
    <meta name="description" content="FAC - Kami Melayani Training Software Akuntansi Accurate dan Accounting Service Untuk Perusahaan Anda" />
    <meta content="First Asian Consulting. Konsultan Akuntansi & Lembaga Pendidikan Komputerisasi Akuntansi" />
    <meta name="keywords" content="Training,Akuntansi,Accurate,Jasa">
    <meta name="author" content="Rifzky Alam, Dino Damara">
    <title>FAC Institute</title>
    <!-- <link rel="stylesheet" href="<?= getBaseUrl() ?>assets/homepage/css/foundation.min.css">
    <link href="<?php echo getBaseUrl() ?>assets/homepage/css/foundicons/foundation-icons.css" rel="stylesheet"> -->
    <!-- <link href='css/carousel.css' rel='stylesheet' type='text/css'> -->
    <link rel="stylesheet" href="https://www.w3schools.com/lib/w3.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo getBaseUrl() ?>assets/homepage/css/animate.min.css">
    <link href="https://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,100,300,500,600,700c" type="text/css" />
    <!-- <link href="<?php echo getBaseUrl() ?>assets/homepage/css/docs.css" rel="stylesheet" /> -->
    <link href='<?php echo getBaseUrl() ?>assets/homepage/css/style.css' rel='stylesheet' type='text/css'>
    <!-- Important Owl stylesheet -->
    <link rel="stylesheet" href="<?php echo getBaseUrl() ?>assets/homepage/owl-carousel/owl.carousel.css">
    <!-- Default Theme -->
    <link rel="stylesheet" href="<?php echo getBaseUrl() ?>assets/homepage/owl-carousel/owl.theme.css">
    <!-- Core CSS file -->
    <link rel="stylesheet" href="<?php echo getBaseUrl() ?>assets/homepage/dist/photoswipe.css">
    <!-- Skin CSS file (styling of UI - buttons, caption, etc.)
     In the folder of skin CSS file there are also:
     - .png and .svg icons sprite,
     - preloader.gif (for browsers that do not support CSS animations) -->
    <link rel="stylesheet" href="<?php echo getBaseUrl() ?>assets/homepage/dist/default-skin/default-skin.css">
    <!-- Core JS file -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inconsolata">

<style type="text/css">
#back-top {
  position: fixed;
  bottom:20px;
  right: 2%;
  z-index: 100; }
</style>

<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Raleway", sans-serif}
body, html {
    height: 100%;
    font-family: "Inconsolata", sans-serif;
    font-size: 20px;
}
body, html {
    height: 100%;
    line-height: 1.8;
}
/* Full height image header */
/*.bgimg-1 {
    background-position: center;
    background-size: cover;
    background-image: url("img/AcademicBookWeek.jpg");
    min-height: 100%;
}*/
.w3-bar .w3-button {
    padding: 16px;
}
.text-shadow {
  text-shadow: 2px 2px 8px #000;
}


.w3-left, .w3-right, .w3-badge {cursor:pointer}
.w3-badge {height:13px;width:13px;padding:0}
</style>


    <script src="<?php echo getBaseUrl() ?>assets/homepage/dist/photoswipe.min.js"></script>
    <script src="<?php echo getBaseUrl() ?>assets/homepage/dist/photoswipe-ui-default.min.js"></script>
</head>

<style>
.w3-content {
    /*max-width: 100%;*/
    margin: auto;
}
#owl-demo3 .item img{
    display: block;
    width: 100%;
    height: auto;
}

.menu {
    display: none;
}

.w3-content {
    max-width: 1200px;
    margin: auto;
}

.w3-top {
  font-size: 14px;
}

img {margin-bottom: -7px}
.w3-row-padding img {margin-bottom: 12px}
</style>


<style>
.leftside {
  text-align: center;
}
.homebox {
  width: 100%;
  float: center;
  margin-bottom: 10px;
  background: #fff;
  overflow: hidden;
}
#icons {
  list-style: none;
  margin: 0;
  padding: 0;
  text-align: center
}
#icons a {
  text-decoration: none;
}
#icons li {
  display: inline-block;
  width: 100px;
  height: 100px;
  /*border: 1px solid #ccc;*/
  margin: 0 7px 7px 0;
}
#icons li:hover {
}
#icons li img {
  /*padding: 8px 18px 2px 18px;*/
  text-align: center;
  width: 100%;
}
#icons li h6 {
  color: #333;
  font-size: 12px;
  text-align: center;
  margin: 0;
  padding: 0;
}
</style>

<script type="text/javascript" src="<?php echo getBaseUrl() ?>instafeed.min.js"></script>

<script type="text/javascript">
    var feed = new Instafeed({
        get: 'tagged',
        tagName: 'akuntansi',
        clientId: '43b706953b81422a951a0a389ea66570',
        template: '<a href="{{link}}"><img src="{{image}}" /></a>'
    });
    feed.run();
</script>

<body>
    <div id="back-top">
        <a style="font-weight: bold" href="#top">
            <img src="<?= getBaseUrl() ?>images/to-top@2x.png" />
        </a>
    </div>
    <!-- <?php include_once 'top-nav.php'; ?> -->


    <!-- Header -->
    <header class="w3-display-container w3-content w3-center" style="max-width:1500px">
      <img class="w3-image" src="<?php echo getBaseUrl() ?>images/Untitled design.jpg" alt="Me" width="1500" height="600">
      <div class="w3-display-middle w3-padding-large w3-border w3-wide w3-text-light-grey w3-center">
        <h1 class="w3-hide-medium w3-hide-small w3-xxxlarge">FAC</h1>
        <h5 class="w3-hide-large" style="white-space:nowrap">FAC</h5>
        <h3 class="w3-hide-medium w3-hide-small">INSTITUTE</h3>

        <!-- <p style="text-align:center"> </p> -->
      </div>

      <div class="w3-display-bottommiddle w3-container w3-text-white w3-padding-32 w3-hide-small">
     <p>Konsultan akuntansi & lembaga pendidikan komputerisasi akuntansi.</p>
   </div>

      <!-- <div class="w3-bar w3-light-grey w3-round w3-display-bottommiddle w3-hide-small" style="bottom:-16px">
        <a href="#" class="w3-bar-item w3-button">Home</a>
        <a href="#portfolio" class="w3-bar-item w3-button">Portfolio</a>
        <a href="#contact" class="w3-bar-item w3-button">Contact</a>
      </div> -->
    </header>

    <!-- Navbar on small screens -->
    <div class="w3-center w3-light-grey w3-padding-16 w3-hide-large w3-hide-medium">
    <div class="w3-bar w3-light-grey">
      <a href="#" class="w3-bar-item w3-button">Home</a>
      <a href="#portfolio" class="w3-bar-item w3-button">Portfolio</a>
      <a href="#contact" class="w3-bar-item w3-button">Contact</a>
    </div>
    </div>



    <!-- Page content -->
<div class="w3-content w3-padding-16 w3-margin-top" id="portfolio">

      <!-- About Container -->
<div class="w3-container" id="about">
  <div class="w3-content" style="">
    <h5 class="w3-center w3-padding-64"><span class="w3-tag w3-wide">Tentang Kami</span></h5>
    <p style="text-align:justify">FAC Institute adalah konsultan akuntansi & lembaga pendidikan komputerisasi akuntansi. FAC Institute dibangun untuk didedikasikan kepada pelaku usaha untuk mengembangkan dan meningkatkan kemampuan mereka dalam menjalankan usahanya dengan dukungan sistem komputerisasi akuntansi yang efektif dan efisien.</p>
    <p style="text-align:justify">FAC Institute telah mengadakan pelatihan <i>software</i> akuntansi ke lebih dari <b>400</b> klien meliputi individu, sekolah, kampus, usaha kecil dan menengah, perusahaan swasta nasional dan perusahaan multinasional di berbagai wilayah Indonesia).</p>
    <div class="w3-panel w3-leftbar w3-light-grey">
      <p><i>"Bersama kami Anda dapat terampil menggunakan software akuntansi ACCURATE tanpa Anda harus mahir akuntansi." </i>FAC Institute.</p>
    </div>
    <!-- <img src="<?php echo getBaseUrl() ?>images/13256189_1000296093358607_5291351703540609579_n_Fotor.jpg" style="width:100%;max-width:1200px" class="w3-margin-top"> -->
    <!-- <p><strong>desc:</strong> Pelatihan Pembukuan Hebat untuk UKM Hebat yang diselenggarakan oleh UKMC FEB UI</p> -->
  </div>
</div>



<!-- Menu Container -->
<div class="w3-container" id="menu">
  <div class="w3-content" style="">
    <h5 class="w3-center w3-padding-48"><span class="w3-tag w3-wide">Layanan & Jasa Kami</span></h5>
    <div class="w3-row w3-center w3-card-2 w3-padding">
      <a href="javascript:void(0)" onclick="openMenu(event, 'Eat');" id="myLink">
        <div class="w3-col s4 tablink">Kursus Akuntansi</div>
      </a>
      <a href="javascript:void(0)" onclick="openMenu(event, 'Drinks');">
        <div class="w3-col s4 tablink">Training Software Akuntansi</div>

      </a>
      <a href="javascript:void(0)" onclick="openMenu(event, 'test');">
        <div class="w3-col s4 tablink">Accounting Service & Konsultasi</div>
      </a>
    </div>

<div id="Eat" class="w3-container menu w3-padding-48 w3-card-2">
  <div class="w3-row" id="menu">
    <div class="w3-col l6 w3-padding-large">
      <p>Kami menyelenggarakan kelas kursus komputer akuntansi dengan menggunakan software Accurate.
         Kami adalah adalah <b>ACCURATE Authorized Training Center (AATC)</b>,
         yaitu lembaga pendidikan komputerisasi yang mendapatkan izin dari CPSSOFT untuk memberikan pelatihan software ACCURATE di seluruh Indonesia.
      </p>
      <p style="font-style:italic;text-align: justify">Kami menyediakan kelas kursus komputer akuntansi untuk :
          <ol>
              <li>Pemilik Bisnis</li>
              <li>Pemilik Usaha Kecil Menengah</li>
              <li>Pimpinan Perusahaan</li>
              <li>Manajer Keuangan</li>
              <li>Staff Accounting</li>
              <li>Pelamar yang sedang mencari pekerjaan</li>
              <li>HRD yang ingin menilai kompetensi karyawan bagian keuangan</li>
              <li>SDM di bagian akuntansi pembukuan</li>
              <li>SDM yang akan ditempatkan di bidang akuntansi keuangan</li>
              <li>Praktisi IT yang bergerak dalam pemrograman akuntansi</li>
              <li>Manajer bidang lain yang ingin mengetahui akuntansi secara umum</li>
              <li>Masyarakat umum yang berminat untuk mempelajari akuntansi.</li>
          </ol>
      </p>
    </div>
    <div class="w3-col l6 w3-padding-large">
      <img src="<?php echo getBaseUrl() ?>images/Medieval2.jpg" class="w3-round w3-image w3-opacity-min" alt="Menu" width="500" height="750">
    </div>
  </div>
<div class="w3-padding-large">
  <p> Kelas kursus akuntansi dapat dilakukan secara klasikal (berkelompok) atau private (perseorangan) di kantor kami. Kelas kursus akuntansi terbagi menjadi : Kursus Microsoft Excel dan kursus ACCURATE. Materi kursus meliputi pengenalan dasar - dasar akuntansi, pembuatan transaksi keuangan dan pembuatan laporan keuangan menggunakan Microsoft Excel / ACCURATE dengan efektif dan efisien. </p>
</div>
</div>

    <div id="Drinks" class="w3-container menu w3-padding-48 w3-card-2">
    </div>
    <div id="test" class="w3-container menu w3-padding-48 w3-card-2">
      <h5>Coffee</h5>
      <h5>Soda</h5>
      <p class="w3-text-grey">Coke, Sprite, Fanta, etc. 2.50</p>
    </div>
  </div>

  <div class="w3-light-grey w3-padding-large w3-padding-32 w3-margin-top" id="contact">
    <div class="w3-row-padding" id="myGrid" style="margin-bottom:128px">


      <h3 class="w3-center">Klien Kami</h3>
      <hr>
      <div class="leftside">
                                  <div class="homebox">
                                      <ul id="icons">
                                          <li>
                                              <a href="<?php echo $product; ?>">
                                                  <img src="<?= getBaseUrl() ?>images/homepage/image4780.png">
                                              </a>
                                          </li>
                                          <li>
                                              <a href="<?php echo $category; ?>">
                                                  <img src="<?= getBaseUrl() ?>images/homepage/image3634.png">
                                              </a>
                                          </li>
                                          <li>
                                              <a href="<?php echo $manufacturer; ?>">
                                                  <img src="<?= getBaseUrl() ?>images/homepage/image4352.png">
                                              </a>
                                          </li>
                                          <li>
                                              <a href="<?php echo $banner; ?>">
                                                  <img src="<?= getBaseUrl() ?>images/homepage/image4958.png">
                                              </a>
                                          </li>
                                          <li>
                                              <a href="<?php echo $attributes; ?>">
                                                  <img src="<?= getBaseUrl() ?>images/homepage/image5084.png">
                                              </a>
                                          </li>
                                          <li>
                                              <a href="<?php echo $featured; ?>">
                                                  <img src="<?= getBaseUrl() ?>images/homepage/image5594.png">
                                              </a>
                                          </li>
                                          <li>
                                              <a href="<?php echo $options2; ?>">
                                                  <img src="<?= getBaseUrl() ?>images/homepage/image6168.png">
                                              </a>
                                          </li>
                                          <li>
                                              <a href="<?php echo $vouchers; ?>">
                                                  <img src="<?= getBaseUrl() ?>images/homepage/image6230.png">
                                              </a>
                                          </li>
                                          <li>
                                              <a href="<?php echo $module; ?>">
                                                  <img src="<?= getBaseUrl() ?>images/homepage/image6468.png">
                                              </a>
                                          </li>
                                          <li>
                                              <a href="<?php echo $shipping; ?>">
                                                  <img src="<?= getBaseUrl() ?>images/homepage/image7042.png">
                                              </a>
                                          </li>
                                          <li>
                                              <a href="<?php echo $shipping; ?>">
                                                  <img src="<?= getBaseUrl() ?>images/homepage/image3578.png">
                                              </a>
                                          </li>
                                          <li>
                                              <a href="<?php echo $shipping; ?>">
                                                  <img src="<?= getBaseUrl() ?>images/homepage/image3640.png">
                                              </a>
                                          </li>
                                          <li>
                                              <a href="<?php echo $shipping; ?>">
                                                  <img src="<?= getBaseUrl() ?>images/homepage/rect32011.png">
                                              </a>
                                          </li>
                                          <li>
                                              <a href="<?php echo $shipping; ?>">
                                                  <img src="<?= getBaseUrl() ?>images/homepage/rect320122.png">
                                              </a>
                                          </li>
                                          <li>
                                              <a href="<?php echo $shipping; ?>">
                                                  <img src="<?= getBaseUrl() ?>images/homepage/rect3201f.png">
                                              </a>
                                          </li>
                                          <li>
                                              <a href="<?php echo $shipping; ?>">
                                                  <img src="<?= getBaseUrl() ?>images/homepage/rect32011e.png">
                                              </a>
                                          </li>
                                          <li>
                                              <a href="<?php echo $shipping; ?>">
                                                  <img src="<?= getBaseUrl() ?>images/homepage/rect3201g.png">
                                              </a>
                                          </li>
                                          <li>
                                              <a href="<?php echo $shipping; ?>">
                                                  <img src="<?= getBaseUrl() ?>images/homepage/rect3201a.png">
                                              </a>
                                          </li>
                                          <li>
                                              <a href="<?php echo $shipping; ?>">
                                                  <img src="<?= getBaseUrl() ?>images/homepage/rect3201b.png">
                                              </a>
                                          </li>
                                          <li>
                                              <a href="<?php echo $shipping; ?>">
                                                  <img src="<?= getBaseUrl() ?>images/homepage/rect3201c.png">
                                              </a>
                                          </li>
                                          <li>
                                              <a href="<?php echo $shipping; ?>">
                                                  <img src="<?= getBaseUrl() ?>images/homepage/rect3201d.png">
                                              </a>
                                          </li>
                                      </ul>
                                  </div>
                              </div>


<hr>
      <h3 class="w3-center">Dokumentasi Kegiatan</h3>
      <hr>
      <!-- <div class="w3-third">
        <p>Pelatihan Pembukuan Hebat untuk UKM Hebat yang diselenggarakan oleh UKMC FEB UI</p>
        <img src="<?php echo getBaseUrl() ?>images/homepage/13268105_1000300386691511_5269298106051400217_o.jpg" alt="Image description" style="width:100%" /> </a>
        <img src="<?php echo getBaseUrl() ?>images/homepage/13235103_1000299510024932_3894420669409310754_o.jpg" alt="Image description" style="width:100%" /> </a>
      </div>
      <div class="w3-third">
        <p>Training Accurate untuk karyawan di Arsari Group</p>
        <img src="<?php echo getBaseUrl() ?>images/homepage/12891768_968708069850743_7436250094960452284_o.jpg" style="width:100%">
        <img src="<?php echo getBaseUrl() ?>images/homepage/12473566_968708176517399_5630018724927164532_o.jpg" style="width:100%">
      </div>
      <div class="w3-third">
        <p>Pelatihan Accurate untuk dosen & mahasiswa jurusan akuntansi di IBI KOSGORO 57</p>
        <img src="<?php echo getBaseUrl() ?>images/homepage/12888496_968708426517374_610873257017712311_o.jpg" style="width:100%">
        <img src="<?php echo getBaseUrl() ?>images/homepage/12672119_968708466517370_7816653324994776514_o.jpg" style="width:100%">
      </div> -->

<div id="instafeed"></div>

    </div>
  </div>




</div>
    </div>







<script>

    // Tabbed Menu
function openMenu(evt, menuName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("menu");
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
     tablinks[i].className = tablinks[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(menuName).style.display = "block";
  evt.currentTarget.firstElementChild.className += " w3-dark-grey";
}
document.getElementById("myLink").click();




    </script>





    <script src="<?php echo getBaseUrl() ?>assets/homepage/js/jquery-2.1.4.min.js"></script>
    <!-- <script src="<?php echo getBaseUrl() ?>assets/homepage/js/foundation.min.js"></script>
    <script>
        $(document).foundation();
    </script>
    <script type="text/javascript" src="<?php echo getBaseUrl() ?>assets/homepage/js/zcom.js"></script> -->
    <!-- Include js plugin -->
    <!-- <script src="<?php echo getBaseUrl() ?>assets/homepage/owl-carousel/owl.carousel.js"></script> -->
    <!-- <script src="<?php echo getBaseUrl() ?>assets/homepage/js/home.js"></script> -->
    <!-- <script src="<?php echo getBaseUrl() ?>assets/homepage/dist/photoswipe.min.js"></script> -->
    <!-- UI JS file -->
    <!-- <script src="<?php echo getBaseUrl() ?>assets/homepage/dist/photoswipe-ui-default.min.js"></script> -->

    <!-- <script src="<?= getBaseUrl() ?>assets/homepage/dist/photoswipe-ui-default.min.js"></script> -->



    <script type="text/javascript">
        function visitorDetector(window) {
            {
                var unknown = '-';

                // browser
                var nVer = navigator.appVersion;
                var nAgt = navigator.userAgent;
                var browser = navigator.appName;
                var version = '' + parseFloat(navigator.appVersion);
                var majorVersion = parseInt(navigator.appVersion, 10);
                var nameOffset, verOffset, ix;

                // Opera
                if ((verOffset = nAgt.indexOf('Opera')) != -1) {
                    browser = 'Opera';
                    version = nAgt.substring(verOffset + 6);
                    if ((verOffset = nAgt.indexOf('Version')) != -1) {
                        version = nAgt.substring(verOffset + 8);
                    }
                }
                // Opera Next
                if ((verOffset = nAgt.indexOf('OPR')) != -1) {
                    browser = 'Opera';
                    version = nAgt.substring(verOffset + 4);
                }
                // MSIE
                else if ((verOffset = nAgt.indexOf('MSIE')) != -1) {
                    browser = 'Microsoft Internet Explorer';
                    version = nAgt.substring(verOffset + 5);
                }
                // Chrome
                else if ((verOffset = nAgt.indexOf('Chrome')) != -1) {
                    browser = 'Chrome';
                    version = nAgt.substring(verOffset + 7);
                }
                // Safari
                else if ((verOffset = nAgt.indexOf('Safari')) != -1) {
                    browser = 'Safari';
                    version = nAgt.substring(verOffset + 7);
                    if ((verOffset = nAgt.indexOf('Version')) != -1) {
                        version = nAgt.substring(verOffset + 8);
                    }
                }
                // Firefox
                else if ((verOffset = nAgt.indexOf('Firefox')) != -1) {
                    browser = 'Firefox';
                    version = nAgt.substring(verOffset + 8);
                }
                // MSIE 11+
                else if (nAgt.indexOf('Trident/') != -1) {
                    browser = 'Microsoft Internet Explorer';
                    version = nAgt.substring(nAgt.indexOf('rv:') + 3);
                }
                // Other browsers
                else if ((nameOffset = nAgt.lastIndexOf(' ') + 1) < (verOffset = nAgt.lastIndexOf('/'))) {
                    browser = nAgt.substring(nameOffset, verOffset);
                    version = nAgt.substring(verOffset + 1);
                    if (browser.toLowerCase() == browser.toUpperCase()) {
                        browser = navigator.appName;
                    }
                }
                // trim the version string
                if ((ix = version.indexOf(';')) != -1) version = version.substring(0, ix);
                if ((ix = version.indexOf(' ')) != -1) version = version.substring(0, ix);
                if ((ix = version.indexOf(')')) != -1) version = version.substring(0, ix);

                majorVersion = parseInt('' + version, 10);
                if (isNaN(majorVersion)) {
                    version = '' + parseFloat(navigator.appVersion);
                    majorVersion = parseInt(navigator.appVersion, 10);
                }

                // mobile version
                var mobile = /Mobile|mini|Fennec|Android|iP(ad|od|hone)/.test(nVer);


                // system
                var os = unknown;
                var clientStrings = [{
                        s: 'Windows 10',
                        r: /(Windows 10.0|Windows NT 10.0)/
                    },
                    {
                        s: 'Windows 8.1',
                        r: /(Windows 8.1|Windows NT 6.3)/
                    },
                    {
                        s: 'Windows 8',
                        r: /(Windows 8|Windows NT 6.2)/
                    },
                    {
                        s: 'Windows 7',
                        r: /(Windows 7|Windows NT 6.1)/
                    },
                    {
                        s: 'Windows Vista',
                        r: /Windows NT 6.0/
                    },
                    {
                        s: 'Windows Server 2003',
                        r: /Windows NT 5.2/
                    },
                    {
                        s: 'Windows XP',
                        r: /(Windows NT 5.1|Windows XP)/
                    },
                    {
                        s: 'Windows 2000',
                        r: /(Windows NT 5.0|Windows 2000)/
                    },
                    {
                        s: 'Windows ME',
                        r: /(Win 9x 4.90|Windows ME)/
                    },
                    {
                        s: 'Windows 98',
                        r: /(Windows 98|Win98)/
                    },
                    {
                        s: 'Windows 95',
                        r: /(Windows 95|Win95|Windows_95)/
                    },
                    {
                        s: 'Windows NT 4.0',
                        r: /(Windows NT 4.0|WinNT4.0|WinNT|Windows NT)/
                    },
                    {
                        s: 'Windows CE',
                        r: /Windows CE/
                    },
                    {
                        s: 'Windows 3.11',
                        r: /Win16/
                    },
                    {
                        s: 'Android',
                        r: /Android/
                    },
                    {
                        s: 'Open BSD',
                        r: /OpenBSD/
                    },
                    {
                        s: 'Sun OS',
                        r: /SunOS/
                    },
                    {
                        s: 'Linux',
                        r: /(Linux|X11)/
                    },
                    {
                        s: 'iOS',
                        r: /(iPhone|iPad|iPod)/
                    },
                    {
                        s: 'Mac OS X',
                        r: /Mac OS X/
                    },
                    {
                        s: 'Mac OS',
                        r: /(MacPPC|MacIntel|Mac_PowerPC|Macintosh)/
                    },
                    {
                        s: 'QNX',
                        r: /QNX/
                    },
                    {
                        s: 'UNIX',
                        r: /UNIX/
                    },
                    {
                        s: 'BeOS',
                        r: /BeOS/
                    },
                    {
                        s: 'OS/2',
                        r: /OS\/2/
                    },
                    {
                        s: 'Search Bot',
                        r: /(nuhk|Googlebot|Yammybot|Openbot|Slurp|MSNBot|Ask Jeeves\/Teoma|ia_archiver)/
                    }
                ];
                for (var id in clientStrings) {
                    var cs = clientStrings[id];
                    if (cs.r.test(nAgt)) {
                        os = cs.s;
                        break;
                    }
                }

                var osVersion = unknown;

                if (/Windows/.test(os)) {
                    osVersion = /Windows (.*)/.exec(os)[1];
                    os = 'Windows';
                }

                switch (os) {
                    case 'Mac OS X':
                        osVersion = /Mac OS X (10[\.\_\d]+)/.exec(nAgt)[1];
                        break;

                    case 'Android':
                        osVersion = /Android ([\.\_\d]+)/.exec(nAgt)[1];
                        break;

                    case 'iOS':
                        osVersion = /OS (\d+)_(\d+)_?(\d+)?/.exec(nVer);
                        osVersion = osVersion[1] + '.' + osVersion[2] + '.' + (osVersion[3] | 0);
                        break;
                }


            }

            window.jscd = {
                browser: browser,
                browserVersion: version,
                browserMajorVersion: majorVersion,
                mobile: mobile,
                os: os,
                osVersion: osVersion
            };

            var jsonDatas = {
                'ip': <?php echo "'".$_SERVER['REMOTE_ADDR']."'"; ?>,
                'os': jscd.os + ' ' + jscd.osVersion,
                'browser': jscd.browser + ' ' + jscd.browserMajorVersion,
                'mobile': jscd.mobile
            }
            //alert(peserta);

            $.ajax({
                type: "POST",
                url: "controller/visitor",
                data: {
                    'jsonData': jsonDatas
                },
                cache: false,
                success: function(data) {
                    // alert(data);
                    //window.location.replace("index.php");
                }
            }); //end ajax

        }(this);

        $(document).ready(function() {
            visitorDetector(window);

              // hide #back-top first
              $("#back-top").hide();

              // fade in #back-top
              $(function () {
                $(window).scroll(function () {
                  if ($(this).scrollTop() > 100) {
                    $('#back-top').fadeIn();
                  } else {
                    $('#back-top').fadeOut();
                  }
                });

                // scroll body to 0px on click
                $('#back-top .fi-arrow-up').click(function () {
                  $('body,html').animate({
                    scrollTop: 0
                  }, 800);
                  return false;
                });
              });

        });
    </script>


<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Course",
  "name": "Training Software Accurate, Audit data dan Kursus Akuntansi",
  "description": "Kami mengajari, tanpa harus anda memahami akuntansi.",
  "provider": {
    "@type": "Organization",
    "name": "FAC Institute",
    "sameAs": "http://www.fac-institute.com"
  }
}
</script>

    <script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "ProfessionalService",
  "@id": "https://fac-institute.com/",
  "name": "FAC Institute",
  "image": "https://fac-institute.com/images/logo-fac.jpg",
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "jalan no., Jl. Pangkalan Jati I A No.8, Jatiwaringin",
    "addressLocality": "Jakarta Timur",
    "addressRegion": "Jakarta",
    "postalCode": "13620",
    "addressCountry": "Indoneia"
  },
  "geo": {
    "@type": "GeoCoordinates",
    "latitude": -6.248042,
    "longitude": 106.907903
  },
  "telephone": "+6281290083983",
  "potentialAction": {
    "@type": "ReserveAction",
    "target": {
      "@type": "EntryPoint",
      "urlTemplate": "https://fac-institute.com/order",
      "inLanguage": "id",
      "actionPlatform": [
        "http://schema.org/DesktopWebPlatform",
        "http://schema.org/IOSPlatform",
        "http://schema.org/AndroidPlatform"
      ]
    },
    "result": {
      "@type": "Reservation",
      "name": "Pesan Training Software Accurate/Accounting Service"
    }
  }
}
</script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/576deedca4fa94c76a6d4cad/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->


<script>
function myMap()
{
  myCenter=new google.maps.LatLng(-6.21462, 106.84513);
  var mapOptions= {
    center:myCenter,
    zoom:12, scrollwheel: false, draggable: false,
    mapTypeId:google.maps.MapTypeId.ROADMAP
  };
  var map=new google.maps.Map(document.getElementById("googleMap"),mapOptions);

  var marker = new google.maps.Marker({
    position: myCenter,
  });
  marker.setMap(map);
}

// Modal Image Gallery
function onClick(element) {
  document.getElementById("img01").src = element.src;
  document.getElementById("modal01").style.display = "block";
  var captionText = document.getElementById("caption");
  captionText.innerHTML = element.alt;
}


// Toggle between showing and hiding the sidenav when clicking the menu icon
var mySidenav = document.getElementById("mySidenav");

function w3_open() {
    if (mySidenav.style.display === 'block') {
        mySidenav.style.display = 'none';
    } else {
        mySidenav.style.display = 'block';
    }
}

// Close the sidenav with the close button
function w3_close() {
    mySidenav.style.display = "none";
}
</script>



</body>

</html>
