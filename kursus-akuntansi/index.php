<?php
function getBaseUrl(){
	$currentPath = $_SERVER['PHP_SELF'];$pathInfo = pathinfo($currentPath);$hostName = $_SERVER['HTTP_HOST'];$protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https://'?'https://':'http://';
	return 'https://'.$hostName."/";
	// return 'http://localhost/facftp/';
}

include_once '../model/Kursus.php';
$kursus = new Kursus();

$jadwal = json_decode($kursus->getJadwal(date('m'),date('Y')));

if (date('m')==12) {
	$thn = intval(date('Y')) + 1;
	$bln = 1;
	$jadwalBulanDepan = json_decode($kursus->getJadwal(intval($bln),intval($thn)));
}else{
	$bln = intval(date('m'))+1;
	$jadwalBulanDepan = json_decode($kursus->getJadwal($bln,date('Y')));
}
$sekarang = strtotime(date('Y-m-d'));

?>

<!DOCTYPE html>
	<html lang="zxx" class="no-js">
	<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="thumbnailUrl" content="<?= $data->base_url ?>_caramel/assets/img/g44508.png" />
    <meta name="description" content="" />
    <meta content="" />
    <meta name="keywords" content="Training,Akuntansi,ACCURATE,Jasa">
    <meta name="author" content="Rifzky Alam, Dino Damara">
    <title>FAC Institute</title>
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
    <!--
      CSS
      ============================================= -->
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/linearicons.css">
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/bootstrap.css">
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/magnific-popup.css">
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/nice-select.css">
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/animate.min.css">
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/owl.carousel.css">
    <link rel="stylesheet" href="<?= $data->base_url ?>assets/homepage2019/css/main.css">
			<style media="screen">
			.banner-content h1 {
			    font-size: 38px;
			}
			.nav-menu a {
					padding: 15px 8px 1px 8px;
					font-size: 16px;
					font-weight: 500;
			}
			.nav-menu ul li a {
					font-size: 14px;
			}

			.menu-active {
			  border-bottom: 5px solid #df003a;
			}

			.navbar {
			    padding: 0.2rem 1rem;
			}


			.circle {
			  padding: 13px 20px;
			  border-radius: 50%;
			  background-color: #f74e4e;
			  color: #fff;
			  max-height: 50px;
			  z-index: 2;
			}

			.how-it-works.row .col-2 {
			  align-self: stretch;
			}
			.how-it-works.row .col-2::after {
			  content: "";
			  position: absolute;
			  border-left: 3px solid #ED8D8D;
			  z-index: 1;
			}
			.how-it-works.row .col-2.bottom::after {
			  height: 50%;
			  left: 50%;
			  top: 50%;
			}
			.how-it-works.row .col-2.full::after {
			  height: 100%;
			  left: calc(50% - 3px);
			}
			.how-it-works.row .col-2.top::after {
			  height: 50%;
			  left: 50%;
			  top: 0;
			}

			.timeline div {
			  padding: 0;
			  height: 40px;
			}
			.timeline hr {
			  border-top: 3px solid #ED8D8D;
			  margin: 0;
			  top: 17px;
			  position: relative;
			}
			.timeline .col-2 {
			  display: flex;
			  overflow: hidden;
			}
			.timeline .corner {
			  border: 3px solid #ED8D8D;
			  width: 100%;
			  position: relative;
			  border-radius: 15px;
			}
			.timeline .top-right {
			  left: 50%;
			  top: -50%;
			}
			.timeline .left-bottom {
			  left: -50%;
			  top: calc(50% - 3px);
			}
			.timeline .top-left {
			  left: -50%;
			  top: -50%;
			}
			.timeline .right-bottom {
			  left: 50%;
			  top: calc(50% - 3px);
			}


/* tooltip */
			.tooltip {
		      position: relative;
		      display: inline-block;
		      border-bottom: 1px dotted black;
		  }

		  .tooltip .tooltiptext {
		      visibility: hidden;
		      width: 200px;
		      background-color: black;
		      color: #fff;
		      text-align: center;
		      border-radius: 6px;
		      padding: 10px;

		      /* Position the tooltip */
		      position: absolute;
		      z-index: 1;
		      bottom: 100%;
		      left: 50%;
		      margin-left: -90px;
		  }

		  .tooltip:hover .tooltiptext {
		      visibility: visible;
		  }

			</style>
      <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
		</head>
		<body>

      <?php include_once 'header.kursus-acc.php'; ?>



			<?php $page='orderan' ?>
		  <h4>header</h4>
		  <?php include_once '../top-nav.php'; ?>

		  <div class="w3-content w3-padding-16" style="max-width:1000px">
		    <h2>Jadwal Kursus Akuntansi dan Accurate</h2>
		    <p style="font-size:12px;margin-top:-10px">By : FAC-Institute & Bakat Cendekia</p>
		    <p><b><?php echo $kursus->getNamaBulan(date('m')); ?></b></p>
		    <table class="w3-table-all w3-centered">
		      <thead>
		        <tr class="w3-blue">
		          <th>Lokasi</th>
		          <th>Nama Kelas</th>
		          <th>Hari Kelas</th>
		          <th>Mulai Kelas <br><span style="color:black">(klik tanggal utk detail)</span></th>
		          <th colspan="2">Durasi Kelas</th>
		          <th>Investasi</th>
		          <th>Link Daftar</th>
		        </tr>
		      </thead>
		      <tbody>
		        <?php if (count($jadwal)!=0): ?>
		        <?php for ($i=0; $i < count($jadwal); $i++) { ?>
		        <?php if (strtotime($jadwal[$i]->mulai_kursus)<$sekarang): ?>
		          <tr>
		          <?php else: ?>
		          <tr>
		        <?php endif ?>
		        <?php $asik = implode(' # ', $kursus->getArrayOfCal($jadwal[$i]->id)); ?>
		          <td>
		            <a style="color:#3a9fe5;text-decoration:none" href="https://www.google.co.id/maps/search/<?php echo $jadwal[$i]->map; ?>" target="_blank">
		            <?php echo $jadwal[$i]->lokasi_kursus; ?>
		            </a>
		          </td>
		          <td><?php echo $jadwal[$i]->nama_kursus; ?></td>
		          <td><?php echo $jadwal[$i]->hari ?></td>
		          <?php
		          $myDate = explode('-', $jadwal[$i]->mulai_kursus);
		          ?>

		          <td>
		            <div class="tooltip"><?php echo $myDate[2].'-'.$myDate[1].'-'.$myDate[0] ?>
		           <span class="tooltiptext"><?php echo $asik.' -- Dimulai pukul ('.$jadwal[$i]->jam.')' ?></span></div>
		          </td>
		          <td ><?php echo $jadwal[$i]->hari ?></td>
		          <td><?php echo $jadwal[$i]->durasi ?></td>
		          <td>Rp <?php echo number_format($jadwal[$i]->investasi) ?>.-</td>
		          <?php if (strtotime($jadwal[$i]->mulai_kursus)<$sekarang): ?>
		          <td><button href="#" class="w3-button w3-red w3-disabled" style="font-size:12px">Ongoing</button>
		          </td>
		            <?php else: ?>
		          <td><a href=<?php echo "'".getBaseUrl()."kursus-akuntansi/daftar?id=".$jadwal[$i]->id."'"; ?> class="w3-button w3-green">Daftar</a>
		          </td>
		          <?php endif ?>
		        </tr>
		        <?php } ?>

		          <?php else: ?>
		            <tr>
		              <td  colspan="7">
		              Maaf, untuk saat ini belum ada data yang masuk pada bulan tertentu atau kami sedang dalam perbaikan sistem.
		              </td>
		            </tr>
		        <?php endif ?>
		      </tbody>
		    </table>


		    <div >
		      <p style="color:red;font-weight:bold">Catatan</p>

		        <ul>
		<li>Mohon membawa Laptop Sendiri untuk <u>di Install Aplikasi Edukasi Accurate-5</u></li>
		<li>Kelas Reguler&nbsp; Max. 7-Peserta/Kelas</li>
		<li>Bagi peserta yang belum mengerti Akuntansi, Kami menyediakan kelas khusus Dasar-Dasar Akuntansi.</li>
		<li>Mendaftar kelas yang sama &amp; bersamaan , kami beri Potongan 10% per peserta (untuk 2-peserta), dan Potongan 15% per peserta (untuk 3-peserta)</li>
		<li>Selain dari waktu yang sudah terjadwal di bawah ini, maka di Kategori kan sebagai kelas Private</li>
		</ul>
		    </div>



		    <div>
		      <p><b>
		        <?php
		          if (date('m') + 1 =='13') {
		            $tahunDepan = date('Y') + 1;
		            echo "Januari " . $tahunDepan;
		          }else{
		            echo $kursus->getNamaBulan(date('m')+1);
		          }
		        ?>
		      </b></p>
		    </div>
		    <table class="w3-table-all w3-centered">
		      <thead>
		        <tr class="w3-blue">
		          <th>Lokasi</th>
		          <th>Nama Kelas</th>
		          <th>Hari Kelas</th>
		          <th>Mulai Kelas</th>
		          <th colspan="2">Durasi Kelas</th>
		          <th>Investasi</th>
		          <th>Link Daftar</th>
		        </tr>
		      </thead>
		      <tbody>

		        <?php if (count($jadwalBulanDepan)!=0): ?>
		        <?php for ($i=0; $i < count($jadwalBulanDepan); $i++) { ?>
		        <?php $asik = implode(' # ', $kursus->getArrayOfCal($jadwalBulanDepan[$i]->id)); ?>
		        <tr>
		          <td>
		            <a style="color:#3a9fe5;text-decoration:none"  href="https://www.google.co.id/maps/search/<?php echo $jadwalBulanDepan[$i]->map; ?>" target="_blank">
		            <?php echo $jadwalBulanDepan[$i]->lokasi_kursus; ?>
		            </a>
		          </td>
		          <td><?php echo $jadwalBulanDepan[$i]->nama_kursus; ?></td>
		          <td><?php echo $jadwalBulanDepan[$i]->hari ?></td>
		          <?php
		          $myDate = explode('-', $jadwalBulanDepan[$i]->mulai_kursus);
		          ?>
		          <td <?php echo "id='".$jadwalBulanDepan[$i]->id."'" ?>>
		          <a data-placement="top" data-toggle="popover" title="Detail Tanggal" data-content="<?php echo $asik.' -- Dimulai pukul ('.$jadwalBulanDepan[$i]->jam.')' ?>">
		          <?php echo $myDate[2].'-'.$myDate[1].'-'.$myDate[0] ?>
		          </a>
		          </td>
		          <td><?php echo $jadwalBulanDepan[$i]->hari ?></td>
		          <td><?php echo $jadwalBulanDepan[$i]->durasi ?></td>
		          <td>Rp <?php echo number_format($jadwalBulanDepan[$i]->investasi) ?>.-</td>
		          <td><a href=<?php echo "'".getBaseUrl()."kursus-akuntansi/daftar?id=".$jadwalBulanDepan[$i]->id."'";?> class="w3-button w3-green">Daftar</a></td>
		        </tr>
		        <?php } ?>

		          <?php else:?>
		        <tr>
		          <td  colspan="7">
		            Maaf, untuk saat ini belum ada data yang masuk pada bulan tertentu atau kami sedang dalam perbaikan sistem.
		          </td>
		        </tr>

		        <?php endif ?>
		      </tbody>
		    </table>


		  </div>



	    <?php include_once 'footer.php'; ?>
      <?php include_once $data->homedir.'view/homepage/footer.homepage.php'; ?>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/jquery-2.2.4.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/vendor/bootstrap.min.js"></script>
      <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/easing.min.js"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/hoverIntent.js"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/superfish.min.js"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.ajaxchimp.min.js"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.magnific-popup.min.js"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/owl.carousel.min.js"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.sticky.js"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/jquery.nice-select.min.js"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/parallax.min.js"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/mail-script.js"></script>
      <script src="<?= $data->base_url ?>assets/homepage2019/js/main.js"></script>
      <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
			<script>
			AOS.init();
			$('.test1').click(function() {
			    var sectionTo = $(this).attr('href');
			    $('html, body').animate({
			      scrollTop: $(sectionTo).offset().top
			    }, 1000);
			});
			</script>
			<script type="text/javascript" src=<?php echo "'".getBaseUrl()."css/datepicker/js/bootstrap-datepicker.js'"; ?>></script>
			<script type="text/javascript">

			$(document).ready(function(){
				var apakah = 0;
				$('#cobaa').datepicker({
					 format: "yyyy-mm-dd"
				});
				$('#fr-accurateLainnya').hide();

					$('[data-toggle="popover"]').popover();


				$('#tambahTombol').click(function(){

						var cobayah = $('.tglPelaksanaan').length;
						var iseng = "tambahan" + cobayah;
						if ($('#tambahan').length){

						if (cobayah==2) {
								$('#tambahan').after("<div class='row' id='"+iseng+"'><div class='col-md-10'><div class='form-inline'><input name='rq[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"'+iseng+'"'+")'></span></div></div></div>");
						}else{
								var okeh = cobayah-1;
								var iseng = "tambahan" + okeh;
								$("#"+iseng).after("<div class='row' id='tambahan"+cobayah+"'><div class='col-md-10'><div class='form-inline'><input name='rq[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"tambahan'+cobayah+'"'+")'></span></div></div></div>");
						};

						 }else{
							$('#jajal').after("<div class='row' id='tambahan'><div class='col-md-10'><div class='form-inline'><input name='rq[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"'+"tambahan"+'"'+")'></span></div></div></div>");
					};

					$('.tglPelaksanaan').datepicker({
							format: 'yyyy-mm-dd'
					});

			});

				$('.tglPelaksanaan').datepicker({
							format: 'yyyy-mm-dd'
					});

			$('#try').click(function(){
				var cobaz = $("#example-getting-started option:selected").map(function () {return this.value;}).get().join(" # ");
				alert(cobaz);
			});

			});

			function removeElement(id) {
					$('#'+id).remove();
			}




			function getValue(){
				var x=document.getElementById("example-getting-started");
				for (var i = 0; i < x.options.length; i++) {
					 if(x.options[i].selected){
								if (x.options[i].value=='lainnya') {
									$('#fr-accurateLainnya').show();
								}else{
									$('#fr-accurateLainnya').hide();
								};
						}
				}
			}

			</script>
		</body>
	</html>
