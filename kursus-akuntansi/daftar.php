<?php 
session_start();
function getBaseUrl(){
  $currentPath = $_SERVER['PHP_SELF'];$pathInfo = pathinfo($currentPath);$hostName = $_SERVER['HTTP_HOST'];$protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https://'?'https://':'http://';
  return 'https://'.$hostName."/";
}
$sekarang = strtotime(date('Y-m-d'));
function messageBox($string){
	return "<script>alert('".$string."')</script>";
}

function pindahkan($path){
	return "<script>location.replace('".$path."');</script>";
}


include_once '../model/Kursus.php';
$kursus = new Kursus();

$jadwal = json_decode($kursus->getJadwal(date('m'),date('Y')));

if (date('m')==12) {
	$thn = intval(date('y')) + 1;
	$bln = 1;
	$jadwalBulanDepan = json_decode($kursus->getJadwal($bln,$thn));
}else{
	$bln = intval(date('m'))+1;
	$jadwalBulanDepan = json_decode($kursus->getJadwal($bln,date('Y')));
}


?>

<html>
<head>
	
	<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>Daftar Online Kursus Akuntansi dan Accurate</title>
	<link rel="stylesheet" type="text/css" href=<?php echo "'".getBaseUrl()."css/bootstrap.min.css"."'"; ?>>
	<link rel="shortcut icon" href=<?php echo "'".getBaseUrl()."images/favicon-fac.ico"."'"; ?>>
	<link rel="stylesheet" type="text/css" href=<?php echo "'".getBaseUrl()."css/datepicker/css/datepicker.css'"; ?>> 
	<link rel="stylesheet" type="text/css" href=<?php echo "'".getBaseUrl()."css/multiselect/css/bootstrap-multiselect.css'"; ?>> 
	<link rel="stylesheet" href=<?php echo "'".getBaseUrl()."css/styles.css"."'"; ?> />
	<link rel="stylesheet" type="text/css" href=<?php echo "'".getBaseUrl()."css/custom.css"."'"; ?>>
	<link rel="stylesheet" type="text/css" href=<?php echo "'".getBaseUrl()."css/set1.css"."'"; ?> />
	<script src=<?php echo "'".getBaseUrl()."js/jquery-1.11.1.js'"; ?>></script>
	<style type="text/css">
	#top-gap{
		padding-top: 80px;
	}
	.tengah{
		text-align: center;
		vertical-align: middle;
	}
	</style>
</head>
<body>
	<?php $page='orderan' ?>
	<div id='top-gap'></div>

	<div class='container' style="padding-bottom:40px">
		<?php include_once '../header.php'; ?>
		<div class="page-header">
			<h2>Daftar Kursus Akuntansi dan Accurate</h2>
			<p>By FAC-Institute & Bakat Cendekia</p>
		</div>

<?php 
if (isset($_SESSION['secure'])&&isset($_POST['d'])){
	
	if ($_SESSION['secure'] == $_POST['d']['captcha']){
		$id = md5($_POST['d']['email'].'#'.$_POST['d']['idKursus']);
		$inz = array(
			'id' => $id, 
			'nama'=> $_POST['d']['namaLengkap'],
			'tempat_lahir'=> $_POST['d']['tempatLahir'],
			'status'=>substr($id, 0,8),
			'tanggal_lahir'=> $_POST['d']['tanggalLahir'],
			'gender'=> $_POST['d']['gender'],
			'alamat'=> $_POST['d']['alamat'],
			'telepon'=> $_POST['d']['telp'],
			'wa'=> $_POST['d']['wa'],
			'email'=> $_POST['d']['email'],
			'pendidikan'=> $_POST['d']['pendidikan'],
			'nama_usaha'=> $_POST['d']['perusahaan'],
			'alamat_usaha'=> $_POST['d']['alamatPerusahaan'],
			'id_kursus'=>$_POST['d']['idKursus']
		);

		$inz = (object) $inz;

		if ($kursus->addStudent($inz)) {
			//echo messageBox("Data berhasil disimpan!");
			include_once '../model/Mail.php';
			include_once '../model/Surat.php';
			$facMail = new FACMail();
			$surat = new Surat();
			
			$myMail = array(
				'to' => $inz->email,
				'cc' => array('training@fac-institute.com','info.bakatcendekia@gmail.com'),
				'subject' => 'Registrasi Kursus FAC Institute',
				'content' =>  $surat->Kursus($inz->id)
			);

			$facMail->sendMailWithCC((object) $myMail);

			echo pindahkan('http://fac-institute.com/docs/reg-complete?nr='.$inz->id);
		}else{
			echo messageBox("Data gagal disimpan, jika ada keluhan harap hubungi admin kami");
		}
		/*
		echo "id: ".$inz->id."<br>";
		echo "nama: ".$inz->nama."<br>";
		echo "Tempat Lahir: ".$inz->tempat_lahir."<br>";
		echo "Tanggal Lahir: ".$inz->tanggal_lahir."<br>";
		echo "Gender: ".$inz->gender."<br>";
		echo "Alamat: ".$inz->alamat."<br>";
		echo "Telepon: ".$inz->telepon."<br>";
		echo "Whatsapp: ".$inz->wa."<br>";
		echo "Email: ".$inz->email."<br>";
		echo "Pendidikan: ".$inz->pendidikan."<br>";
		echo "Nama Usaha: ".$inz->nama_usaha."<br>";
		echo "Alamat Usaha: ".$inz->alamat_usaha."<br>";
		*/
		//echo messageBox("Data berhasil disimpan!");
		//echo pindahkan('http://www.fac-institute.com/kursus-akuntansi/');
		$_SESSION['secure'] = rand(1000,9999);	
	}else{
		echo messageBox("Cek kembali kode verifikasi!");
		$_SESSION['secure'] = rand(1000,9999);	
	}

}else{
	$_SESSION['secure'] = rand(1000,9999);
}



?>

		<form action="" method="post">

		<div class="row">
			<div class="col-md-10">
				<div class="form-group">
					<label>Nama Lengkap</label>
					<?php if (isset($_POST['d']['namaLengkap'])): ?>
							<input type="text" name="d[namaLengkap]" class="form-control" value="<?php echo $_POST['d']['namaLengkap']; ?>" placeholder="Nama sesuai dengan ijazah atau KTP">
						<?php else: ?>
							<input type="text" name="d[namaLengkap]" class="form-control" placeholder="Nama sesuai dengan ijazah atau KTP">
					<?php endif ?>
					
				</div>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-10">
				<div class="form-group">
					<label>Tempat, Tanggal Lahir</label>
					<div class="form-inline">
						<?php if (isset($_POST['d']['tempatLahir'])): ?>
						<input type="text" name="d[tempatLahir]" class="form-control" placeholder="Tempat" value="<?php echo $_POST['d']['tempatLahir']; ?>">
							<?php else: ?>
						<input type="text" name="d[tempatLahir]" class="form-control" placeholder="Tempat">
						<?php endif ?>
						
						<?php if (isset($_POST['d']['tanggalLahir'])): ?>
							<input type="text" name="d[tanggalLahir]" class="form-control" placeholder="tanggal lahir" data-date-viewmode="years" data-date="02-05-1994" id="ttl" value="<?php echo $_POST['d']['tanggalLahir']; ?>">
							<?php else: ?>
						<input type="text" name="d[tanggalLahir]" class="form-control" placeholder="tanggal lahir" data-date-viewmode="years" data-date="02-05-1994" id="ttl">
						<?php endif ?>
						
							
					</div>
					
				</div>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-10">
				<div class="form-group">
					<label>Jenis Kelamin</label>
					<select name="d[gender]" class="form-control">
						<option value="">--Pilih Jenis Kelamin--</option>
						<?php if (isset($_POST['d']['gender'])&&$_POST['d']['gender']=='l'): ?>
						<option value="l" selected>Laki-laki</option>
						<option value="p">Perempuan</option>
						<?php elseif (isset($_POST['d']['gender'])&&$_POST['d']['gender']=='p'): ?>
						<option value="l">Laki-laki</option>
						<option value="p" selected>Perempuan</option>
							<?php else: ?>
						<option value="l">Laki-laki</option>
						<option value="p">Perempuan</option>								
						<?php endif ?>

					</select>
				</div>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-10">
				<div class="form-group">
					<label>Alamat</label>
					<?php if (isset($_POST['d']['alamat'])): ?>
						<textarea name="d[alamat]" class="form-control"><?php echo $_POST['d']['alamat']; ?></textarea>
						<?php else: ?>
						<textarea name="d[alamat]" class="form-control"></textarea>
					<?php endif ?>
				</div>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-10">
				<div class="form-group">
					<label>Telepon aktif</label>
					<?php if (isset($_POST['d']['telp'])): ?>
						<input type="text" name="d[telp]" class="form-control" value="<?php echo $_POST['d']['telp']; ?>">
						<?php else: ?>
						<input type="text" name="d[telp]" class="form-control">
					<?php endif ?>
					

				</div>
			</div>
		</div>
		
			<div class="checkbox">
				<?php if (isset($_POST['d']['wa'])&&$_POST['d']['wa']=='y'): ?>
					<label><input type="checkbox" name="d[wa]" value="y" checked>Telpon Termasuk Whatsapp</label>
					<?php else: ?>
					<label><input type="checkbox" name="d[wa]" value="y">Telpon Termasuk Whatsapp</label>
				<?php endif ?>
				
			</div>
		<?php if (isset($_GET['id'])&&!empty($_GET['id'])): ?>

			<?php 
			include_once '../model/Kursus.php';
			$kursus = new Kursus();
			$jadwal = json_decode($kursus->getJadwalByID($_GET['id']));


			$kursusnya = intval(strtotime($jadwal[0]->mulai_kursus));
			// echo "<br>";
			$hariini = intval(strtotime(date('Y-m-d')));
			if ($kursusnya < $hariini) {
				
				//print_r($jadwal);
				echo messageBox('Kursus ini telah dimulai, silahkan memilih jadwal lainnya.'); 
				echo pindahkan('http://www.fac-institute.com/kursus-akuntansi/');
			}

			?>


		<div class="row" style="display:none;">
			<input type="text" name="d[idKursus]" value="<?php echo $_GET['id'];?>">
		</div>			
			<?php else: ?>
				<?php echo messageBox('Daftar online disediakan setelah memilih kursus, Silahkan memilih kursus terlebih dahulu.'); ?>
				<?php echo pindahkan('http://www.fac-institute.com/kursus-akuntansi/'); ?>
		<?php endif ?>



		<div class="row">
			<div class="col-md-10">
				<div class="form-group">
					<label>E-mail</label>
					<?php if (isset($_POST['d']['email'])): ?>
						<input type="text" name="d[email]" class="form-control" value="<?php echo $_POST['d']['email'] ?>" />
						<?php else: ?>
							<input type="text" name="d[email]" class="form-control" />
					<?php endif ?>
					
				</div>
			</div>
		</div>

		
		<div class="row">
			<div class="col-md-10">
				<div class="form-group">
					<label>Pendidikan Terakhir</label>
					<select class="form-control" name="d[pendidikan]">
						<?php if (isset($_POST['d']['pendidikan'])&&$_POST['d']['pendidikan']=='sma'): ?>
						<option value="sma" selected>SMA</option>
							<?php else: ?>
						<option value="sma">SMA</option>
						<?php endif ?>

						<?php if (isset($_POST['d']['pendidikan'])&&$_POST['d']['pendidikan']=='smk'): ?>
						<option value="smk" selected>SMK</option>
							<?php else: ?>
						<option value="smk">SMK</option>
						<?php endif ?>
						
						<?php if (isset($_POST['d']['pendidikan'])&&$_POST['d']['pendidikan']=='d3'): ?>
							<option value="d3" selected>D3</option>
							<?php else: ?>
							<option value="d3">D3</option>
						<?php endif ?>
						
						<?php if (isset($_POST['d']['pendidikan'])&&$_POST['d']['pendidikan']=='s1'): ?>
						<option value="s1" selected>Sarjana S1</option>	
							<?php else: ?>
						<option value="s1">Sarjana S1</option>
						<?php endif ?>

						<?php if (isset($_POST['d']['pendidikan'])&&$_POST['d']['pendidikan']=='s2'): ?>
							<option value="s2" selected>Sarjana S2</option>
						<?php else: ?>
							<option value="s2">Sarjana S2</option>
						<?php endif ?>
						
						<?php if (isset($_POST['d']['pendidikan'])&&$_POST['d']['pendidikan']=='lainnya'): ?>
							<option value="lainnya" selected>Lainnya</option>
						<?php else: ?>
							<option value="lainnya">Lainnya</option>
						<?php endif ?>
						
					</select>
				</div>
			</div>
			
		</div>

		<div class="row">
			<div class="col-md-10">
				<div class="form-group">
					<label>Nama Perusahaan</label>
					<?php if (isset($_POST['d']['perusahaan'])): ?>
							<input type="text" name="d[perusahaan]" class="form-control" placeholder="jika anda sudah bekerja, boleh di isi" value="<?php echo $_POST['d']['perusahaan']; ?>">					
						<?php else: ?>
							<input type="text" name="d[perusahaan]" class="form-control" placeholder="jika anda sudah bekerja, boleh di isi">
					<?php endif ?>
					
				</div>
			</div>
			
		</div>

		<div class="row">
			<div class="col-md-10">
				<div class="form-group">
					<label>Alamat Perusahaan</label>
					<?php if (isset($_POST['d']['alamatPerusahaan'])): ?>
						<textarea name="d[alamatPerusahaan]" class="form-control" placeholder="bisa juga di isi dengan alamat di google map"><?php echo $_POST['d']['alamatPerusahaan'] ?></textarea>
						<?php else: ?>
						<textarea name="d[alamatPerusahaan]" class="form-control" placeholder="bisa juga di isi dengan alamat di google map"></textarea>
					<?php endif ?>
					
				</div>
			</div>
		</div>

		<center>
		<div class="row">
			<div class="col-md-10">
				<img src="generate">
			</div>
		</div>
		</center>
		<br>
		<div class="row">
			<div class="col-md-10">
				<div class="form-group">
					<input type="text" name="d[captcha]" class="form-control" placeholder="tulis angka sesuai dengan gambar diatas">
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-10">
				<button class="btn btn-lg btn-success" style="width:100%">Submit</button>
			</div>
		</div>
		</form>
	<!--end container-->	
	</div>
	

	
	<script src=<?php echo "'".getBaseUrl()."js/bootstrap.min.js'"; ?>></script>
	<script src=<?php echo "'".getBaseUrl()."js/custom.js"."'"; ?>></script>
	<script src=<?php echo "'".getBaseUrl()."js/slippry.min.js"."'"; ?>></script>
	<script type="text/javascript" src=<?php echo "'".getBaseUrl()."css/multiselect/js/bootstrap-multiselect.js'"; ?>></script>
	<script type="text/javascript" src=<?php echo "'".getBaseUrl()."css/datepicker/js/bootstrap-datepicker.js'"; ?>></script>
	<script type="text/javascript">

	$(document).ready(function(){
		var apakah = 0;
		$('#cobaa').datepicker({
		   format: "yyyy-mm-dd"
		});

		$('#ttl').datepicker({
		   format: "yyyy-mm-dd"
		});



		$('#fr-accurateLainnya').hide();


		$('#tambahTombol').click(function(){
    
		    var cobayah = $('.tglPelaksanaan').length;
		    var iseng = "tambahan" + cobayah;
		    if ($('#tambahan').length){

        if (cobayah==2) {
            $('#tambahan').after("<div class='row' id='"+iseng+"'><div class='col-md-10'><div class='form-inline'><input name='rq[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"'+iseng+'"'+")'></span></div></div></div>");
        }else{
            var okeh = cobayah-1;
            var iseng = "tambahan" + okeh;
            $("#"+iseng).after("<div class='row' id='tambahan"+cobayah+"'><div class='col-md-10'><div class='form-inline'><input name='rq[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"tambahan'+cobayah+'"'+")'></span></div></div></div>");
        };
            //nilai +=1;
         }else{
        	$('#jajal').after("<div class='row' id='tambahan'><div class='col-md-10'><div class='form-inline'><input name='rq[tanggalPelaksanaan][]' class='form-control tglPelaksanaan'/><span class='btn btn-danger glyphicon-minus' onclick='removeElement("+'"'+"tambahan"+'"'+")'></span></div></div></div>");
    	};

    	$('.tglPelaksanaan').datepicker({
	        format: 'yyyy-mm-dd'
	    });
	    
	});

		$('.tglPelaksanaan').datepicker({
	        format: 'yyyy-mm-dd'
	    });

	$('#try').click(function(){
		var cobaz = $("#example-getting-started option:selected").map(function () {return this.value;}).get().join(" # ");
		alert(cobaz);
	});

	});

	function removeElement(id) {
	    $('#'+id).remove();
	}




	function getValue(){
	  var x=document.getElementById("example-getting-started");
	  for (var i = 0; i < x.options.length; i++) {
	     if(x.options[i].selected){
	          if (x.options[i].value=='lainnya') {
	          	$('#fr-accurateLainnya').show();
	          }else{
	          	$('#fr-accurateLainnya').hide();
	          };
	      }
	  }
	}

	</script>
</body>
</html>