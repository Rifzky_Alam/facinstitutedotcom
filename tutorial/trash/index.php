<?php
include_once '../model/Tutorial.php';
$ctrlTuts = new ControlTutorial();

if (isset($_GET['app'])&&$_GET['app']=='1'&&isset($_GET['id'])&&!empty($_GET['id'])) {
	$dataTuts=$ctrlTuts->selectById($_GET['id']);
}

function getValue($value){
	if (empty($value)||$value=='') {
		return 'Tutorial tidak ada';
	} else {
		return $value;
	}
}
?>

<?php
function base_url(){
  $currentPath = $_SERVER['PHP_SELF'];$pathInfo = pathinfo($currentPath);$hostName = $_SERVER['HTTP_HOST'];$protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https://'?'https://':'http://';
  return 'https://'.$hostName."/";
}
?>

<?php $jumlahData = $ctrlTuts->countAllTutorial() ?>
<?php echo "<script>console.log(".$jumlahData.")</script>";?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0" />
<meta property="og:type" content="article" />
<meta property="og:site_name" content="FAC INSTITUTE" />
<?php if (isset($_GET['app'])&&$_GET['app']=='1'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>
<meta property="og:title" content=<?php echo "'$dataTuts->judul'" ?> />
<meta property="og:description" content=<?php echo "'$dataTuts->judul'" ?> />
<meta name="description" content=<?php echo "'$dataTuts->judul'" ?> itemprop="description" />
<meta name="robots" content="index, follow" />
<?php else: ?>
<?php endif ?>
<title>Reading Page</title>
<meta property="og:url" content=<?php echo '"http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'"'; ?> />
<meta property="fb:app_id" content="454421948087465" />
<meta property="og:image" content="http://fac-institute.com/images/logo-fac_header.png" />
<meta property="og:image:height" content="200" />
<meta property="og:image:width" content="200" />
<meta name="thumbnailUrl" content="http://alix.sch.id/pendaftaran/images/logo-fac.jpg" itemprop="thumbnailUrl" />
<meta charset="utf-8">

<?php if (isset($_GET['app'])&&$_GET['app']=='1'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>
	<title><?php echo $dataTuts->judul; ?></title>
	<?php else:?>
	<title>Halaman Tutorial</title>
<?php endif ?>


<?php include_once '../links.php'; ?>
<link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/w3.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/animate.min.css">
<link href="https://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,100,300,500,600,700c" type="text/css" />
<!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inconsolata"> -->
<link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/style.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.10/lodash.min.js"></script>
<link href="https://fac-institute.com/administrasi/view/quotation/dist/easy-autocomplete.min.css" rel="stylesheet">
<link href="https://fac-institute.com/administrasi/view/quotation/dist/easy-autocomplete.themes.min.css" rel="stylesheet">
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Raleway", sans-serif}
/* input[type=text] {
    width: 100%;
    box-sizing: border-box;
		border: none;
		border-bottom: 1px solid #ccc;
    border-radius: 4px;
    font-size: 16px;
    background-color: white;
    background-image: url('<?php echo getBaseUrl() ?>_caramel/assets/img/searchicon.png');
    background-position: 10px 10px;
    background-repeat: no-repeat;
    padding: 12px 20px 12px 40px;
    -webkit-transition: width 0.4s ease-in-out;
    transition: width 0.4s ease-in-out;
} */


/* loader-ellips
------------------------- */

.loader-ellips {
  font-size: 20px;
  position: relative;
  width: 4em;
  height: 1em;
  margin: 10px auto;
}

.loader-ellips__dot {
  display: block;
  width: 1em;
  height: 1em;
  border-radius: 0.5em;
  background: #555;
  position: absolute;
  animation-duration: 0.5s;
  animation-timing-function: ease;
  animation-iteration-count: infinite;
}

.loader-ellips__dot:nth-child(1),
.loader-ellips__dot:nth-child(2) {
  left: 0;
}
.loader-ellips__dot:nth-child(3) { left: 1.5em; }
.loader-ellips__dot:nth-child(4) { left: 3em; }

@keyframes reveal {
  from { transform: scale(0.001); }
  to { transform: scale(1); }
}

@keyframes slide {
  to { transform: translateX(1.5em) }
}

.loader-ellips__dot:nth-child(1) {
  animation-name: reveal;
}

.loader-ellips__dot:nth-child(2),
.loader-ellips__dot:nth-child(3) {
  animation-name: slide;
}

.loader-ellips__dot:nth-child(4) {
  animation-name: reveal;
  animation-direction: reverse;
}

/* loader-wheel
------------------------- */

.loader-wheel {
  font-size: 64px; /* change size here */
  position: relative;
  height: 1em;
  width: 1em;
  padding-left: 0.45em;
  overflow: hidden;
  margin: 0 auto;
  animation: loader-wheel-rotate 0.5s steps(12) infinite;
}

.loader-wheel i {
  display: block;
  position: absolute;
  height: 0.3em;
  width: 0.1em;
  border-radius: 0.05em;
  background: #333; /* change color here */
  opacity: 0.8;
  transform: rotate(-30deg);
  transform-origin: center 0.5em;
}

@keyframes loader-wheel-rotate {
  from { transform: rotate(0deg); }
  to { transform: rotate(360deg); }
}
</style>
<script>
function copyURL() {
  var copyText = document.getElementById("myInput");
	copyText.value = window.location.href;
  copyText.select();
  document.execCommand("copy");
  alert("Copied the text: " + copyText.value);
}
</script>
</head>
<body>
  <?php include_once '../top-nav.php'; ?>
  <?php include_once '../modal-order.php'; ?>
  <?php function SectionHtml(){?>
  <div id="read" class="w3-container w3-padding-64" id="about">
    <?php }?>
    <?php if (isset($_GET['app'])&&$_GET['app']=='1'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>
    <?php SectionHtml() ?>
    <div class="w3-content w3-padding-32" >
      <h2 class="">
        <?php echo getValue(@$dataTuts->judul); ?>
      </h2>
      <?php echo $dataTuts->isi; ?>
			<div class="w3-row">
				<div class="w3-container w3-twothird">
					<input type="text" value="" class="w3-input" type="text" id="myInput">
				</div>
				<div class="w3-container w3-third">
					<button onclick="copyURL()" class="w3-button w3-teal">+</button>
					<i class="fa fa-trash"></i>
				</div>
			</div>
    </div>



  </div>
  <?php else:?>
	<?php SectionHtml() ?>
	<div class="w3-container w3-padding-32">
		<h5 class="w3-center w3-padding-16"><span class="w3-tag w3-wide">Solution Center for ACCURATE & RENE</span></h5>
		<p class="w3-center">Kumpulan Informasi Panduan Dasar & Penyelesaian Masalah pada Accurate V3, V4, V5 & RENE</p>
  </div>
  <div class="w3-row w3-white">
    <div style="margin-top:20px" class="w3-col l8 s12">
			<div class="w3-row">
			  <div class="w3-container w3-twothird">
			    <input type="text" id="kotak_cari" name="search" class="w3-input" type="text" placeholder="Search..">
			  </div>
			  <div class="w3-container w3-third">
			    <button onclick="test2()"  class="w3-button w3-teal w3-block">Cari</button>
			  </div>
			</div>
      <div id="tutorial_list" class="w3-row-padding">
      </div>
      <div class="container">
      </div>
      <div class="page-load-status">
        <div class="loader-ellips infinite-scroll-request">
          <span class="loader-ellips__dot">
          </span>
          <span class="loader-ellips__dot">
          </span>
          <span class="loader-ellips__dot">
          </span>
          <span class="loader-ellips__dot">
          </span>
        </div>
        <div class="infinite-scroll-last w3-container w3-margin w3-pale-yellow" style="display:none">
          <p>End of content
          </p>
        </div>
        <p class="infinite-scroll-error">
        </p>
      </div>
      <div class="w3-center w3-padding-32">
        <div id="paglist" class="w3-bar w3-border">
        </div>
      </div>
    </div>
    <div class="w3-col l4 w3-hide-medium">
      <div class="w3-card-2 w3-margin w3-margin-top">
        <img src="<?php echo getBaseUrl() ?>_caramel/assets/img/blog.jpg" style="width:100%">
        <div class="w3-container w3-white">
          <h4>
            <b>Tentang Kami
            </b>
          </h4>
          <p style="">FAC Institute adalah konsultan akuntansi & lembaga pendidikan komputerisasi akuntansi.
          </p>
        </div>
      </div>
      <hr>
      <!-- Posts -->
      <div class="w3-card-2 w3-margin">
        <div class="w3-container w3-padding">
          <h4>
            <b>Kategori
            </b>
          </h4>
        </div>
        <ul id="taglist" class="w3-ul w3-hoverable w3-white">
        </ul>
      </div>
      <hr>
      <!-- Labels / tags -->
      <div class="w3-card-2 w3-margin">
        <div class="w3-container w3-padding">
          <h4>Tags
          </h4>
        </div>
        <div class="w3-container w3-white">
          <p>
            <span class="w3-tag  w3-small  w3-light-grey w3-margin-bottom">Artikel
            </span>
            <span class="w3-tag  w3-black  w3-margin-bottom">Tutorial
            </span>
          </p>
        </div>
      </div>
    </div>
  </div>
<?php endif ?>
<div class="w3-content"  style="max-width:1200px">
  <?php include_once '../footer.php'; ?>
</div>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'></script>
<script src='https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.js'></script>
<script src="https://fac-institute.com/administrasi/view/quotation/dist/jquery.easy-autocomplete.min.js"></script>
<script>
$(document).ready(function() {
    test();
		document.getElementById("myInput").value = window.location.href;
    $("#kotak_cari").on('keyup', function(e) {
        if (e.keyCode == 13) {
            test2();
        }
    });
    words = $("#kotak_cari").val();
    if (words != "") {
    }
});

var $container = $('.container').infiniteScroll({
    path: function() {
        return 'https://fac-institute.com/api/tutsvc?tagname=Solution%20Center%20for%20ACCURATE%20&%20RENE';
    },
    responseType: 'json',
    history: false,
});

var infScroll = $container.data('infiniteScroll');
$container.on('load.infiniteScroll', function(event, data) {
    var page_num = infScroll.pageIndex;
    var offset = (Number(page_num) - 2) * 10;
    var limit = 10;
    result1 = _(data.data).slice(offset).take(limit).value();
    var dataParse = "";
    for (i = 0; i < result1.length; i++) {
        if (result1[i].thumbnail != '-') {
            dataParse += "<div class='w3-row w3-padding-16'><div class='w3-half w3-container'><img src='https://fac-institute.com/images/tutorial/thumbnails/" + result1[i].thumbnail + "' width='100%' /></div>" +
                "<div class='w3-half w3-container'><p>" + "<a href='https://fac-institute.com/tutorial/?app=1&id=" + result1[i].id + "' style='text-decoration:none' >" + "<b>" + (result1[i].judul) + "</b></a></p>" +
                "<p style='font-size=12px;margin-top:-12px'>" + ((result1[i].konten).substr(0, 400)).replace(/<\/?[^>]+(>|$)/g, "") + "<a class='w3-text-blue' target=''  href='https://fac-institute.com/tutorial/?app=1&id=" + result1[i].id + "'> Baca selengkapnya... <a/></p></div>";
        } else {
            dataParse += "<div class='w3-container w3-white okzx'><p>" + "<a href='https://fac-institute.com/tutorial/?app=1&id=" + result1[i].id + "' style='text-decoration:none' >" + "<b>" + (result1[i].judul) + "</b></a></p>" +
                "<p style='font-size=12px;margin-top:-12px'>" + ((result1[i].konten).substr(0, 400)).replace(/<\/?[^>]+(>|$)/g, "") + "<a class='w3-text-blue' target=''  href='https://fac-institute.com/tutorial/?app=1&id=" + result1[i].id + "'> Baca selengkapnya... <a/></p></div>";
        }

    }

    if (result1.length == 0) {
        $('.loader-ellips').hide();
        $('.infinite-scroll-last').show();
    }

		var option1 = {
				data: data.data,getValue: "judul",
				list: {
						match: {enabled: true},
						showAnimation: {type: "fade",time: 200,callback: function() {}},
						hideAnimation: {type: "slide",time: 400,callback: function() {}},
						maxNumberOfElements: 99,
						sort: {
								enabled: true
						}
				}
			};

			$("#kotak_cari").easyAutocomplete(option1);

    $container.infiniteScroll('appendItems', $(dataParse));
});

$container.infiniteScroll('loadNextPage');

function test() {
    var fruits = [];
    $.ajax({
        type: "GET",
        url: "https://fac-institute.com/api/tutsvc/listtags",
        dataType: 'json',
        cache: false,
        success: function(data) {
            var ff = _.orderBy(data.data, ['tags_txt'], ['asc', 'desc']);
            for (i = 0; i < ff.length; i++) {
                document.getElementById("taglist").innerHTML += "<li><a style='text-decoration:none' target='' href='" + "https://fac-institute.com/tutorial/tags?key=" + ff[i].tags_txt + "'>" + ff[i].tags_txt + " (" + ff[i].total_of_use + ")</a></li>";
            }
        }
    });
}


function test2() {
    var my_query = {
        'words': $("#kotak_cari").val()
    }
    window.location.replace("https://fac-institute.com/tutorial/result?key=" + $("#kotak_cari").val());
}


var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;
    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};


</script>

</body>
</html>
