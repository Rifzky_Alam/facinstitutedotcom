
<?php
include_once '../model/Tutorial.php';
$ctrlTuts = new ControlTutorial();
$datas = json_decode($ctrlTuts->selectAllData());
$dataSidebar = json_decode($ctrlTuts->selectJudulAndID());
if (isset($_GET['app'])&&$_GET['app']=='1'&&isset($_GET['id'])&&!empty($_GET['id'])) {
	$dataTuts=$ctrlTuts->selectById($_GET['id']);
}
function getValue($value){
	if (empty($value)||$value=='') {
		return 'Tutorial tidak ada';
	} else {
		return $value;
	}
}
?>

<?php
function base_url(){
  $currentPath = $_SERVER['PHP_SELF'];$pathInfo = pathinfo($currentPath);$hostName = $_SERVER['HTTP_HOST'];$protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https://'?'https://':'http://';
  return 'https://'.$hostName."/";
	// return 'http://localhost/facftp/';
}
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0" />
<meta property="og:type" content="article" />
<meta property="og:site_name" content="FAC INSTITUTE" />
<?php if (isset($_GET['app'])&&$_GET['app']=='1'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>
	<meta property="og:title" content=<?php echo "'$dataTuts->judul'" ?> />
	<meta property="og:description" content=<?php echo "'$dataTuts->judul'" ?> />
	<meta name="thumbnailUrl" content="http://alix.sch.id/pendaftaran/images/logo-fac.jpg" itemprop="thumbnailUrl" />
	<meta name="description" content=<?php echo "'$dataTuts->judul'" ?> itemprop="description" />
  <meta name="robots" content="index, follow" />
<?php else: ?>
<?php endif ?>
<meta property="og:url" content=<?php echo '"http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'"'; ?> />
<meta property="fb:app_id" content="454421948087465" />
<meta property="og:image" content="http://alix.sch.id/pendaftaran/images/logo-fac.jpg" />
<meta property="og:image:type" content="image/jpeg" />
<meta property="og:image:height" content="200" />
<meta property="og:image:width" content="200" />



<meta charset="utf-8">
<?php if (isset($_GET['app'])&&$_GET['app']=='1'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>
	<title><?php echo $dataTuts->judul; ?></title>
	<?php else: ?>
	<title>Halaman Tutorial</title>
<?php endif ?>



	<style>
	.tengah{
		text-align: center;
	}
	</style>
	<style type="text/css">
  #fl_menu{
		display: none;
	}
		html,body{
			height: 100%;
		}
		body {
    		padding-bottom: 20px;
		}

		#wrapper{
			min-height: 100%;
			/*background-image: url('images/background.jpg');
			background-repeat: no-repeat;
			background-size: cover;
			background-color: snow;*/
			margin-bottom: 40px;
		}

		#visi>li{
			list-style-type: none;
			font-size: 16px;
		}

		#visi>li:before{
			content: "\e127";
			font-family: 'Glyphicons Halflings';
			font-size:12px;
			float: left;

			margin-left: -17px;
			color: black;
		}

		#misi>li{
			list-style-type: none;
			line-height: 2em;
			font-size: 16px;
		}

		#misi>li:before{
			content: "\e013";
			font-family: 'Glyphicons Halflings';
			font-size: 12px;
			float: left;
			margin-right: 5px;
			margin-left: -17px;
			color: black;
		}

		#top-navigations{
			padding-left: 50px;
			color: orange;
		}

		#top-navigations>ul>li>a{

			color: orange;
		}

		#top-brand-container{
			background-image: url('images/brand.jpg');
		}

		#top-brand{
			padding-top: 50px;
			padding-left: 30px;
			color: white;
		}

		#content{
			padding-top: 30px;


		}

		#bottom-brand{
			margin: 0px;
		}

		#content-node{
			/*padding-top: 10px;*/
		}
	</style>
</head>
<body>


<?php function SectionHtml(){?>
	<section class="col-md-9" style="margin-top: 140px;">
		<div class="row" id="content-node">
			<div class="col-md-10 col-md-offset-1" style="padding-left:0;">
<?php }?>
<div class="container-fluid" id="content">

<?php if (isset($_GET['app'])&&$_GET['app']=='1'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>

	<?php SectionHtml() ?>
<div id="detailartikel-full">
		<h2 class="judul-artikel"><?php echo getValue(@$dataTuts->judul); ?></h2>
		<div id="detailfull">
			<div  class="col-md-12">
				<p class="rincian-posting">12 Maret 2016 | Admin</p>
				<div id="mobile-deskripsi"	 class="col-md-12">
					<?php echo $dataTuts->isi; ?>
				</div>
				<div class="col-md-12">
					<h2 class="text-center">
						<a href=<?php echo "'".getBaseUrl()."tutorial/'";?> class="btn btn-primary btn-large btn-readmore">Tampilkan Semua Tutorial</a>
					</h2>
				</div>
			</div>
		</div>
</div>
</div>
</div>
</section>

	<?php else: ?>
<?php SectionHtml() ?>
		<div id="artikelfull">
			<h2 class="judul-artikel">Tutorial</h2>

	<?php

		for ($i=0; $i < count($datas); $i++){

	?>
				<div class="thumbartikel col-md-12">

					<div class="col-md-12">
						<p class="headpost"><?php echo $datas[$i]->judul; ?></p>
						<p class="deskripsi"><?php echo substr($datas[$i]->isi, 0, 200) . ". . . ."; ?></p>
	            		<a class="readmore-artikel" href=<?php echo "'".base_url()."tutorial/?app=1&id=".$datas[$i]->id."'"; ?>>
								<div class="readmore">
									<p>Lanjut Membaca</p>
								</div>
							</a>
					</div>
				</div>



<?php } ?>

			<div id="pagingbawah">
				<ul class="pagination">

				<?php
				$pageNum=0;
				for ($i=0; $i < count($datas); $i++) {
				?>
					<?php if ($i%5==0): ?>
						<li><a href=<?php $pageNum+=1; echo "'".getBaseUrl()."tutorial/?page=".$pageNum."'"; ?>><?php echo $pageNum; ?></a></li>
					<?php endif ?>
				<?php } ?>

				</ul>
			</div>
		</div>
	</div>
</div>
</section>


<?php endif ?>



</section>

	<section class="col-md-3 sidebar-custom"  style="margin-top: 140px;">
	<div id="sidebar" class="row">
			<div id="custom-search-input">
					<div class="input-group col-md-12">
						<input type="text" class="search-query form-control" placeholder="Search" />
						<span class="input-group-btn">
							<button class="btn btn-primary" type="button">
								<span class="glyphicon glyphicon-search"></span>
							</button>
						</span>
				</div>
			</div>
			<div id="top-posting">
					<div class="col-md-12">
							<h3 class="judul-sidebar">Tutorial Terpopuler</h3>
							<hr/>
							<div class="artikel-populer">
								<ol>
								<?php
									for ($i=0; $i < count($dataSidebar) ; $i++) {
										echo "<li class='isi-artikel-populer'><a class='link' href='?app=1&id=".$dataSidebar[$i]->id."'>".$dataSidebar[$i]->judul."</a></li>";
									}
								?>
								</ol>
							</div>
					</div>
			</div>
	    <div id="top-posting">
	        <div class="col-md-12">
	            <h3 class="judul-sidebar">Top Categories</h3>
	            <hr/>
	            <div class="artikel-populer">
	              <ul>
					<?php
						for ($i=0; $i < count($dataSidebar) ; $i++) {
							echo "<li class='isi-artikel-populer'><a class='link' href='?app=1&id=".$dataSidebar[$i]->id."'>".$dataSidebar[$i]->judul."</a></li>";
						}
					?>
	              </ul>
	            </div>
	        </div>
	    </div>
	</div>
</section>
</div>
	<?php include_once '../footer.php'; ?>
	<script type="text/javascript">
		$('#top-navigations>ul>li>a').hover(function() {
			$(this).css('color','red');
		},function(){
			$(this).css('color','orange');
		});

		$('#top-navigations>ul>li').hover(function() {
			$(this).delay(2000).css('background-color','white');
		},function(){
			$(this).delay(2000).css('background-color','#222');
		});

	</script>
		<script type="text/javascript">
// create the back to top button
$('body').prepend('<a href="#" class="back-to-top">Back to Top</a>');

var amountScrolled = 300;

$(window).scroll(function() {
	if ( $(window).scrollTop() > amountScrolled ) {
		$('a.back-to-top').fadeIn('slow');
	} else {
		$('a.back-to-top').fadeOut('slow');
	}
});

$('a.back-to-top, a.simple-back-to-top').click(function() {
	$('html, body').animate({
		scrollTop: 0
	}, 700);
	return false;
});
</script>
</body>
</html>
