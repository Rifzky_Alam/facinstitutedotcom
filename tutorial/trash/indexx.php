<?php
include_once '../model/Tutorial.php';
$ctrlTuts = new ControlTutorial();
if (isset($_GET['page'])&&!empty($_GET['page'])) {
	$datas = json_decode($ctrlTuts->selectAllData($_GET['page'],'6'));
}elseif (isset($_GET['search'])&&!empty($_GET['search'])) {
	$datas = $ctrlTuts->searchAllData($_GET['search']);
}else {
	$datas = json_decode($ctrlTuts->selectAllData('0','6'));
}

$dataSidebar = json_decode($ctrlTuts->selectJudulAndID());
if (isset($_GET['app'])&&$_GET['app']=='1'&&isset($_GET['id'])&&!empty($_GET['id'])) {
	$dataTuts=$ctrlTuts->selectById($_GET['id']);
}
function getValue($value){
	if (empty($value)||$value=='') {
		return 'Tutorial tidak ada';
	} else {
		return $value;
	}
}
?>

<?php
function base_url(){
  $currentPath = $_SERVER['PHP_SELF'];$pathInfo = pathinfo($currentPath);$hostName = $_SERVER['HTTP_HOST'];$protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https://'?'https://':'http://';
  return 'https://'.$hostName."/";
	// return 'http://localhost/facftp/';
}
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0" />
<meta property="og:type" content="article" />
<meta property="og:site_name" content="FAC INSTITUTE" />
<?php if (isset($_GET['app'])&&$_GET['app']=='1'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>
<meta property="og:title" content=<?php echo "'$dataTuts->judul'" ?> />
<meta property="og:description" content=<?php echo "'$dataTuts->judul'" ?> />
<meta name="description" content=<?php echo "'$dataTuts->judul'" ?> itemprop="description" />
<meta name="robots" content="index, follow" />
<?php else: ?>
<?php endif ?>
<title>Reading Page</title>
<meta property="og:url" content=<?php echo '"http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'"'; ?> />
<meta property="fb:app_id" content="454421948087465" />
<meta property="og:image" content="http://fac-institute.com/images/logo-fac_header.png" />
<meta property="og:image:height" content="200" />
<meta property="og:image:width" content="200" />
<meta name="thumbnailUrl" content="http://alix.sch.id/pendaftaran/images/logo-fac.jpg" itemprop="thumbnailUrl" />

<meta charset="utf-8">
<?php if (isset($_GET['app'])&&$_GET['app']=='1'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>
	<title><?php echo $dataTuts->judul; ?></title>
	<?php else: ?>
	<title>Halaman Tutorial</title>
<?php endif ?>


<?php include_once '../links.php'; ?>
<link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/w3.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/animate.min.css">
<link href="https://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,100,300,500,600,700c" type="text/css" />
<!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inconsolata"> -->
<link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/style.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.10/lodash.min.js"></script>


</head>


<body onload="test()">



<div id="tutorial_list" class="w3-row-padding">

</div>



<script>


 function test() {
    var my_query =  $("#kotak_cari").val();
    var page_num = getUrlParameter('page');
    var offset = (Number(page_num) - 1) * 10;
    var ofs = Number(page_num) - 1;
    var limit = 10;

   $.ajax({
      type: "GET",
      url: "https://fac-institute.com/api/tutsvc",
			dataType: 'json',
      data: {
				'tagname': 'Solution Center for ACCURATE & RENE'
      },
      cache: false,
      success: function(data) {

        console.log(data.data);

        document.getElementById("tutorial_list").innerHTML = "";

        var result1 =  _(data.data).slice(offset).take(limit).value() ;

        for (i=0; i < result1.length ; i++){
					document.getElementById("tutorial_list").innerHTML += "<div class='w3-container w3-white okzx'><p>"+ "<a href=" + "https://fac-institute.com/" + "tutorial/?app=1&id=" + result1[i].id + "#top_content  style='text-decoration:none' >" +"<b>"+ (result1[i].judul) + "</b></a><span style='font-size:10px'>  ⏰" + result1[i].tanggal +"</span></p>"+
					"<p style='font-size=12px;margin-top:-12px'>"+ ((result1[i].konten).substr(0, 600)).replace(/<\/?[^>]+(>|$)/g, "") + "<a class='w3-text-blue' target='_blank'  href=" + "https://fac-institute.com/" + "tutorial/?app=1&id=" + result1[i].id + "#top_content  > Baca selengkapnya... <a/></p></div>";

          // console.log((_.split(result1[i].all_tags, '#')));
          // console.log( _.some(  (_.split(result1[i].all_tags, '#'))  , _.unary(_.partialRight(_.includes, 'Other'))) );
				}

        document.getElementById("paglist").innerHTML +="<a href='#' class='w3-bar-item w3-button'>&laquo;</a>";
        for (i=1; i < 5 ; i++){
          if (i == 1) {
            document.getElementById("paglist").innerHTML +="<a href='https://fac-institute.com/tutorial/index?page=" + (Number(ofs) + Number(i)) + "' class='w3-bar-item w3-button w3-green'  >"+ (Number(ofs) + Number(i)) +"</a>"
          }
          else {
            document.getElementById("paglist").innerHTML +="<a href='https://fac-institute.com/tutorial/index?page=" + (Number(ofs) + Number(i)) + "' class='w3-bar-item w3-button'  >"+ (Number(ofs) + Number(i)) +"</a>"
          }
				}
        document.getElementById("paglist").innerHTML +="<a href='#' class='w3-bar-item w3-button'>&raquo;</a>";
      }
  });


 }



 var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};


</script>

<script src="<?php echo getBaseUrl() ?>assets/homepage/js/jquery-2.1.4.min.js"></script>
<script src="<?php echo getBaseUrl() ?>tutorial/js/tutorial.js"></script>

</body>
</html>
