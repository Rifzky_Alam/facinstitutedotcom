<?php
include_once '../model/Tutorial.php';
$ctrlTuts = new ControlTutorial();
if (isset($_GET['page'])&&!empty($_GET['page'])) {
	$datas = json_decode($ctrlTuts->selectAllData($_GET['page'],'6'));
}elseif (isset($_GET['search'])&&!empty($_GET['search'])) {
	$datas = $ctrlTuts->searchAllData($_GET['search']);
}else {
	$datas = json_decode($ctrlTuts->selectAllData('0','6'));
}



$dataSidebar = json_decode($ctrlTuts->selectJudulAndID());
if (isset($_GET['app'])&&$_GET['app']=='1'&&isset($_GET['id'])&&!empty($_GET['id'])) {
	$dataTuts=$ctrlTuts->selectById($_GET['id']);
}
function getValue($value){
	if (empty($value)||$value=='') {
		return 'Tutorial tidak ada';
	} else {
		return $value;
	}
}
?>

<?php
function base_url(){
  $currentPath = $_SERVER['PHP_SELF'];$pathInfo = pathinfo($currentPath);$hostName = $_SERVER['HTTP_HOST'];$protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https://'?'https://':'http://';
  return 'https://'.$hostName."/";
	// return 'http://localhost/facftp/';
}
?>


<?php $jumlahData = $ctrlTuts->countAllTutorial() ?>

<?php
echo "<script>console.log(".$jumlahData.")</script>";
?>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0" />
<meta property="og:type" content="article" />
<meta property="og:site_name" content="FAC INSTITUTE" />
<?php if (isset($_GET['app'])&&$_GET['app']=='1'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>
<meta property="og:title" content=<?php echo "'$dataTuts->judul'" ?> />
<meta property="og:description" content=<?php echo "'$dataTuts->judul'" ?> />
<meta name="description" content=<?php echo "'$dataTuts->judul'" ?> itemprop="description" />
<meta name="robots" content="index, follow" />
<?php else: ?>
<?php endif ?>
<title>Reading Page</title>
<meta property="og:url" content=<?php echo '"http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'"'; ?> />
<meta property="fb:app_id" content="454421948087465" />
<meta property="og:image" content="http://fac-institute.com/images/logo-fac_header.png" />
<meta property="og:image:height" content="200" />
<meta property="og:image:width" content="200" />
<meta name="thumbnailUrl" content="http://alix.sch.id/pendaftaran/images/logo-fac.jpg" itemprop="thumbnailUrl" />

<meta charset="utf-8">
<?php if (isset($_GET['app'])&&$_GET['app']=='1'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>
	<title><?php echo $dataTuts->judul; ?></title>
	<?php else: ?>
	<title>Halaman Tutorial</title>
<?php endif ?>


<?php include_once '../links.php'; ?>
<link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/w3.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/animate.min.css">
<link href="https://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,100,300,500,600,700c" type="text/css" />
<!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inconsolata"> -->
<link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/style.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.10/lodash.min.js"></script>
<!-- <link rel="stylesheet" href="https://infinite-scroll.com/css/infinite-scroll-docs.css?5"> -->
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Raleway", sans-serif}

input[type=text] {
    width: 170px;
    box-sizing: border-box;
    /*border: 2px solid #ccc;*/

		border: none;
		border-bottom: 1px solid #ccc;

    border-radius: 4px;
    font-size: 16px;
    background-color: white;
    background-image: url('<?php echo getBaseUrl() ?>_caramel/assets/img/searchicon.png');
    background-position: 10px 10px;
    background-repeat: no-repeat;
    padding: 12px 20px 12px 40px;
    -webkit-transition: width 0.4s ease-in-out;
    transition: width 0.4s ease-in-out;
}

input[type=text]:focus {
    /*width: 90%;*/
}

/* loader-ellips
------------------------- */

.loader-ellips {
  font-size: 20px;
  position: relative;
  width: 4em;
  height: 1em;
  margin: 10px auto;
}

.loader-ellips__dot {
  display: block;
  width: 1em;
  height: 1em;
  border-radius: 0.5em;
  background: #555;
  position: absolute;
  animation-duration: 0.5s;
  animation-timing-function: ease;
  animation-iteration-count: infinite;
}

.loader-ellips__dot:nth-child(1),
.loader-ellips__dot:nth-child(2) {
  left: 0;
}
.loader-ellips__dot:nth-child(3) { left: 1.5em; }
.loader-ellips__dot:nth-child(4) { left: 3em; }

@keyframes reveal {
  from { transform: scale(0.001); }
  to { transform: scale(1); }
}

@keyframes slide {
  to { transform: translateX(1.5em) }
}

.loader-ellips__dot:nth-child(1) {
  animation-name: reveal;
}

.loader-ellips__dot:nth-child(2),
.loader-ellips__dot:nth-child(3) {
  animation-name: slide;
}

.loader-ellips__dot:nth-child(4) {
  animation-name: reveal;
  animation-direction: reverse;
}

/* loader-wheel
------------------------- */

.loader-wheel {
  font-size: 64px; /* change size here */
  position: relative;
  height: 1em;
  width: 1em;
  padding-left: 0.45em;
  overflow: hidden;
  margin: 0 auto;
  animation: loader-wheel-rotate 0.5s steps(12) infinite;
}

.loader-wheel i {
  display: block;
  position: absolute;
  height: 0.3em;
  width: 0.1em;
  border-radius: 0.05em;
  background: #333; /* change color here */
  opacity: 0.8;
  transform: rotate(-30deg);
  transform-origin: center 0.5em;
}

@keyframes loader-wheel-rotate {
  from { transform: rotate(0deg); }
  to { transform: rotate(360deg); }
}

/* page-nav
------------------------- */

.page-nav {
  font-size: 0.85rem;
  line-height: 1.2;
}

.page-nav__list {
  list-style: none;
  margin: 0 0 40px;
  padding: 0 10px 0 0;
}

.page-nav__item {
  margin: 0.2rem 0;
  overflow-wrap: break-word;
}

.page-nav__item--h3,
.page-nav__item--h4 {
  padding-left: 15px;
}

@media screen and ( min-width: 960px ) {
  .page-nav {
    position: absolute;
    left: 0;
    top: 0;
    width: 200px;
    height: 100%;
  }

  .page-nav__list {
    display: block;
    margin: 0;
    padding: 20px;
  }

  /* activate sticky */
  .page-nav:after {
    content: 'sticky';
    display: none;
  }

  .page-nav.is-sticky .page-nav__list {
    position: -webkit-sticky;
    position: sticky;
    left: 0;
    top: 0;
  }

}

/* scroller
------------------------- */

.scroller {
  height: 400px;
  padding: 10px 10px 100px;
  overflow-y: scroll;
  border: 1px solid #DDD;
  border-radius: 5px;
}

.scroller__content {
}

/* ---- scroller-item ---- */

.scroller-item {
  height: 200px;
  margin-bottom: 10px;
  padding: 20px;
  background: #19F;
  border-radius: 5px;
  color: white;
  font-size: 3.0rem;
  line-height: 1;
}

.scroller-item--height2 { height: 250px; }
.scroller-item--height3 { height: 300px; }

.scroller-item--magenta { background: #C25; }
.scroller-item--red { background: #E21; }
.scroller-item--gold { background: #EA0; }
.scroller-item--green { background: #6C6; }

/* ---- prefill ---- */

.scroller--prefill { height: 500px; }

.scroller--prefill .scroller-item,
.scroller--prefill .scroller-item--height2,
.scroller--prefill .scroller-item--height3 { height: 80px; }

/* scroller-status
------------------------- */


.scroller-status {
  display: none;
  padding: 20px 0;
}

.scroller-status__message {
  text-align: center;
  color: #777;
}
/* site nav
------------------------- */

.site-nav {
  background: #19F;
  list-style: none;
  margin: 0;
  padding: 0;
}

/* clearfix */
.site-nav:after {
  content: '';
  clear: both;
  display: block;
}

/* bottom nav */
.main ~ .site-nav {
  position: relative;
  margin-top: 80px;
  z-index: 2; /* on top of page-nav */
}

.site-nav__item {
  width: 33.333%;
  float: left;
  line-height: 28px;
}

.site-nav__item a {
  display: block;
  color: white;
  padding: 5px
}

.site-nav__item--homepage {
  font-size: 1.5rem;
  font-weight: bold;
}

.site-nav__item--homepage a {
}

.site-nav__item a:hover {
  background: #C25;
  color: white;
}

.site-nav__item a:active {
  background: white;
  color: #8C8;
}

/* selected */
.page--style .site-nav__item--style a,
.page--options .site-nav__item--options a,
.page--api .site-nav__item--api a,
.page--events .site-nav__item--events a,
.page--extras .site-nav__item--extras a,
.page--license .site-nav__item--license a {
  background: #FFF;
  color: #19F;
}

/* size at which it can fit */
@media screen and ( min-width: 768px ) {

  .site-nav__item {
    width: auto;
    font-size: 1.1rem;
    line-height: 70px;
  }

  .site-nav__item a {
    padding: 0px 25px;
  }

  .site-nav__item--homepage {
    font-size: 1.4rem;
    width: 200px;
  }

}

/* site-scroll
------------------------- */

.site-scroll__button {
  margin: 60px auto;
  padding: 20px 40px;
}

</style>
</head>




<body>

	<p>header</p>
	<?php include_once '../top-nav.php'; ?>
	<?php include_once '../modal-order.php'; ?>
	<!-- Header -->
	<header class="w3-display-container w3-content w3-center" style="max-width:1500px">
		<img class="w3-image" src="<?php echo getBaseUrl() ?>_caramel/assets/img/Untitled design2.jpg" alt="Me" width="1500" height="600">
		<div class="w3-display-middle w3-padding-large w3-border w3-wide w3-text-light-grey w3-center">
			<h1 class="w3-hide-medium w3-hide-small w3-xxxlarge">FAC</h1>
			<h5 class="w3-hide-large" style="white-space:nowrap">FAC</h5>
			<h3 class="w3-hide-medium w3-hide-small">Blog</h3>
			<!-- <p style="text-align:center"> </p> -->
		</div>
		<div class="w3-display-bottommiddle w3-container w3-text-white w3-padding-32 w3-hide-small">
	 <!-- <p style="font-size:20px;">Konsultan akuntansi & lembaga pendidikan komputerisasi akuntansi.</p> -->
 </div>
 <div class="w3-bar w3-black w3-round w3-display-bottommiddle w3-hide-small" style="bottom:-16px">
	 <!-- <a href="<?php echo getBaseUrl() ?>artikel/" class="w3-bar-item w3-button">Artikel</a> -->
	 <a href="<?php echo getBaseUrl() ?>tutorial/" class="w3-bar-item w3-button">Tutorial</a>
 </div>
	</header>

	<!-- Navbar on small screens -->
	<div class="w3-center w3-light-grey w3-padding-16 w3-hide-large w3-hide-medium">
		<div class="w3-bar w3-light-grey">
			<!-- <a href="<?php echo getBaseUrl() ?>artikel/" class="w3-bar-item w3-button">Artikel</a> -->
			<a href="<?php echo getBaseUrl() ?>tutorial/" class="w3-bar-item w3-button">Tutorial</a>
		</div>
	</div>

	<div style="margin-top:20px;" class="w3-container">
	</div>

<!-- <h5 class="w3-center w3-padding-16"><span class="w3-tag w3-wide">FAC - Article</span></h5> -->

<?php function SectionHtml(){?>
<div id="top_content" class="w3-container w3-padding-64" id="about">
<?php }?>
<?php if (isset($_GET['app'])&&$_GET['app']=='1'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>
<?php SectionHtml() ?>
<div class="w3-content w3-padding-16">
		<div>
		</div>
</div>



<div class="w3-content" >
<h2 class=""><?php echo getValue(@$dataTuts->judul); ?></h2>
<?php echo $dataTuts->isi; ?>
</div>
</div>
<?php else:?>



<div class="w3-row w3-white">
<?php SectionHtml() ?>
<div style="margin-top:20px" class="w3-col l8 s12">
		<div class="w3-row-padding">
					<input type="text" id="kotak_cari" name="search"  placeholder="Search..">
					<!-- <input type="text" id="kotak_cari" oninput="test()" name="search"  placeholder="Search.."> -->
</div>


<div class="w3-row-padding">
	<table id="tutorial_list" class="w3-table-all">
     <tr>
       <th>Judul</th>
       <th>Link</th>
       <th>Publish Date</th>
     </tr>

   </table>
</div>





<div class="container"></div>

<div class="page-load-status">
  <div class="loader-ellips infinite-scroll-request">
    <span class="loader-ellips__dot"></span>
    <span class="loader-ellips__dot"></span>
    <span class="loader-ellips__dot"></span>
    <span class="loader-ellips__dot"></span>
  </div>
	<div class="infinite-scroll-last w3-container w3-margin w3-pale-yellow" style="display:none">
      <p>End of content</p>
  </div>
  <p class="infinite-scroll-error"></p>
</div>


<div class="w3-center w3-padding-32">
<div id="paglist" class="w3-bar w3-border">
</div>
</div>

</div>


<div class="w3-col l4">


	<div class="w3-card-2 w3-margin w3-margin-top">

	<img src="<?php echo getBaseUrl() ?>_caramel/assets/img/blog.jpg" style="width:100%">
		<div class="w3-container w3-white">
			<h4><b>Tentang Kami</b></h4>
			<p style="">FAC Institute adalah konsultan akuntansi & lembaga pendidikan komputerisasi akuntansi.</p>
		</div>
	</div><hr>

	<!-- Posts -->
	<div class="w3-card-2 w3-margin">
		<div class="w3-container w3-padding">
			<h4><b>PILIH MODUL</b></h4>
		</div>
		<ul id="taglist" class="w3-ul w3-hoverable w3-white">


		</ul>
	</div>
	<hr>

	<!-- Labels / tags -->
	<div class="w3-card-2 w3-margin">
		<div class="w3-container w3-padding">
			<h4>Tags</h4>
		</div>
		<div class="w3-container w3-white">
		<p><span class="w3-tag  w3-small  w3-light-grey w3-margin-bottom">Artikel</span> <span class="w3-tag  w3-black  w3-margin-bottom">Tutorial</span>
		</p>
		</div>
	</div>
</div>

</div>


	</div>
</section>
<?php endif ?>


<div class="w3-content"  style="max-width:1200px">
<?php include_once '../footer.php'; ?>
</div>


<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'></script>
<script src='https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.js'></script>

<script>


$( document ).ready(function() {

	test();

	$("#kotak_cari").on('keyup', function (e) {
	    if (e.keyCode == 13) {
	        test2();
	    }
	});
    words = $("#kotak_cari").val();
		if (words != "" ){
			// test2();
		}
});



var $container = $( '.container').infiniteScroll({
  path: function() {
    return 'https://fac-institute.com/api/tutsvc?tagname=Solution%20Center%20for%20ACCURATE%20&%20RENE';
  },
  responseType: 'text',
  status: '.scroll-status',
  history: false,
});

var infScroll = $container.data('infiniteScroll');

$container.on( 'load.infiniteScroll', function( event, response ) {
// console.log(response);
var data = JSON.parse( response );
console.log(data.data);
console.log(infScroll.pageIndex)

var page_num = infScroll.pageIndex ;
var offset = (Number(page_num) - 2) * 10;
var limit = 10;

console.log(_(data.data).slice(offset).take(limit).value());
result1 = _(data.data).slice(offset).take(limit).value();

	var fuk =  "";

	for (i=0; i < result1.length ; i++){

		fuk += "<tr><td>"+"<b>"+ result1[i].judul + "</td><td>"+"<a class='w3-text-blue' target='_blank' href=" + "https://fac-institute.com/" + "tutorial/?app=1&id=" + result1[i].id + "#top_content >" +
		"https://fac-institute.com/" + "tutorial/?app=1&id=" + result1[i].id +"<a/></td><td>"+ result1[i].tanggal +"</td></tr>";

	}

	if (result1.length == 0){
		$('.loader-ellips').hide();
		$('.infinite-scroll-last').show();
	}

  $container.infiniteScroll( 'appendItems', $(fuk) );
});

// load initial page
$container.infiniteScroll('loadNextPage');


 function test() {

var fruits = [];
 $.ajax({
    type: "GET",
    url: "https://fac-institute.com/api/tutsvc/listtags",
    dataType: 'json',
    cache: false,
    success: function(data) {
      var ff = _.orderBy(data.data, ['tags_txt'], ['asc', 'desc']);
			// var ff = _.orderBy(data.data, ['total_of_use']);
      for (i=0; i < ff.length ; i++){
      	document.getElementById("taglist").innerHTML +=  "<li><a style='text-decoration:none' target='_blank' href='"+"https://fac-institute.com/tutorial/tags?key="+ ff[i].tags_txt +"'>" + ff[i].tags_txt + " ("+ ff[i].total_of_use +")</a></li>";
      }
    }
});



 }


 function test2() {
    var my_query = {
        'words': $("#kotak_cari").val()
    }

		window.open("https://fac-institute.com/tutorial/result?key="+$("#kotak_cari").val(), '_blank');
 }



 var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};


</script>

<script src="<?php echo getBaseUrl() ?>assets/homepage/js/jquery-2.1.4.min.js"></script>
<script src="<?php echo getBaseUrl() ?>tutorial/js/tutorial.js"></script>

</body>
</html>
