﻿<?php
include_once '../model/Tutorial.php';
$ctrlTuts = new ControlTutorial();
if (isset($_GET['page'])&&!empty($_GET['page'])) {
	$datas = json_decode($ctrlTuts->selectAllData($_GET['page'],'6'));
}elseif (isset($_GET['search'])&&!empty($_GET['search'])) {
	$datas = $ctrlTuts->searchAllData($_GET['search']);
}else {
	$datas = json_decode($ctrlTuts->selectAllData('0','6'));
}

$dataSidebar = json_decode($ctrlTuts->selectJudulAndID());
if (isset($_GET['app'])&&$_GET['app']=='1'&&isset($_GET['id'])&&!empty($_GET['id'])) {
	$dataTuts=$ctrlTuts->selectById($_GET['id']);
}
function getValue($value){
	if (empty($value)||$value=='') {
		return 'Tutorial tidak ada';
	} else {
		return $value;
	}
}
?>

<?php
function base_url(){
  $currentPath = $_SERVER['PHP_SELF'];$pathInfo = pathinfo($currentPath);$hostName = $_SERVER['HTTP_HOST'];$protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https://'?'https://':'http://';
  return 'https://'.$hostName."/";
	// return 'http://localhost/facftp/';
}
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0" />
<meta property="og:type" content="article" />
<meta property="og:site_name" content="FAC INSTITUTE" />
<?php if (isset($_GET['app'])&&$_GET['app']=='1'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>
<meta property="og:title" content=<?php echo "'$dataTuts->judul'" ?> />
<meta property="og:description" content=<?php echo "'$dataTuts->judul'" ?> />
<meta name="description" content=<?php echo "'$dataTuts->judul'" ?> itemprop="description" />
<meta name="robots" content="index, follow" />
<?php else: ?>
<?php endif ?>
<title>Reading Page</title>
<meta property="og:url" content=<?php echo '"http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'"'; ?> />
<meta property="fb:app_id" content="454421948087465" />
<meta property="og:image" content="http://fac-institute.com/images/logo-fac_header.png" />
<meta property="og:image:height" content="200" />
<meta property="og:image:width" content="200" />
<meta name="thumbnailUrl" content="http://alix.sch.id/pendaftaran/images/logo-fac.jpg" itemprop="thumbnailUrl" />

<meta charset="utf-8">
<?php if (isset($_GET['app'])&&$_GET['app']=='1'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>
	<title><?php echo $dataTuts->judul; ?></title>
	<?php else: ?>
	<title>Halaman Tutorial</title>
<?php endif ?>


<?php include_once '../links.php'; ?>
<link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/w3.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/animate.min.css">
<link href="https://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,100,300,500,600,700c" type="text/css" />
<!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inconsolata"> -->
<link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/style.css">

<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Raleway", sans-serif}

input[type=text] {
    width: 170px;
    box-sizing: border-box;
    /*border: 2px solid #ccc;*/

		border: none;
		border-bottom: 1px solid #ccc;

    border-radius: 4px;
    font-size: 16px;
    background-color: white;
    background-image: url('<?php echo getBaseUrl() ?>_caramel/assets/img/searchicon.png');
    background-position: 10px 10px;
    background-repeat: no-repeat;
    padding: 12px 20px 12px 40px;
    -webkit-transition: width 0.4s ease-in-out;
    transition: width 0.4s ease-in-out;
}

input[type=text]:focus {
    /*width: 90%;*/
}
</style>
</head>




<body>

	<p>header</p>
	<?php include_once '../top-nav.php'; ?>
	<?php include_once '../modal-order.php'; ?>
	<!-- Header -->
	<header class="w3-display-container w3-content w3-center" style="max-width:1500px">
		<img class="w3-image" src="<?php echo getBaseUrl() ?>_caramel/assets/img/Untitled design2.jpg" alt="Me" width="1500" height="600">
		<div class="w3-display-middle w3-padding-large w3-border w3-wide w3-text-light-grey w3-center">
			<h1 class="w3-hide-medium w3-hide-small w3-xxxlarge">FAC</h1>
			<h5 class="w3-hide-large" style="white-space:nowrap">FAC</h5>
			<h3 class="w3-hide-medium w3-hide-small">Blog</h3>
			<!-- <p style="text-align:center"> </p> -->
		</div>
		<div class="w3-display-bottommiddle w3-container w3-text-white w3-padding-32 w3-hide-small">
	 <!-- <p style="font-size:20px;">Konsultan akuntansi & lembaga pendidikan komputerisasi akuntansi.</p> -->
 </div>
 <div class="w3-bar w3-black w3-round w3-display-bottommiddle w3-hide-small" style="bottom:-16px">
	 <a href="<?php echo getBaseUrl() ?>artikel/" class="w3-bar-item w3-button">Artikel</a>
	 <a href="<?php echo getBaseUrl() ?>tutorial/" class="w3-bar-item w3-button">Tutorial</a>
 </div>
	</header>

	<!-- Navbar on small screens -->
	<div class="w3-center w3-light-grey w3-padding-16 w3-hide-large w3-hide-medium">
		<div class="w3-bar w3-light-grey">
			<a href="<?php echo getBaseUrl() ?>artikel/" class="w3-bar-item w3-button">Artikel</a>
			<a href="<?php echo getBaseUrl() ?>tutorial/" class="w3-bar-item w3-button">Tutorial</a>
		</div>
	</div>
	<div style="margin-top:20px;" class="w3-container">

	</div>

<!-- <h5 class="w3-center w3-padding-16"><span class="w3-tag w3-wide">FAC - Article</span></h5> -->

<?php function SectionHtml(){?>
<div id="top_content" class="w3-container w3-padding-64" id="about">
<?php }?>
<?php if (isset($_GET['app'])&&$_GET['app']=='1'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>
<?php SectionHtml() ?>


<div class="w3-content w3-padding-16">

		<div>

		</div>
</div>



<div class="w3-content" >
<h2 class=""><?php echo getValue(@$dataTuts->judul); ?></h2>

<?php echo $dataTuts->isi; ?>

</div>


</div>
<?php else:?>



<div class="w3-row w3-white">
<?php SectionHtml() ?>

<div style="margin-top:20px" class="w3-col l8 s12">
		<div class="w3-row-padding">
					<input type="text" id="kotak_cari" name="search"  placeholder="Search..">
					<!-- <input type="text" id="kotak_cari" oninput="test()" name="search"  placeholder="Search.."> -->
</div>

<div id="tutorial_list" class="w3-row-padding">
	<?php
		for ($i=0; $i < count($datas); $i++){
	?>
					<div class="w3-container">
						<div class="w3-container w3-white">
							<p><a  href=<?php echo "'".base_url()."tutorial/?app=1&id=".@$datas[$i]->aidi."#top_content'";?> style="text-decoration:none"> <b><?php echo substr($datas[$i]->judul,0,80). " "; ?></b></a><span style="font-size:10px">⏰<?php echo $datas[$i]->tanggal ?></span></p>
							<p style="font-size=12px;margin-top:-12px" ><?php echo substr(strip_tags($datas[$i]->isi),0,200) . ". . . ."; ?><a class="w3-text-blue"  href=<?php echo "'".base_url()."tutorial/?app=1&id=".$datas[$i]->aidi."#top_content'";  ?>>Baca selengkapnya... <a/></p>
						 </div>
					</div>
	<?php } ?>
</div>
</div>


<div class="w3-col l4">


	<div class="w3-card-2 w3-margin w3-margin-top">

	<img src="<?php echo getBaseUrl() ?>_caramel/assets/img/blog.jpg" style="width:100%">
		<div class="w3-container w3-white">
			<h4><b>Tentang Kami</b></h4>
			<p style="">FAC Institute adalah konsultan akuntansi & lembaga pendidikan komputerisasi akuntansi.</p>
		</div>
	</div><hr>

	<!-- Posts -->
	<div class="w3-card-2 w3-margin">
		<div class="w3-container w3-padding">
			<h4>Popular Posts</h4>
		</div>
		<ul class="w3-ul w3-hoverable w3-white">
			<?php
				for ($i=0; $i < count($dataSidebar); $i++) {
					echo "<li><a style='text-decoration:none' href='?app=1&id=".$dataSidebar[$i]->id."'>".$dataSidebar[$i]->judul."</a></li>";
				}
			?>

		</ul>
	</div>
	<hr>

	<!-- Labels / tags -->
	<div class="w3-card-2 w3-margin">
		<div class="w3-container w3-padding">
			<h4>Tags</h4>
		</div>
		<div class="w3-container w3-white">
		<p><span class="w3-tag  w3-small  w3-light-grey w3-margin-bottom">Artikel</span> <span class="w3-tag  w3-black  w3-margin-bottom">Tutorial</span>
		</p>
		</div>
	</div>
</div>

</div>

<div class="w3-center w3-padding-32 ">
	 <div class="w3-bar w3-border	">
		 <?php $jumlahData = $ctrlTuts->countAllTutorial() ?>

		 <?php
    echo "<script>console.log(".$jumlahData.")</script>";
 ?>

		 <?php
		 	$limitPerRow = 6;
			$limit = 5;
            if (isset($_GET['page'])) {
               $page = $_GET['page'];
            } else {
               $page = 0;
            }
            $b = intval($page/$limit);
			$c = ($b + 1) * 5;
			$batas = intval($jumlahData) / $limitPerRow;

			// echo $jumlahData;
			if ($page >= $limit) {
                    if ($page == $limit) {
                        $prev = $b * $limit - 2;
                    } else {
                        $prev = $b * $limit -1;
                    }

                    echo '<a class="w3-bar-item w3-button w3-hover-black" href="'.getBaseUrl().'tutorial/?page='.$prev.'">< Prev</a>';
            }
		 ?>



		 <?php for ($i=$b*$limit-1; $i < $c; $i++) { ?>
			 	<?php $nowPage = $i + 1 ?>
			 	<?php if ($i<$batas&&$nowPage!=0): ?>
			 	 <a class="w3-bar-item w3-button w3-hover-black" href="<?php echo getBaseUrl().'tutorial/?page='.$nowPage ?>">
				 	<?php echo $i + 1 ?>
				 </a>
				<?php endif ?>
		 <?php } ?>

		 <?php if ($c < $batas): ?>
		 	<?php $k = $c + 1; ?>
		 		<a class="w3-bar-item w3-button w3-hover-black" href="<?php echo getBaseUrl().'tutorial/?page='.$k ?>">
					Next >
				</a>
		<?php endif ?>

	 </div>
 </div>

	</div>
</section>
<?php endif ?>


<div class="w3-content"  style="max-width:1200px">
<?php include_once '../footer.php'; ?>
</div>

	<!-- <script type="text/javascript">
		$('#top-navigations>ul>li>a').hover(function() {
			$(this).css('color','red');
		},function(){
			$(this).css('color','orange');
		});

		$('#top-navigations>ul>li').hover(function() {
			$(this).delay(2000).css('background-color','white');
		},function(){
			$(this).delay(2000).css('background-color','#222');
		});

	</script> -->

<!-- // create the back to top button -->
<!-- <script type="text/javascript">
$('body').prepend('<a href="#" class="back-to-top">Back to Top</a>');
var amountScrolled = 300;
$(window).scroll(function() {
	if ( $(window).scrollTop() > amountScrolled ) {
		$('a.back-to-top').fadeIn('slow');
	} else {
		$('a.back-to-top').fadeOut('slow');
	}
});

$('a.back-to-top, a.simple-back-to-top').click(function() {
	$('html, body').animate({
		scrollTop: 0
	}, 700);
	return false;
});
</script> -->


<script src="<?php echo getBaseUrl() ?>assets/homepage/js/jquery-2.1.4.min.js"></script>

<script>

$( document ).ready(function() {
	$("#kotak_cari").on('keyup', function (e) {
	    if (e.keyCode == 13) {
	        test();
	    }
	});
    words = $("#kotak_cari").val();
		if (words != "" ){
			test();
		}
});

$( "#cari" ).click(function() {
test();
});


 function test() {
    var my_query = {
        'words': $("#kotak_cari").val()
    }
   $.ajax({
      type: "POST",
      url: "search",
			// url: "search.php",
			dataType: 'json',
      data: {
        'data': my_query
      },
      cache: false,
      success: function(data) {
				document.getElementById("tutorial_list").innerHTML = "";

				if (data.length == 0 ) {
					document.getElementById("tutorial_list").innerHTML += "<div class='w3-container'><h2>"+"Data tidak ditemukan"+"</h2></div>"
        }

				else {

				for (i=0; i < data.length ; i++){
					document.getElementById("tutorial_list").innerHTML += "<div class='w3-container'><div class='w3-container w3-white'><p>"+ "<a href=" + "https://fac-institute.com/" + "tutorial/?app=1&id=" + data[i].id + "#top_content  style='text-decoration:none' >" +"<b>"+ (data[i].judul).substr(0, 80) + "</b></a><span style='font-size:10px'>  ⏰" + data[i].tanggal +"</span></p>"+
					"<p style='font-size=12px;margin-top:-12px'>"+ ((data[i].isi).substr(0, 300)).replace(/<\/?[^>]+(>|$)/g, "") + "    <a class='w3-text-blue'  href=" + "https://fac-institute.com/" + "tutorial/?app=1&id=" + data[i].id + "#top_content  > Baca selengkapnya... <a/></p></div>"
					+"</div>"

				}

				}


      }
  }); //end ajax
 }

</script>

</body>
</html>
