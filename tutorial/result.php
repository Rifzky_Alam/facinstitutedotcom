<?php
include_once '../model/Tutorial.php';
$ctrlTuts = new ControlTutorial();

if (isset($_GET['app'])&&$_GET['app']=='1'&&isset($_GET['id'])&&!empty($_GET['id'])) {
	$dataTuts=$ctrlTuts->selectById($_GET['id']);
}

function getValue($value){
	if (empty($value)||$value=='') {
		return 'Tutorial tidak ada';
	} else {
		return $value;
	}
}
?>

<?php
function getBaseUrl(){
  $currentPath = $_SERVER['PHP_SELF'];$pathInfo = pathinfo($currentPath);$hostName = $_SERVER['HTTP_HOST'];$protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https://'?'https://':'http://';
  return 'https://'.$hostName."/";
}
?>

<?php $jumlahData = $ctrlTuts->countAllTutorial() ?>
<?php echo "<script>console.log(".$jumlahData.")</script>";?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0" />
<meta property="og:type" content="article" />
<meta property="og:site_name" content="FAC INSTITUTE" />
<?php if (isset($_GET['app'])&&$_GET['app']=='1'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>
<meta property="og:title" content=<?php echo "'$dataTuts->judul'" ?> />
<meta property="og:description" content=<?php echo "'$dataTuts->judul'" ?> />
<meta name="description" content=<?php echo "'$dataTuts->judul'" ?> itemprop="description" />
<meta name="robots" content="index, follow" />
<?php else: ?>
<?php endif ?>
<title>Reading Page</title>
<meta property="og:url" content=<?php echo '"http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'"'; ?> />
<meta property="fb:app_id" content="454421948087465" />
<meta property="og:image" content="http://fac-institute.com/images/logo-fac_header.png" />
<meta property="og:image:height" content="200" />
<meta property="og:image:width" content="200" />
<meta name="thumbnailUrl" content="http://alix.sch.id/pendaftaran/images/logo-fac.jpg" itemprop="thumbnailUrl" />
<meta charset="utf-8">
<?php if (isset($_GET['app'])&&$_GET['app']=='1'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>
	<title><?php echo $dataTuts->judul; ?></title>
	<?php else:?>
	<title>Halaman Tutorial</title>
<?php endif ?>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'></script>
<link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/w3.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/animate.min.css">
<link href="https://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,100,300,500,600,700c" type="text/css" />
<link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/style.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.10/lodash.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/maps/easy-autocomplete.min.css.map">
<link href="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/maps/easy-autocomplete.themes.min.css.map">
<link href="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/easy-autocomplete.min.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/easy-autocomplete.themes.min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo getBaseUrl() ?>tutorial/css/style.css">
</head>
<body>
  <?php include_once '../top-nav.php'; ?>
  <?php include_once '../modal-order.php'; ?>
  <?php function SectionHtml(){?>
  <div id="read" class="w3-container w3-padding-64">
    <?php }?>
    <?php if (isset($_GET['app'])&&$_GET['app']=='1'&&isset($_GET['id'])&&!empty($_GET['id'])): ?>
    <?php SectionHtml() ?>
		<script>
		$( document ).ready(function() {
			 document.getElementById("myInput").value = window.location.href;
		});
		function copyURL() {
		  var copyText = document.getElementById("myInput");
			copyText.value = window.location.href;
		  copyText.select();
		  document.execCommand("copy");
		  alert("Copied!");
		}
		</script>
    <div class="w3-content w3-padding-32" >
      <h2>
        <?php echo preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', getValue(@$dataTuts->judul)) ?>
      </h2>
      <?php echo $dataTuts->isi; ?>
			<script>
			var el = document.querySelectorAll('img');
			if( $(window).width() < 1000)
			{
				for(var i=0;i<el.length;i++){
					el[i].style = "height: auto;width:100%";
					console.log(el[i]);
				}
				el[0].style.width = "180px";
				el[1].style.width = "80px";
			}
			</script>
			<div class="w3-row w3-padding-32">
				<div class="w3-container w3-third">
					<input type="text" value="" class="w3-input" type="text" id="myInput">
				</div>
				<div class="w3-container w3-third">
				<button onclick="copyURL()" class="btn"><i class="fa fa-file-text-o"></i></button>
				<p style="display:inline">Copy Link</p>
				</div>
			</div>
    </div>
  </div>
  <?php else:?>
	<?php SectionHtml() ?>
	<div class="w3-container w3-padding-32">
		<h5 class="w3-center w3-padding-16"><span class="w3-tag w3-wide">Solution Center for ACCURATE & RENE</span></h5>
		<p class="w3-center">Kumpulan Informasi Panduan Dasar & Penyelesaian Masalah pada Accurate V3, V4, V5 & RENE</p>
  </div>
  <div class="w3-row w3-white">
    <div style="margin-top:20px" class="w3-col l8 s12">
			<div class="w3-row">
			  <div class="w3-container w3-twothird">
			    <input type="text" id="kotak_cari" name="search" class="w3-input" type="text" placeholder="Search..">
			  </div>
			  <div class="w3-container w3-third">
			    <button onclick="test2()"  class="w3-button w3-teal w3-block">Cari</button>
			  </div>
			</div>
      <div id="tutorial_list" class="w3-row-padding">
      </div>
      <div class="container">
      </div>
      <div class="page-load-status">
        <div class="loader-ellips infinite-scroll-request">
          <span class="loader-ellips__dot">
          </span>
          <span class="loader-ellips__dot">
          </span>
          <span class="loader-ellips__dot">
          </span>
          <span class="loader-ellips__dot">
          </span>
        </div>
        <div id="deoc" class="infinite-scroll-last w3-container w3-margin w3-pale-yellow" style="display:none">
          <p id="eoc">End of content
          </p>
        </div>
        <p class="infinite-scroll-error">
        </p>
      </div>
      <div class="w3-center w3-padding-32">
        <div id="paglist" class="w3-bar w3-border">
        </div>
      </div>
    </div>
    <div class="w3-col l4">
      <div class="w3-card-2 w3-margin w3-margin-top w3-hide-medium w3-hide-small">
        <img src="<?php echo getBaseUrl() ?>_caramel/assets/img/blog.jpg" style="width:100%">
        <div class="w3-container w3-white">
          <h4>
            <b>Tentang Kami
            </b>
          </h4>
          <p style="">FAC Institute adalah konsultan akuntansi & lembaga pendidikan komputerisasi akuntansi.
          </p>
        </div>
      </div>
      <hr>
      <!-- Posts -->
      <div class="w3-card-2 w3-margin">
        <div class="w3-container w3-padding">
          <h4>
            <b>Kategori
            </b>
          </h4>
        </div>
        <ul id="taglist" class="w3-ul w3-hoverable w3-white">
        </ul>
      </div>
      <hr>
    </div>
  </div>
<?php endif ?>
<div class="w3-content"  style="max-width:1200px">
  <?php include_once '../footer.php'; ?>
</div>
<script src='https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.js'></script>
<script src="https://fac-institute.com/administrasi/view/quotation/dist/jquery.easy-autocomplete.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/mark.js/8.11.1/jquery.mark.min.js"></script>
<script>
$(document).ready(function() {
    $("#kotak_cari").attr("value", getUrlParameter('key'));
    test();

    $("#kotak_cari").on('keyup', function(e) {
        if (e.keyCode == 13) {
            test2();
        }
    });
    words = $("#kotak_cari").val();
    if (words != "") {}
});

var regex = /\?ÕÌ_|_Œ‚|[ŠŽÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÑÒÓÔÕÖØÙÚÛÜÝÞßðÿ_]+/gi;

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};


var page_key = getUrlParameter('key');
var $container = $('.container').infiniteScroll({
    path: function() {
        return 'https://fac-institute.com/api/tutsvc?src=' + page_key;
    },
		responseType: 'json',
    history: false,
});

var infScroll = $container.data('infiniteScroll');
$container.on('load.infiniteScroll', function(event, data) {

if (data.data.length == 0 ){
	document.getElementById('eoc').innerHTML = "Maaf, tidak ada yang cocok dengan istilah pencarian Anda. Silakan coba lagi dengan beberapa kata kunci yang berbeda.";
	$("#deoc").removeClass('w3-pale-yellow').addClass('w3-pale-red');
}

    var page_num = infScroll.pageIndex;
    var offset = (Number(page_num) - 2) * 10;
    var limit = 10;
    result1 = _(data.data).slice(offset).take(limit).value();
    var dataParse = "";
    for (i = 0; i < result1.length; i++) {
        if (result1[i].thumbnail != '-') {
            dataParse += "<div class='w3-row w3-padding-16'><div class='w3-half w3-container'><img src='https://fac-institute.com/images/tutorial/thumbnails/" + result1[i].thumbnail + "' width='100%' /></div>" +
                "<div class='w3-half w3-container'><p>" + "<a href='https://fac-institute.com/tutorial/?app=1&id=" + result1[i].aidi + "'  style='text-decoration:none' >" + "<b>" + (result1[i].judul).replace(regex, ' ').replace(/[^\x00-\x7F]|\?/g, '') + "</b></a></p>" +
                "<p style='font-size=12px;margin-top:-12px'>" + ((result1[i].isi).substr(0, 400)).replace(/<\/?[^>]+(>|$)/g, "") + "<a class='w3-text-blue' target='' href='https://fac-institute.com/tutorial/?app=1&id=" + result1[i].aidi + "'  > Baca selengkapnya... <a/></p></div>";
        } else {
            dataParse += "<div class='w3-container w3-white okzx'><p>" + "<a href='https://fac-institute.com/tutorial/?app=1&id=" + result1[i].aidi + "'  style='text-decoration:none' >" + "<b>" + (result1[i].judul).replace(regex, ' ').replace(/[^\x00-\x7F]|\?/g, '') + "</b></a></p>" +
                "<p style='font-size=12px;margin-top:-12px'>" + ((result1[i].isi).substr(0, 400)).replace(/<\/?[^>]+(>|$)/g, "") + "<a class='w3-text-blue' target=''  href='https://fac-institute.com/tutorial/?app=1&id=" + result1[i].aidi + "'  > Baca selengkapnya... <a/></p></div>";
        }
    }

		$("div").mark(page_key);

    if (result1.length == 0) {
        $('.loader-ellips').hide();
        $('.infinite-scroll-last').show();
    }

    $container.infiniteScroll('appendItems', $(dataParse));

});

$container.infiniteScroll('loadNextPage');

function test() {
    var fruits = [];
		if (getUrlParameter('app') == 1)
		{}
		else {
			$.ajax({
					type: "GET",
					url: "https://fac-institute.com/api/tutsvc/listtags",
					dataType: 'json',
					cache: false,
					success: function(data) {
							var ff = _.orderBy(data.data, ['tags_txt'], ['asc', 'desc']);
							for (i = 0; i < ff.length; i++) {
									document.getElementById("taglist").innerHTML += "<li><a style='text-decoration:none' target='' href='" + "https://fac-institute.com/tutorial/tags?key=" + ff[i].tags_txt + "'>" + ff[i].tags_txt + " (" + ff[i].total_of_use + ")</a></li>";
							}
					}
			});
		}
}


$.ajax({
   type: "POST",
   url: "https://fac-institute.com/api/tutsvc?tagname=Solution%20Center%20for%20ACCURATE%20&%20RENE",
   cache: false,
   success: function(data) {
		 var option1 = {
				 data: data.data,getValue: "judul",
				 list: {
						 match: {enabled: true},
						 showAnimation: {type: "fade",time: 200,callback: function() {}},
						 hideAnimation: {type: "slide",time: 400,callback: function() {}},
						 maxNumberOfElements: 99,
						 sort: {
								 enabled: true
						 }
				 }
			 };

			 $("#kotak_cari").easyAutocomplete(option1);
   }
});

function test2() {
    var my_query = {
        'words': $("#kotak_cari").val()
    }

		if ($("#kotak_cari").val() == '' ){
			alert("Please fill out this field!");
		}
		else {
			    window.location.replace("https://fac-institute.com/tutorial/result?key=" + $("#kotak_cari").val());
		}
}
</script>
</body>
</html>
