<?php 
include_once '../model/page.php';
$myPage = new Page();

session_start();

$myPage->addLogForLogin($_SESSION['admin']['username'],'successfully logged out',$_SERVER['REMOTE_ADDR']);


session_destroy();

header('Location:../index.php')

?>