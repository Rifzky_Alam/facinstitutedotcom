<?php session_start() ?>
<?php include_once '../baseurl.php'; ?>
<?php 
if (!isset($_SESSION['fac_reg_token'])||empty($_SESSION['fac_reg_token'])) {
  $waktu = date('Y-m-d.H:i');
  $_SESSION['fac_reg_token'] = md5($_SERVER['REMOTE_ADDR'].$waktu);
}
// echo $_SESSION['fac_reg_token'];
?>

<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.min.css" />
</head>
<body>

<div class="container" style="padding-bottom:50px;">
  <div class="row">
      
      <div id="hasil">
        <div id="hasill">
          
        </div>
        <button id="tes">tes ajax</button>
      </div>
      
    </div>
</div>

<script src="https://fac-institute.com/assets/homepage/js/jquery-2.1.4.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script>

  Number.prototype.format = function(n, x) {
    var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
  };


  function GetProvinsi(){
    // https://fac-institute.com/administrasi/wilayah/provinsi/
    $.ajax({
      type: "POST",
      url: "https://fac-institute.com/administrasi/wilayah/provinsi/",
      data: {
        'id':''
      },
      cache: false,
      success: function(data){
        console.log(data);
      }
    }); //end ajax
  }

  function DeleteCalendar(parentid) {
    $.ajax({
            type: "POST",
            url: "https://fac-institute.com/administrasi/customerservice/api?req=cte",
            data: {
              'id':parentid
            },
            cache: false,
            success: function(data){
              console.log(data.totaldata);
            }
        }); //end ajax
  }


  $('#tes').click(function(){
    // alert('oke');
    DeleteCalendar('16');
    // GetProvinsi();
  });

    $(document).ready(function(){
        // ShowItems();
    });
</script>
</body>
</html>
