<?php 

// echo getcwd();
function openImage($path){
	if (preg_match('/.png/', $path)) {
		header("Content-type:image/png");
		readfile($path);
	}elseif (preg_match('/.jpg/', $path)||preg_match('/.jpeg/', $path)) {
		header("Content-type:image/jpeg");
		readfile($path);
	}elseif (preg_match('/.gif/', $path)) {
		header("Content-type:image/gif");
		readfile($path);
	}

	
}

function createThumbnail($image_name,$new_width,$new_height,$uploadDir,$moveToDir){
    $path = $uploadDir . $image_name;

    $mime = getimagesize($path);

    if($mime['mime']=='image/png') { 
        $src_img = imagecreatefrompng($path);
    }
    if($mime['mime']=='image/jpg' || $mime['mime']=='image/jpeg' || $mime['mime']=='image/pjpeg') {
        $src_img = imagecreatefromjpeg($path);
    }   

    $old_x          =   imageSX($src_img);
    $old_y          =   imageSY($src_img);

    if($old_x > $old_y) 
    {
        $thumb_w    =   $new_width;
        $thumb_h    =   $old_y*($new_height/$old_x);
    }

    if($old_x < $old_y) 
    {
        $thumb_w    =   $old_x*($new_width/$old_y);
        $thumb_h    =   $new_height;
    }

    if($old_x == $old_y) {
        $thumb_w    =   $new_width;
        $thumb_h    =   $new_height;
    }

    $dst_img        =   ImageCreateTrueColor($thumb_w,$thumb_h);

    imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y); 


    // New save location
    $new_thumb_loc = $moveToDir . $image_name;

    if($mime['mime']=='image/png') {
        $result = imagepng($dst_img,$new_thumb_loc,8);
    }
    if($mime['mime']=='image/jpg' || $mime['mime']=='image/jpeg' || $mime['mime']=='image/pjpeg') {
        $result = imagejpeg($dst_img,$new_thumb_loc,80);
    }

    imagedestroy($dst_img); 
    imagedestroy($src_img);

    return $result;
}
$dir = '/home/facinsti/public_html/images/nitip/';
$filename = 'luluandrifzky2.jpg';
createThumbnail($filename,1300,750,$dir,$dir);
openImage($dir.$filename);

?>