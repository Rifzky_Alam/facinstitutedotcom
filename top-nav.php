      <!-- Navbar (sit on top) -->
      <div class="w3-top">
        <div class="w3-bar w3-indigo w3-card-2" id="myNavbar">
          <a  href="<?php echo getBaseUrl() ?>"  class="w3-bar-item w3-button w3-wide">
            <img width="180px" src="<?php echo getBaseUrl() ?>_caramel/assets/img/logofac3.png" />
          </a>
          <!-- Right-sided navbar links -->
          <div class="w3-right w3-hide-small">
            <!-- <a href="<?php echo getBaseUrl() ?>order/" class="w3-bar-item w3-button">Training</a> -->
            <a class="w3-bar-item w3-button" href="<?php echo getBaseUrl() ?>"onclick="w3_close()">Home</a>
            <a class="w3-bar-item w3-button" href="<?php echo getBaseUrl() ?>training">Training</a>
            <a class="w3-bar-item w3-button" href="<?php echo getBaseUrl() ?>kursus-akuntansi/">Kursus</a>
            <a href="<?php echo getBaseUrl() ?>artikel/" class="w3-bar-item w3-button">Artikel</a>
            <a href="<?php echo getBaseUrl() ?>tutorial/" class="w3-bar-item w3-button">Tutorial</a>
            <a class="w3-bar-item w3-button" href=<?php echo "'".getBaseUrl()."login"."'"; ?>>Login </a>
            <a class="w3-bar-item w3-button" href="<?php echo getBaseUrl() ?>download_new" >Download</a>
            <a class="w3-bar-item w3-button" href="<?php echo getBaseUrl() ?>facteam" >FAC Team</a>
          </div>
          <a href="javascript:void(0)" class="w3-bar-item w3-button w3-right w3-hide-large w3-hide-medium" onclick="w3_open()">
            <i class="fa fa-bars w3-padding-right w3-padding-left"></i>
          </a>
        </div>
      </div>

      <!-- Sidenav on small screens when clicking the menu icon -->
      <nav class="w3-sidenav w3-black w3-card-2 w3-animate-left w3-hide-medium w3-hide-large" style="display:none" id="mySidenav">
        <a href="javascript:void(0)" onclick="w3_close()" class="w3-large w3-padding-16">Close ×</a>
        <a href="<?php echo getBaseUrl() ?>"onclick="w3_close()">Home</a>
        <a href="<?php echo getBaseUrl() ?>training"onclick="w3_close()">Training</a>
        <a href="<?php echo getBaseUrl() ?>kursus-akuntansi/"onclick="w3_close()">Kursus</a>
        <a href="<?php echo getBaseUrl() ?>artikel/"onclick="w3_close()">Artikel</a>
        <a href="<?php echo getBaseUrl() ?>tutorial/"onclick="w3_close()">Tutorial</a>
        <a href=<?php echo "'".getBaseUrl()."login"."'"; ?> onclick="w3_close()">Login</a>
        <a href="<?php echo getBaseUrl() ?>download_new"> Download</a>
        <a href="<?php echo getBaseUrl() ?>facteam"> FAC Team</a>
        <!-- <a href="#team" onclick="w3_close()">MENU 2</a>
        <a href="#work" onclick="w3_close()">MENU 3</a> -->
      </nav>

      <script>
      var mySidenav = document.getElementById("mySidenav");
      function w3_open() {
          if (mySidenav.style.display === 'block') {
              mySidenav.style.display = 'none';
          } else {
              mySidenav.style.display = 'block';
          }
      }


      function w3_close() {
          mySidenav.style.display = "none";
      }
      </script>
