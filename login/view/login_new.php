
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="google-site-verification" content="EfNVtXecxElbcbugY59SeKXJuMzO67bjVvTvlHCc_CM" />
    <meta name="msvalidate.01" content="D925497D8F382164FF0CE0267D381FDA" />
    <meta name="thumbnailUrl" content="https://fac-institute.com/_caramel/assets/img/g44508.png" />
    <meta name="description" content="FAC - Kami Melayani Training Software Akuntansi ACCURATE dan Accounting Service Untuk Perusahaan Anda" />
    <meta content="First Asian Consulting. Konsultan Akuntansi & Lembaga Pendidikan Komputerisasi Akuntansi" />
    <meta name="keywords" content="Training,Akuntansi,ACCURATE,Jasa">
    <meta name="author" content="Rifzky Alam, Dino Damara">
    <title>FAC Institute</title>
    <script defer src="https://use.fontawesome.com/releases/v5.5.0/js/solid.js" integrity="sha384-Xgf/DMe1667bioB9X1UM5QX+EG6FolMT4K7G+6rqNZBSONbmPh/qZ62nBPfTx+xG" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.5.0/js/regular.js" integrity="sha384-XrvTJeiQ46fxxPrZP6fay5yejA2FV4G1XsS8E4Piz6Fz+7FaEFTw7A7GR972irVV" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.5.0/js/brands.js" integrity="sha384-S2C955KPLo8/zc2J7kJTG38hvFV+SnzXM6hwfEUhGHw5wPo6uXbnbjSJgw3clO4G" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.5.0/js/fontawesome.js" integrity="sha384-bNOdVeWbABef8Lh4uZ8c3lJXVlHdf8W5hh1OpJ4dGyqIEhMmcnJrosjQ36Kniaqm" crossorigin="anonymous"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,100,300,500,600,700c" type="text/css" />
    <style>
    .navbar {
      font-family: "Raleway", sans-serif;
    }
    body, html, h3 {
        font-family: 'PT Sans'", sans-serif";
    }
    .navbar-expand-lg .navbar-nav .nav-link {
       padding-right: 1em;
    }

    ::-webkit-input-placeholder { /* Chrome/Opera/Safari */
      font-size: 14px;
    }
    ::-moz-placeholder { /* Firefox 19+ */
      font-size: 14px;
    }
    :-ms-input-placeholder { /* IE 10+ */
      font-size: 14px;
    }
    :-moz-placeholder { /* Firefox 18- */
      font-size: 14px;
    }


    :root {
  --input-padding-x: 1.5rem;
  --input-padding-y: .75rem;
}

body {
  background: #9CECFB;
  background: -webkit-linear-gradient(to right, #0052D4, #65C7F7, #9CECFB);
  background: linear-gradient(to right, #0052D4, #65C7F7, #9CECFB);

  /* background-image: url("https://image.freepik.com/free-vector/education-pattern_1061-494.jpg"); */
  background-image: url("https://images7.alphacoders.com/451/451791.jpg");

  background-repeat: repeat;
}

.card-signin {
  border: 0;
  border-radius: 1rem;
  box-shadow: 0 0.5rem 1rem 0 rgba(0, 0, 0, 0.1);
}

.card-signin .card-title {
  margin-bottom: 2rem;
  font-weight: 300;
  font-size: 1.5rem;
}

.card-signin .card-body {
  padding: 2rem;
}

.form-signin {
  width: 100%;
}

.form-signin .btn {
  font-size: 80%;
  border-radius: 5rem;
  letter-spacing: .1rem;
  font-weight: bold;
  padding: 1rem;
  transition: all 0.2s;
}

.form-label-group {
  position: relative;
  margin-bottom: 1rem;
}

.form-label-group input {
  border-radius: 2rem;
}

/* .form-label-group>input,
.form-label-group>label {
  padding: var(--input-padding-y) var(--input-padding-x);
} */

.form-label-group>label {
  /* position: absolute; */
  top: 0;
  left: 0;
  display: block;
  width: 100%;
  margin-bottom: 2;
  line-height: 1.5;
  color: #495057;
  border: 1px solid transparent;
  border-radius: .25rem;
  transition: all .1s ease-in-out;
  font-size: 12px;
}

.form-label-group input::-webkit-input-placeholder {
  color: transparent;
}

.form-label-group input:-ms-input-placeholder {
  color: transparent;
}

.form-label-group input::-ms-input-placeholder {
  color: transparent;
}

.form-label-group input::-moz-placeholder {
  color: transparent;
}

.form-label-group input::placeholder {
  color: transparent;
}

/* .form-label-group input:not(:placeholder-shown) {
  padding-top: calc(var(--input-padding-y) + var(--input-padding-y) * (2 / 3));
  padding-bottom: calc(var(--input-padding-y) / 3);
} */

.form-label-group input:not(:placeholder-shown)~label {
  padding-top: calc(var(--input-padding-y) / 3);
  padding-bottom: calc(var(--input-padding-y) / 3);
  font-size: 12px;
  color: #777;
}

.btn-google {
  color: white;
  background-color: #ea4335;
}

.btn-facebook {
  color: white;
  background-color: #3b5998;
}
    </style>

</head>
<body>


<body>
  <div class="container">
    <div class="row">
      <div class="col-sm-9 col-md-5 col-lg-4 mx-auto">
        <div class="card card-signin my-5">
          <div class="card-body">
            <h5 class="card-title text-center">Sign In</h5>
            <form class="form-signin" method="post" action="">
              <div class="form-label-group">
                <label for="inputUser">Username</label>
                <input type="text" id="inputUser" name="username" class="form-control" placeholder="Username" required autofocus>

              </div>

              <div class="form-label-group">
                <label for="inputPassword">Password</label>
                <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
              </div>


              <img style="margin-bottom:10px;" src="https://fac-institute.com/member/captcha-fac" class="img-thumbnail float-right" alt="Captcha" width="100%" />
              <br/>
              <div class="form-label-group">
                <label>Captcha</label>
                <input type="text" class="form-control" placeholder="ketik tulisan pada gambar" name="captchaz" required autofocus>
              </div>


              <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Sign in</button>
              <!-- <hr class="my-4">
              <button class="btn btn-lg btn-google btn-block text-uppercase" type="submit"><i class="fab fa-google mr-2"></i> Sign in with Google</button>
              <button class="btn btn-lg btn-facebook btn-block text-uppercase" type="submit"><i class="fab fa-facebook-f mr-2"></i> Sign in with Facebook</button> -->
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>



    <!-- JQuery -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/umd/popper.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.min.js"></script>


</body>
</html>
