<?php include_once 'baseurl.php'; ?>
<html class="no-js" lang="id">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="google-site-verification" content="EfNVtXecxElbcbugY59SeKXJuMzO67bjVvTvlHCc_CM" />
    <meta name="msvalidate.01" content="D925497D8F382164FF0CE0267D381FDA" />
    <meta name="thumbnailUrl" content="https://www.fac-institute.com/administrasi/artikel/images/7cabb38e259c0d5b53f9bb219daf410f.jpg?w=650" />
    <meta name="description" content="FAC - Kami Melayani Training Software Akuntansi ACCURATE dan Accounting Service Untuk Perusahaan Anda" />
    <meta content="First Asian Consulting. Konsultan Akuntansi & Lembaga Pendidikan Komputerisasi Akuntansi" />
    <meta name="keywords" content="Training,Akuntansi,ACCURATE,Jasa">
    <meta name="author" content="Rifzky Alam, Dino Damara">
    <title>FAC Institute</title>
    <link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/w3.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/animate.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,100,300,500,600,700c" type="text/css" />
 <style>
 .w3-top{
   font-family: "Raleway", sans-serif;
 }
 body, html, h3 {
     font-family: 'PT Sans', sans-serif;
 }
 p,li{
   font-size: 16px;
 }

 </style>


<link rel="stylesheet" href="<?php echo getBaseUrl() ?>_caramel/assets/css/style.css">


<script src="<?php echo getBaseUrl() ?>assets/homepage/js/jquery-2.1.4.min.js"></script>




</head>
<body>


<h4>header</h4>
<?php include_once 'top-nav.php'; ?>
<?php include_once 'modal-order.php'; ?>

<div class="w3-content w3-padding-64" style="max-width:800px">
  <div class="w3-padding-32">
   <img id="img_banner" class="w3-image" src="<?php echo getBaseUrl() ?>_caramel/assets/img/rect22280.png" alt="Me" width="1200" >
  </div>
</div>


</body>
</html>
